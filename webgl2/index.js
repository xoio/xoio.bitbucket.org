const raf = require('raf');
const glslify = require('glslify');

import TFBuffer from "./src/TFBuffer"
import ParticleSystem from "./src/ParticleSystem"


////////// SETUP COMPONENTS //////////////
var components = canWebGL2();
var renderer = new THREE.WebGLRenderer( { canvas: components.domElement, context: components.gl } );
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.sortObjects = false;
document.body.appendChild(renderer.domElement);

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 1, 1024 );
camera.position.y = 1.5;
camera.position.z = 4;
camera.lookAt( scene.position );


/**
 * I don't know why, but without SOMETHING in the scene,
 * we get a ton of errors relating to trying to bind uniforms belonging
 * to what I assume is the default THREE shader.
 * @type {THREE.PlaneBufferGeometry}
 */
var m = new THREE.PlaneBufferGeometry(0,0);
var mat = new THREE.MeshBasicMaterial({color:0x000000,transparent:true});
var mesh = new THREE.Mesh(m,mat);
mesh.position.x = 0

scene.add(mesh);

////////// SETUP ELEMENTS //////////////
var system = new ParticleSystem(renderer);
system.addTo(scene);

////////// RENDER //////////////
raf(function tick(){

    system.update();
    renderer.render( scene, camera );

    raf(tick);
});


/**
 * Checks for WebGl2 capabilities
 * @returns {{domElement: Element, gl: CanvasRenderingContext2D}}
 */
function canWebGL2(){
    var canvas = document.createElement("canvas");
    // Try creating a WebGL 2 context first
    var gl = canvas.getContext( 'webgl2', { antialias: false } );
    if (!gl) {
        gl = canvas.getContext( 'experimental-webgl2', { antialias: false } );
    }
    var isWebGL2 = !!gl;
    if (isWebGL2) {
        console.log("I can haz flag, so WebGL 2 is yes!")
    }

    return {
        domElement:canvas,
        gl:gl
    }
}
