# webgl2 #

This is just a very surface level exploration in transform feedback in webgl 2 using three.js. 
It's largely based on [Brandon Jones's](https://twitter.com/Tojiro) [example](http://toji.github.io/webgl2-particles/) with some slight tweaks to bring things more in line with ES6 style code as well as use things like glslify to help get rid of the need to write the shaders in that awful array style. 

Theres also a slight tweak to the animation, the particles will appear to break off from the lettering, then reform the letters every so often.

### How do I get set up? ###

* `npm install`
* run `npm start` to compile your source file(defaults to `index.js`) which will appear in the `public/js` folder
* this uses watchify to continuously update code.