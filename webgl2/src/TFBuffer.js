/**
 * Tranform feedback buffer based on @toji's example
 * view-source:http://toji.github.io/webgl2-particles/
 */

class TFBuffer {
    constructor(ctx){
        this.gl = ctx !== undefined ? ctx : (function(){
            console.error("A TFBuffer needs a webgl context to function");
        });

        this.tf = this.gl.createTransformFeedback();
        this.count = 0;
    }


    pass(shader,source,target,originBuffer,time){
        var program = shader;
        var sourceAttrib = source.attributes['position'];
        let gl = this.gl;
        let transformFeedback = this.tf;

        var attributes = {
            position: 0,
            origin: 1
        };

        if (target.attributes['position'].buffer && sourceAttrib.buffer) {

            gl.useProgram(program);
            gl.uniform1f(program.uniforms["timer"], time);
            //gl.uniform4fv(uniforms.colliders, collidersValue);

            gl.enableVertexAttribArray( attributes.origin );
            gl.bindBuffer(gl.ARRAY_BUFFER, originBuffer);
            gl.vertexAttribPointer( attributes.origin, 4, gl.FLOAT, false, 16, 0 );

            gl.enableVertexAttribArray(attributes.position);
            gl.bindBuffer(gl.ARRAY_BUFFER, sourceAttrib.buffer);
            gl.vertexAttribPointer(attributes.position, 4, gl.FLOAT, false, 16, 0);

            gl.bindTransformFeedback(gl.TRANSFORM_FEEDBACK, transformFeedback);
            gl.bindBufferBase(gl.TRANSFORM_FEEDBACK_BUFFER, 0, target.attributes['position'].buffer);
            gl.enable(gl.RASTERIZER_DISCARD);
            gl.beginTransformFeedback(gl.POINTS);

            gl.drawArrays(gl.POINTS, 0, sourceAttrib.length / sourceAttrib.itemSize);

            gl.endTransformFeedback();
            gl.disable(gl.RASTERIZER_DISCARD);

            // Unbind the transform feedback buffer so subsequent attempts
            // to bind it to ARRAY_BUFFER work.
            gl.bindBufferBase(gl.TRANSFORM_FEEDBACK_BUFFER, 0, null);

            //gl.bindTransformFeedback(gl.TRANSFORM_FEEDBACK, 0);


        }
    }


}

export default TFBuffer


/*

 gl.useProgram(program);
 //gl.uniform1f(uniforms.timer, timerValue);
 //gl.uniform4fv(uniforms.colliders, collidersValue);

 gl.enableVertexAttribArray( attributes.origin );
 gl.bindBuffer(gl.ARRAY_BUFFER, originBuffer);
 gl.vertexAttribPointer( attributes.origin, 4, gl.FLOAT, false, 16, 0 );

 gl.enableVertexAttribArray(attributes.position);
 gl.bindBuffer(gl.ARRAY_BUFFER, sourceAttrib.buffer);
 gl.vertexAttribPointer(attributes.position, 4, gl.FLOAT, false, 16, 0);

 gl.bindTransformFeedback(gl.TRANSFORM_FEEDBACK, transformFeedback);
 gl.bindBufferBase(gl.TRANSFORM_FEEDBACK_BUFFER, 0, target.attributes['position'].buffer);
 gl.enable(gl.RASTERIZER_DISCARD);
 gl.beginTransformFeedback(gl.POINTS);

 gl.drawArrays(gl.POINTS, 0, sourceAttrib.length / sourceAttrib.itemSize);

 gl.endTransformFeedback();
 gl.disable(gl.RASTERIZER_DISCARD);

 // Unbind the transform feedback buffer so subsequent attempts
 // to bind it to ARRAY_BUFFER work.
 gl.bindBufferBase(gl.TRANSFORM_FEEDBACK_BUFFER, 0, null);

 //gl.bindTransformFeedback(gl.TRANSFORM_FEEDBACK, 0);
 */