uniform float pointSize;
varying vec3 vPosition;

void main() {
  vPosition = position;
  gl_PointSize = pointSize;
  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
}