 varying vec3 vPosition;
void main(){
  float depth = smoothstep( 10.24, 1.0, gl_FragCoord.z / gl_FragCoord.w );
  gl_FragColor = vec4( (vec3(0.0, 0.03, 0.05) + (vPosition * 0.25)), depth );
  //gl_FragColor = vec4(1.0,1.0,0.0,1.0);
 }