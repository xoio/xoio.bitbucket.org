const glslify = require('glslify');
const createShader = require('gl-shader');
const createBuffer = require('gl-buffer');


import TFBuffer from "./TFBuffer"

class ParticleSystem {
    constructor(renderer,size) {
        this.size = size !== undefined ? size : (1024 * 1024 * 4);
        this.gl = renderer.getContext();
        this.renderer = renderer;
        this.count = 0;
        this.timer = 0.0;

        this.originBuffer = this.gl.createBuffer();

        this._buildData();

    }

    addTo(scene){
        scene.add(this.mesh);
        this.scene = scene;
    }

    update(){
        let scene = this.scene;
        let gl = this.gl;
        let gpgpu = this.gpgpu;
        let count = this.count;
        let mesh = this.mesh;
        let simulationShader = this.simulationShader;
        let geometry = this.geometry;
        let geometry2 = this.geometry2;

        this.timer += 0.002;


        // Ugly hack to make the particle mesh always draw last
        scene.remove( mesh ); scene.add( mesh );

            if ( count % 2 === 0 ) {
                gpgpu.pass( simulationShader, geometry2, geometry ,this.originBuffer,this.timer);
                mesh.geometry = geometry2;
            } else {
                gpgpu.pass( simulationShader, geometry, geometry2,this.originBuffer,this.timer );
                mesh.geometry = geometry;
            }


        this.count ++;

    }

    _buildData(){
        var width = 1024;
        var height = 1024;

        let gl = this.gl;
        // Start Creation of DataTexture
        var textGeo = new THREE.TextGeometry( "WebGL 2", {
            size: 1.0,
            height: 0.25,
            curveSegments: 0,
            font: "helvetiker",
            weight: "bold",
            style: "normal",
        });

        textGeo.computeBoundingBox();
        var bounds = textGeo.boundingBox;
        textGeo.applyMatrix( new THREE.Matrix4().makeTranslation(
            (bounds.max.x - bounds.min.x) * -0.5,
            (bounds.max.y - bounds.min.y) * -0.5,
            (bounds.max.z - bounds.min.z) * -0.5));



        var points = THREE.GeometryUtils.randomPointsInGeometry( textGeo, width * height );
        var n = 800, n2 = n/2;	// triangles spread in the cube
        var data = new Float32Array( width * height * 4 );
        for ( var i = 0, j = 0, l = data.length; i < l; i += 4, j += 1 ) {


            data[ i ] = points[j].x
            data[ i + 1 ] = points[ j ].y;
            data[ i + 2 ] = points[ j ].z;
            data[ i + 3 ] = 0.0;
        }


            var geometry = new THREE.BufferGeometry();
            geometry.addAttribute( 'position', new THREE.BufferAttribute( data, 4 ) );
            var geometry2 = geometry.clone();

            var material = new THREE.ShaderMaterial( {
                uniforms: {
                    "pointSize": { type: "f", value: 2.0 }
                },
                vertexShader: glslify('./shaders/particles-vs.glsl'),
                fragmentShader: glslify('./shaders/particles-fs.glsl'),
                blending: THREE.AdditiveBlending,
                depthWrite: false,
                depthTest: true,
                transparent: true
            } );

            var gpgpu = new TFBuffer( this.renderer.getContext());
            var simulationShader = this.createProgram();


            this.setOriginData(data);



        var mesh = new THREE.PointCloud( geometry, material );

        this.mesh = mesh;
        this.gpgpu = gpgpu;
        this.geometry = geometry;
        this.geometry2 = geometry2;
        this.simulationShader =simulationShader;
    }

    setOriginData(data) {
        let gl = this.gl;
        gl.bindBuffer(gl.ARRAY_BUFFER, this.originBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
    }

    createProgram (){
        var attributes = {
            position: 0,
            origin: 1
        };

        let gl = this.gl;

        var vertexShader = gl.createShader( gl.VERTEX_SHADER );
        var fragmentShader = gl.createShader( gl.FRAGMENT_SHADER );

        gl.shaderSource( vertexShader, glslify('./shaders/simulation.glsl'));

        gl.shaderSource( fragmentShader, glslify('./shaders/simfrag.glsl'));

        gl.compileShader( vertexShader );
        if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
            console.error("Shader failed to compile", gl.getShaderInfoLog( vertexShader ));
            return null;
        }

        gl.compileShader( fragmentShader );
        if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
            console.error("Shader failed to compile", gl.getShaderInfoLog( fragmentShader ));
            return null;
        }

        var program = gl.createProgram();

        gl.attachShader( program, vertexShader );
        gl.attachShader( program, fragmentShader );

        gl.deleteShader( vertexShader );
        gl.deleteShader( fragmentShader );

        gl.transformFeedbackVaryings( program, ["outpos"], gl.SEPARATE_ATTRIBS );

        gl.linkProgram( program );

        if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
            console.error("Shader program failed to link", gl.getProgramInfoLog( program ));
            gl.deleteProgram(program);
            return null;
        }
        var uniforms = [];
        var count = gl.getProgramParameter(program, gl.ACTIVE_UNIFORMS);
        for (var i = 0; i < count; i++) {
            var uniform = gl.getActiveUniform(program, i);
            var name = uniform.name.replace("[0]", "");
            uniforms[name] = gl.getUniformLocation(program, name);
        }
        program.uniforms = uniforms;
        return program;
    }
}



export default ParticleSystem;