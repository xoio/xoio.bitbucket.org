(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Gets the last element of `array`.
 *
 * @static
 * @memberOf _
 * @category Array
 * @param {Array} array The array to query.
 * @returns {*} Returns the last element of `array`.
 * @example
 *
 * _.last([1, 2, 3]);
 * // => 3
 */
function last(array) {
  var length = array ? array.length : 0;
  return length ? array[length - 1] : undefined;
}

module.exports = last;

},{}],2:[function(require,module,exports){
/** Used as the `TypeError` message for "Functions" methods. */
var FUNC_ERROR_TEXT = 'Expected a function';

/* Native method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max;

/**
 * Creates a function that invokes `func` with the `this` binding of the
 * created function and arguments from `start` and beyond provided as an array.
 *
 * **Note:** This method is based on the [rest parameter](https://developer.mozilla.org/Web/JavaScript/Reference/Functions/rest_parameters).
 *
 * @static
 * @memberOf _
 * @category Function
 * @param {Function} func The function to apply a rest parameter to.
 * @param {number} [start=func.length-1] The start position of the rest parameter.
 * @returns {Function} Returns the new function.
 * @example
 *
 * var say = _.restParam(function(what, names) {
 *   return what + ' ' + _.initial(names).join(', ') +
 *     (_.size(names) > 1 ? ', & ' : '') + _.last(names);
 * });
 *
 * say('hello', 'fred', 'barney', 'pebbles');
 * // => 'hello fred, barney, & pebbles'
 */
function restParam(func, start) {
  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  start = nativeMax(start === undefined ? (func.length - 1) : (+start || 0), 0);
  return function() {
    var args = arguments,
        index = -1,
        length = nativeMax(args.length - start, 0),
        rest = Array(length);

    while (++index < length) {
      rest[index] = args[start + index];
    }
    switch (start) {
      case 0: return func.call(this, rest);
      case 1: return func.call(this, args[0], rest);
      case 2: return func.call(this, args[0], args[1], rest);
    }
    var otherArgs = Array(start + 1);
    index = -1;
    while (++index < start) {
      otherArgs[index] = args[index];
    }
    otherArgs[start] = rest;
    return func.apply(this, otherArgs);
  };
}

module.exports = restParam;

},{}],3:[function(require,module,exports){
(function (global){
var cachePush = require('./cachePush'),
    getNative = require('./getNative');

/** Native method references. */
var Set = getNative(global, 'Set');

/* Native method references for those with the same name as other `lodash` methods. */
var nativeCreate = getNative(Object, 'create');

/**
 *
 * Creates a cache object to store unique values.
 *
 * @private
 * @param {Array} [values] The values to cache.
 */
function SetCache(values) {
  var length = values ? values.length : 0;

  this.data = { 'hash': nativeCreate(null), 'set': new Set };
  while (length--) {
    this.push(values[length]);
  }
}

// Add functions to the `Set` cache.
SetCache.prototype.push = cachePush;

module.exports = SetCache;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./cachePush":40,"./getNative":54}],4:[function(require,module,exports){
/**
 * Copies the values of `source` to `array`.
 *
 * @private
 * @param {Array} source The array to copy values from.
 * @param {Array} [array=[]] The array to copy values to.
 * @returns {Array} Returns `array`.
 */
function arrayCopy(source, array) {
  var index = -1,
      length = source.length;

  array || (array = Array(length));
  while (++index < length) {
    array[index] = source[index];
  }
  return array;
}

module.exports = arrayCopy;

},{}],5:[function(require,module,exports){
/**
 * A specialized version of `_.forEach` for arrays without support for callback
 * shorthands and `this` binding.
 *
 * @private
 * @param {Array} array The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns `array`.
 */
function arrayEach(array, iteratee) {
  var index = -1,
      length = array.length;

  while (++index < length) {
    if (iteratee(array[index], index, array) === false) {
      break;
    }
  }
  return array;
}

module.exports = arrayEach;

},{}],6:[function(require,module,exports){
/**
 * A specialized version of `_.map` for arrays without support for callback
 * shorthands and `this` binding.
 *
 * @private
 * @param {Array} array The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the new mapped array.
 */
function arrayMap(array, iteratee) {
  var index = -1,
      length = array.length,
      result = Array(length);

  while (++index < length) {
    result[index] = iteratee(array[index], index, array);
  }
  return result;
}

module.exports = arrayMap;

},{}],7:[function(require,module,exports){
/**
 * Appends the elements of `values` to `array`.
 *
 * @private
 * @param {Array} array The array to modify.
 * @param {Array} values The values to append.
 * @returns {Array} Returns `array`.
 */
function arrayPush(array, values) {
  var index = -1,
      length = values.length,
      offset = array.length;

  while (++index < length) {
    array[offset + index] = values[index];
  }
  return array;
}

module.exports = arrayPush;

},{}],8:[function(require,module,exports){
/**
 * A specialized version of `_.some` for arrays without support for callback
 * shorthands and `this` binding.
 *
 * @private
 * @param {Array} array The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {boolean} Returns `true` if any element passes the predicate check,
 *  else `false`.
 */
function arraySome(array, predicate) {
  var index = -1,
      length = array.length;

  while (++index < length) {
    if (predicate(array[index], index, array)) {
      return true;
    }
  }
  return false;
}

module.exports = arraySome;

},{}],9:[function(require,module,exports){
/**
 * Used by `_.defaults` to customize its `_.assign` use.
 *
 * @private
 * @param {*} objectValue The destination object property value.
 * @param {*} sourceValue The source object property value.
 * @returns {*} Returns the value to assign to the destination object.
 */
function assignDefaults(objectValue, sourceValue) {
  return objectValue === undefined ? sourceValue : objectValue;
}

module.exports = assignDefaults;

},{}],10:[function(require,module,exports){
var keys = require('../object/keys');

/**
 * A specialized version of `_.assign` for customizing assigned values without
 * support for argument juggling, multiple sources, and `this` binding `customizer`
 * functions.
 *
 * @private
 * @param {Object} object The destination object.
 * @param {Object} source The source object.
 * @param {Function} customizer The function to customize assigned values.
 * @returns {Object} Returns `object`.
 */
function assignWith(object, source, customizer) {
  var index = -1,
      props = keys(source),
      length = props.length;

  while (++index < length) {
    var key = props[index],
        value = object[key],
        result = customizer(value, source[key], key, object, source);

    if ((result === result ? (result !== value) : (value === value)) ||
        (value === undefined && !(key in object))) {
      object[key] = result;
    }
  }
  return object;
}

module.exports = assignWith;

},{"../object/keys":93}],11:[function(require,module,exports){
var baseCopy = require('./baseCopy'),
    keys = require('../object/keys');

/**
 * The base implementation of `_.assign` without support for argument juggling,
 * multiple sources, and `customizer` functions.
 *
 * @private
 * @param {Object} object The destination object.
 * @param {Object} source The source object.
 * @returns {Object} Returns `object`.
 */
function baseAssign(object, source) {
  return source == null
    ? object
    : baseCopy(source, keys(source), object);
}

module.exports = baseAssign;

},{"../object/keys":93,"./baseCopy":13}],12:[function(require,module,exports){
var baseMatches = require('./baseMatches'),
    baseMatchesProperty = require('./baseMatchesProperty'),
    bindCallback = require('./bindCallback'),
    identity = require('../utility/identity'),
    property = require('../utility/property');

/**
 * The base implementation of `_.callback` which supports specifying the
 * number of arguments to provide to `func`.
 *
 * @private
 * @param {*} [func=_.identity] The value to convert to a callback.
 * @param {*} [thisArg] The `this` binding of `func`.
 * @param {number} [argCount] The number of arguments to provide to `func`.
 * @returns {Function} Returns the callback.
 */
function baseCallback(func, thisArg, argCount) {
  var type = typeof func;
  if (type == 'function') {
    return thisArg === undefined
      ? func
      : bindCallback(func, thisArg, argCount);
  }
  if (func == null) {
    return identity;
  }
  if (type == 'object') {
    return baseMatches(func);
  }
  return thisArg === undefined
    ? property(func)
    : baseMatchesProperty(func, thisArg);
}

module.exports = baseCallback;

},{"../utility/identity":107,"../utility/property":108,"./baseMatches":29,"./baseMatchesProperty":30,"./bindCallback":38}],13:[function(require,module,exports){
/**
 * Copies properties of `source` to `object`.
 *
 * @private
 * @param {Object} source The object to copy properties from.
 * @param {Array} props The property names to copy.
 * @param {Object} [object={}] The object to copy properties to.
 * @returns {Object} Returns `object`.
 */
function baseCopy(source, props, object) {
  object || (object = {});

  var index = -1,
      length = props.length;

  while (++index < length) {
    var key = props[index];
    object[key] = source[key];
  }
  return object;
}

module.exports = baseCopy;

},{}],14:[function(require,module,exports){
var isObject = require('../lang/isObject');

/**
 * The base implementation of `_.create` without support for assigning
 * properties to the created object.
 *
 * @private
 * @param {Object} prototype The object to inherit from.
 * @returns {Object} Returns the new object.
 */
var baseCreate = (function() {
  function object() {}
  return function(prototype) {
    if (isObject(prototype)) {
      object.prototype = prototype;
      var result = new object;
      object.prototype = undefined;
    }
    return result || {};
  };
}());

module.exports = baseCreate;

},{"../lang/isObject":73}],15:[function(require,module,exports){
var baseIndexOf = require('./baseIndexOf'),
    cacheIndexOf = require('./cacheIndexOf'),
    createCache = require('./createCache');

/** Used as the size to enable large array optimizations. */
var LARGE_ARRAY_SIZE = 200;

/**
 * The base implementation of `_.difference` which accepts a single array
 * of values to exclude.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {Array} values The values to exclude.
 * @returns {Array} Returns the new array of filtered values.
 */
function baseDifference(array, values) {
  var length = array ? array.length : 0,
      result = [];

  if (!length) {
    return result;
  }
  var index = -1,
      indexOf = baseIndexOf,
      isCommon = true,
      cache = (isCommon && values.length >= LARGE_ARRAY_SIZE) ? createCache(values) : null,
      valuesLength = values.length;

  if (cache) {
    indexOf = cacheIndexOf;
    isCommon = false;
    values = cache;
  }
  outer:
  while (++index < length) {
    var value = array[index];

    if (isCommon && value === value) {
      var valuesIndex = valuesLength;
      while (valuesIndex--) {
        if (values[valuesIndex] === value) {
          continue outer;
        }
      }
      result.push(value);
    }
    else if (indexOf(values, value, 0) < 0) {
      result.push(value);
    }
  }
  return result;
}

module.exports = baseDifference;

},{"./baseIndexOf":25,"./cacheIndexOf":39,"./createCache":43}],16:[function(require,module,exports){
/**
 * The base implementation of `_.find`, `_.findLast`, `_.findKey`, and `_.findLastKey`,
 * without support for callback shorthands and `this` binding, which iterates
 * over `collection` using the provided `eachFunc`.
 *
 * @private
 * @param {Array|Object|string} collection The collection to search.
 * @param {Function} predicate The function invoked per iteration.
 * @param {Function} eachFunc The function to iterate over `collection`.
 * @param {boolean} [retKey] Specify returning the key of the found element
 *  instead of the element itself.
 * @returns {*} Returns the found element or its key, else `undefined`.
 */
function baseFind(collection, predicate, eachFunc, retKey) {
  var result;
  eachFunc(collection, function(value, key, collection) {
    if (predicate(value, key, collection)) {
      result = retKey ? key : value;
      return false;
    }
  });
  return result;
}

module.exports = baseFind;

},{}],17:[function(require,module,exports){
var arrayPush = require('./arrayPush'),
    isArguments = require('../lang/isArguments'),
    isArray = require('../lang/isArray'),
    isArrayLike = require('./isArrayLike'),
    isObjectLike = require('./isObjectLike');

/**
 * The base implementation of `_.flatten` with added support for restricting
 * flattening and specifying the start index.
 *
 * @private
 * @param {Array} array The array to flatten.
 * @param {boolean} [isDeep] Specify a deep flatten.
 * @param {boolean} [isStrict] Restrict flattening to arrays-like objects.
 * @param {Array} [result=[]] The initial result value.
 * @returns {Array} Returns the new flattened array.
 */
function baseFlatten(array, isDeep, isStrict, result) {
  result || (result = []);

  var index = -1,
      length = array.length;

  while (++index < length) {
    var value = array[index];
    if (isObjectLike(value) && isArrayLike(value) &&
        (isStrict || isArray(value) || isArguments(value))) {
      if (isDeep) {
        // Recursively flatten arrays (susceptible to call stack limits).
        baseFlatten(value, isDeep, isStrict, result);
      } else {
        arrayPush(result, value);
      }
    } else if (!isStrict) {
      result[result.length] = value;
    }
  }
  return result;
}

module.exports = baseFlatten;

},{"../lang/isArguments":69,"../lang/isArray":70,"./arrayPush":7,"./isArrayLike":56,"./isObjectLike":61}],18:[function(require,module,exports){
var createBaseFor = require('./createBaseFor');

/**
 * The base implementation of `baseForIn` and `baseForOwn` which iterates
 * over `object` properties returned by `keysFunc` invoking `iteratee` for
 * each property. Iteratee functions may exit iteration early by explicitly
 * returning `false`.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @returns {Object} Returns `object`.
 */
var baseFor = createBaseFor();

module.exports = baseFor;

},{"./createBaseFor":42}],19:[function(require,module,exports){
var baseFor = require('./baseFor'),
    keysIn = require('../object/keysIn');

/**
 * The base implementation of `_.forIn` without support for callback
 * shorthands and `this` binding.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Object} Returns `object`.
 */
function baseForIn(object, iteratee) {
  return baseFor(object, iteratee, keysIn);
}

module.exports = baseForIn;

},{"../object/keysIn":94,"./baseFor":18}],20:[function(require,module,exports){
var baseFor = require('./baseFor'),
    keys = require('../object/keys');

/**
 * The base implementation of `_.forOwn` without support for callback
 * shorthands and `this` binding.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Object} Returns `object`.
 */
function baseForOwn(object, iteratee) {
  return baseFor(object, iteratee, keys);
}

module.exports = baseForOwn;

},{"../object/keys":93,"./baseFor":18}],21:[function(require,module,exports){
var baseForRight = require('./baseForRight'),
    keys = require('../object/keys');

/**
 * The base implementation of `_.forOwnRight` without support for callback
 * shorthands and `this` binding.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Object} Returns `object`.
 */
function baseForOwnRight(object, iteratee) {
  return baseForRight(object, iteratee, keys);
}

module.exports = baseForOwnRight;

},{"../object/keys":93,"./baseForRight":22}],22:[function(require,module,exports){
var createBaseFor = require('./createBaseFor');

/**
 * This function is like `baseFor` except that it iterates over properties
 * in the opposite order.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @returns {Object} Returns `object`.
 */
var baseForRight = createBaseFor(true);

module.exports = baseForRight;

},{"./createBaseFor":42}],23:[function(require,module,exports){
var isFunction = require('../lang/isFunction');

/**
 * The base implementation of `_.functions` which creates an array of
 * `object` function property names filtered from those provided.
 *
 * @private
 * @param {Object} object The object to inspect.
 * @param {Array} props The property names to filter.
 * @returns {Array} Returns the new array of filtered property names.
 */
function baseFunctions(object, props) {
  var index = -1,
      length = props.length,
      resIndex = -1,
      result = [];

  while (++index < length) {
    var key = props[index];
    if (isFunction(object[key])) {
      result[++resIndex] = key;
    }
  }
  return result;
}

module.exports = baseFunctions;

},{"../lang/isFunction":71}],24:[function(require,module,exports){
var toObject = require('./toObject');

/**
 * The base implementation of `get` without support for string paths
 * and default values.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array} path The path of the property to get.
 * @param {string} [pathKey] The key representation of path.
 * @returns {*} Returns the resolved value.
 */
function baseGet(object, path, pathKey) {
  if (object == null) {
    return;
  }
  if (pathKey !== undefined && pathKey in toObject(object)) {
    path = [pathKey];
  }
  var index = 0,
      length = path.length;

  while (object != null && index < length) {
    object = object[path[index++]];
  }
  return (index && index == length) ? object : undefined;
}

module.exports = baseGet;

},{"./toObject":67}],25:[function(require,module,exports){
var indexOfNaN = require('./indexOfNaN');

/**
 * The base implementation of `_.indexOf` without support for binary searches.
 *
 * @private
 * @param {Array} array The array to search.
 * @param {*} value The value to search for.
 * @param {number} fromIndex The index to search from.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function baseIndexOf(array, value, fromIndex) {
  if (value !== value) {
    return indexOfNaN(array, fromIndex);
  }
  var index = fromIndex - 1,
      length = array.length;

  while (++index < length) {
    if (array[index] === value) {
      return index;
    }
  }
  return -1;
}

module.exports = baseIndexOf;

},{"./indexOfNaN":55}],26:[function(require,module,exports){
var baseIsEqualDeep = require('./baseIsEqualDeep'),
    isObject = require('../lang/isObject'),
    isObjectLike = require('./isObjectLike');

/**
 * The base implementation of `_.isEqual` without support for `this` binding
 * `customizer` functions.
 *
 * @private
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @param {Function} [customizer] The function to customize comparing values.
 * @param {boolean} [isLoose] Specify performing partial comparisons.
 * @param {Array} [stackA] Tracks traversed `value` objects.
 * @param {Array} [stackB] Tracks traversed `other` objects.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 */
function baseIsEqual(value, other, customizer, isLoose, stackA, stackB) {
  if (value === other) {
    return true;
  }
  if (value == null || other == null || (!isObject(value) && !isObjectLike(other))) {
    return value !== value && other !== other;
  }
  return baseIsEqualDeep(value, other, baseIsEqual, customizer, isLoose, stackA, stackB);
}

module.exports = baseIsEqual;

},{"../lang/isObject":73,"./baseIsEqualDeep":27,"./isObjectLike":61}],27:[function(require,module,exports){
var equalArrays = require('./equalArrays'),
    equalByTag = require('./equalByTag'),
    equalObjects = require('./equalObjects'),
    isArray = require('../lang/isArray'),
    isTypedArray = require('../lang/isTypedArray');

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    objectTag = '[object Object]';

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
 * of values.
 */
var objToString = objectProto.toString;

/**
 * A specialized version of `baseIsEqual` for arrays and objects which performs
 * deep comparisons and tracks traversed objects enabling objects with circular
 * references to be compared.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Function} [customizer] The function to customize comparing objects.
 * @param {boolean} [isLoose] Specify performing partial comparisons.
 * @param {Array} [stackA=[]] Tracks traversed `value` objects.
 * @param {Array} [stackB=[]] Tracks traversed `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function baseIsEqualDeep(object, other, equalFunc, customizer, isLoose, stackA, stackB) {
  var objIsArr = isArray(object),
      othIsArr = isArray(other),
      objTag = arrayTag,
      othTag = arrayTag;

  if (!objIsArr) {
    objTag = objToString.call(object);
    if (objTag == argsTag) {
      objTag = objectTag;
    } else if (objTag != objectTag) {
      objIsArr = isTypedArray(object);
    }
  }
  if (!othIsArr) {
    othTag = objToString.call(other);
    if (othTag == argsTag) {
      othTag = objectTag;
    } else if (othTag != objectTag) {
      othIsArr = isTypedArray(other);
    }
  }
  var objIsObj = objTag == objectTag,
      othIsObj = othTag == objectTag,
      isSameTag = objTag == othTag;

  if (isSameTag && !(objIsArr || objIsObj)) {
    return equalByTag(object, other, objTag);
  }
  if (!isLoose) {
    var objIsWrapped = objIsObj && hasOwnProperty.call(object, '__wrapped__'),
        othIsWrapped = othIsObj && hasOwnProperty.call(other, '__wrapped__');

    if (objIsWrapped || othIsWrapped) {
      return equalFunc(objIsWrapped ? object.value() : object, othIsWrapped ? other.value() : other, customizer, isLoose, stackA, stackB);
    }
  }
  if (!isSameTag) {
    return false;
  }
  // Assume cyclic values are equal.
  // For more information on detecting circular references see https://es5.github.io/#JO.
  stackA || (stackA = []);
  stackB || (stackB = []);

  var length = stackA.length;
  while (length--) {
    if (stackA[length] == object) {
      return stackB[length] == other;
    }
  }
  // Add `object` and `other` to the stack of traversed objects.
  stackA.push(object);
  stackB.push(other);

  var result = (objIsArr ? equalArrays : equalObjects)(object, other, equalFunc, customizer, isLoose, stackA, stackB);

  stackA.pop();
  stackB.pop();

  return result;
}

module.exports = baseIsEqualDeep;

},{"../lang/isArray":70,"../lang/isTypedArray":75,"./equalArrays":49,"./equalByTag":50,"./equalObjects":51}],28:[function(require,module,exports){
var baseIsEqual = require('./baseIsEqual'),
    toObject = require('./toObject');

/**
 * The base implementation of `_.isMatch` without support for callback
 * shorthands and `this` binding.
 *
 * @private
 * @param {Object} object The object to inspect.
 * @param {Array} matchData The propery names, values, and compare flags to match.
 * @param {Function} [customizer] The function to customize comparing objects.
 * @returns {boolean} Returns `true` if `object` is a match, else `false`.
 */
function baseIsMatch(object, matchData, customizer) {
  var index = matchData.length,
      length = index,
      noCustomizer = !customizer;

  if (object == null) {
    return !length;
  }
  object = toObject(object);
  while (index--) {
    var data = matchData[index];
    if ((noCustomizer && data[2])
          ? data[1] !== object[data[0]]
          : !(data[0] in object)
        ) {
      return false;
    }
  }
  while (++index < length) {
    data = matchData[index];
    var key = data[0],
        objValue = object[key],
        srcValue = data[1];

    if (noCustomizer && data[2]) {
      if (objValue === undefined && !(key in object)) {
        return false;
      }
    } else {
      var result = customizer ? customizer(objValue, srcValue, key) : undefined;
      if (!(result === undefined ? baseIsEqual(srcValue, objValue, customizer, true) : result)) {
        return false;
      }
    }
  }
  return true;
}

module.exports = baseIsMatch;

},{"./baseIsEqual":26,"./toObject":67}],29:[function(require,module,exports){
var baseIsMatch = require('./baseIsMatch'),
    getMatchData = require('./getMatchData'),
    toObject = require('./toObject');

/**
 * The base implementation of `_.matches` which does not clone `source`.
 *
 * @private
 * @param {Object} source The object of property values to match.
 * @returns {Function} Returns the new function.
 */
function baseMatches(source) {
  var matchData = getMatchData(source);
  if (matchData.length == 1 && matchData[0][2]) {
    var key = matchData[0][0],
        value = matchData[0][1];

    return function(object) {
      if (object == null) {
        return false;
      }
      return object[key] === value && (value !== undefined || (key in toObject(object)));
    };
  }
  return function(object) {
    return baseIsMatch(object, matchData);
  };
}

module.exports = baseMatches;

},{"./baseIsMatch":28,"./getMatchData":53,"./toObject":67}],30:[function(require,module,exports){
var baseGet = require('./baseGet'),
    baseIsEqual = require('./baseIsEqual'),
    baseSlice = require('./baseSlice'),
    isArray = require('../lang/isArray'),
    isKey = require('./isKey'),
    isStrictComparable = require('./isStrictComparable'),
    last = require('../array/last'),
    toObject = require('./toObject'),
    toPath = require('./toPath');

/**
 * The base implementation of `_.matchesProperty` which does not clone `srcValue`.
 *
 * @private
 * @param {string} path The path of the property to get.
 * @param {*} srcValue The value to compare.
 * @returns {Function} Returns the new function.
 */
function baseMatchesProperty(path, srcValue) {
  var isArr = isArray(path),
      isCommon = isKey(path) && isStrictComparable(srcValue),
      pathKey = (path + '');

  path = toPath(path);
  return function(object) {
    if (object == null) {
      return false;
    }
    var key = pathKey;
    object = toObject(object);
    if ((isArr || !isCommon) && !(key in object)) {
      object = path.length == 1 ? object : baseGet(object, baseSlice(path, 0, -1));
      if (object == null) {
        return false;
      }
      key = last(path);
      object = toObject(object);
    }
    return object[key] === srcValue
      ? (srcValue !== undefined || (key in object))
      : baseIsEqual(srcValue, object[key], undefined, true);
  };
}

module.exports = baseMatchesProperty;

},{"../array/last":1,"../lang/isArray":70,"./baseGet":24,"./baseIsEqual":26,"./baseSlice":35,"./isKey":59,"./isStrictComparable":62,"./toObject":67,"./toPath":68}],31:[function(require,module,exports){
var arrayEach = require('./arrayEach'),
    baseMergeDeep = require('./baseMergeDeep'),
    isArray = require('../lang/isArray'),
    isArrayLike = require('./isArrayLike'),
    isObject = require('../lang/isObject'),
    isObjectLike = require('./isObjectLike'),
    isTypedArray = require('../lang/isTypedArray'),
    keys = require('../object/keys');

/**
 * The base implementation of `_.merge` without support for argument juggling,
 * multiple sources, and `this` binding `customizer` functions.
 *
 * @private
 * @param {Object} object The destination object.
 * @param {Object} source The source object.
 * @param {Function} [customizer] The function to customize merged values.
 * @param {Array} [stackA=[]] Tracks traversed source objects.
 * @param {Array} [stackB=[]] Associates values with source counterparts.
 * @returns {Object} Returns `object`.
 */
function baseMerge(object, source, customizer, stackA, stackB) {
  if (!isObject(object)) {
    return object;
  }
  var isSrcArr = isArrayLike(source) && (isArray(source) || isTypedArray(source)),
      props = isSrcArr ? undefined : keys(source);

  arrayEach(props || source, function(srcValue, key) {
    if (props) {
      key = srcValue;
      srcValue = source[key];
    }
    if (isObjectLike(srcValue)) {
      stackA || (stackA = []);
      stackB || (stackB = []);
      baseMergeDeep(object, source, key, baseMerge, customizer, stackA, stackB);
    }
    else {
      var value = object[key],
          result = customizer ? customizer(value, srcValue, key, object, source) : undefined,
          isCommon = result === undefined;

      if (isCommon) {
        result = srcValue;
      }
      if ((result !== undefined || (isSrcArr && !(key in object))) &&
          (isCommon || (result === result ? (result !== value) : (value === value)))) {
        object[key] = result;
      }
    }
  });
  return object;
}

module.exports = baseMerge;

},{"../lang/isArray":70,"../lang/isObject":73,"../lang/isTypedArray":75,"../object/keys":93,"./arrayEach":5,"./baseMergeDeep":32,"./isArrayLike":56,"./isObjectLike":61}],32:[function(require,module,exports){
var arrayCopy = require('./arrayCopy'),
    isArguments = require('../lang/isArguments'),
    isArray = require('../lang/isArray'),
    isArrayLike = require('./isArrayLike'),
    isPlainObject = require('../lang/isPlainObject'),
    isTypedArray = require('../lang/isTypedArray'),
    toPlainObject = require('../lang/toPlainObject');

/**
 * A specialized version of `baseMerge` for arrays and objects which performs
 * deep merges and tracks traversed objects enabling objects with circular
 * references to be merged.
 *
 * @private
 * @param {Object} object The destination object.
 * @param {Object} source The source object.
 * @param {string} key The key of the value to merge.
 * @param {Function} mergeFunc The function to merge values.
 * @param {Function} [customizer] The function to customize merged values.
 * @param {Array} [stackA=[]] Tracks traversed source objects.
 * @param {Array} [stackB=[]] Associates values with source counterparts.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function baseMergeDeep(object, source, key, mergeFunc, customizer, stackA, stackB) {
  var length = stackA.length,
      srcValue = source[key];

  while (length--) {
    if (stackA[length] == srcValue) {
      object[key] = stackB[length];
      return;
    }
  }
  var value = object[key],
      result = customizer ? customizer(value, srcValue, key, object, source) : undefined,
      isCommon = result === undefined;

  if (isCommon) {
    result = srcValue;
    if (isArrayLike(srcValue) && (isArray(srcValue) || isTypedArray(srcValue))) {
      result = isArray(value)
        ? value
        : (isArrayLike(value) ? arrayCopy(value) : []);
    }
    else if (isPlainObject(srcValue) || isArguments(srcValue)) {
      result = isArguments(value)
        ? toPlainObject(value)
        : (isPlainObject(value) ? value : {});
    }
    else {
      isCommon = false;
    }
  }
  // Add the source value to the stack of traversed objects and associate
  // it with its merged value.
  stackA.push(srcValue);
  stackB.push(result);

  if (isCommon) {
    // Recursively merge objects and arrays (susceptible to call stack limits).
    object[key] = mergeFunc(result, srcValue, customizer, stackA, stackB);
  } else if (result === result ? (result !== value) : (value === value)) {
    object[key] = result;
  }
}

module.exports = baseMergeDeep;

},{"../lang/isArguments":69,"../lang/isArray":70,"../lang/isPlainObject":74,"../lang/isTypedArray":75,"../lang/toPlainObject":76,"./arrayCopy":4,"./isArrayLike":56}],33:[function(require,module,exports){
/**
 * The base implementation of `_.property` without support for deep paths.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @returns {Function} Returns the new function.
 */
function baseProperty(key) {
  return function(object) {
    return object == null ? undefined : object[key];
  };
}

module.exports = baseProperty;

},{}],34:[function(require,module,exports){
var baseGet = require('./baseGet'),
    toPath = require('./toPath');

/**
 * A specialized version of `baseProperty` which supports deep paths.
 *
 * @private
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new function.
 */
function basePropertyDeep(path) {
  var pathKey = (path + '');
  path = toPath(path);
  return function(object) {
    return baseGet(object, path, pathKey);
  };
}

module.exports = basePropertyDeep;

},{"./baseGet":24,"./toPath":68}],35:[function(require,module,exports){
/**
 * The base implementation of `_.slice` without an iteratee call guard.
 *
 * @private
 * @param {Array} array The array to slice.
 * @param {number} [start=0] The start position.
 * @param {number} [end=array.length] The end position.
 * @returns {Array} Returns the slice of `array`.
 */
function baseSlice(array, start, end) {
  var index = -1,
      length = array.length;

  start = start == null ? 0 : (+start || 0);
  if (start < 0) {
    start = -start > length ? 0 : (length + start);
  }
  end = (end === undefined || end > length) ? length : (+end || 0);
  if (end < 0) {
    end += length;
  }
  length = start > end ? 0 : ((end - start) >>> 0);
  start >>>= 0;

  var result = Array(length);
  while (++index < length) {
    result[index] = array[index + start];
  }
  return result;
}

module.exports = baseSlice;

},{}],36:[function(require,module,exports){
/**
 * Converts `value` to a string if it's not one. An empty string is returned
 * for `null` or `undefined` values.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {string} Returns the string.
 */
function baseToString(value) {
  return value == null ? '' : (value + '');
}

module.exports = baseToString;

},{}],37:[function(require,module,exports){
/**
 * The base implementation of `_.values` and `_.valuesIn` which creates an
 * array of `object` property values corresponding to the property names
 * of `props`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array} props The property names to get values for.
 * @returns {Object} Returns the array of property values.
 */
function baseValues(object, props) {
  var index = -1,
      length = props.length,
      result = Array(length);

  while (++index < length) {
    result[index] = object[props[index]];
  }
  return result;
}

module.exports = baseValues;

},{}],38:[function(require,module,exports){
var identity = require('../utility/identity');

/**
 * A specialized version of `baseCallback` which only supports `this` binding
 * and specifying the number of arguments to provide to `func`.
 *
 * @private
 * @param {Function} func The function to bind.
 * @param {*} thisArg The `this` binding of `func`.
 * @param {number} [argCount] The number of arguments to provide to `func`.
 * @returns {Function} Returns the callback.
 */
function bindCallback(func, thisArg, argCount) {
  if (typeof func != 'function') {
    return identity;
  }
  if (thisArg === undefined) {
    return func;
  }
  switch (argCount) {
    case 1: return function(value) {
      return func.call(thisArg, value);
    };
    case 3: return function(value, index, collection) {
      return func.call(thisArg, value, index, collection);
    };
    case 4: return function(accumulator, value, index, collection) {
      return func.call(thisArg, accumulator, value, index, collection);
    };
    case 5: return function(value, other, key, object, source) {
      return func.call(thisArg, value, other, key, object, source);
    };
  }
  return function() {
    return func.apply(thisArg, arguments);
  };
}

module.exports = bindCallback;

},{"../utility/identity":107}],39:[function(require,module,exports){
var isObject = require('../lang/isObject');

/**
 * Checks if `value` is in `cache` mimicking the return signature of
 * `_.indexOf` by returning `0` if the value is found, else `-1`.
 *
 * @private
 * @param {Object} cache The cache to search.
 * @param {*} value The value to search for.
 * @returns {number} Returns `0` if `value` is found, else `-1`.
 */
function cacheIndexOf(cache, value) {
  var data = cache.data,
      result = (typeof value == 'string' || isObject(value)) ? data.set.has(value) : data.hash[value];

  return result ? 0 : -1;
}

module.exports = cacheIndexOf;

},{"../lang/isObject":73}],40:[function(require,module,exports){
var isObject = require('../lang/isObject');

/**
 * Adds `value` to the cache.
 *
 * @private
 * @name push
 * @memberOf SetCache
 * @param {*} value The value to cache.
 */
function cachePush(value) {
  var data = this.data;
  if (typeof value == 'string' || isObject(value)) {
    data.set.add(value);
  } else {
    data.hash[value] = true;
  }
}

module.exports = cachePush;

},{"../lang/isObject":73}],41:[function(require,module,exports){
var bindCallback = require('./bindCallback'),
    isIterateeCall = require('./isIterateeCall'),
    restParam = require('../function/restParam');

/**
 * Creates a `_.assign`, `_.defaults`, or `_.merge` function.
 *
 * @private
 * @param {Function} assigner The function to assign values.
 * @returns {Function} Returns the new assigner function.
 */
function createAssigner(assigner) {
  return restParam(function(object, sources) {
    var index = -1,
        length = object == null ? 0 : sources.length,
        customizer = length > 2 ? sources[length - 2] : undefined,
        guard = length > 2 ? sources[2] : undefined,
        thisArg = length > 1 ? sources[length - 1] : undefined;

    if (typeof customizer == 'function') {
      customizer = bindCallback(customizer, thisArg, 5);
      length -= 2;
    } else {
      customizer = typeof thisArg == 'function' ? thisArg : undefined;
      length -= (customizer ? 1 : 0);
    }
    if (guard && isIterateeCall(sources[0], sources[1], guard)) {
      customizer = length < 3 ? undefined : customizer;
      length = 1;
    }
    while (++index < length) {
      var source = sources[index];
      if (source) {
        assigner(object, source, customizer);
      }
    }
    return object;
  });
}

module.exports = createAssigner;

},{"../function/restParam":2,"./bindCallback":38,"./isIterateeCall":58}],42:[function(require,module,exports){
var toObject = require('./toObject');

/**
 * Creates a base function for `_.forIn` or `_.forInRight`.
 *
 * @private
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {Function} Returns the new base function.
 */
function createBaseFor(fromRight) {
  return function(object, iteratee, keysFunc) {
    var iterable = toObject(object),
        props = keysFunc(object),
        length = props.length,
        index = fromRight ? length : -1;

    while ((fromRight ? index-- : ++index < length)) {
      var key = props[index];
      if (iteratee(iterable[key], key, iterable) === false) {
        break;
      }
    }
    return object;
  };
}

module.exports = createBaseFor;

},{"./toObject":67}],43:[function(require,module,exports){
(function (global){
var SetCache = require('./SetCache'),
    getNative = require('./getNative');

/** Native method references. */
var Set = getNative(global, 'Set');

/* Native method references for those with the same name as other `lodash` methods. */
var nativeCreate = getNative(Object, 'create');

/**
 * Creates a `Set` cache object to optimize linear searches of large arrays.
 *
 * @private
 * @param {Array} [values] The values to cache.
 * @returns {null|Object} Returns the new cache object if `Set` is supported, else `null`.
 */
function createCache(values) {
  return (nativeCreate && Set) ? new SetCache(values) : null;
}

module.exports = createCache;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./SetCache":3,"./getNative":54}],44:[function(require,module,exports){
var restParam = require('../function/restParam');

/**
 * Creates a `_.defaults` or `_.defaultsDeep` function.
 *
 * @private
 * @param {Function} assigner The function to assign values.
 * @param {Function} customizer The function to customize assigned values.
 * @returns {Function} Returns the new defaults function.
 */
function createDefaults(assigner, customizer) {
  return restParam(function(args) {
    var object = args[0];
    if (object == null) {
      return object;
    }
    args.push(customizer);
    return assigner.apply(undefined, args);
  });
}

module.exports = createDefaults;

},{"../function/restParam":2}],45:[function(require,module,exports){
var baseCallback = require('./baseCallback'),
    baseFind = require('./baseFind');

/**
 * Creates a `_.findKey` or `_.findLastKey` function.
 *
 * @private
 * @param {Function} objectFunc The function to iterate over an object.
 * @returns {Function} Returns the new find function.
 */
function createFindKey(objectFunc) {
  return function(object, predicate, thisArg) {
    predicate = baseCallback(predicate, thisArg, 3);
    return baseFind(object, predicate, objectFunc, true);
  };
}

module.exports = createFindKey;

},{"./baseCallback":12,"./baseFind":16}],46:[function(require,module,exports){
var bindCallback = require('./bindCallback'),
    keysIn = require('../object/keysIn');

/**
 * Creates a function for `_.forIn` or `_.forInRight`.
 *
 * @private
 * @param {Function} objectFunc The function to iterate over an object.
 * @returns {Function} Returns the new each function.
 */
function createForIn(objectFunc) {
  return function(object, iteratee, thisArg) {
    if (typeof iteratee != 'function' || thisArg !== undefined) {
      iteratee = bindCallback(iteratee, thisArg, 3);
    }
    return objectFunc(object, iteratee, keysIn);
  };
}

module.exports = createForIn;

},{"../object/keysIn":94,"./bindCallback":38}],47:[function(require,module,exports){
var bindCallback = require('./bindCallback');

/**
 * Creates a function for `_.forOwn` or `_.forOwnRight`.
 *
 * @private
 * @param {Function} objectFunc The function to iterate over an object.
 * @returns {Function} Returns the new each function.
 */
function createForOwn(objectFunc) {
  return function(object, iteratee, thisArg) {
    if (typeof iteratee != 'function' || thisArg !== undefined) {
      iteratee = bindCallback(iteratee, thisArg, 3);
    }
    return objectFunc(object, iteratee);
  };
}

module.exports = createForOwn;

},{"./bindCallback":38}],48:[function(require,module,exports){
var baseCallback = require('./baseCallback'),
    baseForOwn = require('./baseForOwn');

/**
 * Creates a function for `_.mapKeys` or `_.mapValues`.
 *
 * @private
 * @param {boolean} [isMapKeys] Specify mapping keys instead of values.
 * @returns {Function} Returns the new map function.
 */
function createObjectMapper(isMapKeys) {
  return function(object, iteratee, thisArg) {
    var result = {};
    iteratee = baseCallback(iteratee, thisArg, 3);

    baseForOwn(object, function(value, key, object) {
      var mapped = iteratee(value, key, object);
      key = isMapKeys ? mapped : key;
      value = isMapKeys ? value : mapped;
      result[key] = value;
    });
    return result;
  };
}

module.exports = createObjectMapper;

},{"./baseCallback":12,"./baseForOwn":20}],49:[function(require,module,exports){
var arraySome = require('./arraySome');

/**
 * A specialized version of `baseIsEqualDeep` for arrays with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Array} array The array to compare.
 * @param {Array} other The other array to compare.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Function} [customizer] The function to customize comparing arrays.
 * @param {boolean} [isLoose] Specify performing partial comparisons.
 * @param {Array} [stackA] Tracks traversed `value` objects.
 * @param {Array} [stackB] Tracks traversed `other` objects.
 * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
 */
function equalArrays(array, other, equalFunc, customizer, isLoose, stackA, stackB) {
  var index = -1,
      arrLength = array.length,
      othLength = other.length;

  if (arrLength != othLength && !(isLoose && othLength > arrLength)) {
    return false;
  }
  // Ignore non-index properties.
  while (++index < arrLength) {
    var arrValue = array[index],
        othValue = other[index],
        result = customizer ? customizer(isLoose ? othValue : arrValue, isLoose ? arrValue : othValue, index) : undefined;

    if (result !== undefined) {
      if (result) {
        continue;
      }
      return false;
    }
    // Recursively compare arrays (susceptible to call stack limits).
    if (isLoose) {
      if (!arraySome(other, function(othValue) {
            return arrValue === othValue || equalFunc(arrValue, othValue, customizer, isLoose, stackA, stackB);
          })) {
        return false;
      }
    } else if (!(arrValue === othValue || equalFunc(arrValue, othValue, customizer, isLoose, stackA, stackB))) {
      return false;
    }
  }
  return true;
}

module.exports = equalArrays;

},{"./arraySome":8}],50:[function(require,module,exports){
/** `Object#toString` result references. */
var boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    numberTag = '[object Number]',
    regexpTag = '[object RegExp]',
    stringTag = '[object String]';

/**
 * A specialized version of `baseIsEqualDeep` for comparing objects of
 * the same `toStringTag`.
 *
 * **Note:** This function only supports comparing values with tags of
 * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {string} tag The `toStringTag` of the objects to compare.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalByTag(object, other, tag) {
  switch (tag) {
    case boolTag:
    case dateTag:
      // Coerce dates and booleans to numbers, dates to milliseconds and booleans
      // to `1` or `0` treating invalid dates coerced to `NaN` as not equal.
      return +object == +other;

    case errorTag:
      return object.name == other.name && object.message == other.message;

    case numberTag:
      // Treat `NaN` vs. `NaN` as equal.
      return (object != +object)
        ? other != +other
        : object == +other;

    case regexpTag:
    case stringTag:
      // Coerce regexes to strings and treat strings primitives and string
      // objects as equal. See https://es5.github.io/#x15.10.6.4 for more details.
      return object == (other + '');
  }
  return false;
}

module.exports = equalByTag;

},{}],51:[function(require,module,exports){
var keys = require('../object/keys');

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * A specialized version of `baseIsEqualDeep` for objects with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Function} [customizer] The function to customize comparing values.
 * @param {boolean} [isLoose] Specify performing partial comparisons.
 * @param {Array} [stackA] Tracks traversed `value` objects.
 * @param {Array} [stackB] Tracks traversed `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalObjects(object, other, equalFunc, customizer, isLoose, stackA, stackB) {
  var objProps = keys(object),
      objLength = objProps.length,
      othProps = keys(other),
      othLength = othProps.length;

  if (objLength != othLength && !isLoose) {
    return false;
  }
  var index = objLength;
  while (index--) {
    var key = objProps[index];
    if (!(isLoose ? key in other : hasOwnProperty.call(other, key))) {
      return false;
    }
  }
  var skipCtor = isLoose;
  while (++index < objLength) {
    key = objProps[index];
    var objValue = object[key],
        othValue = other[key],
        result = customizer ? customizer(isLoose ? othValue : objValue, isLoose? objValue : othValue, key) : undefined;

    // Recursively compare objects (susceptible to call stack limits).
    if (!(result === undefined ? equalFunc(objValue, othValue, customizer, isLoose, stackA, stackB) : result)) {
      return false;
    }
    skipCtor || (skipCtor = key == 'constructor');
  }
  if (!skipCtor) {
    var objCtor = object.constructor,
        othCtor = other.constructor;

    // Non `Object` object instances with different constructors are not equal.
    if (objCtor != othCtor &&
        ('constructor' in object && 'constructor' in other) &&
        !(typeof objCtor == 'function' && objCtor instanceof objCtor &&
          typeof othCtor == 'function' && othCtor instanceof othCtor)) {
      return false;
    }
  }
  return true;
}

module.exports = equalObjects;

},{"../object/keys":93}],52:[function(require,module,exports){
var baseProperty = require('./baseProperty');

/**
 * Gets the "length" property value of `object`.
 *
 * **Note:** This function is used to avoid a [JIT bug](https://bugs.webkit.org/show_bug.cgi?id=142792)
 * that affects Safari on at least iOS 8.1-8.3 ARM64.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {*} Returns the "length" value.
 */
var getLength = baseProperty('length');

module.exports = getLength;

},{"./baseProperty":33}],53:[function(require,module,exports){
var isStrictComparable = require('./isStrictComparable'),
    pairs = require('../object/pairs');

/**
 * Gets the propery names, values, and compare flags of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the match data of `object`.
 */
function getMatchData(object) {
  var result = pairs(object),
      length = result.length;

  while (length--) {
    result[length][2] = isStrictComparable(result[length][1]);
  }
  return result;
}

module.exports = getMatchData;

},{"../object/pairs":100,"./isStrictComparable":62}],54:[function(require,module,exports){
var isNative = require('../lang/isNative');

/**
 * Gets the native function at `key` of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {string} key The key of the method to get.
 * @returns {*} Returns the function if it's native, else `undefined`.
 */
function getNative(object, key) {
  var value = object == null ? undefined : object[key];
  return isNative(value) ? value : undefined;
}

module.exports = getNative;

},{"../lang/isNative":72}],55:[function(require,module,exports){
/**
 * Gets the index at which the first occurrence of `NaN` is found in `array`.
 *
 * @private
 * @param {Array} array The array to search.
 * @param {number} fromIndex The index to search from.
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {number} Returns the index of the matched `NaN`, else `-1`.
 */
function indexOfNaN(array, fromIndex, fromRight) {
  var length = array.length,
      index = fromIndex + (fromRight ? 0 : -1);

  while ((fromRight ? index-- : ++index < length)) {
    var other = array[index];
    if (other !== other) {
      return index;
    }
  }
  return -1;
}

module.exports = indexOfNaN;

},{}],56:[function(require,module,exports){
var getLength = require('./getLength'),
    isLength = require('./isLength');

/**
 * Checks if `value` is array-like.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 */
function isArrayLike(value) {
  return value != null && isLength(getLength(value));
}

module.exports = isArrayLike;

},{"./getLength":52,"./isLength":60}],57:[function(require,module,exports){
/** Used to detect unsigned integer values. */
var reIsUint = /^\d+$/;

/**
 * Used as the [maximum length](http://ecma-international.org/ecma-262/6.0/#sec-number.max_safe_integer)
 * of an array-like value.
 */
var MAX_SAFE_INTEGER = 9007199254740991;

/**
 * Checks if `value` is a valid array-like index.
 *
 * @private
 * @param {*} value The value to check.
 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */
function isIndex(value, length) {
  value = (typeof value == 'number' || reIsUint.test(value)) ? +value : -1;
  length = length == null ? MAX_SAFE_INTEGER : length;
  return value > -1 && value % 1 == 0 && value < length;
}

module.exports = isIndex;

},{}],58:[function(require,module,exports){
var isArrayLike = require('./isArrayLike'),
    isIndex = require('./isIndex'),
    isObject = require('../lang/isObject');

/**
 * Checks if the provided arguments are from an iteratee call.
 *
 * @private
 * @param {*} value The potential iteratee value argument.
 * @param {*} index The potential iteratee index or key argument.
 * @param {*} object The potential iteratee object argument.
 * @returns {boolean} Returns `true` if the arguments are from an iteratee call, else `false`.
 */
function isIterateeCall(value, index, object) {
  if (!isObject(object)) {
    return false;
  }
  var type = typeof index;
  if (type == 'number'
      ? (isArrayLike(object) && isIndex(index, object.length))
      : (type == 'string' && index in object)) {
    var other = object[index];
    return value === value ? (value === other) : (other !== other);
  }
  return false;
}

module.exports = isIterateeCall;

},{"../lang/isObject":73,"./isArrayLike":56,"./isIndex":57}],59:[function(require,module,exports){
var isArray = require('../lang/isArray'),
    toObject = require('./toObject');

/** Used to match property names within property paths. */
var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\n\\]|\\.)*?\1)\]/,
    reIsPlainProp = /^\w*$/;

/**
 * Checks if `value` is a property name and not a property path.
 *
 * @private
 * @param {*} value The value to check.
 * @param {Object} [object] The object to query keys on.
 * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
 */
function isKey(value, object) {
  var type = typeof value;
  if ((type == 'string' && reIsPlainProp.test(value)) || type == 'number') {
    return true;
  }
  if (isArray(value)) {
    return false;
  }
  var result = !reIsDeepProp.test(value);
  return result || (object != null && value in toObject(object));
}

module.exports = isKey;

},{"../lang/isArray":70,"./toObject":67}],60:[function(require,module,exports){
/**
 * Used as the [maximum length](http://ecma-international.org/ecma-262/6.0/#sec-number.max_safe_integer)
 * of an array-like value.
 */
var MAX_SAFE_INTEGER = 9007199254740991;

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This function is based on [`ToLength`](http://ecma-international.org/ecma-262/6.0/#sec-tolength).
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 */
function isLength(value) {
  return typeof value == 'number' && value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
}

module.exports = isLength;

},{}],61:[function(require,module,exports){
/**
 * Checks if `value` is object-like.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 */
function isObjectLike(value) {
  return !!value && typeof value == 'object';
}

module.exports = isObjectLike;

},{}],62:[function(require,module,exports){
var isObject = require('../lang/isObject');

/**
 * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` if suitable for strict
 *  equality comparisons, else `false`.
 */
function isStrictComparable(value) {
  return value === value && !isObject(value);
}

module.exports = isStrictComparable;

},{"../lang/isObject":73}],63:[function(require,module,exports){
var merge = require('../object/merge');

/**
 * Used by `_.defaultsDeep` to customize its `_.merge` use.
 *
 * @private
 * @param {*} objectValue The destination object property value.
 * @param {*} sourceValue The source object property value.
 * @returns {*} Returns the value to assign to the destination object.
 */
function mergeDefaults(objectValue, sourceValue) {
  return objectValue === undefined ? sourceValue : merge(objectValue, sourceValue, mergeDefaults);
}

module.exports = mergeDefaults;

},{"../object/merge":97}],64:[function(require,module,exports){
var toObject = require('./toObject');

/**
 * A specialized version of `_.pick` which picks `object` properties specified
 * by `props`.
 *
 * @private
 * @param {Object} object The source object.
 * @param {string[]} props The property names to pick.
 * @returns {Object} Returns the new object.
 */
function pickByArray(object, props) {
  object = toObject(object);

  var index = -1,
      length = props.length,
      result = {};

  while (++index < length) {
    var key = props[index];
    if (key in object) {
      result[key] = object[key];
    }
  }
  return result;
}

module.exports = pickByArray;

},{"./toObject":67}],65:[function(require,module,exports){
var baseForIn = require('./baseForIn');

/**
 * A specialized version of `_.pick` which picks `object` properties `predicate`
 * returns truthy for.
 *
 * @private
 * @param {Object} object The source object.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {Object} Returns the new object.
 */
function pickByCallback(object, predicate) {
  var result = {};
  baseForIn(object, function(value, key, object) {
    if (predicate(value, key, object)) {
      result[key] = value;
    }
  });
  return result;
}

module.exports = pickByCallback;

},{"./baseForIn":19}],66:[function(require,module,exports){
var isArguments = require('../lang/isArguments'),
    isArray = require('../lang/isArray'),
    isIndex = require('./isIndex'),
    isLength = require('./isLength'),
    keysIn = require('../object/keysIn');

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * A fallback implementation of `Object.keys` which creates an array of the
 * own enumerable property names of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function shimKeys(object) {
  var props = keysIn(object),
      propsLength = props.length,
      length = propsLength && object.length;

  var allowIndexes = !!length && isLength(length) &&
    (isArray(object) || isArguments(object));

  var index = -1,
      result = [];

  while (++index < propsLength) {
    var key = props[index];
    if ((allowIndexes && isIndex(key, length)) || hasOwnProperty.call(object, key)) {
      result.push(key);
    }
  }
  return result;
}

module.exports = shimKeys;

},{"../lang/isArguments":69,"../lang/isArray":70,"../object/keysIn":94,"./isIndex":57,"./isLength":60}],67:[function(require,module,exports){
var isObject = require('../lang/isObject');

/**
 * Converts `value` to an object if it's not one.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {Object} Returns the object.
 */
function toObject(value) {
  return isObject(value) ? value : Object(value);
}

module.exports = toObject;

},{"../lang/isObject":73}],68:[function(require,module,exports){
var baseToString = require('./baseToString'),
    isArray = require('../lang/isArray');

/** Used to match property names within property paths. */
var rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\n\\]|\\.)*?)\2)\]/g;

/** Used to match backslashes in property paths. */
var reEscapeChar = /\\(\\)?/g;

/**
 * Converts `value` to property path array if it's not one.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {Array} Returns the property path array.
 */
function toPath(value) {
  if (isArray(value)) {
    return value;
  }
  var result = [];
  baseToString(value).replace(rePropName, function(match, number, quote, string) {
    result.push(quote ? string.replace(reEscapeChar, '$1') : (number || match));
  });
  return result;
}

module.exports = toPath;

},{"../lang/isArray":70,"./baseToString":36}],69:[function(require,module,exports){
var isArrayLike = require('../internal/isArrayLike'),
    isObjectLike = require('../internal/isObjectLike');

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Native method references. */
var propertyIsEnumerable = objectProto.propertyIsEnumerable;

/**
 * Checks if `value` is classified as an `arguments` object.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */
function isArguments(value) {
  return isObjectLike(value) && isArrayLike(value) &&
    hasOwnProperty.call(value, 'callee') && !propertyIsEnumerable.call(value, 'callee');
}

module.exports = isArguments;

},{"../internal/isArrayLike":56,"../internal/isObjectLike":61}],70:[function(require,module,exports){
var getNative = require('../internal/getNative'),
    isLength = require('../internal/isLength'),
    isObjectLike = require('../internal/isObjectLike');

/** `Object#toString` result references. */
var arrayTag = '[object Array]';

/** Used for native method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
 * of values.
 */
var objToString = objectProto.toString;

/* Native method references for those with the same name as other `lodash` methods. */
var nativeIsArray = getNative(Array, 'isArray');

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(function() { return arguments; }());
 * // => false
 */
var isArray = nativeIsArray || function(value) {
  return isObjectLike(value) && isLength(value.length) && objToString.call(value) == arrayTag;
};

module.exports = isArray;

},{"../internal/getNative":54,"../internal/isLength":60,"../internal/isObjectLike":61}],71:[function(require,module,exports){
var isObject = require('./isObject');

/** `Object#toString` result references. */
var funcTag = '[object Function]';

/** Used for native method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
 * of values.
 */
var objToString = objectProto.toString;

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in older versions of Chrome and Safari which return 'function' for regexes
  // and Safari 8 which returns 'object' for typed array constructors.
  return isObject(value) && objToString.call(value) == funcTag;
}

module.exports = isFunction;

},{"./isObject":73}],72:[function(require,module,exports){
var isFunction = require('./isFunction'),
    isObjectLike = require('../internal/isObjectLike');

/** Used to detect host constructors (Safari > 5). */
var reIsHostCtor = /^\[object .+?Constructor\]$/;

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to resolve the decompiled source of functions. */
var fnToString = Function.prototype.toString;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Used to detect if a method is native. */
var reIsNative = RegExp('^' +
  fnToString.call(hasOwnProperty).replace(/[\\^$.*+?()[\]{}|]/g, '\\$&')
  .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
);

/**
 * Checks if `value` is a native function.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a native function, else `false`.
 * @example
 *
 * _.isNative(Array.prototype.push);
 * // => true
 *
 * _.isNative(_);
 * // => false
 */
function isNative(value) {
  if (value == null) {
    return false;
  }
  if (isFunction(value)) {
    return reIsNative.test(fnToString.call(value));
  }
  return isObjectLike(value) && reIsHostCtor.test(value);
}

module.exports = isNative;

},{"../internal/isObjectLike":61,"./isFunction":71}],73:[function(require,module,exports){
/**
 * Checks if `value` is the [language type](https://es5.github.io/#x8) of `Object`.
 * (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(1);
 * // => false
 */
function isObject(value) {
  // Avoid a V8 JIT bug in Chrome 19-20.
  // See https://code.google.com/p/v8/issues/detail?id=2291 for more details.
  var type = typeof value;
  return !!value && (type == 'object' || type == 'function');
}

module.exports = isObject;

},{}],74:[function(require,module,exports){
var baseForIn = require('../internal/baseForIn'),
    isArguments = require('./isArguments'),
    isObjectLike = require('../internal/isObjectLike');

/** `Object#toString` result references. */
var objectTag = '[object Object]';

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
 * of values.
 */
var objToString = objectProto.toString;

/**
 * Checks if `value` is a plain object, that is, an object created by the
 * `Object` constructor or one with a `[[Prototype]]` of `null`.
 *
 * **Note:** This method assumes objects created by the `Object` constructor
 * have no inherited enumerable properties.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 * }
 *
 * _.isPlainObject(new Foo);
 * // => false
 *
 * _.isPlainObject([1, 2, 3]);
 * // => false
 *
 * _.isPlainObject({ 'x': 0, 'y': 0 });
 * // => true
 *
 * _.isPlainObject(Object.create(null));
 * // => true
 */
function isPlainObject(value) {
  var Ctor;

  // Exit early for non `Object` objects.
  if (!(isObjectLike(value) && objToString.call(value) == objectTag && !isArguments(value)) ||
      (!hasOwnProperty.call(value, 'constructor') && (Ctor = value.constructor, typeof Ctor == 'function' && !(Ctor instanceof Ctor)))) {
    return false;
  }
  // IE < 9 iterates inherited properties before own properties. If the first
  // iterated property is an object's own property then there are no inherited
  // enumerable properties.
  var result;
  // In most environments an object's own properties are iterated before
  // its inherited properties. If the last iterated property is an object's
  // own property then there are no inherited enumerable properties.
  baseForIn(value, function(subValue, key) {
    result = key;
  });
  return result === undefined || hasOwnProperty.call(value, result);
}

module.exports = isPlainObject;

},{"../internal/baseForIn":19,"../internal/isObjectLike":61,"./isArguments":69}],75:[function(require,module,exports){
var isLength = require('../internal/isLength'),
    isObjectLike = require('../internal/isObjectLike');

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    funcTag = '[object Function]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    objectTag = '[object Object]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    weakMapTag = '[object WeakMap]';

var arrayBufferTag = '[object ArrayBuffer]',
    float32Tag = '[object Float32Array]',
    float64Tag = '[object Float64Array]',
    int8Tag = '[object Int8Array]',
    int16Tag = '[object Int16Array]',
    int32Tag = '[object Int32Array]',
    uint8Tag = '[object Uint8Array]',
    uint8ClampedTag = '[object Uint8ClampedArray]',
    uint16Tag = '[object Uint16Array]',
    uint32Tag = '[object Uint32Array]';

/** Used to identify `toStringTag` values of typed arrays. */
var typedArrayTags = {};
typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
typedArrayTags[uint32Tag] = true;
typedArrayTags[argsTag] = typedArrayTags[arrayTag] =
typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
typedArrayTags[dateTag] = typedArrayTags[errorTag] =
typedArrayTags[funcTag] = typedArrayTags[mapTag] =
typedArrayTags[numberTag] = typedArrayTags[objectTag] =
typedArrayTags[regexpTag] = typedArrayTags[setTag] =
typedArrayTags[stringTag] = typedArrayTags[weakMapTag] = false;

/** Used for native method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
 * of values.
 */
var objToString = objectProto.toString;

/**
 * Checks if `value` is classified as a typed array.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
 * @example
 *
 * _.isTypedArray(new Uint8Array);
 * // => true
 *
 * _.isTypedArray([]);
 * // => false
 */
function isTypedArray(value) {
  return isObjectLike(value) && isLength(value.length) && !!typedArrayTags[objToString.call(value)];
}

module.exports = isTypedArray;

},{"../internal/isLength":60,"../internal/isObjectLike":61}],76:[function(require,module,exports){
var baseCopy = require('../internal/baseCopy'),
    keysIn = require('../object/keysIn');

/**
 * Converts `value` to a plain object flattening inherited enumerable
 * properties of `value` to own properties of the plain object.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {Object} Returns the converted plain object.
 * @example
 *
 * function Foo() {
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.assign({ 'a': 1 }, new Foo);
 * // => { 'a': 1, 'b': 2 }
 *
 * _.assign({ 'a': 1 }, _.toPlainObject(new Foo));
 * // => { 'a': 1, 'b': 2, 'c': 3 }
 */
function toPlainObject(value) {
  return baseCopy(value, keysIn(value));
}

module.exports = toPlainObject;

},{"../internal/baseCopy":13,"../object/keysIn":94}],77:[function(require,module,exports){
module.exports = {
  'assign': require('./object/assign'),
  'create': require('./object/create'),
  'defaults': require('./object/defaults'),
  'defaultsDeep': require('./object/defaultsDeep'),
  'extend': require('./object/extend'),
  'findKey': require('./object/findKey'),
  'findLastKey': require('./object/findLastKey'),
  'forIn': require('./object/forIn'),
  'forInRight': require('./object/forInRight'),
  'forOwn': require('./object/forOwn'),
  'forOwnRight': require('./object/forOwnRight'),
  'functions': require('./object/functions'),
  'get': require('./object/get'),
  'has': require('./object/has'),
  'invert': require('./object/invert'),
  'keys': require('./object/keys'),
  'keysIn': require('./object/keysIn'),
  'mapKeys': require('./object/mapKeys'),
  'mapValues': require('./object/mapValues'),
  'merge': require('./object/merge'),
  'methods': require('./object/methods'),
  'omit': require('./object/omit'),
  'pairs': require('./object/pairs'),
  'pick': require('./object/pick'),
  'result': require('./object/result'),
  'set': require('./object/set'),
  'transform': require('./object/transform'),
  'values': require('./object/values'),
  'valuesIn': require('./object/valuesIn')
};

},{"./object/assign":78,"./object/create":79,"./object/defaults":80,"./object/defaultsDeep":81,"./object/extend":82,"./object/findKey":83,"./object/findLastKey":84,"./object/forIn":85,"./object/forInRight":86,"./object/forOwn":87,"./object/forOwnRight":88,"./object/functions":89,"./object/get":90,"./object/has":91,"./object/invert":92,"./object/keys":93,"./object/keysIn":94,"./object/mapKeys":95,"./object/mapValues":96,"./object/merge":97,"./object/methods":98,"./object/omit":99,"./object/pairs":100,"./object/pick":101,"./object/result":102,"./object/set":103,"./object/transform":104,"./object/values":105,"./object/valuesIn":106}],78:[function(require,module,exports){
var assignWith = require('../internal/assignWith'),
    baseAssign = require('../internal/baseAssign'),
    createAssigner = require('../internal/createAssigner');

/**
 * Assigns own enumerable properties of source object(s) to the destination
 * object. Subsequent sources overwrite property assignments of previous sources.
 * If `customizer` is provided it's invoked to produce the assigned values.
 * The `customizer` is bound to `thisArg` and invoked with five arguments:
 * (objectValue, sourceValue, key, object, source).
 *
 * **Note:** This method mutates `object` and is based on
 * [`Object.assign`](http://ecma-international.org/ecma-262/6.0/#sec-object.assign).
 *
 * @static
 * @memberOf _
 * @alias extend
 * @category Object
 * @param {Object} object The destination object.
 * @param {...Object} [sources] The source objects.
 * @param {Function} [customizer] The function to customize assigned values.
 * @param {*} [thisArg] The `this` binding of `customizer`.
 * @returns {Object} Returns `object`.
 * @example
 *
 * _.assign({ 'user': 'barney' }, { 'age': 40 }, { 'user': 'fred' });
 * // => { 'user': 'fred', 'age': 40 }
 *
 * // using a customizer callback
 * var defaults = _.partialRight(_.assign, function(value, other) {
 *   return _.isUndefined(value) ? other : value;
 * });
 *
 * defaults({ 'user': 'barney' }, { 'age': 36 }, { 'user': 'fred' });
 * // => { 'user': 'barney', 'age': 36 }
 */
var assign = createAssigner(function(object, source, customizer) {
  return customizer
    ? assignWith(object, source, customizer)
    : baseAssign(object, source);
});

module.exports = assign;

},{"../internal/assignWith":10,"../internal/baseAssign":11,"../internal/createAssigner":41}],79:[function(require,module,exports){
var baseAssign = require('../internal/baseAssign'),
    baseCreate = require('../internal/baseCreate'),
    isIterateeCall = require('../internal/isIterateeCall');

/**
 * Creates an object that inherits from the given `prototype` object. If a
 * `properties` object is provided its own enumerable properties are assigned
 * to the created object.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} prototype The object to inherit from.
 * @param {Object} [properties] The properties to assign to the object.
 * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
 * @returns {Object} Returns the new object.
 * @example
 *
 * function Shape() {
 *   this.x = 0;
 *   this.y = 0;
 * }
 *
 * function Circle() {
 *   Shape.call(this);
 * }
 *
 * Circle.prototype = _.create(Shape.prototype, {
 *   'constructor': Circle
 * });
 *
 * var circle = new Circle;
 * circle instanceof Circle;
 * // => true
 *
 * circle instanceof Shape;
 * // => true
 */
function create(prototype, properties, guard) {
  var result = baseCreate(prototype);
  if (guard && isIterateeCall(prototype, properties, guard)) {
    properties = undefined;
  }
  return properties ? baseAssign(result, properties) : result;
}

module.exports = create;

},{"../internal/baseAssign":11,"../internal/baseCreate":14,"../internal/isIterateeCall":58}],80:[function(require,module,exports){
var assign = require('./assign'),
    assignDefaults = require('../internal/assignDefaults'),
    createDefaults = require('../internal/createDefaults');

/**
 * Assigns own enumerable properties of source object(s) to the destination
 * object for all destination properties that resolve to `undefined`. Once a
 * property is set, additional values of the same property are ignored.
 *
 * **Note:** This method mutates `object`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The destination object.
 * @param {...Object} [sources] The source objects.
 * @returns {Object} Returns `object`.
 * @example
 *
 * _.defaults({ 'user': 'barney' }, { 'age': 36 }, { 'user': 'fred' });
 * // => { 'user': 'barney', 'age': 36 }
 */
var defaults = createDefaults(assign, assignDefaults);

module.exports = defaults;

},{"../internal/assignDefaults":9,"../internal/createDefaults":44,"./assign":78}],81:[function(require,module,exports){
var createDefaults = require('../internal/createDefaults'),
    merge = require('./merge'),
    mergeDefaults = require('../internal/mergeDefaults');

/**
 * This method is like `_.defaults` except that it recursively assigns
 * default properties.
 *
 * **Note:** This method mutates `object`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The destination object.
 * @param {...Object} [sources] The source objects.
 * @returns {Object} Returns `object`.
 * @example
 *
 * _.defaultsDeep({ 'user': { 'name': 'barney' } }, { 'user': { 'name': 'fred', 'age': 36 } });
 * // => { 'user': { 'name': 'barney', 'age': 36 } }
 *
 */
var defaultsDeep = createDefaults(merge, mergeDefaults);

module.exports = defaultsDeep;

},{"../internal/createDefaults":44,"../internal/mergeDefaults":63,"./merge":97}],82:[function(require,module,exports){
module.exports = require('./assign');

},{"./assign":78}],83:[function(require,module,exports){
var baseForOwn = require('../internal/baseForOwn'),
    createFindKey = require('../internal/createFindKey');

/**
 * This method is like `_.find` except that it returns the key of the first
 * element `predicate` returns truthy for instead of the element itself.
 *
 * If a property name is provided for `predicate` the created `_.property`
 * style callback returns the property value of the given element.
 *
 * If a value is also provided for `thisArg` the created `_.matchesProperty`
 * style callback returns `true` for elements that have a matching property
 * value, else `false`.
 *
 * If an object is provided for `predicate` the created `_.matches` style
 * callback returns `true` for elements that have the properties of the given
 * object, else `false`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to search.
 * @param {Function|Object|string} [predicate=_.identity] The function invoked
 *  per iteration.
 * @param {*} [thisArg] The `this` binding of `predicate`.
 * @returns {string|undefined} Returns the key of the matched element, else `undefined`.
 * @example
 *
 * var users = {
 *   'barney':  { 'age': 36, 'active': true },
 *   'fred':    { 'age': 40, 'active': false },
 *   'pebbles': { 'age': 1,  'active': true }
 * };
 *
 * _.findKey(users, function(chr) {
 *   return chr.age < 40;
 * });
 * // => 'barney' (iteration order is not guaranteed)
 *
 * // using the `_.matches` callback shorthand
 * _.findKey(users, { 'age': 1, 'active': true });
 * // => 'pebbles'
 *
 * // using the `_.matchesProperty` callback shorthand
 * _.findKey(users, 'active', false);
 * // => 'fred'
 *
 * // using the `_.property` callback shorthand
 * _.findKey(users, 'active');
 * // => 'barney'
 */
var findKey = createFindKey(baseForOwn);

module.exports = findKey;

},{"../internal/baseForOwn":20,"../internal/createFindKey":45}],84:[function(require,module,exports){
var baseForOwnRight = require('../internal/baseForOwnRight'),
    createFindKey = require('../internal/createFindKey');

/**
 * This method is like `_.findKey` except that it iterates over elements of
 * a collection in the opposite order.
 *
 * If a property name is provided for `predicate` the created `_.property`
 * style callback returns the property value of the given element.
 *
 * If a value is also provided for `thisArg` the created `_.matchesProperty`
 * style callback returns `true` for elements that have a matching property
 * value, else `false`.
 *
 * If an object is provided for `predicate` the created `_.matches` style
 * callback returns `true` for elements that have the properties of the given
 * object, else `false`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to search.
 * @param {Function|Object|string} [predicate=_.identity] The function invoked
 *  per iteration.
 * @param {*} [thisArg] The `this` binding of `predicate`.
 * @returns {string|undefined} Returns the key of the matched element, else `undefined`.
 * @example
 *
 * var users = {
 *   'barney':  { 'age': 36, 'active': true },
 *   'fred':    { 'age': 40, 'active': false },
 *   'pebbles': { 'age': 1,  'active': true }
 * };
 *
 * _.findLastKey(users, function(chr) {
 *   return chr.age < 40;
 * });
 * // => returns `pebbles` assuming `_.findKey` returns `barney`
 *
 * // using the `_.matches` callback shorthand
 * _.findLastKey(users, { 'age': 36, 'active': true });
 * // => 'barney'
 *
 * // using the `_.matchesProperty` callback shorthand
 * _.findLastKey(users, 'active', false);
 * // => 'fred'
 *
 * // using the `_.property` callback shorthand
 * _.findLastKey(users, 'active');
 * // => 'pebbles'
 */
var findLastKey = createFindKey(baseForOwnRight);

module.exports = findLastKey;

},{"../internal/baseForOwnRight":21,"../internal/createFindKey":45}],85:[function(require,module,exports){
var baseFor = require('../internal/baseFor'),
    createForIn = require('../internal/createForIn');

/**
 * Iterates over own and inherited enumerable properties of an object invoking
 * `iteratee` for each property. The `iteratee` is bound to `thisArg` and invoked
 * with three arguments: (value, key, object). Iteratee functions may exit
 * iteration early by explicitly returning `false`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to iterate over.
 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
 * @param {*} [thisArg] The `this` binding of `iteratee`.
 * @returns {Object} Returns `object`.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.forIn(new Foo, function(value, key) {
 *   console.log(key);
 * });
 * // => logs 'a', 'b', and 'c' (iteration order is not guaranteed)
 */
var forIn = createForIn(baseFor);

module.exports = forIn;

},{"../internal/baseFor":18,"../internal/createForIn":46}],86:[function(require,module,exports){
var baseForRight = require('../internal/baseForRight'),
    createForIn = require('../internal/createForIn');

/**
 * This method is like `_.forIn` except that it iterates over properties of
 * `object` in the opposite order.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to iterate over.
 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
 * @param {*} [thisArg] The `this` binding of `iteratee`.
 * @returns {Object} Returns `object`.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.forInRight(new Foo, function(value, key) {
 *   console.log(key);
 * });
 * // => logs 'c', 'b', and 'a' assuming `_.forIn ` logs 'a', 'b', and 'c'
 */
var forInRight = createForIn(baseForRight);

module.exports = forInRight;

},{"../internal/baseForRight":22,"../internal/createForIn":46}],87:[function(require,module,exports){
var baseForOwn = require('../internal/baseForOwn'),
    createForOwn = require('../internal/createForOwn');

/**
 * Iterates over own enumerable properties of an object invoking `iteratee`
 * for each property. The `iteratee` is bound to `thisArg` and invoked with
 * three arguments: (value, key, object). Iteratee functions may exit iteration
 * early by explicitly returning `false`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to iterate over.
 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
 * @param {*} [thisArg] The `this` binding of `iteratee`.
 * @returns {Object} Returns `object`.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.forOwn(new Foo, function(value, key) {
 *   console.log(key);
 * });
 * // => logs 'a' and 'b' (iteration order is not guaranteed)
 */
var forOwn = createForOwn(baseForOwn);

module.exports = forOwn;

},{"../internal/baseForOwn":20,"../internal/createForOwn":47}],88:[function(require,module,exports){
var baseForOwnRight = require('../internal/baseForOwnRight'),
    createForOwn = require('../internal/createForOwn');

/**
 * This method is like `_.forOwn` except that it iterates over properties of
 * `object` in the opposite order.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to iterate over.
 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
 * @param {*} [thisArg] The `this` binding of `iteratee`.
 * @returns {Object} Returns `object`.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.forOwnRight(new Foo, function(value, key) {
 *   console.log(key);
 * });
 * // => logs 'b' and 'a' assuming `_.forOwn` logs 'a' and 'b'
 */
var forOwnRight = createForOwn(baseForOwnRight);

module.exports = forOwnRight;

},{"../internal/baseForOwnRight":21,"../internal/createForOwn":47}],89:[function(require,module,exports){
var baseFunctions = require('../internal/baseFunctions'),
    keysIn = require('./keysIn');

/**
 * Creates an array of function property names from all enumerable properties,
 * own and inherited, of `object`.
 *
 * @static
 * @memberOf _
 * @alias methods
 * @category Object
 * @param {Object} object The object to inspect.
 * @returns {Array} Returns the new array of property names.
 * @example
 *
 * _.functions(_);
 * // => ['after', 'ary', 'assign', ...]
 */
function functions(object) {
  return baseFunctions(object, keysIn(object));
}

module.exports = functions;

},{"../internal/baseFunctions":23,"./keysIn":94}],90:[function(require,module,exports){
var baseGet = require('../internal/baseGet'),
    toPath = require('../internal/toPath');

/**
 * Gets the property value at `path` of `object`. If the resolved value is
 * `undefined` the `defaultValue` is used in its place.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @param {*} [defaultValue] The value returned if the resolved value is `undefined`.
 * @returns {*} Returns the resolved value.
 * @example
 *
 * var object = { 'a': [{ 'b': { 'c': 3 } }] };
 *
 * _.get(object, 'a[0].b.c');
 * // => 3
 *
 * _.get(object, ['a', '0', 'b', 'c']);
 * // => 3
 *
 * _.get(object, 'a.b.c', 'default');
 * // => 'default'
 */
function get(object, path, defaultValue) {
  var result = object == null ? undefined : baseGet(object, toPath(path), (path + ''));
  return result === undefined ? defaultValue : result;
}

module.exports = get;

},{"../internal/baseGet":24,"../internal/toPath":68}],91:[function(require,module,exports){
var baseGet = require('../internal/baseGet'),
    baseSlice = require('../internal/baseSlice'),
    isArguments = require('../lang/isArguments'),
    isArray = require('../lang/isArray'),
    isIndex = require('../internal/isIndex'),
    isKey = require('../internal/isKey'),
    isLength = require('../internal/isLength'),
    last = require('../array/last'),
    toPath = require('../internal/toPath');

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Checks if `path` is a direct property.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @returns {boolean} Returns `true` if `path` is a direct property, else `false`.
 * @example
 *
 * var object = { 'a': { 'b': { 'c': 3 } } };
 *
 * _.has(object, 'a');
 * // => true
 *
 * _.has(object, 'a.b.c');
 * // => true
 *
 * _.has(object, ['a', 'b', 'c']);
 * // => true
 */
function has(object, path) {
  if (object == null) {
    return false;
  }
  var result = hasOwnProperty.call(object, path);
  if (!result && !isKey(path)) {
    path = toPath(path);
    object = path.length == 1 ? object : baseGet(object, baseSlice(path, 0, -1));
    if (object == null) {
      return false;
    }
    path = last(path);
    result = hasOwnProperty.call(object, path);
  }
  return result || (isLength(object.length) && isIndex(path, object.length) &&
    (isArray(object) || isArguments(object)));
}

module.exports = has;

},{"../array/last":1,"../internal/baseGet":24,"../internal/baseSlice":35,"../internal/isIndex":57,"../internal/isKey":59,"../internal/isLength":60,"../internal/toPath":68,"../lang/isArguments":69,"../lang/isArray":70}],92:[function(require,module,exports){
var isIterateeCall = require('../internal/isIterateeCall'),
    keys = require('./keys');

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Creates an object composed of the inverted keys and values of `object`.
 * If `object` contains duplicate values, subsequent values overwrite property
 * assignments of previous values unless `multiValue` is `true`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to invert.
 * @param {boolean} [multiValue] Allow multiple values per key.
 * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
 * @returns {Object} Returns the new inverted object.
 * @example
 *
 * var object = { 'a': 1, 'b': 2, 'c': 1 };
 *
 * _.invert(object);
 * // => { '1': 'c', '2': 'b' }
 *
 * // with `multiValue`
 * _.invert(object, true);
 * // => { '1': ['a', 'c'], '2': ['b'] }
 */
function invert(object, multiValue, guard) {
  if (guard && isIterateeCall(object, multiValue, guard)) {
    multiValue = undefined;
  }
  var index = -1,
      props = keys(object),
      length = props.length,
      result = {};

  while (++index < length) {
    var key = props[index],
        value = object[key];

    if (multiValue) {
      if (hasOwnProperty.call(result, value)) {
        result[value].push(key);
      } else {
        result[value] = [key];
      }
    }
    else {
      result[value] = key;
    }
  }
  return result;
}

module.exports = invert;

},{"../internal/isIterateeCall":58,"./keys":93}],93:[function(require,module,exports){
var getNative = require('../internal/getNative'),
    isArrayLike = require('../internal/isArrayLike'),
    isObject = require('../lang/isObject'),
    shimKeys = require('../internal/shimKeys');

/* Native method references for those with the same name as other `lodash` methods. */
var nativeKeys = getNative(Object, 'keys');

/**
 * Creates an array of the own enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects. See the
 * [ES spec](http://ecma-international.org/ecma-262/6.0/#sec-object.keys)
 * for more details.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keys(new Foo);
 * // => ['a', 'b'] (iteration order is not guaranteed)
 *
 * _.keys('hi');
 * // => ['0', '1']
 */
var keys = !nativeKeys ? shimKeys : function(object) {
  var Ctor = object == null ? undefined : object.constructor;
  if ((typeof Ctor == 'function' && Ctor.prototype === object) ||
      (typeof object != 'function' && isArrayLike(object))) {
    return shimKeys(object);
  }
  return isObject(object) ? nativeKeys(object) : [];
};

module.exports = keys;

},{"../internal/getNative":54,"../internal/isArrayLike":56,"../internal/shimKeys":66,"../lang/isObject":73}],94:[function(require,module,exports){
var isArguments = require('../lang/isArguments'),
    isArray = require('../lang/isArray'),
    isIndex = require('../internal/isIndex'),
    isLength = require('../internal/isLength'),
    isObject = require('../lang/isObject');

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Creates an array of the own and inherited enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keysIn(new Foo);
 * // => ['a', 'b', 'c'] (iteration order is not guaranteed)
 */
function keysIn(object) {
  if (object == null) {
    return [];
  }
  if (!isObject(object)) {
    object = Object(object);
  }
  var length = object.length;
  length = (length && isLength(length) &&
    (isArray(object) || isArguments(object)) && length) || 0;

  var Ctor = object.constructor,
      index = -1,
      isProto = typeof Ctor == 'function' && Ctor.prototype === object,
      result = Array(length),
      skipIndexes = length > 0;

  while (++index < length) {
    result[index] = (index + '');
  }
  for (var key in object) {
    if (!(skipIndexes && isIndex(key, length)) &&
        !(key == 'constructor' && (isProto || !hasOwnProperty.call(object, key)))) {
      result.push(key);
    }
  }
  return result;
}

module.exports = keysIn;

},{"../internal/isIndex":57,"../internal/isLength":60,"../lang/isArguments":69,"../lang/isArray":70,"../lang/isObject":73}],95:[function(require,module,exports){
var createObjectMapper = require('../internal/createObjectMapper');

/**
 * The opposite of `_.mapValues`; this method creates an object with the
 * same values as `object` and keys generated by running each own enumerable
 * property of `object` through `iteratee`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to iterate over.
 * @param {Function|Object|string} [iteratee=_.identity] The function invoked
 *  per iteration.
 * @param {*} [thisArg] The `this` binding of `iteratee`.
 * @returns {Object} Returns the new mapped object.
 * @example
 *
 * _.mapKeys({ 'a': 1, 'b': 2 }, function(value, key) {
 *   return key + value;
 * });
 * // => { 'a1': 1, 'b2': 2 }
 */
var mapKeys = createObjectMapper(true);

module.exports = mapKeys;

},{"../internal/createObjectMapper":48}],96:[function(require,module,exports){
var createObjectMapper = require('../internal/createObjectMapper');

/**
 * Creates an object with the same keys as `object` and values generated by
 * running each own enumerable property of `object` through `iteratee`. The
 * iteratee function is bound to `thisArg` and invoked with three arguments:
 * (value, key, object).
 *
 * If a property name is provided for `iteratee` the created `_.property`
 * style callback returns the property value of the given element.
 *
 * If a value is also provided for `thisArg` the created `_.matchesProperty`
 * style callback returns `true` for elements that have a matching property
 * value, else `false`.
 *
 * If an object is provided for `iteratee` the created `_.matches` style
 * callback returns `true` for elements that have the properties of the given
 * object, else `false`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to iterate over.
 * @param {Function|Object|string} [iteratee=_.identity] The function invoked
 *  per iteration.
 * @param {*} [thisArg] The `this` binding of `iteratee`.
 * @returns {Object} Returns the new mapped object.
 * @example
 *
 * _.mapValues({ 'a': 1, 'b': 2 }, function(n) {
 *   return n * 3;
 * });
 * // => { 'a': 3, 'b': 6 }
 *
 * var users = {
 *   'fred':    { 'user': 'fred',    'age': 40 },
 *   'pebbles': { 'user': 'pebbles', 'age': 1 }
 * };
 *
 * // using the `_.property` callback shorthand
 * _.mapValues(users, 'age');
 * // => { 'fred': 40, 'pebbles': 1 } (iteration order is not guaranteed)
 */
var mapValues = createObjectMapper();

module.exports = mapValues;

},{"../internal/createObjectMapper":48}],97:[function(require,module,exports){
var baseMerge = require('../internal/baseMerge'),
    createAssigner = require('../internal/createAssigner');

/**
 * Recursively merges own enumerable properties of the source object(s), that
 * don't resolve to `undefined` into the destination object. Subsequent sources
 * overwrite property assignments of previous sources. If `customizer` is
 * provided it's invoked to produce the merged values of the destination and
 * source properties. If `customizer` returns `undefined` merging is handled
 * by the method instead. The `customizer` is bound to `thisArg` and invoked
 * with five arguments: (objectValue, sourceValue, key, object, source).
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The destination object.
 * @param {...Object} [sources] The source objects.
 * @param {Function} [customizer] The function to customize assigned values.
 * @param {*} [thisArg] The `this` binding of `customizer`.
 * @returns {Object} Returns `object`.
 * @example
 *
 * var users = {
 *   'data': [{ 'user': 'barney' }, { 'user': 'fred' }]
 * };
 *
 * var ages = {
 *   'data': [{ 'age': 36 }, { 'age': 40 }]
 * };
 *
 * _.merge(users, ages);
 * // => { 'data': [{ 'user': 'barney', 'age': 36 }, { 'user': 'fred', 'age': 40 }] }
 *
 * // using a customizer callback
 * var object = {
 *   'fruits': ['apple'],
 *   'vegetables': ['beet']
 * };
 *
 * var other = {
 *   'fruits': ['banana'],
 *   'vegetables': ['carrot']
 * };
 *
 * _.merge(object, other, function(a, b) {
 *   if (_.isArray(a)) {
 *     return a.concat(b);
 *   }
 * });
 * // => { 'fruits': ['apple', 'banana'], 'vegetables': ['beet', 'carrot'] }
 */
var merge = createAssigner(baseMerge);

module.exports = merge;

},{"../internal/baseMerge":31,"../internal/createAssigner":41}],98:[function(require,module,exports){
module.exports = require('./functions');

},{"./functions":89}],99:[function(require,module,exports){
var arrayMap = require('../internal/arrayMap'),
    baseDifference = require('../internal/baseDifference'),
    baseFlatten = require('../internal/baseFlatten'),
    bindCallback = require('../internal/bindCallback'),
    keysIn = require('./keysIn'),
    pickByArray = require('../internal/pickByArray'),
    pickByCallback = require('../internal/pickByCallback'),
    restParam = require('../function/restParam');

/**
 * The opposite of `_.pick`; this method creates an object composed of the
 * own and inherited enumerable properties of `object` that are not omitted.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The source object.
 * @param {Function|...(string|string[])} [predicate] The function invoked per
 *  iteration or property names to omit, specified as individual property
 *  names or arrays of property names.
 * @param {*} [thisArg] The `this` binding of `predicate`.
 * @returns {Object} Returns the new object.
 * @example
 *
 * var object = { 'user': 'fred', 'age': 40 };
 *
 * _.omit(object, 'age');
 * // => { 'user': 'fred' }
 *
 * _.omit(object, _.isNumber);
 * // => { 'user': 'fred' }
 */
var omit = restParam(function(object, props) {
  if (object == null) {
    return {};
  }
  if (typeof props[0] != 'function') {
    var props = arrayMap(baseFlatten(props), String);
    return pickByArray(object, baseDifference(keysIn(object), props));
  }
  var predicate = bindCallback(props[0], props[1], 3);
  return pickByCallback(object, function(value, key, object) {
    return !predicate(value, key, object);
  });
});

module.exports = omit;

},{"../function/restParam":2,"../internal/arrayMap":6,"../internal/baseDifference":15,"../internal/baseFlatten":17,"../internal/bindCallback":38,"../internal/pickByArray":64,"../internal/pickByCallback":65,"./keysIn":94}],100:[function(require,module,exports){
var keys = require('./keys'),
    toObject = require('../internal/toObject');

/**
 * Creates a two dimensional array of the key-value pairs for `object`,
 * e.g. `[[key1, value1], [key2, value2]]`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the new array of key-value pairs.
 * @example
 *
 * _.pairs({ 'barney': 36, 'fred': 40 });
 * // => [['barney', 36], ['fred', 40]] (iteration order is not guaranteed)
 */
function pairs(object) {
  object = toObject(object);

  var index = -1,
      props = keys(object),
      length = props.length,
      result = Array(length);

  while (++index < length) {
    var key = props[index];
    result[index] = [key, object[key]];
  }
  return result;
}

module.exports = pairs;

},{"../internal/toObject":67,"./keys":93}],101:[function(require,module,exports){
var baseFlatten = require('../internal/baseFlatten'),
    bindCallback = require('../internal/bindCallback'),
    pickByArray = require('../internal/pickByArray'),
    pickByCallback = require('../internal/pickByCallback'),
    restParam = require('../function/restParam');

/**
 * Creates an object composed of the picked `object` properties. Property
 * names may be specified as individual arguments or as arrays of property
 * names. If `predicate` is provided it's invoked for each property of `object`
 * picking the properties `predicate` returns truthy for. The predicate is
 * bound to `thisArg` and invoked with three arguments: (value, key, object).
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The source object.
 * @param {Function|...(string|string[])} [predicate] The function invoked per
 *  iteration or property names to pick, specified as individual property
 *  names or arrays of property names.
 * @param {*} [thisArg] The `this` binding of `predicate`.
 * @returns {Object} Returns the new object.
 * @example
 *
 * var object = { 'user': 'fred', 'age': 40 };
 *
 * _.pick(object, 'user');
 * // => { 'user': 'fred' }
 *
 * _.pick(object, _.isString);
 * // => { 'user': 'fred' }
 */
var pick = restParam(function(object, props) {
  if (object == null) {
    return {};
  }
  return typeof props[0] == 'function'
    ? pickByCallback(object, bindCallback(props[0], props[1], 3))
    : pickByArray(object, baseFlatten(props));
});

module.exports = pick;

},{"../function/restParam":2,"../internal/baseFlatten":17,"../internal/bindCallback":38,"../internal/pickByArray":64,"../internal/pickByCallback":65}],102:[function(require,module,exports){
var baseGet = require('../internal/baseGet'),
    baseSlice = require('../internal/baseSlice'),
    isFunction = require('../lang/isFunction'),
    isKey = require('../internal/isKey'),
    last = require('../array/last'),
    toPath = require('../internal/toPath');

/**
 * This method is like `_.get` except that if the resolved value is a function
 * it's invoked with the `this` binding of its parent object and its result
 * is returned.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to resolve.
 * @param {*} [defaultValue] The value returned if the resolved value is `undefined`.
 * @returns {*} Returns the resolved value.
 * @example
 *
 * var object = { 'a': [{ 'b': { 'c1': 3, 'c2': _.constant(4) } }] };
 *
 * _.result(object, 'a[0].b.c1');
 * // => 3
 *
 * _.result(object, 'a[0].b.c2');
 * // => 4
 *
 * _.result(object, 'a.b.c', 'default');
 * // => 'default'
 *
 * _.result(object, 'a.b.c', _.constant('default'));
 * // => 'default'
 */
function result(object, path, defaultValue) {
  var result = object == null ? undefined : object[path];
  if (result === undefined) {
    if (object != null && !isKey(path, object)) {
      path = toPath(path);
      object = path.length == 1 ? object : baseGet(object, baseSlice(path, 0, -1));
      result = object == null ? undefined : object[last(path)];
    }
    result = result === undefined ? defaultValue : result;
  }
  return isFunction(result) ? result.call(object) : result;
}

module.exports = result;

},{"../array/last":1,"../internal/baseGet":24,"../internal/baseSlice":35,"../internal/isKey":59,"../internal/toPath":68,"../lang/isFunction":71}],103:[function(require,module,exports){
var isIndex = require('../internal/isIndex'),
    isKey = require('../internal/isKey'),
    isObject = require('../lang/isObject'),
    toPath = require('../internal/toPath');

/**
 * Sets the property value of `path` on `object`. If a portion of `path`
 * does not exist it's created.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to augment.
 * @param {Array|string} path The path of the property to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns `object`.
 * @example
 *
 * var object = { 'a': [{ 'b': { 'c': 3 } }] };
 *
 * _.set(object, 'a[0].b.c', 4);
 * console.log(object.a[0].b.c);
 * // => 4
 *
 * _.set(object, 'x[0].y.z', 5);
 * console.log(object.x[0].y.z);
 * // => 5
 */
function set(object, path, value) {
  if (object == null) {
    return object;
  }
  var pathKey = (path + '');
  path = (object[pathKey] != null || isKey(path, object)) ? [pathKey] : toPath(path);

  var index = -1,
      length = path.length,
      lastIndex = length - 1,
      nested = object;

  while (nested != null && ++index < length) {
    var key = path[index];
    if (isObject(nested)) {
      if (index == lastIndex) {
        nested[key] = value;
      } else if (nested[key] == null) {
        nested[key] = isIndex(path[index + 1]) ? [] : {};
      }
    }
    nested = nested[key];
  }
  return object;
}

module.exports = set;

},{"../internal/isIndex":57,"../internal/isKey":59,"../internal/toPath":68,"../lang/isObject":73}],104:[function(require,module,exports){
var arrayEach = require('../internal/arrayEach'),
    baseCallback = require('../internal/baseCallback'),
    baseCreate = require('../internal/baseCreate'),
    baseForOwn = require('../internal/baseForOwn'),
    isArray = require('../lang/isArray'),
    isFunction = require('../lang/isFunction'),
    isObject = require('../lang/isObject'),
    isTypedArray = require('../lang/isTypedArray');

/**
 * An alternative to `_.reduce`; this method transforms `object` to a new
 * `accumulator` object which is the result of running each of its own enumerable
 * properties through `iteratee`, with each invocation potentially mutating
 * the `accumulator` object. The `iteratee` is bound to `thisArg` and invoked
 * with four arguments: (accumulator, value, key, object). Iteratee functions
 * may exit iteration early by explicitly returning `false`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Array|Object} object The object to iterate over.
 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
 * @param {*} [accumulator] The custom accumulator value.
 * @param {*} [thisArg] The `this` binding of `iteratee`.
 * @returns {*} Returns the accumulated value.
 * @example
 *
 * _.transform([2, 3, 4], function(result, n) {
 *   result.push(n *= n);
 *   return n % 2 == 0;
 * });
 * // => [4, 9]
 *
 * _.transform({ 'a': 1, 'b': 2 }, function(result, n, key) {
 *   result[key] = n * 3;
 * });
 * // => { 'a': 3, 'b': 6 }
 */
function transform(object, iteratee, accumulator, thisArg) {
  var isArr = isArray(object) || isTypedArray(object);
  iteratee = baseCallback(iteratee, thisArg, 4);

  if (accumulator == null) {
    if (isArr || isObject(object)) {
      var Ctor = object.constructor;
      if (isArr) {
        accumulator = isArray(object) ? new Ctor : [];
      } else {
        accumulator = baseCreate(isFunction(Ctor) ? Ctor.prototype : undefined);
      }
    } else {
      accumulator = {};
    }
  }
  (isArr ? arrayEach : baseForOwn)(object, function(value, index, object) {
    return iteratee(accumulator, value, index, object);
  });
  return accumulator;
}

module.exports = transform;

},{"../internal/arrayEach":5,"../internal/baseCallback":12,"../internal/baseCreate":14,"../internal/baseForOwn":20,"../lang/isArray":70,"../lang/isFunction":71,"../lang/isObject":73,"../lang/isTypedArray":75}],105:[function(require,module,exports){
var baseValues = require('../internal/baseValues'),
    keys = require('./keys');

/**
 * Creates an array of the own enumerable property values of `object`.
 *
 * **Note:** Non-object values are coerced to objects.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property values.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.values(new Foo);
 * // => [1, 2] (iteration order is not guaranteed)
 *
 * _.values('hi');
 * // => ['h', 'i']
 */
function values(object) {
  return baseValues(object, keys(object));
}

module.exports = values;

},{"../internal/baseValues":37,"./keys":93}],106:[function(require,module,exports){
var baseValues = require('../internal/baseValues'),
    keysIn = require('./keysIn');

/**
 * Creates an array of the own and inherited enumerable property values
 * of `object`.
 *
 * **Note:** Non-object values are coerced to objects.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property values.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.valuesIn(new Foo);
 * // => [1, 2, 3] (iteration order is not guaranteed)
 */
function valuesIn(object) {
  return baseValues(object, keysIn(object));
}

module.exports = valuesIn;

},{"../internal/baseValues":37,"./keysIn":94}],107:[function(require,module,exports){
/**
 * This method returns the first argument provided to it.
 *
 * @static
 * @memberOf _
 * @category Utility
 * @param {*} value Any value.
 * @returns {*} Returns `value`.
 * @example
 *
 * var object = { 'user': 'fred' };
 *
 * _.identity(object) === object;
 * // => true
 */
function identity(value) {
  return value;
}

module.exports = identity;

},{}],108:[function(require,module,exports){
var baseProperty = require('../internal/baseProperty'),
    basePropertyDeep = require('../internal/basePropertyDeep'),
    isKey = require('../internal/isKey');

/**
 * Creates a function that returns the property value at `path` on a
 * given object.
 *
 * @static
 * @memberOf _
 * @category Utility
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new function.
 * @example
 *
 * var objects = [
 *   { 'a': { 'b': { 'c': 2 } } },
 *   { 'a': { 'b': { 'c': 1 } } }
 * ];
 *
 * _.map(objects, _.property('a.b.c'));
 * // => [2, 1]
 *
 * _.pluck(_.sortBy(objects, _.property(['a', 'b', 'c'])), 'a.b.c');
 * // => [1, 2]
 */
function property(path) {
  return isKey(path) ? baseProperty(path) : basePropertyDeep(path);
}

module.exports = property;

},{"../internal/baseProperty":33,"../internal/basePropertyDeep":34,"../internal/isKey":59}],109:[function(require,module,exports){
var AudioContext = window.AudioContext || window.webkitAudioContext

module.exports = WebAudioAnalyser

function WebAudioAnalyser(audio, ctx, opts) {
  if (!(this instanceof WebAudioAnalyser)) return new WebAudioAnalyser(audio, ctx, opts)
  if (!(ctx instanceof AudioContext)) (opts = ctx), (ctx = null)

  opts = opts || {}
  this.ctx = ctx = ctx || new AudioContext

  if (!(audio instanceof AudioNode)) {
    audio = audio instanceof Audio
      ? ctx.createMediaElementSource(audio)
      : ctx.createMediaStreamSource(audio)
  }

  this.analyser = ctx.createAnalyser()
  this.stereo   = !!opts.stereo
  this.audible  = opts.audible !== false
  this.wavedata = null
  this.freqdata = null
  this.splitter = null
  this.merger   = null
  this.source   = audio

  if (!this.stereo) {
    this.output = this.source
    this.source.connect(this.analyser)
    if (this.audible)
      this.analyser.connect(ctx.destination)
  } else {
    this.analyser = [this.analyser]
    this.analyser.push(ctx.createAnalyser())

    this.splitter = ctx.createChannelSplitter(2)
    this.merger   = ctx.createChannelMerger(2)
    this.output   = this.merger

    this.source.connect(this.splitter)

    for (var i = 0; i < 2; i++) {
      this.splitter.connect(this.analyser[i], i, 0)
      this.analyser[i].connect(this.merger, 0, i)
    }

    if (this.audible)
      this.merger.connect(ctx.destination)
  }
}

WebAudioAnalyser.prototype.waveform = function(output, channel) {
  if (!output) output = this.wavedata || (
    this.wavedata = new Uint8Array((this.analyser[0] || this.analyser).frequencyBinCount)
  )

  var analyser = this.stereo
    ? this.analyser[channel || 0]
    : this.analyser

  analyser.getByteTimeDomainData(output)

  return output
}

WebAudioAnalyser.prototype.frequencies = function(output, channel) {
  if (!output) output = this.freqdata || (
    this.freqdata = new Uint8Array((this.analyser[0] || this.analyser).frequencyBinCount)
  )

  var analyser = this.stereo
    ? this.analyser[channel || 0]
    : this.analyser

  analyser.getByteFrequencyData(output)

  return output
}

},{}],110:[function(require,module,exports){
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Analyses the audio stream in prep for drawing.
 * utilizes Hugh Kenenedy's web-audio-analyser and adapted from
 * Felix Turner's web audio example.
 *
 * https://www.npmjs.com/package/web-audio-analyser
 * http://www.airtightinteractive.com/demos/js/uberviz/audioanalysis/js/AudioHandler.js
 */
var lodash = require("lodash/object");
var audioAnalyser = require("web-audio-analyser");

var AudioAnalyser = (function () {
    function AudioAnalyser(source) {
        _classCallCheck(this, AudioAnalyser);

        //create audio element
        var audio = new Audio();
        audio.crossOrigin = 'Anonymous';
        audio.src = source;
        document.body.appendChild(audio);

        //init new web-audio-analyser
        var a = new audioAnalyser(audio);

        //apply properties from web-audio-analyser directly onto the object
        this._applyProps(a);

        //audio element
        this.audio = audio;

        this.avgLevel = 0;
        this.volumeSensitivity = 1;
        this.binCount = this.analyser.frequencyBinCount;
        this.levelsCount = 16; //should be factor of 512
        this.levelBins = Math.floor(this.binCount / this.levelsCount); //number of bins in each level
        this.width = this.analyser.frequencyBinCount / 4;
        this.levelsCount = 16;

        //levels of each frequency - from 0 - 1 . no sound is 0.
        this.levelsData = [];

        //bars - bar data is from 0 - 256 in 512 bins. no sound is 0;
        this.freqByteData = new Uint8Array(this.binCount);

        //waveform - waveform data is from 0-256 for 512 bins. no sound is 128.
        this.timeByteData = new Uint8Array(this.binCount);

        //waveform - from 0 - 1 . no sound is 0.5. Array [binCount]
        this.dataWaveform = [];

        //history of all the levels at various points
        this.levelHistory = [];

        //////// BPM STUFF ///////////
        this.BEAT_HOLD_TIME = 40; //num of frames to hold a beat
        this.BEAT_DECAY_RATE = 0.98;
        this.BEAT_MIN = 0.15; //a volume less than this is no beat

        this.count = 0;
        this.msecsFirst = 0;
        this.msecsPrevious = 0;
        this.msecsAvg = 633; //time between beats (msec)
        this.gotBeat = false;
        this.beatCutOff = 0;
        this.beatTime = 0;
        this.beatHoldTime = 40;
        this.beatDecayRate = 0.97;
        this.bpmStart = 0;
    }

    /**
     * Updates our audio data. Meant to be used within a loop
     */

    _createClass(AudioAnalyser, [{
        key: "update",
        value: function update() {
            if (this.isPlaying) {
                this.waveform(this.timeByteData);
                this.frequencies(this.freqByteData);

                this.updateWaveform();
                this.normalizeLevels();
            }
        }
    }, {
        key: "getLevelsArray",
        value: function getLevelsArray() {
            return this.levelsData;
        }

        /**
         * Returns the data at the specific frequency
         * @param index
         * @returns {*}
         */

    }, {
        key: "getChannelData",
        value: function getChannelData(index) {
            return this.levelsData[index];
        }
    }, {
        key: "getFrequencyData",
        value: function getFrequencyData(index) {
            return this.freqByteData[index];
        }
    }, {
        key: "getWaveformData",
        value: function getWaveformData(index) {
            return this.dataWaveform[index];
        }
    }, {
        key: "getLevelHistory",
        value: function getLevelHistory(index) {
            return this.levelHistory[index];
        }
        ///////////////////////////////

    }, {
        key: "_applyProps",
        value: function _applyProps(a) {
            lodash.merge(this, a);
            lodash.merge(this.__proto__, a.__proto__);
        }
    }, {
        key: "play",
        value: function play() {

            if (this.isPlaying) {
                this.pause();
                this.isPlaying = false;
            } else {

                this.audio.play();
                this.isPlaying = true;
            }

            return this;
        }
    }, {
        key: "pause",
        value: function pause() {
            this.audio.pause();
            this.isPlaying = false;
        }

        /**
         * Updates the waveform information
         */

    }, {
        key: "updateWaveform",
        value: function updateWaveform() {
            for (var i = 0; i < this.binCount; i++) {
                this.dataWaveform[i] = (this.timeByteData[i] - 128) / 128 * this.volumeSensitivity;
            }
        }
    }, {
        key: "beatDetect",
        value: function beatDetect() {
            if (this.avgLevel > this.beatCutOff && this.avgLevel > this.BEAT_MIN) {
                this.gotBeat = true;
                this.beatCutOff = this.avgLevel * 1.1;
                this.beatTime = 0;
            } else {
                if (this.beatTime <= this.beatHoldTime) {
                    this.beatTime++;
                } else {
                    this.beatCutOff *= this.beatDecayRate;
                    this.beatCutOff = Math.max(this.beatCutOff, this.BEAT_MIN);
                }
            }
            this.bpmTime = (new Date().getTime() - this.bpmStart) / this.msecsAvg;
        }

        /**
         * Normalize the levels
         */

    }, {
        key: "normalizeLevels",
        value: function normalizeLevels() {
            for (var i = 0; i < this.levelsCount; i++) {
                var sum = 0;
                for (var j = 0; j < this.levelBins; j++) {
                    sum += this.freqByteData[i * this.levelBins + j];
                }
                this.levelsData[i] = sum / this.levelBins / 256 * this.volumeSensitivity; //freqData maxs at 256

                //adjust for the fact that lower levels are percieved more quietly
                //make lower levels smaller
                //levelsData[i] *=  1 + (i/levelsCount)/2;
            }
        }
    }, {
        key: "getAverageLevel",
        value: function getAverageLevel() {
            //GET AVG LEVEL
            var sum = 0;
            for (var j = 0; j < this.levelsCount; j++) {
                sum += this.levelsData[j];
            }

            this.avgLevel = sum / this.levelsCount;
            this.levelHistory.push(this.avgLevel);
            this.levelHistory.shift(1);
        }
    }]);

    return AudioAnalyser;
})();

exports.default = AudioAnalyser;

},{"lodash/object":77,"web-audio-analyser":109}],111:[function(require,module,exports){
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _AudioAnalyser = require("./AudioAnalyser");

var _AudioAnalyser2 = _interopRequireDefault(_AudioAnalyser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Builds a texture that holds audio data from audio files.
 * Should work either with Three.js or regular webgl via stack-gl
 * Holds onto it's own audio and analyser elements
 */
var lodash = require("lodash/object");

var AudioTexture = (function () {
    function AudioTexture(audio, gl, options) {
        _classCallCheck(this, AudioTexture);

        var defaults = {};
        try {
            defaults = {
                format: THREE.RGBAFormat,
                type: THREE.FloatType
            };
        } catch (e) {
            console.log("Setting defaults... ", e.message);
        }
        options = options !== undefined ? lodash.merge(options, defaults) : defaults;

        var analyser = new _AudioAnalyser2.default(audio);
        options.width = analyser.binCount / 4;

        this.context = gl;

        this.path = audio;
        this.audio = analyser;
        this.options = options;

        var audioData = this.processAudio();
        this.audioDataArray = new Float32Array(options.width * 4);

        try {
            this.texture = new THREE.DataTexture(audioData, audioData.length / 16, 1, options.format, options.type);
            this.texture.needsUpdate = true;
        } catch (e) {
            console.log("Creating Textures... ", e.message, " - building regular webgl texture");
            if (gl === undefined) {
                console.error("AudioTexture with path : ", audio, " ,needs a gl handle in order to function");
            } else {
                this.texture = this.buildTexture(gl, audioData);
            }
        }
    }

    _createClass(AudioTexture, [{
        key: "buildTexture",
        value: function buildTexture(gl, audioData) {
            var tex = this.initTexture(gl);

            var arr = this._buildArray(audioData);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, arr.width, arr.width, 0, gl.RGB, gl.FLOAT, arr.array);
            return tex;
        }
    }, {
        key: "updateTexture",
        value: function updateTexture(gl, tex, data) {
            //rebind texture
            gl.bindTexture(gl.TEXTURE_2D, tex);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 32, 32, 0, gl.RGBA, gl.UNSIGNED_BYTE, data);
        }
    }, {
        key: "_buildArray",
        value: function _buildArray(arr) {
            var texWidth = Math.ceil(Math.sqrt(arr.length / 3));
            var array = new Float32Array(texWidth * texWidth * 3);
            for (var i = 0; i < arr.length; ++i) {
                array[i] = arr[i];
            }

            return {
                array: array,
                width: texWidth
            };
        }
    }, {
        key: "initTexture",
        value: function initTexture(handle) {
            var gl = handle;
            var tex = gl.createTexture();
            gl.bindTexture(gl.TEXTURE_2D, tex);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

            return tex;
        }
    }, {
        key: "getLevels",
        value: function getLevels() {
            return this.audio.levelsData;
        }

        /**
         * Returns the levels data for the specified chanel.
         * The assumption is a 16 channel setup at the moment.
         * TODO might have to revisit if there are comps with more than 16 channel audio
         * @param index
         * @returns {*}
         */

    }, {
        key: "getChannelData",
        value: function getChannelData(index) {
            if (index < 16) {
                return this.audio.getChannelData(index);
            } else if (index === 16) {
                return this.audio.getChannelData(index - 1);
            } else {
                console.error("AudioTexture::getChannelData - specified channel does not exist");
            }
        }
    }, {
        key: "getTexture",
        value: function getTexture() {
            return this.texture;
        }

        /**
         * Returns true/false depending on whether or not the audio
         * is playing.
         * @returns {boolean}
         */

    }, {
        key: "isPlaying",
        value: function isPlaying() {
            return this.audio.isPlaying;
        }

        /**
         * Toggles playback.
         * Triggers update as well at the same time
         */

    }, {
        key: "toggleAudio",
        value: function toggleAudio() {
            this.update();
            this.audio.play();
        }
    }, {
        key: "update",
        value: function update() {
            this.audio.update();

            this.texture.image.data = this.processAudio();
            this.texture.needsUpdate = true;
        }

        /**
         * Binds texture for use in shader
         * @param uniform the uniform location to utilize
         * @param index the texture index to use. Will default to 0 if none is specified.
         */

    }, {
        key: "bind",
        value: function bind(uniform, index) {
            index = index !== undefined ? index : 0;

            try {
                var gl = this.context;
                gl.activeTexture(gl.TEXTURE0 + index);
                gl.bindTexture(gl.TEXTURE_2D, this.texture);
                gl.uniform1i(uniform, index);
            } catch (e) {
                if (THREE !== undefined) {
                    console.log('For the audio texture leading to - ', this.path, ' - this is not the function to use to bind the texture');
                }
            }
        }
    }, {
        key: "unbind",
        value: function unbind() {
            this.context.bindTexture(gl.TEXTURE_2D, null);
        }
    }, {
        key: "getTexture",
        value: function getTexture() {
            return this.texture;
        }
    }, {
        key: "processAudio",
        value: function processAudio() {
            var options = this.options;
            var audio = this.audio;
            this.audioDataArray = new Float32Array(options.width * 4);

            for (var i = 0; i < options.width; i += 4) {
                this.audioDataArray[i] = audio.getChannelData(i / 4);
                this.audioDataArray[i + 1] = audio.getChannelData(i / 4 + 1);
                this.audioDataArray[i + 2] = audio.getChannelData(i / 4 + 2);
                this.audioDataArray[i + 3] = audio.getChannelData(i / 4 + 3);
            }

            return this.audioDataArray;
        }
    }]);

    return AudioTexture;
})();

exports.default = AudioTexture;

},{"./AudioAnalyser":110,"lodash/object":77}],112:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Background = (function () {
    function Background() {
        _classCallCheck(this, Background);
    }

    _createClass(Background, [{
        key: 'buildStarsBackground',
        value: function buildStarsBackground() {}
    }, {
        key: 'buildDemoBackground',
        value: function buildDemoBackground() {
            var radius = Math.max(window.innerWidth, window.innerHeight) * 1.05;
            var width = window.innerWidth;
            var height = window.innerHeight;
            var geo = new THREE.PlaneBufferGeometry(window.innerWidth, window.innerHeight);
            var mat = new THREE.ShaderMaterial({
                vertexShader: "#define GLSLIFY 1\n\nvarying vec2 vUv;\nvoid main() {\n\tvUv = uv;\n\tvec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );\n\tgl_Position = projectionMatrix * mvPosition;\n}\n",
                fragmentShader: "#define GLSLIFY 1\n#ifdef GL_ES\nprecision mediump float;\n#endif\n\nvarying vec2 vUv;\n\nhighp float random_1_0(vec2 co)\n{\n    highp float a = 12.9898;\n    highp float b = 78.233;\n    highp float c = 43758.5453;\n    highp float dt= dot(co.xy ,vec2(a,b));\n    highp float sn= mod(dt,3.14);\n    return fract(sin(sn) * c);\n}\n\n\n\nuniform float aspect;\nuniform vec2 scale;\nuniform vec2 offset;\n\n\nuniform vec2 smoothing;\nuniform float noiseAlpha;\n\nuniform vec3 color1;\nuniform vec3 color2;\n\nvec3 BlendOverlay(vec3 base, vec3 blend) {\n\treturn vec3(\n\t\tbase.r < 0.5 ? (2.0 * base.r * blend.r) : (1.0 - 2.0 * (1.0 - base.r) * (1.0 - blend.r)),\n\t\tbase.g < 0.5 ? (2.0 * base.g * blend.g) : (1.0 - 2.0 * (1.0 - base.g) * (1.0 - blend.g)),\n\t\tbase.b < 0.5 ? (2.0 * base.b * blend.b) : (1.0 - 2.0 * (1.0 - base.b) * (1.0 - blend.b))\n\t);\n}\n\n//TODO: textures, alpha ?\n\nvoid main() {\n\tvec2 pos = vUv;\n\tpos -= 0.5;\n\n\tbool coloredNoise = true;\n\n\tpos.x *= aspect;\n\tpos /= scale;\n\tpos -= offset;\n\n\tfloat dist = length(pos);\n\tdist = smoothstep(smoothing.x, smoothing.y, 1.-dist);\n\n\tvec4 color = vec4(1.0);\n\tcolor.rgb = mix(color2, color1, dist);\n\n\tif (noiseAlpha > 0.0) {\n\t\tvec3 noise = coloredNoise ? vec3(random_1_0(vUv * 1.5), random_1_0(vUv * 2.5), random_1_0(vUv)) : vec3(random_1_0(vUv));\n\t\tcolor.rgb = mix(color.rgb, BlendOverlay(color.rgb, noise), noiseAlpha);\n\t}\n\tgl_FragColor = color;\n}",
                uniforms: {
                    aspect: {
                        type: 'f',
                        value: 1.0
                    },
                    smoothing: {
                        type: 'v2',
                        value: new THREE.Vector2(-0.5, 1.0)
                    },
                    noiseAlpha: {
                        type: 'f',
                        value: 0.9
                    },
                    offset: {
                        type: 'v2',
                        value: new THREE.Vector2(-0.05, -0.15)
                    },
                    color1: {
                        type: 'c',
                        value: new THREE.Color(0xffffff)
                    },

                    color2: {
                        type: 'c',
                        value: new THREE.Color(parseInt("#283844", 16))
                    },
                    scale: {
                        type: 'v2',
                        value: new THREE.Vector2(1 / width * radius, 1 / height * radius)
                    }
                }
            });

            this.mesh = new THREE.Mesh(geo, mat);
            this.geometry = geo;
            this.material = mat;

            return this.mesh;
        }
    }, {
        key: 'addTo',
        value: function addTo(scene) {
            //scene.add(this.mesh);
        }
    }]);

    return Background;
})();

exports.default = Background;

},{}],113:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Mostly static class for dealing with various common operations.
 * Purposely kept as loosely coupled as possible.
 *
 * Will default to a Three.js implementation if Three.js is available. When it makes sense for and a clean api can
 * be put together, some functions allow you to specify a specific format.
 *
 * requires glslify
 */


var GLUtils = (function () {
    function GLUtils() {
        _classCallCheck(this, GLUtils);
    }

    /**
     * Generates a random texture with random data
     * @param width width for the texture
     * @param height height for the texture
     * @param data optionally pass in your own data - otherwise a array with random values will be used.
     * @returns {THREE.DataTexture}
     */

    _createClass(GLUtils, null, [{
        key: 'generateTexture',
        value: function generateTexture(width, height, data) {

            data = data !== undefined ? data : (function () {
                var radius = 200;
                var num = width * height;
                var data = new Float32Array(num * 3);
                for (var i = 0, i3 = 0; i < num; i++, i3 += 3) {
                    data[i3] = 0;
                    data[i3 + 1] = 0;
                    data[i3 + 2] = 0;
                }
                return data;
            })();

            if (THREE) {
                var tex = new THREE.DataTexture(data, width, height, THREE.RGBAFormat, THREE.FloatType);
                tex.minFilter = THREE.NearestFilter;
                tex.magFilter = THREE.NearestFilter;
                tex.generateMipmaps = false;
                tex.needsUpdate = true;
            }

            return tex;
        }
    }, {
        key: 'generateDefaultShader',
        value: function generateDefaultShader(buildType) {
            return GLUtils.createPassthru(buildType);
        }

        /**
         * Generates a shader object or a full program if Three.js is available
         * @param vertex the path to the vertex shader
         * @param fragment the path to the fragment shader
         * @param uniforms any uniforms
         * @param attributes any attributes
         * @returns {THREE.ShaderMaterial}
         */

    }, {
        key: 'generateShader',
        value: function generateShader(vertex, fragment, uniforms, attributes) {

            if (THREE) {
                uniforms = uniforms !== undefined ? uniforms : {
                    time: {
                        type: 'f',
                        value: 0.0
                    },

                    originTexture: {
                        type: 't',
                        value: null
                    },

                    destinationTexture: {
                        type: 't',
                        value: null
                    }
                };

                //if we just need a fragment shader, you can use the keyword 'passthru' to have the function
                //auto generate a passthru shader
                if (vertex === 'passthru') {
                    vertex = GLUtils.createPassthruVertex();
                }

                return new THREE.ShaderMaterial({
                    vertexShader: vertex,
                    fragmentShader: fragment,
                    uniforms: uniforms
                });
            }
        }
    }, {
        key: 'generateRenderTarget',
        value: function generateRenderTarget(width, height, options) {
            if (THREE) {

                var defaults = {
                    minFilter: THREE.NearestFilter,
                    magFilter: THREE.NearestFilter,
                    format: THREE.RGBAFormat,
                    type: THREE.FloatType,
                    stencilBuffer: false
                };

                return new THREE.WebGLRenderTarget(width, height, defaults);
            }
        }

        /**
         * Generates a pass-thru vertex shader
         * @returns {string}
         * @private
         */

    }, {
        key: 'createPassthruVertex',
        value: function createPassthruVertex() {
            //more threejs specific
            if (THREE) {
                return ['varying vec2 vUv;', 'void main() {', '  vUv = uv;', '  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );', '}'].join('\n');
            }
        }

        /**
         *	Creates a passthru shader
         */

    }, {
        key: 'createPassthru',
        value: function createPassthru(buildType) {
            this.passThruVS = ['varying vec2 vUv;', 'void main() {', '  vUv = uv;', '  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );', '}'].join('\n');

            this.passThruFS = ['uniform sampler2D texture;', 'varying vec2 vUv;', 'void main() {', '  vec2 read = vUv;', '  vec4 c = texture2D( texture , vUv );', '  gl_FragColor = c ;',
            //   'gl_FragColor = vec4(1.);',
            '}'].join('\n');

            if (THREE || buildType === 'three') {
                var uniforms = {
                    texture: { type: 't', value: null }
                };

                var texturePassShader = new THREE.ShaderMaterial({
                    uniforms: uniforms,
                    vertexShader: this.passThruVS,
                    fragmentShader: this.passThruFS
                });
            }

            return texturePassShader;
        }
    }]);

    return GLUtils;
})();

exports.default = GLUtils;

},{}],114:[function(require,module,exports){
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _LinkedShader = require("./LinkedShader.js");

var _LinkedShader2 = _interopRequireDefault(_LinkedShader);

var _RenderBuffer = require("./RenderBuffer.js");

var _RenderBuffer2 = _interopRequireDefault(_RenderBuffer);

var _AudioTexture = require("./AudioTexture.js");

var _AudioTexture2 = _interopRequireDefault(_AudioTexture);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _instanceof(left, right) { if (right != null && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var modifier = require("src/TesselateModifier");
var Tesselate = modifier.tesselate;
var Explode = modifier.explode;


// NOTE - SUPER UNOPTIMIZED!!! :[

var IsoHedron = (function () {
    function IsoHedron(renderer) {
        _classCallCheck(this, IsoHedron);

        this.renderer = renderer;
        this._extractData();
        this.resolution = new THREE.Vector2(window.innerWidth, window.innerHeight);
        this.faces = [];
        this.secondaryGeometry = new THREE.IcosahedronGeometry(100, 1);
    }

    /**
     * Pull face verts from Icosahedron
     * @private
     */

    _createClass(IsoHedron, [{
        key: "_extractData",
        value: function _extractData() {
            var size = 1024;
            var geometry = new THREE.IcosahedronGeometry(100, 1);
            //  var mod = new Tesselate(8);
            // for(var i = 0; i < 5; ++i){
            //     mod.modify(geometry);
            // }
            var facesLength = geometry.faces.length;
            var point = new THREE.Vector3();
            var data = new Float32Array(size * size * 3);
            for (var i = 0, l = data.length; i < l; i += 3) {

                var face = geometry.faces[Math.floor(Math.random() * facesLength)];

                var vertex1 = geometry.vertices[face.a];
                var vertex2 = geometry.vertices[Math.random() > 0.5 ? face.b : face.c];

                point.subVectors(vertex2, vertex1);
                point.multiplyScalar(Math.random());
                point.add(vertex1);

                data[i] = point.x;
                data[i + 1] = point.y;
                data[i + 2] = point.z;
            }

            this.vertices = data;
        }

        /**
         * set teh audio texture to use
         * @param audio
         */

    }, {
        key: "setAudio",
        value: function setAudio(audio) {
            if (_instanceof(audio, _AudioTexture2.default)) {
                this.audio = audio;
            } else {
                console.log("Audio texture needed, object is not audio texture");
            }
        }
    }, {
        key: "buildComponents",
        value: function buildComponents(scene) {
            this.scene = scene;

            var particleEdges = this._buildParticleEdges();
            var particleEdges2 = this._buildSecondaryParticleLines();
            var edges = this._buildEdges();
            var shape = this._buildShape();

            var group = new THREE.Object3D();
            group.add(particleEdges);
            group.add(particleEdges2);
            group.add(edges);
            group.add(shape);

            this.shape = group;
            scene.add(group);
        }
    }, {
        key: "_buildSecondaryParticleLines",
        value: function _buildSecondaryParticleLines() {
            var size = 256;
            var geometry = new THREE.IcosahedronGeometry(80, 1);
            var mod = new Tesselate(8);
            for (var i = 0; i < 5; ++i) {
                mod.modify(geometry);
            }
            var facesLength = geometry.faces.length;
            var point = new THREE.Vector3();
            var data = new Float32Array(size * size * 3);
            for (var i = 0, l = data.length; i < l; i += 3) {

                var face = geometry.faces[Math.floor(Math.random() * facesLength)];

                var vertex1 = geometry.vertices[face.a];
                var vertex2 = geometry.vertices[Math.random() > 0.5 ? face.b : face.c];

                point.subVectors(vertex2, vertex1);
                point.multiplyScalar(Math.random());
                point.add(vertex1);

                data[i] = point.x;
                data[i + 1] = point.y;
                data[i + 2] = point.z;
            }
            var buffer = new _RenderBuffer2.default(1024, "#define GLSLIFY 1\nuniform float opacity;\n\nuniform sampler2D destinationTexture;\nuniform sampler2D originTexture;\nuniform sampler2D audioMap;\n\nuniform float timer;\n\nvarying vec2 vUv;\n\n//\n// Description : Array and textureless GLSL 2D/3D/4D simplex\n//               noise functions.\n//      Author : Ian McEwan, Ashima Arts.\n//  Maintainer : ijm\n//     Lastmod : 20110822 (ijm)\n//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.\n//               Distributed under the MIT License. See LICENSE file.\n//               https://github.com/ashima/webgl-noise\n//\n\nvec3 mod289_1_0(vec3 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec4 mod289_1_0(vec4 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec4 permute_1_1(vec4 x) {\n     return mod289_1_0(((x*34.0)+1.0)*x);\n}\n\nvec4 taylorInvSqrt_1_2(vec4 r)\n{\n  return 1.79284291400159 - 0.85373472095314 * r;\n}\n\nfloat snoise_1_3(vec3 v)\n  {\n  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;\n  const vec4  D_1_4 = vec4(0.0, 0.5, 1.0, 2.0);\n\n// First corner\n  vec3 i  = floor(v + dot(v, C.yyy) );\n  vec3 x0 =   v - i + dot(i, C.xxx) ;\n\n// Other corners\n  vec3 g_1_5 = step(x0.yzx, x0.xyz);\n  vec3 l = 1.0 - g_1_5;\n  vec3 i1 = min( g_1_5.xyz, l.zxy );\n  vec3 i2 = max( g_1_5.xyz, l.zxy );\n\n  //   x0 = x0 - 0.0 + 0.0 * C.xxx;\n  //   x1 = x0 - i1  + 1.0 * C.xxx;\n  //   x2 = x0 - i2  + 2.0 * C.xxx;\n  //   x3 = x0 - 1.0 + 3.0 * C.xxx;\n  vec3 x1 = x0 - i1 + C.xxx;\n  vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y\n  vec3 x3 = x0 - D_1_4.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y\n\n// Permutations\n  i = mod289_1_0(i);\n  vec4 p = permute_1_1( permute_1_1( permute_1_1(\n             i.z + vec4(0.0, i1.z, i2.z, 1.0 ))\n           + i.y + vec4(0.0, i1.y, i2.y, 1.0 ))\n           + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));\n\n// Gradients: 7x7 points over a square, mapped onto an octahedron.\n// The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)\n  float n_ = 0.142857142857; // 1.0/7.0\n  vec3  ns = n_ * D_1_4.wyz - D_1_4.xzx;\n\n  vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)\n\n  vec4 x_ = floor(j * ns.z);\n  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)\n\n  vec4 x = x_ *ns.x + ns.yyyy;\n  vec4 y = y_ *ns.x + ns.yyyy;\n  vec4 h = 1.0 - abs(x) - abs(y);\n\n  vec4 b0 = vec4( x.xy, y.xy );\n  vec4 b1 = vec4( x.zw, y.zw );\n\n  //vec4 s0 = vec4(lessThan(b0,0.0))*2.0 - 1.0;\n  //vec4 s1 = vec4(lessThan(b1,0.0))*2.0 - 1.0;\n  vec4 s0 = floor(b0)*2.0 + 1.0;\n  vec4 s1 = floor(b1)*2.0 + 1.0;\n  vec4 sh = -step(h, vec4(0.0));\n\n  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;\n  vec4 a1_1_6 = b1.xzyw + s1.xzyw*sh.zzww ;\n\n  vec3 p0_1_7 = vec3(a0.xy,h.x);\n  vec3 p1 = vec3(a0.zw,h.y);\n  vec3 p2 = vec3(a1_1_6.xy,h.z);\n  vec3 p3 = vec3(a1_1_6.zw,h.w);\n\n//Normalise gradients\n  vec4 norm = taylorInvSqrt_1_2(vec4(dot(p0_1_7,p0_1_7), dot(p1,p1), dot(p2, p2), dot(p3,p3)));\n  p0_1_7 *= norm.x;\n  p1 *= norm.y;\n  p2 *= norm.z;\n  p3 *= norm.w;\n\n// Mix final noise value\n  vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);\n  m = m * m;\n  return 42.0 * dot( m*m, vec4( dot(p0_1_7,x0), dot(p1,x1),\n                                dot(p2,x2), dot(p3,x3) ) );\n  }\n\n\n\n\nvoid main() {\n\n\tvec4 pos = texture2D( destinationTexture, vUv );\n\tvec4 audio = texture2D(audioMap,vUv);\n\n\tfloat test = snoise_1_3(audio.xyz);\n\n\n\tif ( snoise_1_3( vec3(vUv + timer,1.0) ) > 0.99 || pos.w <= 0.0 ) {\n\n\t\tpos.xyz = texture2D( originTexture, vUv ).xyz;\n\t\tpos.w = opacity;\n\n\t} else {\n\n\t\tif ( pos.w <= 0.0 ) discard;\n\n\t\tfloat x = pos.x + timer * 5.0;\n\t\tfloat y = pos.y;\n\t\tfloat z = pos.z + timer * 4.0;\n\n\n\t\tpos.x += sin( y * 0.035 ) * cos( z * 0.037 ) * test * 2.0;\n\t\tpos.y += sin( x * 0.035 ) * cos( x * 0.035 ) * test * 2.0;\n\t\tpos.z += sin( x * 0.037 ) * cos( y * 0.033 ) * test * 2.0;\n\t\tpos.w -= 0.0001;\n\n\t}\n\n\n\tgl_FragColor = pos;\n\n}", this.renderer);
            var array = new Float32Array(size * size * 4);

            var originsTexture = new THREE.DataTexture(data, size, size, THREE.RGBFormat, THREE.FloatType);
            originsTexture.minFilter = THREE.NearestFilter;
            originsTexture.magFilter = THREE.NearestFilter;
            originsTexture.generateMipmaps = false;
            originsTexture.needsUpdate = true;

            buffer.setData(originsTexture);

            var geometry = new THREE.BufferGeometry();
            geometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array(size * size * 3), 3));

            var positions = geometry.getAttribute('position').array;

            for (var i = 0, j = 0, l = positions.length / 3; i < l; i++, j += 3) {

                positions[j] = i % size / size;
                positions[j + 1] = Math.floor(i / size) / size;
            }

            var particleMaterial = new THREE.ShaderMaterial({

                uniforms: {
                    "map": { type: "t", value: null },
                    "size": { type: "f", value: size },
                    "pointColor": { type: "v3", value: new THREE.Vector3(0.1, 0.25, 0.5) },
                    "spark": {
                        type: 't',
                        value: THREE.ImageUtils.loadTexture("beats_rehash/public/img/spark.png")
                    }

                },
                vertexShader: "#define GLSLIFY 1\n\tuniform sampler2D map;\n\n\t\t\tuniform float size;\n\n\t\t\tvarying vec2 vUv;\n\t\t\tvarying vec3 vPosition;\n\t\t\tvarying float opacity;\n\n\t\t\tvoid main() {\n\n\t\t\t\tvec2 uv = position.xy + vec2( 0.5 / size, 0.5 / size );\n\t\t\t\tvUv = uv;\n\t\t\t\tvec4 data = texture2D( map, uv );\n\n\t\t\t\tvPosition = data.xyz;\n\t\t\t\topacity = data.w;\n\n\t\t\t\tgl_PointSize = 8.0; // data.w * 10.0 + 1.0;\n\t\t\t\tgl_Position = projectionMatrix * modelViewMatrix * vec4( vPosition, 1.0 );\n\n\t\t\t}\n",
                fragmentShader: "#define GLSLIFY 1\nuniform vec3 pointColor;\nuniform sampler2D spark;\nvarying vec3 vPosition;\nvarying float opacity;\nvarying vec2 vUv;\n\n//\n// Description : Array and textureless GLSL 2D/3D/4D simplex\n//               noise functions.\n//      Author : Ian McEwan, Ashima Arts.\n//  Maintainer : ijm\n//     Lastmod : 20110822 (ijm)\n//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.\n//               Distributed under the MIT License. See LICENSE file.\n//               https://github.com/ashima/webgl-noise\n//\n\nvec3 mod289_1_0(vec3 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec4 mod289_1_0(vec4 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec4 permute_1_1(vec4 x) {\n     return mod289_1_0(((x*34.0)+1.0)*x);\n}\n\nvec4 taylorInvSqrt_1_2(vec4 r)\n{\n  return 1.79284291400159 - 0.85373472095314 * r;\n}\n\nfloat snoise_1_3(vec3 v)\n  {\n  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;\n  const vec4  D_1_4 = vec4(0.0, 0.5, 1.0, 2.0);\n\n// First corner\n  vec3 i  = floor(v + dot(v, C.yyy) );\n  vec3 x0 =   v - i + dot(i, C.xxx) ;\n\n// Other corners\n  vec3 g_1_5 = step(x0.yzx, x0.xyz);\n  vec3 l = 1.0 - g_1_5;\n  vec3 i1 = min( g_1_5.xyz, l.zxy );\n  vec3 i2 = max( g_1_5.xyz, l.zxy );\n\n  //   x0 = x0 - 0.0 + 0.0 * C.xxx;\n  //   x1 = x0 - i1  + 1.0 * C.xxx;\n  //   x2 = x0 - i2  + 2.0 * C.xxx;\n  //   x3 = x0 - 1.0 + 3.0 * C.xxx;\n  vec3 x1 = x0 - i1 + C.xxx;\n  vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y\n  vec3 x3 = x0 - D_1_4.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y\n\n// Permutations\n  i = mod289_1_0(i);\n  vec4 p = permute_1_1( permute_1_1( permute_1_1(\n             i.z + vec4(0.0, i1.z, i2.z, 1.0 ))\n           + i.y + vec4(0.0, i1.y, i2.y, 1.0 ))\n           + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));\n\n// Gradients: 7x7 points over a square, mapped onto an octahedron.\n// The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)\n  float n_ = 0.142857142857; // 1.0/7.0\n  vec3  ns = n_ * D_1_4.wyz - D_1_4.xzx;\n\n  vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)\n\n  vec4 x_ = floor(j * ns.z);\n  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)\n\n  vec4 x = x_ *ns.x + ns.yyyy;\n  vec4 y = y_ *ns.x + ns.yyyy;\n  vec4 h = 1.0 - abs(x) - abs(y);\n\n  vec4 b0 = vec4( x.xy, y.xy );\n  vec4 b1 = vec4( x.zw, y.zw );\n\n  //vec4 s0 = vec4(lessThan(b0,0.0))*2.0 - 1.0;\n  //vec4 s1 = vec4(lessThan(b1,0.0))*2.0 - 1.0;\n  vec4 s0 = floor(b0)*2.0 + 1.0;\n  vec4 s1 = floor(b1)*2.0 + 1.0;\n  vec4 sh = -step(h, vec4(0.0));\n\n  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;\n  vec4 a1_1_6 = b1.xzyw + s1.xzyw*sh.zzww ;\n\n  vec3 p0_1_7 = vec3(a0.xy,h.x);\n  vec3 p1 = vec3(a0.zw,h.y);\n  vec3 p2 = vec3(a1_1_6.xy,h.z);\n  vec3 p3 = vec3(a1_1_6.zw,h.w);\n\n//Normalise gradients\n  vec4 norm = taylorInvSqrt_1_2(vec4(dot(p0_1_7,p0_1_7), dot(p1,p1), dot(p2, p2), dot(p3,p3)));\n  p0_1_7 *= norm.x;\n  p1 *= norm.y;\n  p2 *= norm.z;\n  p3 *= norm.w;\n\n// Mix final noise value\n  vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);\n  m = m * m;\n  return 42.0 * dot( m*m, vec4( dot(p0_1_7,x0), dot(p1,x1),\n                                dot(p2,x2), dot(p3,x3) ) );\n  }\n\n\n\nvoid main() {\n\tvec4 tex = texture2D(spark,gl_PointCoord);\n\n\tif ( opacity <= 0.0 ) discard;\n\n\n\tvec4 col = vec4( pointColor + vPosition * 0.005, opacity );\n\n\n\n\tgl_FragColor = tex * col;\n\n}",
                blending: THREE.AdditiveBlending,
                depthWrite: false,
                transparent: true

            });

            this.simulation2 = buffer;
            this.edgeRenderMaterial2 = particleMaterial;
            return new THREE.Points(geometry, particleMaterial);
        }

        /**
         * Build the shape within the edges
         * @returns {THREE.Mesh}
         * @private
         */

    }, {
        key: "_buildShape",
        value: function _buildShape() {
            var geometry = new THREE.IcosahedronGeometry(90, 1);
            var mod = new Tesselate(8);
            for (var i = 0; i < 5; ++i) {
                mod.modify(geometry);
            }
            var numFaces = geometry.faces.length;

            geometry = new THREE.BufferGeometry().fromGeometry(geometry);

            var colors = new Float32Array(numFaces * 3 * 3);
            var displacement = new Float32Array(numFaces * 3 * 3);

            var color = new THREE.Color();

            for (var f = 0; f < numFaces; f++) {

                var index = 9 * f;

                var h = 0.2 * Math.random();
                var s = 0.5 + 0.5 * Math.random();
                var l = 0.5 + 0.5 * Math.random();

                color.setHSL(h, s, l);

                var d = 10 * (0.5 - Math.random());

                for (var i = 0; i < 3; i++) {

                    colors[index + 3 * i] = color.r;
                    colors[index + 3 * i + 1] = color.g;
                    colors[index + 3 * i + 2] = color.b;

                    displacement[index + 3 * i] = d;
                    displacement[index + 3 * i + 1] = d;
                    displacement[index + 3 * i + 2] = d;
                }
            }

            geometry.addAttribute('customColor', new THREE.BufferAttribute(colors, 3));
            geometry.addAttribute('displacement', new THREE.BufferAttribute(displacement, 3));
            var tex = THREE.ImageUtils.loadTexture("/img/test.jpg");
            var mat = new _LinkedShader2.default({
                vertexShader: "#define GLSLIFY 1\nuniform float amplitude;\n\nuniform sampler2D AudioTexture;\nattribute vec3 customColor;\nattribute vec3 displacement;\n\n//varying vec3 vNormal;\nvarying vec3 vColor;\nvarying vec2 vUv;\n\n//\n// Description : Array and textureless GLSL 2D/3D/4D simplex\n//               noise functions.\n//      Author : Ian McEwan, Ashima Arts.\n//  Maintainer : ijm\n//     Lastmod : 20110822 (ijm)\n//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.\n//               Distributed under the MIT License. See LICENSE file.\n//               https://github.com/ashima/webgl-noise\n//\n\nvec3 mod289_1_0(vec3 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec4 mod289_1_0(vec4 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec4 permute_1_1(vec4 x) {\n     return mod289_1_0(((x*34.0)+1.0)*x);\n}\n\nvec4 taylorInvSqrt_1_2(vec4 r)\n{\n  return 1.79284291400159 - 0.85373472095314 * r;\n}\n\nfloat snoise_1_3(vec3 v)\n  {\n  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;\n  const vec4  D_1_4 = vec4(0.0, 0.5, 1.0, 2.0);\n\n// First corner\n  vec3 i  = floor(v + dot(v, C.yyy) );\n  vec3 x0 =   v - i + dot(i, C.xxx) ;\n\n// Other corners\n  vec3 g_1_5 = step(x0.yzx, x0.xyz);\n  vec3 l = 1.0 - g_1_5;\n  vec3 i1 = min( g_1_5.xyz, l.zxy );\n  vec3 i2 = max( g_1_5.xyz, l.zxy );\n\n  //   x0 = x0 - 0.0 + 0.0 * C.xxx;\n  //   x1 = x0 - i1  + 1.0 * C.xxx;\n  //   x2 = x0 - i2  + 2.0 * C.xxx;\n  //   x3 = x0 - 1.0 + 3.0 * C.xxx;\n  vec3 x1 = x0 - i1 + C.xxx;\n  vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y\n  vec3 x3 = x0 - D_1_4.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y\n\n// Permutations\n  i = mod289_1_0(i);\n  vec4 p = permute_1_1( permute_1_1( permute_1_1(\n             i.z + vec4(0.0, i1.z, i2.z, 1.0 ))\n           + i.y + vec4(0.0, i1.y, i2.y, 1.0 ))\n           + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));\n\n// Gradients: 7x7 points over a square, mapped onto an octahedron.\n// The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)\n  float n_ = 0.142857142857; // 1.0/7.0\n  vec3  ns = n_ * D_1_4.wyz - D_1_4.xzx;\n\n  vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)\n\n  vec4 x_ = floor(j * ns.z);\n  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)\n\n  vec4 x = x_ *ns.x + ns.yyyy;\n  vec4 y = y_ *ns.x + ns.yyyy;\n  vec4 h = 1.0 - abs(x) - abs(y);\n\n  vec4 b0 = vec4( x.xy, y.xy );\n  vec4 b1 = vec4( x.zw, y.zw );\n\n  //vec4 s0 = vec4(lessThan(b0,0.0))*2.0 - 1.0;\n  //vec4 s1 = vec4(lessThan(b1,0.0))*2.0 - 1.0;\n  vec4 s0 = floor(b0)*2.0 + 1.0;\n  vec4 s1 = floor(b1)*2.0 + 1.0;\n  vec4 sh = -step(h, vec4(0.0));\n\n  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;\n  vec4 a1_1_6 = b1.xzyw + s1.xzyw*sh.zzww ;\n\n  vec3 p0_1_7 = vec3(a0.xy,h.x);\n  vec3 p1 = vec3(a0.zw,h.y);\n  vec3 p2 = vec3(a1_1_6.xy,h.z);\n  vec3 p3 = vec3(a1_1_6.zw,h.w);\n\n//Normalise gradients\n  vec4 norm = taylorInvSqrt_1_2(vec4(dot(p0_1_7,p0_1_7), dot(p1,p1), dot(p2, p2), dot(p3,p3)));\n  p0_1_7 *= norm.x;\n  p1 *= norm.y;\n  p2 *= norm.z;\n  p3 *= norm.w;\n\n// Mix final noise value\n  vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);\n  m = m * m;\n  return 42.0 * dot( m*m, vec4( dot(p0_1_7,x0), dot(p1,x1),\n                                dot(p2,x2), dot(p3,x3) ) );\n  }\n\n\n\n\nvoid main() {\n\tvUv = uv;\n//\tvNormal = normal;\n\tvColor = customColor;\n\n\tvec4 tex = texture2D(AudioTexture,position.xy);\n\n\tvec3 offset = displacement;\n\n\tfloat audioSample = snoise_1_3(tex.xyz);\n\n\toffset.x *= audioSample;\n\toffset.y *= audioSample;\n\n\tvec3 newPosition = position + normal * amplitude * 2.0 * offset;\n\n\tvec4 pos =  projectionMatrix * modelViewMatrix * vec4( newPosition, 1.0 );\n\n\n\tgl_Position = pos;\n\n}\n\n",
                fragmentShader: "#define GLSLIFY 1\nvarying vec3 vNormal;\nvarying vec3 vColor;\nvarying vec2 vUv;\nuniform float Time;\nuniform sampler2D AudioTexture;\nuniform vec2 resolution;\n\nhighp float random_1_0(vec2 co)\n{\n    highp float a = 12.9898;\n    highp float b = 78.233;\n    highp float c = 43758.5453;\n    highp float dt= dot(co.xy ,vec2(a,b));\n    highp float sn= mod(dt,3.14);\n    return fract(sin(sn) * c);\n}\n\n\n\nvoid main() {\n\n\tconst float ambient = 0.4;\n\n\t   vec2 uv = gl_FragCoord.xy / resolution.xy;\n\n    vec4 tex = texture2D(AudioTexture,gl_PointCoord);\n   vec4 f = vec4(0.);\n\n\n       vec2 u = vUv;\n\n         f.xy = .5 - u;\n\n         float t = Time,\n               z = atan(f.y,tex.y) * 2.,\n               v = cos(z + sin(t * .1)) + .5 + sin(tex.x * 10. + (t * tex.y) * 1.3) * .4;\n\n         f.x = 1.2 + cos(z - t *.2) + sin(u.y * 30.+ t * 1.5) * .5;\n     \tf.yz = vec2( sin(v*4.)*.25, sin(v*2.)*.3 ) + f.x*.5;\n\n     \tf *= mix(f,tex,0.2);\n\n       gl_FragColor = f;\n\n}\n",
                uniforms: {
                    resolution: {
                        type: 'v2',
                        value: this.resolution
                    },
                    amplitude: { type: "f", value: 0.0 },
                    AudioTexture: {
                        type: 't',
                        value: tex
                    },
                    Time: {
                        type: 'f',
                        value: 0.0
                    }
                }
            });
            this.shapeMaterial = mat;
            this.shapeGeometry = geometry;
            console.log(this.shapeMaterial);
            return new THREE.Mesh(geometry, mat);
        }
    }, {
        key: "_buildParticleEdges",
        value: function _buildParticleEdges() {
            var buffer = new _RenderBuffer2.default(1024, "#define GLSLIFY 1\nuniform float opacity;\n\nuniform sampler2D destinationTexture;\nuniform sampler2D originTexture;\nuniform sampler2D audioMap;\n\nuniform float timer;\n\nvarying vec2 vUv;\n\n//\n// Description : Array and textureless GLSL 2D/3D/4D simplex\n//               noise functions.\n//      Author : Ian McEwan, Ashima Arts.\n//  Maintainer : ijm\n//     Lastmod : 20110822 (ijm)\n//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.\n//               Distributed under the MIT License. See LICENSE file.\n//               https://github.com/ashima/webgl-noise\n//\n\nvec3 mod289_1_0(vec3 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec4 mod289_1_0(vec4 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec4 permute_1_1(vec4 x) {\n     return mod289_1_0(((x*34.0)+1.0)*x);\n}\n\nvec4 taylorInvSqrt_1_2(vec4 r)\n{\n  return 1.79284291400159 - 0.85373472095314 * r;\n}\n\nfloat snoise_1_3(vec3 v)\n  {\n  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;\n  const vec4  D_1_4 = vec4(0.0, 0.5, 1.0, 2.0);\n\n// First corner\n  vec3 i  = floor(v + dot(v, C.yyy) );\n  vec3 x0 =   v - i + dot(i, C.xxx) ;\n\n// Other corners\n  vec3 g_1_5 = step(x0.yzx, x0.xyz);\n  vec3 l = 1.0 - g_1_5;\n  vec3 i1 = min( g_1_5.xyz, l.zxy );\n  vec3 i2 = max( g_1_5.xyz, l.zxy );\n\n  //   x0 = x0 - 0.0 + 0.0 * C.xxx;\n  //   x1 = x0 - i1  + 1.0 * C.xxx;\n  //   x2 = x0 - i2  + 2.0 * C.xxx;\n  //   x3 = x0 - 1.0 + 3.0 * C.xxx;\n  vec3 x1 = x0 - i1 + C.xxx;\n  vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y\n  vec3 x3 = x0 - D_1_4.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y\n\n// Permutations\n  i = mod289_1_0(i);\n  vec4 p = permute_1_1( permute_1_1( permute_1_1(\n             i.z + vec4(0.0, i1.z, i2.z, 1.0 ))\n           + i.y + vec4(0.0, i1.y, i2.y, 1.0 ))\n           + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));\n\n// Gradients: 7x7 points over a square, mapped onto an octahedron.\n// The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)\n  float n_ = 0.142857142857; // 1.0/7.0\n  vec3  ns = n_ * D_1_4.wyz - D_1_4.xzx;\n\n  vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)\n\n  vec4 x_ = floor(j * ns.z);\n  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)\n\n  vec4 x = x_ *ns.x + ns.yyyy;\n  vec4 y = y_ *ns.x + ns.yyyy;\n  vec4 h = 1.0 - abs(x) - abs(y);\n\n  vec4 b0 = vec4( x.xy, y.xy );\n  vec4 b1 = vec4( x.zw, y.zw );\n\n  //vec4 s0 = vec4(lessThan(b0,0.0))*2.0 - 1.0;\n  //vec4 s1 = vec4(lessThan(b1,0.0))*2.0 - 1.0;\n  vec4 s0 = floor(b0)*2.0 + 1.0;\n  vec4 s1 = floor(b1)*2.0 + 1.0;\n  vec4 sh = -step(h, vec4(0.0));\n\n  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;\n  vec4 a1_1_6 = b1.xzyw + s1.xzyw*sh.zzww ;\n\n  vec3 p0_1_7 = vec3(a0.xy,h.x);\n  vec3 p1 = vec3(a0.zw,h.y);\n  vec3 p2 = vec3(a1_1_6.xy,h.z);\n  vec3 p3 = vec3(a1_1_6.zw,h.w);\n\n//Normalise gradients\n  vec4 norm = taylorInvSqrt_1_2(vec4(dot(p0_1_7,p0_1_7), dot(p1,p1), dot(p2, p2), dot(p3,p3)));\n  p0_1_7 *= norm.x;\n  p1 *= norm.y;\n  p2 *= norm.z;\n  p3 *= norm.w;\n\n// Mix final noise value\n  vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);\n  m = m * m;\n  return 42.0 * dot( m*m, vec4( dot(p0_1_7,x0), dot(p1,x1),\n                                dot(p2,x2), dot(p3,x3) ) );\n  }\n\n\n\n\nvoid main() {\n\n\tvec4 pos = texture2D( destinationTexture, vUv );\n\tvec4 audio = texture2D(audioMap,vUv);\n\n\tfloat test = snoise_1_3(audio.xyz);\n\n\n\tif ( snoise_1_3( vec3(vUv + timer,1.0) ) > 0.99 || pos.w <= 0.0 ) {\n\n\t\tpos.xyz = texture2D( originTexture, vUv ).xyz;\n\t\tpos.w = opacity;\n\n\t} else {\n\n\t\tif ( pos.w <= 0.0 ) discard;\n\n\t\tfloat x = pos.x + timer * 5.0;\n\t\tfloat y = pos.y;\n\t\tfloat z = pos.z + timer * 4.0;\n\n\n\t\tpos.x += sin( y * 0.035 ) * cos( z * 0.037 ) * test * 2.0;\n\t\tpos.y += sin( x * 0.035 ) * cos( x * 0.035 ) * test * 2.0;\n\t\tpos.z += sin( x * 0.037 ) * cos( y * 0.033 ) * test * 2.0;\n\t\tpos.w -= 0.0001;\n\n\t}\n\n\n\tgl_FragColor = pos;\n\n}", this.renderer, {
                audioMap: {
                    type: 't',
                    value: null
                }
            });

            var size = 1024;
            var array = new Float32Array(size * size * 4);

            var originsTexture = new THREE.DataTexture(this.vertices, size, size, THREE.RGBFormat, THREE.FloatType);
            originsTexture.minFilter = THREE.NearestFilter;
            originsTexture.magFilter = THREE.NearestFilter;
            originsTexture.generateMipmaps = false;
            originsTexture.needsUpdate = true;

            buffer.setData(originsTexture);

            ////// BUILD RENDERABLE STUFF //////
            var geometry = new THREE.BufferGeometry();
            geometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array(size * size * 3), 3));
            geometry.addAttribute('particleSize', new THREE.BufferAttribute(new Float32Array(size * size), 1));

            var positions = geometry.getAttribute('position').array;
            var sizes = geometry.getAttribute('particleSize').array;

            for (var i = 0, j = 0, l = positions.length / 3; i < l; i++, j += 3) {

                positions[j] = i % size / size;
                positions[j + 1] = Math.floor(i / size) / size;
            }

            var particleMaterial = new THREE.ShaderMaterial({

                uniforms: {
                    "audioMap": {
                        type: 't',
                        value: null
                    },
                    "map": { type: "t", value: null },
                    "size": { type: "f", value: size },
                    "spark": {
                        type: 't',
                        value: THREE.ImageUtils.loadTexture("/beats_rehash/public/img/spark.png")
                    },
                    "pointColor": { type: "v3", value: new THREE.Vector3(0.1, 0.25, 0.5) }

                },
                vertexShader: "#define GLSLIFY 1\n\tuniform sampler2D map;\n\n\t\t\tuniform float size;\nuniform sampler2D audioMap;\n\t\t\tvarying vec2 vUv;\n\t\t\tvarying vec3 vPosition;\n\t\t\tvarying float opacity;\n//\n// Description : Array and textureless GLSL 2D/3D/4D simplex\n//               noise functions.\n//      Author : Ian McEwan, Ashima Arts.\n//  Maintainer : ijm\n//     Lastmod : 20110822 (ijm)\n//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.\n//               Distributed under the MIT License. See LICENSE file.\n//               https://github.com/ashima/webgl-noise\n//\n\nvec3 mod289_1_0(vec3 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec4 mod289_1_0(vec4 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec4 permute_1_1(vec4 x) {\n     return mod289_1_0(((x*34.0)+1.0)*x);\n}\n\nvec4 taylorInvSqrt_1_2(vec4 r)\n{\n  return 1.79284291400159 - 0.85373472095314 * r;\n}\n\nfloat snoise_1_3(vec3 v)\n  {\n  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;\n  const vec4  D_1_4 = vec4(0.0, 0.5, 1.0, 2.0);\n\n// First corner\n  vec3 i  = floor(v + dot(v, C.yyy) );\n  vec3 x0 =   v - i + dot(i, C.xxx) ;\n\n// Other corners\n  vec3 g_1_5 = step(x0.yzx, x0.xyz);\n  vec3 l = 1.0 - g_1_5;\n  vec3 i1 = min( g_1_5.xyz, l.zxy );\n  vec3 i2 = max( g_1_5.xyz, l.zxy );\n\n  //   x0 = x0 - 0.0 + 0.0 * C.xxx;\n  //   x1 = x0 - i1  + 1.0 * C.xxx;\n  //   x2 = x0 - i2  + 2.0 * C.xxx;\n  //   x3 = x0 - 1.0 + 3.0 * C.xxx;\n  vec3 x1 = x0 - i1 + C.xxx;\n  vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y\n  vec3 x3 = x0 - D_1_4.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y\n\n// Permutations\n  i = mod289_1_0(i);\n  vec4 p = permute_1_1( permute_1_1( permute_1_1(\n             i.z + vec4(0.0, i1.z, i2.z, 1.0 ))\n           + i.y + vec4(0.0, i1.y, i2.y, 1.0 ))\n           + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));\n\n// Gradients: 7x7 points over a square, mapped onto an octahedron.\n// The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)\n  float n_ = 0.142857142857; // 1.0/7.0\n  vec3  ns = n_ * D_1_4.wyz - D_1_4.xzx;\n\n  vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)\n\n  vec4 x_ = floor(j * ns.z);\n  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)\n\n  vec4 x = x_ *ns.x + ns.yyyy;\n  vec4 y = y_ *ns.x + ns.yyyy;\n  vec4 h = 1.0 - abs(x) - abs(y);\n\n  vec4 b0 = vec4( x.xy, y.xy );\n  vec4 b1 = vec4( x.zw, y.zw );\n\n  //vec4 s0 = vec4(lessThan(b0,0.0))*2.0 - 1.0;\n  //vec4 s1 = vec4(lessThan(b1,0.0))*2.0 - 1.0;\n  vec4 s0 = floor(b0)*2.0 + 1.0;\n  vec4 s1 = floor(b1)*2.0 + 1.0;\n  vec4 sh = -step(h, vec4(0.0));\n\n  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;\n  vec4 a1_1_6 = b1.xzyw + s1.xzyw*sh.zzww ;\n\n  vec3 p0_1_7 = vec3(a0.xy,h.x);\n  vec3 p1 = vec3(a0.zw,h.y);\n  vec3 p2 = vec3(a1_1_6.xy,h.z);\n  vec3 p3 = vec3(a1_1_6.zw,h.w);\n\n//Normalise gradients\n  vec4 norm = taylorInvSqrt_1_2(vec4(dot(p0_1_7,p0_1_7), dot(p1,p1), dot(p2, p2), dot(p3,p3)));\n  p0_1_7 *= norm.x;\n  p1 *= norm.y;\n  p2 *= norm.z;\n  p3 *= norm.w;\n\n// Mix final noise value\n  vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);\n  m = m * m;\n  return 42.0 * dot( m*m, vec4( dot(p0_1_7,x0), dot(p1,x1),\n                                dot(p2,x2), dot(p3,x3) ) );\n  }\n\n\n\n\t\t\tvoid main() {\n\n\n\t\t\t\tvec2 uv = position.xy + vec2( 0.5 / size, 0.5 / size );\n\t\t\t\t\tvec4 audio = texture2D(audioMap,uv);\n\t\t\t\tvUv = uv;\n\t\t\t\tvec4 data = texture2D( map, uv );\n\tfloat audioSample = snoise_1_3(audio.xyz);\n\t\t\t\tvPosition = data.xyz;\n\t\t\t\topacity = data.w;\n\n\t\t\t\tgl_PointSize = audioSample * 40.0;\n\t\t\t\tgl_Position = projectionMatrix * modelViewMatrix * vec4( vPosition, 1.0 );\n\n\t\t\t}\n",
                fragmentShader: "#define GLSLIFY 1\nuniform vec3 pointColor;\nuniform sampler2D spark;\nuniform sampler2D audioMap;\nvarying vec3 vPosition;\nvarying float opacity;\nvarying vec2 vUv;\n\n//\n// Description : Array and textureless GLSL 2D/3D/4D simplex\n//               noise functions.\n//      Author : Ian McEwan, Ashima Arts.\n//  Maintainer : ijm\n//     Lastmod : 20110822 (ijm)\n//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.\n//               Distributed under the MIT License. See LICENSE file.\n//               https://github.com/ashima/webgl-noise\n//\n\nvec3 mod289_1_0(vec3 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec4 mod289_1_0(vec4 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec4 permute_1_1(vec4 x) {\n     return mod289_1_0(((x*34.0)+1.0)*x);\n}\n\nvec4 taylorInvSqrt_1_2(vec4 r)\n{\n  return 1.79284291400159 - 0.85373472095314 * r;\n}\n\nfloat snoise_1_3(vec3 v)\n  {\n  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;\n  const vec4  D_1_4 = vec4(0.0, 0.5, 1.0, 2.0);\n\n// First corner\n  vec3 i  = floor(v + dot(v, C.yyy) );\n  vec3 x0 =   v - i + dot(i, C.xxx) ;\n\n// Other corners\n  vec3 g_1_5 = step(x0.yzx, x0.xyz);\n  vec3 l = 1.0 - g_1_5;\n  vec3 i1 = min( g_1_5.xyz, l.zxy );\n  vec3 i2 = max( g_1_5.xyz, l.zxy );\n\n  //   x0 = x0 - 0.0 + 0.0 * C.xxx;\n  //   x1 = x0 - i1  + 1.0 * C.xxx;\n  //   x2 = x0 - i2  + 2.0 * C.xxx;\n  //   x3 = x0 - 1.0 + 3.0 * C.xxx;\n  vec3 x1 = x0 - i1 + C.xxx;\n  vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y\n  vec3 x3 = x0 - D_1_4.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y\n\n// Permutations\n  i = mod289_1_0(i);\n  vec4 p = permute_1_1( permute_1_1( permute_1_1(\n             i.z + vec4(0.0, i1.z, i2.z, 1.0 ))\n           + i.y + vec4(0.0, i1.y, i2.y, 1.0 ))\n           + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));\n\n// Gradients: 7x7 points over a square, mapped onto an octahedron.\n// The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)\n  float n_ = 0.142857142857; // 1.0/7.0\n  vec3  ns = n_ * D_1_4.wyz - D_1_4.xzx;\n\n  vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)\n\n  vec4 x_ = floor(j * ns.z);\n  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)\n\n  vec4 x = x_ *ns.x + ns.yyyy;\n  vec4 y = y_ *ns.x + ns.yyyy;\n  vec4 h = 1.0 - abs(x) - abs(y);\n\n  vec4 b0 = vec4( x.xy, y.xy );\n  vec4 b1 = vec4( x.zw, y.zw );\n\n  //vec4 s0 = vec4(lessThan(b0,0.0))*2.0 - 1.0;\n  //vec4 s1 = vec4(lessThan(b1,0.0))*2.0 - 1.0;\n  vec4 s0 = floor(b0)*2.0 + 1.0;\n  vec4 s1 = floor(b1)*2.0 + 1.0;\n  vec4 sh = -step(h, vec4(0.0));\n\n  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;\n  vec4 a1_1_6 = b1.xzyw + s1.xzyw*sh.zzww ;\n\n  vec3 p0_1_7 = vec3(a0.xy,h.x);\n  vec3 p1 = vec3(a0.zw,h.y);\n  vec3 p2 = vec3(a1_1_6.xy,h.z);\n  vec3 p3 = vec3(a1_1_6.zw,h.w);\n\n//Normalise gradients\n  vec4 norm = taylorInvSqrt_1_2(vec4(dot(p0_1_7,p0_1_7), dot(p1,p1), dot(p2, p2), dot(p3,p3)));\n  p0_1_7 *= norm.x;\n  p1 *= norm.y;\n  p2 *= norm.z;\n  p3 *= norm.w;\n\n// Mix final noise value\n  vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);\n  m = m * m;\n  return 42.0 * dot( m*m, vec4( dot(p0_1_7,x0), dot(p1,x1),\n                                dot(p2,x2), dot(p3,x3) ) );\n  }\n\n\n\nvoid main() {\n\tvec4 tex = texture2D(spark,gl_PointCoord);\n\tvec4 audio = texture2D(audioMap,gl_PointCoord);\n\tif ( opacity <= 0.0 ) discard;\n\n\tvec3 vertColor = pointColor;\n\tfloat audioSample = snoise_1_3(audio.xyz);\n\tvertColor.x += audioSample;\n\tvertColor.y += audioSample;\n\n\n\tvec4 col = vec4( pointColor + vPosition * 0.005, opacity );\n\n\n\n\tgl_FragColor = tex * col;\n\n}",
                blending: THREE.AdditiveBlending,
                depthWrite: false,
                // depthTest: false,
                transparent: true

            });

            this.simulation = buffer;
            this.edgeRenderMaterial = particleMaterial;
            return new THREE.Points(geometry, particleMaterial);
        }
    }, {
        key: "updateParticleEdges",
        value: function updateParticleEdges() {
            this.simulation.update();
            this.simulation2.update();

            this.edgeRenderMaterial.uniforms.map.value = this.simulation.getOutput();
            this.edgeRenderMaterial2.uniforms.map.value = this.simulation2.getOutput();

            this.edgeRenderMaterial.uniforms.audioMap.value = this.audio.getTexture();
            this.simulation.simulation.uniforms.audioMap.value = this.audio.getTexture();
        }

        /**
         * Builds the edges of the Icohahedron
         * @returns {THREE.Points} returns threejs points
         * @private
         */

    }, {
        key: "_buildEdges",
        value: function _buildEdges() {
            //build the edges mesh
            var lines = new THREE.BufferGeometry();
            var sizes = new Float32Array(this.vertices.length * 3);
            var displacement = new Float32Array(this.vertices.length * 3);

            for (var i = 0; i < sizes.length; ++i) {
                sizes[i] = Math.random() * 5.0;
            }

            for (var a = 0; a < sizes.length; a += 3) {
                displacement[i] = Math.random * 5.0;
                displacement[i] = Math.random * 5.0;
                displacement[i] = Math.random * 5.0;
            }

            var sizeAttribute = new THREE.BufferAttribute(sizes, 1);
            var positionAttribute = new THREE.BufferAttribute(this.vertices, 3);

            lines.addAttribute('position', positionAttribute);
            lines.addAttribute('size', sizeAttribute);

            var mat = new _LinkedShader2.default({
                vertexShader: "#define GLSLIFY 1\nattribute float size;\n\nvarying vec2 vUv;\nvoid main() {\n\n\tvUv = uv;\n\tvec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );\n\n\tgl_PointSize = size * ( 300.0 / length( mvPosition.xyz ) );\n\n\tgl_Position = projectionMatrix * mvPosition;\n\n}",
                fragmentShader: "#define GLSLIFY 1\nuniform float channelSize;\nuniform sampler2D AudioTexture;\nvarying vec2 vUv;\nuniform float Time;\n\n\n\nvoid main(){\n    vec2 uv = vUv;\n    vec4 audio = texture2D(AudioTexture,gl_PointCoord);\n    vec4 f = vec4(0.);\n\n    vec2 u = vUv;\n\n      f.xy = .5 - u;\n\n      float t = Time,\n            z = atan(f.y,f.x) * 2.,\n            v = cos(z + sin(t * .1)) + .5 + sin(u.x * 10. + t * 1.3) * .4;\n\n      f.x = 1.2 + cos(z - t*.2) + sin(u.y*30.+t*1.5)*.5;\n  \tf.yz = vec2( sin(v*4.)*.25, sin(v*2.)*.3 ) + f.x*.5;\n\n    gl_FragColor = audio * f;\n}",
                uniforms: {
                    AudioTexture: {
                        type: 't',
                        value: null
                    },
                    Time: {
                        type: 'f',
                        value: 0.0
                    }
                }

            });

            this.edgeMaterial = mat;
            this.edgeGeometry = lines;
            return new THREE.Points(lines, mat);
        }
    }, {
        key: "update",
        value: function update() {
            this.shape.rotation.x += 0.001;
            this.shape.rotation.y += 0.001;
            this.shape.rotation.z += 0.001;

            this.updateParticleEdges();
            // this.audioTexture.update();
            this.updateEdges();
            this.updateShape();
        }
    }, {
        key: "updateShape",
        value: function updateShape() {
            this.shapeMaterial.setTime(performance.now() * 0.001);
            var time = Date.now() * 0.001;

            if (this.audioTexture !== null) {
                this.shapeMaterial.uniforms.AudioTexture.value = this.audio.getTexture();
            }
            this.shapeMaterial.uniforms.amplitude.value = 4.0 + Math.sin(time);
        }
    }, {
        key: "updateEdges",
        value: function updateEdges() {
            this.edgeMaterial.setTime(performance.now() * 0.001);
            if (this.audioTexture !== null) {
                this.edgeMaterial.uniforms.AudioTexture.value = this.audio.getTexture();
            }
            //this.edgeMaterial.setAudioTexture(this.audioTexture.getTexture());

            //const edgeArr = this.edgeGeometry.getAttribute("size");
            //const len = edgeArr.length;
            //for(var i = 0; i < len; ++i){

            //}
        }
    }]);

    return IsoHedron;
})();

exports.default = IsoHedron;

},{"./AudioTexture.js":111,"./LinkedShader.js":115,"./RenderBuffer.js":116,"src/TesselateModifier":117}],115:[function(require,module,exports){
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var LinkedShader = (function (_THREE$ShaderMaterial) {
    _inherits(LinkedShader, _THREE$ShaderMaterial);

    function LinkedShader(options) {
        _classCallCheck(this, LinkedShader);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(LinkedShader).call(this, options));

        _this._ensureOptions(options);
        _this.connectedToEditor = false;
        return _this;
    }

    _createClass(LinkedShader, [{
        key: "getConnectionStatus",
        value: function getConnectionStatus() {
            return this.connectedToEditor;
        }
    }, {
        key: "_ensureOptions",
        value: function _ensureOptions(options) {
            if (!options.hasOwnProperty("vertexShader")) {
                console.error("LinkedShader created without vertex shader");
            }

            if (!options.hasOwnProperty("fragmentShader")) {
                console.error("LinkedShader created without fragment shader");
            }

            if (options.hasOwnProperty("uniforms")) {
                for (var i in options.uniforms) {
                    this[i] = options.uniforms[i];
                    this.__proto__["set" + i] = function (val) {
                        this[i].value = val;
                    };
                }
            }

            /**
             * Look for the editor websocket server. If no
             * url is provided, try default url search.
             */
            if (options.hasOwnProperty("serverURL")) {
                try {
                    this.socket = new WebSocket(options.serverURL);
                    this.socket.onerror = function () {
                        if (window.debug) {
                            console.error("LinkedShader was unable to connect to the specified editor websocket server");
                        }
                    };
                } catch (e) {
                    if (window.debug) {
                        console.error("LinkedShader was unable to connect to the specified editor websocket server");
                    }
                }
            } else {
                // try {
                //     this.socket = new WebSocket("ws://neditor-default.co");
                //     this.socket.onerror = function(){
                //        if(window.debug){
                //            console.error("LinkedShader was unable to connect to the default editor websocket server");
                //        }
                //     }
                // }catch(e){
                //     if(window.debug){
                //         console.error("LinkedShader was unable to connect to the default editor websocket server");
                //     }
                // }
            }
        }
    }]);

    return LinkedShader;
})(THREE.ShaderMaterial);

exports.default = LinkedShader;

},{}],116:[function(require,module,exports){
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })(); /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        * A class for doing GPU pingponging.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        * Largely based on Isaac Cohen's PhysicsRenderer(https://github.com/cabbibo), cleaned up a bit, and
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        * adding some flags for the future so this can (hopefully) be used interchangeably with
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        * regular WebGL code in addition to Three.js
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        * Requires nitro-glutils
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _GLUtils = require("./GLUtils.js");

var _GLUtils2 = _interopRequireDefault(_GLUtils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _instanceof(left, right) { if (right != null && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var RenderBuffer = (function () {
    function RenderBuffer(size, shader, renderer, additionalUniforms) {
        _classCallCheck(this, RenderBuffer);

        this.renderer = renderer;

        this.size = size !== undefined ? size : 128;
        this.s2 = size * size;

        this.renderer = renderer;

        this.clock = new THREE.Clock();

        this.resolution = new THREE.Vector2(this.size, this.size);

        this.counter = 0;

        this.texturePassProgram = _GLUtils2.default.createPassthru();

        // WHERE THE MAGIC HAPPENS
        var uniforms = {
            originTexture: { type: "t", value: null },
            destinationTexture: { type: "t", value: null },
            resolution: { type: "v2", value: this.resolution }
        };
        if (additionalUniforms !== undefined) {
            for (var i in additionalUniforms) {
                uniforms[i] = additionalUniforms[i];
            }
        }

        this.simulation = _GLUtils2.default.generateShader('passthru', shader, uniforms);

        // Sets up our render targets
        this._buildRenderTargets();

        //build scene for target rendering
        this._buildRenderScene();
    }

    /**
     * Updates the simulation
     */

    _createClass(RenderBuffer, [{
        key: "update",
        value: function update() {
            var flipFlop = this.counter % 3;

            if (flipFlop == 0) {
                this.simulation.uniforms.destinationTexture.value = this.rt_2;
                this.pass(this.simulation, this.rt_3);
            } else if (flipFlop == 1) {
                this.simulation.uniforms.destinationTexture.value = this.rt_3;
                this.pass(this.simulation, this.rt_1);
            } else if (flipFlop == 2) {
                this.simulation.uniforms.destinationTexture.value = this.rt_1;
                this.pass(this.simulation, this.rt_2);
            }

            this.counter++;
        }

        /**
         * Runs a pass against the current target. Sets the output target for use
         * in rendering
         * @param shader the shader to use
         * @param target the target to run the pass on
         *
         * TODO remember to take out the shader param as we don't really need it.
         */

    }, {
        key: "pass",
        value: function pass(shader, target) {

            this.mesh.material = shader;
            this.renderer.render(this.scene, this.camera, target, false);
            this.output = target;
        }

        /**
         * Gets the current output of the simulation.
         * @returns {*}
         */

    }, {
        key: "getOutput",
        value: function getOutput() {
            if (this.output !== undefined) {
                return this.output;
            }
        }

        /**
         * Set a additional uniform variable in the simulation
         * @param uniform name of hte uniform
         * @param value the value for the nuniform
         */

    }, {
        key: "setUniform",
        value: function setUniform(uniform, value) {
            if (THREE) {
                var determineType = function determineType(value) {
                    var instance = null;

                    if (_instanceof(value, THREE.Vector2)) {
                        instance = 'v2';
                    } else if (_instanceof(value, THREE.Vector3)) {
                        instance = 'v3';
                    } else if (_instanceof(value, THREE.Texture)) {
                        instance = 't';
                    } else if (_instanceof(value, THREE.WebGLRenderTarget)) {
                        instance = 't';
                    } else {
                        instance = 'f';
                    }

                    return instance;
                };

                this.simulation.uniforms[uniform] = {
                    type: determineType(value),
                    value: value
                };
            }
        }
    }, {
        key: "updateUniform",
        value: function updateUniform(uniform, value) {
            try {
                this.simulation.uniforms[uniform].value = value;
            } catch (e) {}
        }

        /**
         * Kicks things off with a bit of random data.
         * @param size
         * @param alpha
         */

    }, {
        key: "resetRand",
        value: function resetRand(size, alpha) {

            if (THREE) {
                var size = size || 100;
                var data = new Float32Array(this.s2 * 4);

                for (var i = 0; i < data.length; i++) {

                    //console.log('ss');
                    data[i] = (Math.random() - .5) * size;

                    if (alpha && i % 4 === 3) {
                        data[i] = 0;
                    }
                }

                var texture = new THREE.DataTexture(data, this.size, this.size, THREE.RGBAFormat, THREE.FloatType);

                texture.minFilter = THREE.NearestFilter, texture.magFilter = THREE.NearestFilter, texture.needsUpdate = true;

                this.setData(texture);
            }
        }

        ////////////////// INTERNAL /////////////////////////

        /**
         * Sets the data to be used for the simulation and runs a pass across all the buffers.
         * @param texture
         */

    }, {
        key: "setData",
        value: function setData(texture) {
            this.texture = texture;
            this.texturePassProgram.uniforms.texture.value = texture;

            this.pass(this.texturePassProgram, this.rt_1);
            this.pass(this.texturePassProgram, this.rt_2);
            this.pass(this.texturePassProgram, this.rt_3);
        }
    }, {
        key: "_buildRenderScene",
        value: function _buildRenderScene() {
            if (THREE) {
                var camera = new THREE.OrthographicCamera(-0.5, 0.5, 0.5, -0.5, 0, 1);
                var scene = new THREE.Scene();
                var mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(1, 1));

                var size = this.size;
                scene.add(mesh);

                this.mesh = mesh;
                this.camera = camera;
                this.scene = scene;
            }
        }
    }, {
        key: "_buildRenderTargets",
        value: function _buildRenderTargets() {
            if (THREE) {
                // Sets up our render targets
                this.rt_1 = _GLUtils2.default.generateRenderTarget(this.size, this.size);
                this.rt_2 = this.rt_1.clone();
                this.rt_3 = this.rt_1.clone();
            } else {
                this.rt_1 = this.renderer.createFramebuffer();
                this.rt_2 = this.renderer.createFramebuffer();
                this.rt_3 = this.renderer.createFramebuffer();
            }
        }
    }]);

    return RenderBuffer;
})();

exports.default = RenderBuffer;

},{"./GLUtils.js":113}],117:[function(require,module,exports){
/**
 * Break faces with edges longer than maxEdgeLength
 * - not recursive
 *
 * @author alteredq / http://alteredqualia.com/
 */

THREE.TessellateModifier = function ( maxEdgeLength ) {

    this.maxEdgeLength = maxEdgeLength;

};

THREE.TessellateModifier.prototype.modify = function ( geometry ) {

    var edge;

    var faces = [];
    var faceVertexUvs = [];
    var maxEdgeLengthSquared = this.maxEdgeLength * this.maxEdgeLength;

    for ( var i = 0, il = geometry.faceVertexUvs.length; i < il; i ++ ) {

        faceVertexUvs[ i ] = [];

    }

    for ( var i = 0, il = geometry.faces.length; i < il; i ++ ) {

        var face = geometry.faces[ i ];

        if ( face instanceof THREE.Face3 ) {

            var a = face.a;
            var b = face.b;
            var c = face.c;

            var va = geometry.vertices[ a ];
            var vb = geometry.vertices[ b ];
            var vc = geometry.vertices[ c ];

            var dab = va.distanceToSquared( vb );
            var dbc = vb.distanceToSquared( vc );
            var dac = va.distanceToSquared( vc );

            if ( dab > maxEdgeLengthSquared || dbc > maxEdgeLengthSquared || dac > maxEdgeLengthSquared ) {

                var m = geometry.vertices.length;

                var triA = face.clone();
                var triB = face.clone();

                if ( dab >= dbc && dab >= dac ) {

                    var vm = va.clone();
                    vm.lerp( vb, 0.5 );

                    triA.a = a;
                    triA.b = m;
                    triA.c = c;

                    triB.a = m;
                    triB.b = b;
                    triB.c = c;

                    if ( face.vertexNormals.length === 3 ) {

                        var vnm = face.vertexNormals[ 0 ].clone();
                        vnm.lerp( face.vertexNormals[ 1 ], 0.5 );

                        triA.vertexNormals[ 1 ].copy( vnm );
                        triB.vertexNormals[ 0 ].copy( vnm );

                    }

                    if ( face.vertexColors.length === 3 ) {

                        var vcm = face.vertexColors[ 0 ].clone();
                        vcm.lerp( face.vertexColors[ 1 ], 0.5 );

                        triA.vertexColors[ 1 ].copy( vcm );
                        triB.vertexColors[ 0 ].copy( vcm );

                    }

                    edge = 0;

                } else if ( dbc >= dab && dbc >= dac ) {

                    var vm = vb.clone();
                    vm.lerp( vc, 0.5 );

                    triA.a = a;
                    triA.b = b;
                    triA.c = m;

                    triB.a = m;
                    triB.b = c;
                    triB.c = a;

                    if ( face.vertexNormals.length === 3 ) {

                        var vnm = face.vertexNormals[ 1 ].clone();
                        vnm.lerp( face.vertexNormals[ 2 ], 0.5 );

                        triA.vertexNormals[ 2 ].copy( vnm );

                        triB.vertexNormals[ 0 ].copy( vnm );
                        triB.vertexNormals[ 1 ].copy( face.vertexNormals[ 2 ] );
                        triB.vertexNormals[ 2 ].copy( face.vertexNormals[ 0 ] );

                    }

                    if ( face.vertexColors.length === 3 ) {

                        var vcm = face.vertexColors[ 1 ].clone();
                        vcm.lerp( face.vertexColors[ 2 ], 0.5 );

                        triA.vertexColors[ 2 ].copy( vcm );

                        triB.vertexColors[ 0 ].copy( vcm );
                        triB.vertexColors[ 1 ].copy( face.vertexColors[ 2 ] );
                        triB.vertexColors[ 2 ].copy( face.vertexColors[ 0 ] );

                    }

                    edge = 1;

                } else {

                    var vm = va.clone();
                    vm.lerp( vc, 0.5 );

                    triA.a = a;
                    triA.b = b;
                    triA.c = m;

                    triB.a = m;
                    triB.b = b;
                    triB.c = c;

                    if ( face.vertexNormals.length === 3 ) {

                        var vnm = face.vertexNormals[ 0 ].clone();
                        vnm.lerp( face.vertexNormals[ 2 ], 0.5 );

                        triA.vertexNormals[ 2 ].copy( vnm );
                        triB.vertexNormals[ 0 ].copy( vnm );

                    }

                    if ( face.vertexColors.length === 3 ) {

                        var vcm = face.vertexColors[ 0 ].clone();
                        vcm.lerp( face.vertexColors[ 2 ], 0.5 );

                        triA.vertexColors[ 2 ].copy( vcm );
                        triB.vertexColors[ 0 ].copy( vcm );

                    }

                    edge = 2;

                }

                faces.push( triA, triB );
                geometry.vertices.push( vm );

                for ( var j = 0, jl = geometry.faceVertexUvs.length; j < jl; j ++ ) {

                    if ( geometry.faceVertexUvs[ j ].length ) {

                        var uvs = geometry.faceVertexUvs[ j ][ i ];

                        var uvA = uvs[ 0 ];
                        var uvB = uvs[ 1 ];
                        var uvC = uvs[ 2 ];

                        // AB

                        if ( edge === 0 ) {

                            var uvM = uvA.clone();
                            uvM.lerp( uvB, 0.5 );

                            var uvsTriA = [ uvA.clone(), uvM.clone(), uvC.clone() ];
                            var uvsTriB = [ uvM.clone(), uvB.clone(), uvC.clone() ];

                            // BC

                        } else if ( edge === 1 ) {

                            var uvM = uvB.clone();
                            uvM.lerp( uvC, 0.5 );

                            var uvsTriA = [ uvA.clone(), uvB.clone(), uvM.clone() ];
                            var uvsTriB = [ uvM.clone(), uvC.clone(), uvA.clone() ];

                            // AC

                        } else {

                            var uvM = uvA.clone();
                            uvM.lerp( uvC, 0.5 );

                            var uvsTriA = [ uvA.clone(), uvB.clone(), uvM.clone() ];
                            var uvsTriB = [ uvM.clone(), uvB.clone(), uvC.clone() ];

                        }

                        faceVertexUvs[ j ].push( uvsTriA, uvsTriB );

                    }

                }

            } else {

                faces.push( face );

                for ( var j = 0, jl = geometry.faceVertexUvs.length; j < jl; j ++ ) {

                    faceVertexUvs[ j ].push( geometry.faceVertexUvs[ j ][ i ] );

                }

            }

        }

    }

    geometry.faces = faces;
    geometry.faceVertexUvs = faceVertexUvs;

    return geometry;
};

/**
 * Make all faces use unique vertices
 * so that each face can be separated from others
 *
 * @author alteredq / http://alteredqualia.com/
 */

THREE.ExplodeModifier = function () {

};

THREE.ExplodeModifier.prototype.modify = function ( geometry ) {

    var vertices = [];

    for ( var i = 0, il = geometry.faces.length; i < il; i ++ ) {

        var n = vertices.length;

        var face = geometry.faces[ i ];

        var a = face.a;
        var b = face.b;
        var c = face.c;

        var va = geometry.vertices[ a ];
        var vb = geometry.vertices[ b ];
        var vc = geometry.vertices[ c ];

        vertices.push( va.clone() );
        vertices.push( vb.clone() );
        vertices.push( vc.clone() );

        face.a = n;
        face.b = n + 1;
        face.c = n + 2;

    }

    geometry.vertices = vertices;
    delete geometry.__tmpVertices;

};

module.exports = {
    tesselate:THREE.TessellateModifier,
    explode:THREE.ExplodeModifier
};
},{}],118:[function(require,module,exports){
"use strict";

var _Background = require("./Background.js");

var _Background2 = _interopRequireDefault(_Background);

var _IsoHedron = require("./IsoHedron.js");

var _IsoHedron2 = _interopRequireDefault(_IsoHedron);

var _AudioTexture = require("./AudioTexture.js");

var _AudioTexture2 = _interopRequireDefault(_AudioTexture);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var renderer = new THREE.WebGLRenderer({
    preserveDrawingBuffer: false
});
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75.0, window.innerWidth / window.innerHeight, 1.0, 10000);
camera.position.z = 400;

renderer.setSize(window.innerWidth, window.innerHeight);
renderer.autoClearColor = false;
document.body.appendChild(renderer.domElement);

////// BUILD ELEMENTS //////

//var bg = new Background();
//bg.addTo(scene);

var cube = new THREE.BoxGeometry(100, 100, 100);
var mat = new THREE.ShaderMaterial({
    vertexShader: document.querySelector("#vs-render").textContent,
    fragmentShader: document.querySelector("#fs-render").textContent,
    uniforms: {
        map: {
            type: 't',
            value: null
        }
    }
});

var mesh = new THREE.Mesh(cube, mat);
//scene.add(mesh);

var audio = new _AudioTexture2.default('/beats_rehash/public/mp3s/flickr.mp3');

var iso = new _IsoHedron2.default(renderer);
iso.buildComponents(scene);
iso.setAudio(audio);

window.addEventListener('keydown', function () {
    audio.toggleAudio();
});

////// RENDER ///////
var gl = renderer.getContext();

if (gl.getExtension("OES_texture_float") === null || gl.getParameter(gl.MAX_VERTEX_TEXTURE_IMAGE_UNITS) === 0) {
    alert("This demo requires certain WebGL extentions which your browser and/or computer do not seem to support (◕︵◕) \n" + "please try to view this demo on a more modern computer. ");
} else {
    animate();
}

function animate() {
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
    iso.update();
    audio.update();

    mat.uniforms.map.value = audio.getTexture();
    mesh.rotation.x += 0.01;
    mesh.rotation.y += 0.01;
}

},{"./AudioTexture.js":111,"./Background.js":112,"./IsoHedron.js":114}]},{},[118])


//# sourceMappingURL=bundle.js.map
