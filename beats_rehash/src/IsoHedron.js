import LinkedShader from "./LinkedShader.js"
import RenderBuffer from "./RenderBuffer.js"
import AudioTexture from "./AudioTexture.js"
var modifier = require("src/TesselateModifier");
var Tesselate = modifier.tesselate;
var Explode = modifier.explode;
var glslify = require('glslify');


class IsoHedron {
    constructor(renderer){
        this.renderer = renderer;
        this._extractData();
        this.resolution = new THREE.Vector2(window.innerWidth,window.innerHeight);
        this.faces = [];
        this.secondaryGeometry = new THREE.IcosahedronGeometry( 100, 1 );

    }



    /**
     * Pull face verts from Icosahedron
     * @private
     */
    _extractData(){
        var size = 1024;
        var geometry = new THREE.IcosahedronGeometry( 100, 1 );
      //  var mod = new Tesselate(8);
       // for(var i = 0; i < 5; ++i){
       //     mod.modify(geometry);
       // }
        var facesLength = geometry.faces.length;
        var point = new THREE.Vector3();
        var data = new Float32Array( size * size * 3 );
        for ( var i = 0, l = data.length; i < l; i += 3 ) {

            var face = geometry.faces[ Math.floor( Math.random() * facesLength ) ];

            var vertex1 = geometry.vertices[ face.a ];
            var vertex2 = geometry.vertices[ Math.random() > 0.5 ? face.b : face.c ];

            point.subVectors( vertex2, vertex1 );
            point.multiplyScalar( Math.random() );
            point.add( vertex1 );

            data[ i ] = point.x;
            data[ i + 1 ] = point.y;
            data[ i + 2 ] = point.z;

        }


        this.vertices = data;

    }

    /**
     * set teh audio texture to use
     * @param audio
     */
    setAudio(audio){
        if(audio instanceof AudioTexture){
            this.audio = audio;
        }else{
            console.log("Audio texture needed, object is not audio texture");
        }
    }

    buildComponents(scene){
        this.scene = scene;

        var particleEdges = this._buildParticleEdges();
        var particleEdges2 = this._buildSecondaryParticleLines();
        var edges = this._buildEdges();
        var shape = this._buildShape();


        var group = new THREE.Object3D();
        group.add(particleEdges);
        group.add(particleEdges2);
        group.add(edges);
        group.add(shape);

        this.shape = group;
        scene.add(group);
    }

    _buildSecondaryParticleLines(){
        var size = 256
        var geometry = new THREE.IcosahedronGeometry( 80, 1 );
          var mod = new Tesselate(8);
         for(var i = 0; i < 5; ++i){
             mod.modify(geometry);
         }
        var facesLength = geometry.faces.length;
        var point = new THREE.Vector3();
        var data = new Float32Array( size * size * 3 );
        for ( var i = 0, l = data.length; i < l; i += 3 ) {

            var face = geometry.faces[ Math.floor( Math.random() * facesLength ) ];

            var vertex1 = geometry.vertices[ face.a ];
            var vertex2 = geometry.vertices[ Math.random() > 0.5 ? face.b : face.c ];

            point.subVectors( vertex2, vertex1 );
            point.multiplyScalar( Math.random() );
            point.add( vertex1 );

            data[ i ] = point.x;
            data[ i + 1 ] = point.y;
            data[ i + 2 ] = point.z;

        }
        var buffer = new RenderBuffer(1024,glslify('src/shaders/EdgeSim.glsl'),this.renderer);
        var array = new Float32Array( size * size * 4 );

        var originsTexture = new THREE.DataTexture( data, size, size, THREE.RGBFormat, THREE.FloatType );
        originsTexture.minFilter = THREE.NearestFilter;
        originsTexture.magFilter = THREE.NearestFilter;
        originsTexture.generateMipmaps = false;
        originsTexture.needsUpdate = true;

        buffer.setData(originsTexture);

        var geometry = new THREE.BufferGeometry();
        geometry.addAttribute( 'position', new THREE.BufferAttribute(new Float32Array( size * size * 3 ), 3) );

        var positions = geometry.getAttribute( 'position' ).array;

        for ( var i = 0, j = 0, l = positions.length / 3; i < l; i ++, j += 3 ) {

            positions[ j     ] = ( i % size ) / size;
            positions[ j + 1 ] = Math.floor( i / size ) / size;

        }

        var particleMaterial = new THREE.ShaderMaterial( {

            uniforms: {
                "map": { type: "t", value: null },
                "size": { type: "f", value: size },
                "pointColor": { type: "v3", value: new THREE.Vector3( 0.1, 0.25, 0.5 ) },
                "spark":{
                    type:'t',
                    value:THREE.ImageUtils.loadTexture("/img/spark.png")
                },

            },
            vertexShader: glslify('src/shaders/isorendervert2.glsl'),
            fragmentShader: glslify('src/shaders/isorenderfrag2.glsl'),
            blending: THREE.AdditiveBlending,
            depthWrite: false,
            transparent: true

        } );

        this.simulation2 = buffer;
        this.edgeRenderMaterial2 = particleMaterial;
        return new THREE.Points( geometry, particleMaterial );
    }

    /**
     * Build the shape within the edges
     * @returns {THREE.Mesh}
     * @private
     */
    _buildShape(){
        var geometry = new THREE.IcosahedronGeometry( 90, 1 );
        var mod = new Tesselate(8);
        for(var i = 0; i < 5; ++i){
            mod.modify(geometry);
        }
        var numFaces = geometry.faces.length;

        geometry = new THREE.BufferGeometry().fromGeometry( geometry );

        var colors = new Float32Array( numFaces * 3 * 3 );
        var displacement = new Float32Array( numFaces * 3 * 3 );

        var color = new THREE.Color();

        for ( var f = 0; f < numFaces; f ++ ) {

            var index = 9 * f;

            var h = 0.2 * Math.random();
            var s = 0.5 + 0.5 * Math.random();
            var l = 0.5 + 0.5 * Math.random();

            color.setHSL( h, s, l );

            var d = 10 * ( 0.5 - Math.random() );

            for ( var i = 0; i < 3; i ++ ) {

                colors[ index + ( 3 * i )     ] = color.r;
                colors[ index + ( 3 * i ) + 1 ] = color.g;
                colors[ index + ( 3 * i ) + 2 ] = color.b;

                displacement[ index + ( 3 * i )     ] = d;
                displacement[ index + ( 3 * i ) + 1 ] = d;
                displacement[ index + ( 3 * i ) + 2 ] = d;

            }

        }

        geometry.addAttribute( 'customColor', new THREE.BufferAttribute( colors, 3 ) );
        geometry.addAttribute( 'displacement', new THREE.BufferAttribute( displacement, 3 ) );
        var tex = THREE.ImageUtils.loadTexture("/img/test.jpg");
        var mat = new LinkedShader({
            vertexShader:glslify("src/shaders/shape-vert.glsl"),
            fragmentShader:glslify("src/shaders/shape-frag.glsl"),
            uniforms:{
                resolution:{
                    type:'v2',
                    value:this.resolution
                },
                amplitude: { type: "f", value: 0.0 },
                AudioTexture:{
                    type:'t',
                    value:tex
                },
                Time:{
                    type:'f',
                    value:0.0
                }
            }
        });
        this.shapeMaterial = mat;
        this.shapeGeometry = geometry;
        console.log(this.shapeMaterial)
        return new THREE.Mesh(geometry,mat);

    }


    _buildParticleEdges(){
        var buffer = new RenderBuffer(1024,glslify('src/shaders/EdgeSim.glsl'),this.renderer,{
            audioMap:{
                type:'t',
                value:null
            }
        });


        var size = 1024;
        var array = new Float32Array( size * size * 4 );

        var originsTexture = new THREE.DataTexture( this.vertices, size, size, THREE.RGBFormat, THREE.FloatType );
        originsTexture.minFilter = THREE.NearestFilter;
        originsTexture.magFilter = THREE.NearestFilter;
        originsTexture.generateMipmaps = false;
        originsTexture.needsUpdate = true;

        buffer.setData(originsTexture);

        ////// BUILD RENDERABLE STUFF //////
        var geometry = new THREE.BufferGeometry();
        geometry.addAttribute( 'position', new THREE.BufferAttribute(new Float32Array( size * size * 3 ), 3) );
        geometry.addAttribute( 'particleSize', new THREE.BufferAttribute(new Float32Array( size * size ), 1) );


        var positions = geometry.getAttribute( 'position' ).array;
        var sizes = geometry.getAttribute( 'particleSize' ).array;

        for ( var i = 0, j = 0, l = positions.length / 3; i < l; i ++, j += 3 ) {

            positions[ j     ] = ( i % size ) / size;
            positions[ j + 1 ] = Math.floor( i / size ) / size;

        }


        var particleMaterial = new THREE.ShaderMaterial( {

            uniforms: {
                "audioMap":{
                    type:'t',
                    value:null
                },
                "map": { type: "t", value: null },
                "size": { type: "f", value: size },
                "spark":{
                    type:'t',
                    value:THREE.ImageUtils.loadTexture("/img/spark.png")
                },
                "pointColor": { type: "v3", value: new THREE.Vector3( 0.1, 0.25, 0.5 ) }

            },
            vertexShader: glslify('src/shaders/isorendervert.glsl'),
            fragmentShader: glslify('src/shaders/isorenderfrag.glsl'),
            blending: THREE.AdditiveBlending,
            depthWrite: false,
            // depthTest: false,
            transparent: true

        } );


        this.simulation = buffer;
        this.edgeRenderMaterial = particleMaterial;
        return new THREE.Points( geometry, particleMaterial );

    }

    updateParticleEdges() {
        this.simulation.update();
        this.simulation2.update();

        this.edgeRenderMaterial.uniforms.map.value = this.simulation.getOutput();
        this.edgeRenderMaterial2.uniforms.map.value = this.simulation2.getOutput();

        this.edgeRenderMaterial.uniforms.audioMap.value = this.audio.getTexture();
        this.simulation.simulation.uniforms.audioMap.value = this.audio.getTexture();
    }

    /**
     * Builds the edges of the Icohahedron
     * @returns {THREE.Points} returns threejs points
     * @private
     */
    _buildEdges(){
        //build the edges mesh
        var lines = new THREE.BufferGeometry();
        var sizes = new Float32Array(this.vertices.length * 3);
        var displacement = new Float32Array(this.vertices.length * 3);

        for(var i = 0; i < sizes.length;++i){
            sizes[i] = Math.random() * 5.0;

        }

        for(var a = 0; a < sizes.length; a += 3){
            displacement[i] = Math.random * 5.0;
            displacement[i] = Math.random * 5.0;
            displacement[i] = Math.random * 5.0;
        }

        var sizeAttribute = new THREE.BufferAttribute(sizes,1);
        var positionAttribute = new THREE.BufferAttribute(this.vertices,3);

        lines.addAttribute('position',positionAttribute);
        lines.addAttribute('size',sizeAttribute);

        var mat = new LinkedShader({
            vertexShader:glslify("src/shaders/edgeVert.glsl"),
            fragmentShader:glslify("src/shaders/edgeFrag.glsl"),
            uniforms:{
                AudioTexture:{
                    type:'t',
                    value:null
                },
                Time:{
                    type:'f',
                    value:0.0
                }
            }

        });


        this.edgeMaterial = mat;
        this.edgeGeometry = lines;
        return new THREE.Points(lines,mat);


    }

    update(){
        this.shape.rotation.x += 0.001;
        this.shape.rotation.y += 0.001;
        this.shape.rotation.z += 0.001;


        this.updateParticleEdges();
       // this.audioTexture.update();
        this.updateEdges();
        this.updateShape();
    }

    updateShape(){
        this.shapeMaterial.setTime(performance.now() * 0.001);
        var time = Date.now() * 0.001;

        if(this.audioTexture !== null){
            this.shapeMaterial.uniforms.AudioTexture.value = this.audio.getTexture();
        }
        this.shapeMaterial.uniforms.amplitude.value = (4.0 + Math.sin( time));


    }

    updateEdges(){
        this.edgeMaterial.setTime(performance.now() * 0.001);
        if(this.audioTexture !== null){
            this.edgeMaterial.uniforms.AudioTexture.value = this.audio.getTexture();
        }
        //this.edgeMaterial.setAudioTexture(this.audioTexture.getTexture());

        //const edgeArr = this.edgeGeometry.getAttribute("size");
        //const len = edgeArr.length;
        //for(var i = 0; i < len; ++i){

        //}
    }
}


export default IsoHedron;