var glslify = require('glslify');

class Background {
    constructor(){

    }

    buildStarsBackground(){

    }

    buildDemoBackground(){
        var radius = Math.max(window.innerWidth,window.innerHeight) * 1.05;
        var width = window.innerWidth;
        var height = window.innerHeight;
        var geo = new THREE.PlaneBufferGeometry(window.innerWidth,window.innerHeight);
        var mat = new THREE.ShaderMaterial({
            vertexShader:glslify('src/shaders/passthru.glsl'),
            fragmentShader:glslify('src/shaders/bg-vignette.glsl'),
            uniforms:{
                aspect:{
                    type:'f',
                    value:1.0
                },
                smoothing:{
                    type:'v2',
                    value:new THREE.Vector2(-0.5,1.0)
                },
                noiseAlpha:{
                    type:'f',
                    value:0.9
                },
                offset:{
                    type:'v2',
                    value:new THREE.Vector2(-0.05,-0.15)
                },
                color1:{
                    type:'c',
                    value:new THREE.Color(0xffffff)
                },

                color2:{
                    type:'c',
                    value:new THREE.Color(parseInt("#283844",16))
                },
                scale:{
                    type:'v2',
                    value:new THREE.Vector2( 1/width * radius, 1/height * radius)
                }
            }
        });

        this.mesh = new THREE.Mesh(geo,mat);
        this.geometry = geo;
        this.material = mat;

        return this.mesh;
    }

    addTo(scene){
        //scene.add(this.mesh);
    }

}

export default Background;