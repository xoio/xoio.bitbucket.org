
/**
 * Analyses the audio stream in prep for drawing.
 * utilizes Hugh Kenenedy's web-audio-analyser and adapted from
 * Felix Turner's web audio example.
 *
 * https://www.npmjs.com/package/web-audio-analyser
 * http://www.airtightinteractive.com/demos/js/uberviz/audioanalysis/js/AudioHandler.js
 */
var lodash = require("lodash/object");
var audioAnalyser = require("web-audio-analyser");
class AudioAnalyser {
    constructor(source){

        //create audio element
        var audio = new Audio();
        audio.crossOrigin = 'Anonymous';
        audio.src = source;
        document.body.appendChild(audio);

        //init new web-audio-analyser
        var a = new audioAnalyser(audio);

        //apply properties from web-audio-analyser directly onto the object
        this._applyProps(a);

        //audio element
        this.audio = audio;

        this.avgLevel = 0;
        this.volumeSensitivity = 1;
        this.binCount = this.analyser.frequencyBinCount;
        this.levelsCount = 16; //should be factor of 512
        this.levelBins = Math.floor(this.binCount / this.levelsCount); //number of bins in each level
        this.width = this.analyser.frequencyBinCount / 4;
        this.levelsCount = 16;

        //levels of each frequency - from 0 - 1 . no sound is 0.
        this.levelsData = [];

        //bars - bar data is from 0 - 256 in 512 bins. no sound is 0;
        this.freqByteData = new Uint8Array(this.binCount);

        //waveform - waveform data is from 0-256 for 512 bins. no sound is 128.
        this.timeByteData = new Uint8Array(this.binCount);

        //waveform - from 0 - 1 . no sound is 0.5. Array [binCount]
        this.dataWaveform = [];

        //history of all the levels at various points
        this.levelHistory = [];

        //////// BPM STUFF ///////////
        this.BEAT_HOLD_TIME = 40; //num of frames to hold a beat
        this.BEAT_DECAY_RATE = 0.98;
        this.BEAT_MIN = 0.15; //a volume less than this is no beat

        this.count = 0;
        this.msecsFirst = 0;
        this.msecsPrevious = 0;
        this.msecsAvg = 633; //time between beats (msec)
        this.gotBeat = false;
        this.beatCutOff = 0;
        this.beatTime = 0;
        this.beatHoldTime = 40;
        this.beatDecayRate = 0.97;
        this.bpmStart = 0;
    }

    /**
     * Updates our audio data. Meant to be used within a loop
     */
    update(){
        if(this.isPlaying){
            this.waveform(this.timeByteData);
            this.frequencies(this.freqByteData);

            this.updateWaveform();
            this.normalizeLevels();
        }
    }

    getLevelsArray(){
        return this.levelsData;
    }

    /**
     * Returns the data at the specific frequency
     * @param index
     * @returns {*}
     */
    getChannelData(index){
        return this.levelsData[index];
    }

    getFrequencyData(index){
        return this.freqByteData[index];
    }

    getWaveformData(index){
        return this.dataWaveform[index];
    }

    getLevelHistory(index){
        return this.levelHistory[index];
    }
    ///////////////////////////////

    _applyProps(a){
        lodash.merge(this,a);
        lodash.merge(this.__proto__,a.__proto__);
    }

    play(){

        if(this.isPlaying){
            this.pause();
            this.isPlaying = false;
        }else{

            this.audio.play();
            this.isPlaying = true;
        }


        return this;
    }

    pause(){
        this.audio.pause();
        this.isPlaying = false;
    }

    /**
     * Updates the waveform information
     */
    updateWaveform(){
        for(var i = 0; i < this.binCount; i++) {
            this.dataWaveform[i] = ((this.timeByteData[i] - 128) /128 ) * this.volumeSensitivity;
        }
    }

    beatDetect(){
        if(this.avgLevel > this.beatCutOff && this.avgLevel > this.BEAT_MIN){
            this.gotBeat = true;
            this.beatCutOff = this.avgLevel * 1.1;
            this.beatTime = 0;
        }else {
            if(this.beatTime <= this.beatHoldTime){
                this.beatTime++;
            }else{
                this.beatCutOff *= this.beatDecayRate;
                this.beatCutOff = Math.max(this.beatCutOff,this.BEAT_MIN);
            }
        }
        this.bpmTime = (new Date().getTime() - this.bpmStart) / this.msecsAvg;
    }

    /**
     * Normalize the levels
     */
    normalizeLevels(){
        for(var i = 0; i < this.levelsCount; i++) {
            var sum = 0;
            for(var j = 0; j < this.levelBins; j++) {
                sum += this.freqByteData[(i * this.levelBins) + j];
            }
            this.levelsData[i] = sum / this.levelBins / 256 * this.volumeSensitivity //freqData maxs at 256

            //adjust for the fact that lower levels are percieved more quietly
            //make lower levels smaller
            //levelsData[i] *=  1 + (i/levelsCount)/2;
        }
    }

    getAverageLevel(){
        //GET AVG LEVEL
        var sum = 0;
        for(var j = 0; j < this.levelsCount; j++) {
            sum += this.levelsData[j];
        }

        this.avgLevel = sum / this.levelsCount;
        this.levelHistory.push(this.avgLevel);
        this.levelHistory.shift(1);

    }




}

export default AudioAnalyser;