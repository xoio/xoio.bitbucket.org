import Background from "./Background.js"
import IsoHedron from "./IsoHedron.js"
import AudioTexture from "./AudioTexture.js"

/**
 *  Notes:
 *  Really just me messing around with stuff and wanting to try something like Mr. Doob did with Christmas Experiments 2 years ago
 *  with Sporel (http://christmasexperiments.com/2013/24/experiment.html)
 *
 *  Alot of the code is based on his work, some of it are things
 *  I reconstituted from other folks like Isaac Cohen's PhysicsRenderer (http://cabbi.bo/PhysicsRenderer/)
 *  as well as basing the shader work from @denzen on shadertoy (https://www.shadertoy.com/view/4lB3DG)
 */


var renderer = new THREE.WebGLRenderer({
    preserveDrawingBuffer:false
});
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75.0,window.innerWidth / window.innerHeight, 1.0,10000);
camera.position.z = 400;


renderer.setSize(window.innerWidth,window.innerHeight);
renderer.autoClearColor = false;
document.body.appendChild(renderer.domElement);

////// BUILD ELEMENTS //////

//var bg = new Background();
//bg.addTo(scene);

var cube = new THREE.BoxGeometry(100,100,100);
var mat = new THREE.ShaderMaterial({
    vertexShader:document.querySelector("#vs-render").textContent,
    fragmentShader:document.querySelector("#fs-render").textContent,
    uniforms:{
        map:{
            type:'t',
            value:null
        }
    }
})

var mesh = new THREE.Mesh(cube,mat);
//scene.add(mesh);

var audio = new AudioTexture('/mp3s/flickr.mp3');

var iso = new IsoHedron(renderer);
iso.buildComponents(scene);
iso.setAudio(audio);


window.addEventListener('keydown',function(){
    audio.toggleAudio();
})

////// RENDER ///////
animate();
function animate(){
    requestAnimationFrame(animate);
    renderer.render(scene,camera);
    iso.update();
    audio.update();

    mat.uniforms.map.value = audio.getTexture();
    mesh.rotation.x += 0.01;
    mesh.rotation.y += 0.01;
}
