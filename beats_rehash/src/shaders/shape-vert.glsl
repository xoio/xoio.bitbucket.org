uniform float amplitude;

uniform sampler2D AudioTexture;
attribute vec3 customColor;
attribute vec3 displacement;

//varying vec3 vNormal;
varying vec3 vColor;
varying vec2 vUv;

#pragma glslify: snoise3 = require(glsl-noise/simplex/3d)

void main() {
	vUv = uv;
//	vNormal = normal;
	vColor = customColor;

	vec4 tex = texture2D(AudioTexture,position.xy);

	vec3 offset = displacement;

	float audioSample = snoise3(tex.xyz);

	offset.x *= audioSample;
	offset.y *= audioSample;

	vec3 newPosition = position + normal * amplitude * 2.0 * offset;

	vec4 pos =  projectionMatrix * modelViewMatrix * vec4( newPosition, 1.0 );


	gl_Position = pos;

}

