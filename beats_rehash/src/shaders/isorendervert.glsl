	uniform sampler2D map;

			uniform float size;
uniform sampler2D audioMap;
			varying vec2 vUv;
			varying vec3 vPosition;
			varying float opacity;
#pragma glslify: snoise3 = require(glsl-noise/simplex/3d)
			void main() {


				vec2 uv = position.xy + vec2( 0.5 / size, 0.5 / size );
					vec4 audio = texture2D(audioMap,uv);
				vUv = uv;
				vec4 data = texture2D( map, uv );
	float audioSample = snoise3(audio.xyz);
				vPosition = data.xyz;
				opacity = data.w;

				gl_PointSize = audioSample * 40.0;
				gl_Position = projectionMatrix * modelViewMatrix * vec4( vPosition, 1.0 );

			}
