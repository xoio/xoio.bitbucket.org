uniform vec3 pointColor;
uniform sampler2D spark;
varying vec3 vPosition;
varying float opacity;
varying vec2 vUv;

#pragma glslify: snoise3 = require(glsl-noise/simplex/3d)
void main() {
	vec4 tex = texture2D(spark,gl_PointCoord);

	if ( opacity <= 0.0 ) discard;


	vec4 col = vec4( pointColor + vPosition * 0.005, opacity );



	gl_FragColor = tex * col;

}