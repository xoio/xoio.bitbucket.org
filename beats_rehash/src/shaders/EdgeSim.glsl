uniform float opacity;

uniform sampler2D destinationTexture;
uniform sampler2D originTexture;
uniform sampler2D audioMap;

uniform float timer;

varying vec2 vUv;

#pragma glslify: snoise3 = require(glsl-noise/simplex/3d)

void main() {

	vec4 pos = texture2D( destinationTexture, vUv );
	vec4 audio = texture2D(audioMap,vUv);

	float test = snoise3(audio.xyz);


	if ( snoise3( vec3(vUv + timer,1.0) ) > 0.99 || pos.w <= 0.0 ) {

		pos.xyz = texture2D( originTexture, vUv ).xyz;
		pos.w = opacity;

	} else {

		if ( pos.w <= 0.0 ) discard;

		float x = pos.x + timer * 5.0;
		float y = pos.y;
		float z = pos.z + timer * 4.0;


		pos.x += sin( y * 0.035 ) * cos( z * 0.037 ) * test * 2.0;
		pos.y += sin( x * 0.035 ) * cos( x * 0.035 ) * test * 2.0;
		pos.z += sin( x * 0.037 ) * cos( y * 0.033 ) * test * 2.0;
		pos.w -= 0.0001;

	}


	gl_FragColor = pos;

}