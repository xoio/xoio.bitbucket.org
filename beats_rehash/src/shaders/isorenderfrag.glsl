uniform vec3 pointColor;
uniform sampler2D spark;
uniform sampler2D audioMap;
varying vec3 vPosition;
varying float opacity;
varying vec2 vUv;

#pragma glslify: snoise3 = require(glsl-noise/simplex/3d)
void main() {
	vec4 tex = texture2D(spark,gl_PointCoord);
	vec4 audio = texture2D(audioMap,gl_PointCoord);
	if ( opacity <= 0.0 ) discard;

	vec3 vertColor = pointColor;
	float audioSample = snoise3(audio.xyz);
	vertColor.x += audioSample;
	vertColor.y += audioSample;


	vec4 col = vec4( pointColor + vPosition * 0.005, opacity );



	gl_FragColor = tex * col;

}