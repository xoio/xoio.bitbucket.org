/**
 * Builds a texture that holds audio data from audio files.
 * Should work either with Three.js or regular webgl via stack-gl
 * Holds onto it's own audio and analyser elements
 */
var lodash = require("lodash/object");

import AudioAnalyser from "./AudioAnalyser";

class AudioTexture {
    constructor(audio,gl,options){
        var defaults = {};
        try{
            defaults = {
                format:THREE.RGBAFormat,
                type:THREE.FloatType
            };
        }catch(e){
            console.log("Setting defaults... ",e.message);
        }
        options = options !== undefined ? lodash.merge(options,defaults) : defaults;

        var analyser = new AudioAnalyser(audio);
        options.width = analyser.binCount / 4;

        this.context = gl;

        this.path = audio;
        this.audio = analyser;
        this.options = options;

        var audioData = this.processAudio();
        this.audioDataArray = new Float32Array(options.width * 4);


        try{
            this.texture = new THREE.DataTexture( audioData,audioData.length / 16,1,options.format,options.type);
            this.texture.needsUpdate = true;
        }catch(e){
            console.log("Creating Textures... ",e.message," - building regular webgl texture");
            if(gl === undefined){
                console.error("AudioTexture with path : ",audio," ,needs a gl handle in order to function");
            }else{
                this.texture = this.buildTexture(gl,audioData);
            }
        }

    }

    buildTexture(gl,audioData){
        var tex = this.initTexture(gl);

        var arr = this._buildArray(audioData);
        gl.texImage2D(gl.TEXTURE_2D,0,gl.RGB,arr.width,arr.width,0,gl.RGB,gl.FLOAT,arr.array);
        return tex;
    }

    updateTexture(gl,tex,data){
        //rebind texture
        gl.bindTexture(gl.TEXTURE_2D,tex);
        gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,32,32,0,gl.RGBA,gl.UNSIGNED_BYTE,data);

    }

    _buildArray(arr){
        var texWidth = Math.ceil(Math.sqrt(arr.length / 3));
        var array = new Float32Array(texWidth * texWidth * 3);
        for(var i = 0; i < arr.length; ++i){
            array[i] = arr[i];
        }

        return {
            array:array,
            width:texWidth
        }
    }

    initTexture(handle){
        var gl = handle;
        var tex = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, tex);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

        return tex;

    }


    getLevels(){
        return this.audio.levelsData;
    }

    /**
     * Returns the levels data for the specified chanel.
     * The assumption is a 16 channel setup at the moment.
     * TODO might have to revisit if there are comps with more than 16 channel audio
     * @param index
     * @returns {*}
     */
    getChannelData(index){
        if(index < 16){
            return this.audio.getChannelData(index);
        }else if(index === 16){
            return this.audio.getChannelData(index - 1);
        }else{
            console.error("AudioTexture::getChannelData - specified channel does not exist");
        }
    }

    getTexture(){
        return this.texture;
    }

    /**
     * Returns true/false depending on whether or not the audio
     * is playing.
     * @returns {boolean}
     */
    isPlaying(){
        return this.audio.isPlaying;
    }

    /**
     * Toggles playback.
     * Triggers update as well at the same time
     */
    toggleAudio(){
        this.update();
        this.audio.play();
    }

    update(){
        this.audio.update();

        this.texture.image.data = this.processAudio();
        this.texture.needsUpdate = true;
    }

    /**
     * Binds texture for use in shader
     * @param uniform the uniform location to utilize
     * @param index the texture index to use. Will default to 0 if none is specified.
     */
    bind(uniform,index){
        index = index !== undefined ? index : 0;

        try{
            var gl = this.context;
            gl.activeTexture(gl.TEXTURE0 + index);
            gl.bindTexture(gl.TEXTURE_2D, this.texture);
            gl.uniform1i(uniform,index);
        }catch(e){
            if(THREE !== undefined){
                console.log('For the audio texture leading to - ',this.path,' - this is not the function to use to bind the texture');
            }
        }

    }

    unbind(){
        this.context.bindTexture(gl.TEXTURE_2D,null);
    }


    getTexture(){
        return this.texture;
    }

    processAudio(){
        var options = this.options;
        var audio = this.audio;
        this.audioDataArray = new Float32Array(options.width * 4);

        for(var i = 0; i < options.width;i += 4){
            this.audioDataArray[i] = audio.getChannelData((i / 4));
            this.audioDataArray[i + 1] = audio.getChannelData((i / 4) + 1);
            this.audioDataArray[i + 2] = audio.getChannelData((i / 4) + 2);
            this.audioDataArray[i + 3] = audio.getChannelData((i / 4) + 3);
        }

        return this.audioDataArray;
    }


}

export default AudioTexture;