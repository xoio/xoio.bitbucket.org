
class LinkedShader extends THREE.ShaderMaterial {
    constructor(options){
        super(options);

        this._ensureOptions(options);
        this.connectedToEditor = false;
    }

    getConnectionStatus(){
        return this.connectedToEditor;
    }


    _ensureOptions(options){
        if(!options.hasOwnProperty("vertexShader")){
            console.error("LinkedShader created without vertex shader");
        }

        if(!options.hasOwnProperty("fragmentShader")){
            console.error("LinkedShader created without fragment shader");
        }

        if(options.hasOwnProperty("uniforms")){
            for(var i in options.uniforms){
                this[i] = options.uniforms[i];
                this.__proto__[`set${i}`] = function(val){
                    this[i].value = val;
                }
            }
        }

        /**
         * Look for the editor websocket server. If no
         * url is provided, try default url search.
         */
        if(options.hasOwnProperty("serverURL")){
            try{
                this.socket = new WebSocket(options.serverURL);
                this.socket.onerror = function(){
                   if(window.debug){
                       console.error("LinkedShader was unable to connect to the specified editor websocket server");
                   }
                }
            }catch(e){
                if(window.debug){
                    console.error("LinkedShader was unable to connect to the specified editor websocket server");
                }
            }
        }else{
           // try {
           //     this.socket = new WebSocket("ws://neditor-default.co");
           //     this.socket.onerror = function(){
           //        if(window.debug){
           //            console.error("LinkedShader was unable to connect to the default editor websocket server");
           //        }
           //     }
           // }catch(e){
           //     if(window.debug){
           //         console.error("LinkedShader was unable to connect to the default editor websocket server");
           //     }
           // }
        }
    }


}


export default LinkedShader;