uniform vec2 resolution;
uniform float time;
uniform sampler2D top;
uniform sampler2D bottom;

void main()	{

	vec2 uv = gl_FragCoord.xy / resolution.xy;

	vec4 top = texture2D( top, uv );
	vec4 bottom = texture2D(bottom,uv);


	/**
	 *	Blends two textures together. Oddly much much better than using mix() as you can get full
	 *  brightness / color by adding textures together after multiplying by a weight value.
	 *  http://stackoverflow.com/questions/24356075/glsl-shader-interpolate-between-more-than-two-textures
	 */
	vec4 color = (top * 1.0) + (bottom * 1.0);

	gl_FragColor = color;

}