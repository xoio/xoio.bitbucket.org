uniform vec2 resolution;
uniform float time;
uniform sampler2D cubes;
uniform sampler2D stars;

void main()	{

	vec2 uv = gl_FragCoord.xy / resolution.xy;

	vec3 color = texture2D( cubes, uv ).xyz;


	gl_FragColor = vec4(color,1.0);
	//gl_FragColor = vec4( color, 1.0 );

}