uniform vec2 resolution;
uniform vec3 color;
uniform sampler2D audioTexture;
uniform float time;


void main()	{

    vec2 uv = gl_FragCoord.xy / resolution.xy;
    uv.x -= 1.0;
    vec4 audio = texture2D(audioTexture,uv);
	gl_FragColor = vec4(uv / audio.xy, 5.5 + 0.5 * sin(time),1.0);

}