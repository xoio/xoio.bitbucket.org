uniform vec3 color;
uniform float opacity;
uniform vec2 resolution;
uniform sampler2D audioTexture;
uniform float time;
void main() {

	vec2 uv = gl_FragCoord.xy / resolution.xy;
	vec4 audio = texture2D(audioTexture,uv);

    vec3 cubeColor = color;
    float red = 0.0;
    float green = 0.0;
    float blue = 0.0;

     red += sin( uv.x * cos( time / 15.0 ) * 80.0 ) + cos( uv.y * cos( time / 15.0 ) * 10.0 );
        	red += sin( uv.y * sin( time / 10.0 ) * 40.0 ) + cos( uv.x * sin( time / 25.0 ) * 40.0 );
        	red += sin( uv.x * sin( time / 5.0 ) * 10.0 ) + sin( uv.y * sin( time / 35.0 ) * 80.0 );
        	red *= sin( time / 10.0 ) * 0.5;


     green += sin( uv.x * cos( time / 15.0 ) * 80.0 ) + cos( uv.y * cos( time / 15.0 ) * 10.0 );
           green += sin( uv.y * sin( time / 10.0 ) * 40.0 ) + cos( uv.x * sin( time / 25.0 ) * 40.0 );
           green += sin( uv.x * sin( time / 5.0 ) * 10.0 ) + sin( uv.y * sin( time / 35.0 ) * 80.0 );
           green *= sin( time / 10.0 ) * 0.5;



    blue += sin( uv.x * cos( time / 15.0 ) * 80.0 ) + cos( uv.y * cos( time / 15.0 ) * 10.0 );
           blue += sin( uv.y * sin( time / 10.0 ) * 40.0 ) + cos( uv.x * sin( time / 25.0 ) * 40.0 );
           blue += sin( uv.x * sin( time / 5.0 ) * 10.0 ) + sin( uv.y * sin( time / 35.0 ) * 80.0 );
           blue *= sin( time / 10.0 ) * 0.5;


    cubeColor += vec3(red,green,blue);


    vec4 finalcubeColor = vec4(cubeColor,1.0);
	gl_FragColor = finalcubeColor;

}
