uniform float amplitude;
attribute vec3 displacement;
attribute vec3 customColor;
varying vec3 vColor;

void main() {
	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
}
