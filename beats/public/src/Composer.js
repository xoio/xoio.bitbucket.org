
class Composer {
    constructor(renderer,options){

        //build the options.
        this.options = options !== undefined ? this._buildOptions(options,renderer) : {
            width:720,
            height:720,
            size:720,
            wrapS:THREE.RepeatWrapping,
            wrapT:THREE.RepeatWrapping,
            minFilter:THREE.NearestFilter,
            magFilter:THREE.NearestFilter,
            format:THREE.RGBAFormat,
            stencilBuffer:false
        };

        if(options === undefined){
            this.buildType = "three";
        }

        //size of the texture to store data
        this.size = 720;

        //store size doubled
        this.s2 = this.size * this.size;

        //the processing passes to run on the item
        this.passes = [];

        //reference to the simulation program to apply values to
        this.simulation = null;

        //the uniform in the simulation thats set to get new values
        this.receiver = null;

        //counter to help swap between buffers
        this.counter = 0;

        //global uniforms for every type of pass
        this.gUniforms = {
            timer:{
                type:'f',
                value:0
            }
        }

        //const variable used to figure out which uniform to set the render target to.
        this._renderTargetDestination = "destination";

        if(renderer instanceof THREE.WebGLRenderer){
            this._buildThree(renderer);
        }
    }



    /**
     *  Adds a pass to run onto the stack.
     *  @param name a name for the pass which gets used as a id internally
     *  @param source the fragment shader source for the pass
     *  @param uniforms uniforms that are a part of the shader. Note - the destination that is supposed to be
     *  receiving the calculated texture should have the property "destination" as part of it's object.
     */
    addPass(name,source,uniforms){
        var pass = {};

        if(this.buildType === "three"){
            pass.shader = new THREE.ShaderMaterial({
                vertexShader:this._getPassthru(),
                fragmentShader:source,
                uniforms:uniforms
            });
            for(var i in uniforms){
                if(uniforms[i].hasOwnProperty(this._renderTargetDestination)){
                    pass.destination = pass.shader.uniforms[i];
                }
            }
        }
        this.passes.push(pass);
    }

    /**
     *  Does all the calculatin and updates render targets appropriately.
     *  TODO implement some kind of flag switching so that we don't have to referer to
     *  a specific buffer. Complicated in javascript because !flag converts variable to boolean.
     */
    update(){
        //update global uniforms
        //  this.gUniforms.timer.value = Date.now() * 0.0001;


        for(var i = 0;i<this.passes.length;++i){
            var pass = this.passes[i];

            if ( this.counter % 2 === 0 ) {
                pass.destination.value = this.rt_1;
                this.runPass( this.program, this.rt_2 );
                this.renderShader.uniforms.map.value = this.rt_2;

            } else {
                pass.destination.value = this.rt_2;
                this.runPass( this.program, this.rt_1 );
                this.renderShader.uniforms.map.value = this.rt_1;

            }


            this.counter++;
        }
    }

    /**
     *  Adds a post-processing effect
     *  @param effect the post processing effect to add.
     */
    addPostProcessing(effect){

    }

    //set the simulation shader to use
    setSimulation(shader){
        this.simulation = shader;
    }

    setOutput(shader){
        this.renderShader = shader;
    }

    //runs a pass against the specified target
    runPass(shader,target){
        this.mesh.material = shader;
        this.renderer.render(this.scene,this.camera,target,false);
    }


    ////// THREE JS SPECIFIC STUFF ///////////
    _buildThree(renderer){

        var camera = new THREE.OrthographicCamera( - 0.5, 0.5, 0.5, - 0.5, 0, 1 );
        var scene = new THREE.Scene();
        var mesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( 1, 1 ) );
        var renderer = renderer;
        var size = this.size;
        scene.add(mesh);

        this.renderer = renderer;
        this.mesh = mesh;
        this.camera = camera;
        this.scene = scene;

        var rtTexturePos = new THREE.WebGLRenderTarget( size, size, {
            minFilter: THREE.NearestFilter,
            magFilter: THREE.NearestFilter,
            format: THREE.RGBAFormat,
            type:THREE.FloatType,
            stencilBuffer: false
        });

        var rtTexturePos2 = rtTexturePos.clone();

        this.rt_1 = rtTexturePos;
        this.rt_2 = rtTexturePos2;
    }


    /////////// INTERNAL //////////////
    /**
     *  Build out the options object a little bit more, filing in
     *  any missing attributes with the defaults
     */
    _buildOptions(options,renderer){
        if(renderer instanceof THREE.WebGLRenderer){
            //options for the buffering
            var defaults = {
                width:720,
                height:720,
                wrapS:THREE.RepeatWrapping,
                wrapT:THREE.RepeatWrapping,
                minFilter:THREE.NearestFilter,
                magFilter:THREE.NearestFilter,
                format:THREE.RGBAFormat,
                stencilBuffer:false
            };
            for(var a in defaults){
                if(!options.hasOwnProperty(a)){
                    options[a] = defaults[a];
                }
            }

            this.buildType = "three;"
        }
    }

    /**
     *  Generates a pass-thur vertex shader.
     */
    _getPassthru(){

        //still need to check for build type because
        //Three.js provides some variables when constructing ShaderMaterial(s)
        if(this.buildType === "three"){
            return [
                'varying vec2 vUv;',
                'void main(){',
                'vUv = vec2(uv.x, 1.0 - uv.y);',
                'gl_Position = projectionMatrix * modelViewMatrix * vec4(position.xyz,1.0);',
                '}',
            ].join("\n");
        }
    }

    /**
     *  Tests for the capabilities to use WebGL2
     */
    _canUseWebGL2(){
        const canvas = document.createElement("canvas");
        // Try creating a WebGL 2 context first
        var gl = canvas.getContext( 'webgl2', { antialias: false } );
        if (!gl) {
            gl = canvas.getContext( 'experimental-webgl2', { antialias: false } );
        }
        const isWebGL2 = !!gl;
        if (isWebGL2) {
            console.log("I can haz flag, so WebGL 2 is yes!")
            return gl;
        }else{
            return false;
        }
    }
}

export default Composer;
