/**
 * Builds a texture that holds audio data from audio files.
 * Holds onto it's own audio and analyser elements
 */
var lodash = require("lodash/object");
import AudioAnalyser from "src/audio/AudioAnalyser";

class AudioTexture {
    constructor(audio,options){

        var defaults = {
            format:THREE.RGBAFormat,
            type:THREE.FloatType
        };

        options = options !== undefined ? lodash.merge(options,defaults) : defaults;

        var analyser = new AudioAnalyser(audio);
        options.width = analyser.binCount / 4;


        this.audio = analyser;
        this.options = options;

        var audioData = this.processAudio();
        this.texture = new THREE.DataTexture( audioData,audioData.length / 16,1,options.format,options.type);
        this.texture.needsUpdate = true;


    }

    getLevels(){
        return this.audio.levelsData;
    }

    /**
     * Returns the levels data for the specified chanel.
     * The assumption is a 16 channel setup at the moment.
     * TODO might have to revisit if there are comps with more than 16 channel audio
     * @param index
     * @returns {*}
     */
    getChannelData(index){
        if(index < 16){
            return this.audio.getChannelData(index);
        }else if(index === 16){
            return this.audio.getChannelData(index - 1);
        }else{
            console.error("AudioTexture::getChannelData - specified channel does not exist");
        }
    }


    /**
     * Returns true/false depending on whether or not the audio
     * is playing.
     * @returns {boolean}
     */
    isPlaying(){
        return this.audio.isPlaying;
    }

    /**
     * Toggles playback.
     * Triggers update as well at the same time
     */
    toggleAudio(){
        this.update();
        this.audio.play();
    }

    update(){
        this.audio.update();
        this.texture.image.data = this.processAudio();
        this.texture.needsUpdate = true;
    }

    getTexture(){
        return this.texture;
    }

    processAudio(){
        var options = this.options;
        var audio = this.audio;
        var audioData = new Float32Array(options.width * 4);

        for(var i = 0; i < options.width;i += 4){
            audioData[i] = audio.getChannelData((i / 4));
            audioData[i + 1] = audio.getChannelData((i / 4) + 1);
            audioData[i + 2] = audio.getChannelData((i / 4) + 2);
            audioData[i + 3] = audio.getChannelData((i / 4) + 3);
        }

        return audioData;
    }


}

export default AudioTexture;