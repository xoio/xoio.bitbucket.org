import CubeField from "src/cubes/CubeField"
import FieldGroup from "src/cubes/FieldGroup"
import AudioAnalyser from "src/audio/AudioAnalyser"
import AudioTexture from "src/audio/AudioTexture"
import Wagner from "src/Wagner"



var renderer = new THREE.WebGLRenderer();
var camera = new THREE.PerspectiveCamera(75,window.innerWidth / window.innerHeight, 1.0,1000.0);
camera.position.z = 100;
var scene = new THREE.Scene();


/////////// SETUP ////////////////
renderer.setSize(window.innerWidth,window.innerHeight);
document.body.appendChild(renderer.domElement);

Wagner.vertexShadersPath = '/post/vertex-shaders';
Wagner.fragmentShadersPath = '/post/fragment-shaders';


var composer = new Wagner.Composer(renderer);
composer.setSize(window.innerWidth,window.innerHeight);


var circBlur = new Wagner.FullBoxBlurPass();
circBlur.params.amount = 4;

var toon = new Wagner.NoisePass();

/////////// SET SCENE ////////////////
var group = new FieldGroup();

//top field
var topField = new CubeField(renderer);
topField.setPosition("top");


//bottom field
var bottomField = new CubeField(renderer);
bottomField.setPosition("bottom");

group.addField("top",topField);
group.addField("bottom",bottomField);

var audio = new AudioTexture("mp3/littlesecrets.mp3");

group.connectAudio(audio);

group.addTo(scene);


window.addEventListener("keydown",function(e){
   audio.toggleAudio();
});



/////////// RENDER ////////////////
animate();
function animate(){
    requestAnimationFrame(animate);
    //renderer.render(scene,camera);
    camera.lookAt( scene.position );

    composer.reset();
    composer.render(scene,camera);
    composer.pass(circBlur);
    composer.pass(toon);
    composer.toScreen();


    group.render(renderer);

}

