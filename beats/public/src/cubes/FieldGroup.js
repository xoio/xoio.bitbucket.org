
//renders a group of fields on one plane
import CubeField from "src/cubes/CubeField";
import AudioTexture from "src/audio/AudioTexture"
var glslify = require("glslify");

class FieldGroup {
    constructor(){
        var renderVertex = glslify("src/shaders/passthru.glsl");
        var renderFragment = glslify("src/shaders/fieldgroup.glsl");

        var positionShader = new THREE.ShaderMaterial({
            vertexShader:renderVertex,
            fragmentShader:renderFragment,
            uniforms:{
                fieldTop:{
                    type:'t',
                    value:null
                },

                fieldBottom:{
                    type:'t',
                    value:null
                },

                resolution:{
                    type:'v2',
                    value:new THREE.Vector2(window.innerWidth,window.innerHeight)
                }
            }
        });

        var renderPlane = new THREE.PlaneBufferGeometry(window.innerWidth,window.innerHeight);
        var renderMesh = new THREE.Mesh(renderPlane,positionShader);

        this.shader = positionShader;
        this.renderMesh = renderMesh;

        this._fields = [];
    }

    connectAudio(audioTexture){
        if(audioTexture instanceof AudioTexture){
            this.texture = audioTexture;
            for(var field of this._fields){
                field.field.connectAudio(audioTexture);
            }
        }
    }


    addField(name,field){

        //add a uniform value
        this.shader.uniforms[name] = {
            type:'t',
            value:null
        };

        this._fields.push({
            name:name,
            field:field
        });
    }

    addTo(scene){
        scene.add(this.renderMesh);
    }

    render(renderer){

        for(var field of this._fields){
            field.field.update();
            this.shader.uniforms[field.name].value = field.field.render(renderer);
        }

        this.texture.update();
        this.shader.uniforms.resolution.value.x = window.innerWidth;
        this.shader.uniforms.resolution.value.y = window.innerHeight;
    }


}

export default FieldGroup;