var glslify = require("glslify")
import CubeItem from "src/cubes/CubeItem"
import AudioTexture from "src/audio/AudioTexture"

class CubeField {
    constructor(renderer,width,height,resolution){
        renderer = renderer !== undefined ? renderer :false;
        if(renderer === false){

            console.error("Cube field needs a renderer");
            return false;

        }


        this.width = width !== undefined ? width : 400;
        this.height = height !== undefined ? height : 400;
        this.resolution = resolution !== undefined ? resolution : 48;

        this._buildRenderTarget(renderer);

        //positions that make up the grid
        this.positions = [];

        // CubeItem representations of all the elements in the field
        this.cubes = [];

        //the final grouping of objects
        this.group = new THREE.Object3D();

        //build the geometry for teh field
        this._buildGeometry();

        //build the rendering plane
        this._buildRenderPlane();

    }

    /**
     * Sets the audio texture used to manipulate the field
     */
    connectAudio(audioTexture){
        if(audioTexture instanceof AudioTexture){

            /**
             * Loop through the field and assign one channel to a cube.
             * If we run out of channels(which is likely) duplicate assignments
             * and/or use waveform or history data.
             * @type {Array}
             */
            var cubes = this.cubes;
            var len = cubes.length;

            var maxChannels = 16;
            var count = 0;

            for(var i = 0; i < len;++i){
                if(count < maxChannels){
                    cubes[i].setAudio(audioTexture)
                    cubes[i].setChannel(count);
                    count++;
                }else{
                    count = 0;
                    count++;
                }
            }

            this.audioTexture = audioTexture;

        }else{
            console.error("variable audioTexture is not a instance of AudioTexture")
        }
    }

    /**
     * Updates / renders the field.
     * Meant to be used within a loop
     */
    update(){
        var cubes = this.cubes;
        var len = cubes.length;

        var d = new Date();
        for(var i = 0; i < len;++i){
            var index = cubes[i].getChannel();

           cubes[i].updateSize(this.audioTexture.getChannelData(index));
            //cubes[i].updateSize(performance.now() * 0.000001);
        }
    }

    _buildRenderPlane(){
        var renderVertex = glslify("src/shaders/passthru.glsl");
        var renderFragment = glslify("src/shaders/cubefield.glsl");

        var positionShader = new THREE.ShaderMaterial({
            vertexShader:renderVertex,
            fragmentShader:renderFragment,
            uniforms:{
                cubes:{
                    type:'t',
                    value:null
                },
                stars:{
                    type:'t',
                    value:null
                },
                resolution:{
                    type:'v2',
                    value:new THREE.Vector2()
                }
            }
        });

        var renderPlane = new THREE.PlaneBufferGeometry(window.innerWidth,window.innerHeight);
        var renderMesh = new THREE.Mesh(renderPlane,positionShader);

        this.shader = positionShader;
        this.renderMesh = renderMesh;
    }

    addTo(scene){
        scene.add(this.renderMesh)
    }

    render(renderer){
        var time = Date.now() * 0.001;

        this.camera.rotation.z += 0.001;

        renderer.render(this.scene,this.camera,this.fbo,true);
        //this.shader.uniforms.cubes.value = this.fbo;
        //this.shader.uniforms.resolution.value.x = window.innerWidth;
        //this.shader.uniforms.resolution.value.y = window.innerHeight;

        return this.fbo;
    }

    getFbo(){
        return this.fbo;
    }

    _buildGeometry(){
        var cols = this.width / this.resolution;
        var rows = this.height / this.resolution;

        var vec;
        for(var i = 0; i < cols; ++i){
            for(var j = 0; j < rows; ++j){
                var x = i * this.resolution;
                var y = j * this.resolution;
                vec = new THREE.Vector3(x,y,0);

                this.positions.push(vec);
            }
        }

        for(var i of this.positions){
            var cube = new CubeItem();

            cube.mesh.position.set(i.x,i.y,i.z);
            cube.wireframe.position.set(i.x,i.y,i.z);
            this.cubes.push(cube);
            this.group.add(cube.mesh);
            this.group.add(cube.wireframe);
        }



        this.group.position.x -= this.width / 2;
        this.group.position.y -= this.height / 2;
      //  this.group.position.z += this.width / 2;
        //bottom
        //this.group.rotation.set( -Math.PI/2, 0, 0 );
        //this.group.position.set( -this.width / 2, -25, 200 );

        //left
        //this.group.rotation.set(0,-Math.PI/2,0);
        //this.group.position.set(-200,0,-500 );

        //right
        //this.group.rotation.set(0,-Math.PI/2,0);
        //this.group.position.set(200,0,-500 );

        //top
        //this.group.rotation.set( -Math.PI/2, 0, 0 );
        //this.group.position.set( -this.width / 2, this.height / 2, 200 );

        this.scene.add(this.group);

    }

    /**
     * Positions the field either in the top,left,bottom or right. Can also pass in custom position and rotation
     * @param position
     * @param customRotation
     * @param customPosition
     */
    setPosition(position,customRotation,customPosition){
        switch(position){
            case "left":
                this.group.rotation.set(0,-Math.PI/2,0);
                this.group.position.set(-200,0,-500 );
                break;

            case "right":
                this.group.rotation.set(0,-Math.PI/2,0);
                this.group.position.set(200,0,-500 );
                break;

            case "top":
                this.group.rotation.set( -Math.PI/2, 0, 0 );
                this.group.position.set( -this.width / 2, this.height / 2, 200 );
                break;
            default:
                this.group.rotation.set( -Math.PI/2, 0, 0 );
                this.group.position.set( -this.width / 2, -25, 200 );
                break;
        }



        if(position === undefined){
            this.group.rotation.set( -Math.PI/2, 0, 0 );
            this.group.position.set( -this.width / 2, -25, 200 );
        }

        if(customRotation !== undefined){
            this.group.rotation.set(customRotation[0],customRotation[1],customRotation[2]);
        }

        if(customPosition !== undefined){
            this.group.position.set(customPosition[0],customPosition[1],customPosition[2]);
        }
    }

    _buildRenderTarget(renderer){
        var camera =  new THREE.PerspectiveCamera(75.0, window.innerWidth / window.innerHeight , 1.0, 5000.0);
        camera.position.z = 200;
        camera.position.y += 100;

        //var camera = new THREE.OrthographicCamera( window.innerWidth / - 2, window.innerWidth / 2, window.innerHeight / 2, window.innerHeight / - 2, -10000, 10000 );
        var scene = new THREE.Scene();


        this.renderer = renderer;
        this.camera = camera;
        this.scene = scene;

        this.fbo = new THREE.WebGLRenderTarget( window.innerWidth,window.innerHeight, {
            minFilter: THREE.NearestFilter,
            magFilter: THREE.NearestFilter,
            format: THREE.RGBFormat,
            type:THREE.FloatType
        });
    }

}


export default CubeField;