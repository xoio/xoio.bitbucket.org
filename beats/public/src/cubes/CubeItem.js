
var glslify = require("glslify");
import AudioTexture from "src/audio/AudioTexture"

//  //z scaling should give height
class CubeItem {
    constructor(width,height,depth){
        width = width !== undefined ? width : 20;
        height = height !== undefined ? height : 20;
        depth = depth !== undefined ? depth : 20;

        var box,wirebox = null;
        box = new THREE.BoxGeometry(width,height,depth);
        wirebox = new THREE.BoxGeometry(width + 2,height + 2,depth + 2);

        var color = new THREE.Color();

        color.setHSL(Math.random() * 500 / 500,1.0,0.5);

        var material = new THREE.ShaderMaterial({
            vertexShader:glslify("src/shaders/cube-v.glsl"),
            fragmentShader:glslify("src/shaders/cube.glsl"),
            linewidth:2,
            uniforms:{
                audioTexture:{
                    type:'t',
                    value:null
                },


                color:{
                    type:'v3',
                    value:new THREE.Vector3(color.r,color.g,color.b)
                },

                channelValue:{
                    type:'f',
                    value:0.0
                },

                time:{
                    type:'f',
                    value:0
                },
                resolution:{
                    type:'v2',
                    value:new THREE.Vector2(window.innerWidth,window.innerHeight)
                }
            }
        });

        //var wirematerial = new THREE.MeshBasicMaterial({
        //    color:0xff0000,
        //    wireframe:true,
        //    blending:THREE.AdditiveBlending
        //});

        var wirematerial = new THREE.ShaderMaterial({
            vertexShader:glslify("src/shaders/cubeline-v.glsl"),
            fragmentShader:glslify("src/shaders/cubeline-f.glsl"),
            wireframe:true,
            uniforms:{
                audioTexture:{
                    type:'t',
                    value:null
                },


                color:{
                    type:'v3',
                    value:new THREE.Vector3(color.r,color.g,color.b)
                },

                time:{
                    type:'f',
                    value:0
                },
                resolution:{
                    type:'v2',
                    value:new THREE.Vector2(window.innerWidth,window.innerHeight)
                }
            }
        });

        this.mesh = new THREE.Mesh(box,material,THREE.LineStrip);

        this.wirematerial = wirematerial;
        this.wireframe = new THREE.Mesh(wirebox,wirematerial);


        this.material = material;

        //max height of the bar
        this.maxHeight = 250;

        //make sure maxHeight is normalized
        this.normHeight = new THREE.Vector2(this.maxHeight,0).normalize().x;


        this.resolution = new THREE.Vector2(window.innerWidth,window.innerHeight);

        var instance = this;
        window.addEventListener("resize",function(){
            instance.resolution.x = window.innerWidth;
            instance.resolution.y = window.innerHeight;
        });

        this.channel = 0;
    }

    /**
     * Sets the audiotexture to be used in the Cube's shader
     * @param audioTexture
     */
    setAudio(audioTexture){
        if(audioTexture instanceof AudioTexture){
            this.material.uniforms.audioTexture.value = audioTexture.getTexture();
        }
    }

    /**
     * Sets the channel cube that this cube is listening to
     * @param channel the channel number for the cube
     * @returns {CubeItem}//
     */
    setChannel(channel){
        this.channel = channel;
        return this;
    }

    /**
     * Returns the channel associated with this cube
     * @returns {number|*}
     */
    getChannel(){
        return this.channel;
    }

    /**
     * Scales up the height of the cube like a level indicator
     * @param val
     */
    updateSize(val){

        //pass in time to shader
       // this.material.uniforms.time.value =  Date.now() * 0.001;

        this.material.uniforms.time.value =  performance.now() * 0.01;
        this.material.uniforms.resolution.value = this.resolution;

        this.wirematerial.uniforms.time.value =  performance.now() * 0.01;
        this.wirematerial.uniforms.resolution.value = this.resolution;


        //only update size if value is greater than 0
        if(val > 0){
            this.mesh.scale.z = val * (0.05 * this.maxHeight);
            this.wireframe.scale.z = val * (0.05 * this.maxHeight);
        }
    }

}

export default CubeItem;