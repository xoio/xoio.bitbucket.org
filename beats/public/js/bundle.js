(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Gets the last element of `array`.
 *
 * @static
 * @memberOf _
 * @category Array
 * @param {Array} array The array to query.
 * @returns {*} Returns the last element of `array`.
 * @example
 *
 * _.last([1, 2, 3]);
 * // => 3
 */
function last(array) {
  var length = array ? array.length : 0;
  return length ? array[length - 1] : undefined;
}

module.exports = last;

},{}],2:[function(require,module,exports){
/** Used as the `TypeError` message for "Functions" methods. */
var FUNC_ERROR_TEXT = 'Expected a function';

/* Native method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max;

/**
 * Creates a function that invokes `func` with the `this` binding of the
 * created function and arguments from `start` and beyond provided as an array.
 *
 * **Note:** This method is based on the [rest parameter](https://developer.mozilla.org/Web/JavaScript/Reference/Functions/rest_parameters).
 *
 * @static
 * @memberOf _
 * @category Function
 * @param {Function} func The function to apply a rest parameter to.
 * @param {number} [start=func.length-1] The start position of the rest parameter.
 * @returns {Function} Returns the new function.
 * @example
 *
 * var say = _.restParam(function(what, names) {
 *   return what + ' ' + _.initial(names).join(', ') +
 *     (_.size(names) > 1 ? ', & ' : '') + _.last(names);
 * });
 *
 * say('hello', 'fred', 'barney', 'pebbles');
 * // => 'hello fred, barney, & pebbles'
 */
function restParam(func, start) {
  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  start = nativeMax(start === undefined ? (func.length - 1) : (+start || 0), 0);
  return function() {
    var args = arguments,
        index = -1,
        length = nativeMax(args.length - start, 0),
        rest = Array(length);

    while (++index < length) {
      rest[index] = args[start + index];
    }
    switch (start) {
      case 0: return func.call(this, rest);
      case 1: return func.call(this, args[0], rest);
      case 2: return func.call(this, args[0], args[1], rest);
    }
    var otherArgs = Array(start + 1);
    index = -1;
    while (++index < start) {
      otherArgs[index] = args[index];
    }
    otherArgs[start] = rest;
    return func.apply(this, otherArgs);
  };
}

module.exports = restParam;

},{}],3:[function(require,module,exports){
(function (global){
var cachePush = require('./cachePush'),
    getNative = require('./getNative');

/** Native method references. */
var Set = getNative(global, 'Set');

/* Native method references for those with the same name as other `lodash` methods. */
var nativeCreate = getNative(Object, 'create');

/**
 *
 * Creates a cache object to store unique values.
 *
 * @private
 * @param {Array} [values] The values to cache.
 */
function SetCache(values) {
  var length = values ? values.length : 0;

  this.data = { 'hash': nativeCreate(null), 'set': new Set };
  while (length--) {
    this.push(values[length]);
  }
}

// Add functions to the `Set` cache.
SetCache.prototype.push = cachePush;

module.exports = SetCache;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./cachePush":40,"./getNative":54}],4:[function(require,module,exports){
/**
 * Copies the values of `source` to `array`.
 *
 * @private
 * @param {Array} source The array to copy values from.
 * @param {Array} [array=[]] The array to copy values to.
 * @returns {Array} Returns `array`.
 */
function arrayCopy(source, array) {
  var index = -1,
      length = source.length;

  array || (array = Array(length));
  while (++index < length) {
    array[index] = source[index];
  }
  return array;
}

module.exports = arrayCopy;

},{}],5:[function(require,module,exports){
/**
 * A specialized version of `_.forEach` for arrays without support for callback
 * shorthands and `this` binding.
 *
 * @private
 * @param {Array} array The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns `array`.
 */
function arrayEach(array, iteratee) {
  var index = -1,
      length = array.length;

  while (++index < length) {
    if (iteratee(array[index], index, array) === false) {
      break;
    }
  }
  return array;
}

module.exports = arrayEach;

},{}],6:[function(require,module,exports){
/**
 * A specialized version of `_.map` for arrays without support for callback
 * shorthands and `this` binding.
 *
 * @private
 * @param {Array} array The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the new mapped array.
 */
function arrayMap(array, iteratee) {
  var index = -1,
      length = array.length,
      result = Array(length);

  while (++index < length) {
    result[index] = iteratee(array[index], index, array);
  }
  return result;
}

module.exports = arrayMap;

},{}],7:[function(require,module,exports){
/**
 * Appends the elements of `values` to `array`.
 *
 * @private
 * @param {Array} array The array to modify.
 * @param {Array} values The values to append.
 * @returns {Array} Returns `array`.
 */
function arrayPush(array, values) {
  var index = -1,
      length = values.length,
      offset = array.length;

  while (++index < length) {
    array[offset + index] = values[index];
  }
  return array;
}

module.exports = arrayPush;

},{}],8:[function(require,module,exports){
/**
 * A specialized version of `_.some` for arrays without support for callback
 * shorthands and `this` binding.
 *
 * @private
 * @param {Array} array The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {boolean} Returns `true` if any element passes the predicate check,
 *  else `false`.
 */
function arraySome(array, predicate) {
  var index = -1,
      length = array.length;

  while (++index < length) {
    if (predicate(array[index], index, array)) {
      return true;
    }
  }
  return false;
}

module.exports = arraySome;

},{}],9:[function(require,module,exports){
/**
 * Used by `_.defaults` to customize its `_.assign` use.
 *
 * @private
 * @param {*} objectValue The destination object property value.
 * @param {*} sourceValue The source object property value.
 * @returns {*} Returns the value to assign to the destination object.
 */
function assignDefaults(objectValue, sourceValue) {
  return objectValue === undefined ? sourceValue : objectValue;
}

module.exports = assignDefaults;

},{}],10:[function(require,module,exports){
var keys = require('../object/keys');

/**
 * A specialized version of `_.assign` for customizing assigned values without
 * support for argument juggling, multiple sources, and `this` binding `customizer`
 * functions.
 *
 * @private
 * @param {Object} object The destination object.
 * @param {Object} source The source object.
 * @param {Function} customizer The function to customize assigned values.
 * @returns {Object} Returns `object`.
 */
function assignWith(object, source, customizer) {
  var index = -1,
      props = keys(source),
      length = props.length;

  while (++index < length) {
    var key = props[index],
        value = object[key],
        result = customizer(value, source[key], key, object, source);

    if ((result === result ? (result !== value) : (value === value)) ||
        (value === undefined && !(key in object))) {
      object[key] = result;
    }
  }
  return object;
}

module.exports = assignWith;

},{"../object/keys":93}],11:[function(require,module,exports){
var baseCopy = require('./baseCopy'),
    keys = require('../object/keys');

/**
 * The base implementation of `_.assign` without support for argument juggling,
 * multiple sources, and `customizer` functions.
 *
 * @private
 * @param {Object} object The destination object.
 * @param {Object} source The source object.
 * @returns {Object} Returns `object`.
 */
function baseAssign(object, source) {
  return source == null
    ? object
    : baseCopy(source, keys(source), object);
}

module.exports = baseAssign;

},{"../object/keys":93,"./baseCopy":13}],12:[function(require,module,exports){
var baseMatches = require('./baseMatches'),
    baseMatchesProperty = require('./baseMatchesProperty'),
    bindCallback = require('./bindCallback'),
    identity = require('../utility/identity'),
    property = require('../utility/property');

/**
 * The base implementation of `_.callback` which supports specifying the
 * number of arguments to provide to `func`.
 *
 * @private
 * @param {*} [func=_.identity] The value to convert to a callback.
 * @param {*} [thisArg] The `this` binding of `func`.
 * @param {number} [argCount] The number of arguments to provide to `func`.
 * @returns {Function} Returns the callback.
 */
function baseCallback(func, thisArg, argCount) {
  var type = typeof func;
  if (type == 'function') {
    return thisArg === undefined
      ? func
      : bindCallback(func, thisArg, argCount);
  }
  if (func == null) {
    return identity;
  }
  if (type == 'object') {
    return baseMatches(func);
  }
  return thisArg === undefined
    ? property(func)
    : baseMatchesProperty(func, thisArg);
}

module.exports = baseCallback;

},{"../utility/identity":107,"../utility/property":108,"./baseMatches":29,"./baseMatchesProperty":30,"./bindCallback":38}],13:[function(require,module,exports){
/**
 * Copies properties of `source` to `object`.
 *
 * @private
 * @param {Object} source The object to copy properties from.
 * @param {Array} props The property names to copy.
 * @param {Object} [object={}] The object to copy properties to.
 * @returns {Object} Returns `object`.
 */
function baseCopy(source, props, object) {
  object || (object = {});

  var index = -1,
      length = props.length;

  while (++index < length) {
    var key = props[index];
    object[key] = source[key];
  }
  return object;
}

module.exports = baseCopy;

},{}],14:[function(require,module,exports){
var isObject = require('../lang/isObject');

/**
 * The base implementation of `_.create` without support for assigning
 * properties to the created object.
 *
 * @private
 * @param {Object} prototype The object to inherit from.
 * @returns {Object} Returns the new object.
 */
var baseCreate = (function() {
  function object() {}
  return function(prototype) {
    if (isObject(prototype)) {
      object.prototype = prototype;
      var result = new object;
      object.prototype = undefined;
    }
    return result || {};
  };
}());

module.exports = baseCreate;

},{"../lang/isObject":73}],15:[function(require,module,exports){
var baseIndexOf = require('./baseIndexOf'),
    cacheIndexOf = require('./cacheIndexOf'),
    createCache = require('./createCache');

/** Used as the size to enable large array optimizations. */
var LARGE_ARRAY_SIZE = 200;

/**
 * The base implementation of `_.difference` which accepts a single array
 * of values to exclude.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {Array} values The values to exclude.
 * @returns {Array} Returns the new array of filtered values.
 */
function baseDifference(array, values) {
  var length = array ? array.length : 0,
      result = [];

  if (!length) {
    return result;
  }
  var index = -1,
      indexOf = baseIndexOf,
      isCommon = true,
      cache = (isCommon && values.length >= LARGE_ARRAY_SIZE) ? createCache(values) : null,
      valuesLength = values.length;

  if (cache) {
    indexOf = cacheIndexOf;
    isCommon = false;
    values = cache;
  }
  outer:
  while (++index < length) {
    var value = array[index];

    if (isCommon && value === value) {
      var valuesIndex = valuesLength;
      while (valuesIndex--) {
        if (values[valuesIndex] === value) {
          continue outer;
        }
      }
      result.push(value);
    }
    else if (indexOf(values, value, 0) < 0) {
      result.push(value);
    }
  }
  return result;
}

module.exports = baseDifference;

},{"./baseIndexOf":25,"./cacheIndexOf":39,"./createCache":43}],16:[function(require,module,exports){
/**
 * The base implementation of `_.find`, `_.findLast`, `_.findKey`, and `_.findLastKey`,
 * without support for callback shorthands and `this` binding, which iterates
 * over `collection` using the provided `eachFunc`.
 *
 * @private
 * @param {Array|Object|string} collection The collection to search.
 * @param {Function} predicate The function invoked per iteration.
 * @param {Function} eachFunc The function to iterate over `collection`.
 * @param {boolean} [retKey] Specify returning the key of the found element
 *  instead of the element itself.
 * @returns {*} Returns the found element or its key, else `undefined`.
 */
function baseFind(collection, predicate, eachFunc, retKey) {
  var result;
  eachFunc(collection, function(value, key, collection) {
    if (predicate(value, key, collection)) {
      result = retKey ? key : value;
      return false;
    }
  });
  return result;
}

module.exports = baseFind;

},{}],17:[function(require,module,exports){
var arrayPush = require('./arrayPush'),
    isArguments = require('../lang/isArguments'),
    isArray = require('../lang/isArray'),
    isArrayLike = require('./isArrayLike'),
    isObjectLike = require('./isObjectLike');

/**
 * The base implementation of `_.flatten` with added support for restricting
 * flattening and specifying the start index.
 *
 * @private
 * @param {Array} array The array to flatten.
 * @param {boolean} [isDeep] Specify a deep flatten.
 * @param {boolean} [isStrict] Restrict flattening to arrays-like objects.
 * @param {Array} [result=[]] The initial result value.
 * @returns {Array} Returns the new flattened array.
 */
function baseFlatten(array, isDeep, isStrict, result) {
  result || (result = []);

  var index = -1,
      length = array.length;

  while (++index < length) {
    var value = array[index];
    if (isObjectLike(value) && isArrayLike(value) &&
        (isStrict || isArray(value) || isArguments(value))) {
      if (isDeep) {
        // Recursively flatten arrays (susceptible to call stack limits).
        baseFlatten(value, isDeep, isStrict, result);
      } else {
        arrayPush(result, value);
      }
    } else if (!isStrict) {
      result[result.length] = value;
    }
  }
  return result;
}

module.exports = baseFlatten;

},{"../lang/isArguments":69,"../lang/isArray":70,"./arrayPush":7,"./isArrayLike":56,"./isObjectLike":61}],18:[function(require,module,exports){
var createBaseFor = require('./createBaseFor');

/**
 * The base implementation of `baseForIn` and `baseForOwn` which iterates
 * over `object` properties returned by `keysFunc` invoking `iteratee` for
 * each property. Iteratee functions may exit iteration early by explicitly
 * returning `false`.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @returns {Object} Returns `object`.
 */
var baseFor = createBaseFor();

module.exports = baseFor;

},{"./createBaseFor":42}],19:[function(require,module,exports){
var baseFor = require('./baseFor'),
    keysIn = require('../object/keysIn');

/**
 * The base implementation of `_.forIn` without support for callback
 * shorthands and `this` binding.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Object} Returns `object`.
 */
function baseForIn(object, iteratee) {
  return baseFor(object, iteratee, keysIn);
}

module.exports = baseForIn;

},{"../object/keysIn":94,"./baseFor":18}],20:[function(require,module,exports){
var baseFor = require('./baseFor'),
    keys = require('../object/keys');

/**
 * The base implementation of `_.forOwn` without support for callback
 * shorthands and `this` binding.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Object} Returns `object`.
 */
function baseForOwn(object, iteratee) {
  return baseFor(object, iteratee, keys);
}

module.exports = baseForOwn;

},{"../object/keys":93,"./baseFor":18}],21:[function(require,module,exports){
var baseForRight = require('./baseForRight'),
    keys = require('../object/keys');

/**
 * The base implementation of `_.forOwnRight` without support for callback
 * shorthands and `this` binding.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Object} Returns `object`.
 */
function baseForOwnRight(object, iteratee) {
  return baseForRight(object, iteratee, keys);
}

module.exports = baseForOwnRight;

},{"../object/keys":93,"./baseForRight":22}],22:[function(require,module,exports){
var createBaseFor = require('./createBaseFor');

/**
 * This function is like `baseFor` except that it iterates over properties
 * in the opposite order.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @returns {Object} Returns `object`.
 */
var baseForRight = createBaseFor(true);

module.exports = baseForRight;

},{"./createBaseFor":42}],23:[function(require,module,exports){
var isFunction = require('../lang/isFunction');

/**
 * The base implementation of `_.functions` which creates an array of
 * `object` function property names filtered from those provided.
 *
 * @private
 * @param {Object} object The object to inspect.
 * @param {Array} props The property names to filter.
 * @returns {Array} Returns the new array of filtered property names.
 */
function baseFunctions(object, props) {
  var index = -1,
      length = props.length,
      resIndex = -1,
      result = [];

  while (++index < length) {
    var key = props[index];
    if (isFunction(object[key])) {
      result[++resIndex] = key;
    }
  }
  return result;
}

module.exports = baseFunctions;

},{"../lang/isFunction":71}],24:[function(require,module,exports){
var toObject = require('./toObject');

/**
 * The base implementation of `get` without support for string paths
 * and default values.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array} path The path of the property to get.
 * @param {string} [pathKey] The key representation of path.
 * @returns {*} Returns the resolved value.
 */
function baseGet(object, path, pathKey) {
  if (object == null) {
    return;
  }
  if (pathKey !== undefined && pathKey in toObject(object)) {
    path = [pathKey];
  }
  var index = 0,
      length = path.length;

  while (object != null && index < length) {
    object = object[path[index++]];
  }
  return (index && index == length) ? object : undefined;
}

module.exports = baseGet;

},{"./toObject":67}],25:[function(require,module,exports){
var indexOfNaN = require('./indexOfNaN');

/**
 * The base implementation of `_.indexOf` without support for binary searches.
 *
 * @private
 * @param {Array} array The array to search.
 * @param {*} value The value to search for.
 * @param {number} fromIndex The index to search from.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function baseIndexOf(array, value, fromIndex) {
  if (value !== value) {
    return indexOfNaN(array, fromIndex);
  }
  var index = fromIndex - 1,
      length = array.length;

  while (++index < length) {
    if (array[index] === value) {
      return index;
    }
  }
  return -1;
}

module.exports = baseIndexOf;

},{"./indexOfNaN":55}],26:[function(require,module,exports){
var baseIsEqualDeep = require('./baseIsEqualDeep'),
    isObject = require('../lang/isObject'),
    isObjectLike = require('./isObjectLike');

/**
 * The base implementation of `_.isEqual` without support for `this` binding
 * `customizer` functions.
 *
 * @private
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @param {Function} [customizer] The function to customize comparing values.
 * @param {boolean} [isLoose] Specify performing partial comparisons.
 * @param {Array} [stackA] Tracks traversed `value` objects.
 * @param {Array} [stackB] Tracks traversed `other` objects.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 */
function baseIsEqual(value, other, customizer, isLoose, stackA, stackB) {
  if (value === other) {
    return true;
  }
  if (value == null || other == null || (!isObject(value) && !isObjectLike(other))) {
    return value !== value && other !== other;
  }
  return baseIsEqualDeep(value, other, baseIsEqual, customizer, isLoose, stackA, stackB);
}

module.exports = baseIsEqual;

},{"../lang/isObject":73,"./baseIsEqualDeep":27,"./isObjectLike":61}],27:[function(require,module,exports){
var equalArrays = require('./equalArrays'),
    equalByTag = require('./equalByTag'),
    equalObjects = require('./equalObjects'),
    isArray = require('../lang/isArray'),
    isTypedArray = require('../lang/isTypedArray');

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    objectTag = '[object Object]';

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
 * of values.
 */
var objToString = objectProto.toString;

/**
 * A specialized version of `baseIsEqual` for arrays and objects which performs
 * deep comparisons and tracks traversed objects enabling objects with circular
 * references to be compared.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Function} [customizer] The function to customize comparing objects.
 * @param {boolean} [isLoose] Specify performing partial comparisons.
 * @param {Array} [stackA=[]] Tracks traversed `value` objects.
 * @param {Array} [stackB=[]] Tracks traversed `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function baseIsEqualDeep(object, other, equalFunc, customizer, isLoose, stackA, stackB) {
  var objIsArr = isArray(object),
      othIsArr = isArray(other),
      objTag = arrayTag,
      othTag = arrayTag;

  if (!objIsArr) {
    objTag = objToString.call(object);
    if (objTag == argsTag) {
      objTag = objectTag;
    } else if (objTag != objectTag) {
      objIsArr = isTypedArray(object);
    }
  }
  if (!othIsArr) {
    othTag = objToString.call(other);
    if (othTag == argsTag) {
      othTag = objectTag;
    } else if (othTag != objectTag) {
      othIsArr = isTypedArray(other);
    }
  }
  var objIsObj = objTag == objectTag,
      othIsObj = othTag == objectTag,
      isSameTag = objTag == othTag;

  if (isSameTag && !(objIsArr || objIsObj)) {
    return equalByTag(object, other, objTag);
  }
  if (!isLoose) {
    var objIsWrapped = objIsObj && hasOwnProperty.call(object, '__wrapped__'),
        othIsWrapped = othIsObj && hasOwnProperty.call(other, '__wrapped__');

    if (objIsWrapped || othIsWrapped) {
      return equalFunc(objIsWrapped ? object.value() : object, othIsWrapped ? other.value() : other, customizer, isLoose, stackA, stackB);
    }
  }
  if (!isSameTag) {
    return false;
  }
  // Assume cyclic values are equal.
  // For more information on detecting circular references see https://es5.github.io/#JO.
  stackA || (stackA = []);
  stackB || (stackB = []);

  var length = stackA.length;
  while (length--) {
    if (stackA[length] == object) {
      return stackB[length] == other;
    }
  }
  // Add `object` and `other` to the stack of traversed objects.
  stackA.push(object);
  stackB.push(other);

  var result = (objIsArr ? equalArrays : equalObjects)(object, other, equalFunc, customizer, isLoose, stackA, stackB);

  stackA.pop();
  stackB.pop();

  return result;
}

module.exports = baseIsEqualDeep;

},{"../lang/isArray":70,"../lang/isTypedArray":75,"./equalArrays":49,"./equalByTag":50,"./equalObjects":51}],28:[function(require,module,exports){
var baseIsEqual = require('./baseIsEqual'),
    toObject = require('./toObject');

/**
 * The base implementation of `_.isMatch` without support for callback
 * shorthands and `this` binding.
 *
 * @private
 * @param {Object} object The object to inspect.
 * @param {Array} matchData The propery names, values, and compare flags to match.
 * @param {Function} [customizer] The function to customize comparing objects.
 * @returns {boolean} Returns `true` if `object` is a match, else `false`.
 */
function baseIsMatch(object, matchData, customizer) {
  var index = matchData.length,
      length = index,
      noCustomizer = !customizer;

  if (object == null) {
    return !length;
  }
  object = toObject(object);
  while (index--) {
    var data = matchData[index];
    if ((noCustomizer && data[2])
          ? data[1] !== object[data[0]]
          : !(data[0] in object)
        ) {
      return false;
    }
  }
  while (++index < length) {
    data = matchData[index];
    var key = data[0],
        objValue = object[key],
        srcValue = data[1];

    if (noCustomizer && data[2]) {
      if (objValue === undefined && !(key in object)) {
        return false;
      }
    } else {
      var result = customizer ? customizer(objValue, srcValue, key) : undefined;
      if (!(result === undefined ? baseIsEqual(srcValue, objValue, customizer, true) : result)) {
        return false;
      }
    }
  }
  return true;
}

module.exports = baseIsMatch;

},{"./baseIsEqual":26,"./toObject":67}],29:[function(require,module,exports){
var baseIsMatch = require('./baseIsMatch'),
    getMatchData = require('./getMatchData'),
    toObject = require('./toObject');

/**
 * The base implementation of `_.matches` which does not clone `source`.
 *
 * @private
 * @param {Object} source The object of property values to match.
 * @returns {Function} Returns the new function.
 */
function baseMatches(source) {
  var matchData = getMatchData(source);
  if (matchData.length == 1 && matchData[0][2]) {
    var key = matchData[0][0],
        value = matchData[0][1];

    return function(object) {
      if (object == null) {
        return false;
      }
      return object[key] === value && (value !== undefined || (key in toObject(object)));
    };
  }
  return function(object) {
    return baseIsMatch(object, matchData);
  };
}

module.exports = baseMatches;

},{"./baseIsMatch":28,"./getMatchData":53,"./toObject":67}],30:[function(require,module,exports){
var baseGet = require('./baseGet'),
    baseIsEqual = require('./baseIsEqual'),
    baseSlice = require('./baseSlice'),
    isArray = require('../lang/isArray'),
    isKey = require('./isKey'),
    isStrictComparable = require('./isStrictComparable'),
    last = require('../array/last'),
    toObject = require('./toObject'),
    toPath = require('./toPath');

/**
 * The base implementation of `_.matchesProperty` which does not clone `srcValue`.
 *
 * @private
 * @param {string} path The path of the property to get.
 * @param {*} srcValue The value to compare.
 * @returns {Function} Returns the new function.
 */
function baseMatchesProperty(path, srcValue) {
  var isArr = isArray(path),
      isCommon = isKey(path) && isStrictComparable(srcValue),
      pathKey = (path + '');

  path = toPath(path);
  return function(object) {
    if (object == null) {
      return false;
    }
    var key = pathKey;
    object = toObject(object);
    if ((isArr || !isCommon) && !(key in object)) {
      object = path.length == 1 ? object : baseGet(object, baseSlice(path, 0, -1));
      if (object == null) {
        return false;
      }
      key = last(path);
      object = toObject(object);
    }
    return object[key] === srcValue
      ? (srcValue !== undefined || (key in object))
      : baseIsEqual(srcValue, object[key], undefined, true);
  };
}

module.exports = baseMatchesProperty;

},{"../array/last":1,"../lang/isArray":70,"./baseGet":24,"./baseIsEqual":26,"./baseSlice":35,"./isKey":59,"./isStrictComparable":62,"./toObject":67,"./toPath":68}],31:[function(require,module,exports){
var arrayEach = require('./arrayEach'),
    baseMergeDeep = require('./baseMergeDeep'),
    isArray = require('../lang/isArray'),
    isArrayLike = require('./isArrayLike'),
    isObject = require('../lang/isObject'),
    isObjectLike = require('./isObjectLike'),
    isTypedArray = require('../lang/isTypedArray'),
    keys = require('../object/keys');

/**
 * The base implementation of `_.merge` without support for argument juggling,
 * multiple sources, and `this` binding `customizer` functions.
 *
 * @private
 * @param {Object} object The destination object.
 * @param {Object} source The source object.
 * @param {Function} [customizer] The function to customize merged values.
 * @param {Array} [stackA=[]] Tracks traversed source objects.
 * @param {Array} [stackB=[]] Associates values with source counterparts.
 * @returns {Object} Returns `object`.
 */
function baseMerge(object, source, customizer, stackA, stackB) {
  if (!isObject(object)) {
    return object;
  }
  var isSrcArr = isArrayLike(source) && (isArray(source) || isTypedArray(source)),
      props = isSrcArr ? undefined : keys(source);

  arrayEach(props || source, function(srcValue, key) {
    if (props) {
      key = srcValue;
      srcValue = source[key];
    }
    if (isObjectLike(srcValue)) {
      stackA || (stackA = []);
      stackB || (stackB = []);
      baseMergeDeep(object, source, key, baseMerge, customizer, stackA, stackB);
    }
    else {
      var value = object[key],
          result = customizer ? customizer(value, srcValue, key, object, source) : undefined,
          isCommon = result === undefined;

      if (isCommon) {
        result = srcValue;
      }
      if ((result !== undefined || (isSrcArr && !(key in object))) &&
          (isCommon || (result === result ? (result !== value) : (value === value)))) {
        object[key] = result;
      }
    }
  });
  return object;
}

module.exports = baseMerge;

},{"../lang/isArray":70,"../lang/isObject":73,"../lang/isTypedArray":75,"../object/keys":93,"./arrayEach":5,"./baseMergeDeep":32,"./isArrayLike":56,"./isObjectLike":61}],32:[function(require,module,exports){
var arrayCopy = require('./arrayCopy'),
    isArguments = require('../lang/isArguments'),
    isArray = require('../lang/isArray'),
    isArrayLike = require('./isArrayLike'),
    isPlainObject = require('../lang/isPlainObject'),
    isTypedArray = require('../lang/isTypedArray'),
    toPlainObject = require('../lang/toPlainObject');

/**
 * A specialized version of `baseMerge` for arrays and objects which performs
 * deep merges and tracks traversed objects enabling objects with circular
 * references to be merged.
 *
 * @private
 * @param {Object} object The destination object.
 * @param {Object} source The source object.
 * @param {string} key The key of the value to merge.
 * @param {Function} mergeFunc The function to merge values.
 * @param {Function} [customizer] The function to customize merged values.
 * @param {Array} [stackA=[]] Tracks traversed source objects.
 * @param {Array} [stackB=[]] Associates values with source counterparts.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function baseMergeDeep(object, source, key, mergeFunc, customizer, stackA, stackB) {
  var length = stackA.length,
      srcValue = source[key];

  while (length--) {
    if (stackA[length] == srcValue) {
      object[key] = stackB[length];
      return;
    }
  }
  var value = object[key],
      result = customizer ? customizer(value, srcValue, key, object, source) : undefined,
      isCommon = result === undefined;

  if (isCommon) {
    result = srcValue;
    if (isArrayLike(srcValue) && (isArray(srcValue) || isTypedArray(srcValue))) {
      result = isArray(value)
        ? value
        : (isArrayLike(value) ? arrayCopy(value) : []);
    }
    else if (isPlainObject(srcValue) || isArguments(srcValue)) {
      result = isArguments(value)
        ? toPlainObject(value)
        : (isPlainObject(value) ? value : {});
    }
    else {
      isCommon = false;
    }
  }
  // Add the source value to the stack of traversed objects and associate
  // it with its merged value.
  stackA.push(srcValue);
  stackB.push(result);

  if (isCommon) {
    // Recursively merge objects and arrays (susceptible to call stack limits).
    object[key] = mergeFunc(result, srcValue, customizer, stackA, stackB);
  } else if (result === result ? (result !== value) : (value === value)) {
    object[key] = result;
  }
}

module.exports = baseMergeDeep;

},{"../lang/isArguments":69,"../lang/isArray":70,"../lang/isPlainObject":74,"../lang/isTypedArray":75,"../lang/toPlainObject":76,"./arrayCopy":4,"./isArrayLike":56}],33:[function(require,module,exports){
/**
 * The base implementation of `_.property` without support for deep paths.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @returns {Function} Returns the new function.
 */
function baseProperty(key) {
  return function(object) {
    return object == null ? undefined : object[key];
  };
}

module.exports = baseProperty;

},{}],34:[function(require,module,exports){
var baseGet = require('./baseGet'),
    toPath = require('./toPath');

/**
 * A specialized version of `baseProperty` which supports deep paths.
 *
 * @private
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new function.
 */
function basePropertyDeep(path) {
  var pathKey = (path + '');
  path = toPath(path);
  return function(object) {
    return baseGet(object, path, pathKey);
  };
}

module.exports = basePropertyDeep;

},{"./baseGet":24,"./toPath":68}],35:[function(require,module,exports){
/**
 * The base implementation of `_.slice` without an iteratee call guard.
 *
 * @private
 * @param {Array} array The array to slice.
 * @param {number} [start=0] The start position.
 * @param {number} [end=array.length] The end position.
 * @returns {Array} Returns the slice of `array`.
 */
function baseSlice(array, start, end) {
  var index = -1,
      length = array.length;

  start = start == null ? 0 : (+start || 0);
  if (start < 0) {
    start = -start > length ? 0 : (length + start);
  }
  end = (end === undefined || end > length) ? length : (+end || 0);
  if (end < 0) {
    end += length;
  }
  length = start > end ? 0 : ((end - start) >>> 0);
  start >>>= 0;

  var result = Array(length);
  while (++index < length) {
    result[index] = array[index + start];
  }
  return result;
}

module.exports = baseSlice;

},{}],36:[function(require,module,exports){
/**
 * Converts `value` to a string if it's not one. An empty string is returned
 * for `null` or `undefined` values.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {string} Returns the string.
 */
function baseToString(value) {
  return value == null ? '' : (value + '');
}

module.exports = baseToString;

},{}],37:[function(require,module,exports){
/**
 * The base implementation of `_.values` and `_.valuesIn` which creates an
 * array of `object` property values corresponding to the property names
 * of `props`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array} props The property names to get values for.
 * @returns {Object} Returns the array of property values.
 */
function baseValues(object, props) {
  var index = -1,
      length = props.length,
      result = Array(length);

  while (++index < length) {
    result[index] = object[props[index]];
  }
  return result;
}

module.exports = baseValues;

},{}],38:[function(require,module,exports){
var identity = require('../utility/identity');

/**
 * A specialized version of `baseCallback` which only supports `this` binding
 * and specifying the number of arguments to provide to `func`.
 *
 * @private
 * @param {Function} func The function to bind.
 * @param {*} thisArg The `this` binding of `func`.
 * @param {number} [argCount] The number of arguments to provide to `func`.
 * @returns {Function} Returns the callback.
 */
function bindCallback(func, thisArg, argCount) {
  if (typeof func != 'function') {
    return identity;
  }
  if (thisArg === undefined) {
    return func;
  }
  switch (argCount) {
    case 1: return function(value) {
      return func.call(thisArg, value);
    };
    case 3: return function(value, index, collection) {
      return func.call(thisArg, value, index, collection);
    };
    case 4: return function(accumulator, value, index, collection) {
      return func.call(thisArg, accumulator, value, index, collection);
    };
    case 5: return function(value, other, key, object, source) {
      return func.call(thisArg, value, other, key, object, source);
    };
  }
  return function() {
    return func.apply(thisArg, arguments);
  };
}

module.exports = bindCallback;

},{"../utility/identity":107}],39:[function(require,module,exports){
var isObject = require('../lang/isObject');

/**
 * Checks if `value` is in `cache` mimicking the return signature of
 * `_.indexOf` by returning `0` if the value is found, else `-1`.
 *
 * @private
 * @param {Object} cache The cache to search.
 * @param {*} value The value to search for.
 * @returns {number} Returns `0` if `value` is found, else `-1`.
 */
function cacheIndexOf(cache, value) {
  var data = cache.data,
      result = (typeof value == 'string' || isObject(value)) ? data.set.has(value) : data.hash[value];

  return result ? 0 : -1;
}

module.exports = cacheIndexOf;

},{"../lang/isObject":73}],40:[function(require,module,exports){
var isObject = require('../lang/isObject');

/**
 * Adds `value` to the cache.
 *
 * @private
 * @name push
 * @memberOf SetCache
 * @param {*} value The value to cache.
 */
function cachePush(value) {
  var data = this.data;
  if (typeof value == 'string' || isObject(value)) {
    data.set.add(value);
  } else {
    data.hash[value] = true;
  }
}

module.exports = cachePush;

},{"../lang/isObject":73}],41:[function(require,module,exports){
var bindCallback = require('./bindCallback'),
    isIterateeCall = require('./isIterateeCall'),
    restParam = require('../function/restParam');

/**
 * Creates a `_.assign`, `_.defaults`, or `_.merge` function.
 *
 * @private
 * @param {Function} assigner The function to assign values.
 * @returns {Function} Returns the new assigner function.
 */
function createAssigner(assigner) {
  return restParam(function(object, sources) {
    var index = -1,
        length = object == null ? 0 : sources.length,
        customizer = length > 2 ? sources[length - 2] : undefined,
        guard = length > 2 ? sources[2] : undefined,
        thisArg = length > 1 ? sources[length - 1] : undefined;

    if (typeof customizer == 'function') {
      customizer = bindCallback(customizer, thisArg, 5);
      length -= 2;
    } else {
      customizer = typeof thisArg == 'function' ? thisArg : undefined;
      length -= (customizer ? 1 : 0);
    }
    if (guard && isIterateeCall(sources[0], sources[1], guard)) {
      customizer = length < 3 ? undefined : customizer;
      length = 1;
    }
    while (++index < length) {
      var source = sources[index];
      if (source) {
        assigner(object, source, customizer);
      }
    }
    return object;
  });
}

module.exports = createAssigner;

},{"../function/restParam":2,"./bindCallback":38,"./isIterateeCall":58}],42:[function(require,module,exports){
var toObject = require('./toObject');

/**
 * Creates a base function for `_.forIn` or `_.forInRight`.
 *
 * @private
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {Function} Returns the new base function.
 */
function createBaseFor(fromRight) {
  return function(object, iteratee, keysFunc) {
    var iterable = toObject(object),
        props = keysFunc(object),
        length = props.length,
        index = fromRight ? length : -1;

    while ((fromRight ? index-- : ++index < length)) {
      var key = props[index];
      if (iteratee(iterable[key], key, iterable) === false) {
        break;
      }
    }
    return object;
  };
}

module.exports = createBaseFor;

},{"./toObject":67}],43:[function(require,module,exports){
(function (global){
var SetCache = require('./SetCache'),
    getNative = require('./getNative');

/** Native method references. */
var Set = getNative(global, 'Set');

/* Native method references for those with the same name as other `lodash` methods. */
var nativeCreate = getNative(Object, 'create');

/**
 * Creates a `Set` cache object to optimize linear searches of large arrays.
 *
 * @private
 * @param {Array} [values] The values to cache.
 * @returns {null|Object} Returns the new cache object if `Set` is supported, else `null`.
 */
function createCache(values) {
  return (nativeCreate && Set) ? new SetCache(values) : null;
}

module.exports = createCache;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./SetCache":3,"./getNative":54}],44:[function(require,module,exports){
var restParam = require('../function/restParam');

/**
 * Creates a `_.defaults` or `_.defaultsDeep` function.
 *
 * @private
 * @param {Function} assigner The function to assign values.
 * @param {Function} customizer The function to customize assigned values.
 * @returns {Function} Returns the new defaults function.
 */
function createDefaults(assigner, customizer) {
  return restParam(function(args) {
    var object = args[0];
    if (object == null) {
      return object;
    }
    args.push(customizer);
    return assigner.apply(undefined, args);
  });
}

module.exports = createDefaults;

},{"../function/restParam":2}],45:[function(require,module,exports){
var baseCallback = require('./baseCallback'),
    baseFind = require('./baseFind');

/**
 * Creates a `_.findKey` or `_.findLastKey` function.
 *
 * @private
 * @param {Function} objectFunc The function to iterate over an object.
 * @returns {Function} Returns the new find function.
 */
function createFindKey(objectFunc) {
  return function(object, predicate, thisArg) {
    predicate = baseCallback(predicate, thisArg, 3);
    return baseFind(object, predicate, objectFunc, true);
  };
}

module.exports = createFindKey;

},{"./baseCallback":12,"./baseFind":16}],46:[function(require,module,exports){
var bindCallback = require('./bindCallback'),
    keysIn = require('../object/keysIn');

/**
 * Creates a function for `_.forIn` or `_.forInRight`.
 *
 * @private
 * @param {Function} objectFunc The function to iterate over an object.
 * @returns {Function} Returns the new each function.
 */
function createForIn(objectFunc) {
  return function(object, iteratee, thisArg) {
    if (typeof iteratee != 'function' || thisArg !== undefined) {
      iteratee = bindCallback(iteratee, thisArg, 3);
    }
    return objectFunc(object, iteratee, keysIn);
  };
}

module.exports = createForIn;

},{"../object/keysIn":94,"./bindCallback":38}],47:[function(require,module,exports){
var bindCallback = require('./bindCallback');

/**
 * Creates a function for `_.forOwn` or `_.forOwnRight`.
 *
 * @private
 * @param {Function} objectFunc The function to iterate over an object.
 * @returns {Function} Returns the new each function.
 */
function createForOwn(objectFunc) {
  return function(object, iteratee, thisArg) {
    if (typeof iteratee != 'function' || thisArg !== undefined) {
      iteratee = bindCallback(iteratee, thisArg, 3);
    }
    return objectFunc(object, iteratee);
  };
}

module.exports = createForOwn;

},{"./bindCallback":38}],48:[function(require,module,exports){
var baseCallback = require('./baseCallback'),
    baseForOwn = require('./baseForOwn');

/**
 * Creates a function for `_.mapKeys` or `_.mapValues`.
 *
 * @private
 * @param {boolean} [isMapKeys] Specify mapping keys instead of values.
 * @returns {Function} Returns the new map function.
 */
function createObjectMapper(isMapKeys) {
  return function(object, iteratee, thisArg) {
    var result = {};
    iteratee = baseCallback(iteratee, thisArg, 3);

    baseForOwn(object, function(value, key, object) {
      var mapped = iteratee(value, key, object);
      key = isMapKeys ? mapped : key;
      value = isMapKeys ? value : mapped;
      result[key] = value;
    });
    return result;
  };
}

module.exports = createObjectMapper;

},{"./baseCallback":12,"./baseForOwn":20}],49:[function(require,module,exports){
var arraySome = require('./arraySome');

/**
 * A specialized version of `baseIsEqualDeep` for arrays with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Array} array The array to compare.
 * @param {Array} other The other array to compare.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Function} [customizer] The function to customize comparing arrays.
 * @param {boolean} [isLoose] Specify performing partial comparisons.
 * @param {Array} [stackA] Tracks traversed `value` objects.
 * @param {Array} [stackB] Tracks traversed `other` objects.
 * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
 */
function equalArrays(array, other, equalFunc, customizer, isLoose, stackA, stackB) {
  var index = -1,
      arrLength = array.length,
      othLength = other.length;

  if (arrLength != othLength && !(isLoose && othLength > arrLength)) {
    return false;
  }
  // Ignore non-index properties.
  while (++index < arrLength) {
    var arrValue = array[index],
        othValue = other[index],
        result = customizer ? customizer(isLoose ? othValue : arrValue, isLoose ? arrValue : othValue, index) : undefined;

    if (result !== undefined) {
      if (result) {
        continue;
      }
      return false;
    }
    // Recursively compare arrays (susceptible to call stack limits).
    if (isLoose) {
      if (!arraySome(other, function(othValue) {
            return arrValue === othValue || equalFunc(arrValue, othValue, customizer, isLoose, stackA, stackB);
          })) {
        return false;
      }
    } else if (!(arrValue === othValue || equalFunc(arrValue, othValue, customizer, isLoose, stackA, stackB))) {
      return false;
    }
  }
  return true;
}

module.exports = equalArrays;

},{"./arraySome":8}],50:[function(require,module,exports){
/** `Object#toString` result references. */
var boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    numberTag = '[object Number]',
    regexpTag = '[object RegExp]',
    stringTag = '[object String]';

/**
 * A specialized version of `baseIsEqualDeep` for comparing objects of
 * the same `toStringTag`.
 *
 * **Note:** This function only supports comparing values with tags of
 * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {string} tag The `toStringTag` of the objects to compare.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalByTag(object, other, tag) {
  switch (tag) {
    case boolTag:
    case dateTag:
      // Coerce dates and booleans to numbers, dates to milliseconds and booleans
      // to `1` or `0` treating invalid dates coerced to `NaN` as not equal.
      return +object == +other;

    case errorTag:
      return object.name == other.name && object.message == other.message;

    case numberTag:
      // Treat `NaN` vs. `NaN` as equal.
      return (object != +object)
        ? other != +other
        : object == +other;

    case regexpTag:
    case stringTag:
      // Coerce regexes to strings and treat strings primitives and string
      // objects as equal. See https://es5.github.io/#x15.10.6.4 for more details.
      return object == (other + '');
  }
  return false;
}

module.exports = equalByTag;

},{}],51:[function(require,module,exports){
var keys = require('../object/keys');

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * A specialized version of `baseIsEqualDeep` for objects with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Function} [customizer] The function to customize comparing values.
 * @param {boolean} [isLoose] Specify performing partial comparisons.
 * @param {Array} [stackA] Tracks traversed `value` objects.
 * @param {Array} [stackB] Tracks traversed `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalObjects(object, other, equalFunc, customizer, isLoose, stackA, stackB) {
  var objProps = keys(object),
      objLength = objProps.length,
      othProps = keys(other),
      othLength = othProps.length;

  if (objLength != othLength && !isLoose) {
    return false;
  }
  var index = objLength;
  while (index--) {
    var key = objProps[index];
    if (!(isLoose ? key in other : hasOwnProperty.call(other, key))) {
      return false;
    }
  }
  var skipCtor = isLoose;
  while (++index < objLength) {
    key = objProps[index];
    var objValue = object[key],
        othValue = other[key],
        result = customizer ? customizer(isLoose ? othValue : objValue, isLoose? objValue : othValue, key) : undefined;

    // Recursively compare objects (susceptible to call stack limits).
    if (!(result === undefined ? equalFunc(objValue, othValue, customizer, isLoose, stackA, stackB) : result)) {
      return false;
    }
    skipCtor || (skipCtor = key == 'constructor');
  }
  if (!skipCtor) {
    var objCtor = object.constructor,
        othCtor = other.constructor;

    // Non `Object` object instances with different constructors are not equal.
    if (objCtor != othCtor &&
        ('constructor' in object && 'constructor' in other) &&
        !(typeof objCtor == 'function' && objCtor instanceof objCtor &&
          typeof othCtor == 'function' && othCtor instanceof othCtor)) {
      return false;
    }
  }
  return true;
}

module.exports = equalObjects;

},{"../object/keys":93}],52:[function(require,module,exports){
var baseProperty = require('./baseProperty');

/**
 * Gets the "length" property value of `object`.
 *
 * **Note:** This function is used to avoid a [JIT bug](https://bugs.webkit.org/show_bug.cgi?id=142792)
 * that affects Safari on at least iOS 8.1-8.3 ARM64.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {*} Returns the "length" value.
 */
var getLength = baseProperty('length');

module.exports = getLength;

},{"./baseProperty":33}],53:[function(require,module,exports){
var isStrictComparable = require('./isStrictComparable'),
    pairs = require('../object/pairs');

/**
 * Gets the propery names, values, and compare flags of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the match data of `object`.
 */
function getMatchData(object) {
  var result = pairs(object),
      length = result.length;

  while (length--) {
    result[length][2] = isStrictComparable(result[length][1]);
  }
  return result;
}

module.exports = getMatchData;

},{"../object/pairs":100,"./isStrictComparable":62}],54:[function(require,module,exports){
var isNative = require('../lang/isNative');

/**
 * Gets the native function at `key` of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {string} key The key of the method to get.
 * @returns {*} Returns the function if it's native, else `undefined`.
 */
function getNative(object, key) {
  var value = object == null ? undefined : object[key];
  return isNative(value) ? value : undefined;
}

module.exports = getNative;

},{"../lang/isNative":72}],55:[function(require,module,exports){
/**
 * Gets the index at which the first occurrence of `NaN` is found in `array`.
 *
 * @private
 * @param {Array} array The array to search.
 * @param {number} fromIndex The index to search from.
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {number} Returns the index of the matched `NaN`, else `-1`.
 */
function indexOfNaN(array, fromIndex, fromRight) {
  var length = array.length,
      index = fromIndex + (fromRight ? 0 : -1);

  while ((fromRight ? index-- : ++index < length)) {
    var other = array[index];
    if (other !== other) {
      return index;
    }
  }
  return -1;
}

module.exports = indexOfNaN;

},{}],56:[function(require,module,exports){
var getLength = require('./getLength'),
    isLength = require('./isLength');

/**
 * Checks if `value` is array-like.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 */
function isArrayLike(value) {
  return value != null && isLength(getLength(value));
}

module.exports = isArrayLike;

},{"./getLength":52,"./isLength":60}],57:[function(require,module,exports){
/** Used to detect unsigned integer values. */
var reIsUint = /^\d+$/;

/**
 * Used as the [maximum length](http://ecma-international.org/ecma-262/6.0/#sec-number.max_safe_integer)
 * of an array-like value.
 */
var MAX_SAFE_INTEGER = 9007199254740991;

/**
 * Checks if `value` is a valid array-like index.
 *
 * @private
 * @param {*} value The value to check.
 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */
function isIndex(value, length) {
  value = (typeof value == 'number' || reIsUint.test(value)) ? +value : -1;
  length = length == null ? MAX_SAFE_INTEGER : length;
  return value > -1 && value % 1 == 0 && value < length;
}

module.exports = isIndex;

},{}],58:[function(require,module,exports){
var isArrayLike = require('./isArrayLike'),
    isIndex = require('./isIndex'),
    isObject = require('../lang/isObject');

/**
 * Checks if the provided arguments are from an iteratee call.
 *
 * @private
 * @param {*} value The potential iteratee value argument.
 * @param {*} index The potential iteratee index or key argument.
 * @param {*} object The potential iteratee object argument.
 * @returns {boolean} Returns `true` if the arguments are from an iteratee call, else `false`.
 */
function isIterateeCall(value, index, object) {
  if (!isObject(object)) {
    return false;
  }
  var type = typeof index;
  if (type == 'number'
      ? (isArrayLike(object) && isIndex(index, object.length))
      : (type == 'string' && index in object)) {
    var other = object[index];
    return value === value ? (value === other) : (other !== other);
  }
  return false;
}

module.exports = isIterateeCall;

},{"../lang/isObject":73,"./isArrayLike":56,"./isIndex":57}],59:[function(require,module,exports){
var isArray = require('../lang/isArray'),
    toObject = require('./toObject');

/** Used to match property names within property paths. */
var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\n\\]|\\.)*?\1)\]/,
    reIsPlainProp = /^\w*$/;

/**
 * Checks if `value` is a property name and not a property path.
 *
 * @private
 * @param {*} value The value to check.
 * @param {Object} [object] The object to query keys on.
 * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
 */
function isKey(value, object) {
  var type = typeof value;
  if ((type == 'string' && reIsPlainProp.test(value)) || type == 'number') {
    return true;
  }
  if (isArray(value)) {
    return false;
  }
  var result = !reIsDeepProp.test(value);
  return result || (object != null && value in toObject(object));
}

module.exports = isKey;

},{"../lang/isArray":70,"./toObject":67}],60:[function(require,module,exports){
/**
 * Used as the [maximum length](http://ecma-international.org/ecma-262/6.0/#sec-number.max_safe_integer)
 * of an array-like value.
 */
var MAX_SAFE_INTEGER = 9007199254740991;

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This function is based on [`ToLength`](http://ecma-international.org/ecma-262/6.0/#sec-tolength).
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 */
function isLength(value) {
  return typeof value == 'number' && value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
}

module.exports = isLength;

},{}],61:[function(require,module,exports){
/**
 * Checks if `value` is object-like.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 */
function isObjectLike(value) {
  return !!value && typeof value == 'object';
}

module.exports = isObjectLike;

},{}],62:[function(require,module,exports){
var isObject = require('../lang/isObject');

/**
 * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` if suitable for strict
 *  equality comparisons, else `false`.
 */
function isStrictComparable(value) {
  return value === value && !isObject(value);
}

module.exports = isStrictComparable;

},{"../lang/isObject":73}],63:[function(require,module,exports){
var merge = require('../object/merge');

/**
 * Used by `_.defaultsDeep` to customize its `_.merge` use.
 *
 * @private
 * @param {*} objectValue The destination object property value.
 * @param {*} sourceValue The source object property value.
 * @returns {*} Returns the value to assign to the destination object.
 */
function mergeDefaults(objectValue, sourceValue) {
  return objectValue === undefined ? sourceValue : merge(objectValue, sourceValue, mergeDefaults);
}

module.exports = mergeDefaults;

},{"../object/merge":97}],64:[function(require,module,exports){
var toObject = require('./toObject');

/**
 * A specialized version of `_.pick` which picks `object` properties specified
 * by `props`.
 *
 * @private
 * @param {Object} object The source object.
 * @param {string[]} props The property names to pick.
 * @returns {Object} Returns the new object.
 */
function pickByArray(object, props) {
  object = toObject(object);

  var index = -1,
      length = props.length,
      result = {};

  while (++index < length) {
    var key = props[index];
    if (key in object) {
      result[key] = object[key];
    }
  }
  return result;
}

module.exports = pickByArray;

},{"./toObject":67}],65:[function(require,module,exports){
var baseForIn = require('./baseForIn');

/**
 * A specialized version of `_.pick` which picks `object` properties `predicate`
 * returns truthy for.
 *
 * @private
 * @param {Object} object The source object.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {Object} Returns the new object.
 */
function pickByCallback(object, predicate) {
  var result = {};
  baseForIn(object, function(value, key, object) {
    if (predicate(value, key, object)) {
      result[key] = value;
    }
  });
  return result;
}

module.exports = pickByCallback;

},{"./baseForIn":19}],66:[function(require,module,exports){
var isArguments = require('../lang/isArguments'),
    isArray = require('../lang/isArray'),
    isIndex = require('./isIndex'),
    isLength = require('./isLength'),
    keysIn = require('../object/keysIn');

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * A fallback implementation of `Object.keys` which creates an array of the
 * own enumerable property names of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function shimKeys(object) {
  var props = keysIn(object),
      propsLength = props.length,
      length = propsLength && object.length;

  var allowIndexes = !!length && isLength(length) &&
    (isArray(object) || isArguments(object));

  var index = -1,
      result = [];

  while (++index < propsLength) {
    var key = props[index];
    if ((allowIndexes && isIndex(key, length)) || hasOwnProperty.call(object, key)) {
      result.push(key);
    }
  }
  return result;
}

module.exports = shimKeys;

},{"../lang/isArguments":69,"../lang/isArray":70,"../object/keysIn":94,"./isIndex":57,"./isLength":60}],67:[function(require,module,exports){
var isObject = require('../lang/isObject');

/**
 * Converts `value` to an object if it's not one.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {Object} Returns the object.
 */
function toObject(value) {
  return isObject(value) ? value : Object(value);
}

module.exports = toObject;

},{"../lang/isObject":73}],68:[function(require,module,exports){
var baseToString = require('./baseToString'),
    isArray = require('../lang/isArray');

/** Used to match property names within property paths. */
var rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\n\\]|\\.)*?)\2)\]/g;

/** Used to match backslashes in property paths. */
var reEscapeChar = /\\(\\)?/g;

/**
 * Converts `value` to property path array if it's not one.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {Array} Returns the property path array.
 */
function toPath(value) {
  if (isArray(value)) {
    return value;
  }
  var result = [];
  baseToString(value).replace(rePropName, function(match, number, quote, string) {
    result.push(quote ? string.replace(reEscapeChar, '$1') : (number || match));
  });
  return result;
}

module.exports = toPath;

},{"../lang/isArray":70,"./baseToString":36}],69:[function(require,module,exports){
var isArrayLike = require('../internal/isArrayLike'),
    isObjectLike = require('../internal/isObjectLike');

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Native method references. */
var propertyIsEnumerable = objectProto.propertyIsEnumerable;

/**
 * Checks if `value` is classified as an `arguments` object.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */
function isArguments(value) {
  return isObjectLike(value) && isArrayLike(value) &&
    hasOwnProperty.call(value, 'callee') && !propertyIsEnumerable.call(value, 'callee');
}

module.exports = isArguments;

},{"../internal/isArrayLike":56,"../internal/isObjectLike":61}],70:[function(require,module,exports){
var getNative = require('../internal/getNative'),
    isLength = require('../internal/isLength'),
    isObjectLike = require('../internal/isObjectLike');

/** `Object#toString` result references. */
var arrayTag = '[object Array]';

/** Used for native method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
 * of values.
 */
var objToString = objectProto.toString;

/* Native method references for those with the same name as other `lodash` methods. */
var nativeIsArray = getNative(Array, 'isArray');

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(function() { return arguments; }());
 * // => false
 */
var isArray = nativeIsArray || function(value) {
  return isObjectLike(value) && isLength(value.length) && objToString.call(value) == arrayTag;
};

module.exports = isArray;

},{"../internal/getNative":54,"../internal/isLength":60,"../internal/isObjectLike":61}],71:[function(require,module,exports){
var isObject = require('./isObject');

/** `Object#toString` result references. */
var funcTag = '[object Function]';

/** Used for native method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
 * of values.
 */
var objToString = objectProto.toString;

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in older versions of Chrome and Safari which return 'function' for regexes
  // and Safari 8 which returns 'object' for typed array constructors.
  return isObject(value) && objToString.call(value) == funcTag;
}

module.exports = isFunction;

},{"./isObject":73}],72:[function(require,module,exports){
var isFunction = require('./isFunction'),
    isObjectLike = require('../internal/isObjectLike');

/** Used to detect host constructors (Safari > 5). */
var reIsHostCtor = /^\[object .+?Constructor\]$/;

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to resolve the decompiled source of functions. */
var fnToString = Function.prototype.toString;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Used to detect if a method is native. */
var reIsNative = RegExp('^' +
  fnToString.call(hasOwnProperty).replace(/[\\^$.*+?()[\]{}|]/g, '\\$&')
  .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
);

/**
 * Checks if `value` is a native function.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a native function, else `false`.
 * @example
 *
 * _.isNative(Array.prototype.push);
 * // => true
 *
 * _.isNative(_);
 * // => false
 */
function isNative(value) {
  if (value == null) {
    return false;
  }
  if (isFunction(value)) {
    return reIsNative.test(fnToString.call(value));
  }
  return isObjectLike(value) && reIsHostCtor.test(value);
}

module.exports = isNative;

},{"../internal/isObjectLike":61,"./isFunction":71}],73:[function(require,module,exports){
/**
 * Checks if `value` is the [language type](https://es5.github.io/#x8) of `Object`.
 * (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(1);
 * // => false
 */
function isObject(value) {
  // Avoid a V8 JIT bug in Chrome 19-20.
  // See https://code.google.com/p/v8/issues/detail?id=2291 for more details.
  var type = typeof value;
  return !!value && (type == 'object' || type == 'function');
}

module.exports = isObject;

},{}],74:[function(require,module,exports){
var baseForIn = require('../internal/baseForIn'),
    isArguments = require('./isArguments'),
    isObjectLike = require('../internal/isObjectLike');

/** `Object#toString` result references. */
var objectTag = '[object Object]';

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
 * of values.
 */
var objToString = objectProto.toString;

/**
 * Checks if `value` is a plain object, that is, an object created by the
 * `Object` constructor or one with a `[[Prototype]]` of `null`.
 *
 * **Note:** This method assumes objects created by the `Object` constructor
 * have no inherited enumerable properties.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 * }
 *
 * _.isPlainObject(new Foo);
 * // => false
 *
 * _.isPlainObject([1, 2, 3]);
 * // => false
 *
 * _.isPlainObject({ 'x': 0, 'y': 0 });
 * // => true
 *
 * _.isPlainObject(Object.create(null));
 * // => true
 */
function isPlainObject(value) {
  var Ctor;

  // Exit early for non `Object` objects.
  if (!(isObjectLike(value) && objToString.call(value) == objectTag && !isArguments(value)) ||
      (!hasOwnProperty.call(value, 'constructor') && (Ctor = value.constructor, typeof Ctor == 'function' && !(Ctor instanceof Ctor)))) {
    return false;
  }
  // IE < 9 iterates inherited properties before own properties. If the first
  // iterated property is an object's own property then there are no inherited
  // enumerable properties.
  var result;
  // In most environments an object's own properties are iterated before
  // its inherited properties. If the last iterated property is an object's
  // own property then there are no inherited enumerable properties.
  baseForIn(value, function(subValue, key) {
    result = key;
  });
  return result === undefined || hasOwnProperty.call(value, result);
}

module.exports = isPlainObject;

},{"../internal/baseForIn":19,"../internal/isObjectLike":61,"./isArguments":69}],75:[function(require,module,exports){
var isLength = require('../internal/isLength'),
    isObjectLike = require('../internal/isObjectLike');

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    funcTag = '[object Function]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    objectTag = '[object Object]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    weakMapTag = '[object WeakMap]';

var arrayBufferTag = '[object ArrayBuffer]',
    float32Tag = '[object Float32Array]',
    float64Tag = '[object Float64Array]',
    int8Tag = '[object Int8Array]',
    int16Tag = '[object Int16Array]',
    int32Tag = '[object Int32Array]',
    uint8Tag = '[object Uint8Array]',
    uint8ClampedTag = '[object Uint8ClampedArray]',
    uint16Tag = '[object Uint16Array]',
    uint32Tag = '[object Uint32Array]';

/** Used to identify `toStringTag` values of typed arrays. */
var typedArrayTags = {};
typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
typedArrayTags[uint32Tag] = true;
typedArrayTags[argsTag] = typedArrayTags[arrayTag] =
typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
typedArrayTags[dateTag] = typedArrayTags[errorTag] =
typedArrayTags[funcTag] = typedArrayTags[mapTag] =
typedArrayTags[numberTag] = typedArrayTags[objectTag] =
typedArrayTags[regexpTag] = typedArrayTags[setTag] =
typedArrayTags[stringTag] = typedArrayTags[weakMapTag] = false;

/** Used for native method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
 * of values.
 */
var objToString = objectProto.toString;

/**
 * Checks if `value` is classified as a typed array.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
 * @example
 *
 * _.isTypedArray(new Uint8Array);
 * // => true
 *
 * _.isTypedArray([]);
 * // => false
 */
function isTypedArray(value) {
  return isObjectLike(value) && isLength(value.length) && !!typedArrayTags[objToString.call(value)];
}

module.exports = isTypedArray;

},{"../internal/isLength":60,"../internal/isObjectLike":61}],76:[function(require,module,exports){
var baseCopy = require('../internal/baseCopy'),
    keysIn = require('../object/keysIn');

/**
 * Converts `value` to a plain object flattening inherited enumerable
 * properties of `value` to own properties of the plain object.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {Object} Returns the converted plain object.
 * @example
 *
 * function Foo() {
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.assign({ 'a': 1 }, new Foo);
 * // => { 'a': 1, 'b': 2 }
 *
 * _.assign({ 'a': 1 }, _.toPlainObject(new Foo));
 * // => { 'a': 1, 'b': 2, 'c': 3 }
 */
function toPlainObject(value) {
  return baseCopy(value, keysIn(value));
}

module.exports = toPlainObject;

},{"../internal/baseCopy":13,"../object/keysIn":94}],77:[function(require,module,exports){
module.exports = {
  'assign': require('./object/assign'),
  'create': require('./object/create'),
  'defaults': require('./object/defaults'),
  'defaultsDeep': require('./object/defaultsDeep'),
  'extend': require('./object/extend'),
  'findKey': require('./object/findKey'),
  'findLastKey': require('./object/findLastKey'),
  'forIn': require('./object/forIn'),
  'forInRight': require('./object/forInRight'),
  'forOwn': require('./object/forOwn'),
  'forOwnRight': require('./object/forOwnRight'),
  'functions': require('./object/functions'),
  'get': require('./object/get'),
  'has': require('./object/has'),
  'invert': require('./object/invert'),
  'keys': require('./object/keys'),
  'keysIn': require('./object/keysIn'),
  'mapKeys': require('./object/mapKeys'),
  'mapValues': require('./object/mapValues'),
  'merge': require('./object/merge'),
  'methods': require('./object/methods'),
  'omit': require('./object/omit'),
  'pairs': require('./object/pairs'),
  'pick': require('./object/pick'),
  'result': require('./object/result'),
  'set': require('./object/set'),
  'transform': require('./object/transform'),
  'values': require('./object/values'),
  'valuesIn': require('./object/valuesIn')
};

},{"./object/assign":78,"./object/create":79,"./object/defaults":80,"./object/defaultsDeep":81,"./object/extend":82,"./object/findKey":83,"./object/findLastKey":84,"./object/forIn":85,"./object/forInRight":86,"./object/forOwn":87,"./object/forOwnRight":88,"./object/functions":89,"./object/get":90,"./object/has":91,"./object/invert":92,"./object/keys":93,"./object/keysIn":94,"./object/mapKeys":95,"./object/mapValues":96,"./object/merge":97,"./object/methods":98,"./object/omit":99,"./object/pairs":100,"./object/pick":101,"./object/result":102,"./object/set":103,"./object/transform":104,"./object/values":105,"./object/valuesIn":106}],78:[function(require,module,exports){
var assignWith = require('../internal/assignWith'),
    baseAssign = require('../internal/baseAssign'),
    createAssigner = require('../internal/createAssigner');

/**
 * Assigns own enumerable properties of source object(s) to the destination
 * object. Subsequent sources overwrite property assignments of previous sources.
 * If `customizer` is provided it's invoked to produce the assigned values.
 * The `customizer` is bound to `thisArg` and invoked with five arguments:
 * (objectValue, sourceValue, key, object, source).
 *
 * **Note:** This method mutates `object` and is based on
 * [`Object.assign`](http://ecma-international.org/ecma-262/6.0/#sec-object.assign).
 *
 * @static
 * @memberOf _
 * @alias extend
 * @category Object
 * @param {Object} object The destination object.
 * @param {...Object} [sources] The source objects.
 * @param {Function} [customizer] The function to customize assigned values.
 * @param {*} [thisArg] The `this` binding of `customizer`.
 * @returns {Object} Returns `object`.
 * @example
 *
 * _.assign({ 'user': 'barney' }, { 'age': 40 }, { 'user': 'fred' });
 * // => { 'user': 'fred', 'age': 40 }
 *
 * // using a customizer callback
 * var defaults = _.partialRight(_.assign, function(value, other) {
 *   return _.isUndefined(value) ? other : value;
 * });
 *
 * defaults({ 'user': 'barney' }, { 'age': 36 }, { 'user': 'fred' });
 * // => { 'user': 'barney', 'age': 36 }
 */
var assign = createAssigner(function(object, source, customizer) {
  return customizer
    ? assignWith(object, source, customizer)
    : baseAssign(object, source);
});

module.exports = assign;

},{"../internal/assignWith":10,"../internal/baseAssign":11,"../internal/createAssigner":41}],79:[function(require,module,exports){
var baseAssign = require('../internal/baseAssign'),
    baseCreate = require('../internal/baseCreate'),
    isIterateeCall = require('../internal/isIterateeCall');

/**
 * Creates an object that inherits from the given `prototype` object. If a
 * `properties` object is provided its own enumerable properties are assigned
 * to the created object.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} prototype The object to inherit from.
 * @param {Object} [properties] The properties to assign to the object.
 * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
 * @returns {Object} Returns the new object.
 * @example
 *
 * function Shape() {
 *   this.x = 0;
 *   this.y = 0;
 * }
 *
 * function Circle() {
 *   Shape.call(this);
 * }
 *
 * Circle.prototype = _.create(Shape.prototype, {
 *   'constructor': Circle
 * });
 *
 * var circle = new Circle;
 * circle instanceof Circle;
 * // => true
 *
 * circle instanceof Shape;
 * // => true
 */
function create(prototype, properties, guard) {
  var result = baseCreate(prototype);
  if (guard && isIterateeCall(prototype, properties, guard)) {
    properties = undefined;
  }
  return properties ? baseAssign(result, properties) : result;
}

module.exports = create;

},{"../internal/baseAssign":11,"../internal/baseCreate":14,"../internal/isIterateeCall":58}],80:[function(require,module,exports){
var assign = require('./assign'),
    assignDefaults = require('../internal/assignDefaults'),
    createDefaults = require('../internal/createDefaults');

/**
 * Assigns own enumerable properties of source object(s) to the destination
 * object for all destination properties that resolve to `undefined`. Once a
 * property is set, additional values of the same property are ignored.
 *
 * **Note:** This method mutates `object`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The destination object.
 * @param {...Object} [sources] The source objects.
 * @returns {Object} Returns `object`.
 * @example
 *
 * _.defaults({ 'user': 'barney' }, { 'age': 36 }, { 'user': 'fred' });
 * // => { 'user': 'barney', 'age': 36 }
 */
var defaults = createDefaults(assign, assignDefaults);

module.exports = defaults;

},{"../internal/assignDefaults":9,"../internal/createDefaults":44,"./assign":78}],81:[function(require,module,exports){
var createDefaults = require('../internal/createDefaults'),
    merge = require('./merge'),
    mergeDefaults = require('../internal/mergeDefaults');

/**
 * This method is like `_.defaults` except that it recursively assigns
 * default properties.
 *
 * **Note:** This method mutates `object`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The destination object.
 * @param {...Object} [sources] The source objects.
 * @returns {Object} Returns `object`.
 * @example
 *
 * _.defaultsDeep({ 'user': { 'name': 'barney' } }, { 'user': { 'name': 'fred', 'age': 36 } });
 * // => { 'user': { 'name': 'barney', 'age': 36 } }
 *
 */
var defaultsDeep = createDefaults(merge, mergeDefaults);

module.exports = defaultsDeep;

},{"../internal/createDefaults":44,"../internal/mergeDefaults":63,"./merge":97}],82:[function(require,module,exports){
module.exports = require('./assign');

},{"./assign":78}],83:[function(require,module,exports){
var baseForOwn = require('../internal/baseForOwn'),
    createFindKey = require('../internal/createFindKey');

/**
 * This method is like `_.find` except that it returns the key of the first
 * element `predicate` returns truthy for instead of the element itself.
 *
 * If a property name is provided for `predicate` the created `_.property`
 * style callback returns the property value of the given element.
 *
 * If a value is also provided for `thisArg` the created `_.matchesProperty`
 * style callback returns `true` for elements that have a matching property
 * value, else `false`.
 *
 * If an object is provided for `predicate` the created `_.matches` style
 * callback returns `true` for elements that have the properties of the given
 * object, else `false`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to search.
 * @param {Function|Object|string} [predicate=_.identity] The function invoked
 *  per iteration.
 * @param {*} [thisArg] The `this` binding of `predicate`.
 * @returns {string|undefined} Returns the key of the matched element, else `undefined`.
 * @example
 *
 * var users = {
 *   'barney':  { 'age': 36, 'active': true },
 *   'fred':    { 'age': 40, 'active': false },
 *   'pebbles': { 'age': 1,  'active': true }
 * };
 *
 * _.findKey(users, function(chr) {
 *   return chr.age < 40;
 * });
 * // => 'barney' (iteration order is not guaranteed)
 *
 * // using the `_.matches` callback shorthand
 * _.findKey(users, { 'age': 1, 'active': true });
 * // => 'pebbles'
 *
 * // using the `_.matchesProperty` callback shorthand
 * _.findKey(users, 'active', false);
 * // => 'fred'
 *
 * // using the `_.property` callback shorthand
 * _.findKey(users, 'active');
 * // => 'barney'
 */
var findKey = createFindKey(baseForOwn);

module.exports = findKey;

},{"../internal/baseForOwn":20,"../internal/createFindKey":45}],84:[function(require,module,exports){
var baseForOwnRight = require('../internal/baseForOwnRight'),
    createFindKey = require('../internal/createFindKey');

/**
 * This method is like `_.findKey` except that it iterates over elements of
 * a collection in the opposite order.
 *
 * If a property name is provided for `predicate` the created `_.property`
 * style callback returns the property value of the given element.
 *
 * If a value is also provided for `thisArg` the created `_.matchesProperty`
 * style callback returns `true` for elements that have a matching property
 * value, else `false`.
 *
 * If an object is provided for `predicate` the created `_.matches` style
 * callback returns `true` for elements that have the properties of the given
 * object, else `false`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to search.
 * @param {Function|Object|string} [predicate=_.identity] The function invoked
 *  per iteration.
 * @param {*} [thisArg] The `this` binding of `predicate`.
 * @returns {string|undefined} Returns the key of the matched element, else `undefined`.
 * @example
 *
 * var users = {
 *   'barney':  { 'age': 36, 'active': true },
 *   'fred':    { 'age': 40, 'active': false },
 *   'pebbles': { 'age': 1,  'active': true }
 * };
 *
 * _.findLastKey(users, function(chr) {
 *   return chr.age < 40;
 * });
 * // => returns `pebbles` assuming `_.findKey` returns `barney`
 *
 * // using the `_.matches` callback shorthand
 * _.findLastKey(users, { 'age': 36, 'active': true });
 * // => 'barney'
 *
 * // using the `_.matchesProperty` callback shorthand
 * _.findLastKey(users, 'active', false);
 * // => 'fred'
 *
 * // using the `_.property` callback shorthand
 * _.findLastKey(users, 'active');
 * // => 'pebbles'
 */
var findLastKey = createFindKey(baseForOwnRight);

module.exports = findLastKey;

},{"../internal/baseForOwnRight":21,"../internal/createFindKey":45}],85:[function(require,module,exports){
var baseFor = require('../internal/baseFor'),
    createForIn = require('../internal/createForIn');

/**
 * Iterates over own and inherited enumerable properties of an object invoking
 * `iteratee` for each property. The `iteratee` is bound to `thisArg` and invoked
 * with three arguments: (value, key, object). Iteratee functions may exit
 * iteration early by explicitly returning `false`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to iterate over.
 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
 * @param {*} [thisArg] The `this` binding of `iteratee`.
 * @returns {Object} Returns `object`.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.forIn(new Foo, function(value, key) {
 *   console.log(key);
 * });
 * // => logs 'a', 'b', and 'c' (iteration order is not guaranteed)
 */
var forIn = createForIn(baseFor);

module.exports = forIn;

},{"../internal/baseFor":18,"../internal/createForIn":46}],86:[function(require,module,exports){
var baseForRight = require('../internal/baseForRight'),
    createForIn = require('../internal/createForIn');

/**
 * This method is like `_.forIn` except that it iterates over properties of
 * `object` in the opposite order.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to iterate over.
 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
 * @param {*} [thisArg] The `this` binding of `iteratee`.
 * @returns {Object} Returns `object`.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.forInRight(new Foo, function(value, key) {
 *   console.log(key);
 * });
 * // => logs 'c', 'b', and 'a' assuming `_.forIn ` logs 'a', 'b', and 'c'
 */
var forInRight = createForIn(baseForRight);

module.exports = forInRight;

},{"../internal/baseForRight":22,"../internal/createForIn":46}],87:[function(require,module,exports){
var baseForOwn = require('../internal/baseForOwn'),
    createForOwn = require('../internal/createForOwn');

/**
 * Iterates over own enumerable properties of an object invoking `iteratee`
 * for each property. The `iteratee` is bound to `thisArg` and invoked with
 * three arguments: (value, key, object). Iteratee functions may exit iteration
 * early by explicitly returning `false`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to iterate over.
 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
 * @param {*} [thisArg] The `this` binding of `iteratee`.
 * @returns {Object} Returns `object`.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.forOwn(new Foo, function(value, key) {
 *   console.log(key);
 * });
 * // => logs 'a' and 'b' (iteration order is not guaranteed)
 */
var forOwn = createForOwn(baseForOwn);

module.exports = forOwn;

},{"../internal/baseForOwn":20,"../internal/createForOwn":47}],88:[function(require,module,exports){
var baseForOwnRight = require('../internal/baseForOwnRight'),
    createForOwn = require('../internal/createForOwn');

/**
 * This method is like `_.forOwn` except that it iterates over properties of
 * `object` in the opposite order.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to iterate over.
 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
 * @param {*} [thisArg] The `this` binding of `iteratee`.
 * @returns {Object} Returns `object`.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.forOwnRight(new Foo, function(value, key) {
 *   console.log(key);
 * });
 * // => logs 'b' and 'a' assuming `_.forOwn` logs 'a' and 'b'
 */
var forOwnRight = createForOwn(baseForOwnRight);

module.exports = forOwnRight;

},{"../internal/baseForOwnRight":21,"../internal/createForOwn":47}],89:[function(require,module,exports){
var baseFunctions = require('../internal/baseFunctions'),
    keysIn = require('./keysIn');

/**
 * Creates an array of function property names from all enumerable properties,
 * own and inherited, of `object`.
 *
 * @static
 * @memberOf _
 * @alias methods
 * @category Object
 * @param {Object} object The object to inspect.
 * @returns {Array} Returns the new array of property names.
 * @example
 *
 * _.functions(_);
 * // => ['after', 'ary', 'assign', ...]
 */
function functions(object) {
  return baseFunctions(object, keysIn(object));
}

module.exports = functions;

},{"../internal/baseFunctions":23,"./keysIn":94}],90:[function(require,module,exports){
var baseGet = require('../internal/baseGet'),
    toPath = require('../internal/toPath');

/**
 * Gets the property value at `path` of `object`. If the resolved value is
 * `undefined` the `defaultValue` is used in its place.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @param {*} [defaultValue] The value returned if the resolved value is `undefined`.
 * @returns {*} Returns the resolved value.
 * @example
 *
 * var object = { 'a': [{ 'b': { 'c': 3 } }] };
 *
 * _.get(object, 'a[0].b.c');
 * // => 3
 *
 * _.get(object, ['a', '0', 'b', 'c']);
 * // => 3
 *
 * _.get(object, 'a.b.c', 'default');
 * // => 'default'
 */
function get(object, path, defaultValue) {
  var result = object == null ? undefined : baseGet(object, toPath(path), (path + ''));
  return result === undefined ? defaultValue : result;
}

module.exports = get;

},{"../internal/baseGet":24,"../internal/toPath":68}],91:[function(require,module,exports){
var baseGet = require('../internal/baseGet'),
    baseSlice = require('../internal/baseSlice'),
    isArguments = require('../lang/isArguments'),
    isArray = require('../lang/isArray'),
    isIndex = require('../internal/isIndex'),
    isKey = require('../internal/isKey'),
    isLength = require('../internal/isLength'),
    last = require('../array/last'),
    toPath = require('../internal/toPath');

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Checks if `path` is a direct property.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @returns {boolean} Returns `true` if `path` is a direct property, else `false`.
 * @example
 *
 * var object = { 'a': { 'b': { 'c': 3 } } };
 *
 * _.has(object, 'a');
 * // => true
 *
 * _.has(object, 'a.b.c');
 * // => true
 *
 * _.has(object, ['a', 'b', 'c']);
 * // => true
 */
function has(object, path) {
  if (object == null) {
    return false;
  }
  var result = hasOwnProperty.call(object, path);
  if (!result && !isKey(path)) {
    path = toPath(path);
    object = path.length == 1 ? object : baseGet(object, baseSlice(path, 0, -1));
    if (object == null) {
      return false;
    }
    path = last(path);
    result = hasOwnProperty.call(object, path);
  }
  return result || (isLength(object.length) && isIndex(path, object.length) &&
    (isArray(object) || isArguments(object)));
}

module.exports = has;

},{"../array/last":1,"../internal/baseGet":24,"../internal/baseSlice":35,"../internal/isIndex":57,"../internal/isKey":59,"../internal/isLength":60,"../internal/toPath":68,"../lang/isArguments":69,"../lang/isArray":70}],92:[function(require,module,exports){
var isIterateeCall = require('../internal/isIterateeCall'),
    keys = require('./keys');

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Creates an object composed of the inverted keys and values of `object`.
 * If `object` contains duplicate values, subsequent values overwrite property
 * assignments of previous values unless `multiValue` is `true`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to invert.
 * @param {boolean} [multiValue] Allow multiple values per key.
 * @param- {Object} [guard] Enables use as a callback for functions like `_.map`.
 * @returns {Object} Returns the new inverted object.
 * @example
 *
 * var object = { 'a': 1, 'b': 2, 'c': 1 };
 *
 * _.invert(object);
 * // => { '1': 'c', '2': 'b' }
 *
 * // with `multiValue`
 * _.invert(object, true);
 * // => { '1': ['a', 'c'], '2': ['b'] }
 */
function invert(object, multiValue, guard) {
  if (guard && isIterateeCall(object, multiValue, guard)) {
    multiValue = undefined;
  }
  var index = -1,
      props = keys(object),
      length = props.length,
      result = {};

  while (++index < length) {
    var key = props[index],
        value = object[key];

    if (multiValue) {
      if (hasOwnProperty.call(result, value)) {
        result[value].push(key);
      } else {
        result[value] = [key];
      }
    }
    else {
      result[value] = key;
    }
  }
  return result;
}

module.exports = invert;

},{"../internal/isIterateeCall":58,"./keys":93}],93:[function(require,module,exports){
var getNative = require('../internal/getNative'),
    isArrayLike = require('../internal/isArrayLike'),
    isObject = require('../lang/isObject'),
    shimKeys = require('../internal/shimKeys');

/* Native method references for those with the same name as other `lodash` methods. */
var nativeKeys = getNative(Object, 'keys');

/**
 * Creates an array of the own enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects. See the
 * [ES spec](http://ecma-international.org/ecma-262/6.0/#sec-object.keys)
 * for more details.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keys(new Foo);
 * // => ['a', 'b'] (iteration order is not guaranteed)
 *
 * _.keys('hi');
 * // => ['0', '1']
 */
var keys = !nativeKeys ? shimKeys : function(object) {
  var Ctor = object == null ? undefined : object.constructor;
  if ((typeof Ctor == 'function' && Ctor.prototype === object) ||
      (typeof object != 'function' && isArrayLike(object))) {
    return shimKeys(object);
  }
  return isObject(object) ? nativeKeys(object) : [];
};

module.exports = keys;

},{"../internal/getNative":54,"../internal/isArrayLike":56,"../internal/shimKeys":66,"../lang/isObject":73}],94:[function(require,module,exports){
var isArguments = require('../lang/isArguments'),
    isArray = require('../lang/isArray'),
    isIndex = require('../internal/isIndex'),
    isLength = require('../internal/isLength'),
    isObject = require('../lang/isObject');

/** Used for native method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Creates an array of the own and inherited enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keysIn(new Foo);
 * // => ['a', 'b', 'c'] (iteration order is not guaranteed)
 */
function keysIn(object) {
  if (object == null) {
    return [];
  }
  if (!isObject(object)) {
    object = Object(object);
  }
  var length = object.length;
  length = (length && isLength(length) &&
    (isArray(object) || isArguments(object)) && length) || 0;

  var Ctor = object.constructor,
      index = -1,
      isProto = typeof Ctor == 'function' && Ctor.prototype === object,
      result = Array(length),
      skipIndexes = length > 0;

  while (++index < length) {
    result[index] = (index + '');
  }
  for (var key in object) {
    if (!(skipIndexes && isIndex(key, length)) &&
        !(key == 'constructor' && (isProto || !hasOwnProperty.call(object, key)))) {
      result.push(key);
    }
  }
  return result;
}

module.exports = keysIn;

},{"../internal/isIndex":57,"../internal/isLength":60,"../lang/isArguments":69,"../lang/isArray":70,"../lang/isObject":73}],95:[function(require,module,exports){
var createObjectMapper = require('../internal/createObjectMapper');

/**
 * The opposite of `_.mapValues`; this method creates an object with the
 * same values as `object` and keys generated by running each own enumerable
 * property of `object` through `iteratee`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to iterate over.
 * @param {Function|Object|string} [iteratee=_.identity] The function invoked
 *  per iteration.
 * @param {*} [thisArg] The `this` binding of `iteratee`.
 * @returns {Object} Returns the new mapped object.
 * @example
 *
 * _.mapKeys({ 'a': 1, 'b': 2 }, function(value, key) {
 *   return key + value;
 * });
 * // => { 'a1': 1, 'b2': 2 }
 */
var mapKeys = createObjectMapper(true);

module.exports = mapKeys;

},{"../internal/createObjectMapper":48}],96:[function(require,module,exports){
var createObjectMapper = require('../internal/createObjectMapper');

/**
 * Creates an object with the same keys as `object` and values generated by
 * running each own enumerable property of `object` through `iteratee`. The
 * iteratee function is bound to `thisArg` and invoked with three arguments:
 * (value, key, object).
 *
 * If a property name is provided for `iteratee` the created `_.property`
 * style callback returns the property value of the given element.
 *
 * If a value is also provided for `thisArg` the created `_.matchesProperty`
 * style callback returns `true` for elements that have a matching property
 * value, else `false`.
 *
 * If an object is provided for `iteratee` the created `_.matches` style
 * callback returns `true` for elements that have the properties of the given
 * object, else `false`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to iterate over.
 * @param {Function|Object|string} [iteratee=_.identity] The function invoked
 *  per iteration.
 * @param {*} [thisArg] The `this` binding of `iteratee`.
 * @returns {Object} Returns the new mapped object.
 * @example
 *
 * _.mapValues({ 'a': 1, 'b': 2 }, function(n) {
 *   return n * 3;
 * });
 * // => { 'a': 3, 'b': 6 }
 *
 * var users = {
 *   'fred':    { 'user': 'fred',    'age': 40 },
 *   'pebbles': { 'user': 'pebbles', 'age': 1 }
 * };
 *
 * // using the `_.property` callback shorthand
 * _.mapValues(users, 'age');
 * // => { 'fred': 40, 'pebbles': 1 } (iteration order is not guaranteed)
 */
var mapValues = createObjectMapper();

module.exports = mapValues;

},{"../internal/createObjectMapper":48}],97:[function(require,module,exports){
var baseMerge = require('../internal/baseMerge'),
    createAssigner = require('../internal/createAssigner');

/**
 * Recursively merges own enumerable properties of the source object(s), that
 * don't resolve to `undefined` into the destination object. Subsequent sources
 * overwrite property assignments of previous sources. If `customizer` is
 * provided it's invoked to produce the merged values of the destination and
 * source properties. If `customizer` returns `undefined` merging is handled
 * by the method instead. The `customizer` is bound to `thisArg` and invoked
 * with five arguments: (objectValue, sourceValue, key, object, source).
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The destination object.
 * @param {...Object} [sources] The source objects.
 * @param {Function} [customizer] The function to customize assigned values.
 * @param {*} [thisArg] The `this` binding of `customizer`.
 * @returns {Object} Returns `object`.
 * @example
 *
 * var users = {
 *   'data': [{ 'user': 'barney' }, { 'user': 'fred' }]
 * };
 *
 * var ages = {
 *   'data': [{ 'age': 36 }, { 'age': 40 }]
 * };
 *
 * _.merge(users, ages);
 * // => { 'data': [{ 'user': 'barney', 'age': 36 }, { 'user': 'fred', 'age': 40 }] }
 *
 * // using a customizer callback
 * var object = {
 *   'fruits': ['apple'],
 *   'vegetables': ['beet']
 * };
 *
 * var other = {
 *   'fruits': ['banana'],
 *   'vegetables': ['carrot']
 * };
 *
 * _.merge(object, other, function(a, b) {
 *   if (_.isArray(a)) {
 *     return a.concat(b);
 *   }
 * });
 * // => { 'fruits': ['apple', 'banana'], 'vegetables': ['beet', 'carrot'] }
 */
var merge = createAssigner(baseMerge);

module.exports = merge;

},{"../internal/baseMerge":31,"../internal/createAssigner":41}],98:[function(require,module,exports){
module.exports = require('./functions');

},{"./functions":89}],99:[function(require,module,exports){
var arrayMap = require('../internal/arrayMap'),
    baseDifference = require('../internal/baseDifference'),
    baseFlatten = require('../internal/baseFlatten'),
    bindCallback = require('../internal/bindCallback'),
    keysIn = require('./keysIn'),
    pickByArray = require('../internal/pickByArray'),
    pickByCallback = require('../internal/pickByCallback'),
    restParam = require('../function/restParam');

/**
 * The opposite of `_.pick`; this method creates an object composed of the
 * own and inherited enumerable properties of `object` that are not omitted.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The source object.
 * @param {Function|...(string|string[])} [predicate] The function invoked per
 *  iteration or property names to omit, specified as individual property
 *  names or arrays of property names.
 * @param {*} [thisArg] The `this` binding of `predicate`.
 * @returns {Object} Returns the new object.
 * @example
 *
 * var object = { 'user': 'fred', 'age': 40 };
 *
 * _.omit(object, 'age');
 * // => { 'user': 'fred' }
 *
 * _.omit(object, _.isNumber);
 * // => { 'user': 'fred' }
 */
var omit = restParam(function(object, props) {
  if (object == null) {
    return {};
  }
  if (typeof props[0] != 'function') {
    var props = arrayMap(baseFlatten(props), String);
    return pickByArray(object, baseDifference(keysIn(object), props));
  }
  var predicate = bindCallback(props[0], props[1], 3);
  return pickByCallback(object, function(value, key, object) {
    return !predicate(value, key, object);
  });
});

module.exports = omit;

},{"../function/restParam":2,"../internal/arrayMap":6,"../internal/baseDifference":15,"../internal/baseFlatten":17,"../internal/bindCallback":38,"../internal/pickByArray":64,"../internal/pickByCallback":65,"./keysIn":94}],100:[function(require,module,exports){
var keys = require('./keys'),
    toObject = require('../internal/toObject');

/**
 * Creates a two dimensional array of the key-value pairs for `object`,
 * e.g. `[[key1, value1], [key2, value2]]`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the new array of key-value pairs.
 * @example
 *
 * _.pairs({ 'barney': 36, 'fred': 40 });
 * // => [['barney', 36], ['fred', 40]] (iteration order is not guaranteed)
 */
function pairs(object) {
  object = toObject(object);

  var index = -1,
      props = keys(object),
      length = props.length,
      result = Array(length);

  while (++index < length) {
    var key = props[index];
    result[index] = [key, object[key]];
  }
  return result;
}

module.exports = pairs;

},{"../internal/toObject":67,"./keys":93}],101:[function(require,module,exports){
var baseFlatten = require('../internal/baseFlatten'),
    bindCallback = require('../internal/bindCallback'),
    pickByArray = require('../internal/pickByArray'),
    pickByCallback = require('../internal/pickByCallback'),
    restParam = require('../function/restParam');

/**
 * Creates an object composed of the picked `object` properties. Property
 * names may be specified as individual arguments or as arrays of property
 * names. If `predicate` is provided it's invoked for each property of `object`
 * picking the properties `predicate` returns truthy for. The predicate is
 * bound to `thisArg` and invoked with three arguments: (value, key, object).
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The source object.
 * @param {Function|...(string|string[])} [predicate] The function invoked per
 *  iteration or property names to pick, specified as individual property
 *  names or arrays of property names.
 * @param {*} [thisArg] The `this` binding of `predicate`.
 * @returns {Object} Returns the new object.
 * @example
 *
 * var object = { 'user': 'fred', 'age': 40 };
 *
 * _.pick(object, 'user');
 * // => { 'user': 'fred' }
 *
 * _.pick(object, _.isString);
 * // => { 'user': 'fred' }
 */
var pick = restParam(function(object, props) {
  if (object == null) {
    return {};
  }
  return typeof props[0] == 'function'
    ? pickByCallback(object, bindCallback(props[0], props[1], 3))
    : pickByArray(object, baseFlatten(props));
});

module.exports = pick;

},{"../function/restParam":2,"../internal/baseFlatten":17,"../internal/bindCallback":38,"../internal/pickByArray":64,"../internal/pickByCallback":65}],102:[function(require,module,exports){
var baseGet = require('../internal/baseGet'),
    baseSlice = require('../internal/baseSlice'),
    isFunction = require('../lang/isFunction'),
    isKey = require('../internal/isKey'),
    last = require('../array/last'),
    toPath = require('../internal/toPath');

/**
 * This method is like `_.get` except that if the resolved value is a function
 * it's invoked with the `this` binding of its parent object and its result
 * is returned.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to resolve.
 * @param {*} [defaultValue] The value returned if the resolved value is `undefined`.
 * @returns {*} Returns the resolved value.
 * @example
 *
 * var object = { 'a': [{ 'b': { 'c1': 3, 'c2': _.constant(4) } }] };
 *
 * _.result(object, 'a[0].b.c1');
 * // => 3
 *
 * _.result(object, 'a[0].b.c2');
 * // => 4
 *
 * _.result(object, 'a.b.c', 'default');
 * // => 'default'
 *
 * _.result(object, 'a.b.c', _.constant('default'));
 * // => 'default'
 */
function result(object, path, defaultValue) {
  var result = object == null ? undefined : object[path];
  if (result === undefined) {
    if (object != null && !isKey(path, object)) {
      path = toPath(path);
      object = path.length == 1 ? object : baseGet(object, baseSlice(path, 0, -1));
      result = object == null ? undefined : object[last(path)];
    }
    result = result === undefined ? defaultValue : result;
  }
  return isFunction(result) ? result.call(object) : result;
}

module.exports = result;

},{"../array/last":1,"../internal/baseGet":24,"../internal/baseSlice":35,"../internal/isKey":59,"../internal/toPath":68,"../lang/isFunction":71}],103:[function(require,module,exports){
var isIndex = require('../internal/isIndex'),
    isKey = require('../internal/isKey'),
    isObject = require('../lang/isObject'),
    toPath = require('../internal/toPath');

/**
 * Sets the property value of `path` on `object`. If a portion of `path`
 * does not exist it's created.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to augment.
 * @param {Array|string} path The path of the property to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns `object`.
 * @example
 *
 * var object = { 'a': [{ 'b': { 'c': 3 } }] };
 *
 * _.set(object, 'a[0].b.c', 4);
 * console.log(object.a[0].b.c);
 * // => 4
 *
 * _.set(object, 'x[0].y.z', 5);
 * console.log(object.x[0].y.z);
 * // => 5
 */
function set(object, path, value) {
  if (object == null) {
    return object;
  }
  var pathKey = (path + '');
  path = (object[pathKey] != null || isKey(path, object)) ? [pathKey] : toPath(path);

  var index = -1,
      length = path.length,
      lastIndex = length - 1,
      nested = object;

  while (nested != null && ++index < length) {
    var key = path[index];
    if (isObject(nested)) {
      if (index == lastIndex) {
        nested[key] = value;
      } else if (nested[key] == null) {
        nested[key] = isIndex(path[index + 1]) ? [] : {};
      }
    }
    nested = nested[key];
  }
  return object;
}

module.exports = set;

},{"../internal/isIndex":57,"../internal/isKey":59,"../internal/toPath":68,"../lang/isObject":73}],104:[function(require,module,exports){
var arrayEach = require('../internal/arrayEach'),
    baseCallback = require('../internal/baseCallback'),
    baseCreate = require('../internal/baseCreate'),
    baseForOwn = require('../internal/baseForOwn'),
    isArray = require('../lang/isArray'),
    isFunction = require('../lang/isFunction'),
    isObject = require('../lang/isObject'),
    isTypedArray = require('../lang/isTypedArray');

/**
 * An alternative to `_.reduce`; this method transforms `object` to a new
 * `accumulator` object which is the result of running each of its own enumerable
 * properties through `iteratee`, with each invocation potentially mutating
 * the `accumulator` object. The `iteratee` is bound to `thisArg` and invoked
 * with four arguments: (accumulator, value, key, object). Iteratee functions
 * may exit iteration early by explicitly returning `false`.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Array|Object} object The object to iterate over.
 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
 * @param {*} [accumulator] The custom accumulator value.
 * @param {*} [thisArg] The `this` binding of `iteratee`.
 * @returns {*} Returns the accumulated value.
 * @example
 *
 * _.transform([2, 3, 4], function(result, n) {
 *   result.push(n *= n);
 *   return n % 2 == 0;
 * });
 * // => [4, 9]
 *
 * _.transform({ 'a': 1, 'b': 2 }, function(result, n, key) {
 *   result[key] = n * 3;
 * });
 * // => { 'a': 3, 'b': 6 }
 */
function transform(object, iteratee, accumulator, thisArg) {
  var isArr = isArray(object) || isTypedArray(object);
  iteratee = baseCallback(iteratee, thisArg, 4);

  if (accumulator == null) {
    if (isArr || isObject(object)) {
      var Ctor = object.constructor;
      if (isArr) {
        accumulator = isArray(object) ? new Ctor : [];
      } else {
        accumulator = baseCreate(isFunction(Ctor) ? Ctor.prototype : undefined);
      }
    } else {
      accumulator = {};
    }
  }
  (isArr ? arrayEach : baseForOwn)(object, function(value, index, object) {
    return iteratee(accumulator, value, index, object);
  });
  return accumulator;
}

module.exports = transform;

},{"../internal/arrayEach":5,"../internal/baseCallback":12,"../internal/baseCreate":14,"../internal/baseForOwn":20,"../lang/isArray":70,"../lang/isFunction":71,"../lang/isObject":73,"../lang/isTypedArray":75}],105:[function(require,module,exports){
var baseValues = require('../internal/baseValues'),
    keys = require('./keys');

/**
 * Creates an array of the own enumerable property values of `object`.
 *
 * **Note:** Non-object values are coerced to objects.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property values.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.values(new Foo);
 * // => [1, 2] (iteration order is not guaranteed)
 *
 * _.values('hi');
 * // => ['h', 'i']
 */
function values(object) {
  return baseValues(object, keys(object));
}

module.exports = values;

},{"../internal/baseValues":37,"./keys":93}],106:[function(require,module,exports){
var baseValues = require('../internal/baseValues'),
    keysIn = require('./keysIn');

/**
 * Creates an array of the own and inherited enumerable property values
 * of `object`.
 *
 * **Note:** Non-object values are coerced to objects.
 *
 * @static
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property values.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.valuesIn(new Foo);
 * // => [1, 2, 3] (iteration order is not guaranteed)
 */
function valuesIn(object) {
  return baseValues(object, keysIn(object));
}

module.exports = valuesIn;

},{"../internal/baseValues":37,"./keysIn":94}],107:[function(require,module,exports){
/**
 * This method returns the first argument provided to it.
 *
 * @static
 * @memberOf _
 * @category Utility
 * @param {*} value Any value.
 * @returns {*} Returns `value`.
 * @example
 *
 * var object = { 'user': 'fred' };
 *
 * _.identity(object) === object;
 * // => true
 */
function identity(value) {
  return value;
}

module.exports = identity;

},{}],108:[function(require,module,exports){
var baseProperty = require('../internal/baseProperty'),
    basePropertyDeep = require('../internal/basePropertyDeep'),
    isKey = require('../internal/isKey');

/**
 * Creates a function that returns the property value at `path` on a
 * given object.
 *
 * @static
 * @memberOf _
 * @category Utility
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new function.
 * @example
 *
 * var objects = [
 *   { 'a': { 'b': { 'c': 2 } } },
 *   { 'a': { 'b': { 'c': 1 } } }
 * ];
 *
 * _.map(objects, _.property('a.b.c'));
 * // => [2, 1]
 *
 * _.pluck(_.sortBy(objects, _.property(['a', 'b', 'c'])), 'a.b.c');
 * // => [1, 2]
 */
function property(path) {
  return isKey(path) ? baseProperty(path) : basePropertyDeep(path);
}

module.exports = property;

},{"../internal/baseProperty":33,"../internal/basePropertyDeep":34,"../internal/isKey":59}],109:[function(require,module,exports){
var AudioContext = window.AudioContext || window.webkitAudioContext

module.exports = WebAudioAnalyser

function WebAudioAnalyser(audio, ctx, opts) {
  if (!(this instanceof WebAudioAnalyser)) return new WebAudioAnalyser(audio, ctx, opts)
  if (!(ctx instanceof AudioContext)) (opts = ctx), (ctx = null)

  opts = opts || {}
  this.ctx = ctx = ctx || new AudioContext

  if (!(audio instanceof AudioNode)) {
    audio = audio instanceof Audio
      ? ctx.createMediaElementSource(audio)
      : ctx.createMediaStreamSource(audio)
  }

  this.analyser = ctx.createAnalyser()
  this.stereo   = !!opts.stereo
  this.audible  = opts.audible !== false
  this.wavedata = null
  this.freqdata = null
  this.splitter = null
  this.merger   = null
  this.source   = audio

  if (!this.stereo) {
    this.output = this.source
    this.source.connect(this.analyser)
    if (this.audible)
      this.analyser.connect(ctx.destination)
  } else {
    this.analyser = [this.analyser]
    this.analyser.push(ctx.createAnalyser())

    this.splitter = ctx.createChannelSplitter(2)
    this.merger   = ctx.createChannelMerger(2)
    this.output   = this.merger

    this.source.connect(this.splitter)

    for (var i = 0; i < 2; i++) {
      this.splitter.connect(this.analyser[i], i, 0)
      this.analyser[i].connect(this.merger, 0, i)
    }

    if (this.audible)
      this.merger.connect(ctx.destination)
  }
}

WebAudioAnalyser.prototype.waveform = function(output, channel) {
  if (!output) output = this.wavedata || (
    this.wavedata = new Uint8Array((this.analyser[0] || this.analyser).frequencyBinCount)
  )

  var analyser = this.stereo
    ? this.analyser[channel || 0]
    : this.analyser

  analyser.getByteTimeDomainData(output)

  return output
}

WebAudioAnalyser.prototype.frequencies = function(output, channel) {
  if (!output) output = this.freqdata || (
    this.freqdata = new Uint8Array((this.analyser[0] || this.analyser).frequencyBinCount)
  )

  var analyser = this.stereo
    ? this.analyser[channel || 0]
    : this.analyser

  analyser.getByteFrequencyData(output)

  return output
}

},{}],110:[function(require,module,exports){
"use strict";

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _srcCubesCubeField = require("src/cubes/CubeField");

var _srcCubesCubeField2 = _interopRequireDefault(_srcCubesCubeField);

var _srcCubesFieldGroup = require("src/cubes/FieldGroup");

var _srcCubesFieldGroup2 = _interopRequireDefault(_srcCubesFieldGroup);

var _srcAudioAudioAnalyser = require("src/audio/AudioAnalyser");

var _srcAudioAudioAnalyser2 = _interopRequireDefault(_srcAudioAudioAnalyser);

var _srcAudioAudioTexture = require("src/audio/AudioTexture");

var _srcAudioAudioTexture2 = _interopRequireDefault(_srcAudioAudioTexture);

var _srcWagner = require("src/Wagner");

var _srcWagner2 = _interopRequireDefault(_srcWagner);

var renderer = new THREE.WebGLRenderer();
var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1.0, 1000.0);
camera.position.z = 100;
var scene = new THREE.Scene();

/////////// SETUP ////////////////
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

_srcWagner2["default"].vertexShadersPath = '/beats/post/vertex-shaders';
_srcWagner2["default"].fragmentShadersPath = '/beats/post/fragment-shaders';

var composer = new _srcWagner2["default"].Composer(renderer);
composer.setSize(window.innerWidth, window.innerHeight);

var circBlur = new _srcWagner2["default"].FullBoxBlurPass();
circBlur.params.amount = 4;

var toon = new _srcWagner2["default"].NoisePass();

/////////// SET SCENE ////////////////
var group = new _srcCubesFieldGroup2["default"]();

//top field
var topField = new _srcCubesCubeField2["default"](renderer);
topField.setPosition("top");

//bottom field
var bottomField = new _srcCubesCubeField2["default"](renderer);
bottomField.setPosition("bottom");

group.addField("top", topField);
group.addField("bottom", bottomField);

var audio = new _srcAudioAudioTexture2["default"]("mp3/littlesecrets.mp3");

group.connectAudio(audio);

group.addTo(scene);

window.addEventListener("keydown", function (e) {
    audio.toggleAudio();
});

/////////// RENDER ////////////////
animate();
function animate() {
    requestAnimationFrame(animate);
    //renderer.render(scene,camera);
    camera.lookAt(scene.position);

    composer.reset();
    composer.render(scene, camera);
    composer.pass(circBlur);
    composer.pass(toon);
    composer.toScreen();

    group.render(renderer);
}

},{"src/Wagner":111,"src/audio/AudioAnalyser":112,"src/audio/AudioTexture":113,"src/cubes/CubeField":114,"src/cubes/FieldGroup":116}],111:[function(require,module,exports){
'use strict';

var WAGNER = WAGNER || {};

WAGNER.vertexShadersPath = './vertex-shaders';
WAGNER.fragmentShadersPath = './fragment-shaders';
WAGNER.assetsPath = './assets';

WAGNER.log = function () {
	//console.log( Array.prototype.slice.call( arguments ).join( ' ' ) );
};

WAGNER.Composer = function (renderer, settings) {

	this.width = 1;
	this.height = 1;

	this.settings = settings || {};
	this.useRGBA = this.settings.useRGBA || false;

	this.renderer = renderer;
	this.copyPass = new WAGNER.CopyPass(this.settings);

	this.scene = new THREE.Scene();
	this.quad = new THREE.Mesh(new THREE.PlaneBufferGeometry(1, 1), this.defaultMaterial);
	this.scene.add(this.quad);
	this.camera = new THREE.OrthographicCamera(1, 1, 1, 1, -10000, 10000);

	this.front = new THREE.WebGLRenderTarget(1, 1, {
		minFilter: this.settings.minFilter !== undefined ? this.settings.minFilter : THREE.LinearFilter,
		magFilter: this.settings.magFilter !== undefined ? this.settings.magFilter : THREE.LinearFilter,
		wrapS: this.settings.wrapS !== undefined ? this.settings.wrapS : THREE.ClampToEdgeWrapping,
		wrapT: this.settings.wrapT !== undefined ? this.settings.wrapT : THREE.ClampToEdgeWrapping,
		format: this.useRGBA ? THREE.RGBAFormat : THREE.RGBFormat,
		type: this.settings.type !== undefined ? this.settings.type : THREE.UnsignedByteType,
		stencilBuffer: this.settings.stencilBuffer !== undefined ? this.settings.stencilBuffer : true
	});

	this.back = this.front.clone();

	this.startTime = Date.now();

	this.passes = {};
};

WAGNER.Composer.prototype.linkPass = function (id, pass) {

	function WagnerLoadPassException(message) {
		this.message = 'Pass "' + id + '" already loaded.';
		this.name = "WagnerLoadPassException";
		this.toString = function () {
			return this.message;
		};
	}

	if (this.passes[id]) {
		throw new WagnerLoadPassException(id, pass);
	}

	this.passes[id] = pass;
};

WAGNER.Composer.prototype.swapBuffers = function () {

	this.output = this.write;
	this.input = this.read;

	var t = this.write;
	this.write = this.read;
	this.read = t;
};

WAGNER.Composer.prototype.render = function (scene, camera, keep, output) {

	if (this.copyPass.isLoaded()) {
		if (keep) this.swapBuffers();
		this.renderer.render(scene, camera, output ? output : this.write, true);
		if (!output) this.swapBuffers();
	}
};

WAGNER.Composer.prototype.toScreen = function () {

	if (this.copyPass.isLoaded()) {
		this.quad.material = this.copyPass.shader;
		this.quad.material.uniforms.tInput.value = this.read;
		this.quad.material.uniforms.resolution.value.set(this.width, this.height);
		this.renderer.render(this.scene, this.camera);
	}
};

WAGNER.Composer.prototype.toTexture = function (t) {

	if (this.copyPass.isLoaded()) {
		this.quad.material = this.copyPass.shader;
		this.quad.material.uniforms.tInput.value = this.read;
		this.renderer.render(this.scene, this.camera, t, false);
	}
};

WAGNER.Composer.prototype.pass = function (pass) {

	if (pass instanceof WAGNER.Stack) {

		this.passStack(pass);
	} else {

		if (typeof pass === 'string') {
			this.quad.material = this.passes[pass];
		}
		if (pass instanceof THREE.ShaderMaterial) {
			this.quad.material = pass;
		}
		if (pass instanceof WAGNER.Pass) {
			if (!pass.isLoaded()) return;
			pass.run(this);
			return;
		}

		if (!pass.isSim) this.quad.material.uniforms.tInput.value = this.read;

		this.quad.material.uniforms.resolution.value.set(this.width, this.height);
		this.quad.material.uniforms.time.value = 0.001 * (Date.now() - this.startTime);
		this.renderer.render(this.scene, this.camera, this.write, false);
		this.swapBuffers();
	}
};

WAGNER.Composer.prototype.passStack = function (stack) {

	stack.getPasses().forEach((function (pass) {

		this.pass(pass);
	}).bind(this));
};

WAGNER.Composer.prototype.reset = function () {

	this.read = this.front;
	this.write = this.back;

	this.output = this.write;
	this.input = this.read;
};

WAGNER.Composer.prototype.setSource = function (src) {

	if (this.copyPass.isLoaded()) {
		this.quad.material = this.copyPass.shader;
		this.quad.material.uniforms.tInput.value = src;
		this.renderer.render(this.scene, this.camera, this.write, true);
		this.swapBuffers();
	}
};

WAGNER.Composer.prototype.setSize = function (w, h) {

	this.width = w;
	this.height = h;

	this.camera.projectionMatrix.makeOrthographic(w / -2, w / 2, h / 2, h / -2, this.camera.near, this.camera.far);
	this.quad.scale.set(w, h, 1);

	var rt = this.front.clone();
	rt.width = w;
	rt.height = h;
	if (this.quad.material instanceof WAGNER.Pass) this.quad.material.uniforms.tInput.value = rt;
	this.front = rt;

	rt = this.back.clone();
	rt.width = w;
	rt.height = h;
	this.back = rt;
};

WAGNER.Composer.prototype.defaultMaterial = new THREE.MeshBasicMaterial();

WAGNER.loadShader = function (file, callback) {

	var oReq = new XMLHttpRequest();
	oReq.onload = (function () {
		var content = oReq.responseText;
		callback(content);
	}).bind(this);
	oReq.onerror = function () {

		function WagnerLoadShaderException(f) {
			this.message = 'Shader "' + f + '" couldn\'t be loaded.';
			this.name = "WagnerLoadShaderException";
			this.toString = function () {
				return this.message;
			};
		}
		throw new WagnerLoadShaderException(file);
	};
	oReq.onabort = function () {

		function WagnerLoadShaderException(f) {
			this.message = 'Shader "' + f + '" load was aborted.';
			this.name = "WagnerLoadShaderException";
			this.toString = function () {
				return this.message;
			};
		}
		throw new WagnerLoadShaderException(file);
	};
	oReq.open('get', file, true);
	oReq.send();
};

WAGNER.processShader = function (vertexShaderCode, fragmentShaderCode) {

	WAGNER.log('Processing Shader | Performing uniform Reflection...');

	var regExp = /uniform\s+([^\s]+)\s+([^\s]+)\s*;/gi;
	var regExp2 = /uniform\s+([^\s]+)\s+([^\s]+)\s*\[\s*(\w+)\s*\]*\s*;/gi;

	var typesMap = {

		sampler2D: { type: 't', value: function value() {
				return new THREE.Texture();
			} },
		samplerCube: { type: 't', value: function value() {} },

		bool: { type: 'b', value: function value() {
				return 0;
			} },
		int: { type: 'i', value: function value() {
				return 0;
			} },
		float: { type: 'f', value: function value() {
				return 0;
			} },

		vec2: { type: 'v2', value: function value() {
				return new THREE.Vector2();
			} },
		vec3: { type: 'v3', value: function value() {
				return new THREE.Vector3();
			} },
		vec4: { type: 'v4', value: function value() {
				return new THREE.Vector4();
			} },

		bvec2: { type: 'v2', value: function value() {
				return new THREE.Vector2();
			} },
		bvec3: { type: 'v3', value: function value() {
				return new THREE.Vector3();
			} },
		bvec4: { type: 'v4', value: function value() {
				return new THREE.Vector4();
			} },

		ivec2: { type: 'v2', value: function value() {
				return new THREE.Vector2();
			} },
		ivec3: { type: 'v3', value: function value() {
				return new THREE.Vector3();
			} },
		ivec4: { type: 'v4', value: function value() {
				return new THREE.Vector4();
			} },

		mat2: { type: 'v2', value: function value() {
				return new THREE.Matrix2();
			} },
		mat3: { type: 'v3', value: function value() {
				return new THREE.Matrix3();
			} },
		mat4: { type: 'v4', value: function value() {
				return new THREE.Matrix4();
			} }

	};

	var arrayTypesMap = {
		float: { type: 'fv', value: function value() {
				return [];
			} },
		vec3: { type: 'v3v', value: function value() {
				return [];
			} }
	};

	var matches;
	var uniforms = {
		resolution: { type: 'v2', value: new THREE.Vector2(1, 1), 'default': true },
		time: { type: 'f', value: Date.now(), 'default': true },
		tInput: { type: 't', value: new THREE.Texture(), 'default': true }
	};

	var uniformType, uniformName, arraySize;

	while ((matches = regExp.exec(fragmentShaderCode)) !== null) {
		if (matches.index === regExp.lastIndex) {
			regExp.lastIndex++;
		}
		uniformType = matches[1];
		uniformName = matches[2];
		WAGNER.log('  > SINGLE', uniformType, uniformName);
		uniforms[uniformName] = {
			type: typesMap[uniformType].type,
			value: typesMap[uniformType].value()
		};
	}

	while ((matches = regExp2.exec(fragmentShaderCode)) !== null) {
		if (matches.index === regExp.lastIndex) {
			regExp.lastIndex++;
		}
		uniformType = matches[1];
		uniformName = matches[2];
		arraySize = matches[3];
		WAGNER.log('  > ARRAY', arraySize, uniformType, uniformName);
		uniforms[uniformName] = {
			type: arrayTypesMap[uniformType].type,
			value: arrayTypesMap[uniformType].value()
		};
	}

	WAGNER.log('Uniform reflection completed. Compiling...');

	var shader = new THREE.ShaderMaterial({
		uniforms: uniforms,
		vertexShader: vertexShaderCode,
		fragmentShader: fragmentShaderCode,
		shading: THREE.FlatShading,
		depthWrite: false,
		depthTest: false,
		transparent: true,
		derivatives: true
	});

	WAGNER.log('Compiled');

	return shader;
};

WAGNER.Pass = function () {

	WAGNER.log('Pass constructor');
	this.shader = null;
	this.loaded = null;
	this.params = {};
	this.isSim = false;
};

WAGNER.Pass.prototype.loadShader = function (id, c) {

	var self = this;
	var vs = 'varying vec2 vUv; void main() { vUv = uv; gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 ); }';
	WAGNER.loadShader(WAGNER.fragmentShadersPath + '/' + id, function (fs) {
		self.shader = WAGNER.processShader(vs, fs);
		//self.mapUniforms( self.shader.uniforms );
		if (c) c.apply(self);
	});
};

WAGNER.Pass.prototype.mapUniforms = function (uniforms) {

	var params = this.params;

	for (var j in uniforms) {
		if (!uniforms[j]['default']) {
			(function (id) {
				Object.defineProperty(params, id, {
					get: function get() {
						return uniforms[id].value;
					},
					set: function set(v) {
						uniforms[id].value = v;
					},
					configurable: false
				});
			})(j);
		}
	}
};

WAGNER.Pass.prototype.run = function (c) {

	//WAGNER.log( 'Pass run' );
	c.pass(this.shader);
};

WAGNER.Pass.prototype.isLoaded = function () {

	if (this.loaded === null) {
		if (this.shader instanceof THREE.ShaderMaterial) {
			this.loaded = true;
		}
	} else {
		return this.loaded;
	}
};

WAGNER.Pass.prototype.getOfflineTexture = function (w, h, useRGBA) {

	var rtTexture = new THREE.WebGLRenderTarget(w, h, {
		minFilter: THREE.LinearFilter,
		magFilter: THREE.LinearFilter,
		format: useRGBA ? THREE.RGBAFormat : THREE.RGBFormat
	});

	return rtTexture;
};

WAGNER.CopyPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('CopyPass constructor');
	this.loadShader('copy-fs.glsl');
};

WAGNER.CopyPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.GenericPass = function (fragmentShaderSource, c) {

	WAGNER.Pass.call(this);
	var self = this;
	WAGNER.loadShader(WAGNER.vertexShadersPath + '/orto-vs.glsl', function (vs) {
		WAGNER.loadShader(fragmentShaderSource, function (fs) {
			self.shader = WAGNER.processShader(vs, fs);
			if (c) c.apply(self);
		});
	});
};

WAGNER.GenericPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.Stack = function (shadersPool) {

	this.passItems = [];
	this.shadersPool = shadersPool;
	this.passes = [];
};

WAGNER.Stack.prototype.addPass = function (shaderName, enabled, params, index) {

	var length,
	    passItem = {
		shaderName: shaderName,
		enabled: enabled || false
	};

	//todo: use and store params values

	this.passItems.push(passItem);
	length = this.passItems.length;

	this.updatePasses();

	if (index) {

		return this.movePassToIndex(this.passItems[length], index);
	} else {

		return length - 1;
	}
};

WAGNER.Stack.prototype.removePass = function (index) {

	this.passItems.splice(index, 1);
	this.updatePasses();
};

WAGNER.Stack.prototype.enablePass = function (index) {

	this.passItems[index].enabled = true;
	this.updatePasses();
};

WAGNER.Stack.prototype.disablePass = function (index) {

	this.passItems[index].enabled = false;
	this.updatePasses();
};

WAGNER.Stack.prototype.isPassEnabled = function (index) {

	return this.passItems[index].enabled;
};

WAGNER.Stack.prototype.movePassToIndex = function (index, destIndex) {

	this.passItems.splice(destIndex, 0, this.passItems.splice(index, 1)[0]);
	this.updatePasses();
	return destIndex; //TODO: check if destIndex is final index
};

WAGNER.Stack.prototype.reverse = function () {

	this.passItems.reverse();
	this.updatePasses();
};

WAGNER.Stack.prototype.updatePasses = function () {

	this.passes = this.shadersPool.getPasses(this.passItems);

	// init default params for new passItems
	this.passItems.forEach((function (passItem, index) {

		if (passItem.params === undefined) {

			passItem.params = JSON.parse(JSON.stringify(this.passes[index].params)); // clone params without reference to the real shader instance params
			// console.log('updatePasses', passItem, passItem.params);
		}
	}).bind(this));

	// console.log('Updated stack passes list from shaders pool. Stack contains', this.passes.length, 'shaders, and there are', this.shadersPool.availableShaders.length, 'shaders in the pool.');
};

WAGNER.Stack.prototype.getPasses = function () {

	return this.passes;
};

WAGNER.ShadersPool = function () {

	this.availableShaders = [];
};

WAGNER.ShadersPool.prototype.getPasses = function (passItems) {

	var pass,
	    passes = [];

	this.availableShaders.forEach(function (availableShader) {

		availableShader.used = false;
	});

	if (passItems) {

		passItems.forEach((function (passItem, index) {

			if (passItem.enabled) {

				pass = this.getShaderFromPool(passItem.shaderName);

				if (passItem.params) {

					pass.params = this.extendParams(pass.params, passItem.params);
				}

				passes.push(pass);
			}
		}).bind(this));
	}

	return passes;
};

WAGNER.ShadersPool.prototype.getShaderFromPool = function (shaderName) {

	var pass, shaderItem;

	if (shaderName && WAGNER[shaderName]) {

		for (var i = this.availableShaders.length - 1; i >= 0; i--) {

			shaderItem = this.availableShaders[i];

			if (!shaderItem.used && shaderItem.name === shaderName) {

				shaderItem.used = true;
				pass = shaderItem.pass;
				break;
			}
		};

		if (!pass) {

			pass = new WAGNER[shaderName]();

			shaderItem = {
				pass: pass,
				name: shaderName,
				used: true
			};

			this.availableShaders.push(shaderItem);
		}

		return pass;
	}
};

WAGNER.ShadersPool.prototype.extendParams = function (target, source) {

	var obj = {},
	    i = 0,
	    il = arguments.length,
	    key;

	for (; i < il; i++) {

		for (key in arguments[i]) {

			if (arguments[i].hasOwnProperty(key)) {

				obj[key] = arguments[i][key];
			}
		}
	}

	return obj;
};

WAGNER.BlendPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('BlendPass constructor');
	this.loadShader('blend-fs.glsl');

	this.params.mode = 1;
	this.params.opacity = 1;
	this.params.tInput2 = null;
	this.params.resolution2 = new THREE.Vector2();
	this.params.sizeMode = 1;
	this.params.aspectRatio = 1;
	this.params.aspectRatio2 = 1;
};

WAGNER.BlendMode = {
	Normal: 1,
	Dissolve: 2,
	Darken: 3,
	Multiply: 4,
	ColorBurn: 5,
	LinearBurn: 6,
	DarkerColor: 7,
	Lighten: 8,
	Screen: 9,
	ColorDodge: 10,
	LinearDodge: 11,
	LighterColor: 12,
	Overlay: 13,
	SoftLight: 14,
	HardLight: 15,
	VividLight: 16,
	LinearLight: 17,
	PinLight: 18,
	HardMix: 19,
	Difference: 20,
	Exclusion: 21,
	Substract: 22,
	Divide: 23
};

WAGNER.BlendPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.BlendPass.prototype.run = function (c) {

	this.shader.uniforms.mode.value = this.params.mode;
	this.shader.uniforms.opacity.value = this.params.opacity;
	this.shader.uniforms.tInput2.value = this.params.tInput2;
	this.shader.uniforms.sizeMode.value = this.params.sizeMode;
	this.shader.uniforms.aspectRatio.value = this.params.aspectRatio;
	this.shader.uniforms.aspectRatio2.value = this.params.aspectRatio2;
	c.pass(this.shader);
};

WAGNER.InvertPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('InvertPass constructor');
	this.loadShader('invert-fs.glsl');
};

WAGNER.InvertPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.SymetricPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('SymetricPass constructor');
	this.loadShader('symetric-fs.glsl');

	this.params.xReverse = false;
	this.params.yReverse = false;
	this.params.xMirror = false;
	this.params.yMirror = false;
	this.params.mirrorCenter = new THREE.Vector2(0.5, 0.5);
	this.params.angle = 0;
};

WAGNER.SymetricPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.SymetricPass.prototype.run = function (c) {

	this.shader.uniforms.xReverse.value = this.params.xReverse;
	this.shader.uniforms.yReverse.value = this.params.yReverse;
	this.shader.uniforms.xMirror.value = this.params.xMirror;
	this.shader.uniforms.yMirror.value = this.params.yMirror;
	this.shader.uniforms.mirrorCenter.value = this.params.mirrorCenter;
	this.shader.uniforms.angle.value = this.params.angle;

	c.pass(this.shader);
};

WAGNER.SepiaPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('SepiaPass constructor');
	this.loadShader('sepia-fs.glsl');

	this.params.amount = 1;
};

WAGNER.SepiaPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.SepiaPass.prototype.run = function (c) {

	this.shader.uniforms.amount.value = this.params.amount;
	c.pass(this.shader);
};

WAGNER.BrightnessContrastPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('BrightnessContrastPass constructor');
	this.loadShader('brightness-contrast-fs.glsl');

	this.params.brightness = 1;
	this.params.contrast = 1;
};

WAGNER.BrightnessContrastPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.BrightnessContrastPass.prototype.run = function (c) {

	this.shader.uniforms.brightness.value = this.params.brightness;
	this.shader.uniforms.contrast.value = this.params.contrast;

	c.pass(this.shader);
};

WAGNER.Pass.prototype.bindUniform = function (p, s, v, c) {

	Object.defineProperty(p, v, {
		get: function get() {
			return s.uniforms[id].value;
		},
		set: c,
		configurable: false
	});
};

WAGNER.NoisePass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('Noise Pass constructor');
	this.loadShader('noise-fs.glsl');

	this.params.amount = 0.1;
	this.params.speed = 0;
};

WAGNER.NoisePass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.NoisePass.prototype.run = function (c) {

	this.shader.uniforms.amount.value = this.params.amount;
	this.shader.uniforms.speed.value = this.params.speed;
	c.pass(this.shader);
};

WAGNER.VignettePass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('Vignette Pass constructor');
	this.loadShader('vignette-fs.glsl');

	this.params.amount = 1;
	this.params.falloff = 0.1;
};

WAGNER.VignettePass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.VignettePass.prototype.run = function (c) {

	this.shader.uniforms.amount.value = this.params.amount;
	this.shader.uniforms.falloff.value = this.params.falloff;
	c.pass(this.shader);
};

WAGNER.Vignette2Pass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('Vignette Pass constructor');
	this.loadShader('vignette2-fs.glsl');

	this.params.boost = 2;
	this.params.reduction = 2;
};

WAGNER.Vignette2Pass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.Vignette2Pass.prototype.run = function (c) {

	this.shader.uniforms.boost.value = this.params.boost;
	this.shader.uniforms.reduction.value = this.params.reduction;
	c.pass(this.shader);
};

WAGNER.DenoisePass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('Denoise Pass constructor');
	this.loadShader('denoise-fs.glsl');

	this.params.exponent = 5;
	this.params.strength = 10;
};

WAGNER.DenoisePass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.DenoisePass.prototype.run = function (c) {

	this.shader.uniforms.exponent.value = this.params.exponent;
	this.shader.uniforms.strength.value = this.params.strength;
	c.pass(this.shader);
};

WAGNER.BoxBlurPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('BoxBlurPass Pass constructor');
	this.loadShader('box-blur2-fs.glsl');
	this.params.delta = new THREE.Vector2(0, 0);
	this.params.taps = 1;
};

WAGNER.BoxBlurPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.BoxBlurPass.prototype.run = function (c) {

	this.shader.uniforms.delta.value.copy(this.params.delta);
	/*for( var j = 0; j < this.params.taps; j++ ) {
 	this.shader.uniforms.delta.value.copy( this.params.delta );
 	c.pass( this.shader );
 }*/
	c.pass(this.shader);
};

WAGNER.FullBoxBlurPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('FullBoxBlurPass Pass constructor');
	this.boxPass = new WAGNER.BoxBlurPass();
	this.params.amount = 20;
	this.params.taps = 1;
};

WAGNER.FullBoxBlurPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.FullBoxBlurPass.prototype.isLoaded = function () {

	if (this.boxPass.isLoaded()) {
		this.loaded = true;
	}
	return WAGNER.Pass.prototype.isLoaded.call(this);
};

WAGNER.FullBoxBlurPass.prototype.run = function (c) {

	var s = this.params.amount;
	this.boxPass.params.delta.set(s, 0);
	this.boxPass.params.taps = this.params.taps;
	c.pass(this.boxPass);
	this.boxPass.params.delta.set(0, s);
	c.pass(this.boxPass);
};

WAGNER.ZoomBlurPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('ZoomBlurPass Pass constructor');
	this.loadShader('zoom-blur-fs.glsl');

	this.params.center = new THREE.Vector2(0.5, 0.5);
	this.params.strength = 2;
};

WAGNER.ZoomBlurPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.ZoomBlurPass.prototype.run = function (c) {

	this.shader.uniforms.center.value.copy(this.params.center);
	this.shader.uniforms.strength.value = this.params.strength;
	c.pass(this.shader);
};

WAGNER.MultiPassBloomPass = function (w, h) {

	WAGNER.Pass.call(this);
	WAGNER.log('MultiPassBloomPass Pass constructor');

	this.composer = null;

	this.tmpTexture = this.getOfflineTexture(w, h, true);
	this.blurPass = new WAGNER.FullBoxBlurPass();
	this.blendPass = new WAGNER.BlendPass();
	this.zoomBlur = new WAGNER.ZoomBlurPass();
	this.brightnessContrastPass = new WAGNER.BrightnessContrastPass();

	this.width = w || 512;
	this.height = h || 512;

	this.params.blurAmount = 20;
	this.params.applyZoomBlur = false;
	this.params.zoomBlurStrength = 2;
	this.params.useTexture = false;
	this.params.zoomBlurCenter = new THREE.Vector2(0, 0);
	this.params.blendMode = WAGNER.BlendMode.Screen;
};

WAGNER.MultiPassBloomPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.MultiPassBloomPass.prototype.isLoaded = function () {

	if (this.blurPass.isLoaded() && this.blendPass.isLoaded() && this.zoomBlur.isLoaded()) {
		this.loaded = true;
	}
	return WAGNER.Pass.prototype.isLoaded.call(this);
};

WAGNER.MultiPassBloomPass.prototype.run = function (c) {

	if (!this.composer) {
		this.composer = new WAGNER.Composer(c.renderer, { useRGBA: true });
		this.composer.setSize(this.width, this.height);
		//this.composer.setSize( this.tmpTexture.width, this.tmpTexture.height );
	}

	/*var s = 0.5;
 if( c.width != this.tmpTexture.width / s || c.height != this.tmpTexture.height / s ) {
 	this.tmpTexture  = this.getOfflineTexture( c.width * s, c.height * s, true );
 	this.composer.setSize( this.tmpTexture.width, this.tmpTexture.height );
 }*/

	this.composer.reset();

	if (this.params.useTexture === true) {
		this.composer.setSource(this.params.glowTexture);
	} else {
		this.composer.setSource(c.output);
		/*this.brightnessContrastPass.params.brightness = -1;
  this.brightnessContrastPass.params.contrast = 5;
  this.composer.pass( this.brightnessContrastPass );*/
	}

	this.blurPass.params.amount = this.params.blurAmount;
	this.composer.pass(this.blurPass);

	if (this.params.applyZoomBlur) {
		this.zoomBlur.params.center.set(.5 * this.composer.width, .5 * this.composer.height);
		this.zoomBlur.params.strength = this.params.zoomBlurStrength;
		this.composer.pass(this.zoomBlur);
	}

	if (this.params.useTexture === true) {
		this.blendPass.params.mode = WAGNER.BlendMode.Screen;
		this.blendPass.params.tInput = this.params.glowTexture;
		c.pass(this.blendPass);
	}

	this.blendPass.params.mode = this.params.blendMode;
	this.blendPass.params.tInput2 = this.composer.output;
	c.pass(this.blendPass);
};

WAGNER.CGAPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('CGA Pass constructor');
	this.loadShader('cga-fs.glsl', function () {
		this.shader.uniforms.pixelDensity.value = window.devicePixelRatio;
	});
};

WAGNER.CGAPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.SobelEdgeDetectionPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('SobelEdgeDetectionPass Pass constructor');
	this.loadShader('sobel-fs.glsl');
};

WAGNER.SobelEdgeDetectionPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.FreiChenEdgeDetectionPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('FreiChenEdgeDetectionPass Pass constructor');
	this.loadShader('frei-chen-fs.glsl');
};

WAGNER.FreiChenEdgeDetectionPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.DirtPass = function () {

	WAGNER.Pass.call(this);
	this.blendPass = new WAGNER.BlendPass();
	this.dirtTexture = THREE.ImageUtils.loadTexture(WAGNER.assetsPath + '/textures/dirt8.jpg');

	this.params.blendMode = WAGNER.BlendMode.SoftLight;
};

WAGNER.DirtPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.DirtPass.prototype.isLoaded = function () {

	if (this.blendPass.isLoaded()) {
		this.loaded = true;
	}
	return WAGNER.Pass.prototype.isLoaded.call(this);
};

WAGNER.DirtPass.prototype.run = function (c) {

	this.blendPass.params.sizeMode = 1;
	this.blendPass.params.mode = this.params.blendMode;
	this.blendPass.params.tInput2 = this.dirtTexture;
	if (this.dirtTexture.image) {
		this.blendPass.params.resolution2.set(this.dirtTexture.image.width, this.dirtTexture.image.height);
		this.blendPass.params.aspectRatio2 = this.dirtTexture.image.width / this.dirtTexture.image.height;
	}
	this.blendPass.params.aspectRatio = c.read.width / c.read.height;
	c.pass(this.blendPass);
};

WAGNER.GuidedBoxBlurPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('GuidedBoxBlurPass Pass constructor');
	this.loadShader('guided-box-blur2-fs.glsl');

	this.params.tBias = null;
	this.params.delta = new THREE.Vector2(1., 0);
	this.params.invertBiasMap = false;
	this.params.isPacked = 0;
	this.params.from = 0;
	this.params.to = 1;
};

WAGNER.GuidedBoxBlurPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.GuidedBoxBlurPass.prototype.run = function (c) {

	this.shader.uniforms.tBias.value = this.params.tBias, this.shader.uniforms.delta.value.copy(this.params.delta);
	this.shader.uniforms.delta.value.multiplyScalar(.0001);
	this.shader.uniforms.invertBiasMap.value = this.params.invertBiasMap;
	this.shader.uniforms.isPacked.value = this.params.isPacked;
	this.shader.uniforms.from.value = this.params.from;
	this.shader.uniforms.to.value = this.params.to;
	c.pass(this.shader);
};

WAGNER.GuidedFullBoxBlurPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('FullBoxBlurPass Pass constructor');
	this.guidedBoxPass = new WAGNER.GuidedBoxBlurPass();

	this.params.tBias = null;
	this.params.invertBiasMap = false;
	this.params.isPacked = 0;
	this.params.amount = 10;
	this.params.from = 0;
	this.params.to = 1;
	this.params.taps = 1;
};

WAGNER.GuidedFullBoxBlurPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.GuidedFullBoxBlurPass.prototype.isLoaded = function () {

	if (this.guidedBoxPass.isLoaded()) {
		this.loaded = true;
	}
	return WAGNER.Pass.prototype.isLoaded.call(this);
};

WAGNER.GuidedFullBoxBlurPass.prototype.run = function (c) {

	this.guidedBoxPass.params.invertBiasMap = this.params.invertBiasMap;
	this.guidedBoxPass.params.isPacked = this.params.isPacked;
	this.guidedBoxPass.params.tBias = this.params.tBias;
	this.guidedBoxPass.params.from = this.params.from;
	this.guidedBoxPass.params.to = this.params.to;
	var s = this.params.amount;
	for (var j = 0; j < this.params.taps; j++) {
		this.guidedBoxPass.params.delta.set(s, 0);
		c.pass(this.guidedBoxPass);
		this.guidedBoxPass.params.delta.set(0, s);
		c.pass(this.guidedBoxPass);
	}
};

WAGNER.PixelatePass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('PixelatePass Pass constructor');
	this.loadShader('pixelate-fs.glsl');

	this.params.amount = 320;
};

WAGNER.PixelatePass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.PixelatePass.prototype.run = function (c) {

	this.shader.uniforms.amount.value = this.params.amount;
	c.pass(this.shader);
};

WAGNER.RGBSplitPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('RGBSplitPass Pass constructor');
	this.loadShader('rgb-split-fs.glsl', function () {});

	this.params.delta = new THREE.Vector2();
};

WAGNER.RGBSplitPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.RGBSplitPass.prototype.run = function (c) {

	this.shader.uniforms.delta.value.copy(this.params.delta);
	c.pass(this.shader);
};

/*

https://www.shadertoy.com/view/XssGz8

Simulates Chromatic Aberration by linearly interpolating blur-weights from red to green to blue.
Original idea by Kusma: https://github.com/kusma/vlee/blob/master/data/postprocess.fx
Barrel Blur forked from https://www.shadertoy.com/view/XslGz8

*/

WAGNER.ChromaticAberrationPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('ChromaticAberrationPass Pass constructor');
	this.loadShader('chromatic-aberration-fs.glsl');
};

WAGNER.ChromaticAberrationPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.BarrelBlurPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('BarrelBlurPass Pass constructor');
	this.loadShader('barrel-blur-fs.glsl');
};

WAGNER.BarrelBlurPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.OldVideoPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('OldVideoPass Pass constructor');
	this.loadShader('old-video-fs.glsl');
};

WAGNER.OldVideoPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.DotScreenPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('DotScreenPass Pass constructor');
	this.loadShader('dot-screen-fs.glsl');
};

WAGNER.DotScreenPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.PoissonDiscBlurPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('PoissonDiscBlurPass Pass constructor');
	this.loadShader('poisson-disc-blur-fs.glsl');
};

WAGNER.PoissonDiscBlurPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.CircularBlurPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('CircularBlurPass Pass constructor');
	this.loadShader('circular-blur-fs.glsl');
};

WAGNER.CircularBlurPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.ToonPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('ToonPass Pass constructor');
	this.loadShader('toon-fs.glsl');
};

WAGNER.ToonPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.FXAAPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('FXAA Pass constructor');
	this.loadShader('fxaa-fs.glsl');
};

WAGNER.FXAAPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.HighPassPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('HighPass Pass constructor');
	this.loadShader('high-pass-fs.glsl');
};

WAGNER.HighPassPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.GrayscalePass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('GrayscalePass Pass constructor');
	this.loadShader('grayscale-fs.glsl');
};

WAGNER.GrayscalePass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.ASCIIPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('ASCIIPass Pass constructor');
	this.loadShader('ascii-fs.glsl', function () {
		this.shader.uniforms.tAscii.value = THREE.ImageUtils.loadTexture(WAGNER.assetsPath + '/ascii/8x16_ascii_font_sorted.gif');
	});
};

WAGNER.ASCIIPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.LEDPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('LEDPass Pass constructor');
	this.loadShader('led-fs.glsl', function () {

		//this.shader.uniforms.noiseTexture.value = 1;
	});

	this.params.pixelSize = 10;
	this.params.tolerance = .25;
	this.params.pixelRadius = .25;
	this.params.luminanceSteps = 100;
	this.params.luminanceBoost = .2;
	this.params.colorBoost = .01;
	this.params.burntOutPercent = 50;
};

WAGNER.LEDPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.LEDPass.prototype.run = function (c) {

	this.shader.uniforms.pixelSize.value = this.params.pixelSize;
	this.shader.uniforms.tolerance.value = this.params.tolerance;
	this.shader.uniforms.pixelRadius.value = this.params.pixelRadius;
	this.shader.uniforms.luminanceSteps.value = this.params.luminanceSteps;
	this.shader.uniforms.luminanceBoost.value = this.params.luminanceBoost;
	this.shader.uniforms.colorBoost.value = this.params.colorBoost;
	this.shader.uniforms.burntOutPercent.value = this.params.burntOutPercent;

	c.pass(this.shader);
};

WAGNER.HalftonePass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('HalftonePass Pass constructor');
	this.loadShader('halftone-fs.glsl', function () {
		this.shader.uniforms.pixelSize.value = 6;
	});
};

WAGNER.HalftonePass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.Halftone2Pass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('Halftone2Pass Pass constructor');
	this.loadShader('halftone2-fs.glsl');

	this.params.amount = 128;
	this.params.smoothness = .25;
};

WAGNER.Halftone2Pass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.Halftone2Pass.prototype.run = function (c) {

	this.shader.uniforms.amount.value = this.params.amount;
	this.shader.uniforms.smoothness.value = this.params.smoothness;

	c.pass(this.shader);
};

WAGNER.HalftoneCMYKPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('HalftoneCMYKPass Pass constructor');
	this.loadShader('halftone-cmyk-fs.glsl', function () {});
};

WAGNER.HalftoneCMYKPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.CrossFadePass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('CrossFadePass Pass constructor');
	this.loadShader('crossfade-fs.glsl');

	this.params.tInput2 = null;
	this.params.tFadeMap = null;
	this.params.amount = 0;
};

WAGNER.CrossFadePass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.CrossFadePass.prototype.run = function (c) {

	this.shader.uniforms.tInput2.value = this.params.tInput2;
	this.shader.uniforms.tFadeMap.value = this.params.tFadeMap;
	this.shader.uniforms.amount.value = this.params.amount;

	c.pass(this.shader);
};

WAGNER.SSAOPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('SSAOPass Pass constructor');
	this.loadShader('ssao-fs.glsl', function (fs) {

		/*this.shader.uniforms.pSphere.value = [
  	new THREE.Vector3(-0.010735935, 0.01647018, 0.0062425877),
  	new THREE.Vector3(-0.06533369, 0.3647007, -0.13746321),
  	new THREE.Vector3(-0.6539235, -0.016726388, -0.53000957),
  	new THREE.Vector3(0.40958285, 0.0052428036, -0.5591124),
  	new THREE.Vector3(-0.1465366, 0.09899267, 0.15571679),
  	new THREE.Vector3(-0.44122112, -0.5458797, 0.04912532),
  	new THREE.Vector3(0.03755566, -0.10961345, -0.33040273),
  	new THREE.Vector3(0.019100213, 0.29652783, 0.066237666),
  	new THREE.Vector3(0.8765323, 0.011236004, 0.28265962),
  	new THREE.Vector3(0.29264435, -0.40794238, 0.15964167)
  ];*/

	});

	this.params.texture = null;
	this.params.isPacked = false;
	this.params.onlyOcclusion = false;

	this.blurPass = new WAGNER.FullBoxBlurPass();
	this.blendPass = new WAGNER.BlendPass();
	this.composer = null;
};

WAGNER.SSAOPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.SSAOPass.prototype.run = function (c) {

	if (!this.composer) {
		var s = 4;
		this.composer = new WAGNER.Composer(c.renderer, { useRGBA: true });
		this.composer.setSize(c.width / s, c.height / s);
	}

	this.composer.reset();

	this.composer.setSource(c.output);

	this.shader.uniforms.tDepth.value = this.params.texture;
	//this.shader.uniforms.isPacked.value = this.params.isPacked;
	this.shader.uniforms.onlyOcclusion.value = this.params.onlyOcclusion;
	this.composer.pass(this.shader);

	this.blurPass.params.amount = .1;
	this.composer.pass(this.blurPass);

	if (this.params.onlyOcclusion) {
		c.setSource(this.composer.output);
	} else {
		this.blendPass.params.mode = WAGNER.BlendMode.Multiply;
		this.blendPass.params.tInput2 = this.composer.output;

		c.pass(this.blendPass);
	}
};

WAGNER.SimpleSSAOPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('SimpleSSAOPass Pass constructor');
	this.loadShader('ssao-simple-fs.glsl', function (fs) {});

	this.params.texture = null;
	this.params.onlyOcclusion = 0;
	this.params.zNear = 1;
	this.params.zFar = 10000;
	this.params.strength = 1;
};

WAGNER.SimpleSSAOPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.SimpleSSAOPass.prototype.run = function (c) {

	this.shader.uniforms.tDepth.value = this.params.texture;
	//	this.shader.uniforms.onlyOcclusion.value = this.params.onlyOcclusion;
	this.shader.uniforms.zNear.value = this.params.zNear;
	this.shader.uniforms.zFar.value = this.params.zFar;
	this.shader.uniforms.strength.value = this.params.strength;

	c.pass(this.shader);
};

WAGNER.DirectionalBlurPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('Directional Blur Pass constructor');
	this.loadShader('guided-directional-blur-fs.glsl', function (fs) {});

	this.params.tBias = null;
	this.params.delta = .1;
};

WAGNER.DirectionalBlurPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.DirectionalBlurPass.prototype.run = function (c) {

	this.shader.uniforms.tBias.value = this.params.tBias;
	this.shader.uniforms.delta.value = this.params.delta;

	c.pass(this.shader);
};

WAGNER.BleachPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('Bleach Pass constructor');
	this.loadShader('bleach-fs.glsl', function (fs) {});

	this.params.amount = 1;
};

WAGNER.BleachPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.BleachPass.prototype.run = function (c) {

	this.shader.uniforms.amount.value = this.params.amount;

	c.pass(this.shader);
};

WAGNER.DOFPass = function () {

	WAGNER.Pass.call(this);
	WAGNER.log('DOFPass Pass constructor');
	this.loadShader('dof-fs.glsl');

	this.params.focalDistance = 0;
	this.params.aperture = .005;
	this.params.tBias = null;
	this.params.blurAmount = 1;
};

WAGNER.DOFPass.prototype = Object.create(WAGNER.Pass.prototype);

WAGNER.DOFPass.prototype.run = function (c) {

	this.shader.uniforms.tBias.value = this.params.tBias;
	this.shader.uniforms.focalDistance.value = this.params.focalDistance;
	this.shader.uniforms.aperture.value = this.params.aperture;
	this.shader.uniforms.blurAmount.value = this.params.blurAmount;

	this.shader.uniforms.delta.value.set(1, 0);
	c.pass(this.shader);

	this.shader.uniforms.delta.value.set(0, 1);
	c.pass(this.shader);
};

module.exports = WAGNER;

},{}],112:[function(require,module,exports){

/**
 * Analyses the audio stream in prep for drawing.
 * utilizes Hugh Kenenedy's web-audio-analyser and adapted from
 * Felix Turner's web audio example.
 *
 * https://www.npmjs.com/package/web-audio-analyser
 * http://www.airtightinteractive.com/demos/js/uberviz/audioanalysis/js/AudioHandler.js
 */
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var lodash = require("lodash/object");
var audioAnalyser = require("web-audio-analyser");

var AudioAnalyser = (function () {
    function AudioAnalyser(source) {
        _classCallCheck(this, AudioAnalyser);

        //create audio element
        var audio = new Audio();
        audio.crossOrigin = 'Anonymous';
        audio.src = source;
        document.body.appendChild(audio);

        //init new web-audio-analyser
        var a = new audioAnalyser(audio);

        //apply properties from web-audio-analyser directly onto the object
        this._applyProps(a);

        //audio element
        this.audio = audio;

        this.avgLevel = 0;
        this.volumeSensitivity = 1;
        this.binCount = this.analyser.frequencyBinCount;
        this.levelsCount = 16; //should be factor of 512
        this.levelBins = Math.floor(this.binCount / this.levelsCount); //number of bins in each level
        this.width = this.analyser.frequencyBinCount / 4;
        this.levelsCount = 16;

        //levels of each frequency - from 0 - 1 . no sound is 0.
        this.levelsData = [];

        //bars - bar data is from 0 - 256 in 512 bins. no sound is 0;
        this.freqByteData = new Uint8Array(this.binCount);

        //waveform - waveform data is from 0-256 for 512 bins. no sound is 128.
        this.timeByteData = new Uint8Array(this.binCount);

        //waveform - from 0 - 1 . no sound is 0.5. Array [binCount]
        this.dataWaveform = [];

        //history of all the levels at various points
        this.levelHistory = [];

        //////// BPM STUFF ///////////
        this.BEAT_HOLD_TIME = 40; //num of frames to hold a beat
        this.BEAT_DECAY_RATE = 0.98;
        this.BEAT_MIN = 0.15; //a volume less than this is no beat

        this.count = 0;
        this.msecsFirst = 0;
        this.msecsPrevious = 0;
        this.msecsAvg = 633; //time between beats (msec)
        this.gotBeat = false;
        this.beatCutOff = 0;
        this.beatTime = 0;
        this.beatHoldTime = 40;
        this.beatDecayRate = 0.97;
        this.bpmStart = 0;
    }

    /**
     * Updates our audio data. Meant to be used within a loop
     */

    _createClass(AudioAnalyser, [{
        key: "update",
        value: function update() {
            if (this.isPlaying) {
                this.waveform(this.timeByteData);
                this.frequencies(this.freqByteData);

                this.updateWaveform();
                this.normalizeLevels();
            }
        }
    }, {
        key: "getLevelsArray",
        value: function getLevelsArray() {
            return this.levelsData;
        }

        /**
         * Returns the data at the specific frequency
         * @param index
         * @returns {*}
         */
    }, {
        key: "getChannelData",
        value: function getChannelData(index) {
            return this.levelsData[index];
        }
    }, {
        key: "getFrequencyData",
        value: function getFrequencyData(index) {
            return this.freqByteData[index];
        }
    }, {
        key: "getWaveformData",
        value: function getWaveformData(index) {
            return this.dataWaveform[index];
        }
    }, {
        key: "getLevelHistory",
        value: function getLevelHistory(index) {
            return this.levelHistory[index];
        }

        ///////////////////////////////

    }, {
        key: "_applyProps",
        value: function _applyProps(a) {
            lodash.merge(this, a);
            lodash.merge(this.__proto__, a.__proto__);
        }
    }, {
        key: "play",
        value: function play() {

            if (this.isPlaying) {
                this.pause();
                this.isPlaying = false;
            } else {

                this.audio.play();
                this.isPlaying = true;
            }

            return this;
        }
    }, {
        key: "pause",
        value: function pause() {
            this.audio.pause();
            this.isPlaying = false;
        }

        /**
         * Updates the waveform information
         */
    }, {
        key: "updateWaveform",
        value: function updateWaveform() {
            for (var i = 0; i < this.binCount; i++) {
                this.dataWaveform[i] = (this.timeByteData[i] - 128) / 128 * this.volumeSensitivity;
            }
        }
    }, {
        key: "beatDetect",
        value: function beatDetect() {
            if (this.avgLevel > this.beatCutOff && this.avgLevel > this.BEAT_MIN) {
                this.gotBeat = true;
                this.beatCutOff = this.avgLevel * 1.1;
                this.beatTime = 0;
            } else {
                if (this.beatTime <= this.beatHoldTime) {
                    this.beatTime++;
                } else {
                    this.beatCutOff *= this.beatDecayRate;
                    this.beatCutOff = Math.max(this.beatCutOff, this.BEAT_MIN);
                }
            }
            this.bpmTime = (new Date().getTime() - this.bpmStart) / this.msecsAvg;
        }

        /**
         * Normalize the levels
         */
    }, {
        key: "normalizeLevels",
        value: function normalizeLevels() {
            for (var i = 0; i < this.levelsCount; i++) {
                var sum = 0;
                for (var j = 0; j < this.levelBins; j++) {
                    sum += this.freqByteData[i * this.levelBins + j];
                }
                this.levelsData[i] = sum / this.levelBins / 256 * this.volumeSensitivity; //freqData maxs at 256

                //adjust for the fact that lower levels are percieved more quietly
                //make lower levels smaller
                //levelsData[i] *=  1 + (i/levelsCount)/2;
            }
        }
    }, {
        key: "getAverageLevel",
        value: function getAverageLevel() {
            //GET AVG LEVEL
            var sum = 0;
            for (var j = 0; j < this.levelsCount; j++) {
                sum += this.levelsData[j];
            }

            this.avgLevel = sum / this.levelsCount;
            this.levelHistory.push(this.avgLevel);
            this.levelHistory.shift(1);
        }
    }]);

    return AudioAnalyser;
})();

exports["default"] = AudioAnalyser;
module.exports = exports["default"];

},{"lodash/object":77,"web-audio-analyser":109}],113:[function(require,module,exports){
/**
 * Builds a texture that holds audio data from audio files.
 * Holds onto it's own audio and analyser elements
 */
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _srcAudioAudioAnalyser = require("src/audio/AudioAnalyser");

var _srcAudioAudioAnalyser2 = _interopRequireDefault(_srcAudioAudioAnalyser);

var lodash = require("lodash/object");

var AudioTexture = (function () {
    function AudioTexture(audio, options) {
        _classCallCheck(this, AudioTexture);

        var defaults = {
            format: THREE.RGBAFormat,
            type: THREE.FloatType
        };

        options = options !== undefined ? lodash.merge(options, defaults) : defaults;

        var analyser = new _srcAudioAudioAnalyser2["default"](audio);
        options.width = analyser.binCount / 4;

        this.audio = analyser;
        this.options = options;

        var audioData = this.processAudio();
        this.texture = new THREE.DataTexture(audioData, audioData.length / 16, 1, options.format, options.type);
        this.texture.needsUpdate = true;
    }

    _createClass(AudioTexture, [{
        key: "getLevels",
        value: function getLevels() {
            return this.audio.levelsData;
        }

        /**
         * Returns the levels data for the specified chanel.
         * The assumption is a 16 channel setup at the moment.
         * TODO might have to revisit if there are comps with more than 16 channel audio
         * @param index
         * @returns {*}
         */
    }, {
        key: "getChannelData",
        value: function getChannelData(index) {
            if (index < 16) {
                return this.audio.getChannelData(index);
            } else if (index === 16) {
                return this.audio.getChannelData(index - 1);
            } else {
                console.error("AudioTexture::getChannelData - specified channel does not exist");
            }
        }

        /**
         * Returns true/false depending on whether or not the audio
         * is playing.
         * @returns {boolean}
         */
    }, {
        key: "isPlaying",
        value: function isPlaying() {
            return this.audio.isPlaying;
        }

        /**
         * Toggles playback.
         * Triggers update as well at the same time
         */
    }, {
        key: "toggleAudio",
        value: function toggleAudio() {
            this.update();
            this.audio.play();
        }
    }, {
        key: "update",
        value: function update() {
            this.audio.update();
            this.texture.image.data = this.processAudio();
            this.texture.needsUpdate = true;
        }
    }, {
        key: "getTexture",
        value: function getTexture() {
            return this.texture;
        }
    }, {
        key: "processAudio",
        value: function processAudio() {
            var options = this.options;
            var audio = this.audio;
            var audioData = new Float32Array(options.width * 4);

            for (var i = 0; i < options.width; i += 4) {
                audioData[i] = audio.getChannelData(i / 4);
                audioData[i + 1] = audio.getChannelData(i / 4 + 1);
                audioData[i + 2] = audio.getChannelData(i / 4 + 2);
                audioData[i + 3] = audio.getChannelData(i / 4 + 3);
            }

            return audioData;
        }
    }]);

    return AudioTexture;
})();

exports["default"] = AudioTexture;
module.exports = exports["default"];

},{"lodash/object":77,"src/audio/AudioAnalyser":112}],114:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _srcCubesCubeItem = require("src/cubes/CubeItem");

var _srcCubesCubeItem2 = _interopRequireDefault(_srcCubesCubeItem);

var _srcAudioAudioTexture = require("src/audio/AudioTexture");

var _srcAudioAudioTexture2 = _interopRequireDefault(_srcAudioAudioTexture);



var CubeField = (function () {
    function CubeField(renderer, width, height, resolution) {
        _classCallCheck(this, CubeField);

        renderer = renderer !== undefined ? renderer : false;
        if (renderer === false) {

            console.error("Cube field needs a renderer");
            return false;
        }

        this.width = width !== undefined ? width : 400;
        this.height = height !== undefined ? height : 400;
        this.resolution = resolution !== undefined ? resolution : 48;

        this._buildRenderTarget(renderer);

        //positions that make up the grid
        this.positions = [];

        // CubeItem representations of all the elements in the field
        this.cubes = [];

        //the final grouping of objects
        this.group = new THREE.Object3D();

        //build the geometry for teh field
        this._buildGeometry();

        //build the rendering plane
        this._buildRenderPlane();
    }

    /**
     * Sets the audio texture used to manipulate the field
     */

    _createClass(CubeField, [{
        key: "connectAudio",
        value: function connectAudio(audioTexture) {
            if (audioTexture instanceof _srcAudioAudioTexture2["default"]) {

                /**
                 * Loop through the field and assign one channel to a cube.
                 * If we run out of channels(which is likely) duplicate assignments
                 * and/or use waveform or history data.
                 * @type {Array}
                 */
                var cubes = this.cubes;
                var len = cubes.length;

                var maxChannels = 16;
                var count = 0;

                for (var i = 0; i < len; ++i) {
                    if (count < maxChannels) {
                        cubes[i].setAudio(audioTexture);
                        cubes[i].setChannel(count);
                        count++;
                    } else {
                        count = 0;
                        count++;
                    }
                }

                this.audioTexture = audioTexture;
            } else {
                console.error("variable audioTexture is not a instance of AudioTexture");
            }
        }

        /**
         * Updates / renders the field.
         * Meant to be used within a loop
         */
    }, {
        key: "update",
        value: function update() {
            var cubes = this.cubes;
            var len = cubes.length;

            var d = new Date();
            for (var i = 0; i < len; ++i) {
                var index = cubes[i].getChannel();

                cubes[i].updateSize(this.audioTexture.getChannelData(index));
                //cubes[i].updateSize(performance.now() * 0.000001);
            }
        }
    }, {
        key: "_buildRenderPlane",
        value: function _buildRenderPlane() {
            var renderVertex = "#define GLSLIFY 1\nvoid main() {\n\tvec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );\n\tgl_Position = projectionMatrix * mvPosition;\n}\n";
            var renderFragment = "#define GLSLIFY 1\nuniform vec2 resolution;\nuniform float time;\nuniform sampler2D cubes;\nuniform sampler2D stars;\n\nvoid main()\t{\n\n\tvec2 uv = gl_FragCoord.xy / resolution.xy;\n\n\tvec3 color = texture2D( cubes, uv ).xyz;\n\n\n\tgl_FragColor = vec4(color,1.0);\n\t//gl_FragColor = vec4( color, 1.0 );\n\n}";

            var positionShader = new THREE.ShaderMaterial({
                vertexShader: renderVertex,
                fragmentShader: renderFragment,
                uniforms: {
                    cubes: {
                        type: 't',
                        value: null
                    },
                    stars: {
                        type: 't',
                        value: null
                    },
                    resolution: {
                        type: 'v2',
                        value: new THREE.Vector2()
                    }
                }
            });

            var renderPlane = new THREE.PlaneBufferGeometry(window.innerWidth, window.innerHeight);
            var renderMesh = new THREE.Mesh(renderPlane, positionShader);

            this.shader = positionShader;
            this.renderMesh = renderMesh;
        }
    }, {
        key: "addTo",
        value: function addTo(scene) {
            scene.add(this.renderMesh);
        }
    }, {
        key: "render",
        value: function render(renderer) {
            var time = Date.now() * 0.001;

            this.camera.rotation.z += 0.001;

            renderer.render(this.scene, this.camera, this.fbo, true);
            //this.shader.uniforms.cubes.value = this.fbo;
            //this.shader.uniforms.resolution.value.x = window.innerWidth;
            //this.shader.uniforms.resolution.value.y = window.innerHeight;

            return this.fbo;
        }
    }, {
        key: "getFbo",
        value: function getFbo() {
            return this.fbo;
        }
    }, {
        key: "_buildGeometry",
        value: function _buildGeometry() {
            var cols = this.width / this.resolution;
            var rows = this.height / this.resolution;

            var vec;
            for (var i = 0; i < cols; ++i) {
                for (var j = 0; j < rows; ++j) {
                    var x = i * this.resolution;
                    var y = j * this.resolution;
                    vec = new THREE.Vector3(x, y, 0);

                    this.positions.push(vec);
                }
            }

            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = this.positions[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var i = _step.value;

                    var cube = new _srcCubesCubeItem2["default"]();

                    cube.mesh.position.set(i.x, i.y, i.z);
                    cube.wireframe.position.set(i.x, i.y, i.z);
                    this.cubes.push(cube);
                    this.group.add(cube.mesh);
                    this.group.add(cube.wireframe);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator["return"]) {
                        _iterator["return"]();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            this.group.position.x -= this.width / 2;
            this.group.position.y -= this.height / 2;
            //  this.group.position.z += this.width / 2;
            //bottom
            //this.group.rotation.set( -Math.PI/2, 0, 0 );
            //this.group.position.set( -this.width / 2, -25, 200 );

            //left
            //this.group.rotation.set(0,-Math.PI/2,0);
            //this.group.position.set(-200,0,-500 );

            //right
            //this.group.rotation.set(0,-Math.PI/2,0);
            //this.group.position.set(200,0,-500 );

            //top
            //this.group.rotation.set( -Math.PI/2, 0, 0 );
            //this.group.position.set( -this.width / 2, this.height / 2, 200 );

            this.scene.add(this.group);
        }

        /**
         * Positions the field either in the top,left,bottom or right. Can also pass in custom position and rotation
         * @param position
         * @param customRotation
         * @param customPosition
         */
    }, {
        key: "setPosition",
        value: function setPosition(position, customRotation, customPosition) {
            switch (position) {
                case "left":
                    this.group.rotation.set(0, -Math.PI / 2, 0);
                    this.group.position.set(-200, 0, -500);
                    break;

                case "right":
                    this.group.rotation.set(0, -Math.PI / 2, 0);
                    this.group.position.set(200, 0, -500);
                    break;

                case "top":
                    this.group.rotation.set(-Math.PI / 2, 0, 0);
                    this.group.position.set(-this.width / 2, this.height / 2, 200);
                    break;
                default:
                    this.group.rotation.set(-Math.PI / 2, 0, 0);
                    this.group.position.set(-this.width / 2, -25, 200);
                    break;
            }

            if (position === undefined) {
                this.group.rotation.set(-Math.PI / 2, 0, 0);
                this.group.position.set(-this.width / 2, -25, 200);
            }

            if (customRotation !== undefined) {
                this.group.rotation.set(customRotation[0], customRotation[1], customRotation[2]);
            }

            if (customPosition !== undefined) {
                this.group.position.set(customPosition[0], customPosition[1], customPosition[2]);
            }
        }
    }, {
        key: "_buildRenderTarget",
        value: function _buildRenderTarget(renderer) {
            var camera = new THREE.PerspectiveCamera(75.0, window.innerWidth / window.innerHeight, 1.0, 5000.0);
            camera.position.z = 200;
            camera.position.y += 100;

            //var camera = new THREE.OrthographicCamera( window.innerWidth / - 2, window.innerWidth / 2, window.innerHeight / 2, window.innerHeight / - 2, -10000, 10000 );
            var scene = new THREE.Scene();

            this.renderer = renderer;
            this.camera = camera;
            this.scene = scene;

            this.fbo = new THREE.WebGLRenderTarget(window.innerWidth, window.innerHeight, {
                minFilter: THREE.NearestFilter,
                magFilter: THREE.NearestFilter,
                format: THREE.RGBFormat,
                type: THREE.FloatType
            });
        }
    }]);

    return CubeField;
})();

exports["default"] = CubeField;
module.exports = exports["default"];

},{"src/audio/AudioTexture":113,"src/cubes/CubeItem":115}],115:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _srcAudioAudioTexture = require("src/audio/AudioTexture");

var _srcAudioAudioTexture2 = _interopRequireDefault(_srcAudioAudioTexture);

//  //z scaling should give height



var CubeItem = (function () {
    function CubeItem(width, height, depth) {
        _classCallCheck(this, CubeItem);

        width = width !== undefined ? width : 20;
        height = height !== undefined ? height : 20;
        depth = depth !== undefined ? depth : 20;

        var box,
            wirebox = null;
        box = new THREE.BoxGeometry(width, height, depth);
        wirebox = new THREE.BoxGeometry(width + 2, height + 2, depth + 2);

        var color = new THREE.Color();

        color.setHSL(Math.random() * 500 / 500, 1.0, 0.5);

        var material = new THREE.ShaderMaterial({
            vertexShader: "#define GLSLIFY 1\n\nvoid main() {\n\tvec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );\n\tgl_Position = projectionMatrix * mvPosition;\n}\n",
            fragmentShader: "#define GLSLIFY 1\nuniform vec2 resolution;\nuniform vec3 color;\nuniform sampler2D audioTexture;\nuniform float time;\n\n\nvoid main()\t{\n\n    vec2 uv = gl_FragCoord.xy / resolution.xy;\n    uv.x -= 1.0;\n    vec4 audio = texture2D(audioTexture,uv);\n\tgl_FragColor = vec4(uv / audio.xy, 5.5 + 0.5 * sin(time),1.0);\n\n}",
            linewidth: 2,
            uniforms: {
                audioTexture: {
                    type: 't',
                    value: null
                },

                color: {
                    type: 'v3',
                    value: new THREE.Vector3(color.r, color.g, color.b)
                },

                channelValue: {
                    type: 'f',
                    value: 0.0
                },

                time: {
                    type: 'f',
                    value: 0
                },
                resolution: {
                    type: 'v2',
                    value: new THREE.Vector2(window.innerWidth, window.innerHeight)
                }
            }
        });

        //var wirematerial = new THREE.MeshBasicMaterial({
        //    color:0xff0000,
        //    wireframe:true,
        //    blending:THREE.AdditiveBlending
        //});

        var wirematerial = new THREE.ShaderMaterial({
            vertexShader: "#define GLSLIFY 1\nuniform float amplitude;\nattribute vec3 displacement;\nattribute vec3 customColor;\nvarying vec3 vColor;\n\nvoid main() {\n\tgl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );\n}\n",
            fragmentShader: "#define GLSLIFY 1\nuniform vec3 color;\nuniform float opacity;\nuniform vec2 resolution;\nuniform sampler2D audioTexture;\nuniform float time;\nvoid main() {\n\n\tvec2 uv = gl_FragCoord.xy / resolution.xy;\n\tvec4 audio = texture2D(audioTexture,uv);\n\n    vec3 cubeColor = color;\n    float red = 0.0;\n    float green = 0.0;\n    float blue = 0.0;\n\n     red += sin( uv.x * cos( time / 15.0 ) * 80.0 ) + cos( uv.y * cos( time / 15.0 ) * 10.0 );\n        \tred += sin( uv.y * sin( time / 10.0 ) * 40.0 ) + cos( uv.x * sin( time / 25.0 ) * 40.0 );\n        \tred += sin( uv.x * sin( time / 5.0 ) * 10.0 ) + sin( uv.y * sin( time / 35.0 ) * 80.0 );\n        \tred *= sin( time / 10.0 ) * 0.5;\n\n\n     green += sin( uv.x * cos( time / 15.0 ) * 80.0 ) + cos( uv.y * cos( time / 15.0 ) * 10.0 );\n           green += sin( uv.y * sin( time / 10.0 ) * 40.0 ) + cos( uv.x * sin( time / 25.0 ) * 40.0 );\n           green += sin( uv.x * sin( time / 5.0 ) * 10.0 ) + sin( uv.y * sin( time / 35.0 ) * 80.0 );\n           green *= sin( time / 10.0 ) * 0.5;\n\n\n\n    blue += sin( uv.x * cos( time / 15.0 ) * 80.0 ) + cos( uv.y * cos( time / 15.0 ) * 10.0 );\n           blue += sin( uv.y * sin( time / 10.0 ) * 40.0 ) + cos( uv.x * sin( time / 25.0 ) * 40.0 );\n           blue += sin( uv.x * sin( time / 5.0 ) * 10.0 ) + sin( uv.y * sin( time / 35.0 ) * 80.0 );\n           blue *= sin( time / 10.0 ) * 0.5;\n\n\n    cubeColor += vec3(red,green,blue);\n\n\n    vec4 finalcubeColor = vec4(cubeColor,1.0);\n\tgl_FragColor = finalcubeColor;\n\n}\n",
            wireframe: true,
            uniforms: {
                audioTexture: {
                    type: 't',
                    value: null
                },

                color: {
                    type: 'v3',
                    value: new THREE.Vector3(color.r, color.g, color.b)
                },

                time: {
                    type: 'f',
                    value: 0
                },
                resolution: {
                    type: 'v2',
                    value: new THREE.Vector2(window.innerWidth, window.innerHeight)
                }
            }
        });

        this.mesh = new THREE.Mesh(box, material, THREE.LineStrip);

        this.wirematerial = wirematerial;
        this.wireframe = new THREE.Mesh(wirebox, wirematerial);

        this.material = material;

        //max height of the bar
        this.maxHeight = 250;

        //make sure maxHeight is normalized
        this.normHeight = new THREE.Vector2(this.maxHeight, 0).normalize().x;

        this.resolution = new THREE.Vector2(window.innerWidth, window.innerHeight);

        var instance = this;
        window.addEventListener("resize", function () {
            instance.resolution.x = window.innerWidth;
            instance.resolution.y = window.innerHeight;
        });

        this.channel = 0;
    }

    /**
     * Sets the audiotexture to be used in the Cube's shader
     * @param audioTexture
     */

    _createClass(CubeItem, [{
        key: "setAudio",
        value: function setAudio(audioTexture) {
            if (audioTexture instanceof _srcAudioAudioTexture2["default"]) {
                this.material.uniforms.audioTexture.value = audioTexture.getTexture();
            }
        }

        /**
         * Sets the channel cube that this cube is listening to
         * @param channel the channel number for the cube
         * @returns {CubeItem}//
         */
    }, {
        key: "setChannel",
        value: function setChannel(channel) {
            this.channel = channel;
            return this;
        }

        /**
         * Returns the channel associated with this cube
         * @returns {number|*}
         */
    }, {
        key: "getChannel",
        value: function getChannel() {
            return this.channel;
        }

        /**
         * Scales up the height of the cube like a level indicator
         * @param val
         */
    }, {
        key: "updateSize",
        value: function updateSize(val) {

            //pass in time to shader
            // this.material.uniforms.time.value =  Date.now() * 0.001;

            this.material.uniforms.time.value = performance.now() * 0.01;
            this.material.uniforms.resolution.value = this.resolution;

            this.wirematerial.uniforms.time.value = performance.now() * 0.01;
            this.wirematerial.uniforms.resolution.value = this.resolution;

            //only update size if value is greater than 0
            if (val > 0) {
                this.mesh.scale.z = val * (0.05 * this.maxHeight);
                this.wireframe.scale.z = val * (0.05 * this.maxHeight);
            }
        }
    }]);

    return CubeItem;
})();

exports["default"] = CubeItem;
module.exports = exports["default"];

},{"src/audio/AudioTexture":113}],116:[function(require,module,exports){

//renders a group of fields on one plane
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _srcCubesCubeField = require("src/cubes/CubeField");

var _srcCubesCubeField2 = _interopRequireDefault(_srcCubesCubeField);

var _srcAudioAudioTexture = require("src/audio/AudioTexture");

var _srcAudioAudioTexture2 = _interopRequireDefault(_srcAudioAudioTexture);



var FieldGroup = (function () {
    function FieldGroup() {
        _classCallCheck(this, FieldGroup);

        var renderVertex = "#define GLSLIFY 1\nvoid main() {\n\tvec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );\n\tgl_Position = projectionMatrix * mvPosition;\n}\n";
        var renderFragment = "#define GLSLIFY 1\nuniform vec2 resolution;\nuniform float time;\nuniform sampler2D top;\nuniform sampler2D bottom;\n\nvoid main()\t{\n\n\tvec2 uv = gl_FragCoord.xy / resolution.xy;\n\n\tvec4 top = texture2D( top, uv );\n\tvec4 bottom = texture2D(bottom,uv);\n\n\n\t/**\n\t *\tBlends two textures together. Oddly much much better than using mix() as you can get full\n\t *  brightness / color by adding textures together after multiplying by a weight value.\n\t *  http://stackoverflow.com/questions/24356075/glsl-shader-interpolate-between-more-than-two-textures\n\t */\n\tvec4 color = (top * 1.0) + (bottom * 1.0);\n\n\tgl_FragColor = color;\n\n}";

        var positionShader = new THREE.ShaderMaterial({
            vertexShader: renderVertex,
            fragmentShader: renderFragment,
            uniforms: {
                fieldTop: {
                    type: 't',
                    value: null
                },

                fieldBottom: {
                    type: 't',
                    value: null
                },

                resolution: {
                    type: 'v2',
                    value: new THREE.Vector2(window.innerWidth, window.innerHeight)
                }
            }
        });

        var renderPlane = new THREE.PlaneBufferGeometry(window.innerWidth, window.innerHeight);
        var renderMesh = new THREE.Mesh(renderPlane, positionShader);

        this.shader = positionShader;
        this.renderMesh = renderMesh;

        this._fields = [];
    }

    _createClass(FieldGroup, [{
        key: "connectAudio",
        value: function connectAudio(audioTexture) {
            if (audioTexture instanceof _srcAudioAudioTexture2["default"]) {
                this.texture = audioTexture;
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = this._fields[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var field = _step.value;

                        field.field.connectAudio(audioTexture);
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator["return"]) {
                            _iterator["return"]();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }
            }
        }
    }, {
        key: "addField",
        value: function addField(name, field) {

            //add a uniform value
            this.shader.uniforms[name] = {
                type: 't',
                value: null
            };

            this._fields.push({
                name: name,
                field: field
            });
        }
    }, {
        key: "addTo",
        value: function addTo(scene) {
            scene.add(this.renderMesh);
        }
    }, {
        key: "render",
        value: function render(renderer) {
            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {

                for (var _iterator2 = this._fields[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                    var field = _step2.value;

                    field.field.update();
                    this.shader.uniforms[field.name].value = field.field.render(renderer);
                }
            } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion2 && _iterator2["return"]) {
                        _iterator2["return"]();
                    }
                } finally {
                    if (_didIteratorError2) {
                        throw _iteratorError2;
                    }
                }
            }

            this.texture.update();
            this.shader.uniforms.resolution.value.x = window.innerWidth;
            this.shader.uniforms.resolution.value.y = window.innerHeight;
        }
    }]);

    return FieldGroup;
})();

exports["default"] = FieldGroup;
module.exports = exports["default"];

},{"src/audio/AudioTexture":113,"src/cubes/CubeField":114}]},{},[110]);

//# sourceMappingURL=bundle.js.map