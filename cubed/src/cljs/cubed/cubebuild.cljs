(ns cubed.cubebuild
  (:require
    [shaders.cubeshader :as cube]))

(defn BuildSkybox []
  (let [urls ["img/px.png"
              "img/nx.png"
              "img/py.png"
              "img/ny.png"
              "img/pz.png"
              "img/nz.png"]
        loader (js/THREE.CubeTextureLoader.)]

    (let [textureCube (.load loader (clj->js urls) nil nil (fn [e]
                                                             (js/console.log "texture load error")))]
      (js/console.log textureCube)
      textureCube)

    ))
(def resolution 30)
(def width 200)
(def height 200)
(def depth 200)

(def shader (cube/getPhongShader))
(def cubeMap (BuildSkybox))
(def shaderUniforms {:diffuse {:type "v4"
                               :value (js/THREE.Vector4. 0.5 0.0 0.0 1.0)}
                     :specular {:type "v4"
                                :value (js/THREE.Vector4. 1.0 1.0 0.0 1.0)}

                     :ambient {:type "v4"
                               :value (js/THREE.Vector4. 1.0 0.0 0.0 1.0)}

                     :lightPos {:type "v3"
                                :value (js/THREE.Vector3. 1.0 1.0 1.0)}

                     :shininess {:type "f"
                                 :value 0.9}
                     :cubemap {:type "t"
                               :value cubeMap}

                     })
(defn MakeCube [width height depth]
  "Creates a Cube based on a width,height and depth"
  (let [geo (js/THREE.BufferGeometry.)
        newGeo (.fromGeometry geo (js/THREE.BoxGeometry. width height depth))
        mat (js/THREE.ShaderMaterial. (clj->js {:vertexShader (:vertexShader shader)
                                                :fragmentShader (:fragmentShader shader)
                                                :uniforms shaderUniforms}))]
    (js/THREE.Mesh. newGeo mat)))



(defn BuildLargeCube []
  "Builds a cube...made up of cubes"
  (let [cubes #js []
        cols (/ width resolution)
        rows (/ height resolution)
        dep (/ depth resolution)]

      (dotimes [i cols]
        (dotimes [j rows]
          (dotimes [p dep]

            (let [x (* i resolution)
                  y (* j resolution)
                  z (* p resolution)
                  randRot (js/Math.random)
                  cube (MakeCube. 20 20 20)]
              (aset cube "rot" randRot)
              (aset (aget cube "position") "x" x)
              (aset (aget cube "position") "y" y)
              (aset (aget cube "position") "z" z)
              (.push cubes cube)
              )))) cubes))


(defn updateLightPosition [cube newLightPos]
  "Update the position of the lighting"
  (let [lightPos (aget (aget (aget cube "material") "uniforms") "lightPos")]
    (aset lightPos "value" newLightPos)))