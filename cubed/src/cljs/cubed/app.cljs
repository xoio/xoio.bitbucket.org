(ns cubed.app
  (:require
    [thi.ng.geom.webgl.animator :as anim]
    [cubed.cubebuild :as cubed]))


(defn getAspectRatio []
  (/ js/window.innerWidth js/window.innerHeight))


(defonce
  core
  (do
    (def envShader (aget js/THREE.ShaderLib "cube"))
    (def renderer (js/THREE.WebGLRenderer.))
    (aset renderer "autoClear" false)
    (.setPixelRatio renderer js/window.devicePixelRatio)


    (def scene (js/THREE.Scene.))
    (def sceneCube (js/THREE.Scene.))
    (def camera (js/THREE.PerspectiveCamera. 60.0 (getAspectRatio) 1.0 10000))
    (def cameraCube (js/THREE.PerspectiveCamera. 60.0 (getAspectRatio) 1.0 10000))
    (aset (aget camera "position") "z" 180)

    (.setSize renderer js/window.innerWidth js/window.innerHeight)
    (.appendChild js/document.body (aget renderer "domElement"))

    (def world (js/THREE.Object3D.))
    (def pivot (js/THREE.Object3D.))
    (def cubes (cubed/BuildLargeCube))

    ;; build skybox
    (let [cubeTex (cubed/BuildSkybox)
          uniforms (aget envShader "uniforms") ]
      (aset (aget uniforms "tCube") "value" cubeTex)
      (let [mat (js/THREE.ShaderMaterial. (clj->js {
                                                    :vertexShader (aget envShader "vertexShader")
                                                    :fragmentShader (aget envShader "fragmentShader")
                                                    :uniforms uniforms
                                                    :depthWrite false
                                                    :side js/THREE.BackSide
                                                    }))
            geo (js/THREE.BoxGeometry. 100 100 100)
            mesh (js/THREE.Mesh. geo mat)]
        (.add sceneCube mesh)

        ))



    ;; build light
    (def lightSphere (js/THREE.SphereBufferGeometry. 3 8 8))
    (def lightMat (js/THREE.MeshBasicMaterial. (clj->js {:color 0xffffff})))
    (def particleLight (js/THREE.Mesh. lightSphere lightMat))
    (def pointLight (js/THREE.PointLight. 0xfffff 2 800))
    (.add particleLight pointLight)
    (.add scene particleLight)

    (doseq [cube cubes]
       (.add pivot cube)
      )

    ))

(.add world pivot)
(def pivotMatrix (js/THREE.Matrix4.))
(.applyMatrix pivot (.makeTranslation pivotMatrix -100 -100 -100))
(.add scene world)


(anim/animate
  (fn [[t frame]]

    (.render renderer sceneCube cameraCube)
    (.render renderer scene camera)



    (aset (aget world "rotation") "x" (* t 0.4))
    (aset (aget world "rotation") "y" (* t 0.4))
    (aset (aget world "rotation") "z" (* t 0.4))




    (aset (aget particleLight "position") "x" (* (js/Math.sin (* 7 (* t 0.05))) 200))
    (aset (aget particleLight "position") "y" (* (js/Math.cos (* 5 (* t 0.05))) 200))
    (aset (aget particleLight "position") "z" (* (js/Math.cos (* 3 (* t 0.05))) 200))
    (doseq [cube cubes]
      (aset (aget cube "rotation") "y" (* t 0.4))
      (aset (aget cube "rotation") "x" (* t 0.4))
      (cubed/updateLightPosition cube (aget particleLight "position"))
      )

true))

(defn init [])
