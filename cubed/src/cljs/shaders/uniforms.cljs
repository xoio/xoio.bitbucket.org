(ns shaders.uniforms
  (:require
    [thi.ng.glsl.core :as glsl :include-macros true :refer-macros [defglsl]]))


(defglsl
  phongUniforms
  []
  "uniform vec3 lightPos;
   uniform vec4 diffuseColor;
   uniform vec4 ambient;
   uniform vec4 specular;
   uniform float shininess;
   uniform samplerCube cubemap;

   ")


(defglsl
  phongVaryings
  []
  "varying vec3 vNormal;
   varying vec4 eye;
   varying vec4 n_eye;")