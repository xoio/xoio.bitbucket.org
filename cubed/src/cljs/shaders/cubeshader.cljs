(ns shaders.cubeshader
  (:require
    [shaders.uniforms :as phong]
    [thi.ng.glsl.core :as glsl :include-macros true :refer-macros [defglsl]]))


(defglsl
  cube-vertex
  []
  "void main(){
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1.0);
  }")


(defglsl
  cube-fragment
  []
  "void main(){
    gl_FragColor = vec4(1.0,1.0,0.0,1.0);
  }")




(defglsl
  cube-phong-vertex
  [phong/phongVaryings]
  "

    void main(){
      vNormal = normalize(normalMatrix * normal);
      eye = -(modelViewMatrix * vec4(position,1.0));
      n_eye = modelViewMatrix * vec4(normal,1.0);
      gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1.0);

    }
  ")

(defglsl
  cube-phong-fragment
  [phong/phongUniforms
   phong/phongVaryings]
  "void main(){
    vec4 spec = vec4(0.);
    vec3 n = normalize(vNormal);
    vec3 e = normalize(vec3(eye));

    float shine = shininess;
    vec3 dir = lightPos;
    vec3 reflected = reflect(e,normalize(vec3(n_eye.xyz)));
    vec4 tex = textureCube(cubemap,reflected);
    float intensity = max(dot(n,dir),0.0);

    if(intensity > 0.0){
      vec3 h = normalize(dir + e);
      float intSpec = max(dot(h,n),0.0);
      spec = specular * pow(intSpec,shine);
    }



    gl_FragColor = max(intensity * diffuseColor + spec,ambient) * tex;

  }"
  )


(defn getPhongShader []
  {:vertexShader (glsl/assemble cube-phong-vertex)
   :fragmentShader (glsl/assemble cube-phong-fragment)})