# ribbontrails

Experiment in Three.js and Ribbon making

## Overview

Based on [Felix Turner's](https://www.airtightinteractive.com/) [experiment](http://airtightinteractive.com/demos/js/noise-ribbons). Mostly the same but instead of simply using the Three.Geometry class, I wanted to
give myself a bit of a challenge and reconstruct everything using BufferGeometry. Almost got it too but couldn't quite figure
out the correct indices.

It's also written in Clojurescript as a test to see what it'd be like to write something creatively oriented.

The compiled source is already in the `public` folder. A naming conflict somehow got introduced in conjunction with the `THREE` object
that I've been meaning to try and figure out, so the source is uncompressed, still using the Closure library and therefore much larger than it normally might be.

## Setup
If you'd like to try and get this running on your machine, you'll need to first download 
[Leiningen](https://github.com/technomancy/leiningen)


To get an interactive development environment run:

    lein figwheel

and open your browser at [localhost:3449](http://localhost:3449/).
This will auto compile and send all changes to the browser without the
need to reload. After the compilation process is complete, you will
get a Browser Connected REPL. An easy way to try it is:

    (js/alert "Am I connected?")

and you should see an alert in the browser window.

To clean all compiled files:

    lein clean

To create a production build run:

    lein cljsbuild once min

And open your browser in `resources/public/index.html`. You will not
get live reloading, nor a REPL.

## License

Copyright © 2014 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
