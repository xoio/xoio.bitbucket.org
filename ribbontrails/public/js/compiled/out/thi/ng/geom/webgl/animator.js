// Compiled by ClojureScript 1.7.170 {}
goog.provide('thi.ng.geom.webgl.animator');
goog.require('cljs.core');
thi.ng.geom.webgl.animator.animframe_provider = (function (){var or__16796__auto__ = window.requestAnimationFrame;
if(cljs.core.truth_(or__16796__auto__)){
return or__16796__auto__;
} else {
var or__16796__auto____$1 = window.webkitRequestAnimationFrame;
if(cljs.core.truth_(or__16796__auto____$1)){
return or__16796__auto____$1;
} else {
var or__16796__auto____$2 = window.mozRequestAnimationFrame;
if(cljs.core.truth_(or__16796__auto____$2)){
return or__16796__auto____$2;
} else {
var or__16796__auto____$3 = window.msRequestAnimationFrame;
if(cljs.core.truth_(or__16796__auto____$3)){
return or__16796__auto____$3;
} else {
return window.oRequestAnimationFrame;
}
}
}
}
})();
thi.ng.geom.webgl.animator.now = (function thi$ng$geom$webgl$animator$now(){
var or__16796__auto__ = performance.now();
if(cljs.core.truth_(or__16796__auto__)){
return or__16796__auto__;
} else {
var or__16796__auto____$1 = performance.webkitNow();
if(cljs.core.truth_(or__16796__auto____$1)){
return or__16796__auto____$1;
} else {
var or__16796__auto____$2 = performance.mozNow();
if(cljs.core.truth_(or__16796__auto____$2)){
return or__16796__auto____$2;
} else {
var or__16796__auto____$3 = performance.msNow();
if(cljs.core.truth_(or__16796__auto____$3)){
return or__16796__auto____$3;
} else {
return performance.oNow();
}
}
}
}
});
thi.ng.geom.webgl.animator.animate = (function thi$ng$geom$webgl$animator$animate(var_args){
var args__17862__auto__ = [];
var len__17855__auto___20570 = arguments.length;
var i__17856__auto___20571 = (0);
while(true){
if((i__17856__auto___20571 < len__17855__auto___20570)){
args__17862__auto__.push((arguments[i__17856__auto___20571]));

var G__20572 = (i__17856__auto___20571 + (1));
i__17856__auto___20571 = G__20572;
continue;
} else {
}
break;
}

var argseq__17863__auto__ = ((((1) < args__17862__auto__.length))?(new cljs.core.IndexedSeq(args__17862__auto__.slice((1)),(0))):null);
return thi.ng.geom.webgl.animator.animate.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__17863__auto__);
});

thi.ng.geom.webgl.animator.animate.cljs$core$IFn$_invoke$arity$variadic = (function (f,p__20568){
var vec__20569 = p__20568;
var element = cljs.core.nth.call(null,vec__20569,(0),null);
var t0 = (new Date()).getTime();
var t = cljs.core.volatile_BANG_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(0),(0)], null));
var f_SINGLEQUOTE_ = ((function (t0,t,vec__20569,element){
return (function thi$ng$geom$webgl$animator$animate_STAR_(){
if(cljs.core.truth_(f.call(null,cljs.core.vreset_BANG_.call(null,t,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(((new Date()).getTime() - t0) * 0.001),(cljs.core.deref.call(null,t).call(null,(1)) + (1))], null))))){
if(cljs.core.truth_(element)){
return thi.ng.geom.webgl.animator.animframe_provider.call(null,thi$ng$geom$webgl$animator$animate_STAR_,element);
} else {
return thi.ng.geom.webgl.animator.animframe_provider.call(null,thi$ng$geom$webgl$animator$animate_STAR_);
}
} else {
return null;
}
});})(t0,t,vec__20569,element))
;
return f_SINGLEQUOTE_.call(null);
});

thi.ng.geom.webgl.animator.animate.cljs$lang$maxFixedArity = (1);

thi.ng.geom.webgl.animator.animate.cljs$lang$applyTo = (function (seq20566){
var G__20567 = cljs.core.first.call(null,seq20566);
var seq20566__$1 = cljs.core.next.call(null,seq20566);
return thi.ng.geom.webgl.animator.animate.cljs$core$IFn$_invoke$arity$variadic(G__20567,seq20566__$1);
});

//# sourceMappingURL=animator.js.map?rel=1448506744474