// Compiled by ClojureScript 1.7.170 {}
goog.provide('thi.ng.geom.core.vector');
goog.require('cljs.core');
goog.require('thi.ng.geom.core');
goog.require('thi.ng.math.core');
goog.require('thi.ng.xerror.core');
thi.ng.geom.core.vector.V2;

thi.ng.geom.core.vector.V3;

thi.ng.geom.core.vector.vec2;

thi.ng.geom.core.vector.vec3;

thi.ng.geom.core.vector.vec2_reduce_STAR_;

thi.ng.geom.core.vector.vec3_reduce_STAR_;

thi.ng.geom.core.vector.swizzle_assoc_STAR_;

thi.ng.geom.core.vector.swizzle2_fns;

thi.ng.geom.core.vector.swizzle3_fns;

/**
* @constructor
 * @implements {thi.ng.geom.core.PScale}
 * @implements {thi.ng.geom.core.PHeading}
 * @implements {thi.ng.geom.core.PMagnitude}
 * @implements {cljs.core.IIndexed}
 * @implements {thi.ng.geom.core.PDistance}
 * @implements {cljs.core.IVector}
 * @implements {cljs.core.IReversible}
 * @implements {thi.ng.geom.core.vector.Object}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {thi.ng.geom.core.PMathOps}
 * @implements {cljs.core.IFn}
 * @implements {thi.ng.geom.core.PNormal}
 * @implements {cljs.core.ICollection}
 * @implements {thi.ng.geom.core.PTranslate}
 * @implements {thi.ng.geom.core.PMinMax}
 * @implements {thi.ng.geom.core.PLimit}
 * @implements {thi.ng.geom.core.PVectorReduce}
 * @implements {thi.ng.geom.core.PPolar}
 * @implements {thi.ng.geom.core.PInterpolate}
 * @implements {thi.ng.geom.core.PTransform}
 * @implements {cljs.core.ICounted}
 * @implements {thi.ng.geom.core.PClear}
 * @implements {thi.ng.geom.core.PDotProduct}
 * @implements {cljs.core.ISeq}
 * @implements {thi.ng.geom.core.PNormalize}
 * @implements {thi.ng.geom.core.PBuffered}
 * @implements {cljs.core.INext}
 * @implements {cljs.core.ISeqable}
 * @implements {thi.ng.geom.core.PRotate}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {thi.ng.geom.core.PCrossProduct}
 * @implements {cljs.core.IStack}
 * @implements {thi.ng.geom.core.PReflect}
 * @implements {thi.ng.geom.core.PMutableMathOps}
 * @implements {thi.ng.math.core.PDeltaEquals}
 * @implements {cljs.core.IComparable}
 * @implements {cljs.core.ISequential}
 * @implements {cljs.core.IWithMeta}
 * @implements {thi.ng.geom.core.PInvert}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.ILookup}
 * @implements {cljs.core.IReduce}
*/
thi.ng.geom.core.vector.Vec2 = (function (buf,_hash,_meta){
this.buf = buf;
this._hash = _hash;
this._meta = _meta;
this.cljs$lang$protocol_mask$partition0$ = 166618075;
this.cljs$lang$protocol_mask$partition1$ = 10240;
})
thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PBuffered$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PBuffered$get_buffer$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.buf;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PBuffered$copy_to_buffer$arity$4 = (function (_,dest,stride,idx){
var self__ = this;
var ___$1 = this;
dest.set(self__.buf,idx);

return (idx + stride);
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PTransform$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PTransform$transform$arity$2 = (function (_,m){
var self__ = this;
var ___$1 = this;
return thi.ng.geom.core.transform_vector.call(null,m,___$1);
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PRotate$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PRotate$rotate$arity$2 = (function (_,theta){
var self__ = this;
var ___$1 = this;
var s = Math.sin(theta);
var c = Math.cos(theta);
var b = (new Float32Array((2)));
var G__18822 = self__.buf;
var G__18823 = (G__18822[(0)]);
var G__18824 = (G__18822[(1)]);
(b[(0)] = ((G__18823 * c) - (G__18824 * s)));

(b[(1)] = ((G__18823 * s) + (G__18824 * c)));

return (new thi.ng.geom.core.vector.Vec2(b,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.toString = (function (){
var self__ = this;
var _ = this;
return [cljs.core.str("["),cljs.core.str((self__.buf[(0)])),cljs.core.str(" "),cljs.core.str((self__.buf[(1)])),cljs.core.str("]")].join('');
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (_,k){
var self__ = this;
var ___$1 = this;
if((k instanceof cljs.core.Keyword)){
var temp__4423__auto__ = thi.ng.geom.core.vector.swizzle2_fns.call(null,k);
if(cljs.core.truth_(temp__4423__auto__)){
var f = temp__4423__auto__;
return f.call(null,___$1);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
} else {
if(((k >= (0))) && ((k < (2)))){
return (self__.buf[k]);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
}
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (_,k,nf){
var self__ = this;
var ___$1 = this;
if((k instanceof cljs.core.Keyword)){
var temp__4423__auto__ = thi.ng.geom.core.vector.swizzle2_fns.call(null,k);
if(cljs.core.truth_(temp__4423__auto__)){
var f = temp__4423__auto__;
return f.call(null,___$1);
} else {
return nf;
}
} else {
if(((k >= (0))) && ((k < (2)))){
return (self__.buf[k]);
} else {
return nf;
}
}
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PDotProduct$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PDotProduct$dot$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var G__18825 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__18826 = v.buf;
return (((G__18825[(0)]) * (G__18826[(0)])) + ((G__18825[(1)]) * (G__18826[(1)])));
} else {
return (((G__18825[(0)]) * cljs.core.nth.call(null,v,(0),0.0)) + ((G__18825[(1)]) * cljs.core.nth.call(null,v,(1),0.0)));
}
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PNormalize$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PNormalize$normalize$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var G__18827 = self__.buf;
var G__18828 = (G__18827[(0)]);
var G__18829 = (G__18827[(1)]);
var l = Math.sqrt(((G__18828 * G__18828) + (G__18829 * G__18829)));
if((l > (0))){
var b = (new Float32Array((2)));
(b[(0)] = (G__18828 / l));

(b[(1)] = (G__18829 / l));

return (new thi.ng.geom.core.vector.Vec2(b,null,self__._meta));
} else {
return ___$1;
}
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PNormalize$normalize$arity$2 = (function (_,len){
var self__ = this;
var ___$1 = this;
var G__18830 = self__.buf;
var G__18831 = (G__18830[(0)]);
var G__18832 = (G__18830[(1)]);
var l = Math.sqrt(((G__18831 * G__18831) + (G__18832 * G__18832)));
if((l > (0))){
var l__$1 = (len / l);
var b = (new Float32Array((2)));
(b[(0)] = (G__18831 * l__$1));

(b[(1)] = (G__18832 * l__$1));

return (new thi.ng.geom.core.vector.Vec2(b,null,self__._meta));
} else {
return ___$1;
}
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PNormalize$normalized_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return thi.ng.math.core.delta_EQ_.call(null,1.0,thi.ng.geom.core.mag_squared.call(null,___$1));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PClear$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PClear$clear_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return (new thi.ng.geom.core.vector.Vec2((new Float32Array((2))),null,null));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PClear$clear_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
(self__.buf[(0)] = 0.0);

(self__.buf[(1)] = 0.0);

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PReflect$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PReflect$reflect$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var b = (new Float32Array((2)));
var G__18833 = self__.buf;
var G__18835 = (G__18833[(0)]);
var G__18836 = (G__18833[(1)]);
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__18834 = v.buf;
var G__18837 = (G__18834[(0)]);
var G__18838 = (G__18834[(1)]);
var d = (((G__18835 * G__18837) + (G__18836 * G__18838)) + (2));
(b[(0)] = ((G__18837 * d) - G__18835));

(b[(1)] = ((G__18838 * d) - G__18836));

return (new thi.ng.geom.core.vector.Vec2(b,null,self__._meta));
} else {
var G__18837 = cljs.core.nth.call(null,v,(0),0.0);
var G__18838 = cljs.core.nth.call(null,v,(1),0.0);
var d = (((G__18835 * G__18837) + (G__18836 * G__18838)) + (2));
(b[(0)] = ((G__18837 * d) - G__18835));

(b[(1)] = ((G__18838 * d) - G__18836));

return (new thi.ng.geom.core.vector.Vec2(b,null,self__._meta));
}
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PCrossProduct$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PCrossProduct$cross$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var G__18839 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__18840 = v.buf;
return (((G__18839[(0)]) * (G__18840[(1)])) - ((G__18839[(1)]) * (G__18840[(0)])));
} else {
return (((G__18839[(0)]) * cljs.core.nth.call(null,v,(1),0.0)) - ((G__18839[(1)]) * cljs.core.nth.call(null,v,(0),0.0)));
}
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (_,n){
var self__ = this;
var ___$1 = this;
if((n >= (0))){
if((n < (2))){
return (self__.buf[n]);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,n);
}
} else {
return null;
}
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (_,n,nf){
var self__ = this;
var ___$1 = this;
if((n >= (0))){
if((n < (2))){
return (self__.buf[n]);
} else {
return nf;
}
} else {
return null;
}
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IVector$_assoc_n$arity$3 = (function (_,n,v){
var self__ = this;
var ___$1 = this;
var b = (new Float32Array(self__.buf));
(b[n] = v);

return (new thi.ng.geom.core.vector.Vec2(b,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__._meta;
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return (new thi.ng.geom.core.vector.Vec2((new Float32Array(self__.buf)),self__._hash,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$INext$_next$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.cons.call(null,(self__.buf[(1)]),null);
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$ICounted$_count$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return (2);
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IStack$_peek$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return (self__.buf[(1)]);
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IStack$_pop$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.with_meta.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [(self__.buf[(0)])], null),self__._meta);
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$__BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
(self__.buf[(0)] = (- (self__.buf[(0)])));

(self__.buf[(1)] = (- (self__.buf[(1)])));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$__BANG_$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var G__18844_19171 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__18845_19172 = v.buf;
(self__.buf[(0)] = ((G__18844_19171[(0)]) - (G__18845_19172[(0)])));

(self__.buf[(1)] = ((G__18844_19171[(1)]) - (G__18845_19172[(1)])));

self__._hash = null;
} else {
if(typeof v === 'number'){
(self__.buf[(0)] = ((G__18844_19171[(0)]) - v));

(self__.buf[(1)] = ((G__18844_19171[(1)]) - v));

self__._hash = null;
} else {
(self__.buf[(0)] = ((G__18844_19171[(0)]) - cljs.core.nth.call(null,v,(0),0.0)));

(self__.buf[(1)] = ((G__18844_19171[(1)]) - cljs.core.nth.call(null,v,(1),0.0)));

self__._hash = null;
}
}

return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$__BANG_$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var G__18853_19173 = typeof v1 === 'number';
var G__18854_19174 = typeof v2 === 'number';
if(((G__18853_19173)?G__18854_19174:false)){
(self__.buf[(0)] = ((self__.buf[(0)]) - v1));

(self__.buf[(1)] = ((self__.buf[(1)]) - v2));
} else {
var G__18855_19175 = ((!(G__18853_19173))?(v1 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__18856_19176 = ((!(G__18854_19174))?(v2 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__18847_19177 = (cljs.core.truth_(G__18855_19175)?v1.buf:null);
var G__18848_19178 = (cljs.core.truth_(G__18856_19176)?v2.buf:null);
var G__18849_19179 = (cljs.core.truth_(G__18855_19175)?(G__18847_19177[(0)]):((G__18853_19173)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__18850_19180 = (cljs.core.truth_(G__18855_19175)?(G__18847_19177[(1)]):((G__18853_19173)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__18851_19181 = (cljs.core.truth_(G__18856_19176)?(G__18848_19178[(0)]):((G__18854_19174)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__18852_19182 = (cljs.core.truth_(G__18856_19176)?(G__18848_19178[(1)]):((G__18854_19174)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
(self__.buf[(0)] = (((self__.buf[(0)]) - G__18849_19179) - G__18851_19181));

(self__.buf[(1)] = (((self__.buf[(1)]) - G__18850_19180) - G__18852_19182));
}

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$_STAR__BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$_STAR__BANG_$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var G__18857_19183 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__18858_19184 = v.buf;
(self__.buf[(0)] = ((G__18857_19183[(0)]) * (G__18858_19184[(0)])));

(self__.buf[(1)] = ((G__18857_19183[(1)]) * (G__18858_19184[(1)])));

self__._hash = null;
} else {
if(typeof v === 'number'){
(self__.buf[(0)] = ((G__18857_19183[(0)]) * v));

(self__.buf[(1)] = ((G__18857_19183[(1)]) * v));

self__._hash = null;
} else {
(self__.buf[(0)] = ((G__18857_19183[(0)]) * cljs.core.nth.call(null,v,(0),0.0)));

(self__.buf[(1)] = ((G__18857_19183[(1)]) * cljs.core.nth.call(null,v,(1),0.0)));

self__._hash = null;
}
}

return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$_STAR__BANG_$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var G__18866_19185 = typeof v1 === 'number';
var G__18867_19186 = typeof v2 === 'number';
if(((G__18866_19185)?G__18867_19186:false)){
(self__.buf[(0)] = ((self__.buf[(0)]) * v1));

(self__.buf[(1)] = ((self__.buf[(1)]) * v2));
} else {
var G__18868_19187 = ((!(G__18866_19185))?(v1 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__18869_19188 = ((!(G__18867_19186))?(v2 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__18860_19189 = (cljs.core.truth_(G__18868_19187)?v1.buf:null);
var G__18861_19190 = (cljs.core.truth_(G__18869_19188)?v2.buf:null);
var G__18862_19191 = (cljs.core.truth_(G__18868_19187)?(G__18860_19189[(0)]):((G__18866_19185)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__18863_19192 = (cljs.core.truth_(G__18868_19187)?(G__18860_19189[(1)]):((G__18866_19185)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__18864_19193 = (cljs.core.truth_(G__18869_19188)?(G__18861_19190[(0)]):((G__18867_19186)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__18865_19194 = (cljs.core.truth_(G__18869_19188)?(G__18861_19190[(1)]):((G__18867_19186)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
(self__.buf[(0)] = (((self__.buf[(0)]) * G__18862_19191) * G__18864_19193));

(self__.buf[(1)] = (((self__.buf[(1)]) * G__18863_19192) * G__18865_19194));
}

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$subm_BANG_$arity$3 = (function (_,a,b){
var self__ = this;
var ___$1 = this;
var G__18879_19195 = (a instanceof thi.ng.geom.core.vector.Vec2);
var G__18880_19196 = (b instanceof thi.ng.geom.core.vector.Vec2);
var G__18881_19197 = ((!(G__18879_19195))?typeof a === 'number':null);
var G__18882_19198 = ((!(G__18880_19196))?typeof b === 'number':null);
var G__18870_19199 = self__.buf;
var G__18871_19200 = ((G__18879_19195)?a.buf:null);
var G__18872_19201 = ((G__18880_19196)?b.buf:null);
var G__18873_19202 = (G__18870_19199[(0)]);
var G__18874_19203 = (G__18870_19199[(1)]);
var G__18875_19204 = ((G__18879_19195)?(G__18871_19200[(0)]):(cljs.core.truth_(G__18881_19197)?a:cljs.core.nth.call(null,a,(0),0.0)));
var G__18876_19205 = ((G__18879_19195)?(G__18871_19200[(1)]):(cljs.core.truth_(G__18881_19197)?a:cljs.core.nth.call(null,a,(1),0.0)));
var G__18877_19206 = ((G__18880_19196)?(G__18872_19201[(0)]):(cljs.core.truth_(G__18882_19198)?b:cljs.core.nth.call(null,b,(0),1.0)));
var G__18878_19207 = ((G__18880_19196)?(G__18872_19201[(1)]):(cljs.core.truth_(G__18882_19198)?b:cljs.core.nth.call(null,b,(1),1.0)));
(self__.buf[(0)] = ((G__18873_19202 - G__18875_19204) * G__18877_19206));

(self__.buf[(1)] = ((G__18874_19203 - G__18876_19205) * G__18878_19207));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$msub_BANG_$arity$3 = (function (_,a,b){
var self__ = this;
var ___$1 = this;
var G__18892_19208 = (a instanceof thi.ng.geom.core.vector.Vec2);
var G__18893_19209 = (b instanceof thi.ng.geom.core.vector.Vec2);
var G__18894_19210 = ((!(G__18892_19208))?typeof a === 'number':null);
var G__18895_19211 = ((!(G__18893_19209))?typeof b === 'number':null);
var G__18883_19212 = self__.buf;
var G__18884_19213 = ((G__18892_19208)?a.buf:null);
var G__18885_19214 = ((G__18893_19209)?b.buf:null);
var G__18886_19215 = (G__18883_19212[(0)]);
var G__18887_19216 = (G__18883_19212[(1)]);
var G__18888_19217 = ((G__18892_19208)?(G__18884_19213[(0)]):(cljs.core.truth_(G__18894_19210)?a:cljs.core.nth.call(null,a,(0),1.0)));
var G__18889_19218 = ((G__18892_19208)?(G__18884_19213[(1)]):(cljs.core.truth_(G__18894_19210)?a:cljs.core.nth.call(null,a,(1),1.0)));
var G__18890_19219 = ((G__18893_19209)?(G__18885_19214[(0)]):(cljs.core.truth_(G__18895_19211)?b:cljs.core.nth.call(null,b,(0),0.0)));
var G__18891_19220 = ((G__18893_19209)?(G__18885_19214[(1)]):(cljs.core.truth_(G__18895_19211)?b:cljs.core.nth.call(null,b,(1),0.0)));
(self__.buf[(0)] = ((G__18886_19215 * G__18888_19217) - G__18890_19219));

(self__.buf[(1)] = ((G__18887_19216 * G__18889_19218) - G__18891_19220));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$abs_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
(self__.buf[(0)] = thi.ng.math.core.abs.call(null,(self__.buf[(0)])));

(self__.buf[(1)] = thi.ng.math.core.abs.call(null,(self__.buf[(1)])));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$madd_BANG_$arity$3 = (function (_,a,b){
var self__ = this;
var ___$1 = this;
var G__18905_19221 = (a instanceof thi.ng.geom.core.vector.Vec2);
var G__18906_19222 = (b instanceof thi.ng.geom.core.vector.Vec2);
var G__18907_19223 = ((!(G__18905_19221))?typeof a === 'number':null);
var G__18908_19224 = ((!(G__18906_19222))?typeof b === 'number':null);
var G__18896_19225 = self__.buf;
var G__18897_19226 = ((G__18905_19221)?a.buf:null);
var G__18898_19227 = ((G__18906_19222)?b.buf:null);
var G__18899_19228 = (G__18896_19225[(0)]);
var G__18900_19229 = (G__18896_19225[(1)]);
var G__18901_19230 = ((G__18905_19221)?(G__18897_19226[(0)]):(cljs.core.truth_(G__18907_19223)?a:cljs.core.nth.call(null,a,(0),1.0)));
var G__18902_19231 = ((G__18905_19221)?(G__18897_19226[(1)]):(cljs.core.truth_(G__18907_19223)?a:cljs.core.nth.call(null,a,(1),1.0)));
var G__18903_19232 = ((G__18906_19222)?(G__18898_19227[(0)]):(cljs.core.truth_(G__18908_19224)?b:cljs.core.nth.call(null,b,(0),0.0)));
var G__18904_19233 = ((G__18906_19222)?(G__18898_19227[(1)]):(cljs.core.truth_(G__18908_19224)?b:cljs.core.nth.call(null,b,(1),0.0)));
(self__.buf[(0)] = ((G__18899_19228 * G__18901_19230) + G__18903_19232));

(self__.buf[(1)] = ((G__18900_19229 * G__18902_19231) + G__18904_19233));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$div_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
(self__.buf[(0)] = ((1) / (self__.buf[(0)])));

(self__.buf[(1)] = ((1) / (self__.buf[(1)])));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$div_BANG_$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var G__18909_19234 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__18910_19235 = v.buf;
(self__.buf[(0)] = ((G__18909_19234[(0)]) / (G__18910_19235[(0)])));

(self__.buf[(1)] = ((G__18909_19234[(1)]) / (G__18910_19235[(1)])));

self__._hash = null;
} else {
if(typeof v === 'number'){
(self__.buf[(0)] = ((G__18909_19234[(0)]) / v));

(self__.buf[(1)] = ((G__18909_19234[(1)]) / v));

self__._hash = null;
} else {
(self__.buf[(0)] = ((G__18909_19234[(0)]) / cljs.core.nth.call(null,v,(0),0.0)));

(self__.buf[(1)] = ((G__18909_19234[(1)]) / cljs.core.nth.call(null,v,(1),0.0)));

self__._hash = null;
}
}

return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$div_BANG_$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var G__18918_19236 = typeof v1 === 'number';
var G__18919_19237 = typeof v2 === 'number';
if(((G__18918_19236)?G__18919_19237:false)){
(self__.buf[(0)] = ((self__.buf[(0)]) / v1));

(self__.buf[(1)] = ((self__.buf[(1)]) / v2));
} else {
var G__18920_19238 = ((!(G__18918_19236))?(v1 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__18921_19239 = ((!(G__18919_19237))?(v2 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__18912_19240 = (cljs.core.truth_(G__18920_19238)?v1.buf:null);
var G__18913_19241 = (cljs.core.truth_(G__18921_19239)?v2.buf:null);
var G__18914_19242 = (cljs.core.truth_(G__18920_19238)?(G__18912_19240[(0)]):((G__18918_19236)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__18915_19243 = (cljs.core.truth_(G__18920_19238)?(G__18912_19240[(1)]):((G__18918_19236)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__18916_19244 = (cljs.core.truth_(G__18921_19239)?(G__18913_19241[(0)]):((G__18919_19237)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__18917_19245 = (cljs.core.truth_(G__18921_19239)?(G__18913_19241[(1)]):((G__18919_19237)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
(self__.buf[(0)] = (((self__.buf[(0)]) / G__18914_19242) / G__18916_19244));

(self__.buf[(1)] = (((self__.buf[(1)]) / G__18915_19243) / G__18917_19245));
}

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$_PLUS__BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$_PLUS__BANG_$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var G__18922_19246 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__18923_19247 = v.buf;
(self__.buf[(0)] = ((G__18922_19246[(0)]) + (G__18923_19247[(0)])));

(self__.buf[(1)] = ((G__18922_19246[(1)]) + (G__18923_19247[(1)])));

self__._hash = null;
} else {
if(typeof v === 'number'){
(self__.buf[(0)] = ((G__18922_19246[(0)]) + v));

(self__.buf[(1)] = ((G__18922_19246[(1)]) + v));

self__._hash = null;
} else {
(self__.buf[(0)] = ((G__18922_19246[(0)]) + cljs.core.nth.call(null,v,(0),0.0)));

(self__.buf[(1)] = ((G__18922_19246[(1)]) + cljs.core.nth.call(null,v,(1),0.0)));

self__._hash = null;
}
}

return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$_PLUS__BANG_$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var G__18931_19248 = typeof v1 === 'number';
var G__18932_19249 = typeof v2 === 'number';
if(((G__18931_19248)?G__18932_19249:false)){
(self__.buf[(0)] = ((self__.buf[(0)]) + v1));

(self__.buf[(1)] = ((self__.buf[(1)]) + v2));
} else {
var G__18933_19250 = ((!(G__18931_19248))?(v1 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__18934_19251 = ((!(G__18932_19249))?(v2 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__18925_19252 = (cljs.core.truth_(G__18933_19250)?v1.buf:null);
var G__18926_19253 = (cljs.core.truth_(G__18934_19251)?v2.buf:null);
var G__18927_19254 = (cljs.core.truth_(G__18933_19250)?(G__18925_19252[(0)]):((G__18931_19248)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__18928_19255 = (cljs.core.truth_(G__18933_19250)?(G__18925_19252[(1)]):((G__18931_19248)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__18929_19256 = (cljs.core.truth_(G__18934_19251)?(G__18926_19253[(0)]):((G__18932_19249)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__18930_19257 = (cljs.core.truth_(G__18934_19251)?(G__18926_19253[(1)]):((G__18932_19249)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
(self__.buf[(0)] = (((self__.buf[(0)]) + G__18927_19254) + G__18929_19256));

(self__.buf[(1)] = (((self__.buf[(1)]) + G__18928_19255) + G__18930_19257));
}

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMutableMathOps$addm_BANG_$arity$3 = (function (_,a,b){
var self__ = this;
var ___$1 = this;
var G__18944_19258 = (a instanceof thi.ng.geom.core.vector.Vec2);
var G__18945_19259 = (b instanceof thi.ng.geom.core.vector.Vec2);
var G__18946_19260 = ((!(G__18944_19258))?typeof a === 'number':null);
var G__18947_19261 = ((!(G__18945_19259))?typeof b === 'number':null);
var G__18935_19262 = self__.buf;
var G__18936_19263 = ((G__18944_19258)?a.buf:null);
var G__18937_19264 = ((G__18945_19259)?b.buf:null);
var G__18938_19265 = (G__18935_19262[(0)]);
var G__18939_19266 = (G__18935_19262[(1)]);
var G__18940_19267 = ((G__18944_19258)?(G__18936_19263[(0)]):(cljs.core.truth_(G__18946_19260)?a:cljs.core.nth.call(null,a,(0),0.0)));
var G__18941_19268 = ((G__18944_19258)?(G__18936_19263[(1)]):(cljs.core.truth_(G__18946_19260)?a:cljs.core.nth.call(null,a,(1),0.0)));
var G__18942_19269 = ((G__18945_19259)?(G__18937_19264[(0)]):(cljs.core.truth_(G__18947_19261)?b:cljs.core.nth.call(null,b,(0),1.0)));
var G__18943_19270 = ((G__18945_19259)?(G__18937_19264[(1)]):(cljs.core.truth_(G__18947_19261)?b:cljs.core.nth.call(null,b,(1),1.0)));
(self__.buf[(0)] = ((G__18938_19265 + G__18940_19267) * G__18942_19269));

(self__.buf[(1)] = ((G__18939_19266 + G__18941_19268) * G__18943_19270));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IReversible$_rseq$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return thi.ng.geom.core.vector.swizzle2_fns.call(null,new cljs.core.Keyword(null,"yx","yx",1696579752)).call(null,___$1);
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IHash$_hash$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var or__16796__auto__ = self__._hash;
if(cljs.core.truth_(or__16796__auto__)){
return or__16796__auto__;
} else {
return ___$1._hash = cljs.core.mix_collection_hash.call(null,((cljs.core.imul.call(null,(((31) + cljs.core.hash.call(null,(self__.buf[(0)]))) | (0)),(31)) + cljs.core.hash.call(null,(self__.buf[(1)]))) | (0)),(2));
}
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (_,o){
var self__ = this;
var ___$1 = this;
if((o instanceof thi.ng.geom.core.vector.Vec2)){
var b_SINGLEQUOTE_ = o.buf;
return (((self__.buf[(0)]) === (b_SINGLEQUOTE_[(0)]))) && (((self__.buf[(1)]) === (b_SINGLEQUOTE_[(1)])));
} else {
return (cljs.core.sequential_QMARK_.call(null,o)) && (((2) === cljs.core.count.call(null,o))) && (cljs.core._EQ_.call(null,(self__.buf[(0)]),cljs.core.first.call(null,o))) && (cljs.core._EQ_.call(null,(self__.buf[(1)]),cljs.core.nth.call(null,o,(1))));
}
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PHeading$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PHeading$heading$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var t = Math.atan2((self__.buf[(1)]),(self__.buf[(0)]));
if((t < (0))){
return (t + thi.ng.math.core.TWO_PI);
} else {
return t;
}
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PHeading$heading_xy$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return thi.ng.geom.core.heading.call(null,___$1);
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PHeading$angle_between$arity$2 = (function (_,a){
var self__ = this;
var ___$1 = this;
var t = (thi.ng.geom.core.heading.call(null,a) - thi.ng.geom.core.heading.call(null,___$1));
if((t < (0))){
return (t + thi.ng.math.core.TWO_PI);
} else {
return t;
}
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PHeading$slope_xy$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return ((self__.buf[(1)]) / (self__.buf[(0)]));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PDistance$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PDistance$dist$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
return Math.sqrt(thi.ng.geom.core.dist_squared.call(null,___$1,v));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PDistance$dist_squared$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var G__18948 = self__.buf;
var G__18950 = (G__18948[(0)]);
var G__18951 = (G__18948[(1)]);
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__18949 = v.buf;
var G__18952 = (G__18949[(0)]);
var G__18953 = (G__18949[(1)]);
var dx = (G__18950 - G__18952);
var dy = (G__18951 - G__18953);
return ((dx * dx) + (dy * dy));
} else {
var G__18952 = cljs.core.nth.call(null,v,(0),0.0);
var G__18953 = cljs.core.nth.call(null,v,(1),0.0);
var dx = (G__18950 - G__18952);
var dy = (G__18951 - G__18953);
return ((dx * dx) + (dy * dy));
}
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IReduce$_reduce$arity$2 = (function (coll,f){
var self__ = this;
var coll__$1 = this;
var acc = f.call(null,(self__.buf[(0)]),(self__.buf[(1)]));
if(cljs.core.reduced_QMARK_.call(null,acc)){
return cljs.core.deref.call(null,acc);
} else {
return acc;
}
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IReduce$_reduce$arity$3 = (function (coll,f,start){
var self__ = this;
var coll__$1 = this;
var acc = f.call(null,start,(self__.buf[(0)]));
if(cljs.core.reduced_QMARK_.call(null,acc)){
return cljs.core.deref.call(null,acc);
} else {
var acc__$1 = f.call(null,acc,(self__.buf[(1)]));
if(cljs.core.reduced_QMARK_.call(null,acc__$1)){
return cljs.core.deref.call(null,acc__$1);
} else {
return acc__$1;
}
}
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PInvert$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PInvert$invert$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return thi.ng.geom.core._.call(null,___$1);
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$msub$arity$3 = (function (_,a,b){
var self__ = this;
var ___$1 = this;
var dest__18707__auto__ = (new Float32Array((2)));
var G__18963_19271 = (a instanceof thi.ng.geom.core.vector.Vec2);
var G__18964_19272 = (b instanceof thi.ng.geom.core.vector.Vec2);
var G__18965_19273 = ((!(G__18963_19271))?typeof a === 'number':null);
var G__18966_19274 = ((!(G__18964_19272))?typeof b === 'number':null);
var G__18954_19275 = self__.buf;
var G__18955_19276 = ((G__18963_19271)?a.buf:null);
var G__18956_19277 = ((G__18964_19272)?b.buf:null);
var G__18957_19278 = (G__18954_19275[(0)]);
var G__18958_19279 = (G__18954_19275[(1)]);
var G__18959_19280 = ((G__18963_19271)?(G__18955_19276[(0)]):(cljs.core.truth_(G__18965_19273)?a:cljs.core.nth.call(null,a,(0),1.0)));
var G__18960_19281 = ((G__18963_19271)?(G__18955_19276[(1)]):(cljs.core.truth_(G__18965_19273)?a:cljs.core.nth.call(null,a,(1),1.0)));
var G__18961_19282 = ((G__18964_19272)?(G__18956_19277[(0)]):(cljs.core.truth_(G__18966_19274)?b:cljs.core.nth.call(null,b,(0),0.0)));
var G__18962_19283 = ((G__18964_19272)?(G__18956_19277[(1)]):(cljs.core.truth_(G__18966_19274)?b:cljs.core.nth.call(null,b,(1),0.0)));
(dest__18707__auto__[(0)] = ((G__18957_19278 * G__18959_19280) - G__18961_19282));

(dest__18707__auto__[(1)] = ((G__18958_19279 * G__18960_19281) - G__18962_19283));

return (new thi.ng.geom.core.vector.Vec2(dest__18707__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$_STAR_$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var dest__18696__auto__ = (new Float32Array((2)));
var G__18967_19284 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__18968_19285 = v.buf;
(dest__18696__auto__[(0)] = ((G__18967_19284[(0)]) * (G__18968_19285[(0)])));

(dest__18696__auto__[(1)] = ((G__18967_19284[(1)]) * (G__18968_19285[(1)])));
} else {
if(typeof v === 'number'){
(dest__18696__auto__[(0)] = ((G__18967_19284[(0)]) * v));

(dest__18696__auto__[(1)] = ((G__18967_19284[(1)]) * v));
} else {
(dest__18696__auto__[(0)] = ((G__18967_19284[(0)]) * cljs.core.nth.call(null,v,(0),0.0)));

(dest__18696__auto__[(1)] = ((G__18967_19284[(1)]) * cljs.core.nth.call(null,v,(1),0.0)));
}
}

return (new thi.ng.geom.core.vector.Vec2(dest__18696__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$_STAR_$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var G__18969 = self__.buf;
var G__18972 = (new Float32Array((2)));
var G__18973 = (G__18969[(0)]);
var G__18974 = (G__18969[(1)]);
var G__18979 = typeof v1 === 'number';
var G__18980 = typeof v2 === 'number';
if(((G__18979)?G__18980:false)){
(G__18972[(0)] = (G__18973 * v1));

(G__18972[(1)] = (G__18974 * v2));
} else {
var G__18981_19286 = ((!(G__18979))?(v1 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__18982_19287 = ((!(G__18980))?(v2 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__18970_19288 = (cljs.core.truth_(G__18981_19286)?v1.buf:null);
var G__18971_19289 = (cljs.core.truth_(G__18982_19287)?v2.buf:null);
var G__18975_19290 = (cljs.core.truth_(G__18981_19286)?(G__18970_19288[(0)]):((G__18979)?v1:cljs.core.nth.call(null,v1,(0),1.0)));
var G__18976_19291 = (cljs.core.truth_(G__18981_19286)?(G__18970_19288[(1)]):((G__18979)?v1:cljs.core.nth.call(null,v1,(1),1.0)));
var G__18977_19292 = (cljs.core.truth_(G__18982_19287)?(G__18971_19289[(0)]):((G__18980)?v2:cljs.core.nth.call(null,v2,(0),1.0)));
var G__18978_19293 = (cljs.core.truth_(G__18982_19287)?(G__18971_19289[(1)]):((G__18980)?v2:cljs.core.nth.call(null,v2,(1),1.0)));
(G__18972[(0)] = ((G__18973 * G__18975_19290) * G__18977_19292));

(G__18972[(1)] = ((G__18974 * G__18976_19291) * G__18978_19293));
}

return (new thi.ng.geom.core.vector.Vec2(G__18972,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var dest__18670__auto__ = (new Float32Array((2)));
var G__18983_19294 = self__.buf;
(dest__18670__auto__[(0)] = (- (G__18983_19294[(0)])));

(dest__18670__auto__[(1)] = (- (G__18983_19294[(1)])));

return (new thi.ng.geom.core.vector.Vec2(dest__18670__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$_$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var dest__18696__auto__ = (new Float32Array((2)));
var G__18984_19295 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__18985_19296 = v.buf;
(dest__18696__auto__[(0)] = ((G__18984_19295[(0)]) - (G__18985_19296[(0)])));

(dest__18696__auto__[(1)] = ((G__18984_19295[(1)]) - (G__18985_19296[(1)])));
} else {
if(typeof v === 'number'){
(dest__18696__auto__[(0)] = ((G__18984_19295[(0)]) - v));

(dest__18696__auto__[(1)] = ((G__18984_19295[(1)]) - v));
} else {
(dest__18696__auto__[(0)] = ((G__18984_19295[(0)]) - cljs.core.nth.call(null,v,(0),0.0)));

(dest__18696__auto__[(1)] = ((G__18984_19295[(1)]) - cljs.core.nth.call(null,v,(1),0.0)));
}
}

return (new thi.ng.geom.core.vector.Vec2(dest__18696__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$_$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var G__18986 = self__.buf;
var G__18989 = (new Float32Array((2)));
var G__18990 = (G__18986[(0)]);
var G__18991 = (G__18986[(1)]);
var G__18996 = typeof v1 === 'number';
var G__18997 = typeof v2 === 'number';
if(((G__18996)?G__18997:false)){
(G__18989[(0)] = (G__18990 - v1));

(G__18989[(1)] = (G__18991 - v2));
} else {
var G__18998_19297 = ((!(G__18996))?(v1 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__18999_19298 = ((!(G__18997))?(v2 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__18987_19299 = (cljs.core.truth_(G__18998_19297)?v1.buf:null);
var G__18988_19300 = (cljs.core.truth_(G__18999_19298)?v2.buf:null);
var G__18992_19301 = (cljs.core.truth_(G__18998_19297)?(G__18987_19299[(0)]):((G__18996)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__18993_19302 = (cljs.core.truth_(G__18998_19297)?(G__18987_19299[(1)]):((G__18996)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__18994_19303 = (cljs.core.truth_(G__18999_19298)?(G__18988_19300[(0)]):((G__18997)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__18995_19304 = (cljs.core.truth_(G__18999_19298)?(G__18988_19300[(1)]):((G__18997)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
(G__18989[(0)] = ((G__18990 - G__18992_19301) - G__18994_19303));

(G__18989[(1)] = ((G__18991 - G__18993_19302) - G__18995_19304));
}

return (new thi.ng.geom.core.vector.Vec2(G__18989,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$madd$arity$3 = (function (_,a,b){
var self__ = this;
var ___$1 = this;
var dest__18707__auto__ = (new Float32Array((2)));
var G__19009_19305 = (a instanceof thi.ng.geom.core.vector.Vec2);
var G__19010_19306 = (b instanceof thi.ng.geom.core.vector.Vec2);
var G__19011_19307 = ((!(G__19009_19305))?typeof a === 'number':null);
var G__19012_19308 = ((!(G__19010_19306))?typeof b === 'number':null);
var G__19000_19309 = self__.buf;
var G__19001_19310 = ((G__19009_19305)?a.buf:null);
var G__19002_19311 = ((G__19010_19306)?b.buf:null);
var G__19003_19312 = (G__19000_19309[(0)]);
var G__19004_19313 = (G__19000_19309[(1)]);
var G__19005_19314 = ((G__19009_19305)?(G__19001_19310[(0)]):(cljs.core.truth_(G__19011_19307)?a:cljs.core.nth.call(null,a,(0),1.0)));
var G__19006_19315 = ((G__19009_19305)?(G__19001_19310[(1)]):(cljs.core.truth_(G__19011_19307)?a:cljs.core.nth.call(null,a,(1),1.0)));
var G__19007_19316 = ((G__19010_19306)?(G__19002_19311[(0)]):(cljs.core.truth_(G__19012_19308)?b:cljs.core.nth.call(null,b,(0),0.0)));
var G__19008_19317 = ((G__19010_19306)?(G__19002_19311[(1)]):(cljs.core.truth_(G__19012_19308)?b:cljs.core.nth.call(null,b,(1),0.0)));
(dest__18707__auto__[(0)] = ((G__19003_19312 * G__19005_19314) + G__19007_19316));

(dest__18707__auto__[(1)] = ((G__19004_19313 * G__19006_19315) + G__19008_19317));

return (new thi.ng.geom.core.vector.Vec2(dest__18707__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$addm$arity$3 = (function (_,a,b){
var self__ = this;
var ___$1 = this;
var dest__18707__auto__ = (new Float32Array((2)));
var G__19022_19318 = (a instanceof thi.ng.geom.core.vector.Vec2);
var G__19023_19319 = (b instanceof thi.ng.geom.core.vector.Vec2);
var G__19024_19320 = ((!(G__19022_19318))?typeof a === 'number':null);
var G__19025_19321 = ((!(G__19023_19319))?typeof b === 'number':null);
var G__19013_19322 = self__.buf;
var G__19014_19323 = ((G__19022_19318)?a.buf:null);
var G__19015_19324 = ((G__19023_19319)?b.buf:null);
var G__19016_19325 = (G__19013_19322[(0)]);
var G__19017_19326 = (G__19013_19322[(1)]);
var G__19018_19327 = ((G__19022_19318)?(G__19014_19323[(0)]):(cljs.core.truth_(G__19024_19320)?a:cljs.core.nth.call(null,a,(0),0.0)));
var G__19019_19328 = ((G__19022_19318)?(G__19014_19323[(1)]):(cljs.core.truth_(G__19024_19320)?a:cljs.core.nth.call(null,a,(1),0.0)));
var G__19020_19329 = ((G__19023_19319)?(G__19015_19324[(0)]):(cljs.core.truth_(G__19025_19321)?b:cljs.core.nth.call(null,b,(0),1.0)));
var G__19021_19330 = ((G__19023_19319)?(G__19015_19324[(1)]):(cljs.core.truth_(G__19025_19321)?b:cljs.core.nth.call(null,b,(1),1.0)));
(dest__18707__auto__[(0)] = ((G__19016_19325 + G__19018_19327) * G__19020_19329));

(dest__18707__auto__[(1)] = ((G__19017_19326 + G__19019_19328) * G__19021_19330));

return (new thi.ng.geom.core.vector.Vec2(dest__18707__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$div$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var dest__18670__auto__ = (new Float32Array((2)));
var G__19026_19331 = self__.buf;
(dest__18670__auto__[(0)] = ((1) / (G__19026_19331[(0)])));

(dest__18670__auto__[(1)] = ((1) / (G__19026_19331[(1)])));

return (new thi.ng.geom.core.vector.Vec2(dest__18670__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$div$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var dest__18696__auto__ = (new Float32Array((2)));
var G__19027_19332 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__19028_19333 = v.buf;
(dest__18696__auto__[(0)] = ((G__19027_19332[(0)]) / (G__19028_19333[(0)])));

(dest__18696__auto__[(1)] = ((G__19027_19332[(1)]) / (G__19028_19333[(1)])));
} else {
if(typeof v === 'number'){
(dest__18696__auto__[(0)] = ((G__19027_19332[(0)]) / v));

(dest__18696__auto__[(1)] = ((G__19027_19332[(1)]) / v));
} else {
(dest__18696__auto__[(0)] = ((G__19027_19332[(0)]) / cljs.core.nth.call(null,v,(0),0.0)));

(dest__18696__auto__[(1)] = ((G__19027_19332[(1)]) / cljs.core.nth.call(null,v,(1),0.0)));
}
}

return (new thi.ng.geom.core.vector.Vec2(dest__18696__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$div$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var G__19029 = self__.buf;
var G__19032 = (new Float32Array((2)));
var G__19033 = (G__19029[(0)]);
var G__19034 = (G__19029[(1)]);
var G__19039 = typeof v1 === 'number';
var G__19040 = typeof v2 === 'number';
if(((G__19039)?G__19040:false)){
(G__19032[(0)] = (G__19033 / v1));

(G__19032[(1)] = (G__19034 / v2));
} else {
var G__19041_19334 = ((!(G__19039))?(v1 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__19042_19335 = ((!(G__19040))?(v2 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__19030_19336 = (cljs.core.truth_(G__19041_19334)?v1.buf:null);
var G__19031_19337 = (cljs.core.truth_(G__19042_19335)?v2.buf:null);
var G__19035_19338 = (cljs.core.truth_(G__19041_19334)?(G__19030_19336[(0)]):((G__19039)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__19036_19339 = (cljs.core.truth_(G__19041_19334)?(G__19030_19336[(1)]):((G__19039)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__19037_19340 = (cljs.core.truth_(G__19042_19335)?(G__19031_19337[(0)]):((G__19040)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19038_19341 = (cljs.core.truth_(G__19042_19335)?(G__19031_19337[(1)]):((G__19040)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
(G__19032[(0)] = ((G__19033 / G__19035_19338) / G__19037_19340));

(G__19032[(1)] = ((G__19034 / G__19036_19339) / G__19038_19341));
}

return (new thi.ng.geom.core.vector.Vec2(G__19032,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$_PLUS_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$_PLUS_$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var dest__18696__auto__ = (new Float32Array((2)));
var G__19043_19342 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__19044_19343 = v.buf;
(dest__18696__auto__[(0)] = ((G__19043_19342[(0)]) + (G__19044_19343[(0)])));

(dest__18696__auto__[(1)] = ((G__19043_19342[(1)]) + (G__19044_19343[(1)])));
} else {
if(typeof v === 'number'){
(dest__18696__auto__[(0)] = ((G__19043_19342[(0)]) + v));

(dest__18696__auto__[(1)] = ((G__19043_19342[(1)]) + v));
} else {
(dest__18696__auto__[(0)] = ((G__19043_19342[(0)]) + cljs.core.nth.call(null,v,(0),0.0)));

(dest__18696__auto__[(1)] = ((G__19043_19342[(1)]) + cljs.core.nth.call(null,v,(1),0.0)));
}
}

return (new thi.ng.geom.core.vector.Vec2(dest__18696__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$_PLUS_$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var G__19045 = self__.buf;
var G__19048 = (new Float32Array((2)));
var G__19049 = (G__19045[(0)]);
var G__19050 = (G__19045[(1)]);
var G__19055 = typeof v1 === 'number';
var G__19056 = typeof v2 === 'number';
if(((G__19055)?G__19056:false)){
(G__19048[(0)] = (G__19049 + v1));

(G__19048[(1)] = (G__19050 + v2));
} else {
var G__19057_19344 = ((!(G__19055))?(v1 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__19058_19345 = ((!(G__19056))?(v2 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__19046_19346 = (cljs.core.truth_(G__19057_19344)?v1.buf:null);
var G__19047_19347 = (cljs.core.truth_(G__19058_19345)?v2.buf:null);
var G__19051_19348 = (cljs.core.truth_(G__19057_19344)?(G__19046_19346[(0)]):((G__19055)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__19052_19349 = (cljs.core.truth_(G__19057_19344)?(G__19046_19346[(1)]):((G__19055)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__19053_19350 = (cljs.core.truth_(G__19058_19345)?(G__19047_19347[(0)]):((G__19056)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19054_19351 = (cljs.core.truth_(G__19058_19345)?(G__19047_19347[(1)]):((G__19056)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
(G__19048[(0)] = ((G__19049 + G__19051_19348) + G__19053_19350));

(G__19048[(1)] = ((G__19050 + G__19052_19349) + G__19054_19351));
}

return (new thi.ng.geom.core.vector.Vec2(G__19048,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$abs$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var dest__18670__auto__ = (new Float32Array((2)));
var G__19059_19352 = self__.buf;
(dest__18670__auto__[(0)] = thi.ng.math.core.abs.call(null,(G__19059_19352[(0)])));

(dest__18670__auto__[(1)] = thi.ng.math.core.abs.call(null,(G__19059_19352[(1)])));

return (new thi.ng.geom.core.vector.Vec2(dest__18670__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMathOps$subm$arity$3 = (function (_,a,b){
var self__ = this;
var ___$1 = this;
var dest__18707__auto__ = (new Float32Array((2)));
var G__19069_19353 = (a instanceof thi.ng.geom.core.vector.Vec2);
var G__19070_19354 = (b instanceof thi.ng.geom.core.vector.Vec2);
var G__19071_19355 = ((!(G__19069_19353))?typeof a === 'number':null);
var G__19072_19356 = ((!(G__19070_19354))?typeof b === 'number':null);
var G__19060_19357 = self__.buf;
var G__19061_19358 = ((G__19069_19353)?a.buf:null);
var G__19062_19359 = ((G__19070_19354)?b.buf:null);
var G__19063_19360 = (G__19060_19357[(0)]);
var G__19064_19361 = (G__19060_19357[(1)]);
var G__19065_19362 = ((G__19069_19353)?(G__19061_19358[(0)]):(cljs.core.truth_(G__19071_19355)?a:cljs.core.nth.call(null,a,(0),0.0)));
var G__19066_19363 = ((G__19069_19353)?(G__19061_19358[(1)]):(cljs.core.truth_(G__19071_19355)?a:cljs.core.nth.call(null,a,(1),0.0)));
var G__19067_19364 = ((G__19070_19354)?(G__19062_19359[(0)]):(cljs.core.truth_(G__19072_19356)?b:cljs.core.nth.call(null,b,(0),1.0)));
var G__19068_19365 = ((G__19070_19354)?(G__19062_19359[(1)]):(cljs.core.truth_(G__19072_19356)?b:cljs.core.nth.call(null,b,(1),1.0)));
(dest__18707__auto__[(0)] = ((G__19063_19360 - G__19065_19362) * G__19067_19364));

(dest__18707__auto__[(1)] = ((G__19064_19361 - G__19066_19363) * G__19068_19365));

return (new thi.ng.geom.core.vector.Vec2(dest__18707__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PNormal$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PNormal$normal$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var b = (new Float32Array((2)));
(b[(0)] = (- (self__.buf[(1)])));

(b[(1)] = (self__.buf[(0)]));

return (new thi.ng.geom.core.vector.Vec2(b,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$ISeq$_first$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return (self__.buf[(0)]);
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$ISeq$_rest$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.cons.call(null,(self__.buf[(1)]),null);
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PTranslate$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PTranslate$translate$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var dest__18696__auto__ = (new Float32Array((2)));
var G__19073_19366 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__19074_19367 = v.buf;
(dest__18696__auto__[(0)] = ((G__19073_19366[(0)]) + (G__19074_19367[(0)])));

(dest__18696__auto__[(1)] = ((G__19073_19366[(1)]) + (G__19074_19367[(1)])));
} else {
if(typeof v === 'number'){
(dest__18696__auto__[(0)] = ((G__19073_19366[(0)]) + v));

(dest__18696__auto__[(1)] = ((G__19073_19366[(1)]) + v));
} else {
(dest__18696__auto__[(0)] = ((G__19073_19366[(0)]) + cljs.core.nth.call(null,v,(0),0.0)));

(dest__18696__auto__[(1)] = ((G__19073_19366[(1)]) + cljs.core.nth.call(null,v,(1),0.0)));
}
}

return (new thi.ng.geom.core.vector.Vec2(dest__18696__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PTranslate$translate$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var G__19075 = self__.buf;
var G__19078 = (new Float32Array((2)));
var G__19079 = (G__19075[(0)]);
var G__19080 = (G__19075[(1)]);
var G__19085 = typeof v1 === 'number';
var G__19086 = typeof v2 === 'number';
if(((G__19085)?G__19086:false)){
(G__19078[(0)] = (G__19079 + v1));

(G__19078[(1)] = (G__19080 + v2));
} else {
var G__19087_19368 = ((!(G__19085))?(v1 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__19088_19369 = ((!(G__19086))?(v2 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__19076_19370 = (cljs.core.truth_(G__19087_19368)?v1.buf:null);
var G__19077_19371 = (cljs.core.truth_(G__19088_19369)?v2.buf:null);
var G__19081_19372 = (cljs.core.truth_(G__19087_19368)?(G__19076_19370[(0)]):((G__19085)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__19082_19373 = (cljs.core.truth_(G__19087_19368)?(G__19076_19370[(1)]):((G__19085)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__19083_19374 = (cljs.core.truth_(G__19088_19369)?(G__19077_19371[(0)]):((G__19086)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19084_19375 = (cljs.core.truth_(G__19088_19369)?(G__19077_19371[(1)]):((G__19086)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
(G__19078[(0)] = ((G__19079 + G__19081_19372) + G__19083_19374));

(G__19078[(1)] = ((G__19080 + G__19082_19373) + G__19084_19375));
}

return (new thi.ng.geom.core.vector.Vec2(G__19078,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IAssociative$_contains_key_QMARK_$arity$2 = (function (_,k){
var self__ = this;
var ___$1 = this;
if(typeof k === 'number'){
return ((k >= (0))) && ((k < (2)));
} else {
if(cljs.core.truth_(thi.ng.geom.core.vector.swizzle2_fns.call(null,k))){
return true;
} else {
return false;
}
}
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (_,k,v){
var self__ = this;
var ___$1 = this;
if(typeof k === 'number'){
if(((k === (0))) || ((k === (1)))){
var b = (new Float32Array(self__.buf));
(b[k] = v);

return (new thi.ng.geom.core.vector.Vec2(b,null,self__._meta));
} else {
if((k === (2))){
return cljs.core.conj.call(null,___$1,v);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
}
} else {
if((k instanceof cljs.core.Keyword)){
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"z","z",-789527183),k)){
return cljs.core.conj.call(null,___$1,v);
} else {
return (new thi.ng.geom.core.vector.Vec2(thi.ng.geom.core.vector.swizzle_assoc_STAR_.call(null,self__.buf,(new Float32Array(self__.buf)),new cljs.core.PersistentArrayMap(null, 2, ["x",(0),"y",(1)], null),k,v),null,self__._meta));
}
} else {
return null;
}
}
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return ___$1;
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PScale$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PScale$scale$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var dest__18696__auto__ = (new Float32Array((2)));
var G__19089_19376 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__19090_19377 = v.buf;
(dest__18696__auto__[(0)] = ((G__19089_19376[(0)]) * (G__19090_19377[(0)])));

(dest__18696__auto__[(1)] = ((G__19089_19376[(1)]) * (G__19090_19377[(1)])));
} else {
if(typeof v === 'number'){
(dest__18696__auto__[(0)] = ((G__19089_19376[(0)]) * v));

(dest__18696__auto__[(1)] = ((G__19089_19376[(1)]) * v));
} else {
(dest__18696__auto__[(0)] = ((G__19089_19376[(0)]) * cljs.core.nth.call(null,v,(0),0.0)));

(dest__18696__auto__[(1)] = ((G__19089_19376[(1)]) * cljs.core.nth.call(null,v,(1),0.0)));
}
}

return (new thi.ng.geom.core.vector.Vec2(dest__18696__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PScale$scale$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var G__19091 = self__.buf;
var G__19094 = (new Float32Array((2)));
var G__19095 = (G__19091[(0)]);
var G__19096 = (G__19091[(1)]);
var G__19101 = typeof v1 === 'number';
var G__19102 = typeof v2 === 'number';
if(((G__19101)?G__19102:false)){
(G__19094[(0)] = (G__19095 * v1));

(G__19094[(1)] = (G__19096 * v2));
} else {
var G__19103_19378 = ((!(G__19101))?(v1 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__19104_19379 = ((!(G__19102))?(v2 instanceof thi.ng.geom.core.vector.Vec2):null);
var G__19092_19380 = (cljs.core.truth_(G__19103_19378)?v1.buf:null);
var G__19093_19381 = (cljs.core.truth_(G__19104_19379)?v2.buf:null);
var G__19097_19382 = (cljs.core.truth_(G__19103_19378)?(G__19092_19380[(0)]):((G__19101)?v1:cljs.core.nth.call(null,v1,(0),1.0)));
var G__19098_19383 = (cljs.core.truth_(G__19103_19378)?(G__19092_19380[(1)]):((G__19101)?v1:cljs.core.nth.call(null,v1,(1),1.0)));
var G__19099_19384 = (cljs.core.truth_(G__19104_19379)?(G__19093_19381[(0)]):((G__19102)?v2:cljs.core.nth.call(null,v2,(0),1.0)));
var G__19100_19385 = (cljs.core.truth_(G__19104_19379)?(G__19093_19381[(1)]):((G__19102)?v2:cljs.core.nth.call(null,v2,(1),1.0)));
(G__19094[(0)] = ((G__19095 * G__19097_19382) * G__19099_19384));

(G__19094[(1)] = ((G__19096 * G__19098_19383) * G__19100_19385));
}

return (new thi.ng.geom.core.vector.Vec2(G__19094,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_,m){
var self__ = this;
var ___$1 = this;
return (new thi.ng.geom.core.vector.Vec2((new Float32Array(self__.buf)),self__._hash,m));
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$ICollection$_conj$arity$2 = (function (_,x){
var self__ = this;
var ___$1 = this;
return cljs.core.with_meta.call(null,thi.ng.geom.core.vector.vec3.call(null,(self__.buf[(0)]),(self__.buf[(1)]),x),self__._meta);
});

thi.ng.geom.core.vector.Vec2.prototype.call = (function() {
var G__19386 = null;
var G__19386__2 = (function (self__,k){
var self__ = this;
var self____$1 = this;
var _ = self____$1;
if((k instanceof cljs.core.Keyword)){
var temp__4423__auto__ = thi.ng.geom.core.vector.swizzle2_fns.call(null,k);
if(cljs.core.truth_(temp__4423__auto__)){
var f = temp__4423__auto__;
return f.call(null,_);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
} else {
if(((k >= (0))) && ((k < (2)))){
return (self__.buf[k]);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
}
});
var G__19386__3 = (function (self__,k,nf){
var self__ = this;
var self____$1 = this;
var _ = self____$1;
if((k instanceof cljs.core.Keyword)){
var temp__4423__auto__ = thi.ng.geom.core.vector.swizzle2_fns.call(null,k);
if(cljs.core.truth_(temp__4423__auto__)){
var f = temp__4423__auto__;
return f.call(null,_);
} else {
return nf;
}
} else {
if(((k >= (0))) && ((k < (2)))){
return (self__.buf[k]);
} else {
return nf;
}
}
});
G__19386 = function(self__,k,nf){
switch(arguments.length){
case 2:
return G__19386__2.call(this,self__,k);
case 3:
return G__19386__3.call(this,self__,k,nf);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__19386.cljs$core$IFn$_invoke$arity$2 = G__19386__2;
G__19386.cljs$core$IFn$_invoke$arity$3 = G__19386__3;
return G__19386;
})()
;

thi.ng.geom.core.vector.Vec2.prototype.apply = (function (self__,args18821){
var self__ = this;
var self____$1 = this;
return self____$1.call.apply(self____$1,[self____$1].concat(cljs.core.aclone.call(null,args18821)));
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IFn$_invoke$arity$1 = (function (k){
var self__ = this;
var _ = this;
if((k instanceof cljs.core.Keyword)){
var temp__4423__auto__ = thi.ng.geom.core.vector.swizzle2_fns.call(null,k);
if(cljs.core.truth_(temp__4423__auto__)){
var f = temp__4423__auto__;
return f.call(null,_);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
} else {
if(((k >= (0))) && ((k < (2)))){
return (self__.buf[k]);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
}
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IFn$_invoke$arity$2 = (function (k,nf){
var self__ = this;
var _ = this;
if((k instanceof cljs.core.Keyword)){
var temp__4423__auto__ = thi.ng.geom.core.vector.swizzle2_fns.call(null,k);
if(cljs.core.truth_(temp__4423__auto__)){
var f = temp__4423__auto__;
return f.call(null,_);
} else {
return nf;
}
} else {
if(((k >= (0))) && ((k < (2)))){
return (self__.buf[k]);
} else {
return nf;
}
}
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMinMax$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMinMax$min$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var dest__18696__auto__ = (new Float32Array((2)));
var G__19105_19387 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__19106_19388 = v.buf;
(dest__18696__auto__[(0)] = (function (){var a__18352__auto__ = (G__19105_19387[(0)]);
var b__18353__auto__ = (G__19106_19388[(0)]);
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());

(dest__18696__auto__[(1)] = (function (){var a__18352__auto__ = (G__19105_19387[(1)]);
var b__18353__auto__ = (G__19106_19388[(1)]);
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());
} else {
if(typeof v === 'number'){
(dest__18696__auto__[(0)] = (function (){var a__18352__auto__ = (G__19105_19387[(0)]);
var b__18353__auto__ = v;
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());

(dest__18696__auto__[(1)] = (function (){var a__18352__auto__ = (G__19105_19387[(1)]);
var b__18353__auto__ = v;
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());
} else {
(dest__18696__auto__[(0)] = (function (){var a__18352__auto__ = (G__19105_19387[(0)]);
var b__18353__auto__ = cljs.core.nth.call(null,v,(0),0.0);
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());

(dest__18696__auto__[(1)] = (function (){var a__18352__auto__ = (G__19105_19387[(1)]);
var b__18353__auto__ = cljs.core.nth.call(null,v,(1),0.0);
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());
}
}

return (new thi.ng.geom.core.vector.Vec2(dest__18696__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMinMax$min$arity$3 = (function (_,v,v2){
var self__ = this;
var ___$1 = this;
var dest__18707__auto__ = (new Float32Array((2)));
var G__19116_19389 = (v instanceof thi.ng.geom.core.vector.Vec2);
var G__19117_19390 = (v2 instanceof thi.ng.geom.core.vector.Vec2);
var G__19118_19391 = ((!(G__19116_19389))?typeof v === 'number':null);
var G__19119_19392 = ((!(G__19117_19390))?typeof v2 === 'number':null);
var G__19107_19393 = self__.buf;
var G__19108_19394 = ((G__19116_19389)?v.buf:null);
var G__19109_19395 = ((G__19117_19390)?v2.buf:null);
var G__19110_19396 = (G__19107_19393[(0)]);
var G__19111_19397 = (G__19107_19393[(1)]);
var G__19112_19398 = ((G__19116_19389)?(G__19108_19394[(0)]):(cljs.core.truth_(G__19118_19391)?v:cljs.core.nth.call(null,v,(0),0.0)));
var G__19113_19399 = ((G__19116_19389)?(G__19108_19394[(1)]):(cljs.core.truth_(G__19118_19391)?v:cljs.core.nth.call(null,v,(1),0.0)));
var G__19114_19400 = ((G__19117_19390)?(G__19109_19395[(0)]):(cljs.core.truth_(G__19119_19392)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19115_19401 = ((G__19117_19390)?(G__19109_19395[(1)]):(cljs.core.truth_(G__19119_19392)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
(dest__18707__auto__[(0)] = (function (){var a__18352__auto__ = (function (){var a__18352__auto__ = G__19110_19396;
var b__18353__auto__ = G__19112_19398;
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})();
var b__18353__auto__ = G__19114_19400;
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());

(dest__18707__auto__[(1)] = (function (){var a__18352__auto__ = (function (){var a__18352__auto__ = G__19111_19397;
var b__18353__auto__ = G__19113_19399;
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})();
var b__18353__auto__ = G__19115_19401;
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());

return (new thi.ng.geom.core.vector.Vec2(dest__18707__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMinMax$max$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var dest__18696__auto__ = (new Float32Array((2)));
var G__19120_19402 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__19121_19403 = v.buf;
(dest__18696__auto__[(0)] = (function (){var a__18359__auto__ = (G__19120_19402[(0)]);
var b__18360__auto__ = (G__19121_19403[(0)]);
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());

(dest__18696__auto__[(1)] = (function (){var a__18359__auto__ = (G__19120_19402[(1)]);
var b__18360__auto__ = (G__19121_19403[(1)]);
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());
} else {
if(typeof v === 'number'){
(dest__18696__auto__[(0)] = (function (){var a__18359__auto__ = (G__19120_19402[(0)]);
var b__18360__auto__ = v;
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());

(dest__18696__auto__[(1)] = (function (){var a__18359__auto__ = (G__19120_19402[(1)]);
var b__18360__auto__ = v;
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());
} else {
(dest__18696__auto__[(0)] = (function (){var a__18359__auto__ = (G__19120_19402[(0)]);
var b__18360__auto__ = cljs.core.nth.call(null,v,(0),0.0);
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());

(dest__18696__auto__[(1)] = (function (){var a__18359__auto__ = (G__19120_19402[(1)]);
var b__18360__auto__ = cljs.core.nth.call(null,v,(1),0.0);
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());
}
}

return (new thi.ng.geom.core.vector.Vec2(dest__18696__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMinMax$max$arity$3 = (function (_,v,v2){
var self__ = this;
var ___$1 = this;
var dest__18707__auto__ = (new Float32Array((2)));
var G__19131_19404 = (v instanceof thi.ng.geom.core.vector.Vec2);
var G__19132_19405 = (v2 instanceof thi.ng.geom.core.vector.Vec2);
var G__19133_19406 = ((!(G__19131_19404))?typeof v === 'number':null);
var G__19134_19407 = ((!(G__19132_19405))?typeof v2 === 'number':null);
var G__19122_19408 = self__.buf;
var G__19123_19409 = ((G__19131_19404)?v.buf:null);
var G__19124_19410 = ((G__19132_19405)?v2.buf:null);
var G__19125_19411 = (G__19122_19408[(0)]);
var G__19126_19412 = (G__19122_19408[(1)]);
var G__19127_19413 = ((G__19131_19404)?(G__19123_19409[(0)]):(cljs.core.truth_(G__19133_19406)?v:cljs.core.nth.call(null,v,(0),0.0)));
var G__19128_19414 = ((G__19131_19404)?(G__19123_19409[(1)]):(cljs.core.truth_(G__19133_19406)?v:cljs.core.nth.call(null,v,(1),0.0)));
var G__19129_19415 = ((G__19132_19405)?(G__19124_19410[(0)]):(cljs.core.truth_(G__19134_19407)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19130_19416 = ((G__19132_19405)?(G__19124_19410[(1)]):(cljs.core.truth_(G__19134_19407)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
(dest__18707__auto__[(0)] = (function (){var a__18359__auto__ = (function (){var a__18359__auto__ = G__19125_19411;
var b__18360__auto__ = G__19127_19413;
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})();
var b__18360__auto__ = G__19129_19415;
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());

(dest__18707__auto__[(1)] = (function (){var a__18359__auto__ = (function (){var a__18359__auto__ = G__19126_19412;
var b__18360__auto__ = G__19128_19414;
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})();
var b__18360__auto__ = G__19130_19416;
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());

return (new thi.ng.geom.core.vector.Vec2(dest__18707__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.cljs$core$IComparable$_compare$arity$2 = (function (_,o){
var self__ = this;
var ___$1 = this;
if((o instanceof thi.ng.geom.core.vector.Vec2)){
var b_SINGLEQUOTE_ = o.buf;
var c = cljs.core.compare.call(null,(self__.buf[(0)]),(b_SINGLEQUOTE_[(0)]));
if(((0) === c)){
return cljs.core.compare.call(null,(self__.buf[(1)]),(b_SINGLEQUOTE_[(1)]));
} else {
return c;
}
} else {
var c = cljs.core.count.call(null,o);
if(((2) === c)){
return (- cljs.core.compare.call(null,o,___$1));
} else {
return ((2) - c);
}
}
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PVectorReduce$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PVectorReduce$reduce_vector$arity$3 = (function (_,f,xs){
var self__ = this;
var ___$1 = this;
var buf_SINGLEQUOTE_ = (new Float32Array(self__.buf));
return (new thi.ng.geom.core.vector.Vec2(thi.ng.geom.core.vector.vec2_reduce_STAR_.call(null,f,buf_SINGLEQUOTE_,xs),null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PVectorReduce$reduce_vector$arity$4 = (function (_,f,f2,xs){
var self__ = this;
var ___$1 = this;
var buf_SINGLEQUOTE_ = (new Float32Array(self__.buf));
thi.ng.geom.core.vector.vec2_reduce_STAR_.call(null,f,buf_SINGLEQUOTE_,xs);

(buf_SINGLEQUOTE_[(0)] = f2.call(null,(buf_SINGLEQUOTE_[(0)]),(0)));

(buf_SINGLEQUOTE_[(1)] = f2.call(null,(buf_SINGLEQUOTE_[(1)]),(1)));

return (new thi.ng.geom.core.vector.Vec2(buf_SINGLEQUOTE_,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$math$core$PDeltaEquals$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$math$core$PDeltaEquals$delta_EQ_$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
return thi.ng.math.core.delta_EQ_.call(null,___$1,v,thi.ng.math.core._STAR_eps_STAR_);
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$math$core$PDeltaEquals$delta_EQ_$arity$3 = (function (_,v,eps){
var self__ = this;
var ___$1 = this;
if(cljs.core.sequential_QMARK_.call(null,v)){
if(((2) === cljs.core.count.call(null,v))){
var G__19135 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__19136 = v.buf;
if(cljs.core.truth_(thi.ng.math.core.delta_EQ_.call(null,(G__19135[(0)]),(G__19136[(0)]),eps))){
return thi.ng.math.core.delta_EQ_.call(null,(G__19135[(1)]),(G__19136[(1)]),eps);
} else {
return null;
}
} else {
if(cljs.core.truth_(thi.ng.math.core.delta_EQ_.call(null,(G__19135[(0)]),cljs.core.nth.call(null,v,(0),0.0),eps))){
return thi.ng.math.core.delta_EQ_.call(null,(G__19135[(1)]),cljs.core.nth.call(null,v,(1),0.0),eps);
} else {
return null;
}
}
} else {
return null;
}
} else {
return null;
}
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMagnitude$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMagnitude$mag$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var G__19137 = self__.buf;
var G__19138 = (G__19137[(0)]);
var G__19139 = (G__19137[(1)]);
return Math.sqrt(((G__19138 * G__19138) + (G__19139 * G__19139)));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PMagnitude$mag_squared$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var G__19140 = self__.buf;
var G__19141 = (G__19140[(0)]);
var G__19142 = (G__19140[(1)]);
return ((G__19141 * G__19141) + (G__19142 * G__19142));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PInterpolate$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PInterpolate$mix$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var b = (new Float32Array((2)));
var G__19143_19417 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec2)){
var G__19144_19418 = v.buf;
(b[(0)] = (((G__19143_19417[(0)]) + (G__19144_19418[(0)])) * 0.5));

(b[(1)] = (((G__19143_19417[(1)]) + (G__19144_19418[(1)])) * 0.5));
} else {
if(typeof v === 'number'){
(b[(0)] = (((G__19143_19417[(0)]) + v) * 0.5));

(b[(1)] = (((G__19143_19417[(1)]) + v) * 0.5));
} else {
(b[(0)] = (((G__19143_19417[(0)]) + cljs.core.nth.call(null,v,(0),0.0)) * 0.5));

(b[(1)] = (((G__19143_19417[(1)]) + cljs.core.nth.call(null,v,(1),0.0)) * 0.5));
}
}

return (new thi.ng.geom.core.vector.Vec2(b,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PInterpolate$mix$arity$3 = (function (_,v,v2){
var self__ = this;
var ___$1 = this;
var b = (new Float32Array((2)));
var G__19154_19419 = (v instanceof thi.ng.geom.core.vector.Vec2);
var G__19155_19420 = (v2 instanceof thi.ng.geom.core.vector.Vec2);
var G__19156_19421 = ((!(G__19154_19419))?typeof v === 'number':null);
var G__19157_19422 = ((!(G__19155_19420))?typeof v2 === 'number':null);
var G__19145_19423 = self__.buf;
var G__19146_19424 = ((G__19154_19419)?v.buf:null);
var G__19147_19425 = ((G__19155_19420)?v2.buf:null);
var G__19148_19426 = (G__19145_19423[(0)]);
var G__19149_19427 = (G__19145_19423[(1)]);
var G__19150_19428 = ((G__19154_19419)?(G__19146_19424[(0)]):(cljs.core.truth_(G__19156_19421)?v:cljs.core.nth.call(null,v,(0),0.0)));
var G__19151_19429 = ((G__19154_19419)?(G__19146_19424[(1)]):(cljs.core.truth_(G__19156_19421)?v:cljs.core.nth.call(null,v,(1),0.0)));
var G__19152_19430 = ((G__19155_19420)?(G__19147_19425[(0)]):(cljs.core.truth_(G__19157_19422)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19153_19431 = ((G__19155_19420)?(G__19147_19425[(1)]):(cljs.core.truth_(G__19157_19422)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
(b[(0)] = (((G__19150_19428 - G__19148_19426) * G__19152_19430) + G__19148_19426));

(b[(1)] = (((G__19151_19429 - G__19149_19427) * G__19153_19431) + G__19149_19427));

return (new thi.ng.geom.core.vector.Vec2(b,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PInterpolate$mix$arity$6 = (function (_,b,c,d,u,v){
var self__ = this;
var ___$1 = this;
var b_SINGLEQUOTE_ = (new Float32Array((2)));
var dv_QMARK_ = (d instanceof thi.ng.geom.core.vector.Vec2);
var dn_QMARK_ = typeof d === 'number';
var dv = ((dv_QMARK_)?d.buf:null);
var dx = ((dv_QMARK_)?(dv[(0)]):((dn_QMARK_)?d:cljs.core.nth.call(null,d,(0),0.0)));
var dy = ((dv_QMARK_)?(dv[(1)]):((dn_QMARK_)?d:cljs.core.nth.call(null,d,(1),0.0)));
var G__19167_19432 = (b instanceof thi.ng.geom.core.vector.Vec2);
var G__19168_19433 = (c instanceof thi.ng.geom.core.vector.Vec2);
var G__19169_19434 = ((!(G__19167_19432))?typeof b === 'number':null);
var G__19170_19435 = ((!(G__19168_19433))?typeof c === 'number':null);
var G__19158_19436 = self__.buf;
var G__19159_19437 = ((G__19167_19432)?b.buf:null);
var G__19160_19438 = ((G__19168_19433)?c.buf:null);
var G__19161_19439 = (G__19158_19436[(0)]);
var G__19162_19440 = (G__19158_19436[(1)]);
var G__19163_19441 = ((G__19167_19432)?(G__19159_19437[(0)]):(cljs.core.truth_(G__19169_19434)?b:cljs.core.nth.call(null,b,(0),0.0)));
var G__19164_19442 = ((G__19167_19432)?(G__19159_19437[(1)]):(cljs.core.truth_(G__19169_19434)?b:cljs.core.nth.call(null,b,(1),0.0)));
var G__19165_19443 = ((G__19168_19433)?(G__19160_19438[(0)]):(cljs.core.truth_(G__19170_19435)?c:cljs.core.nth.call(null,c,(0),0.0)));
var G__19166_19444 = ((G__19168_19433)?(G__19160_19438[(1)]):(cljs.core.truth_(G__19170_19435)?c:cljs.core.nth.call(null,c,(1),0.0)));
var x1_19445 = (((G__19163_19441 - G__19161_19439) * u) + G__19161_19439);
var y1_19446 = (((G__19164_19442 - G__19162_19440) * u) + G__19162_19440);
(b_SINGLEQUOTE_[(0)] = ((((((dx - G__19165_19443) * u) + G__19165_19443) - x1_19445) * v) + x1_19445));

(b_SINGLEQUOTE_[(1)] = ((((((dy - G__19166_19444) * u) + G__19166_19444) - y1_19446) * v) + y1_19446));

return (new thi.ng.geom.core.vector.Vec2(b_SINGLEQUOTE_,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PLimit$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PLimit$limit$arity$2 = (function (_,len){
var self__ = this;
var ___$1 = this;
if((thi.ng.geom.core.mag_squared.call(null,___$1) > (len * len))){
return thi.ng.geom.core.normalize.call(null,___$1,len);
} else {
return ___$1;
}
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PPolar$ = true;

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PPolar$as_polar$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var b = (new Float32Array((2)));
(b[(0)] = thi.ng.geom.core.mag.call(null,___$1));

(b[(1)] = thi.ng.geom.core.heading.call(null,___$1));

return (new thi.ng.geom.core.vector.Vec2(b,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.prototype.thi$ng$geom$core$PPolar$as_cartesian$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var G__18841 = self__.buf;
var G__18842 = (G__18841[(0)]);
var G__18843 = (G__18841[(1)]);
var b = (new Float32Array((2)));
(b[(0)] = (G__18842 * Math.cos(G__18843)));

(b[(1)] = (G__18842 * Math.sin(G__18843)));

return (new thi.ng.geom.core.vector.Vec2(b,null,self__._meta));
});

thi.ng.geom.core.vector.Vec2.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"buf","buf",1426618187,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_hash","_hash",-2130838312,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"mutable","mutable",875778266),true], null)),new cljs.core.Symbol(null,"_meta","_meta",-1716892533,null)], null);
});

thi.ng.geom.core.vector.Vec2.cljs$lang$type = true;

thi.ng.geom.core.vector.Vec2.cljs$lang$ctorStr = "thi.ng.geom.core.vector/Vec2";

thi.ng.geom.core.vector.Vec2.cljs$lang$ctorPrWriter = (function (this__17394__auto__,writer__17395__auto__,opt__17396__auto__){
return cljs.core._write.call(null,writer__17395__auto__,"thi.ng.geom.core.vector/Vec2");
});

thi.ng.geom.core.vector.__GT_Vec2 = (function thi$ng$geom$core$vector$__GT_Vec2(buf,_hash,_meta){
return (new thi.ng.geom.core.vector.Vec2(buf,_hash,_meta));
});


/**
* @constructor
 * @implements {thi.ng.geom.core.PScale}
 * @implements {thi.ng.geom.core.PHeading}
 * @implements {thi.ng.geom.core.PMagnitude}
 * @implements {cljs.core.IIndexed}
 * @implements {thi.ng.geom.core.PDistance}
 * @implements {cljs.core.IVector}
 * @implements {cljs.core.IReversible}
 * @implements {thi.ng.geom.core.vector.Object}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {thi.ng.geom.core.PMathOps}
 * @implements {cljs.core.IFn}
 * @implements {cljs.core.ICollection}
 * @implements {thi.ng.geom.core.PTranslate}
 * @implements {thi.ng.geom.core.PMinMax}
 * @implements {thi.ng.geom.core.PLimit}
 * @implements {thi.ng.geom.core.PVectorReduce}
 * @implements {thi.ng.geom.core.PPolar}
 * @implements {thi.ng.geom.core.PInterpolate}
 * @implements {thi.ng.geom.core.PTransform}
 * @implements {cljs.core.ICounted}
 * @implements {thi.ng.geom.core.PRotate3D}
 * @implements {thi.ng.geom.core.PClear}
 * @implements {thi.ng.geom.core.PDotProduct}
 * @implements {cljs.core.ISeq}
 * @implements {thi.ng.geom.core.PNormalize}
 * @implements {thi.ng.geom.core.PBuffered}
 * @implements {cljs.core.INext}
 * @implements {cljs.core.ISeqable}
 * @implements {thi.ng.geom.core.PRotate}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {thi.ng.geom.core.PCrossProduct}
 * @implements {cljs.core.IStack}
 * @implements {thi.ng.geom.core.PReflect}
 * @implements {thi.ng.geom.core.PMutableMathOps}
 * @implements {thi.ng.math.core.PDeltaEquals}
 * @implements {cljs.core.IComparable}
 * @implements {cljs.core.ISequential}
 * @implements {cljs.core.IWithMeta}
 * @implements {thi.ng.geom.core.PInvert}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.ILookup}
 * @implements {cljs.core.IReduce}
*/
thi.ng.geom.core.vector.Vec3 = (function (buf,_hash,_meta){
this.buf = buf;
this._hash = _hash;
this._meta = _meta;
this.cljs$lang$protocol_mask$partition0$ = 166618075;
this.cljs$lang$protocol_mask$partition1$ = 10240;
})
thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PBuffered$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PBuffered$get_buffer$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.buf;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PBuffered$copy_to_buffer$arity$4 = (function (_,dest,stride,idx){
var self__ = this;
var ___$1 = this;
dest.set(self__.buf,idx);

return (idx + stride);
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PTransform$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PTransform$transform$arity$2 = (function (_,m){
var self__ = this;
var ___$1 = this;
return thi.ng.geom.core.transform_vector.call(null,m,___$1);
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PRotate$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PRotate$rotate$arity$2 = (function (_,theta){
var self__ = this;
var ___$1 = this;
return thi.ng.geom.core.rotate_z.call(null,___$1,theta);
});

thi.ng.geom.core.vector.Vec3.prototype.toString = (function (){
var self__ = this;
var _ = this;
return [cljs.core.str("["),cljs.core.str((self__.buf[(0)])),cljs.core.str(" "),cljs.core.str((self__.buf[(1)])),cljs.core.str(" "),cljs.core.str((self__.buf[(2)])),cljs.core.str("]")].join('');
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (_,k){
var self__ = this;
var ___$1 = this;
if((k instanceof cljs.core.Keyword)){
var temp__4423__auto__ = thi.ng.geom.core.vector.swizzle3_fns.call(null,k);
if(cljs.core.truth_(temp__4423__auto__)){
var f = temp__4423__auto__;
return f.call(null,___$1);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
} else {
if(((k >= (0))) && ((k <= (2)))){
return (self__.buf[k]);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
}
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (_,k,nf){
var self__ = this;
var ___$1 = this;
if((k instanceof cljs.core.Keyword)){
var temp__4423__auto__ = thi.ng.geom.core.vector.swizzle3_fns.call(null,k);
if(cljs.core.truth_(temp__4423__auto__)){
var f = temp__4423__auto__;
return f.call(null,___$1);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
} else {
if(((k >= (0))) && ((k <= (2)))){
return (self__.buf[k]);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
}
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PDotProduct$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PDotProduct$dot$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var G__19448 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19449 = v.buf;
return ((((G__19448[(0)]) * (G__19449[(0)])) + ((G__19448[(1)]) * (G__19449[(1)]))) + ((G__19448[(2)]) * (G__19449[(2)])));
} else {
return ((((G__19448[(0)]) * cljs.core.nth.call(null,v,(0),0.0)) + ((G__19448[(1)]) * cljs.core.nth.call(null,v,(1),0.0))) + ((G__19448[(2)]) * cljs.core.nth.call(null,v,(2),0.0)));
}
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PNormalize$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PNormalize$normalize$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var G__19450 = self__.buf;
var G__19451 = (G__19450[(0)]);
var G__19452 = (G__19450[(1)]);
var G__19453 = (G__19450[(2)]);
var l = Math.sqrt((((G__19451 * G__19451) + (G__19452 * G__19452)) + (G__19453 * G__19453)));
if((l > (0))){
var b = (new Float32Array((3)));
(b[(0)] = (G__19451 / l));

(b[(1)] = (G__19452 / l));

(b[(2)] = (G__19453 / l));

return (new thi.ng.geom.core.vector.Vec3(b,null,self__._meta));
} else {
return ___$1;
}
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PNormalize$normalize$arity$2 = (function (_,len){
var self__ = this;
var ___$1 = this;
var G__19454 = self__.buf;
var G__19455 = (G__19454[(0)]);
var G__19456 = (G__19454[(1)]);
var G__19457 = (G__19454[(2)]);
var l = Math.sqrt((((G__19455 * G__19455) + (G__19456 * G__19456)) + (G__19457 * G__19457)));
if((l > (0))){
var l__$1 = (len / l);
var b = (new Float32Array((3)));
(b[(0)] = (G__19455 * l__$1));

(b[(1)] = (G__19456 * l__$1));

(b[(2)] = (G__19457 * l__$1));

return (new thi.ng.geom.core.vector.Vec3(b,null,self__._meta));
} else {
return ___$1;
}
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PNormalize$normalized_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return thi.ng.math.core.delta_EQ_.call(null,1.0,thi.ng.geom.core.mag_squared.call(null,___$1));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PRotate3D$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PRotate3D$rotate_x$arity$2 = (function (_,theta){
var self__ = this;
var ___$1 = this;
var s = Math.sin(theta);
var c = Math.cos(theta);
var b = (new Float32Array((3)));
var G__19458 = self__.buf;
var G__19459 = (G__19458[(0)]);
var G__19460 = (G__19458[(1)]);
var G__19461 = (G__19458[(2)]);
(b[(0)] = G__19459);

(b[(1)] = ((G__19460 * c) - (G__19461 * s)));

(b[(2)] = ((G__19460 * s) + (G__19461 * c)));

return (new thi.ng.geom.core.vector.Vec3(b,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PRotate3D$rotate_y$arity$2 = (function (_,theta){
var self__ = this;
var ___$1 = this;
var s = Math.sin(theta);
var c = Math.cos(theta);
var b = (new Float32Array((3)));
var G__19462 = self__.buf;
var G__19463 = (G__19462[(0)]);
var G__19464 = (G__19462[(1)]);
var G__19465 = (G__19462[(2)]);
(b[(0)] = ((G__19463 * c) + (G__19465 * s)));

(b[(1)] = G__19464);

(b[(2)] = ((G__19465 * c) - (G__19463 * s)));

return (new thi.ng.geom.core.vector.Vec3(b,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PRotate3D$rotate_z$arity$2 = (function (_,theta){
var self__ = this;
var ___$1 = this;
var s = Math.sin(theta);
var c = Math.cos(theta);
var b = (new Float32Array((3)));
var G__19466 = self__.buf;
var G__19467 = (G__19466[(0)]);
var G__19468 = (G__19466[(1)]);
var G__19469 = (G__19466[(2)]);
(b[(0)] = ((G__19467 * c) - (G__19468 * s)));

(b[(1)] = ((G__19467 * s) + (G__19468 * c)));

(b[(2)] = G__19469);

return (new thi.ng.geom.core.vector.Vec3(b,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PRotate3D$rotate_around_axis$arity$3 = (function (_,v,theta){
var self__ = this;
var ___$1 = this;
var G__19470 = self__.buf;
var G__19472 = (G__19470[(0)]);
var G__19473 = (G__19470[(1)]);
var G__19474 = (G__19470[(2)]);
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19471 = v.buf;
var G__19475 = (G__19471[(0)]);
var G__19476 = (G__19471[(1)]);
var G__19477 = (G__19471[(2)]);
var ux_SINGLEQUOTE_ = (G__19475 * G__19472);
var uy_SINGLEQUOTE_ = (G__19475 * G__19473);
var uz_SINGLEQUOTE_ = (G__19475 * G__19474);
var vx_SINGLEQUOTE_ = (G__19476 * G__19472);
var vy_SINGLEQUOTE_ = (G__19476 * G__19473);
var vz_SINGLEQUOTE_ = (G__19476 * G__19474);
var wx_SINGLEQUOTE_ = (G__19477 * G__19472);
var wy_SINGLEQUOTE_ = (G__19477 * G__19473);
var wz_SINGLEQUOTE_ = (G__19477 * G__19474);
var vx2 = (G__19475 * G__19475);
var vy2 = (G__19476 * G__19476);
var vz2 = (G__19477 * G__19477);
var s = Math.sin(theta);
var c = Math.cos(theta);
var uvw = ((ux_SINGLEQUOTE_ + vy_SINGLEQUOTE_) + wz_SINGLEQUOTE_);
var b = (new Float32Array((3)));
(b[(0)] = (((uvw * G__19475) + ((((vy2 + vz2) * G__19472) - ((vy_SINGLEQUOTE_ + wz_SINGLEQUOTE_) * G__19475)) * c)) + ((vz_SINGLEQUOTE_ - wy_SINGLEQUOTE_) * s)));

(b[(1)] = (((uvw * G__19476) + ((((vx2 + vz2) * G__19473) - ((ux_SINGLEQUOTE_ + wz_SINGLEQUOTE_) * G__19476)) * c)) + ((wx_SINGLEQUOTE_ - uz_SINGLEQUOTE_) * s)));

(b[(2)] = (((uvw * G__19477) + ((((vx2 + vy2) * G__19474) - ((ux_SINGLEQUOTE_ + vy_SINGLEQUOTE_) * G__19477)) * c)) + ((uy_SINGLEQUOTE_ - vx_SINGLEQUOTE_) * s)));

return (new thi.ng.geom.core.vector.Vec3(b,null,self__._meta));
} else {
var G__19475 = cljs.core.nth.call(null,v,(0),0.0);
var G__19476 = cljs.core.nth.call(null,v,(1),0.0);
var G__19477 = cljs.core.nth.call(null,v,(2),0.0);
var ux_SINGLEQUOTE_ = (G__19475 * G__19472);
var uy_SINGLEQUOTE_ = (G__19475 * G__19473);
var uz_SINGLEQUOTE_ = (G__19475 * G__19474);
var vx_SINGLEQUOTE_ = (G__19476 * G__19472);
var vy_SINGLEQUOTE_ = (G__19476 * G__19473);
var vz_SINGLEQUOTE_ = (G__19476 * G__19474);
var wx_SINGLEQUOTE_ = (G__19477 * G__19472);
var wy_SINGLEQUOTE_ = (G__19477 * G__19473);
var wz_SINGLEQUOTE_ = (G__19477 * G__19474);
var vx2 = (G__19475 * G__19475);
var vy2 = (G__19476 * G__19476);
var vz2 = (G__19477 * G__19477);
var s = Math.sin(theta);
var c = Math.cos(theta);
var uvw = ((ux_SINGLEQUOTE_ + vy_SINGLEQUOTE_) + wz_SINGLEQUOTE_);
var b = (new Float32Array((3)));
(b[(0)] = (((uvw * G__19475) + ((((vy2 + vz2) * G__19472) - ((vy_SINGLEQUOTE_ + wz_SINGLEQUOTE_) * G__19475)) * c)) + ((vz_SINGLEQUOTE_ - wy_SINGLEQUOTE_) * s)));

(b[(1)] = (((uvw * G__19476) + ((((vx2 + vz2) * G__19473) - ((ux_SINGLEQUOTE_ + wz_SINGLEQUOTE_) * G__19476)) * c)) + ((wx_SINGLEQUOTE_ - uz_SINGLEQUOTE_) * s)));

(b[(2)] = (((uvw * G__19477) + ((((vx2 + vy2) * G__19474) - ((ux_SINGLEQUOTE_ + vy_SINGLEQUOTE_) * G__19477)) * c)) + ((uy_SINGLEQUOTE_ - vx_SINGLEQUOTE_) * s)));

return (new thi.ng.geom.core.vector.Vec3(b,null,self__._meta));
}
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PClear$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PClear$clear_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return (new thi.ng.geom.core.vector.Vec3((new Float32Array((3))),null,null));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PClear$clear_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
(self__.buf[(0)] = 0.0);

(self__.buf[(1)] = 0.0);

(self__.buf[(2)] = 0.0);

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PReflect$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PReflect$reflect$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var b = (new Float32Array((3)));
var G__19478 = self__.buf;
var G__19480 = (G__19478[(0)]);
var G__19481 = (G__19478[(1)]);
var G__19482 = (G__19478[(2)]);
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19479 = v.buf;
var G__19483 = (G__19479[(0)]);
var G__19484 = (G__19479[(1)]);
var G__19485 = (G__19479[(2)]);
var d = ((((G__19480 * G__19483) + (G__19481 * G__19484)) + (G__19482 * G__19485)) * 2.0);
(b[(0)] = ((G__19483 * d) - G__19480));

(b[(1)] = ((G__19484 * d) - G__19481));

(b[(2)] = ((G__19485 * d) - G__19482));

return (new thi.ng.geom.core.vector.Vec3(b,null,self__._meta));
} else {
var G__19483 = cljs.core.nth.call(null,v,(0),0.0);
var G__19484 = cljs.core.nth.call(null,v,(1),0.0);
var G__19485 = cljs.core.nth.call(null,v,(2),0.0);
var d = ((((G__19480 * G__19483) + (G__19481 * G__19484)) + (G__19482 * G__19485)) * 2.0);
(b[(0)] = ((G__19483 * d) - G__19480));

(b[(1)] = ((G__19484 * d) - G__19481));

(b[(2)] = ((G__19485 * d) - G__19482));

return (new thi.ng.geom.core.vector.Vec3(b,null,self__._meta));
}
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PCrossProduct$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PCrossProduct$cross$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var b = (new Float32Array((3)));
var G__19486_19899 = self__.buf;
var G__19488_19900 = (G__19486_19899[(0)]);
var G__19489_19901 = (G__19486_19899[(1)]);
var G__19490_19902 = (G__19486_19899[(2)]);
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19487_19903 = v.buf;
var G__19491_19904 = (G__19487_19903[(0)]);
var G__19492_19905 = (G__19487_19903[(1)]);
var G__19493_19906 = (G__19487_19903[(2)]);
(b[(0)] = ((G__19489_19901 * G__19493_19906) - (G__19492_19905 * G__19490_19902)));

(b[(1)] = ((G__19490_19902 * G__19491_19904) - (G__19493_19906 * G__19488_19900)));

(b[(2)] = ((G__19488_19900 * G__19492_19905) - (G__19491_19904 * G__19489_19901)));
} else {
var G__19491_19907 = cljs.core.nth.call(null,v,(0),0.0);
var G__19492_19908 = cljs.core.nth.call(null,v,(1),0.0);
var G__19493_19909 = cljs.core.nth.call(null,v,(2),0.0);
(b[(0)] = ((G__19489_19901 * G__19493_19909) - (G__19492_19908 * G__19490_19902)));

(b[(1)] = ((G__19490_19902 * G__19491_19907) - (G__19493_19909 * G__19488_19900)));

(b[(2)] = ((G__19488_19900 * G__19492_19908) - (G__19491_19907 * G__19489_19901)));
}

return (new thi.ng.geom.core.vector.Vec3(b,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (_,n){
var self__ = this;
var ___$1 = this;
if((n >= (0))){
if((n < (3))){
return (self__.buf[n]);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,n);
}
} else {
return null;
}
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (_,n,nf){
var self__ = this;
var ___$1 = this;
if((n >= (0))){
if((n < (3))){
return (self__.buf[n]);
} else {
return nf;
}
} else {
return null;
}
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IVector$_assoc_n$arity$3 = (function (_,n,v){
var self__ = this;
var ___$1 = this;
var b = (new Float32Array(self__.buf));
(b[n] = v);

return (new thi.ng.geom.core.vector.Vec3(b,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__._meta;
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return (new thi.ng.geom.core.vector.Vec3((new Float32Array(self__.buf)),self__._hash,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$INext$_next$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.cons.call(null,(self__.buf[(1)]),cljs.core.cons.call(null,(self__.buf[(2)]),null));
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$ICounted$_count$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return (3);
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IStack$_peek$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return (self__.buf[(2)]);
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IStack$_pop$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var b = (new Float32Array((2)));
(b[(0)] = (self__.buf[(0)]));

(b[(1)] = (self__.buf[(1)]));

return (new thi.ng.geom.core.vector.Vec2(b,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$__BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
(self__.buf[(0)] = (- (self__.buf[(0)])));

(self__.buf[(1)] = (- (self__.buf[(1)])));

(self__.buf[(2)] = (- (self__.buf[(2)])));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$__BANG_$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var G__19494_19910 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19495_19911 = v.buf;
(self__.buf[(0)] = ((G__19494_19910[(0)]) - (G__19495_19911[(0)])));

(self__.buf[(1)] = ((G__19494_19910[(1)]) - (G__19495_19911[(1)])));

(self__.buf[(2)] = ((G__19494_19910[(2)]) - (G__19495_19911[(2)])));

self__._hash = null;
} else {
if(typeof v === 'number'){
(self__.buf[(0)] = ((G__19494_19910[(0)]) - v));

(self__.buf[(1)] = ((G__19494_19910[(1)]) - v));

(self__.buf[(2)] = ((G__19494_19910[(2)]) - v));

self__._hash = null;
} else {
(self__.buf[(0)] = ((G__19494_19910[(0)]) - cljs.core.nth.call(null,v,(0),0.0)));

(self__.buf[(1)] = ((G__19494_19910[(1)]) - cljs.core.nth.call(null,v,(1),0.0)));

(self__.buf[(2)] = ((G__19494_19910[(2)]) - cljs.core.nth.call(null,v,(2),0.0)));

self__._hash = null;
}
}

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$__BANG_$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var G__19508_19912 = (v1 instanceof thi.ng.geom.core.vector.Vec3);
var G__19509_19913 = (v2 instanceof thi.ng.geom.core.vector.Vec3);
var G__19510_19914 = ((!(G__19508_19912))?typeof v1 === 'number':null);
var G__19511_19915 = ((!(G__19509_19913))?typeof v2 === 'number':null);
var G__19496_19916 = self__.buf;
var G__19497_19917 = ((G__19508_19912)?v1.buf:null);
var G__19498_19918 = ((G__19509_19913)?v2.buf:null);
var G__19499_19919 = (G__19496_19916[(0)]);
var G__19500_19920 = (G__19496_19916[(1)]);
var G__19501_19921 = (G__19496_19916[(2)]);
var G__19502_19922 = ((G__19508_19912)?(G__19497_19917[(0)]):(cljs.core.truth_(G__19510_19914)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__19503_19923 = ((G__19508_19912)?(G__19497_19917[(1)]):(cljs.core.truth_(G__19510_19914)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__19504_19924 = ((G__19508_19912)?(G__19497_19917[(2)]):(cljs.core.truth_(G__19510_19914)?v1:cljs.core.nth.call(null,v1,(2),0.0)));
var G__19505_19925 = ((G__19509_19913)?(G__19498_19918[(0)]):(cljs.core.truth_(G__19511_19915)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19506_19926 = ((G__19509_19913)?(G__19498_19918[(1)]):(cljs.core.truth_(G__19511_19915)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
var G__19507_19927 = ((G__19509_19913)?(G__19498_19918[(2)]):(cljs.core.truth_(G__19511_19915)?v2:cljs.core.nth.call(null,v2,(2),0.0)));
(self__.buf[(0)] = ((G__19499_19919 - G__19502_19922) - G__19505_19925));

(self__.buf[(1)] = ((G__19500_19920 - G__19503_19923) - G__19506_19926));

(self__.buf[(2)] = ((G__19501_19921 - G__19504_19924) - G__19507_19927));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$__BANG_$arity$4 = (function (_,x,y,z){
var self__ = this;
var ___$1 = this;
(self__.buf[(0)] = ((self__.buf[(0)]) - x));

(self__.buf[(1)] = ((self__.buf[(1)]) - y));

(self__.buf[(2)] = ((self__.buf[(2)]) - z));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$_STAR__BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$_STAR__BANG_$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var G__19512_19928 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19513_19929 = v.buf;
(self__.buf[(0)] = ((G__19512_19928[(0)]) * (G__19513_19929[(0)])));

(self__.buf[(1)] = ((G__19512_19928[(1)]) * (G__19513_19929[(1)])));

(self__.buf[(2)] = ((G__19512_19928[(2)]) * (G__19513_19929[(2)])));

self__._hash = null;
} else {
if(typeof v === 'number'){
(self__.buf[(0)] = ((G__19512_19928[(0)]) * v));

(self__.buf[(1)] = ((G__19512_19928[(1)]) * v));

(self__.buf[(2)] = ((G__19512_19928[(2)]) * v));

self__._hash = null;
} else {
(self__.buf[(0)] = ((G__19512_19928[(0)]) * cljs.core.nth.call(null,v,(0),0.0)));

(self__.buf[(1)] = ((G__19512_19928[(1)]) * cljs.core.nth.call(null,v,(1),0.0)));

(self__.buf[(2)] = ((G__19512_19928[(2)]) * cljs.core.nth.call(null,v,(2),0.0)));

self__._hash = null;
}
}

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$_STAR__BANG_$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var G__19526_19930 = (v1 instanceof thi.ng.geom.core.vector.Vec3);
var G__19527_19931 = (v2 instanceof thi.ng.geom.core.vector.Vec3);
var G__19528_19932 = ((!(G__19526_19930))?typeof v1 === 'number':null);
var G__19529_19933 = ((!(G__19527_19931))?typeof v2 === 'number':null);
var G__19514_19934 = self__.buf;
var G__19515_19935 = ((G__19526_19930)?v1.buf:null);
var G__19516_19936 = ((G__19527_19931)?v2.buf:null);
var G__19517_19937 = (G__19514_19934[(0)]);
var G__19518_19938 = (G__19514_19934[(1)]);
var G__19519_19939 = (G__19514_19934[(2)]);
var G__19520_19940 = ((G__19526_19930)?(G__19515_19935[(0)]):(cljs.core.truth_(G__19528_19932)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__19521_19941 = ((G__19526_19930)?(G__19515_19935[(1)]):(cljs.core.truth_(G__19528_19932)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__19522_19942 = ((G__19526_19930)?(G__19515_19935[(2)]):(cljs.core.truth_(G__19528_19932)?v1:cljs.core.nth.call(null,v1,(2),0.0)));
var G__19523_19943 = ((G__19527_19931)?(G__19516_19936[(0)]):(cljs.core.truth_(G__19529_19933)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19524_19944 = ((G__19527_19931)?(G__19516_19936[(1)]):(cljs.core.truth_(G__19529_19933)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
var G__19525_19945 = ((G__19527_19931)?(G__19516_19936[(2)]):(cljs.core.truth_(G__19529_19933)?v2:cljs.core.nth.call(null,v2,(2),0.0)));
(self__.buf[(0)] = ((G__19517_19937 * G__19520_19940) * G__19523_19943));

(self__.buf[(1)] = ((G__19518_19938 * G__19521_19941) * G__19524_19944));

(self__.buf[(2)] = ((G__19519_19939 * G__19522_19942) * G__19525_19945));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$_STAR__BANG_$arity$4 = (function (_,x,y,z){
var self__ = this;
var ___$1 = this;
(self__.buf[(0)] = ((self__.buf[(0)]) * x));

(self__.buf[(1)] = ((self__.buf[(1)]) * y));

(self__.buf[(2)] = ((self__.buf[(2)]) * z));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$subm_BANG_$arity$3 = (function (_,a,b){
var self__ = this;
var ___$1 = this;
var G__19542_19946 = (a instanceof thi.ng.geom.core.vector.Vec3);
var G__19543_19947 = (b instanceof thi.ng.geom.core.vector.Vec3);
var G__19544_19948 = ((!(G__19542_19946))?typeof a === 'number':null);
var G__19545_19949 = ((!(G__19543_19947))?typeof b === 'number':null);
var G__19530_19950 = self__.buf;
var G__19531_19951 = ((G__19542_19946)?a.buf:null);
var G__19532_19952 = ((G__19543_19947)?b.buf:null);
var G__19533_19953 = (G__19530_19950[(0)]);
var G__19534_19954 = (G__19530_19950[(1)]);
var G__19535_19955 = (G__19530_19950[(2)]);
var G__19536_19956 = ((G__19542_19946)?(G__19531_19951[(0)]):(cljs.core.truth_(G__19544_19948)?a:cljs.core.nth.call(null,a,(0),0.0)));
var G__19537_19957 = ((G__19542_19946)?(G__19531_19951[(1)]):(cljs.core.truth_(G__19544_19948)?a:cljs.core.nth.call(null,a,(1),0.0)));
var G__19538_19958 = ((G__19542_19946)?(G__19531_19951[(2)]):(cljs.core.truth_(G__19544_19948)?a:cljs.core.nth.call(null,a,(2),0.0)));
var G__19539_19959 = ((G__19543_19947)?(G__19532_19952[(0)]):(cljs.core.truth_(G__19545_19949)?b:cljs.core.nth.call(null,b,(0),1.0)));
var G__19540_19960 = ((G__19543_19947)?(G__19532_19952[(1)]):(cljs.core.truth_(G__19545_19949)?b:cljs.core.nth.call(null,b,(1),1.0)));
var G__19541_19961 = ((G__19543_19947)?(G__19532_19952[(2)]):(cljs.core.truth_(G__19545_19949)?b:cljs.core.nth.call(null,b,(2),1.0)));
(self__.buf[(0)] = ((G__19533_19953 - G__19536_19956) * G__19539_19959));

(self__.buf[(1)] = ((G__19534_19954 - G__19537_19957) * G__19540_19960));

(self__.buf[(2)] = ((G__19535_19955 - G__19538_19958) * G__19541_19961));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$msub_BANG_$arity$3 = (function (_,a,b){
var self__ = this;
var ___$1 = this;
var G__19558_19962 = (a instanceof thi.ng.geom.core.vector.Vec3);
var G__19559_19963 = (b instanceof thi.ng.geom.core.vector.Vec3);
var G__19560_19964 = ((!(G__19558_19962))?typeof a === 'number':null);
var G__19561_19965 = ((!(G__19559_19963))?typeof b === 'number':null);
var G__19546_19966 = self__.buf;
var G__19547_19967 = ((G__19558_19962)?a.buf:null);
var G__19548_19968 = ((G__19559_19963)?b.buf:null);
var G__19549_19969 = (G__19546_19966[(0)]);
var G__19550_19970 = (G__19546_19966[(1)]);
var G__19551_19971 = (G__19546_19966[(2)]);
var G__19552_19972 = ((G__19558_19962)?(G__19547_19967[(0)]):(cljs.core.truth_(G__19560_19964)?a:cljs.core.nth.call(null,a,(0),1.0)));
var G__19553_19973 = ((G__19558_19962)?(G__19547_19967[(1)]):(cljs.core.truth_(G__19560_19964)?a:cljs.core.nth.call(null,a,(1),1.0)));
var G__19554_19974 = ((G__19558_19962)?(G__19547_19967[(2)]):(cljs.core.truth_(G__19560_19964)?a:cljs.core.nth.call(null,a,(2),1.0)));
var G__19555_19975 = ((G__19559_19963)?(G__19548_19968[(0)]):(cljs.core.truth_(G__19561_19965)?b:cljs.core.nth.call(null,b,(0),0.0)));
var G__19556_19976 = ((G__19559_19963)?(G__19548_19968[(1)]):(cljs.core.truth_(G__19561_19965)?b:cljs.core.nth.call(null,b,(1),0.0)));
var G__19557_19977 = ((G__19559_19963)?(G__19548_19968[(2)]):(cljs.core.truth_(G__19561_19965)?b:cljs.core.nth.call(null,b,(2),0.0)));
(self__.buf[(0)] = ((G__19549_19969 * G__19552_19972) - G__19555_19975));

(self__.buf[(1)] = ((G__19550_19970 * G__19553_19973) - G__19556_19976));

(self__.buf[(2)] = ((G__19551_19971 * G__19554_19974) - G__19557_19977));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$abs_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
(self__.buf[(0)] = thi.ng.math.core.abs.call(null,(self__.buf[(0)])));

(self__.buf[(1)] = thi.ng.math.core.abs.call(null,(self__.buf[(1)])));

(self__.buf[(2)] = thi.ng.math.core.abs.call(null,(self__.buf[(2)])));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$madd_BANG_$arity$3 = (function (_,a,b){
var self__ = this;
var ___$1 = this;
var G__19574_19978 = (a instanceof thi.ng.geom.core.vector.Vec3);
var G__19575_19979 = (b instanceof thi.ng.geom.core.vector.Vec3);
var G__19576_19980 = ((!(G__19574_19978))?typeof a === 'number':null);
var G__19577_19981 = ((!(G__19575_19979))?typeof b === 'number':null);
var G__19562_19982 = self__.buf;
var G__19563_19983 = ((G__19574_19978)?a.buf:null);
var G__19564_19984 = ((G__19575_19979)?b.buf:null);
var G__19565_19985 = (G__19562_19982[(0)]);
var G__19566_19986 = (G__19562_19982[(1)]);
var G__19567_19987 = (G__19562_19982[(2)]);
var G__19568_19988 = ((G__19574_19978)?(G__19563_19983[(0)]):(cljs.core.truth_(G__19576_19980)?a:cljs.core.nth.call(null,a,(0),1.0)));
var G__19569_19989 = ((G__19574_19978)?(G__19563_19983[(1)]):(cljs.core.truth_(G__19576_19980)?a:cljs.core.nth.call(null,a,(1),1.0)));
var G__19570_19990 = ((G__19574_19978)?(G__19563_19983[(2)]):(cljs.core.truth_(G__19576_19980)?a:cljs.core.nth.call(null,a,(2),1.0)));
var G__19571_19991 = ((G__19575_19979)?(G__19564_19984[(0)]):(cljs.core.truth_(G__19577_19981)?b:cljs.core.nth.call(null,b,(0),0.0)));
var G__19572_19992 = ((G__19575_19979)?(G__19564_19984[(1)]):(cljs.core.truth_(G__19577_19981)?b:cljs.core.nth.call(null,b,(1),0.0)));
var G__19573_19993 = ((G__19575_19979)?(G__19564_19984[(2)]):(cljs.core.truth_(G__19577_19981)?b:cljs.core.nth.call(null,b,(2),0.0)));
(self__.buf[(0)] = ((G__19565_19985 * G__19568_19988) + G__19571_19991));

(self__.buf[(1)] = ((G__19566_19986 * G__19569_19989) + G__19572_19992));

(self__.buf[(2)] = ((G__19567_19987 * G__19570_19990) + G__19573_19993));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$div_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
(self__.buf[(0)] = ((1) / (self__.buf[(0)])));

(self__.buf[(1)] = ((1) / (self__.buf[(1)])));

(self__.buf[(2)] = ((1) / (self__.buf[(2)])));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$div_BANG_$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var G__19578_19994 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19579_19995 = v.buf;
(self__.buf[(0)] = ((G__19578_19994[(0)]) / (G__19579_19995[(0)])));

(self__.buf[(1)] = ((G__19578_19994[(1)]) / (G__19579_19995[(1)])));

(self__.buf[(2)] = ((G__19578_19994[(2)]) / (G__19579_19995[(2)])));

self__._hash = null;
} else {
if(typeof v === 'number'){
(self__.buf[(0)] = ((G__19578_19994[(0)]) / v));

(self__.buf[(1)] = ((G__19578_19994[(1)]) / v));

(self__.buf[(2)] = ((G__19578_19994[(2)]) / v));

self__._hash = null;
} else {
(self__.buf[(0)] = ((G__19578_19994[(0)]) / cljs.core.nth.call(null,v,(0),0.0)));

(self__.buf[(1)] = ((G__19578_19994[(1)]) / cljs.core.nth.call(null,v,(1),0.0)));

(self__.buf[(2)] = ((G__19578_19994[(2)]) / cljs.core.nth.call(null,v,(2),0.0)));

self__._hash = null;
}
}

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$div_BANG_$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var G__19592_19996 = (v1 instanceof thi.ng.geom.core.vector.Vec3);
var G__19593_19997 = (v2 instanceof thi.ng.geom.core.vector.Vec3);
var G__19594_19998 = ((!(G__19592_19996))?typeof v1 === 'number':null);
var G__19595_19999 = ((!(G__19593_19997))?typeof v2 === 'number':null);
var G__19580_20000 = self__.buf;
var G__19581_20001 = ((G__19592_19996)?v1.buf:null);
var G__19582_20002 = ((G__19593_19997)?v2.buf:null);
var G__19583_20003 = (G__19580_20000[(0)]);
var G__19584_20004 = (G__19580_20000[(1)]);
var G__19585_20005 = (G__19580_20000[(2)]);
var G__19586_20006 = ((G__19592_19996)?(G__19581_20001[(0)]):(cljs.core.truth_(G__19594_19998)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__19587_20007 = ((G__19592_19996)?(G__19581_20001[(1)]):(cljs.core.truth_(G__19594_19998)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__19588_20008 = ((G__19592_19996)?(G__19581_20001[(2)]):(cljs.core.truth_(G__19594_19998)?v1:cljs.core.nth.call(null,v1,(2),0.0)));
var G__19589_20009 = ((G__19593_19997)?(G__19582_20002[(0)]):(cljs.core.truth_(G__19595_19999)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19590_20010 = ((G__19593_19997)?(G__19582_20002[(1)]):(cljs.core.truth_(G__19595_19999)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
var G__19591_20011 = ((G__19593_19997)?(G__19582_20002[(2)]):(cljs.core.truth_(G__19595_19999)?v2:cljs.core.nth.call(null,v2,(2),0.0)));
(self__.buf[(0)] = ((G__19583_20003 / G__19586_20006) / G__19589_20009));

(self__.buf[(1)] = ((G__19584_20004 / G__19587_20007) / G__19590_20010));

(self__.buf[(2)] = ((G__19585_20005 / G__19588_20008) / G__19591_20011));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$div_BANG_$arity$4 = (function (_,x,y,z){
var self__ = this;
var ___$1 = this;
(self__.buf[(0)] = ((self__.buf[(0)]) / x));

(self__.buf[(1)] = ((self__.buf[(1)]) / y));

(self__.buf[(2)] = ((self__.buf[(2)]) / z));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$_PLUS__BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$_PLUS__BANG_$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var G__19596_20012 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19597_20013 = v.buf;
(self__.buf[(0)] = ((G__19596_20012[(0)]) + (G__19597_20013[(0)])));

(self__.buf[(1)] = ((G__19596_20012[(1)]) + (G__19597_20013[(1)])));

(self__.buf[(2)] = ((G__19596_20012[(2)]) + (G__19597_20013[(2)])));

self__._hash = null;
} else {
if(typeof v === 'number'){
(self__.buf[(0)] = ((G__19596_20012[(0)]) + v));

(self__.buf[(1)] = ((G__19596_20012[(1)]) + v));

(self__.buf[(2)] = ((G__19596_20012[(2)]) + v));

self__._hash = null;
} else {
(self__.buf[(0)] = ((G__19596_20012[(0)]) + cljs.core.nth.call(null,v,(0),0.0)));

(self__.buf[(1)] = ((G__19596_20012[(1)]) + cljs.core.nth.call(null,v,(1),0.0)));

(self__.buf[(2)] = ((G__19596_20012[(2)]) + cljs.core.nth.call(null,v,(2),0.0)));

self__._hash = null;
}
}

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$_PLUS__BANG_$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var G__19610_20014 = (v1 instanceof thi.ng.geom.core.vector.Vec3);
var G__19611_20015 = (v2 instanceof thi.ng.geom.core.vector.Vec3);
var G__19612_20016 = ((!(G__19610_20014))?typeof v1 === 'number':null);
var G__19613_20017 = ((!(G__19611_20015))?typeof v2 === 'number':null);
var G__19598_20018 = self__.buf;
var G__19599_20019 = ((G__19610_20014)?v1.buf:null);
var G__19600_20020 = ((G__19611_20015)?v2.buf:null);
var G__19601_20021 = (G__19598_20018[(0)]);
var G__19602_20022 = (G__19598_20018[(1)]);
var G__19603_20023 = (G__19598_20018[(2)]);
var G__19604_20024 = ((G__19610_20014)?(G__19599_20019[(0)]):(cljs.core.truth_(G__19612_20016)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__19605_20025 = ((G__19610_20014)?(G__19599_20019[(1)]):(cljs.core.truth_(G__19612_20016)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__19606_20026 = ((G__19610_20014)?(G__19599_20019[(2)]):(cljs.core.truth_(G__19612_20016)?v1:cljs.core.nth.call(null,v1,(2),0.0)));
var G__19607_20027 = ((G__19611_20015)?(G__19600_20020[(0)]):(cljs.core.truth_(G__19613_20017)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19608_20028 = ((G__19611_20015)?(G__19600_20020[(1)]):(cljs.core.truth_(G__19613_20017)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
var G__19609_20029 = ((G__19611_20015)?(G__19600_20020[(2)]):(cljs.core.truth_(G__19613_20017)?v2:cljs.core.nth.call(null,v2,(2),0.0)));
(self__.buf[(0)] = ((G__19601_20021 + G__19604_20024) + G__19607_20027));

(self__.buf[(1)] = ((G__19602_20022 + G__19605_20025) + G__19608_20028));

(self__.buf[(2)] = ((G__19603_20023 + G__19606_20026) + G__19609_20029));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$_PLUS__BANG_$arity$4 = (function (_,x,y,z){
var self__ = this;
var ___$1 = this;
(self__.buf[(0)] = ((self__.buf[(0)]) + x));

(self__.buf[(1)] = ((self__.buf[(1)]) + y));

(self__.buf[(2)] = ((self__.buf[(2)]) + z));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMutableMathOps$addm_BANG_$arity$3 = (function (_,a,b){
var self__ = this;
var ___$1 = this;
var G__19626_20030 = (a instanceof thi.ng.geom.core.vector.Vec3);
var G__19627_20031 = (b instanceof thi.ng.geom.core.vector.Vec3);
var G__19628_20032 = ((!(G__19626_20030))?typeof a === 'number':null);
var G__19629_20033 = ((!(G__19627_20031))?typeof b === 'number':null);
var G__19614_20034 = self__.buf;
var G__19615_20035 = ((G__19626_20030)?a.buf:null);
var G__19616_20036 = ((G__19627_20031)?b.buf:null);
var G__19617_20037 = (G__19614_20034[(0)]);
var G__19618_20038 = (G__19614_20034[(1)]);
var G__19619_20039 = (G__19614_20034[(2)]);
var G__19620_20040 = ((G__19626_20030)?(G__19615_20035[(0)]):(cljs.core.truth_(G__19628_20032)?a:cljs.core.nth.call(null,a,(0),0.0)));
var G__19621_20041 = ((G__19626_20030)?(G__19615_20035[(1)]):(cljs.core.truth_(G__19628_20032)?a:cljs.core.nth.call(null,a,(1),0.0)));
var G__19622_20042 = ((G__19626_20030)?(G__19615_20035[(2)]):(cljs.core.truth_(G__19628_20032)?a:cljs.core.nth.call(null,a,(2),0.0)));
var G__19623_20043 = ((G__19627_20031)?(G__19616_20036[(0)]):(cljs.core.truth_(G__19629_20033)?b:cljs.core.nth.call(null,b,(0),1.0)));
var G__19624_20044 = ((G__19627_20031)?(G__19616_20036[(1)]):(cljs.core.truth_(G__19629_20033)?b:cljs.core.nth.call(null,b,(1),1.0)));
var G__19625_20045 = ((G__19627_20031)?(G__19616_20036[(2)]):(cljs.core.truth_(G__19629_20033)?b:cljs.core.nth.call(null,b,(2),1.0)));
(self__.buf[(0)] = ((G__19617_20037 + G__19620_20040) * G__19623_20043));

(self__.buf[(1)] = ((G__19618_20038 + G__19621_20041) * G__19624_20044));

(self__.buf[(2)] = ((G__19619_20039 + G__19622_20042) * G__19625_20045));

self__._hash = null;

return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IReversible$_rseq$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return thi.ng.geom.core.vector.swizzle3_fns.call(null,new cljs.core.Keyword(null,"zyx","zyx",1752527951)).call(null,___$1);
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IHash$_hash$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var or__16796__auto__ = self__._hash;
if(cljs.core.truth_(or__16796__auto__)){
return or__16796__auto__;
} else {
return ___$1._hash = cljs.core.mix_collection_hash.call(null,((cljs.core.imul.call(null,((cljs.core.imul.call(null,(((31) + cljs.core.hash.call(null,(self__.buf[(0)]))) | (0)),(31)) + cljs.core.hash.call(null,(self__.buf[(1)]))) | (0)),(31)) + cljs.core.hash.call(null,(self__.buf[(2)]))) | (0)),(3));
}
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (_,o){
var self__ = this;
var ___$1 = this;
if((o instanceof thi.ng.geom.core.vector.Vec3)){
var b_SINGLEQUOTE_ = o.buf;
return (((self__.buf[(0)]) === (b_SINGLEQUOTE_[(0)]))) && (((self__.buf[(1)]) === (b_SINGLEQUOTE_[(1)]))) && (((self__.buf[(2)]) === (b_SINGLEQUOTE_[(2)])));
} else {
return (cljs.core.sequential_QMARK_.call(null,o)) && (((3) === cljs.core.count.call(null,o))) && (cljs.core._EQ_.call(null,(self__.buf[(0)]),cljs.core.first.call(null,o))) && (cljs.core._EQ_.call(null,(self__.buf[(1)]),cljs.core.nth.call(null,o,(1)))) && (cljs.core._EQ_.call(null,(self__.buf[(2)]),cljs.core.nth.call(null,o,(2))));
}
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PHeading$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PHeading$heading$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return thi.ng.geom.core.heading_xy.call(null,___$1);
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PHeading$heading_xy$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var t = Math.atan2((self__.buf[(1)]),(self__.buf[(0)]));
if((t < (0))){
return (t + thi.ng.math.core.TWO_PI);
} else {
return t;
}
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PHeading$heading_xz$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var t = Math.atan2((self__.buf[(2)]),(self__.buf[(0)]));
if((t < (0))){
return (t + thi.ng.math.core.TWO_PI);
} else {
return t;
}
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PHeading$heading_yz$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var t = Math.atan2((self__.buf[(2)]),(self__.buf[(1)]));
if((t < (0))){
return (t + thi.ng.math.core.TWO_PI);
} else {
return t;
}
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PHeading$angle_between$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var v__$1 = (((v instanceof thi.ng.geom.core.vector.Vec3))?v:thi.ng.geom.core.vector.vec3.call(null,v));
return Math.acos(thi.ng.geom.core.dot.call(null,thi.ng.geom.core.normalize.call(null,___$1),thi.ng.geom.core.normalize.call(null,v__$1)));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PHeading$slope_xy$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return ((self__.buf[(1)]) / (self__.buf[(0)]));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PHeading$slope_xz$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return ((self__.buf[(2)]) / (self__.buf[(0)]));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PHeading$slope_yz$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return ((self__.buf[(2)]) / (self__.buf[(1)]));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PDistance$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PDistance$dist$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
return Math.sqrt(thi.ng.geom.core.dist_squared.call(null,___$1,v));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PDistance$dist_squared$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var G__19630 = self__.buf;
var G__19632 = (G__19630[(0)]);
var G__19633 = (G__19630[(1)]);
var G__19634 = (G__19630[(2)]);
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19631 = v.buf;
var G__19635 = (G__19631[(0)]);
var G__19636 = (G__19631[(1)]);
var G__19637 = (G__19631[(2)]);
var dx = (G__19632 - G__19635);
var dy = (G__19633 - G__19636);
var dz = (G__19634 - G__19637);
return (((dx * dx) + (dy * dy)) + (dz * dz));
} else {
var G__19635 = cljs.core.nth.call(null,v,(0),0.0);
var G__19636 = cljs.core.nth.call(null,v,(1),0.0);
var G__19637 = cljs.core.nth.call(null,v,(2),0.0);
var dx = (G__19632 - G__19635);
var dy = (G__19633 - G__19636);
var dz = (G__19634 - G__19637);
return (((dx * dx) + (dy * dy)) + (dz * dz));
}
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IReduce$_reduce$arity$2 = (function (coll,f){
var self__ = this;
var coll__$1 = this;
var acc = f.call(null,(self__.buf[(0)]),(self__.buf[(1)]));
if(cljs.core.reduced_QMARK_.call(null,acc)){
return cljs.core.deref.call(null,acc);
} else {
var acc__$1 = f.call(null,acc,(self__.buf[(2)]));
if(cljs.core.reduced_QMARK_.call(null,acc__$1)){
return cljs.core.deref.call(null,acc__$1);
} else {
return acc__$1;
}
}
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IReduce$_reduce$arity$3 = (function (coll,f,start){
var self__ = this;
var coll__$1 = this;
var acc = f.call(null,start,(self__.buf[(0)]));
if(cljs.core.reduced_QMARK_.call(null,acc)){
return cljs.core.deref.call(null,acc);
} else {
var acc__$1 = f.call(null,acc,(self__.buf[(1)]));
if(cljs.core.reduced_QMARK_.call(null,acc__$1)){
return cljs.core.deref.call(null,acc__$1);
} else {
var acc__$2 = f.call(null,acc__$1,(self__.buf[(2)]));
if(cljs.core.reduced_QMARK_.call(null,acc__$2)){
return cljs.core.deref.call(null,acc__$2);
} else {
return acc__$2;
}
}
}
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PInvert$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PInvert$invert$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return thi.ng.geom.core._.call(null,___$1);
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$msub$arity$3 = (function (_,a,b){
var self__ = this;
var ___$1 = this;
var dest__18802__auto__ = (new Float32Array((3)));
var G__19650_20046 = (a instanceof thi.ng.geom.core.vector.Vec3);
var G__19651_20047 = (b instanceof thi.ng.geom.core.vector.Vec3);
var G__19652_20048 = ((!(G__19650_20046))?typeof a === 'number':null);
var G__19653_20049 = ((!(G__19651_20047))?typeof b === 'number':null);
var G__19638_20050 = self__.buf;
var G__19639_20051 = ((G__19650_20046)?a.buf:null);
var G__19640_20052 = ((G__19651_20047)?b.buf:null);
var G__19641_20053 = (G__19638_20050[(0)]);
var G__19642_20054 = (G__19638_20050[(1)]);
var G__19643_20055 = (G__19638_20050[(2)]);
var G__19644_20056 = ((G__19650_20046)?(G__19639_20051[(0)]):(cljs.core.truth_(G__19652_20048)?a:cljs.core.nth.call(null,a,(0),1.0)));
var G__19645_20057 = ((G__19650_20046)?(G__19639_20051[(1)]):(cljs.core.truth_(G__19652_20048)?a:cljs.core.nth.call(null,a,(1),1.0)));
var G__19646_20058 = ((G__19650_20046)?(G__19639_20051[(2)]):(cljs.core.truth_(G__19652_20048)?a:cljs.core.nth.call(null,a,(2),1.0)));
var G__19647_20059 = ((G__19651_20047)?(G__19640_20052[(0)]):(cljs.core.truth_(G__19653_20049)?b:cljs.core.nth.call(null,b,(0),0.0)));
var G__19648_20060 = ((G__19651_20047)?(G__19640_20052[(1)]):(cljs.core.truth_(G__19653_20049)?b:cljs.core.nth.call(null,b,(1),0.0)));
var G__19649_20061 = ((G__19651_20047)?(G__19640_20052[(2)]):(cljs.core.truth_(G__19653_20049)?b:cljs.core.nth.call(null,b,(2),0.0)));
(dest__18802__auto__[(0)] = ((G__19641_20053 * G__19644_20056) - G__19647_20059));

(dest__18802__auto__[(1)] = ((G__19642_20054 * G__19645_20057) - G__19648_20060));

(dest__18802__auto__[(2)] = ((G__19643_20055 * G__19646_20058) - G__19649_20061));

return (new thi.ng.geom.core.vector.Vec3(dest__18802__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$_STAR_$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var dest__18791__auto__ = (new Float32Array((3)));
var G__19654_20062 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19655_20063 = v.buf;
(dest__18791__auto__[(0)] = ((G__19654_20062[(0)]) * (G__19655_20063[(0)])));

(dest__18791__auto__[(1)] = ((G__19654_20062[(1)]) * (G__19655_20063[(1)])));

(dest__18791__auto__[(2)] = ((G__19654_20062[(2)]) * (G__19655_20063[(2)])));
} else {
if(typeof v === 'number'){
(dest__18791__auto__[(0)] = ((G__19654_20062[(0)]) * v));

(dest__18791__auto__[(1)] = ((G__19654_20062[(1)]) * v));

(dest__18791__auto__[(2)] = ((G__19654_20062[(2)]) * v));
} else {
(dest__18791__auto__[(0)] = ((G__19654_20062[(0)]) * cljs.core.nth.call(null,v,(0),0.0)));

(dest__18791__auto__[(1)] = ((G__19654_20062[(1)]) * cljs.core.nth.call(null,v,(1),0.0)));

(dest__18791__auto__[(2)] = ((G__19654_20062[(2)]) * cljs.core.nth.call(null,v,(2),0.0)));
}
}

return (new thi.ng.geom.core.vector.Vec3(dest__18791__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$_STAR_$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var dest__18802__auto__ = (new Float32Array((3)));
var G__19668_20064 = (v1 instanceof thi.ng.geom.core.vector.Vec3);
var G__19669_20065 = (v2 instanceof thi.ng.geom.core.vector.Vec3);
var G__19670_20066 = ((!(G__19668_20064))?typeof v1 === 'number':null);
var G__19671_20067 = ((!(G__19669_20065))?typeof v2 === 'number':null);
var G__19656_20068 = self__.buf;
var G__19657_20069 = ((G__19668_20064)?v1.buf:null);
var G__19658_20070 = ((G__19669_20065)?v2.buf:null);
var G__19659_20071 = (G__19656_20068[(0)]);
var G__19660_20072 = (G__19656_20068[(1)]);
var G__19661_20073 = (G__19656_20068[(2)]);
var G__19662_20074 = ((G__19668_20064)?(G__19657_20069[(0)]):(cljs.core.truth_(G__19670_20066)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__19663_20075 = ((G__19668_20064)?(G__19657_20069[(1)]):(cljs.core.truth_(G__19670_20066)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__19664_20076 = ((G__19668_20064)?(G__19657_20069[(2)]):(cljs.core.truth_(G__19670_20066)?v1:cljs.core.nth.call(null,v1,(2),0.0)));
var G__19665_20077 = ((G__19669_20065)?(G__19658_20070[(0)]):(cljs.core.truth_(G__19671_20067)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19666_20078 = ((G__19669_20065)?(G__19658_20070[(1)]):(cljs.core.truth_(G__19671_20067)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
var G__19667_20079 = ((G__19669_20065)?(G__19658_20070[(2)]):(cljs.core.truth_(G__19671_20067)?v2:cljs.core.nth.call(null,v2,(2),0.0)));
(dest__18802__auto__[(0)] = ((G__19659_20071 * G__19662_20074) * G__19665_20077));

(dest__18802__auto__[(1)] = ((G__19660_20072 * G__19663_20075) * G__19666_20078));

(dest__18802__auto__[(2)] = ((G__19661_20073 * G__19664_20076) * G__19667_20079));

return (new thi.ng.geom.core.vector.Vec3(dest__18802__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$_STAR_$arity$4 = (function (_,x,y,z){
var self__ = this;
var ___$1 = this;
var G__19672 = self__.buf;
var dest__18780__auto__ = (new Float32Array((3)));
(dest__18780__auto__[(0)] = ((G__19672[(0)]) * x));

(dest__18780__auto__[(1)] = ((G__19672[(1)]) * y));

(dest__18780__auto__[(2)] = ((G__19672[(2)]) * z));

return (new thi.ng.geom.core.vector.Vec3(dest__18780__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var dest__18774__auto__ = (new Float32Array((3)));
var G__19673_20080 = self__.buf;
(dest__18774__auto__[(0)] = (- (G__19673_20080[(0)])));

(dest__18774__auto__[(1)] = (- (G__19673_20080[(1)])));

(dest__18774__auto__[(2)] = (- (G__19673_20080[(2)])));

return (new thi.ng.geom.core.vector.Vec3(dest__18774__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$_$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var dest__18791__auto__ = (new Float32Array((3)));
var G__19674_20081 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19675_20082 = v.buf;
(dest__18791__auto__[(0)] = ((G__19674_20081[(0)]) - (G__19675_20082[(0)])));

(dest__18791__auto__[(1)] = ((G__19674_20081[(1)]) - (G__19675_20082[(1)])));

(dest__18791__auto__[(2)] = ((G__19674_20081[(2)]) - (G__19675_20082[(2)])));
} else {
if(typeof v === 'number'){
(dest__18791__auto__[(0)] = ((G__19674_20081[(0)]) - v));

(dest__18791__auto__[(1)] = ((G__19674_20081[(1)]) - v));

(dest__18791__auto__[(2)] = ((G__19674_20081[(2)]) - v));
} else {
(dest__18791__auto__[(0)] = ((G__19674_20081[(0)]) - cljs.core.nth.call(null,v,(0),0.0)));

(dest__18791__auto__[(1)] = ((G__19674_20081[(1)]) - cljs.core.nth.call(null,v,(1),0.0)));

(dest__18791__auto__[(2)] = ((G__19674_20081[(2)]) - cljs.core.nth.call(null,v,(2),0.0)));
}
}

return (new thi.ng.geom.core.vector.Vec3(dest__18791__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$_$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var dest__18802__auto__ = (new Float32Array((3)));
var G__19688_20083 = (v1 instanceof thi.ng.geom.core.vector.Vec3);
var G__19689_20084 = (v2 instanceof thi.ng.geom.core.vector.Vec3);
var G__19690_20085 = ((!(G__19688_20083))?typeof v1 === 'number':null);
var G__19691_20086 = ((!(G__19689_20084))?typeof v2 === 'number':null);
var G__19676_20087 = self__.buf;
var G__19677_20088 = ((G__19688_20083)?v1.buf:null);
var G__19678_20089 = ((G__19689_20084)?v2.buf:null);
var G__19679_20090 = (G__19676_20087[(0)]);
var G__19680_20091 = (G__19676_20087[(1)]);
var G__19681_20092 = (G__19676_20087[(2)]);
var G__19682_20093 = ((G__19688_20083)?(G__19677_20088[(0)]):(cljs.core.truth_(G__19690_20085)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__19683_20094 = ((G__19688_20083)?(G__19677_20088[(1)]):(cljs.core.truth_(G__19690_20085)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__19684_20095 = ((G__19688_20083)?(G__19677_20088[(2)]):(cljs.core.truth_(G__19690_20085)?v1:cljs.core.nth.call(null,v1,(2),0.0)));
var G__19685_20096 = ((G__19689_20084)?(G__19678_20089[(0)]):(cljs.core.truth_(G__19691_20086)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19686_20097 = ((G__19689_20084)?(G__19678_20089[(1)]):(cljs.core.truth_(G__19691_20086)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
var G__19687_20098 = ((G__19689_20084)?(G__19678_20089[(2)]):(cljs.core.truth_(G__19691_20086)?v2:cljs.core.nth.call(null,v2,(2),0.0)));
(dest__18802__auto__[(0)] = ((G__19679_20090 - G__19682_20093) - G__19685_20096));

(dest__18802__auto__[(1)] = ((G__19680_20091 - G__19683_20094) - G__19686_20097));

(dest__18802__auto__[(2)] = ((G__19681_20092 - G__19684_20095) - G__19687_20098));

return (new thi.ng.geom.core.vector.Vec3(dest__18802__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$_$arity$4 = (function (_,x,y,z){
var self__ = this;
var ___$1 = this;
var G__19692 = self__.buf;
var dest__18780__auto__ = (new Float32Array((3)));
(dest__18780__auto__[(0)] = ((G__19692[(0)]) - x));

(dest__18780__auto__[(1)] = ((G__19692[(1)]) - y));

(dest__18780__auto__[(2)] = ((G__19692[(2)]) - z));

return (new thi.ng.geom.core.vector.Vec3(dest__18780__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$madd$arity$3 = (function (_,a,b){
var self__ = this;
var ___$1 = this;
var dest__18802__auto__ = (new Float32Array((3)));
var G__19705_20099 = (a instanceof thi.ng.geom.core.vector.Vec3);
var G__19706_20100 = (b instanceof thi.ng.geom.core.vector.Vec3);
var G__19707_20101 = ((!(G__19705_20099))?typeof a === 'number':null);
var G__19708_20102 = ((!(G__19706_20100))?typeof b === 'number':null);
var G__19693_20103 = self__.buf;
var G__19694_20104 = ((G__19705_20099)?a.buf:null);
var G__19695_20105 = ((G__19706_20100)?b.buf:null);
var G__19696_20106 = (G__19693_20103[(0)]);
var G__19697_20107 = (G__19693_20103[(1)]);
var G__19698_20108 = (G__19693_20103[(2)]);
var G__19699_20109 = ((G__19705_20099)?(G__19694_20104[(0)]):(cljs.core.truth_(G__19707_20101)?a:cljs.core.nth.call(null,a,(0),1.0)));
var G__19700_20110 = ((G__19705_20099)?(G__19694_20104[(1)]):(cljs.core.truth_(G__19707_20101)?a:cljs.core.nth.call(null,a,(1),1.0)));
var G__19701_20111 = ((G__19705_20099)?(G__19694_20104[(2)]):(cljs.core.truth_(G__19707_20101)?a:cljs.core.nth.call(null,a,(2),1.0)));
var G__19702_20112 = ((G__19706_20100)?(G__19695_20105[(0)]):(cljs.core.truth_(G__19708_20102)?b:cljs.core.nth.call(null,b,(0),0.0)));
var G__19703_20113 = ((G__19706_20100)?(G__19695_20105[(1)]):(cljs.core.truth_(G__19708_20102)?b:cljs.core.nth.call(null,b,(1),0.0)));
var G__19704_20114 = ((G__19706_20100)?(G__19695_20105[(2)]):(cljs.core.truth_(G__19708_20102)?b:cljs.core.nth.call(null,b,(2),0.0)));
(dest__18802__auto__[(0)] = ((G__19696_20106 * G__19699_20109) + G__19702_20112));

(dest__18802__auto__[(1)] = ((G__19697_20107 * G__19700_20110) + G__19703_20113));

(dest__18802__auto__[(2)] = ((G__19698_20108 * G__19701_20111) + G__19704_20114));

return (new thi.ng.geom.core.vector.Vec3(dest__18802__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$addm$arity$3 = (function (_,a,b){
var self__ = this;
var ___$1 = this;
var dest__18802__auto__ = (new Float32Array((3)));
var G__19721_20115 = (a instanceof thi.ng.geom.core.vector.Vec3);
var G__19722_20116 = (b instanceof thi.ng.geom.core.vector.Vec3);
var G__19723_20117 = ((!(G__19721_20115))?typeof a === 'number':null);
var G__19724_20118 = ((!(G__19722_20116))?typeof b === 'number':null);
var G__19709_20119 = self__.buf;
var G__19710_20120 = ((G__19721_20115)?a.buf:null);
var G__19711_20121 = ((G__19722_20116)?b.buf:null);
var G__19712_20122 = (G__19709_20119[(0)]);
var G__19713_20123 = (G__19709_20119[(1)]);
var G__19714_20124 = (G__19709_20119[(2)]);
var G__19715_20125 = ((G__19721_20115)?(G__19710_20120[(0)]):(cljs.core.truth_(G__19723_20117)?a:cljs.core.nth.call(null,a,(0),0.0)));
var G__19716_20126 = ((G__19721_20115)?(G__19710_20120[(1)]):(cljs.core.truth_(G__19723_20117)?a:cljs.core.nth.call(null,a,(1),0.0)));
var G__19717_20127 = ((G__19721_20115)?(G__19710_20120[(2)]):(cljs.core.truth_(G__19723_20117)?a:cljs.core.nth.call(null,a,(2),0.0)));
var G__19718_20128 = ((G__19722_20116)?(G__19711_20121[(0)]):(cljs.core.truth_(G__19724_20118)?b:cljs.core.nth.call(null,b,(0),1.0)));
var G__19719_20129 = ((G__19722_20116)?(G__19711_20121[(1)]):(cljs.core.truth_(G__19724_20118)?b:cljs.core.nth.call(null,b,(1),1.0)));
var G__19720_20130 = ((G__19722_20116)?(G__19711_20121[(2)]):(cljs.core.truth_(G__19724_20118)?b:cljs.core.nth.call(null,b,(2),1.0)));
(dest__18802__auto__[(0)] = ((G__19712_20122 + G__19715_20125) * G__19718_20128));

(dest__18802__auto__[(1)] = ((G__19713_20123 + G__19716_20126) * G__19719_20129));

(dest__18802__auto__[(2)] = ((G__19714_20124 + G__19717_20127) * G__19720_20130));

return (new thi.ng.geom.core.vector.Vec3(dest__18802__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$div$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var dest__18774__auto__ = (new Float32Array((3)));
var G__19725_20131 = self__.buf;
(dest__18774__auto__[(0)] = ((1) / (G__19725_20131[(0)])));

(dest__18774__auto__[(1)] = ((1) / (G__19725_20131[(1)])));

(dest__18774__auto__[(2)] = ((1) / (G__19725_20131[(2)])));

return (new thi.ng.geom.core.vector.Vec3(dest__18774__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$div$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var dest__18791__auto__ = (new Float32Array((3)));
var G__19726_20132 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19727_20133 = v.buf;
(dest__18791__auto__[(0)] = ((G__19726_20132[(0)]) / (G__19727_20133[(0)])));

(dest__18791__auto__[(1)] = ((G__19726_20132[(1)]) / (G__19727_20133[(1)])));

(dest__18791__auto__[(2)] = ((G__19726_20132[(2)]) / (G__19727_20133[(2)])));
} else {
if(typeof v === 'number'){
(dest__18791__auto__[(0)] = ((G__19726_20132[(0)]) / v));

(dest__18791__auto__[(1)] = ((G__19726_20132[(1)]) / v));

(dest__18791__auto__[(2)] = ((G__19726_20132[(2)]) / v));
} else {
(dest__18791__auto__[(0)] = ((G__19726_20132[(0)]) / cljs.core.nth.call(null,v,(0),0.0)));

(dest__18791__auto__[(1)] = ((G__19726_20132[(1)]) / cljs.core.nth.call(null,v,(1),0.0)));

(dest__18791__auto__[(2)] = ((G__19726_20132[(2)]) / cljs.core.nth.call(null,v,(2),0.0)));
}
}

return (new thi.ng.geom.core.vector.Vec3(dest__18791__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$div$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var dest__18802__auto__ = (new Float32Array((3)));
var G__19740_20134 = (v1 instanceof thi.ng.geom.core.vector.Vec3);
var G__19741_20135 = (v2 instanceof thi.ng.geom.core.vector.Vec3);
var G__19742_20136 = ((!(G__19740_20134))?typeof v1 === 'number':null);
var G__19743_20137 = ((!(G__19741_20135))?typeof v2 === 'number':null);
var G__19728_20138 = self__.buf;
var G__19729_20139 = ((G__19740_20134)?v1.buf:null);
var G__19730_20140 = ((G__19741_20135)?v2.buf:null);
var G__19731_20141 = (G__19728_20138[(0)]);
var G__19732_20142 = (G__19728_20138[(1)]);
var G__19733_20143 = (G__19728_20138[(2)]);
var G__19734_20144 = ((G__19740_20134)?(G__19729_20139[(0)]):(cljs.core.truth_(G__19742_20136)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__19735_20145 = ((G__19740_20134)?(G__19729_20139[(1)]):(cljs.core.truth_(G__19742_20136)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__19736_20146 = ((G__19740_20134)?(G__19729_20139[(2)]):(cljs.core.truth_(G__19742_20136)?v1:cljs.core.nth.call(null,v1,(2),0.0)));
var G__19737_20147 = ((G__19741_20135)?(G__19730_20140[(0)]):(cljs.core.truth_(G__19743_20137)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19738_20148 = ((G__19741_20135)?(G__19730_20140[(1)]):(cljs.core.truth_(G__19743_20137)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
var G__19739_20149 = ((G__19741_20135)?(G__19730_20140[(2)]):(cljs.core.truth_(G__19743_20137)?v2:cljs.core.nth.call(null,v2,(2),0.0)));
(dest__18802__auto__[(0)] = ((G__19731_20141 / G__19734_20144) / G__19737_20147));

(dest__18802__auto__[(1)] = ((G__19732_20142 / G__19735_20145) / G__19738_20148));

(dest__18802__auto__[(2)] = ((G__19733_20143 / G__19736_20146) / G__19739_20149));

return (new thi.ng.geom.core.vector.Vec3(dest__18802__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$div$arity$4 = (function (_,x,y,z){
var self__ = this;
var ___$1 = this;
var G__19744 = self__.buf;
var dest__18780__auto__ = (new Float32Array((3)));
(dest__18780__auto__[(0)] = ((G__19744[(0)]) / x));

(dest__18780__auto__[(1)] = ((G__19744[(1)]) / y));

(dest__18780__auto__[(2)] = ((G__19744[(2)]) / z));

return (new thi.ng.geom.core.vector.Vec3(dest__18780__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$_PLUS_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$_PLUS_$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var dest__18791__auto__ = (new Float32Array((3)));
var G__19745_20150 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19746_20151 = v.buf;
(dest__18791__auto__[(0)] = ((G__19745_20150[(0)]) + (G__19746_20151[(0)])));

(dest__18791__auto__[(1)] = ((G__19745_20150[(1)]) + (G__19746_20151[(1)])));

(dest__18791__auto__[(2)] = ((G__19745_20150[(2)]) + (G__19746_20151[(2)])));
} else {
if(typeof v === 'number'){
(dest__18791__auto__[(0)] = ((G__19745_20150[(0)]) + v));

(dest__18791__auto__[(1)] = ((G__19745_20150[(1)]) + v));

(dest__18791__auto__[(2)] = ((G__19745_20150[(2)]) + v));
} else {
(dest__18791__auto__[(0)] = ((G__19745_20150[(0)]) + cljs.core.nth.call(null,v,(0),0.0)));

(dest__18791__auto__[(1)] = ((G__19745_20150[(1)]) + cljs.core.nth.call(null,v,(1),0.0)));

(dest__18791__auto__[(2)] = ((G__19745_20150[(2)]) + cljs.core.nth.call(null,v,(2),0.0)));
}
}

return (new thi.ng.geom.core.vector.Vec3(dest__18791__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$_PLUS_$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var dest__18802__auto__ = (new Float32Array((3)));
var G__19759_20152 = (v1 instanceof thi.ng.geom.core.vector.Vec3);
var G__19760_20153 = (v2 instanceof thi.ng.geom.core.vector.Vec3);
var G__19761_20154 = ((!(G__19759_20152))?typeof v1 === 'number':null);
var G__19762_20155 = ((!(G__19760_20153))?typeof v2 === 'number':null);
var G__19747_20156 = self__.buf;
var G__19748_20157 = ((G__19759_20152)?v1.buf:null);
var G__19749_20158 = ((G__19760_20153)?v2.buf:null);
var G__19750_20159 = (G__19747_20156[(0)]);
var G__19751_20160 = (G__19747_20156[(1)]);
var G__19752_20161 = (G__19747_20156[(2)]);
var G__19753_20162 = ((G__19759_20152)?(G__19748_20157[(0)]):(cljs.core.truth_(G__19761_20154)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__19754_20163 = ((G__19759_20152)?(G__19748_20157[(1)]):(cljs.core.truth_(G__19761_20154)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__19755_20164 = ((G__19759_20152)?(G__19748_20157[(2)]):(cljs.core.truth_(G__19761_20154)?v1:cljs.core.nth.call(null,v1,(2),0.0)));
var G__19756_20165 = ((G__19760_20153)?(G__19749_20158[(0)]):(cljs.core.truth_(G__19762_20155)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19757_20166 = ((G__19760_20153)?(G__19749_20158[(1)]):(cljs.core.truth_(G__19762_20155)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
var G__19758_20167 = ((G__19760_20153)?(G__19749_20158[(2)]):(cljs.core.truth_(G__19762_20155)?v2:cljs.core.nth.call(null,v2,(2),0.0)));
(dest__18802__auto__[(0)] = ((G__19750_20159 + G__19753_20162) + G__19756_20165));

(dest__18802__auto__[(1)] = ((G__19751_20160 + G__19754_20163) + G__19757_20166));

(dest__18802__auto__[(2)] = ((G__19752_20161 + G__19755_20164) + G__19758_20167));

return (new thi.ng.geom.core.vector.Vec3(dest__18802__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$_PLUS_$arity$4 = (function (_,x,y,z){
var self__ = this;
var ___$1 = this;
var G__19763 = self__.buf;
var dest__18780__auto__ = (new Float32Array((3)));
(dest__18780__auto__[(0)] = ((G__19763[(0)]) + x));

(dest__18780__auto__[(1)] = ((G__19763[(1)]) + y));

(dest__18780__auto__[(2)] = ((G__19763[(2)]) + z));

return (new thi.ng.geom.core.vector.Vec3(dest__18780__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$abs$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var dest__18774__auto__ = (new Float32Array((3)));
var G__19764_20168 = self__.buf;
(dest__18774__auto__[(0)] = thi.ng.math.core.abs.call(null,(G__19764_20168[(0)])));

(dest__18774__auto__[(1)] = thi.ng.math.core.abs.call(null,(G__19764_20168[(1)])));

(dest__18774__auto__[(2)] = thi.ng.math.core.abs.call(null,(G__19764_20168[(2)])));

return (new thi.ng.geom.core.vector.Vec3(dest__18774__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMathOps$subm$arity$3 = (function (_,a,b){
var self__ = this;
var ___$1 = this;
var dest__18802__auto__ = (new Float32Array((3)));
var G__19777_20169 = (a instanceof thi.ng.geom.core.vector.Vec3);
var G__19778_20170 = (b instanceof thi.ng.geom.core.vector.Vec3);
var G__19779_20171 = ((!(G__19777_20169))?typeof a === 'number':null);
var G__19780_20172 = ((!(G__19778_20170))?typeof b === 'number':null);
var G__19765_20173 = self__.buf;
var G__19766_20174 = ((G__19777_20169)?a.buf:null);
var G__19767_20175 = ((G__19778_20170)?b.buf:null);
var G__19768_20176 = (G__19765_20173[(0)]);
var G__19769_20177 = (G__19765_20173[(1)]);
var G__19770_20178 = (G__19765_20173[(2)]);
var G__19771_20179 = ((G__19777_20169)?(G__19766_20174[(0)]):(cljs.core.truth_(G__19779_20171)?a:cljs.core.nth.call(null,a,(0),0.0)));
var G__19772_20180 = ((G__19777_20169)?(G__19766_20174[(1)]):(cljs.core.truth_(G__19779_20171)?a:cljs.core.nth.call(null,a,(1),0.0)));
var G__19773_20181 = ((G__19777_20169)?(G__19766_20174[(2)]):(cljs.core.truth_(G__19779_20171)?a:cljs.core.nth.call(null,a,(2),0.0)));
var G__19774_20182 = ((G__19778_20170)?(G__19767_20175[(0)]):(cljs.core.truth_(G__19780_20172)?b:cljs.core.nth.call(null,b,(0),1.0)));
var G__19775_20183 = ((G__19778_20170)?(G__19767_20175[(1)]):(cljs.core.truth_(G__19780_20172)?b:cljs.core.nth.call(null,b,(1),1.0)));
var G__19776_20184 = ((G__19778_20170)?(G__19767_20175[(2)]):(cljs.core.truth_(G__19780_20172)?b:cljs.core.nth.call(null,b,(2),1.0)));
(dest__18802__auto__[(0)] = ((G__19768_20176 - G__19771_20179) * G__19774_20182));

(dest__18802__auto__[(1)] = ((G__19769_20177 - G__19772_20180) * G__19775_20183));

(dest__18802__auto__[(2)] = ((G__19770_20178 - G__19773_20181) * G__19776_20184));

return (new thi.ng.geom.core.vector.Vec3(dest__18802__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$ISeq$_first$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return (self__.buf[(0)]);
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$ISeq$_rest$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.cons.call(null,(self__.buf[(1)]),cljs.core.cons.call(null,(self__.buf[(2)]),null));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PTranslate$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PTranslate$translate$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var dest__18791__auto__ = (new Float32Array((3)));
var G__19781_20185 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19782_20186 = v.buf;
(dest__18791__auto__[(0)] = ((G__19781_20185[(0)]) + (G__19782_20186[(0)])));

(dest__18791__auto__[(1)] = ((G__19781_20185[(1)]) + (G__19782_20186[(1)])));

(dest__18791__auto__[(2)] = ((G__19781_20185[(2)]) + (G__19782_20186[(2)])));
} else {
if(typeof v === 'number'){
(dest__18791__auto__[(0)] = ((G__19781_20185[(0)]) + v));

(dest__18791__auto__[(1)] = ((G__19781_20185[(1)]) + v));

(dest__18791__auto__[(2)] = ((G__19781_20185[(2)]) + v));
} else {
(dest__18791__auto__[(0)] = ((G__19781_20185[(0)]) + cljs.core.nth.call(null,v,(0),0.0)));

(dest__18791__auto__[(1)] = ((G__19781_20185[(1)]) + cljs.core.nth.call(null,v,(1),0.0)));

(dest__18791__auto__[(2)] = ((G__19781_20185[(2)]) + cljs.core.nth.call(null,v,(2),0.0)));
}
}

return (new thi.ng.geom.core.vector.Vec3(dest__18791__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PTranslate$translate$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var dest__18802__auto__ = (new Float32Array((3)));
var G__19795_20187 = (v1 instanceof thi.ng.geom.core.vector.Vec3);
var G__19796_20188 = (v2 instanceof thi.ng.geom.core.vector.Vec3);
var G__19797_20189 = ((!(G__19795_20187))?typeof v1 === 'number':null);
var G__19798_20190 = ((!(G__19796_20188))?typeof v2 === 'number':null);
var G__19783_20191 = self__.buf;
var G__19784_20192 = ((G__19795_20187)?v1.buf:null);
var G__19785_20193 = ((G__19796_20188)?v2.buf:null);
var G__19786_20194 = (G__19783_20191[(0)]);
var G__19787_20195 = (G__19783_20191[(1)]);
var G__19788_20196 = (G__19783_20191[(2)]);
var G__19789_20197 = ((G__19795_20187)?(G__19784_20192[(0)]):(cljs.core.truth_(G__19797_20189)?v1:cljs.core.nth.call(null,v1,(0),0.0)));
var G__19790_20198 = ((G__19795_20187)?(G__19784_20192[(1)]):(cljs.core.truth_(G__19797_20189)?v1:cljs.core.nth.call(null,v1,(1),0.0)));
var G__19791_20199 = ((G__19795_20187)?(G__19784_20192[(2)]):(cljs.core.truth_(G__19797_20189)?v1:cljs.core.nth.call(null,v1,(2),0.0)));
var G__19792_20200 = ((G__19796_20188)?(G__19785_20193[(0)]):(cljs.core.truth_(G__19798_20190)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19793_20201 = ((G__19796_20188)?(G__19785_20193[(1)]):(cljs.core.truth_(G__19798_20190)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
var G__19794_20202 = ((G__19796_20188)?(G__19785_20193[(2)]):(cljs.core.truth_(G__19798_20190)?v2:cljs.core.nth.call(null,v2,(2),0.0)));
(dest__18802__auto__[(0)] = ((G__19786_20194 + G__19789_20197) + G__19792_20200));

(dest__18802__auto__[(1)] = ((G__19787_20195 + G__19790_20198) + G__19793_20201));

(dest__18802__auto__[(2)] = ((G__19788_20196 + G__19791_20199) + G__19794_20202));

return (new thi.ng.geom.core.vector.Vec3(dest__18802__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PTranslate$translate$arity$4 = (function (_,x,y,z){
var self__ = this;
var ___$1 = this;
var G__19799 = self__.buf;
var dest__18780__auto__ = (new Float32Array((3)));
(dest__18780__auto__[(0)] = ((G__19799[(0)]) + x));

(dest__18780__auto__[(1)] = ((G__19799[(1)]) + y));

(dest__18780__auto__[(2)] = ((G__19799[(2)]) + z));

return (new thi.ng.geom.core.vector.Vec3(dest__18780__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IAssociative$_contains_key_QMARK_$arity$2 = (function (_,k){
var self__ = this;
var ___$1 = this;
if(typeof k === 'number'){
return ((k >= (0))) && ((k <= (2)));
} else {
if(cljs.core.truth_(thi.ng.geom.core.vector.swizzle3_fns.call(null,k))){
return true;
} else {
return false;
}
}
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (_,k,v){
var self__ = this;
var ___$1 = this;
if(typeof k === 'number'){
if(((k >= (0))) && ((k <= (2)))){
var b = (new Float32Array(self__.buf));
(b[k] = v);

return (new thi.ng.geom.core.vector.Vec3(b,null,self__._meta));
} else {
if((k === (3))){
return cljs.core.conj.call(null,___$1,v);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
}
} else {
if((k instanceof cljs.core.Keyword)){
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"w","w",354169001),k)){
return cljs.core.conj.call(null,___$1,v);
} else {
return (new thi.ng.geom.core.vector.Vec3(thi.ng.geom.core.vector.swizzle_assoc_STAR_.call(null,self__.buf,(new Float32Array(self__.buf)),new cljs.core.PersistentArrayMap(null, 3, ["x",(0),"y",(1),"z",(2)], null),k,v),null,self__._meta));
}
} else {
return null;
}
}
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return ___$1;
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PScale$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PScale$scale$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var dest__18791__auto__ = (new Float32Array((3)));
var G__19800_20203 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19801_20204 = v.buf;
(dest__18791__auto__[(0)] = ((G__19800_20203[(0)]) * (G__19801_20204[(0)])));

(dest__18791__auto__[(1)] = ((G__19800_20203[(1)]) * (G__19801_20204[(1)])));

(dest__18791__auto__[(2)] = ((G__19800_20203[(2)]) * (G__19801_20204[(2)])));
} else {
if(typeof v === 'number'){
(dest__18791__auto__[(0)] = ((G__19800_20203[(0)]) * v));

(dest__18791__auto__[(1)] = ((G__19800_20203[(1)]) * v));

(dest__18791__auto__[(2)] = ((G__19800_20203[(2)]) * v));
} else {
(dest__18791__auto__[(0)] = ((G__19800_20203[(0)]) * cljs.core.nth.call(null,v,(0),0.0)));

(dest__18791__auto__[(1)] = ((G__19800_20203[(1)]) * cljs.core.nth.call(null,v,(1),0.0)));

(dest__18791__auto__[(2)] = ((G__19800_20203[(2)]) * cljs.core.nth.call(null,v,(2),0.0)));
}
}

return (new thi.ng.geom.core.vector.Vec3(dest__18791__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PScale$scale$arity$3 = (function (_,v1,v2){
var self__ = this;
var ___$1 = this;
var dest__18802__auto__ = (new Float32Array((3)));
var G__19814_20205 = (v1 instanceof thi.ng.geom.core.vector.Vec3);
var G__19815_20206 = (v2 instanceof thi.ng.geom.core.vector.Vec3);
var G__19816_20207 = ((!(G__19814_20205))?typeof v1 === 'number':null);
var G__19817_20208 = ((!(G__19815_20206))?typeof v2 === 'number':null);
var G__19802_20209 = self__.buf;
var G__19803_20210 = ((G__19814_20205)?v1.buf:null);
var G__19804_20211 = ((G__19815_20206)?v2.buf:null);
var G__19805_20212 = (G__19802_20209[(0)]);
var G__19806_20213 = (G__19802_20209[(1)]);
var G__19807_20214 = (G__19802_20209[(2)]);
var G__19808_20215 = ((G__19814_20205)?(G__19803_20210[(0)]):(cljs.core.truth_(G__19816_20207)?v1:cljs.core.nth.call(null,v1,(0),1.0)));
var G__19809_20216 = ((G__19814_20205)?(G__19803_20210[(1)]):(cljs.core.truth_(G__19816_20207)?v1:cljs.core.nth.call(null,v1,(1),1.0)));
var G__19810_20217 = ((G__19814_20205)?(G__19803_20210[(2)]):(cljs.core.truth_(G__19816_20207)?v1:cljs.core.nth.call(null,v1,(2),1.0)));
var G__19811_20218 = ((G__19815_20206)?(G__19804_20211[(0)]):(cljs.core.truth_(G__19817_20208)?v2:cljs.core.nth.call(null,v2,(0),1.0)));
var G__19812_20219 = ((G__19815_20206)?(G__19804_20211[(1)]):(cljs.core.truth_(G__19817_20208)?v2:cljs.core.nth.call(null,v2,(1),1.0)));
var G__19813_20220 = ((G__19815_20206)?(G__19804_20211[(2)]):(cljs.core.truth_(G__19817_20208)?v2:cljs.core.nth.call(null,v2,(2),1.0)));
(dest__18802__auto__[(0)] = ((G__19805_20212 * G__19808_20215) * G__19811_20218));

(dest__18802__auto__[(1)] = ((G__19806_20213 * G__19809_20216) * G__19812_20219));

(dest__18802__auto__[(2)] = ((G__19807_20214 * G__19810_20217) * G__19813_20220));

return (new thi.ng.geom.core.vector.Vec3(dest__18802__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PScale$scale$arity$4 = (function (_,x,y,z){
var self__ = this;
var ___$1 = this;
var G__19818 = self__.buf;
var dest__18780__auto__ = (new Float32Array((3)));
(dest__18780__auto__[(0)] = ((G__19818[(0)]) * x));

(dest__18780__auto__[(1)] = ((G__19818[(1)]) * y));

(dest__18780__auto__[(2)] = ((G__19818[(2)]) * z));

return (new thi.ng.geom.core.vector.Vec3(dest__18780__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_,m){
var self__ = this;
var ___$1 = this;
return (new thi.ng.geom.core.vector.Vec3((new Float32Array(self__.buf)),self__._hash,m));
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$ICollection$_conj$arity$2 = (function (_,x){
var self__ = this;
var ___$1 = this;
return cljs.core.with_meta.call(null,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [(self__.buf[(0)]),(self__.buf[(1)]),(self__.buf[(2)]),x], null),self__._meta);
});

thi.ng.geom.core.vector.Vec3.prototype.call = (function() {
var G__20221 = null;
var G__20221__2 = (function (self__,k){
var self__ = this;
var self____$1 = this;
var _ = self____$1;
if((k instanceof cljs.core.Keyword)){
var temp__4423__auto__ = thi.ng.geom.core.vector.swizzle3_fns.call(null,k);
if(cljs.core.truth_(temp__4423__auto__)){
var f = temp__4423__auto__;
return f.call(null,_);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
} else {
if(((k >= (0))) && ((k <= (2)))){
return (self__.buf[k]);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
}
});
var G__20221__3 = (function (self__,k,nf){
var self__ = this;
var self____$1 = this;
var _ = self____$1;
if((k instanceof cljs.core.Keyword)){
var temp__4423__auto__ = thi.ng.geom.core.vector.swizzle3_fns.call(null,k);
if(cljs.core.truth_(temp__4423__auto__)){
var f = temp__4423__auto__;
return f.call(null,_);
} else {
return nf;
}
} else {
if(((k >= (0))) && ((k <= (2)))){
return (self__.buf[k]);
} else {
return nf;
}
}
});
G__20221 = function(self__,k,nf){
switch(arguments.length){
case 2:
return G__20221__2.call(this,self__,k);
case 3:
return G__20221__3.call(this,self__,k,nf);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__20221.cljs$core$IFn$_invoke$arity$2 = G__20221__2;
G__20221.cljs$core$IFn$_invoke$arity$3 = G__20221__3;
return G__20221;
})()
;

thi.ng.geom.core.vector.Vec3.prototype.apply = (function (self__,args19447){
var self__ = this;
var self____$1 = this;
return self____$1.call.apply(self____$1,[self____$1].concat(cljs.core.aclone.call(null,args19447)));
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IFn$_invoke$arity$1 = (function (k){
var self__ = this;
var _ = this;
if((k instanceof cljs.core.Keyword)){
var temp__4423__auto__ = thi.ng.geom.core.vector.swizzle3_fns.call(null,k);
if(cljs.core.truth_(temp__4423__auto__)){
var f = temp__4423__auto__;
return f.call(null,_);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
} else {
if(((k >= (0))) && ((k <= (2)))){
return (self__.buf[k]);
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
}
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IFn$_invoke$arity$2 = (function (k,nf){
var self__ = this;
var _ = this;
if((k instanceof cljs.core.Keyword)){
var temp__4423__auto__ = thi.ng.geom.core.vector.swizzle3_fns.call(null,k);
if(cljs.core.truth_(temp__4423__auto__)){
var f = temp__4423__auto__;
return f.call(null,_);
} else {
return nf;
}
} else {
if(((k >= (0))) && ((k <= (2)))){
return (self__.buf[k]);
} else {
return nf;
}
}
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMinMax$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMinMax$min$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var dest__18791__auto__ = (new Float32Array((3)));
var G__19819_20222 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19820_20223 = v.buf;
(dest__18791__auto__[(0)] = (function (){var a__18352__auto__ = (G__19819_20222[(0)]);
var b__18353__auto__ = (G__19820_20223[(0)]);
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());

(dest__18791__auto__[(1)] = (function (){var a__18352__auto__ = (G__19819_20222[(1)]);
var b__18353__auto__ = (G__19820_20223[(1)]);
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());

(dest__18791__auto__[(2)] = (function (){var a__18352__auto__ = (G__19819_20222[(2)]);
var b__18353__auto__ = (G__19820_20223[(2)]);
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());
} else {
if(typeof v === 'number'){
(dest__18791__auto__[(0)] = (function (){var a__18352__auto__ = (G__19819_20222[(0)]);
var b__18353__auto__ = v;
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());

(dest__18791__auto__[(1)] = (function (){var a__18352__auto__ = (G__19819_20222[(1)]);
var b__18353__auto__ = v;
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());

(dest__18791__auto__[(2)] = (function (){var a__18352__auto__ = (G__19819_20222[(2)]);
var b__18353__auto__ = v;
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());
} else {
(dest__18791__auto__[(0)] = (function (){var a__18352__auto__ = (G__19819_20222[(0)]);
var b__18353__auto__ = cljs.core.nth.call(null,v,(0),0.0);
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());

(dest__18791__auto__[(1)] = (function (){var a__18352__auto__ = (G__19819_20222[(1)]);
var b__18353__auto__ = cljs.core.nth.call(null,v,(1),0.0);
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());

(dest__18791__auto__[(2)] = (function (){var a__18352__auto__ = (G__19819_20222[(2)]);
var b__18353__auto__ = cljs.core.nth.call(null,v,(2),0.0);
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());
}
}

return (new thi.ng.geom.core.vector.Vec3(dest__18791__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMinMax$min$arity$3 = (function (_,v,v2){
var self__ = this;
var ___$1 = this;
var dest__18802__auto__ = (new Float32Array((3)));
var G__19833_20224 = (v instanceof thi.ng.geom.core.vector.Vec3);
var G__19834_20225 = (v2 instanceof thi.ng.geom.core.vector.Vec3);
var G__19835_20226 = ((!(G__19833_20224))?typeof v === 'number':null);
var G__19836_20227 = ((!(G__19834_20225))?typeof v2 === 'number':null);
var G__19821_20228 = self__.buf;
var G__19822_20229 = ((G__19833_20224)?v.buf:null);
var G__19823_20230 = ((G__19834_20225)?v2.buf:null);
var G__19824_20231 = (G__19821_20228[(0)]);
var G__19825_20232 = (G__19821_20228[(1)]);
var G__19826_20233 = (G__19821_20228[(2)]);
var G__19827_20234 = ((G__19833_20224)?(G__19822_20229[(0)]):(cljs.core.truth_(G__19835_20226)?v:cljs.core.nth.call(null,v,(0),0.0)));
var G__19828_20235 = ((G__19833_20224)?(G__19822_20229[(1)]):(cljs.core.truth_(G__19835_20226)?v:cljs.core.nth.call(null,v,(1),0.0)));
var G__19829_20236 = ((G__19833_20224)?(G__19822_20229[(2)]):(cljs.core.truth_(G__19835_20226)?v:cljs.core.nth.call(null,v,(2),0.0)));
var G__19830_20237 = ((G__19834_20225)?(G__19823_20230[(0)]):(cljs.core.truth_(G__19836_20227)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19831_20238 = ((G__19834_20225)?(G__19823_20230[(1)]):(cljs.core.truth_(G__19836_20227)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
var G__19832_20239 = ((G__19834_20225)?(G__19823_20230[(2)]):(cljs.core.truth_(G__19836_20227)?v2:cljs.core.nth.call(null,v2,(2),0.0)));
(dest__18802__auto__[(0)] = (function (){var a__18352__auto__ = (function (){var a__18352__auto__ = G__19824_20231;
var b__18353__auto__ = G__19827_20234;
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})();
var b__18353__auto__ = G__19830_20237;
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());

(dest__18802__auto__[(1)] = (function (){var a__18352__auto__ = (function (){var a__18352__auto__ = G__19825_20232;
var b__18353__auto__ = G__19828_20235;
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})();
var b__18353__auto__ = G__19831_20238;
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());

(dest__18802__auto__[(2)] = (function (){var a__18352__auto__ = (function (){var a__18352__auto__ = G__19826_20233;
var b__18353__auto__ = G__19829_20236;
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})();
var b__18353__auto__ = G__19832_20239;
if((a__18352__auto__ <= b__18353__auto__)){
return a__18352__auto__;
} else {
return b__18353__auto__;
}
})());

return (new thi.ng.geom.core.vector.Vec3(dest__18802__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMinMax$max$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var dest__18791__auto__ = (new Float32Array((3)));
var G__19837_20240 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19838_20241 = v.buf;
(dest__18791__auto__[(0)] = (function (){var a__18359__auto__ = (G__19837_20240[(0)]);
var b__18360__auto__ = (G__19838_20241[(0)]);
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());

(dest__18791__auto__[(1)] = (function (){var a__18359__auto__ = (G__19837_20240[(1)]);
var b__18360__auto__ = (G__19838_20241[(1)]);
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());

(dest__18791__auto__[(2)] = (function (){var a__18359__auto__ = (G__19837_20240[(2)]);
var b__18360__auto__ = (G__19838_20241[(2)]);
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());
} else {
if(typeof v === 'number'){
(dest__18791__auto__[(0)] = (function (){var a__18359__auto__ = (G__19837_20240[(0)]);
var b__18360__auto__ = v;
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());

(dest__18791__auto__[(1)] = (function (){var a__18359__auto__ = (G__19837_20240[(1)]);
var b__18360__auto__ = v;
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());

(dest__18791__auto__[(2)] = (function (){var a__18359__auto__ = (G__19837_20240[(2)]);
var b__18360__auto__ = v;
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());
} else {
(dest__18791__auto__[(0)] = (function (){var a__18359__auto__ = (G__19837_20240[(0)]);
var b__18360__auto__ = cljs.core.nth.call(null,v,(0),0.0);
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());

(dest__18791__auto__[(1)] = (function (){var a__18359__auto__ = (G__19837_20240[(1)]);
var b__18360__auto__ = cljs.core.nth.call(null,v,(1),0.0);
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());

(dest__18791__auto__[(2)] = (function (){var a__18359__auto__ = (G__19837_20240[(2)]);
var b__18360__auto__ = cljs.core.nth.call(null,v,(2),0.0);
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());
}
}

return (new thi.ng.geom.core.vector.Vec3(dest__18791__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMinMax$max$arity$3 = (function (_,v,v2){
var self__ = this;
var ___$1 = this;
var dest__18802__auto__ = (new Float32Array((3)));
var G__19851_20242 = (v instanceof thi.ng.geom.core.vector.Vec3);
var G__19852_20243 = (v2 instanceof thi.ng.geom.core.vector.Vec3);
var G__19853_20244 = ((!(G__19851_20242))?typeof v === 'number':null);
var G__19854_20245 = ((!(G__19852_20243))?typeof v2 === 'number':null);
var G__19839_20246 = self__.buf;
var G__19840_20247 = ((G__19851_20242)?v.buf:null);
var G__19841_20248 = ((G__19852_20243)?v2.buf:null);
var G__19842_20249 = (G__19839_20246[(0)]);
var G__19843_20250 = (G__19839_20246[(1)]);
var G__19844_20251 = (G__19839_20246[(2)]);
var G__19845_20252 = ((G__19851_20242)?(G__19840_20247[(0)]):(cljs.core.truth_(G__19853_20244)?v:cljs.core.nth.call(null,v,(0),0.0)));
var G__19846_20253 = ((G__19851_20242)?(G__19840_20247[(1)]):(cljs.core.truth_(G__19853_20244)?v:cljs.core.nth.call(null,v,(1),0.0)));
var G__19847_20254 = ((G__19851_20242)?(G__19840_20247[(2)]):(cljs.core.truth_(G__19853_20244)?v:cljs.core.nth.call(null,v,(2),0.0)));
var G__19848_20255 = ((G__19852_20243)?(G__19841_20248[(0)]):(cljs.core.truth_(G__19854_20245)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19849_20256 = ((G__19852_20243)?(G__19841_20248[(1)]):(cljs.core.truth_(G__19854_20245)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
var G__19850_20257 = ((G__19852_20243)?(G__19841_20248[(2)]):(cljs.core.truth_(G__19854_20245)?v2:cljs.core.nth.call(null,v2,(2),0.0)));
(dest__18802__auto__[(0)] = (function (){var a__18359__auto__ = (function (){var a__18359__auto__ = G__19842_20249;
var b__18360__auto__ = G__19845_20252;
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})();
var b__18360__auto__ = G__19848_20255;
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());

(dest__18802__auto__[(1)] = (function (){var a__18359__auto__ = (function (){var a__18359__auto__ = G__19843_20250;
var b__18360__auto__ = G__19846_20253;
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})();
var b__18360__auto__ = G__19849_20256;
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());

(dest__18802__auto__[(2)] = (function (){var a__18359__auto__ = (function (){var a__18359__auto__ = G__19844_20251;
var b__18360__auto__ = G__19847_20254;
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})();
var b__18360__auto__ = G__19850_20257;
if((a__18359__auto__ >= b__18360__auto__)){
return a__18359__auto__;
} else {
return b__18360__auto__;
}
})());

return (new thi.ng.geom.core.vector.Vec3(dest__18802__auto__,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.cljs$core$IComparable$_compare$arity$2 = (function (_,o){
var self__ = this;
var ___$1 = this;
if((o instanceof thi.ng.geom.core.vector.Vec3)){
var b_SINGLEQUOTE_ = o.buf;
var c = cljs.core.compare.call(null,(self__.buf[(0)]),(b_SINGLEQUOTE_[(0)]));
if(((0) === c)){
var c__$1 = cljs.core.compare.call(null,(self__.buf[(1)]),(b_SINGLEQUOTE_[(1)]));
if(((0) === c__$1)){
return cljs.core.compare.call(null,(self__.buf[(2)]),(b_SINGLEQUOTE_[(2)]));
} else {
return c__$1;
}
} else {
return c;
}
} else {
var c = cljs.core.count.call(null,o);
if(((3) === c)){
return (- cljs.core.compare.call(null,o,___$1));
} else {
return ((3) - c);
}
}
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PVectorReduce$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PVectorReduce$reduce_vector$arity$3 = (function (_,f,xs){
var self__ = this;
var ___$1 = this;
var buf_SINGLEQUOTE_ = (new Float32Array(self__.buf));
return (new thi.ng.geom.core.vector.Vec3(thi.ng.geom.core.vector.vec3_reduce_STAR_.call(null,f,buf_SINGLEQUOTE_,xs),null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PVectorReduce$reduce_vector$arity$4 = (function (_,f,f2,xs){
var self__ = this;
var ___$1 = this;
var buf_SINGLEQUOTE_ = (new Float32Array(self__.buf));
thi.ng.geom.core.vector.vec3_reduce_STAR_.call(null,f,buf_SINGLEQUOTE_,xs);

(buf_SINGLEQUOTE_[(0)] = f2.call(null,(buf_SINGLEQUOTE_[(0)]),(0)));

(buf_SINGLEQUOTE_[(1)] = f2.call(null,(buf_SINGLEQUOTE_[(1)]),(1)));

(buf_SINGLEQUOTE_[(2)] = f2.call(null,(buf_SINGLEQUOTE_[(2)]),(2)));

return (new thi.ng.geom.core.vector.Vec3(buf_SINGLEQUOTE_,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$math$core$PDeltaEquals$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$math$core$PDeltaEquals$delta_EQ_$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
return thi.ng.math.core.delta_EQ_.call(null,___$1,v,thi.ng.math.core._STAR_eps_STAR_);
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$math$core$PDeltaEquals$delta_EQ_$arity$3 = (function (_,v,eps){
var self__ = this;
var ___$1 = this;
if(cljs.core.sequential_QMARK_.call(null,v)){
if(((3) === cljs.core.count.call(null,v))){
var G__19855 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19856 = v.buf;
if(cljs.core.truth_(thi.ng.math.core.delta_EQ_.call(null,(G__19855[(0)]),(G__19856[(0)]),eps))){
if(cljs.core.truth_(thi.ng.math.core.delta_EQ_.call(null,(G__19855[(1)]),(G__19856[(1)]),eps))){
return thi.ng.math.core.delta_EQ_.call(null,(G__19855[(2)]),(G__19856[(2)]),eps);
} else {
return null;
}
} else {
return null;
}
} else {
if(cljs.core.truth_(thi.ng.math.core.delta_EQ_.call(null,(G__19855[(0)]),cljs.core.nth.call(null,v,(0),0.0),eps))){
if(cljs.core.truth_(thi.ng.math.core.delta_EQ_.call(null,(G__19855[(1)]),cljs.core.nth.call(null,v,(1),0.0),eps))){
return thi.ng.math.core.delta_EQ_.call(null,(G__19855[(2)]),cljs.core.nth.call(null,v,(2),0.0),eps);
} else {
return null;
}
} else {
return null;
}
}
} else {
return null;
}
} else {
return null;
}
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMagnitude$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMagnitude$mag$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var G__19857 = self__.buf;
var G__19858 = (G__19857[(0)]);
var G__19859 = (G__19857[(1)]);
var G__19860 = (G__19857[(2)]);
return Math.sqrt((((G__19858 * G__19858) + (G__19859 * G__19859)) + (G__19860 * G__19860)));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PMagnitude$mag_squared$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var G__19861 = self__.buf;
var G__19862 = (G__19861[(0)]);
var G__19863 = (G__19861[(1)]);
var G__19864 = (G__19861[(2)]);
return (((G__19862 * G__19862) + (G__19863 * G__19863)) + (G__19864 * G__19864));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PInterpolate$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PInterpolate$mix$arity$2 = (function (_,v){
var self__ = this;
var ___$1 = this;
var b = (new Float32Array((3)));
var G__19865_20258 = self__.buf;
if((v instanceof thi.ng.geom.core.vector.Vec3)){
var G__19866_20259 = v.buf;
(b[(0)] = (((G__19865_20258[(0)]) + (G__19866_20259[(0)])) * 0.5));

(b[(1)] = (((G__19865_20258[(1)]) + (G__19866_20259[(1)])) * 0.5));

(b[(2)] = (((G__19865_20258[(2)]) + (G__19866_20259[(2)])) * 0.5));
} else {
if(typeof v === 'number'){
(b[(0)] = (((G__19865_20258[(0)]) + v) * 0.5));

(b[(1)] = (((G__19865_20258[(1)]) + v) * 0.5));

(b[(2)] = (((G__19865_20258[(2)]) + v) * 0.5));
} else {
(b[(0)] = (((G__19865_20258[(0)]) + cljs.core.nth.call(null,v,(0),0.0)) * 0.5));

(b[(1)] = (((G__19865_20258[(1)]) + cljs.core.nth.call(null,v,(1),0.0)) * 0.5));

(b[(2)] = (((G__19865_20258[(2)]) + cljs.core.nth.call(null,v,(2),0.0)) * 0.5));
}
}

return (new thi.ng.geom.core.vector.Vec3(b,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PInterpolate$mix$arity$3 = (function (_,v,v2){
var self__ = this;
var ___$1 = this;
var b = (new Float32Array((3)));
var G__19879_20260 = (v instanceof thi.ng.geom.core.vector.Vec3);
var G__19880_20261 = (v2 instanceof thi.ng.geom.core.vector.Vec3);
var G__19881_20262 = ((!(G__19879_20260))?typeof v === 'number':null);
var G__19882_20263 = ((!(G__19880_20261))?typeof v2 === 'number':null);
var G__19867_20264 = self__.buf;
var G__19868_20265 = ((G__19879_20260)?v.buf:null);
var G__19869_20266 = ((G__19880_20261)?v2.buf:null);
var G__19870_20267 = (G__19867_20264[(0)]);
var G__19871_20268 = (G__19867_20264[(1)]);
var G__19872_20269 = (G__19867_20264[(2)]);
var G__19873_20270 = ((G__19879_20260)?(G__19868_20265[(0)]):(cljs.core.truth_(G__19881_20262)?v:cljs.core.nth.call(null,v,(0),0.0)));
var G__19874_20271 = ((G__19879_20260)?(G__19868_20265[(1)]):(cljs.core.truth_(G__19881_20262)?v:cljs.core.nth.call(null,v,(1),0.0)));
var G__19875_20272 = ((G__19879_20260)?(G__19868_20265[(2)]):(cljs.core.truth_(G__19881_20262)?v:cljs.core.nth.call(null,v,(2),0.0)));
var G__19876_20273 = ((G__19880_20261)?(G__19869_20266[(0)]):(cljs.core.truth_(G__19882_20263)?v2:cljs.core.nth.call(null,v2,(0),0.0)));
var G__19877_20274 = ((G__19880_20261)?(G__19869_20266[(1)]):(cljs.core.truth_(G__19882_20263)?v2:cljs.core.nth.call(null,v2,(1),0.0)));
var G__19878_20275 = ((G__19880_20261)?(G__19869_20266[(2)]):(cljs.core.truth_(G__19882_20263)?v2:cljs.core.nth.call(null,v2,(2),0.0)));
(b[(0)] = (((G__19873_20270 - G__19870_20267) * G__19876_20273) + G__19870_20267));

(b[(1)] = (((G__19874_20271 - G__19871_20268) * G__19877_20274) + G__19871_20268));

(b[(2)] = (((G__19875_20272 - G__19872_20269) * G__19878_20275) + G__19872_20269));

return (new thi.ng.geom.core.vector.Vec3(b,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PInterpolate$mix$arity$6 = (function (_,b,c,d,u,v){
var self__ = this;
var ___$1 = this;
var b_SINGLEQUOTE_ = (new Float32Array((3)));
var dv_QMARK_ = (d instanceof thi.ng.geom.core.vector.Vec3);
var dn_QMARK_ = typeof d === 'number';
var dv = ((dv_QMARK_)?d.buf:null);
var dx = ((dv_QMARK_)?(dv[(0)]):((dn_QMARK_)?d:cljs.core.nth.call(null,d,(0),0.0)));
var dy = ((dv_QMARK_)?(dv[(1)]):((dn_QMARK_)?d:cljs.core.nth.call(null,d,(1),0.0)));
var dz = ((dv_QMARK_)?(dv[(2)]):((dn_QMARK_)?d:cljs.core.nth.call(null,d,(2),0.0)));
var G__19895_20276 = (b instanceof thi.ng.geom.core.vector.Vec3);
var G__19896_20277 = (c instanceof thi.ng.geom.core.vector.Vec3);
var G__19897_20278 = ((!(G__19895_20276))?typeof b === 'number':null);
var G__19898_20279 = ((!(G__19896_20277))?typeof c === 'number':null);
var G__19883_20280 = self__.buf;
var G__19884_20281 = ((G__19895_20276)?b.buf:null);
var G__19885_20282 = ((G__19896_20277)?c.buf:null);
var G__19886_20283 = (G__19883_20280[(0)]);
var G__19887_20284 = (G__19883_20280[(1)]);
var G__19888_20285 = (G__19883_20280[(2)]);
var G__19889_20286 = ((G__19895_20276)?(G__19884_20281[(0)]):(cljs.core.truth_(G__19897_20278)?b:cljs.core.nth.call(null,b,(0),0.0)));
var G__19890_20287 = ((G__19895_20276)?(G__19884_20281[(1)]):(cljs.core.truth_(G__19897_20278)?b:cljs.core.nth.call(null,b,(1),0.0)));
var G__19891_20288 = ((G__19895_20276)?(G__19884_20281[(2)]):(cljs.core.truth_(G__19897_20278)?b:cljs.core.nth.call(null,b,(2),0.0)));
var G__19892_20289 = ((G__19896_20277)?(G__19885_20282[(0)]):(cljs.core.truth_(G__19898_20279)?c:cljs.core.nth.call(null,c,(0),0.0)));
var G__19893_20290 = ((G__19896_20277)?(G__19885_20282[(1)]):(cljs.core.truth_(G__19898_20279)?c:cljs.core.nth.call(null,c,(1),0.0)));
var G__19894_20291 = ((G__19896_20277)?(G__19885_20282[(2)]):(cljs.core.truth_(G__19898_20279)?c:cljs.core.nth.call(null,c,(2),0.0)));
var x1_20292 = (((G__19889_20286 - G__19886_20283) * u) + G__19886_20283);
var y1_20293 = (((G__19890_20287 - G__19887_20284) * u) + G__19887_20284);
var z1_20294 = (((G__19891_20288 - G__19888_20285) * u) + G__19888_20285);
(b_SINGLEQUOTE_[(0)] = ((((((dx - G__19892_20289) * u) + G__19892_20289) - x1_20292) * v) + x1_20292));

(b_SINGLEQUOTE_[(1)] = ((((((dy - G__19893_20290) * u) + G__19893_20290) - y1_20293) * v) + y1_20293));

(b_SINGLEQUOTE_[(2)] = ((((((dz - G__19894_20291) * u) + G__19894_20291) - z1_20294) * v) + z1_20294));

return (new thi.ng.geom.core.vector.Vec3(b_SINGLEQUOTE_,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PLimit$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PLimit$limit$arity$2 = (function (_,len){
var self__ = this;
var ___$1 = this;
if((thi.ng.geom.core.mag_squared.call(null,___$1) > (len * len))){
return thi.ng.geom.core.normalize.call(null,___$1,len);
} else {
return ___$1;
}
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PPolar$ = true;

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PPolar$as_polar$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var r = thi.ng.geom.core.mag.call(null,___$1);
var b = (new Float32Array((3)));
(b[(0)] = r);

(b[(1)] = Math.asin(((self__.buf[(2)]) / r)));

(b[(2)] = Math.atan2((self__.buf[(1)]),(self__.buf[(0)])));

return (new thi.ng.geom.core.vector.Vec3(b,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.prototype.thi$ng$geom$core$PPolar$as_cartesian$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
var b = self__.buf;
var x = (b[(0)]);
var y = (b[(1)]);
var z = (b[(2)]);
var rcos = (x * Math.cos(y));
var b_SINGLEQUOTE_ = (new Float32Array((3)));
(b_SINGLEQUOTE_[(0)] = (rcos * Math.cos(z)));

(b_SINGLEQUOTE_[(1)] = (rcos * Math.sin(z)));

(b_SINGLEQUOTE_[(2)] = (x * Math.sin(y)));

return (new thi.ng.geom.core.vector.Vec3(b_SINGLEQUOTE_,null,self__._meta));
});

thi.ng.geom.core.vector.Vec3.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"buf","buf",1426618187,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_hash","_hash",-2130838312,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"mutable","mutable",875778266),true], null)),new cljs.core.Symbol(null,"_meta","_meta",-1716892533,null)], null);
});

thi.ng.geom.core.vector.Vec3.cljs$lang$type = true;

thi.ng.geom.core.vector.Vec3.cljs$lang$ctorStr = "thi.ng.geom.core.vector/Vec3";

thi.ng.geom.core.vector.Vec3.cljs$lang$ctorPrWriter = (function (this__17394__auto__,writer__17395__auto__,opt__17396__auto__){
return cljs.core._write.call(null,writer__17395__auto__,"thi.ng.geom.core.vector/Vec3");
});

thi.ng.geom.core.vector.__GT_Vec3 = (function thi$ng$geom$core$vector$__GT_Vec3(buf,_hash,_meta){
return (new thi.ng.geom.core.vector.Vec3(buf,_hash,_meta));
});

thi.ng.geom.core.vector.x = (function thi$ng$geom$core$vector$x(G__20297){
var G__20295 = (((G__20297 instanceof thi.ng.geom.core.vector.Vec2))?G__20297.buf:G__20297.buf);
return (G__20295[(0)]);
});
thi.ng.geom.core.vector.xx = (function thi$ng$geom$core$vector$xx(G__20300){
var G__20298 = (((G__20300 instanceof thi.ng.geom.core.vector.Vec2))?G__20300.buf:G__20300.buf);
var G__20299 = (new Float32Array(2));
(G__20299[(0)] = (G__20298[(0)]));

(G__20299[(1)] = (G__20298[(0)]));

return (new thi.ng.geom.core.vector.Vec2(G__20299,null,cljs.core.meta.call(null,G__20300)));
});
thi.ng.geom.core.vector.xxx = (function thi$ng$geom$core$vector$xxx(G__20303){
var G__20301 = (((G__20303 instanceof thi.ng.geom.core.vector.Vec2))?G__20303.buf:G__20303.buf);
var G__20302 = (new Float32Array(3));
(G__20302[(0)] = (G__20301[(0)]));

(G__20302[(1)] = (G__20301[(0)]));

(G__20302[(2)] = (G__20301[(0)]));

return (new thi.ng.geom.core.vector.Vec3(G__20302,null,cljs.core.meta.call(null,G__20303)));
});
thi.ng.geom.core.vector.xxy = (function thi$ng$geom$core$vector$xxy(G__20306){
var G__20304 = (((G__20306 instanceof thi.ng.geom.core.vector.Vec2))?G__20306.buf:G__20306.buf);
var G__20305 = (new Float32Array(3));
(G__20305[(0)] = (G__20304[(0)]));

(G__20305[(1)] = (G__20304[(0)]));

(G__20305[(2)] = (G__20304[(1)]));

return (new thi.ng.geom.core.vector.Vec3(G__20305,null,cljs.core.meta.call(null,G__20306)));
});
thi.ng.geom.core.vector.xxz = (function thi$ng$geom$core$vector$xxz(G__20309){
if((G__20309 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20309","G__20309",-289699558,null))))].join('')));
}

var G__20307 = (((G__20309 instanceof thi.ng.geom.core.vector.Vec2))?G__20309.buf:G__20309.buf);
var G__20308 = (new Float32Array(3));
(G__20308[(0)] = (G__20307[(0)]));

(G__20308[(1)] = (G__20307[(0)]));

(G__20308[(2)] = (G__20307[(2)]));

return (new thi.ng.geom.core.vector.Vec3(G__20308,null,cljs.core.meta.call(null,G__20309)));
});
thi.ng.geom.core.vector.xy = (function thi$ng$geom$core$vector$xy(G__20312){
var G__20310 = (((G__20312 instanceof thi.ng.geom.core.vector.Vec2))?G__20312.buf:G__20312.buf);
var G__20311 = (new Float32Array(2));
(G__20311[(0)] = (G__20310[(0)]));

(G__20311[(1)] = (G__20310[(1)]));

return (new thi.ng.geom.core.vector.Vec2(G__20311,null,cljs.core.meta.call(null,G__20312)));
});
thi.ng.geom.core.vector.xyx = (function thi$ng$geom$core$vector$xyx(G__20315){
var G__20313 = (((G__20315 instanceof thi.ng.geom.core.vector.Vec2))?G__20315.buf:G__20315.buf);
var G__20314 = (new Float32Array(3));
(G__20314[(0)] = (G__20313[(0)]));

(G__20314[(1)] = (G__20313[(1)]));

(G__20314[(2)] = (G__20313[(0)]));

return (new thi.ng.geom.core.vector.Vec3(G__20314,null,cljs.core.meta.call(null,G__20315)));
});
thi.ng.geom.core.vector.xyy = (function thi$ng$geom$core$vector$xyy(G__20318){
var G__20316 = (((G__20318 instanceof thi.ng.geom.core.vector.Vec2))?G__20318.buf:G__20318.buf);
var G__20317 = (new Float32Array(3));
(G__20317[(0)] = (G__20316[(0)]));

(G__20317[(1)] = (G__20316[(1)]));

(G__20317[(2)] = (G__20316[(1)]));

return (new thi.ng.geom.core.vector.Vec3(G__20317,null,cljs.core.meta.call(null,G__20318)));
});
thi.ng.geom.core.vector.xyz = (function thi$ng$geom$core$vector$xyz(G__20321){
if((G__20321 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20321","G__20321",-1439916495,null))))].join('')));
}

var G__20319 = (((G__20321 instanceof thi.ng.geom.core.vector.Vec2))?G__20321.buf:G__20321.buf);
var G__20320 = (new Float32Array(3));
(G__20320[(0)] = (G__20319[(0)]));

(G__20320[(1)] = (G__20319[(1)]));

(G__20320[(2)] = (G__20319[(2)]));

return (new thi.ng.geom.core.vector.Vec3(G__20320,null,cljs.core.meta.call(null,G__20321)));
});
thi.ng.geom.core.vector.xz = (function thi$ng$geom$core$vector$xz(G__20324){
if((G__20324 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20324","G__20324",-1843920777,null))))].join('')));
}

var G__20322 = (((G__20324 instanceof thi.ng.geom.core.vector.Vec2))?G__20324.buf:G__20324.buf);
var G__20323 = (new Float32Array(2));
(G__20323[(0)] = (G__20322[(0)]));

(G__20323[(1)] = (G__20322[(2)]));

return (new thi.ng.geom.core.vector.Vec2(G__20323,null,cljs.core.meta.call(null,G__20324)));
});
thi.ng.geom.core.vector.xzx = (function thi$ng$geom$core$vector$xzx(G__20327){
if((G__20327 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20327","G__20327",144843640,null))))].join('')));
}

var G__20325 = (((G__20327 instanceof thi.ng.geom.core.vector.Vec2))?G__20327.buf:G__20327.buf);
var G__20326 = (new Float32Array(3));
(G__20326[(0)] = (G__20325[(0)]));

(G__20326[(1)] = (G__20325[(2)]));

(G__20326[(2)] = (G__20325[(0)]));

return (new thi.ng.geom.core.vector.Vec3(G__20326,null,cljs.core.meta.call(null,G__20327)));
});
thi.ng.geom.core.vector.xzy = (function thi$ng$geom$core$vector$xzy(G__20330){
if((G__20330 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20330","G__20330",1617930871,null))))].join('')));
}

var G__20328 = (((G__20330 instanceof thi.ng.geom.core.vector.Vec2))?G__20330.buf:G__20330.buf);
var G__20329 = (new Float32Array(3));
(G__20329[(0)] = (G__20328[(0)]));

(G__20329[(1)] = (G__20328[(2)]));

(G__20329[(2)] = (G__20328[(1)]));

return (new thi.ng.geom.core.vector.Vec3(G__20329,null,cljs.core.meta.call(null,G__20330)));
});
thi.ng.geom.core.vector.xzz = (function thi$ng$geom$core$vector$xzz(G__20333){
if((G__20333 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20333","G__20333",-79591614,null))))].join('')));
}

var G__20331 = (((G__20333 instanceof thi.ng.geom.core.vector.Vec2))?G__20333.buf:G__20333.buf);
var G__20332 = (new Float32Array(3));
(G__20332[(0)] = (G__20331[(0)]));

(G__20332[(1)] = (G__20331[(2)]));

(G__20332[(2)] = (G__20331[(2)]));

return (new thi.ng.geom.core.vector.Vec3(G__20332,null,cljs.core.meta.call(null,G__20333)));
});
thi.ng.geom.core.vector.y = (function thi$ng$geom$core$vector$y(G__20336){
var G__20334 = (((G__20336 instanceof thi.ng.geom.core.vector.Vec2))?G__20336.buf:G__20336.buf);
return (G__20334[(1)]);
});
thi.ng.geom.core.vector.yx = (function thi$ng$geom$core$vector$yx(G__20339){
var G__20337 = (((G__20339 instanceof thi.ng.geom.core.vector.Vec2))?G__20339.buf:G__20339.buf);
var G__20338 = (new Float32Array(2));
(G__20338[(0)] = (G__20337[(1)]));

(G__20338[(1)] = (G__20337[(0)]));

return (new thi.ng.geom.core.vector.Vec2(G__20338,null,cljs.core.meta.call(null,G__20339)));
});
thi.ng.geom.core.vector.yxx = (function thi$ng$geom$core$vector$yxx(G__20342){
var G__20340 = (((G__20342 instanceof thi.ng.geom.core.vector.Vec2))?G__20342.buf:G__20342.buf);
var G__20341 = (new Float32Array(3));
(G__20341[(0)] = (G__20340[(1)]));

(G__20341[(1)] = (G__20340[(0)]));

(G__20341[(2)] = (G__20340[(0)]));

return (new thi.ng.geom.core.vector.Vec3(G__20341,null,cljs.core.meta.call(null,G__20342)));
});
thi.ng.geom.core.vector.yxy = (function thi$ng$geom$core$vector$yxy(G__20345){
var G__20343 = (((G__20345 instanceof thi.ng.geom.core.vector.Vec2))?G__20345.buf:G__20345.buf);
var G__20344 = (new Float32Array(3));
(G__20344[(0)] = (G__20343[(1)]));

(G__20344[(1)] = (G__20343[(0)]));

(G__20344[(2)] = (G__20343[(1)]));

return (new thi.ng.geom.core.vector.Vec3(G__20344,null,cljs.core.meta.call(null,G__20345)));
});
thi.ng.geom.core.vector.yxz = (function thi$ng$geom$core$vector$yxz(G__20348){
if((G__20348 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20348","G__20348",-954136950,null))))].join('')));
}

var G__20346 = (((G__20348 instanceof thi.ng.geom.core.vector.Vec2))?G__20348.buf:G__20348.buf);
var G__20347 = (new Float32Array(3));
(G__20347[(0)] = (G__20346[(1)]));

(G__20347[(1)] = (G__20346[(0)]));

(G__20347[(2)] = (G__20346[(2)]));

return (new thi.ng.geom.core.vector.Vec3(G__20347,null,cljs.core.meta.call(null,G__20348)));
});
thi.ng.geom.core.vector.yy = (function thi$ng$geom$core$vector$yy(G__20351){
var G__20349 = (((G__20351 instanceof thi.ng.geom.core.vector.Vec2))?G__20351.buf:G__20351.buf);
var G__20350 = (new Float32Array(2));
(G__20350[(0)] = (G__20349[(1)]));

(G__20350[(1)] = (G__20349[(1)]));

return (new thi.ng.geom.core.vector.Vec2(G__20350,null,cljs.core.meta.call(null,G__20351)));
});
thi.ng.geom.core.vector.yyx = (function thi$ng$geom$core$vector$yyx(G__20354){
var G__20352 = (((G__20354 instanceof thi.ng.geom.core.vector.Vec2))?G__20354.buf:G__20354.buf);
var G__20353 = (new Float32Array(3));
(G__20353[(0)] = (G__20352[(1)]));

(G__20353[(1)] = (G__20352[(1)]));

(G__20353[(2)] = (G__20352[(0)]));

return (new thi.ng.geom.core.vector.Vec3(G__20353,null,cljs.core.meta.call(null,G__20354)));
});
thi.ng.geom.core.vector.yyy = (function thi$ng$geom$core$vector$yyy(G__20357){
var G__20355 = (((G__20357 instanceof thi.ng.geom.core.vector.Vec2))?G__20357.buf:G__20357.buf);
var G__20356 = (new Float32Array(3));
(G__20356[(0)] = (G__20355[(1)]));

(G__20356[(1)] = (G__20355[(1)]));

(G__20356[(2)] = (G__20355[(1)]));

return (new thi.ng.geom.core.vector.Vec3(G__20356,null,cljs.core.meta.call(null,G__20357)));
});
thi.ng.geom.core.vector.yyz = (function thi$ng$geom$core$vector$yyz(G__20360){
if((G__20360 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20360","G__20360",643171700,null))))].join('')));
}

var G__20358 = (((G__20360 instanceof thi.ng.geom.core.vector.Vec2))?G__20360.buf:G__20360.buf);
var G__20359 = (new Float32Array(3));
(G__20359[(0)] = (G__20358[(1)]));

(G__20359[(1)] = (G__20358[(1)]));

(G__20359[(2)] = (G__20358[(2)]));

return (new thi.ng.geom.core.vector.Vec3(G__20359,null,cljs.core.meta.call(null,G__20360)));
});
thi.ng.geom.core.vector.yz = (function thi$ng$geom$core$vector$yz(G__20363){
if((G__20363 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20363","G__20363",1281143138,null))))].join('')));
}

var G__20361 = (((G__20363 instanceof thi.ng.geom.core.vector.Vec2))?G__20363.buf:G__20363.buf);
var G__20362 = (new Float32Array(2));
(G__20362[(0)] = (G__20361[(1)]));

(G__20362[(1)] = (G__20361[(2)]));

return (new thi.ng.geom.core.vector.Vec2(G__20362,null,cljs.core.meta.call(null,G__20363)));
});
thi.ng.geom.core.vector.yzx = (function thi$ng$geom$core$vector$yzx(G__20366){
if((G__20366 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20366","G__20366",973448067,null))))].join('')));
}

var G__20364 = (((G__20366 instanceof thi.ng.geom.core.vector.Vec2))?G__20366.buf:G__20366.buf);
var G__20365 = (new Float32Array(3));
(G__20365[(0)] = (G__20364[(1)]));

(G__20365[(1)] = (G__20364[(2)]));

(G__20365[(2)] = (G__20364[(0)]));

return (new thi.ng.geom.core.vector.Vec3(G__20365,null,cljs.core.meta.call(null,G__20366)));
});
thi.ng.geom.core.vector.yzy = (function thi$ng$geom$core$vector$yzy(G__20369){
if((G__20369 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20369","G__20369",323384698,null))))].join('')));
}

var G__20367 = (((G__20369 instanceof thi.ng.geom.core.vector.Vec2))?G__20369.buf:G__20369.buf);
var G__20368 = (new Float32Array(3));
(G__20368[(0)] = (G__20367[(1)]));

(G__20368[(1)] = (G__20367[(2)]));

(G__20368[(2)] = (G__20367[(1)]));

return (new thi.ng.geom.core.vector.Vec3(G__20368,null,cljs.core.meta.call(null,G__20369)));
});
thi.ng.geom.core.vector.yzz = (function thi$ng$geom$core$vector$yzz(G__20372){
if((G__20372 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20372","G__20372",152413964,null))))].join('')));
}

var G__20370 = (((G__20372 instanceof thi.ng.geom.core.vector.Vec2))?G__20372.buf:G__20372.buf);
var G__20371 = (new Float32Array(3));
(G__20371[(0)] = (G__20370[(1)]));

(G__20371[(1)] = (G__20370[(2)]));

(G__20371[(2)] = (G__20370[(2)]));

return (new thi.ng.geom.core.vector.Vec3(G__20371,null,cljs.core.meta.call(null,G__20372)));
});
thi.ng.geom.core.vector.z = (function thi$ng$geom$core$vector$z(G__20375){
if((G__20375 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20375","G__20375",-1950441988,null))))].join('')));
}

var G__20373 = (((G__20375 instanceof thi.ng.geom.core.vector.Vec2))?G__20375.buf:G__20375.buf);
return (G__20373[(2)]);
});
thi.ng.geom.core.vector.zx = (function thi$ng$geom$core$vector$zx(G__20378){
if((G__20378 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20378","G__20378",-1017851158,null))))].join('')));
}

var G__20376 = (((G__20378 instanceof thi.ng.geom.core.vector.Vec2))?G__20378.buf:G__20378.buf);
var G__20377 = (new Float32Array(2));
(G__20377[(0)] = (G__20376[(2)]));

(G__20377[(1)] = (G__20376[(0)]));

return (new thi.ng.geom.core.vector.Vec2(G__20377,null,cljs.core.meta.call(null,G__20378)));
});
thi.ng.geom.core.vector.zxx = (function thi$ng$geom$core$vector$zxx(G__20381){
if((G__20381 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20381","G__20381",-773530318,null))))].join('')));
}

var G__20379 = (((G__20381 instanceof thi.ng.geom.core.vector.Vec2))?G__20381.buf:G__20381.buf);
var G__20380 = (new Float32Array(3));
(G__20380[(0)] = (G__20379[(2)]));

(G__20380[(1)] = (G__20379[(0)]));

(G__20380[(2)] = (G__20379[(0)]));

return (new thi.ng.geom.core.vector.Vec3(G__20380,null,cljs.core.meta.call(null,G__20381)));
});
thi.ng.geom.core.vector.zxy = (function thi$ng$geom$core$vector$zxy(G__20384){
if((G__20384 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20384","G__20384",-1777781161,null))))].join('')));
}

var G__20382 = (((G__20384 instanceof thi.ng.geom.core.vector.Vec2))?G__20384.buf:G__20384.buf);
var G__20383 = (new Float32Array(3));
(G__20383[(0)] = (G__20382[(2)]));

(G__20383[(1)] = (G__20382[(0)]));

(G__20383[(2)] = (G__20382[(1)]));

return (new thi.ng.geom.core.vector.Vec3(G__20383,null,cljs.core.meta.call(null,G__20384)));
});
thi.ng.geom.core.vector.zxz = (function thi$ng$geom$core$vector$zxz(G__20387){
if((G__20387 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20387","G__20387",22951851,null))))].join('')));
}

var G__20385 = (((G__20387 instanceof thi.ng.geom.core.vector.Vec2))?G__20387.buf:G__20387.buf);
var G__20386 = (new Float32Array(3));
(G__20386[(0)] = (G__20385[(2)]));

(G__20386[(1)] = (G__20385[(0)]));

(G__20386[(2)] = (G__20385[(2)]));

return (new thi.ng.geom.core.vector.Vec3(G__20386,null,cljs.core.meta.call(null,G__20387)));
});
thi.ng.geom.core.vector.zy = (function thi$ng$geom$core$vector$zy(G__20390){
if((G__20390 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20390","G__20390",1369810836,null))))].join('')));
}

var G__20388 = (((G__20390 instanceof thi.ng.geom.core.vector.Vec2))?G__20390.buf:G__20390.buf);
var G__20389 = (new Float32Array(2));
(G__20389[(0)] = (G__20388[(2)]));

(G__20389[(1)] = (G__20388[(1)]));

return (new thi.ng.geom.core.vector.Vec2(G__20389,null,cljs.core.meta.call(null,G__20390)));
});
thi.ng.geom.core.vector.zyx = (function thi$ng$geom$core$vector$zyx(G__20393){
if((G__20393 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20393","G__20393",1506726298,null))))].join('')));
}

var G__20391 = (((G__20393 instanceof thi.ng.geom.core.vector.Vec2))?G__20393.buf:G__20393.buf);
var G__20392 = (new Float32Array(3));
(G__20392[(0)] = (G__20391[(2)]));

(G__20392[(1)] = (G__20391[(1)]));

(G__20392[(2)] = (G__20391[(0)]));

return (new thi.ng.geom.core.vector.Vec3(G__20392,null,cljs.core.meta.call(null,G__20393)));
});
thi.ng.geom.core.vector.zyy = (function thi$ng$geom$core$vector$zyy(G__20396){
if((G__20396 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20396","G__20396",1861291742,null))))].join('')));
}

var G__20394 = (((G__20396 instanceof thi.ng.geom.core.vector.Vec2))?G__20396.buf:G__20396.buf);
var G__20395 = (new Float32Array(3));
(G__20395[(0)] = (G__20394[(2)]));

(G__20395[(1)] = (G__20394[(1)]));

(G__20395[(2)] = (G__20394[(1)]));

return (new thi.ng.geom.core.vector.Vec3(G__20395,null,cljs.core.meta.call(null,G__20396)));
});
thi.ng.geom.core.vector.zyz = (function thi$ng$geom$core$vector$zyz(G__20399){
if((G__20399 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20399","G__20399",-1999341547,null))))].join('')));
}

var G__20397 = (((G__20399 instanceof thi.ng.geom.core.vector.Vec2))?G__20399.buf:G__20399.buf);
var G__20398 = (new Float32Array(3));
(G__20398[(0)] = (G__20397[(2)]));

(G__20398[(1)] = (G__20397[(1)]));

(G__20398[(2)] = (G__20397[(2)]));

return (new thi.ng.geom.core.vector.Vec3(G__20398,null,cljs.core.meta.call(null,G__20399)));
});
thi.ng.geom.core.vector.zz = (function thi$ng$geom$core$vector$zz(G__20402){
if((G__20402 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20402","G__20402",-1681041212,null))))].join('')));
}

var G__20400 = (((G__20402 instanceof thi.ng.geom.core.vector.Vec2))?G__20402.buf:G__20402.buf);
var G__20401 = (new Float32Array(2));
(G__20401[(0)] = (G__20400[(2)]));

(G__20401[(1)] = (G__20400[(2)]));

return (new thi.ng.geom.core.vector.Vec2(G__20401,null,cljs.core.meta.call(null,G__20402)));
});
thi.ng.geom.core.vector.zzx = (function thi$ng$geom$core$vector$zzx(G__20405){
if((G__20405 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20405","G__20405",-158082677,null))))].join('')));
}

var G__20403 = (((G__20405 instanceof thi.ng.geom.core.vector.Vec2))?G__20405.buf:G__20405.buf);
var G__20404 = (new Float32Array(3));
(G__20404[(0)] = (G__20403[(2)]));

(G__20404[(1)] = (G__20403[(2)]));

(G__20404[(2)] = (G__20403[(0)]));

return (new thi.ng.geom.core.vector.Vec3(G__20404,null,cljs.core.meta.call(null,G__20405)));
});
thi.ng.geom.core.vector.zzy = (function thi$ng$geom$core$vector$zzy(G__20408){
if((G__20408 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20408","G__20408",1658007900,null))))].join('')));
}

var G__20406 = (((G__20408 instanceof thi.ng.geom.core.vector.Vec2))?G__20408.buf:G__20408.buf);
var G__20407 = (new Float32Array(3));
(G__20407[(0)] = (G__20406[(2)]));

(G__20407[(1)] = (G__20406[(2)]));

(G__20407[(2)] = (G__20406[(1)]));

return (new thi.ng.geom.core.vector.Vec3(G__20407,null,cljs.core.meta.call(null,G__20408)));
});
thi.ng.geom.core.vector.zzz = (function thi$ng$geom$core$vector$zzz(G__20411){
if((G__20411 instanceof thi.ng.geom.core.vector.Vec3)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol("clojure.core","instance?","clojure.core/instance?",2143709132,null),new cljs.core.Symbol(null,"Vec3","Vec3",429395803,null),new cljs.core.Symbol(null,"G__20411","G__20411",-1619198064,null))))].join('')));
}

var G__20409 = (((G__20411 instanceof thi.ng.geom.core.vector.Vec2))?G__20411.buf:G__20411.buf);
var G__20410 = (new Float32Array(3));
(G__20410[(0)] = (G__20409[(2)]));

(G__20410[(1)] = (G__20409[(2)]));

(G__20410[(2)] = (G__20409[(2)]));

return (new thi.ng.geom.core.vector.Vec3(G__20410,null,cljs.core.meta.call(null,G__20411)));
});
thi.ng.geom.core.vector.swizzle2_fns = new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"x","x",2099068185),thi.ng.geom.core.vector.x,new cljs.core.Keyword(null,"xx","xx",-1542203733),thi.ng.geom.core.vector.xx,new cljs.core.Keyword(null,"xy","xy",-696978232),thi.ng.geom.core.vector.xy,new cljs.core.Keyword(null,"y","y",-1757859776),thi.ng.geom.core.vector.y,new cljs.core.Keyword(null,"yx","yx",1696579752),thi.ng.geom.core.vector.yx,new cljs.core.Keyword(null,"yy","yy",-1432012814),thi.ng.geom.core.vector.yy], null);
thi.ng.geom.core.vector.swizzle3_fns = cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"y","y",-1757859776),new cljs.core.Keyword(null,"xzx","xzx",-1000197983),new cljs.core.Keyword(null,"xyz","xyz",-1605570015),new cljs.core.Keyword(null,"zzy","zzy",-874287326),new cljs.core.Keyword(null,"yx","yx",1696579752),new cljs.core.Keyword(null,"xy","xy",-696978232),new cljs.core.Keyword(null,"yyz","yyz",1133968296),new cljs.core.Keyword(null,"zxy","zxy",-1258840183),new cljs.core.Keyword(null,"xzy","xzy",1043177385),new cljs.core.Keyword(null,"zxz","zxz",1026042602),new cljs.core.Keyword(null,"zx","zx",-933582998),new cljs.core.Keyword(null,"xx","xx",-1542203733),new cljs.core.Keyword(null,"xxx","xxx",-1019301908),new cljs.core.Keyword(null,"zy","zy",-1975963090),new cljs.core.Keyword(null,"zzx","zzx",20750383),new cljs.core.Keyword(null,"zyx","zyx",1752527951),new cljs.core.Keyword(null,"yzx","yzx",-1496223025),new cljs.core.Keyword(null,"z","z",-789527183),new cljs.core.Keyword(null,"yyx","yyx",-1318218191),new cljs.core.Keyword(null,"xz","xz",426487154),new cljs.core.Keyword(null,"zyz","zyz",-1838068142),new cljs.core.Keyword(null,"yy","yy",-1432012814),new cljs.core.Keyword(null,"xxz","xxz",129827699),new cljs.core.Keyword(null,"yzy","yzy",-179510251),new cljs.core.Keyword(null,"yz","yz",679015029),new cljs.core.Keyword(null,"yxx","yxx",-332290091),new cljs.core.Keyword(null,"xyy","xyy",996073014),new cljs.core.Keyword(null,"xxy","xxy",-650102026),new cljs.core.Keyword(null,"zz","zz",122901783),new cljs.core.Keyword(null,"zzz","zzz",-77420552),new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"xzz","xzz",-643126693),new cljs.core.Keyword(null,"yxz","yxz",1786796508),new cljs.core.Keyword(null,"zxx","zxx",-61980804),new cljs.core.Keyword(null,"yzz","yzz",-1034441732),new cljs.core.Keyword(null,"xyx","xyx",1899467293),new cljs.core.Keyword(null,"yxy","yxy",1369901661),new cljs.core.Keyword(null,"yyy","yyy",780595422),new cljs.core.Keyword(null,"zyy","zyy",1946268991)],[thi.ng.geom.core.vector.y,thi.ng.geom.core.vector.xzx,thi.ng.geom.core.vector.xyz,thi.ng.geom.core.vector.zzy,thi.ng.geom.core.vector.yx,thi.ng.geom.core.vector.xy,thi.ng.geom.core.vector.yyz,thi.ng.geom.core.vector.zxy,thi.ng.geom.core.vector.xzy,thi.ng.geom.core.vector.zxz,thi.ng.geom.core.vector.zx,thi.ng.geom.core.vector.xx,thi.ng.geom.core.vector.xxx,thi.ng.geom.core.vector.zy,thi.ng.geom.core.vector.zzx,thi.ng.geom.core.vector.zyx,thi.ng.geom.core.vector.yzx,thi.ng.geom.core.vector.z,thi.ng.geom.core.vector.yyx,thi.ng.geom.core.vector.xz,thi.ng.geom.core.vector.zyz,thi.ng.geom.core.vector.yy,thi.ng.geom.core.vector.xxz,thi.ng.geom.core.vector.yzy,thi.ng.geom.core.vector.yz,thi.ng.geom.core.vector.yxx,thi.ng.geom.core.vector.xyy,thi.ng.geom.core.vector.xxy,thi.ng.geom.core.vector.zz,thi.ng.geom.core.vector.zzz,thi.ng.geom.core.vector.x,thi.ng.geom.core.vector.xzz,thi.ng.geom.core.vector.yxz,thi.ng.geom.core.vector.zxx,thi.ng.geom.core.vector.yzz,thi.ng.geom.core.vector.xyx,thi.ng.geom.core.vector.yxy,thi.ng.geom.core.vector.yyy,thi.ng.geom.core.vector.zyy]);
thi.ng.geom.core.vector.swizzle_assoc_STAR_ = (function thi$ng$geom$core$vector$swizzle_assoc_STAR_(src,dest,keymap,k,v){
var n = cljs.core.name.call(null,k);
var c = cljs.core.count.call(null,n);
var temp__4423__auto__ = (function (){var and__16784__auto__ = ((1) === c);
if(and__16784__auto__){
return keymap.call(null,cljs.core.first.call(null,n));
} else {
return and__16784__auto__;
}
})();
if(cljs.core.truth_(temp__4423__auto__)){
var idx = temp__4423__auto__;
(dest[(idx | (0))] = v);

return dest;
} else {
if(((c <= cljs.core.count.call(null,keymap))) && (((c === cljs.core.count.call(null,v))) && ((cljs.core.count.call(null,v) === cljs.core.count.call(null,cljs.core.into.call(null,cljs.core.PersistentHashSet.EMPTY,n)))))){
var i = (0);
var n__$1 = n;
while(true){
if(cljs.core.truth_(n__$1)){
var temp__4423__auto____$1 = keymap.call(null,cljs.core.first.call(null,n__$1));
if(cljs.core.truth_(temp__4423__auto____$1)){
var idx = temp__4423__auto____$1;
(dest[(idx | (0))] = v.call(null,i));

var G__20412 = (i + (1));
var G__20413 = cljs.core.next.call(null,n__$1);
i = G__20412;
n__$1 = G__20413;
continue;
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
} else {
return dest;
}
break;
}
} else {
return thi.ng.xerror.core.key_error_BANG_.call(null,k);
}
}
});
thi.ng.geom.core.vector.vec2_reduce_STAR_ = (function thi$ng$geom$core$vector$vec2_reduce_STAR_(op,acc,xs){
return cljs.core.transduce.call(null,cljs.core.map.call(null,(function (x){
return x.buf;
})),(function() {
var G__20414 = null;
var G__20414__1 = (function (a){
return a;
});
var G__20414__2 = (function (a,b){
(a[(0)] = op.call(null,(a[(0)]),(b[(0)])));

(a[(1)] = op.call(null,(a[(1)]),(b[(1)])));

return a;
});
G__20414 = function(a,b){
switch(arguments.length){
case 1:
return G__20414__1.call(this,a);
case 2:
return G__20414__2.call(this,a,b);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__20414.cljs$core$IFn$_invoke$arity$1 = G__20414__1;
G__20414.cljs$core$IFn$_invoke$arity$2 = G__20414__2;
return G__20414;
})()
,acc,xs);
});
thi.ng.geom.core.vector.vec3_reduce_STAR_ = (function thi$ng$geom$core$vector$vec3_reduce_STAR_(op,acc,xs){
return cljs.core.transduce.call(null,cljs.core.map.call(null,(function (x){
return x.buf;
})),(function() {
var G__20415 = null;
var G__20415__1 = (function (a){
return a;
});
var G__20415__2 = (function (a,b){
(a[(0)] = op.call(null,(a[(0)]),(b[(0)])));

(a[(1)] = op.call(null,(a[(1)]),(b[(1)])));

(a[(2)] = op.call(null,(a[(2)]),(b[(2)])));

return a;
});
G__20415 = function(a,b){
switch(arguments.length){
case 1:
return G__20415__1.call(this,a);
case 2:
return G__20415__2.call(this,a,b);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__20415.cljs$core$IFn$_invoke$arity$1 = G__20415__1;
G__20415.cljs$core$IFn$_invoke$arity$2 = G__20415__2;
return G__20415;
})()
,acc,xs);
});
thi.ng.geom.core.vector.V2 = (new thi.ng.geom.core.vector.Vec2((new Float32Array((2))),null,null));
thi.ng.geom.core.vector.V3 = (new thi.ng.geom.core.vector.Vec3((new Float32Array((3))),null,null));
thi.ng.geom.core.vector.vec2 = (function thi$ng$geom$core$vector$vec2(var_args){
var args20416 = [];
var len__17855__auto___20419 = arguments.length;
var i__17856__auto___20420 = (0);
while(true){
if((i__17856__auto___20420 < len__17855__auto___20419)){
args20416.push((arguments[i__17856__auto___20420]));

var G__20421 = (i__17856__auto___20420 + (1));
i__17856__auto___20420 = G__20421;
continue;
} else {
}
break;
}

var G__20418 = args20416.length;
switch (G__20418) {
case 0:
return thi.ng.geom.core.vector.vec2.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return thi.ng.geom.core.vector.vec2.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return thi.ng.geom.core.vector.vec2.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args20416.length)].join('')));

}
});

thi.ng.geom.core.vector.vec2.cljs$core$IFn$_invoke$arity$0 = (function (){
return thi.ng.geom.core.vector.V2;
});

thi.ng.geom.core.vector.vec2.cljs$core$IFn$_invoke$arity$1 = (function (v){
if((v instanceof thi.ng.geom.core.vector.Vec2)){
return v;
} else {
if(typeof v === 'number'){
return thi.ng.geom.core.vector.vec2.call(null,v,v);
} else {
if(cljs.core.sequential_QMARK_.call(null,v)){
return thi.ng.geom.core.vector.vec2.call(null,cljs.core.nth.call(null,v,(0),0.0),cljs.core.nth.call(null,v,(1),0.0));
} else {
if(cljs.core.map_QMARK_.call(null,v)){
return thi.ng.geom.core.vector.vec2.call(null,new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$2(v,(0)),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$2(v,(0)));
} else {
return thi.ng.xerror.core.type_error_BANG_.call(null,"Vec2",v);

}
}
}
}
});

thi.ng.geom.core.vector.vec2.cljs$core$IFn$_invoke$arity$2 = (function (x,y){
var b = (new Float32Array((2)));
(b[(0)] = x);

(b[(1)] = y);

return (new thi.ng.geom.core.vector.Vec2(b,null,null));
});

thi.ng.geom.core.vector.vec2.cljs$lang$maxFixedArity = 2;
thi.ng.geom.core.vector.vec3 = (function thi$ng$geom$core$vector$vec3(var_args){
var args20423 = [];
var len__17855__auto___20426 = arguments.length;
var i__17856__auto___20427 = (0);
while(true){
if((i__17856__auto___20427 < len__17855__auto___20426)){
args20423.push((arguments[i__17856__auto___20427]));

var G__20428 = (i__17856__auto___20427 + (1));
i__17856__auto___20427 = G__20428;
continue;
} else {
}
break;
}

var G__20425 = args20423.length;
switch (G__20425) {
case 0:
return thi.ng.geom.core.vector.vec3.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return thi.ng.geom.core.vector.vec3.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return thi.ng.geom.core.vector.vec3.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return thi.ng.geom.core.vector.vec3.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args20423.length)].join('')));

}
});

thi.ng.geom.core.vector.vec3.cljs$core$IFn$_invoke$arity$0 = (function (){
return thi.ng.geom.core.vector.V3;
});

thi.ng.geom.core.vector.vec3.cljs$core$IFn$_invoke$arity$1 = (function (v){
if((v instanceof thi.ng.geom.core.vector.Vec3)){
return v;
} else {
if(typeof v === 'number'){
return thi.ng.geom.core.vector.vec3.call(null,v,v,v);
} else {
if(cljs.core.sequential_QMARK_.call(null,v)){
return thi.ng.geom.core.vector.vec3.call(null,cljs.core.nth.call(null,v,(0),0.0),cljs.core.nth.call(null,v,(1),0.0),cljs.core.nth.call(null,v,(2),0.0));
} else {
if(cljs.core.map_QMARK_.call(null,v)){
return thi.ng.geom.core.vector.vec3.call(null,new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$2(v,(0)),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$2(v,(0)),new cljs.core.Keyword(null,"z","z",-789527183).cljs$core$IFn$_invoke$arity$2(v,(0)));
} else {
return thi.ng.xerror.core.type_error_BANG_.call(null,"Vec3",v);

}
}
}
}
});

thi.ng.geom.core.vector.vec3.cljs$core$IFn$_invoke$arity$2 = (function (v,z){
if(cljs.core.sequential_QMARK_.call(null,v)){
return thi.ng.geom.core.vector.vec3.call(null,cljs.core.nth.call(null,v,(0),0.0),cljs.core.nth.call(null,v,(1),0.0),z);
} else {
if(cljs.core.map_QMARK_.call(null,v)){
return thi.ng.geom.core.vector.vec3.call(null,new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$2(v,(0)),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$2(v,(0)),z);
} else {
if(typeof v === 'number'){
return thi.ng.geom.core.vector.vec3.call(null,v,z,(0));
} else {
return thi.ng.xerror.core.type_error_BANG_.call(null,"Vec3",v);

}
}
}
});

thi.ng.geom.core.vector.vec3.cljs$core$IFn$_invoke$arity$3 = (function (x,y,z){
var b = (new Float32Array((3)));
(b[(0)] = x);

(b[(1)] = y);

(b[(2)] = z);

return (new thi.ng.geom.core.vector.Vec3(b,null,null));
});

thi.ng.geom.core.vector.vec3.cljs$lang$maxFixedArity = 3;
thi.ng.geom.core.vector.vec2_with_meta = (function thi$ng$geom$core$vector$vec2_with_meta(var_args){
var args20430 = [];
var len__17855__auto___20433 = arguments.length;
var i__17856__auto___20434 = (0);
while(true){
if((i__17856__auto___20434 < len__17855__auto___20433)){
args20430.push((arguments[i__17856__auto___20434]));

var G__20435 = (i__17856__auto___20434 + (1));
i__17856__auto___20434 = G__20435;
continue;
} else {
}
break;
}

var G__20432 = args20430.length;
switch (G__20432) {
case 2:
return thi.ng.geom.core.vector.vec2_with_meta.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return thi.ng.geom.core.vector.vec2_with_meta.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args20430.length)].join('')));

}
});

thi.ng.geom.core.vector.vec2_with_meta.cljs$core$IFn$_invoke$arity$2 = (function (v,meta){
if((v instanceof thi.ng.geom.core.vector.Vec2)){
return cljs.core.with_meta.call(null,v,meta);
} else {
if(typeof v === 'number'){
return thi.ng.geom.core.vector.vec2_with_meta.call(null,v,v,meta);
} else {
if(cljs.core.sequential_QMARK_.call(null,v)){
return thi.ng.geom.core.vector.vec2_with_meta.call(null,cljs.core.nth.call(null,v,(0),0.0),cljs.core.nth.call(null,v,(1),0.0),meta);
} else {
if(cljs.core.map_QMARK_.call(null,v)){
return thi.ng.geom.core.vector.vec2_with_meta.call(null,new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$2(v,0.0),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$2(v,0.0),meta);
} else {
return thi.ng.xerror.core.type_error_BANG_.call(null,"Vec2",v);

}
}
}
}
});

thi.ng.geom.core.vector.vec2_with_meta.cljs$core$IFn$_invoke$arity$3 = (function (x,y,meta){
var b = (new Float32Array((2)));
(b[(0)] = x);

(b[(1)] = y);

return (new thi.ng.geom.core.vector.Vec2(b,null,meta));
});

thi.ng.geom.core.vector.vec2_with_meta.cljs$lang$maxFixedArity = 3;
thi.ng.geom.core.vector.vec3_with_meta = (function thi$ng$geom$core$vector$vec3_with_meta(var_args){
var args20437 = [];
var len__17855__auto___20440 = arguments.length;
var i__17856__auto___20441 = (0);
while(true){
if((i__17856__auto___20441 < len__17855__auto___20440)){
args20437.push((arguments[i__17856__auto___20441]));

var G__20442 = (i__17856__auto___20441 + (1));
i__17856__auto___20441 = G__20442;
continue;
} else {
}
break;
}

var G__20439 = args20437.length;
switch (G__20439) {
case 2:
return thi.ng.geom.core.vector.vec3_with_meta.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return thi.ng.geom.core.vector.vec3_with_meta.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args20437.length)].join('')));

}
});

thi.ng.geom.core.vector.vec3_with_meta.cljs$core$IFn$_invoke$arity$2 = (function (v,meta){
if((v instanceof thi.ng.geom.core.vector.Vec3)){
return cljs.core.with_meta.call(null,v,meta);
} else {
if(typeof v === 'number'){
return thi.ng.geom.core.vector.vec3_with_meta.call(null,v,v,v,meta);
} else {
if(cljs.core.sequential_QMARK_.call(null,v)){
return thi.ng.geom.core.vector.vec3_with_meta.call(null,cljs.core.nth.call(null,v,(0),0.0),cljs.core.nth.call(null,v,(1),0.0),cljs.core.nth.call(null,v,(2),0.0),meta);
} else {
if(cljs.core.map_QMARK_.call(null,v)){
return thi.ng.geom.core.vector.vec3_with_meta.call(null,new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$2(v,0.0),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$2(v,0.0),new cljs.core.Keyword(null,"z","z",-789527183).cljs$core$IFn$_invoke$arity$2(v,0.0),meta);
} else {
return thi.ng.xerror.core.type_error_BANG_.call(null,"Vec3",v);

}
}
}
}
});

thi.ng.geom.core.vector.vec3_with_meta.cljs$core$IFn$_invoke$arity$4 = (function (x,y,z,meta){
var b = (new Float32Array((3)));
(b[(0)] = x);

(b[(1)] = y);

(b[(2)] = z);

return (new thi.ng.geom.core.vector.Vec3(b,null,meta));
});

thi.ng.geom.core.vector.vec3_with_meta.cljs$lang$maxFixedArity = 4;
thi.ng.geom.core.vector.vec2_QMARK_ = (function thi$ng$geom$core$vector$vec2_QMARK_(x){
return (x instanceof thi.ng.geom.core.vector.Vec2);
});
thi.ng.geom.core.vector.vec3_QMARK_ = (function thi$ng$geom$core$vector$vec3_QMARK_(x){
return (x instanceof thi.ng.geom.core.vector.Vec3);
});
thi.ng.geom.core.vector.V2X = thi.ng.geom.core.vector.vec2.call(null,(1),(0));
thi.ng.geom.core.vector.V2Y = thi.ng.geom.core.vector.vec2.call(null,(0),(1));
thi.ng.geom.core.vector.V3X = thi.ng.geom.core.vector.vec3.call(null,(1),(0),(0));
thi.ng.geom.core.vector.V3Y = thi.ng.geom.core.vector.vec3.call(null,(0),(1),(0));
thi.ng.geom.core.vector.V3Z = thi.ng.geom.core.vector.vec3.call(null,(0),(0),(1));
thi.ng.geom.core.vector.V2INF_ = thi.ng.geom.core.vector.vec2.call(null,thi.ng.math.core.INF_);
thi.ng.geom.core.vector.V2INF_PLUS_ = thi.ng.geom.core.vector.vec2.call(null,thi.ng.math.core.INF_PLUS_);
thi.ng.geom.core.vector.V3INF_ = thi.ng.geom.core.vector.vec3.call(null,thi.ng.math.core.INF_);
thi.ng.geom.core.vector.V3INF_PLUS_ = thi.ng.geom.core.vector.vec3.call(null,thi.ng.math.core.INF_PLUS_);
thi.ng.geom.core.vector.randvec2 = (function thi$ng$geom$core$vector$randvec2(var_args){
var args20444 = [];
var len__17855__auto___20447 = arguments.length;
var i__17856__auto___20448 = (0);
while(true){
if((i__17856__auto___20448 < len__17855__auto___20447)){
args20444.push((arguments[i__17856__auto___20448]));

var G__20449 = (i__17856__auto___20448 + (1));
i__17856__auto___20448 = G__20449;
continue;
} else {
}
break;
}

var G__20446 = args20444.length;
switch (G__20446) {
case 0:
return thi.ng.geom.core.vector.randvec2.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return thi.ng.geom.core.vector.randvec2.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args20444.length)].join('')));

}
});

thi.ng.geom.core.vector.randvec2.cljs$core$IFn$_invoke$arity$0 = (function (){
return thi.ng.geom.core.normalize.call(null,thi.ng.geom.core.vector.vec2.call(null,thi.ng.math.core.randnorm.call(null),thi.ng.math.core.randnorm.call(null)));
});

thi.ng.geom.core.vector.randvec2.cljs$core$IFn$_invoke$arity$1 = (function (n){
return thi.ng.geom.core.normalize.call(null,thi.ng.geom.core.vector.vec2.call(null,thi.ng.math.core.randnorm.call(null),thi.ng.math.core.randnorm.call(null)),n);
});

thi.ng.geom.core.vector.randvec2.cljs$lang$maxFixedArity = 1;
thi.ng.geom.core.vector.randvec3 = (function thi$ng$geom$core$vector$randvec3(var_args){
var args20451 = [];
var len__17855__auto___20454 = arguments.length;
var i__17856__auto___20455 = (0);
while(true){
if((i__17856__auto___20455 < len__17855__auto___20454)){
args20451.push((arguments[i__17856__auto___20455]));

var G__20456 = (i__17856__auto___20455 + (1));
i__17856__auto___20455 = G__20456;
continue;
} else {
}
break;
}

var G__20453 = args20451.length;
switch (G__20453) {
case 0:
return thi.ng.geom.core.vector.randvec3.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return thi.ng.geom.core.vector.randvec3.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args20451.length)].join('')));

}
});

thi.ng.geom.core.vector.randvec3.cljs$core$IFn$_invoke$arity$0 = (function (){
return thi.ng.geom.core.normalize.call(null,thi.ng.geom.core.vector.vec3.call(null,thi.ng.math.core.randnorm.call(null),thi.ng.math.core.randnorm.call(null),thi.ng.math.core.randnorm.call(null)));
});

thi.ng.geom.core.vector.randvec3.cljs$core$IFn$_invoke$arity$1 = (function (n){
return thi.ng.geom.core.normalize.call(null,thi.ng.geom.core.vector.vec3.call(null,thi.ng.math.core.randnorm.call(null),thi.ng.math.core.randnorm.call(null),thi.ng.math.core.randnorm.call(null)),n);
});

thi.ng.geom.core.vector.randvec3.cljs$lang$maxFixedArity = 1;

//# sourceMappingURL=vector.js.map?rel=1448506744156