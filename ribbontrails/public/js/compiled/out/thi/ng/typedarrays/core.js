// Compiled by ClojureScript 1.7.170 {}
goog.provide('thi.ng.typedarrays.core');
goog.require('cljs.core');
/**
 * Creates a native Int8Array of the given size or from `coll`.
 */
thi.ng.typedarrays.core.int8 = (function thi$ng$typedarrays$core$int8(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Int8Array(size_or_coll));
} else {
var len = cljs.core.count.call(null,size_or_coll);
var buf = (new Int8Array(len));
var i_20460 = (0);
var coll_20461 = size_or_coll;
while(true){
if((i_20460 < len)){
(buf[i_20460] = cljs.core.first.call(null,coll_20461));

var G__20462 = (i_20460 + (1));
var G__20463 = cljs.core.next.call(null,coll_20461);
i_20460 = G__20462;
coll_20461 = G__20463;
continue;
} else {
}
break;
}

return buf;
}
});
/**
 * Creates a native Uint8Array of the given size or from `coll`.
 */
thi.ng.typedarrays.core.uint8 = (function thi$ng$typedarrays$core$uint8(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Uint8Array(size_or_coll));
} else {
var len = cljs.core.count.call(null,size_or_coll);
var buf = (new Uint8Array(len));
var i_20464 = (0);
var coll_20465 = size_or_coll;
while(true){
if((i_20464 < len)){
(buf[i_20464] = cljs.core.first.call(null,coll_20465));

var G__20466 = (i_20464 + (1));
var G__20467 = cljs.core.next.call(null,coll_20465);
i_20464 = G__20466;
coll_20465 = G__20467;
continue;
} else {
}
break;
}

return buf;
}
});
/**
 * Creates a native Uint8ClampedArray of the given size or from `coll`.
 */
thi.ng.typedarrays.core.uint8_clamped = (function thi$ng$typedarrays$core$uint8_clamped(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Uint8ClampedArray(size_or_coll));
} else {
var len = cljs.core.count.call(null,size_or_coll);
var buf = (new Uint8ClampedArray(len));
var i_20468 = (0);
var coll_20469 = size_or_coll;
while(true){
if((i_20468 < len)){
(buf[i_20468] = cljs.core.first.call(null,coll_20469));

var G__20470 = (i_20468 + (1));
var G__20471 = cljs.core.next.call(null,coll_20469);
i_20468 = G__20470;
coll_20469 = G__20471;
continue;
} else {
}
break;
}

return buf;
}
});
/**
 * Creates a native Int16Array of the given size or from `coll`.
 */
thi.ng.typedarrays.core.int16 = (function thi$ng$typedarrays$core$int16(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Int16Array(size_or_coll));
} else {
var len = cljs.core.count.call(null,size_or_coll);
var buf = (new Int16Array(len));
var i_20472 = (0);
var coll_20473 = size_or_coll;
while(true){
if((i_20472 < len)){
(buf[i_20472] = cljs.core.first.call(null,coll_20473));

var G__20474 = (i_20472 + (1));
var G__20475 = cljs.core.next.call(null,coll_20473);
i_20472 = G__20474;
coll_20473 = G__20475;
continue;
} else {
}
break;
}

return buf;
}
});
/**
 * Creates a native Uint16Array of the given size or from `coll`.
 */
thi.ng.typedarrays.core.uint16 = (function thi$ng$typedarrays$core$uint16(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Uint16Array(size_or_coll));
} else {
var len = cljs.core.count.call(null,size_or_coll);
var buf = (new Uint16Array(len));
var i_20476 = (0);
var coll_20477 = size_or_coll;
while(true){
if((i_20476 < len)){
(buf[i_20476] = cljs.core.first.call(null,coll_20477));

var G__20478 = (i_20476 + (1));
var G__20479 = cljs.core.next.call(null,coll_20477);
i_20476 = G__20478;
coll_20477 = G__20479;
continue;
} else {
}
break;
}

return buf;
}
});
/**
 * Creates a native Int32Array of the given size or from `coll`.
 */
thi.ng.typedarrays.core.int32 = (function thi$ng$typedarrays$core$int32(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Int32Array(size_or_coll));
} else {
var len = cljs.core.count.call(null,size_or_coll);
var buf = (new Int32Array(len));
var i_20480 = (0);
var coll_20481 = size_or_coll;
while(true){
if((i_20480 < len)){
(buf[i_20480] = cljs.core.first.call(null,coll_20481));

var G__20482 = (i_20480 + (1));
var G__20483 = cljs.core.next.call(null,coll_20481);
i_20480 = G__20482;
coll_20481 = G__20483;
continue;
} else {
}
break;
}

return buf;
}
});
/**
 * Creates a native Uint32Array of the given size or from `coll`.
 */
thi.ng.typedarrays.core.uint32 = (function thi$ng$typedarrays$core$uint32(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Uint32Array(size_or_coll));
} else {
var len = cljs.core.count.call(null,size_or_coll);
var buf = (new Uint32Array(len));
var i_20484 = (0);
var coll_20485 = size_or_coll;
while(true){
if((i_20484 < len)){
(buf[i_20484] = cljs.core.first.call(null,coll_20485));

var G__20486 = (i_20484 + (1));
var G__20487 = cljs.core.next.call(null,coll_20485);
i_20484 = G__20486;
coll_20485 = G__20487;
continue;
} else {
}
break;
}

return buf;
}
});
/**
 * Creates a native Float32Array of the given size or from `coll`.
 */
thi.ng.typedarrays.core.float32 = (function thi$ng$typedarrays$core$float32(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Float32Array(size_or_coll));
} else {
var len = cljs.core.count.call(null,size_or_coll);
var buf = (new Float32Array(len));
var i_20488 = (0);
var coll_20489 = size_or_coll;
while(true){
if((i_20488 < len)){
(buf[i_20488] = cljs.core.first.call(null,coll_20489));

var G__20490 = (i_20488 + (1));
var G__20491 = cljs.core.next.call(null,coll_20489);
i_20488 = G__20490;
coll_20489 = G__20491;
continue;
} else {
}
break;
}

return buf;
}
});
/**
 * Creates a native Float64Array of the given size or from `coll`.
 */
thi.ng.typedarrays.core.float64 = (function thi$ng$typedarrays$core$float64(size_or_coll){
if(typeof size_or_coll === 'number'){
return (new Float64Array(size_or_coll));
} else {
var len = cljs.core.count.call(null,size_or_coll);
var buf = (new Float64Array(len));
var i_20492 = (0);
var coll_20493 = size_or_coll;
while(true){
if((i_20492 < len)){
(buf[i_20492] = cljs.core.first.call(null,coll_20493));

var G__20494 = (i_20492 + (1));
var G__20495 = cljs.core.next.call(null,coll_20493);
i_20492 = G__20494;
coll_20493 = G__20495;
continue;
} else {
}
break;
}

return buf;
}
});
/**
 * Returns true if JS runtime supports typed arrays
 */
thi.ng.typedarrays.core.typed_arrays_supported_QMARK_ = (function thi$ng$typedarrays$core$typed_arrays_supported_QMARK_(){
return !(((window["ArrayBuffer"]) == null));
});
thi.ng.typedarrays.core.array_types = cljs.core.PersistentHashMap.fromArrays(["[object Float64Array]","[object Int8Array]","[object Int16Array]","[object Uint8Array]","[object Uint16Array]","[object Uint8ClampedArray]","[object Uint32Array]","[object Float32Array]","[object Int32Array]"],[new cljs.core.Keyword(null,"float64","float64",1881838306),new cljs.core.Keyword(null,"int8","int8",-1834023920),new cljs.core.Keyword(null,"int16","int16",-188764863),new cljs.core.Keyword(null,"uint8","uint8",956521151),new cljs.core.Keyword(null,"uint16","uint16",-588869202),new cljs.core.Keyword(null,"uint8-clamped","uint8-clamped",1439331936),new cljs.core.Keyword(null,"uint32","uint32",-418789486),new cljs.core.Keyword(null,"float32","float32",-2119815775),new cljs.core.Keyword(null,"int32","int32",1718804896)]);
thi.ng.typedarrays.core.array_type = (function thi$ng$typedarrays$core$array_type(x){
if(cljs.core.array_QMARK_.call(null,x)){
return new cljs.core.Keyword(null,"array","array",-2080713842);
} else {
return thi.ng.typedarrays.core.array_types.call(null,[cljs.core.str(x)].join(''));
}
});
/**
 * Returns truthy value if the given arg is a typed array instance
 */
thi.ng.typedarrays.core.typed_array_QMARK_ = (function thi$ng$typedarrays$core$typed_array_QMARK_(x){
if(cljs.core.sequential_QMARK_.call(null,x)){
return false;
} else {
if(typeof x === 'number'){
return false;
} else {
return thi.ng.typedarrays.core.array_types.call(null,[cljs.core.str(x)].join(''));

}
}
});

//# sourceMappingURL=core.js.map?rel=1448506744348