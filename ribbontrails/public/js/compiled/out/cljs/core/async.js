// Compiled by ClojureScript 1.7.170 {}
goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.ioc_helpers');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var args21611 = [];
var len__17855__auto___21617 = arguments.length;
var i__17856__auto___21618 = (0);
while(true){
if((i__17856__auto___21618 < len__17855__auto___21617)){
args21611.push((arguments[i__17856__auto___21618]));

var G__21619 = (i__17856__auto___21618 + (1));
i__17856__auto___21618 = G__21619;
continue;
} else {
}
break;
}

var G__21613 = args21611.length;
switch (G__21613) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args21611.length)].join('')));

}
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.call(null,f,true);
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if(typeof cljs.core.async.t_cljs$core$async21614 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async21614 = (function (f,blockable,meta21615){
this.f = f;
this.blockable = blockable;
this.meta21615 = meta21615;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async21614.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_21616,meta21615__$1){
var self__ = this;
var _21616__$1 = this;
return (new cljs.core.async.t_cljs$core$async21614(self__.f,self__.blockable,meta21615__$1));
});

cljs.core.async.t_cljs$core$async21614.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_21616){
var self__ = this;
var _21616__$1 = this;
return self__.meta21615;
});

cljs.core.async.t_cljs$core$async21614.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t_cljs$core$async21614.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async21614.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
});

cljs.core.async.t_cljs$core$async21614.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
});

cljs.core.async.t_cljs$core$async21614.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta21615","meta21615",-197523873,null)], null);
});

cljs.core.async.t_cljs$core$async21614.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async21614.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async21614";

cljs.core.async.t_cljs$core$async21614.cljs$lang$ctorPrWriter = (function (this__17394__auto__,writer__17395__auto__,opt__17396__auto__){
return cljs.core._write.call(null,writer__17395__auto__,"cljs.core.async/t_cljs$core$async21614");
});

cljs.core.async.__GT_t_cljs$core$async21614 = (function cljs$core$async$__GT_t_cljs$core$async21614(f__$1,blockable__$1,meta21615){
return (new cljs.core.async.t_cljs$core$async21614(f__$1,blockable__$1,meta21615));
});

}

return (new cljs.core.async.t_cljs$core$async21614(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
});

cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2;
/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer.call(null,n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if(!((buff == null))){
if((false) || (buff.cljs$core$async$impl$protocols$UnblockingBuffer$)){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var args21623 = [];
var len__17855__auto___21626 = arguments.length;
var i__17856__auto___21627 = (0);
while(true){
if((i__17856__auto___21627 < len__17855__auto___21626)){
args21623.push((arguments[i__17856__auto___21627]));

var G__21628 = (i__17856__auto___21627 + (1));
i__17856__auto___21627 = G__21628;
continue;
} else {
}
break;
}

var G__21625 = args21623.length;
switch (G__21625) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args21623.length)].join('')));

}
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.call(null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.call(null,buf_or_n,null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.call(null,buf_or_n,xform,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.call(null,buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str("buffer must be supplied when transducer is"),cljs.core.str("\n"),cljs.core.str(cljs.core.pr_str.call(null,new cljs.core.Symbol(null,"buf-or-n","buf-or-n",-1646815050,null)))].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.call(null,((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer.call(null,buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
});

cljs.core.async.chan.cljs$lang$maxFixedArity = 3;
/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var args21630 = [];
var len__17855__auto___21633 = arguments.length;
var i__17856__auto___21634 = (0);
while(true){
if((i__17856__auto___21634 < len__17855__auto___21633)){
args21630.push((arguments[i__17856__auto___21634]));

var G__21635 = (i__17856__auto___21634 + (1));
i__17856__auto___21634 = G__21635;
continue;
} else {
}
break;
}

var G__21632 = args21630.length;
switch (G__21632) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args21630.length)].join('')));

}
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.call(null,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.call(null,xform,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.call(null,cljs.core.async.impl.buffers.promise_buffer.call(null),xform,ex_handler);
});

cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2;
/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout.call(null,msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var args21637 = [];
var len__17855__auto___21640 = arguments.length;
var i__17856__auto___21641 = (0);
while(true){
if((i__17856__auto___21641 < len__17855__auto___21640)){
args21637.push((arguments[i__17856__auto___21641]));

var G__21642 = (i__17856__auto___21641 + (1));
i__17856__auto___21641 = G__21642;
continue;
} else {
}
break;
}

var G__21639 = args21637.length;
switch (G__21639) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args21637.length)].join('')));

}
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.call(null,port,fn1,true);
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(ret)){
var val_21644 = cljs.core.deref.call(null,ret);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,val_21644);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (val_21644,ret){
return (function (){
return fn1.call(null,val_21644);
});})(val_21644,ret))
);
}
} else {
}

return null;
});

cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3;
cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.call(null,cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn0 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn0 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var args21645 = [];
var len__17855__auto___21648 = arguments.length;
var i__17856__auto___21649 = (0);
while(true){
if((i__17856__auto___21649 < len__17855__auto___21648)){
args21645.push((arguments[i__17856__auto___21649]));

var G__21650 = (i__17856__auto___21649 + (1));
i__17856__auto___21649 = G__21650;
continue;
} else {
}
break;
}

var G__21647 = args21645.length;
switch (G__21647) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args21645.length)].join('')));

}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__4423__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__4423__auto__)){
var ret = temp__4423__auto__;
return cljs.core.deref.call(null,ret);
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.call(null,port,val,fn1,true);
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__4423__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(temp__4423__auto__)){
var retb = temp__4423__auto__;
var ret = cljs.core.deref.call(null,retb);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,ret);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (ret,retb,temp__4423__auto__){
return (function (){
return fn1.call(null,ret);
});})(ret,retb,temp__4423__auto__))
);
}

return ret;
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4;
cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_.call(null,port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__17700__auto___21652 = n;
var x_21653 = (0);
while(true){
if((x_21653 < n__17700__auto___21652)){
(a[x_21653] = (0));

var G__21654 = (x_21653 + (1));
x_21653 = G__21654;
continue;
} else {
}
break;
}

var i = (1);
while(true){
if(cljs.core._EQ_.call(null,i,n)){
return a;
} else {
var j = cljs.core.rand_int.call(null,i);
(a[i] = (a[j]));

(a[j] = i);

var G__21655 = (i + (1));
i = G__21655;
continue;
}
break;
}
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.call(null,true);
if(typeof cljs.core.async.t_cljs$core$async21659 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async21659 = (function (alt_flag,flag,meta21660){
this.alt_flag = alt_flag;
this.flag = flag;
this.meta21660 = meta21660;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async21659.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (flag){
return (function (_21661,meta21660__$1){
var self__ = this;
var _21661__$1 = this;
return (new cljs.core.async.t_cljs$core$async21659(self__.alt_flag,self__.flag,meta21660__$1));
});})(flag))
;

cljs.core.async.t_cljs$core$async21659.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (flag){
return (function (_21661){
var self__ = this;
var _21661__$1 = this;
return self__.meta21660;
});})(flag))
;

cljs.core.async.t_cljs$core$async21659.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t_cljs$core$async21659.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref.call(null,self__.flag);
});})(flag))
;

cljs.core.async.t_cljs$core$async21659.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async21659.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.flag,null);

return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async21659.getBasis = ((function (flag){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"alt-flag","alt-flag",-1794972754,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"private","private",-558947994),true,new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(cljs.core.PersistentVector.EMPTY))], null)),new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta21660","meta21660",-1643325035,null)], null);
});})(flag))
;

cljs.core.async.t_cljs$core$async21659.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async21659.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async21659";

cljs.core.async.t_cljs$core$async21659.cljs$lang$ctorPrWriter = ((function (flag){
return (function (this__17394__auto__,writer__17395__auto__,opt__17396__auto__){
return cljs.core._write.call(null,writer__17395__auto__,"cljs.core.async/t_cljs$core$async21659");
});})(flag))
;

cljs.core.async.__GT_t_cljs$core$async21659 = ((function (flag){
return (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async21659(alt_flag__$1,flag__$1,meta21660){
return (new cljs.core.async.t_cljs$core$async21659(alt_flag__$1,flag__$1,meta21660));
});})(flag))
;

}

return (new cljs.core.async.t_cljs$core$async21659(cljs$core$async$alt_flag,flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if(typeof cljs.core.async.t_cljs$core$async21665 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async21665 = (function (alt_handler,flag,cb,meta21666){
this.alt_handler = alt_handler;
this.flag = flag;
this.cb = cb;
this.meta21666 = meta21666;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async21665.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_21667,meta21666__$1){
var self__ = this;
var _21667__$1 = this;
return (new cljs.core.async.t_cljs$core$async21665(self__.alt_handler,self__.flag,self__.cb,meta21666__$1));
});

cljs.core.async.t_cljs$core$async21665.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_21667){
var self__ = this;
var _21667__$1 = this;
return self__.meta21666;
});

cljs.core.async.t_cljs$core$async21665.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t_cljs$core$async21665.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.flag);
});

cljs.core.async.t_cljs$core$async21665.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async21665.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit.call(null,self__.flag);

return self__.cb;
});

cljs.core.async.t_cljs$core$async21665.getBasis = (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"alt-handler","alt-handler",963786170,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"private","private",-558947994),true,new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null)], null)))], null)),new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta21666","meta21666",-850058858,null)], null);
});

cljs.core.async.t_cljs$core$async21665.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async21665.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async21665";

cljs.core.async.t_cljs$core$async21665.cljs$lang$ctorPrWriter = (function (this__17394__auto__,writer__17395__auto__,opt__17396__auto__){
return cljs.core._write.call(null,writer__17395__auto__,"cljs.core.async/t_cljs$core$async21665");
});

cljs.core.async.__GT_t_cljs$core$async21665 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async21665(alt_handler__$1,flag__$1,cb__$1,meta21666){
return (new cljs.core.async.t_cljs$core$async21665(alt_handler__$1,flag__$1,cb__$1,meta21666));
});

}

return (new cljs.core.async.t_cljs$core$async21665(cljs$core$async$alt_handler,flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
var flag = cljs.core.async.alt_flag.call(null);
var n = cljs.core.count.call(null,ports);
var idxs = cljs.core.async.random_array.call(null,n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.call(null,ports,idx);
var wport = ((cljs.core.vector_QMARK_.call(null,port))?port.call(null,(0)):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = port.call(null,(1));
return cljs.core.async.impl.protocols.put_BANG_.call(null,wport,val,cljs.core.async.alt_handler.call(null,flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__21668_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__21668_SHARP_,wport], null));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.alt_handler.call(null,flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__21669_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__21669_SHARP_,port], null));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref.call(null,vbox),(function (){var or__16796__auto__ = wport;
if(cljs.core.truth_(or__16796__auto__)){
return or__16796__auto__;
} else {
return port;
}
})()], null));
} else {
var G__21670 = (i + (1));
i = G__21670;
continue;
}
} else {
return null;
}
break;
}
})();
var or__16796__auto__ = ret;
if(cljs.core.truth_(or__16796__auto__)){
return or__16796__auto__;
} else {
if(cljs.core.contains_QMARK_.call(null,opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__4425__auto__ = (function (){var and__16784__auto__ = cljs.core.async.impl.protocols.active_QMARK_.call(null,flag);
if(cljs.core.truth_(and__16784__auto__)){
return cljs.core.async.impl.protocols.commit.call(null,flag);
} else {
return and__16784__auto__;
}
})();
if(cljs.core.truth_(temp__4425__auto__)){
var got = temp__4425__auto__;
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__17862__auto__ = [];
var len__17855__auto___21676 = arguments.length;
var i__17856__auto___21677 = (0);
while(true){
if((i__17856__auto___21677 < len__17855__auto___21676)){
args__17862__auto__.push((arguments[i__17856__auto___21677]));

var G__21678 = (i__17856__auto___21677 + (1));
i__17856__auto___21677 = G__21678;
continue;
} else {
}
break;
}

var argseq__17863__auto__ = ((((1) < args__17862__auto__.length))?(new cljs.core.IndexedSeq(args__17862__auto__.slice((1)),(0))):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__17863__auto__);
});

cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__21673){
var map__21674 = p__21673;
var map__21674__$1 = ((((!((map__21674 == null)))?((((map__21674.cljs$lang$protocol_mask$partition0$ & (64))) || (map__21674.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__21674):map__21674);
var opts = map__21674__$1;
throw (new Error("alts! used not in (go ...) block"));
});

cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1);

cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq21671){
var G__21672 = cljs.core.first.call(null,seq21671);
var seq21671__$1 = cljs.core.next.call(null,seq21671);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__21672,seq21671__$1);
});
/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref.call(null,ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref.call(null,ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var args21679 = [];
var len__17855__auto___21729 = arguments.length;
var i__17856__auto___21730 = (0);
while(true){
if((i__17856__auto___21730 < len__17855__auto___21729)){
args21679.push((arguments[i__17856__auto___21730]));

var G__21731 = (i__17856__auto___21730 + (1));
i__17856__auto___21730 = G__21731;
continue;
} else {
}
break;
}

var G__21681 = args21679.length;
switch (G__21681) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args21679.length)].join('')));

}
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.call(null,from,to,true);
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__21566__auto___21733 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___21733){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___21733){
return (function (state_21705){
var state_val_21706 = (state_21705[(1)]);
if((state_val_21706 === (7))){
var inst_21701 = (state_21705[(2)]);
var state_21705__$1 = state_21705;
var statearr_21707_21734 = state_21705__$1;
(statearr_21707_21734[(2)] = inst_21701);

(statearr_21707_21734[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21706 === (1))){
var state_21705__$1 = state_21705;
var statearr_21708_21735 = state_21705__$1;
(statearr_21708_21735[(2)] = null);

(statearr_21708_21735[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21706 === (4))){
var inst_21684 = (state_21705[(7)]);
var inst_21684__$1 = (state_21705[(2)]);
var inst_21685 = (inst_21684__$1 == null);
var state_21705__$1 = (function (){var statearr_21709 = state_21705;
(statearr_21709[(7)] = inst_21684__$1);

return statearr_21709;
})();
if(cljs.core.truth_(inst_21685)){
var statearr_21710_21736 = state_21705__$1;
(statearr_21710_21736[(1)] = (5));

} else {
var statearr_21711_21737 = state_21705__$1;
(statearr_21711_21737[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21706 === (13))){
var state_21705__$1 = state_21705;
var statearr_21712_21738 = state_21705__$1;
(statearr_21712_21738[(2)] = null);

(statearr_21712_21738[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21706 === (6))){
var inst_21684 = (state_21705[(7)]);
var state_21705__$1 = state_21705;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_21705__$1,(11),to,inst_21684);
} else {
if((state_val_21706 === (3))){
var inst_21703 = (state_21705[(2)]);
var state_21705__$1 = state_21705;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_21705__$1,inst_21703);
} else {
if((state_val_21706 === (12))){
var state_21705__$1 = state_21705;
var statearr_21713_21739 = state_21705__$1;
(statearr_21713_21739[(2)] = null);

(statearr_21713_21739[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21706 === (2))){
var state_21705__$1 = state_21705;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_21705__$1,(4),from);
} else {
if((state_val_21706 === (11))){
var inst_21694 = (state_21705[(2)]);
var state_21705__$1 = state_21705;
if(cljs.core.truth_(inst_21694)){
var statearr_21714_21740 = state_21705__$1;
(statearr_21714_21740[(1)] = (12));

} else {
var statearr_21715_21741 = state_21705__$1;
(statearr_21715_21741[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21706 === (9))){
var state_21705__$1 = state_21705;
var statearr_21716_21742 = state_21705__$1;
(statearr_21716_21742[(2)] = null);

(statearr_21716_21742[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21706 === (5))){
var state_21705__$1 = state_21705;
if(cljs.core.truth_(close_QMARK_)){
var statearr_21717_21743 = state_21705__$1;
(statearr_21717_21743[(1)] = (8));

} else {
var statearr_21718_21744 = state_21705__$1;
(statearr_21718_21744[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21706 === (14))){
var inst_21699 = (state_21705[(2)]);
var state_21705__$1 = state_21705;
var statearr_21719_21745 = state_21705__$1;
(statearr_21719_21745[(2)] = inst_21699);

(statearr_21719_21745[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21706 === (10))){
var inst_21691 = (state_21705[(2)]);
var state_21705__$1 = state_21705;
var statearr_21720_21746 = state_21705__$1;
(statearr_21720_21746[(2)] = inst_21691);

(statearr_21720_21746[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21706 === (8))){
var inst_21688 = cljs.core.async.close_BANG_.call(null,to);
var state_21705__$1 = state_21705;
var statearr_21721_21747 = state_21705__$1;
(statearr_21721_21747[(2)] = inst_21688);

(statearr_21721_21747[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto___21733))
;
return ((function (switch__21501__auto__,c__21566__auto___21733){
return (function() {
var cljs$core$async$state_machine__21502__auto__ = null;
var cljs$core$async$state_machine__21502__auto____0 = (function (){
var statearr_21725 = [null,null,null,null,null,null,null,null];
(statearr_21725[(0)] = cljs$core$async$state_machine__21502__auto__);

(statearr_21725[(1)] = (1));

return statearr_21725;
});
var cljs$core$async$state_machine__21502__auto____1 = (function (state_21705){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_21705);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e21726){if((e21726 instanceof Object)){
var ex__21505__auto__ = e21726;
var statearr_21727_21748 = state_21705;
(statearr_21727_21748[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_21705);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e21726;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__21749 = state_21705;
state_21705 = G__21749;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$state_machine__21502__auto__ = function(state_21705){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21502__auto____1.call(this,state_21705);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21502__auto____0;
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21502__auto____1;
return cljs$core$async$state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___21733))
})();
var state__21568__auto__ = (function (){var statearr_21728 = f__21567__auto__.call(null);
(statearr_21728[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___21733);

return statearr_21728;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___21733))
);


return to;
});

cljs.core.async.pipe.cljs$lang$maxFixedArity = 3;
cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"pos?","pos?",-244377722,null),new cljs.core.Symbol(null,"n","n",-2092305744,null))))].join('')));
}

var jobs = cljs.core.async.chan.call(null,n);
var results = cljs.core.async.chan.call(null,n);
var process = ((function (jobs,results){
return (function (p__21933){
var vec__21934 = p__21933;
var v = cljs.core.nth.call(null,vec__21934,(0),null);
var p = cljs.core.nth.call(null,vec__21934,(1),null);
var job = vec__21934;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1),xf,ex_handler);
var c__21566__auto___22116 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___22116,res,vec__21934,v,p,job,jobs,results){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___22116,res,vec__21934,v,p,job,jobs,results){
return (function (state_21939){
var state_val_21940 = (state_21939[(1)]);
if((state_val_21940 === (1))){
var state_21939__$1 = state_21939;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_21939__$1,(2),res,v);
} else {
if((state_val_21940 === (2))){
var inst_21936 = (state_21939[(2)]);
var inst_21937 = cljs.core.async.close_BANG_.call(null,res);
var state_21939__$1 = (function (){var statearr_21941 = state_21939;
(statearr_21941[(7)] = inst_21936);

return statearr_21941;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_21939__$1,inst_21937);
} else {
return null;
}
}
});})(c__21566__auto___22116,res,vec__21934,v,p,job,jobs,results))
;
return ((function (switch__21501__auto__,c__21566__auto___22116,res,vec__21934,v,p,job,jobs,results){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____0 = (function (){
var statearr_21945 = [null,null,null,null,null,null,null,null];
(statearr_21945[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__);

(statearr_21945[(1)] = (1));

return statearr_21945;
});
var cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____1 = (function (state_21939){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_21939);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e21946){if((e21946 instanceof Object)){
var ex__21505__auto__ = e21946;
var statearr_21947_22117 = state_21939;
(statearr_21947_22117[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_21939);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e21946;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__22118 = state_21939;
state_21939 = G__22118;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__ = function(state_21939){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____1.call(this,state_21939);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___22116,res,vec__21934,v,p,job,jobs,results))
})();
var state__21568__auto__ = (function (){var statearr_21948 = f__21567__auto__.call(null);
(statearr_21948[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___22116);

return statearr_21948;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___22116,res,vec__21934,v,p,job,jobs,results))
);


cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results))
;
var async = ((function (jobs,results,process){
return (function (p__21949){
var vec__21950 = p__21949;
var v = cljs.core.nth.call(null,vec__21950,(0),null);
var p = cljs.core.nth.call(null,vec__21950,(1),null);
var job = vec__21950;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1));
xf.call(null,v,res);

cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results,process))
;
var n__17700__auto___22119 = n;
var __22120 = (0);
while(true){
if((__22120 < n__17700__auto___22119)){
var G__21951_22121 = (((type instanceof cljs.core.Keyword))?type.fqn:null);
switch (G__21951_22121) {
case "compute":
var c__21566__auto___22123 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__22120,c__21566__auto___22123,G__21951_22121,n__17700__auto___22119,jobs,results,process,async){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (__22120,c__21566__auto___22123,G__21951_22121,n__17700__auto___22119,jobs,results,process,async){
return (function (state_21964){
var state_val_21965 = (state_21964[(1)]);
if((state_val_21965 === (1))){
var state_21964__$1 = state_21964;
var statearr_21966_22124 = state_21964__$1;
(statearr_21966_22124[(2)] = null);

(statearr_21966_22124[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21965 === (2))){
var state_21964__$1 = state_21964;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_21964__$1,(4),jobs);
} else {
if((state_val_21965 === (3))){
var inst_21962 = (state_21964[(2)]);
var state_21964__$1 = state_21964;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_21964__$1,inst_21962);
} else {
if((state_val_21965 === (4))){
var inst_21954 = (state_21964[(2)]);
var inst_21955 = process.call(null,inst_21954);
var state_21964__$1 = state_21964;
if(cljs.core.truth_(inst_21955)){
var statearr_21967_22125 = state_21964__$1;
(statearr_21967_22125[(1)] = (5));

} else {
var statearr_21968_22126 = state_21964__$1;
(statearr_21968_22126[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21965 === (5))){
var state_21964__$1 = state_21964;
var statearr_21969_22127 = state_21964__$1;
(statearr_21969_22127[(2)] = null);

(statearr_21969_22127[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21965 === (6))){
var state_21964__$1 = state_21964;
var statearr_21970_22128 = state_21964__$1;
(statearr_21970_22128[(2)] = null);

(statearr_21970_22128[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21965 === (7))){
var inst_21960 = (state_21964[(2)]);
var state_21964__$1 = state_21964;
var statearr_21971_22129 = state_21964__$1;
(statearr_21971_22129[(2)] = inst_21960);

(statearr_21971_22129[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__22120,c__21566__auto___22123,G__21951_22121,n__17700__auto___22119,jobs,results,process,async))
;
return ((function (__22120,switch__21501__auto__,c__21566__auto___22123,G__21951_22121,n__17700__auto___22119,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____0 = (function (){
var statearr_21975 = [null,null,null,null,null,null,null];
(statearr_21975[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__);

(statearr_21975[(1)] = (1));

return statearr_21975;
});
var cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____1 = (function (state_21964){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_21964);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e21976){if((e21976 instanceof Object)){
var ex__21505__auto__ = e21976;
var statearr_21977_22130 = state_21964;
(statearr_21977_22130[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_21964);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e21976;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__22131 = state_21964;
state_21964 = G__22131;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__ = function(state_21964){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____1.call(this,state_21964);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__;
})()
;})(__22120,switch__21501__auto__,c__21566__auto___22123,G__21951_22121,n__17700__auto___22119,jobs,results,process,async))
})();
var state__21568__auto__ = (function (){var statearr_21978 = f__21567__auto__.call(null);
(statearr_21978[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___22123);

return statearr_21978;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(__22120,c__21566__auto___22123,G__21951_22121,n__17700__auto___22119,jobs,results,process,async))
);


break;
case "async":
var c__21566__auto___22132 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__22120,c__21566__auto___22132,G__21951_22121,n__17700__auto___22119,jobs,results,process,async){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (__22120,c__21566__auto___22132,G__21951_22121,n__17700__auto___22119,jobs,results,process,async){
return (function (state_21991){
var state_val_21992 = (state_21991[(1)]);
if((state_val_21992 === (1))){
var state_21991__$1 = state_21991;
var statearr_21993_22133 = state_21991__$1;
(statearr_21993_22133[(2)] = null);

(statearr_21993_22133[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21992 === (2))){
var state_21991__$1 = state_21991;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_21991__$1,(4),jobs);
} else {
if((state_val_21992 === (3))){
var inst_21989 = (state_21991[(2)]);
var state_21991__$1 = state_21991;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_21991__$1,inst_21989);
} else {
if((state_val_21992 === (4))){
var inst_21981 = (state_21991[(2)]);
var inst_21982 = async.call(null,inst_21981);
var state_21991__$1 = state_21991;
if(cljs.core.truth_(inst_21982)){
var statearr_21994_22134 = state_21991__$1;
(statearr_21994_22134[(1)] = (5));

} else {
var statearr_21995_22135 = state_21991__$1;
(statearr_21995_22135[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21992 === (5))){
var state_21991__$1 = state_21991;
var statearr_21996_22136 = state_21991__$1;
(statearr_21996_22136[(2)] = null);

(statearr_21996_22136[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21992 === (6))){
var state_21991__$1 = state_21991;
var statearr_21997_22137 = state_21991__$1;
(statearr_21997_22137[(2)] = null);

(statearr_21997_22137[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21992 === (7))){
var inst_21987 = (state_21991[(2)]);
var state_21991__$1 = state_21991;
var statearr_21998_22138 = state_21991__$1;
(statearr_21998_22138[(2)] = inst_21987);

(statearr_21998_22138[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__22120,c__21566__auto___22132,G__21951_22121,n__17700__auto___22119,jobs,results,process,async))
;
return ((function (__22120,switch__21501__auto__,c__21566__auto___22132,G__21951_22121,n__17700__auto___22119,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____0 = (function (){
var statearr_22002 = [null,null,null,null,null,null,null];
(statearr_22002[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__);

(statearr_22002[(1)] = (1));

return statearr_22002;
});
var cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____1 = (function (state_21991){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_21991);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e22003){if((e22003 instanceof Object)){
var ex__21505__auto__ = e22003;
var statearr_22004_22139 = state_21991;
(statearr_22004_22139[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_21991);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e22003;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__22140 = state_21991;
state_21991 = G__22140;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__ = function(state_21991){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____1.call(this,state_21991);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__;
})()
;})(__22120,switch__21501__auto__,c__21566__auto___22132,G__21951_22121,n__17700__auto___22119,jobs,results,process,async))
})();
var state__21568__auto__ = (function (){var statearr_22005 = f__21567__auto__.call(null);
(statearr_22005[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___22132);

return statearr_22005;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(__22120,c__21566__auto___22132,G__21951_22121,n__17700__auto___22119,jobs,results,process,async))
);


break;
default:
throw (new Error([cljs.core.str("No matching clause: "),cljs.core.str(type)].join('')));

}

var G__22141 = (__22120 + (1));
__22120 = G__22141;
continue;
} else {
}
break;
}

var c__21566__auto___22142 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___22142,jobs,results,process,async){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___22142,jobs,results,process,async){
return (function (state_22027){
var state_val_22028 = (state_22027[(1)]);
if((state_val_22028 === (1))){
var state_22027__$1 = state_22027;
var statearr_22029_22143 = state_22027__$1;
(statearr_22029_22143[(2)] = null);

(statearr_22029_22143[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22028 === (2))){
var state_22027__$1 = state_22027;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_22027__$1,(4),from);
} else {
if((state_val_22028 === (3))){
var inst_22025 = (state_22027[(2)]);
var state_22027__$1 = state_22027;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_22027__$1,inst_22025);
} else {
if((state_val_22028 === (4))){
var inst_22008 = (state_22027[(7)]);
var inst_22008__$1 = (state_22027[(2)]);
var inst_22009 = (inst_22008__$1 == null);
var state_22027__$1 = (function (){var statearr_22030 = state_22027;
(statearr_22030[(7)] = inst_22008__$1);

return statearr_22030;
})();
if(cljs.core.truth_(inst_22009)){
var statearr_22031_22144 = state_22027__$1;
(statearr_22031_22144[(1)] = (5));

} else {
var statearr_22032_22145 = state_22027__$1;
(statearr_22032_22145[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22028 === (5))){
var inst_22011 = cljs.core.async.close_BANG_.call(null,jobs);
var state_22027__$1 = state_22027;
var statearr_22033_22146 = state_22027__$1;
(statearr_22033_22146[(2)] = inst_22011);

(statearr_22033_22146[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22028 === (6))){
var inst_22013 = (state_22027[(8)]);
var inst_22008 = (state_22027[(7)]);
var inst_22013__$1 = cljs.core.async.chan.call(null,(1));
var inst_22014 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_22015 = [inst_22008,inst_22013__$1];
var inst_22016 = (new cljs.core.PersistentVector(null,2,(5),inst_22014,inst_22015,null));
var state_22027__$1 = (function (){var statearr_22034 = state_22027;
(statearr_22034[(8)] = inst_22013__$1);

return statearr_22034;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_22027__$1,(8),jobs,inst_22016);
} else {
if((state_val_22028 === (7))){
var inst_22023 = (state_22027[(2)]);
var state_22027__$1 = state_22027;
var statearr_22035_22147 = state_22027__$1;
(statearr_22035_22147[(2)] = inst_22023);

(statearr_22035_22147[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22028 === (8))){
var inst_22013 = (state_22027[(8)]);
var inst_22018 = (state_22027[(2)]);
var state_22027__$1 = (function (){var statearr_22036 = state_22027;
(statearr_22036[(9)] = inst_22018);

return statearr_22036;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_22027__$1,(9),results,inst_22013);
} else {
if((state_val_22028 === (9))){
var inst_22020 = (state_22027[(2)]);
var state_22027__$1 = (function (){var statearr_22037 = state_22027;
(statearr_22037[(10)] = inst_22020);

return statearr_22037;
})();
var statearr_22038_22148 = state_22027__$1;
(statearr_22038_22148[(2)] = null);

(statearr_22038_22148[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});})(c__21566__auto___22142,jobs,results,process,async))
;
return ((function (switch__21501__auto__,c__21566__auto___22142,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____0 = (function (){
var statearr_22042 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_22042[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__);

(statearr_22042[(1)] = (1));

return statearr_22042;
});
var cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____1 = (function (state_22027){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_22027);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e22043){if((e22043 instanceof Object)){
var ex__21505__auto__ = e22043;
var statearr_22044_22149 = state_22027;
(statearr_22044_22149[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_22027);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e22043;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__22150 = state_22027;
state_22027 = G__22150;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__ = function(state_22027){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____1.call(this,state_22027);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___22142,jobs,results,process,async))
})();
var state__21568__auto__ = (function (){var statearr_22045 = f__21567__auto__.call(null);
(statearr_22045[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___22142);

return statearr_22045;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___22142,jobs,results,process,async))
);


var c__21566__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto__,jobs,results,process,async){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto__,jobs,results,process,async){
return (function (state_22083){
var state_val_22084 = (state_22083[(1)]);
if((state_val_22084 === (7))){
var inst_22079 = (state_22083[(2)]);
var state_22083__$1 = state_22083;
var statearr_22085_22151 = state_22083__$1;
(statearr_22085_22151[(2)] = inst_22079);

(statearr_22085_22151[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22084 === (20))){
var state_22083__$1 = state_22083;
var statearr_22086_22152 = state_22083__$1;
(statearr_22086_22152[(2)] = null);

(statearr_22086_22152[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22084 === (1))){
var state_22083__$1 = state_22083;
var statearr_22087_22153 = state_22083__$1;
(statearr_22087_22153[(2)] = null);

(statearr_22087_22153[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22084 === (4))){
var inst_22048 = (state_22083[(7)]);
var inst_22048__$1 = (state_22083[(2)]);
var inst_22049 = (inst_22048__$1 == null);
var state_22083__$1 = (function (){var statearr_22088 = state_22083;
(statearr_22088[(7)] = inst_22048__$1);

return statearr_22088;
})();
if(cljs.core.truth_(inst_22049)){
var statearr_22089_22154 = state_22083__$1;
(statearr_22089_22154[(1)] = (5));

} else {
var statearr_22090_22155 = state_22083__$1;
(statearr_22090_22155[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22084 === (15))){
var inst_22061 = (state_22083[(8)]);
var state_22083__$1 = state_22083;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_22083__$1,(18),to,inst_22061);
} else {
if((state_val_22084 === (21))){
var inst_22074 = (state_22083[(2)]);
var state_22083__$1 = state_22083;
var statearr_22091_22156 = state_22083__$1;
(statearr_22091_22156[(2)] = inst_22074);

(statearr_22091_22156[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22084 === (13))){
var inst_22076 = (state_22083[(2)]);
var state_22083__$1 = (function (){var statearr_22092 = state_22083;
(statearr_22092[(9)] = inst_22076);

return statearr_22092;
})();
var statearr_22093_22157 = state_22083__$1;
(statearr_22093_22157[(2)] = null);

(statearr_22093_22157[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22084 === (6))){
var inst_22048 = (state_22083[(7)]);
var state_22083__$1 = state_22083;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_22083__$1,(11),inst_22048);
} else {
if((state_val_22084 === (17))){
var inst_22069 = (state_22083[(2)]);
var state_22083__$1 = state_22083;
if(cljs.core.truth_(inst_22069)){
var statearr_22094_22158 = state_22083__$1;
(statearr_22094_22158[(1)] = (19));

} else {
var statearr_22095_22159 = state_22083__$1;
(statearr_22095_22159[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22084 === (3))){
var inst_22081 = (state_22083[(2)]);
var state_22083__$1 = state_22083;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_22083__$1,inst_22081);
} else {
if((state_val_22084 === (12))){
var inst_22058 = (state_22083[(10)]);
var state_22083__$1 = state_22083;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_22083__$1,(14),inst_22058);
} else {
if((state_val_22084 === (2))){
var state_22083__$1 = state_22083;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_22083__$1,(4),results);
} else {
if((state_val_22084 === (19))){
var state_22083__$1 = state_22083;
var statearr_22096_22160 = state_22083__$1;
(statearr_22096_22160[(2)] = null);

(statearr_22096_22160[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22084 === (11))){
var inst_22058 = (state_22083[(2)]);
var state_22083__$1 = (function (){var statearr_22097 = state_22083;
(statearr_22097[(10)] = inst_22058);

return statearr_22097;
})();
var statearr_22098_22161 = state_22083__$1;
(statearr_22098_22161[(2)] = null);

(statearr_22098_22161[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22084 === (9))){
var state_22083__$1 = state_22083;
var statearr_22099_22162 = state_22083__$1;
(statearr_22099_22162[(2)] = null);

(statearr_22099_22162[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22084 === (5))){
var state_22083__$1 = state_22083;
if(cljs.core.truth_(close_QMARK_)){
var statearr_22100_22163 = state_22083__$1;
(statearr_22100_22163[(1)] = (8));

} else {
var statearr_22101_22164 = state_22083__$1;
(statearr_22101_22164[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22084 === (14))){
var inst_22063 = (state_22083[(11)]);
var inst_22061 = (state_22083[(8)]);
var inst_22061__$1 = (state_22083[(2)]);
var inst_22062 = (inst_22061__$1 == null);
var inst_22063__$1 = cljs.core.not.call(null,inst_22062);
var state_22083__$1 = (function (){var statearr_22102 = state_22083;
(statearr_22102[(11)] = inst_22063__$1);

(statearr_22102[(8)] = inst_22061__$1);

return statearr_22102;
})();
if(inst_22063__$1){
var statearr_22103_22165 = state_22083__$1;
(statearr_22103_22165[(1)] = (15));

} else {
var statearr_22104_22166 = state_22083__$1;
(statearr_22104_22166[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22084 === (16))){
var inst_22063 = (state_22083[(11)]);
var state_22083__$1 = state_22083;
var statearr_22105_22167 = state_22083__$1;
(statearr_22105_22167[(2)] = inst_22063);

(statearr_22105_22167[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22084 === (10))){
var inst_22055 = (state_22083[(2)]);
var state_22083__$1 = state_22083;
var statearr_22106_22168 = state_22083__$1;
(statearr_22106_22168[(2)] = inst_22055);

(statearr_22106_22168[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22084 === (18))){
var inst_22066 = (state_22083[(2)]);
var state_22083__$1 = state_22083;
var statearr_22107_22169 = state_22083__$1;
(statearr_22107_22169[(2)] = inst_22066);

(statearr_22107_22169[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22084 === (8))){
var inst_22052 = cljs.core.async.close_BANG_.call(null,to);
var state_22083__$1 = state_22083;
var statearr_22108_22170 = state_22083__$1;
(statearr_22108_22170[(2)] = inst_22052);

(statearr_22108_22170[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto__,jobs,results,process,async))
;
return ((function (switch__21501__auto__,c__21566__auto__,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____0 = (function (){
var statearr_22112 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_22112[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__);

(statearr_22112[(1)] = (1));

return statearr_22112;
});
var cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____1 = (function (state_22083){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_22083);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e22113){if((e22113 instanceof Object)){
var ex__21505__auto__ = e22113;
var statearr_22114_22171 = state_22083;
(statearr_22114_22171[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_22083);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e22113;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__22172 = state_22083;
state_22083 = G__22172;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__ = function(state_22083){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____1.call(this,state_22083);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__21502__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto__,jobs,results,process,async))
})();
var state__21568__auto__ = (function (){var statearr_22115 = f__21567__auto__.call(null);
(statearr_22115[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto__);

return statearr_22115;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto__,jobs,results,process,async))
);

return c__21566__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var args22173 = [];
var len__17855__auto___22176 = arguments.length;
var i__17856__auto___22177 = (0);
while(true){
if((i__17856__auto___22177 < len__17855__auto___22176)){
args22173.push((arguments[i__17856__auto___22177]));

var G__22178 = (i__17856__auto___22177 + (1));
i__17856__auto___22177 = G__22178;
continue;
} else {
}
break;
}

var G__22175 = args22173.length;
switch (G__22175) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args22173.length)].join('')));

}
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.call(null,n,to,af,from,true);
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_.call(null,n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
});

cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5;
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var args22180 = [];
var len__17855__auto___22183 = arguments.length;
var i__17856__auto___22184 = (0);
while(true){
if((i__17856__auto___22184 < len__17855__auto___22183)){
args22180.push((arguments[i__17856__auto___22184]));

var G__22185 = (i__17856__auto___22184 + (1));
i__17856__auto___22184 = G__22185;
continue;
} else {
}
break;
}

var G__22182 = args22180.length;
switch (G__22182) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args22180.length)].join('')));

}
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.call(null,n,to,xf,from,true);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.call(null,n,to,xf,from,close_QMARK_,null);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_.call(null,n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
});

cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6;
/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var args22187 = [];
var len__17855__auto___22240 = arguments.length;
var i__17856__auto___22241 = (0);
while(true){
if((i__17856__auto___22241 < len__17855__auto___22240)){
args22187.push((arguments[i__17856__auto___22241]));

var G__22242 = (i__17856__auto___22241 + (1));
i__17856__auto___22241 = G__22242;
continue;
} else {
}
break;
}

var G__22189 = args22187.length;
switch (G__22189) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args22187.length)].join('')));

}
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.call(null,p,ch,null,null);
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.call(null,t_buf_or_n);
var fc = cljs.core.async.chan.call(null,f_buf_or_n);
var c__21566__auto___22244 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___22244,tc,fc){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___22244,tc,fc){
return (function (state_22215){
var state_val_22216 = (state_22215[(1)]);
if((state_val_22216 === (7))){
var inst_22211 = (state_22215[(2)]);
var state_22215__$1 = state_22215;
var statearr_22217_22245 = state_22215__$1;
(statearr_22217_22245[(2)] = inst_22211);

(statearr_22217_22245[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22216 === (1))){
var state_22215__$1 = state_22215;
var statearr_22218_22246 = state_22215__$1;
(statearr_22218_22246[(2)] = null);

(statearr_22218_22246[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22216 === (4))){
var inst_22192 = (state_22215[(7)]);
var inst_22192__$1 = (state_22215[(2)]);
var inst_22193 = (inst_22192__$1 == null);
var state_22215__$1 = (function (){var statearr_22219 = state_22215;
(statearr_22219[(7)] = inst_22192__$1);

return statearr_22219;
})();
if(cljs.core.truth_(inst_22193)){
var statearr_22220_22247 = state_22215__$1;
(statearr_22220_22247[(1)] = (5));

} else {
var statearr_22221_22248 = state_22215__$1;
(statearr_22221_22248[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22216 === (13))){
var state_22215__$1 = state_22215;
var statearr_22222_22249 = state_22215__$1;
(statearr_22222_22249[(2)] = null);

(statearr_22222_22249[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22216 === (6))){
var inst_22192 = (state_22215[(7)]);
var inst_22198 = p.call(null,inst_22192);
var state_22215__$1 = state_22215;
if(cljs.core.truth_(inst_22198)){
var statearr_22223_22250 = state_22215__$1;
(statearr_22223_22250[(1)] = (9));

} else {
var statearr_22224_22251 = state_22215__$1;
(statearr_22224_22251[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22216 === (3))){
var inst_22213 = (state_22215[(2)]);
var state_22215__$1 = state_22215;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_22215__$1,inst_22213);
} else {
if((state_val_22216 === (12))){
var state_22215__$1 = state_22215;
var statearr_22225_22252 = state_22215__$1;
(statearr_22225_22252[(2)] = null);

(statearr_22225_22252[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22216 === (2))){
var state_22215__$1 = state_22215;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_22215__$1,(4),ch);
} else {
if((state_val_22216 === (11))){
var inst_22192 = (state_22215[(7)]);
var inst_22202 = (state_22215[(2)]);
var state_22215__$1 = state_22215;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_22215__$1,(8),inst_22202,inst_22192);
} else {
if((state_val_22216 === (9))){
var state_22215__$1 = state_22215;
var statearr_22226_22253 = state_22215__$1;
(statearr_22226_22253[(2)] = tc);

(statearr_22226_22253[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22216 === (5))){
var inst_22195 = cljs.core.async.close_BANG_.call(null,tc);
var inst_22196 = cljs.core.async.close_BANG_.call(null,fc);
var state_22215__$1 = (function (){var statearr_22227 = state_22215;
(statearr_22227[(8)] = inst_22195);

return statearr_22227;
})();
var statearr_22228_22254 = state_22215__$1;
(statearr_22228_22254[(2)] = inst_22196);

(statearr_22228_22254[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22216 === (14))){
var inst_22209 = (state_22215[(2)]);
var state_22215__$1 = state_22215;
var statearr_22229_22255 = state_22215__$1;
(statearr_22229_22255[(2)] = inst_22209);

(statearr_22229_22255[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22216 === (10))){
var state_22215__$1 = state_22215;
var statearr_22230_22256 = state_22215__$1;
(statearr_22230_22256[(2)] = fc);

(statearr_22230_22256[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22216 === (8))){
var inst_22204 = (state_22215[(2)]);
var state_22215__$1 = state_22215;
if(cljs.core.truth_(inst_22204)){
var statearr_22231_22257 = state_22215__$1;
(statearr_22231_22257[(1)] = (12));

} else {
var statearr_22232_22258 = state_22215__$1;
(statearr_22232_22258[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto___22244,tc,fc))
;
return ((function (switch__21501__auto__,c__21566__auto___22244,tc,fc){
return (function() {
var cljs$core$async$state_machine__21502__auto__ = null;
var cljs$core$async$state_machine__21502__auto____0 = (function (){
var statearr_22236 = [null,null,null,null,null,null,null,null,null];
(statearr_22236[(0)] = cljs$core$async$state_machine__21502__auto__);

(statearr_22236[(1)] = (1));

return statearr_22236;
});
var cljs$core$async$state_machine__21502__auto____1 = (function (state_22215){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_22215);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e22237){if((e22237 instanceof Object)){
var ex__21505__auto__ = e22237;
var statearr_22238_22259 = state_22215;
(statearr_22238_22259[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_22215);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e22237;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__22260 = state_22215;
state_22215 = G__22260;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$state_machine__21502__auto__ = function(state_22215){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21502__auto____1.call(this,state_22215);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21502__auto____0;
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21502__auto____1;
return cljs$core$async$state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___22244,tc,fc))
})();
var state__21568__auto__ = (function (){var statearr_22239 = f__21567__auto__.call(null);
(statearr_22239[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___22244);

return statearr_22239;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___22244,tc,fc))
);


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
});

cljs.core.async.split.cljs$lang$maxFixedArity = 4;
/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__21566__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto__){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto__){
return (function (state_22324){
var state_val_22325 = (state_22324[(1)]);
if((state_val_22325 === (7))){
var inst_22320 = (state_22324[(2)]);
var state_22324__$1 = state_22324;
var statearr_22326_22347 = state_22324__$1;
(statearr_22326_22347[(2)] = inst_22320);

(statearr_22326_22347[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22325 === (1))){
var inst_22304 = init;
var state_22324__$1 = (function (){var statearr_22327 = state_22324;
(statearr_22327[(7)] = inst_22304);

return statearr_22327;
})();
var statearr_22328_22348 = state_22324__$1;
(statearr_22328_22348[(2)] = null);

(statearr_22328_22348[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22325 === (4))){
var inst_22307 = (state_22324[(8)]);
var inst_22307__$1 = (state_22324[(2)]);
var inst_22308 = (inst_22307__$1 == null);
var state_22324__$1 = (function (){var statearr_22329 = state_22324;
(statearr_22329[(8)] = inst_22307__$1);

return statearr_22329;
})();
if(cljs.core.truth_(inst_22308)){
var statearr_22330_22349 = state_22324__$1;
(statearr_22330_22349[(1)] = (5));

} else {
var statearr_22331_22350 = state_22324__$1;
(statearr_22331_22350[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22325 === (6))){
var inst_22311 = (state_22324[(9)]);
var inst_22307 = (state_22324[(8)]);
var inst_22304 = (state_22324[(7)]);
var inst_22311__$1 = f.call(null,inst_22304,inst_22307);
var inst_22312 = cljs.core.reduced_QMARK_.call(null,inst_22311__$1);
var state_22324__$1 = (function (){var statearr_22332 = state_22324;
(statearr_22332[(9)] = inst_22311__$1);

return statearr_22332;
})();
if(inst_22312){
var statearr_22333_22351 = state_22324__$1;
(statearr_22333_22351[(1)] = (8));

} else {
var statearr_22334_22352 = state_22324__$1;
(statearr_22334_22352[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22325 === (3))){
var inst_22322 = (state_22324[(2)]);
var state_22324__$1 = state_22324;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_22324__$1,inst_22322);
} else {
if((state_val_22325 === (2))){
var state_22324__$1 = state_22324;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_22324__$1,(4),ch);
} else {
if((state_val_22325 === (9))){
var inst_22311 = (state_22324[(9)]);
var inst_22304 = inst_22311;
var state_22324__$1 = (function (){var statearr_22335 = state_22324;
(statearr_22335[(7)] = inst_22304);

return statearr_22335;
})();
var statearr_22336_22353 = state_22324__$1;
(statearr_22336_22353[(2)] = null);

(statearr_22336_22353[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22325 === (5))){
var inst_22304 = (state_22324[(7)]);
var state_22324__$1 = state_22324;
var statearr_22337_22354 = state_22324__$1;
(statearr_22337_22354[(2)] = inst_22304);

(statearr_22337_22354[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22325 === (10))){
var inst_22318 = (state_22324[(2)]);
var state_22324__$1 = state_22324;
var statearr_22338_22355 = state_22324__$1;
(statearr_22338_22355[(2)] = inst_22318);

(statearr_22338_22355[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22325 === (8))){
var inst_22311 = (state_22324[(9)]);
var inst_22314 = cljs.core.deref.call(null,inst_22311);
var state_22324__$1 = state_22324;
var statearr_22339_22356 = state_22324__$1;
(statearr_22339_22356[(2)] = inst_22314);

(statearr_22339_22356[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto__))
;
return ((function (switch__21501__auto__,c__21566__auto__){
return (function() {
var cljs$core$async$reduce_$_state_machine__21502__auto__ = null;
var cljs$core$async$reduce_$_state_machine__21502__auto____0 = (function (){
var statearr_22343 = [null,null,null,null,null,null,null,null,null,null];
(statearr_22343[(0)] = cljs$core$async$reduce_$_state_machine__21502__auto__);

(statearr_22343[(1)] = (1));

return statearr_22343;
});
var cljs$core$async$reduce_$_state_machine__21502__auto____1 = (function (state_22324){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_22324);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e22344){if((e22344 instanceof Object)){
var ex__21505__auto__ = e22344;
var statearr_22345_22357 = state_22324;
(statearr_22345_22357[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_22324);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e22344;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__22358 = state_22324;
state_22324 = G__22358;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__21502__auto__ = function(state_22324){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__21502__auto____1.call(this,state_22324);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__21502__auto____0;
cljs$core$async$reduce_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__21502__auto____1;
return cljs$core$async$reduce_$_state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto__))
})();
var state__21568__auto__ = (function (){var statearr_22346 = f__21567__auto__.call(null);
(statearr_22346[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto__);

return statearr_22346;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto__))
);

return c__21566__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var args22359 = [];
var len__17855__auto___22411 = arguments.length;
var i__17856__auto___22412 = (0);
while(true){
if((i__17856__auto___22412 < len__17855__auto___22411)){
args22359.push((arguments[i__17856__auto___22412]));

var G__22413 = (i__17856__auto___22412 + (1));
i__17856__auto___22412 = G__22413;
continue;
} else {
}
break;
}

var G__22361 = args22359.length;
switch (G__22361) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args22359.length)].join('')));

}
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan.call(null,ch,coll,true);
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__21566__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto__){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto__){
return (function (state_22386){
var state_val_22387 = (state_22386[(1)]);
if((state_val_22387 === (7))){
var inst_22368 = (state_22386[(2)]);
var state_22386__$1 = state_22386;
var statearr_22388_22415 = state_22386__$1;
(statearr_22388_22415[(2)] = inst_22368);

(statearr_22388_22415[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22387 === (1))){
var inst_22362 = cljs.core.seq.call(null,coll);
var inst_22363 = inst_22362;
var state_22386__$1 = (function (){var statearr_22389 = state_22386;
(statearr_22389[(7)] = inst_22363);

return statearr_22389;
})();
var statearr_22390_22416 = state_22386__$1;
(statearr_22390_22416[(2)] = null);

(statearr_22390_22416[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22387 === (4))){
var inst_22363 = (state_22386[(7)]);
var inst_22366 = cljs.core.first.call(null,inst_22363);
var state_22386__$1 = state_22386;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_22386__$1,(7),ch,inst_22366);
} else {
if((state_val_22387 === (13))){
var inst_22380 = (state_22386[(2)]);
var state_22386__$1 = state_22386;
var statearr_22391_22417 = state_22386__$1;
(statearr_22391_22417[(2)] = inst_22380);

(statearr_22391_22417[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22387 === (6))){
var inst_22371 = (state_22386[(2)]);
var state_22386__$1 = state_22386;
if(cljs.core.truth_(inst_22371)){
var statearr_22392_22418 = state_22386__$1;
(statearr_22392_22418[(1)] = (8));

} else {
var statearr_22393_22419 = state_22386__$1;
(statearr_22393_22419[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22387 === (3))){
var inst_22384 = (state_22386[(2)]);
var state_22386__$1 = state_22386;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_22386__$1,inst_22384);
} else {
if((state_val_22387 === (12))){
var state_22386__$1 = state_22386;
var statearr_22394_22420 = state_22386__$1;
(statearr_22394_22420[(2)] = null);

(statearr_22394_22420[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22387 === (2))){
var inst_22363 = (state_22386[(7)]);
var state_22386__$1 = state_22386;
if(cljs.core.truth_(inst_22363)){
var statearr_22395_22421 = state_22386__$1;
(statearr_22395_22421[(1)] = (4));

} else {
var statearr_22396_22422 = state_22386__$1;
(statearr_22396_22422[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22387 === (11))){
var inst_22377 = cljs.core.async.close_BANG_.call(null,ch);
var state_22386__$1 = state_22386;
var statearr_22397_22423 = state_22386__$1;
(statearr_22397_22423[(2)] = inst_22377);

(statearr_22397_22423[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22387 === (9))){
var state_22386__$1 = state_22386;
if(cljs.core.truth_(close_QMARK_)){
var statearr_22398_22424 = state_22386__$1;
(statearr_22398_22424[(1)] = (11));

} else {
var statearr_22399_22425 = state_22386__$1;
(statearr_22399_22425[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22387 === (5))){
var inst_22363 = (state_22386[(7)]);
var state_22386__$1 = state_22386;
var statearr_22400_22426 = state_22386__$1;
(statearr_22400_22426[(2)] = inst_22363);

(statearr_22400_22426[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22387 === (10))){
var inst_22382 = (state_22386[(2)]);
var state_22386__$1 = state_22386;
var statearr_22401_22427 = state_22386__$1;
(statearr_22401_22427[(2)] = inst_22382);

(statearr_22401_22427[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22387 === (8))){
var inst_22363 = (state_22386[(7)]);
var inst_22373 = cljs.core.next.call(null,inst_22363);
var inst_22363__$1 = inst_22373;
var state_22386__$1 = (function (){var statearr_22402 = state_22386;
(statearr_22402[(7)] = inst_22363__$1);

return statearr_22402;
})();
var statearr_22403_22428 = state_22386__$1;
(statearr_22403_22428[(2)] = null);

(statearr_22403_22428[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto__))
;
return ((function (switch__21501__auto__,c__21566__auto__){
return (function() {
var cljs$core$async$state_machine__21502__auto__ = null;
var cljs$core$async$state_machine__21502__auto____0 = (function (){
var statearr_22407 = [null,null,null,null,null,null,null,null];
(statearr_22407[(0)] = cljs$core$async$state_machine__21502__auto__);

(statearr_22407[(1)] = (1));

return statearr_22407;
});
var cljs$core$async$state_machine__21502__auto____1 = (function (state_22386){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_22386);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e22408){if((e22408 instanceof Object)){
var ex__21505__auto__ = e22408;
var statearr_22409_22429 = state_22386;
(statearr_22409_22429[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_22386);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e22408;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__22430 = state_22386;
state_22386 = G__22430;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$state_machine__21502__auto__ = function(state_22386){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21502__auto____1.call(this,state_22386);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21502__auto____0;
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21502__auto____1;
return cljs$core$async$state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto__))
})();
var state__21568__auto__ = (function (){var statearr_22410 = f__21567__auto__.call(null);
(statearr_22410[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto__);

return statearr_22410;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto__))
);

return c__21566__auto__;
});

cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3;
/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
var ch = cljs.core.async.chan.call(null,cljs.core.bounded_count.call(null,(100),coll));
cljs.core.async.onto_chan.call(null,ch,coll);

return ch;
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((!((_ == null))) && (!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
var x__17452__auto__ = (((_ == null))?null:_);
var m__17453__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__17452__auto__)]);
if(!((m__17453__auto__ == null))){
return m__17453__auto__.call(null,_);
} else {
var m__17453__auto____$1 = (cljs.core.async.muxch_STAR_["_"]);
if(!((m__17453__auto____$1 == null))){
return m__17453__auto____$1.call(null,_);
} else {
throw cljs.core.missing_protocol.call(null,"Mux.muxch*",_);
}
}
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((!((m == null))) && (!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
var x__17452__auto__ = (((m == null))?null:m);
var m__17453__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__17452__auto__)]);
if(!((m__17453__auto__ == null))){
return m__17453__auto__.call(null,m,ch,close_QMARK_);
} else {
var m__17453__auto____$1 = (cljs.core.async.tap_STAR_["_"]);
if(!((m__17453__auto____$1 == null))){
return m__17453__auto____$1.call(null,m,ch,close_QMARK_);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.tap*",m);
}
}
}
});

cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
var x__17452__auto__ = (((m == null))?null:m);
var m__17453__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__17452__auto__)]);
if(!((m__17453__auto__ == null))){
return m__17453__auto__.call(null,m,ch);
} else {
var m__17453__auto____$1 = (cljs.core.async.untap_STAR_["_"]);
if(!((m__17453__auto____$1 == null))){
return m__17453__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap*",m);
}
}
}
});

cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((!((m == null))) && (!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
var x__17452__auto__ = (((m == null))?null:m);
var m__17453__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__17452__auto__)]);
if(!((m__17453__auto__ == null))){
return m__17453__auto__.call(null,m);
} else {
var m__17453__auto____$1 = (cljs.core.async.untap_all_STAR_["_"]);
if(!((m__17453__auto____$1 == null))){
return m__17453__auto____$1.call(null,m);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap-all*",m);
}
}
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if(typeof cljs.core.async.t_cljs$core$async22652 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async22652 = (function (mult,ch,cs,meta22653){
this.mult = mult;
this.ch = ch;
this.cs = cs;
this.meta22653 = meta22653;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async22652.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_22654,meta22653__$1){
var self__ = this;
var _22654__$1 = this;
return (new cljs.core.async.t_cljs$core$async22652(self__.mult,self__.ch,self__.cs,meta22653__$1));
});})(cs))
;

cljs.core.async.t_cljs$core$async22652.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_22654){
var self__ = this;
var _22654__$1 = this;
return self__.meta22653;
});})(cs))
;

cljs.core.async.t_cljs$core$async22652.prototype.cljs$core$async$Mux$ = true;

cljs.core.async.t_cljs$core$async22652.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(cs))
;

cljs.core.async.t_cljs$core$async22652.prototype.cljs$core$async$Mult$ = true;

cljs.core.async.t_cljs$core$async22652.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async22652.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch__$1);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async22652.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async22652.getBasis = ((function (cs){
return (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"mult","mult",-1187640995,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Creates and returns a mult(iple) of the supplied channel. Channels\n  containing copies of the channel can be created with 'tap', and\n  detached with 'untap'.\n\n  Each item is distributed to all taps in parallel and synchronously,\n  i.e. each tap must accept before the next item is distributed. Use\n  buffering/windowing to prevent slow taps from holding up the mult.\n\n  Items received when there are no taps get dropped.\n\n  If a tap puts to a closed channel, it will be removed from the mult."], null)),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta22653","meta22653",648469553,null)], null);
});})(cs))
;

cljs.core.async.t_cljs$core$async22652.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async22652.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async22652";

cljs.core.async.t_cljs$core$async22652.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__17394__auto__,writer__17395__auto__,opt__17396__auto__){
return cljs.core._write.call(null,writer__17395__auto__,"cljs.core.async/t_cljs$core$async22652");
});})(cs))
;

cljs.core.async.__GT_t_cljs$core$async22652 = ((function (cs){
return (function cljs$core$async$mult_$___GT_t_cljs$core$async22652(mult__$1,ch__$1,cs__$1,meta22653){
return (new cljs.core.async.t_cljs$core$async22652(mult__$1,ch__$1,cs__$1,meta22653));
});})(cs))
;

}

return (new cljs.core.async.t_cljs$core$async22652(cljs$core$async$mult,ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = ((function (cs,m,dchan,dctr){
return (function (_){
if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,true);
} else {
return null;
}
});})(cs,m,dchan,dctr))
;
var c__21566__auto___22873 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___22873,cs,m,dchan,dctr,done){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___22873,cs,m,dchan,dctr,done){
return (function (state_22785){
var state_val_22786 = (state_22785[(1)]);
if((state_val_22786 === (7))){
var inst_22781 = (state_22785[(2)]);
var state_22785__$1 = state_22785;
var statearr_22787_22874 = state_22785__$1;
(statearr_22787_22874[(2)] = inst_22781);

(statearr_22787_22874[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (20))){
var inst_22686 = (state_22785[(7)]);
var inst_22696 = cljs.core.first.call(null,inst_22686);
var inst_22697 = cljs.core.nth.call(null,inst_22696,(0),null);
var inst_22698 = cljs.core.nth.call(null,inst_22696,(1),null);
var state_22785__$1 = (function (){var statearr_22788 = state_22785;
(statearr_22788[(8)] = inst_22697);

return statearr_22788;
})();
if(cljs.core.truth_(inst_22698)){
var statearr_22789_22875 = state_22785__$1;
(statearr_22789_22875[(1)] = (22));

} else {
var statearr_22790_22876 = state_22785__$1;
(statearr_22790_22876[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (27))){
var inst_22728 = (state_22785[(9)]);
var inst_22726 = (state_22785[(10)]);
var inst_22657 = (state_22785[(11)]);
var inst_22733 = (state_22785[(12)]);
var inst_22733__$1 = cljs.core._nth.call(null,inst_22726,inst_22728);
var inst_22734 = cljs.core.async.put_BANG_.call(null,inst_22733__$1,inst_22657,done);
var state_22785__$1 = (function (){var statearr_22791 = state_22785;
(statearr_22791[(12)] = inst_22733__$1);

return statearr_22791;
})();
if(cljs.core.truth_(inst_22734)){
var statearr_22792_22877 = state_22785__$1;
(statearr_22792_22877[(1)] = (30));

} else {
var statearr_22793_22878 = state_22785__$1;
(statearr_22793_22878[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (1))){
var state_22785__$1 = state_22785;
var statearr_22794_22879 = state_22785__$1;
(statearr_22794_22879[(2)] = null);

(statearr_22794_22879[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (24))){
var inst_22686 = (state_22785[(7)]);
var inst_22703 = (state_22785[(2)]);
var inst_22704 = cljs.core.next.call(null,inst_22686);
var inst_22666 = inst_22704;
var inst_22667 = null;
var inst_22668 = (0);
var inst_22669 = (0);
var state_22785__$1 = (function (){var statearr_22795 = state_22785;
(statearr_22795[(13)] = inst_22668);

(statearr_22795[(14)] = inst_22703);

(statearr_22795[(15)] = inst_22667);

(statearr_22795[(16)] = inst_22669);

(statearr_22795[(17)] = inst_22666);

return statearr_22795;
})();
var statearr_22796_22880 = state_22785__$1;
(statearr_22796_22880[(2)] = null);

(statearr_22796_22880[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (39))){
var state_22785__$1 = state_22785;
var statearr_22800_22881 = state_22785__$1;
(statearr_22800_22881[(2)] = null);

(statearr_22800_22881[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (4))){
var inst_22657 = (state_22785[(11)]);
var inst_22657__$1 = (state_22785[(2)]);
var inst_22658 = (inst_22657__$1 == null);
var state_22785__$1 = (function (){var statearr_22801 = state_22785;
(statearr_22801[(11)] = inst_22657__$1);

return statearr_22801;
})();
if(cljs.core.truth_(inst_22658)){
var statearr_22802_22882 = state_22785__$1;
(statearr_22802_22882[(1)] = (5));

} else {
var statearr_22803_22883 = state_22785__$1;
(statearr_22803_22883[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (15))){
var inst_22668 = (state_22785[(13)]);
var inst_22667 = (state_22785[(15)]);
var inst_22669 = (state_22785[(16)]);
var inst_22666 = (state_22785[(17)]);
var inst_22682 = (state_22785[(2)]);
var inst_22683 = (inst_22669 + (1));
var tmp22797 = inst_22668;
var tmp22798 = inst_22667;
var tmp22799 = inst_22666;
var inst_22666__$1 = tmp22799;
var inst_22667__$1 = tmp22798;
var inst_22668__$1 = tmp22797;
var inst_22669__$1 = inst_22683;
var state_22785__$1 = (function (){var statearr_22804 = state_22785;
(statearr_22804[(13)] = inst_22668__$1);

(statearr_22804[(18)] = inst_22682);

(statearr_22804[(15)] = inst_22667__$1);

(statearr_22804[(16)] = inst_22669__$1);

(statearr_22804[(17)] = inst_22666__$1);

return statearr_22804;
})();
var statearr_22805_22884 = state_22785__$1;
(statearr_22805_22884[(2)] = null);

(statearr_22805_22884[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (21))){
var inst_22707 = (state_22785[(2)]);
var state_22785__$1 = state_22785;
var statearr_22809_22885 = state_22785__$1;
(statearr_22809_22885[(2)] = inst_22707);

(statearr_22809_22885[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (31))){
var inst_22733 = (state_22785[(12)]);
var inst_22737 = done.call(null,null);
var inst_22738 = cljs.core.async.untap_STAR_.call(null,m,inst_22733);
var state_22785__$1 = (function (){var statearr_22810 = state_22785;
(statearr_22810[(19)] = inst_22737);

return statearr_22810;
})();
var statearr_22811_22886 = state_22785__$1;
(statearr_22811_22886[(2)] = inst_22738);

(statearr_22811_22886[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (32))){
var inst_22728 = (state_22785[(9)]);
var inst_22726 = (state_22785[(10)]);
var inst_22725 = (state_22785[(20)]);
var inst_22727 = (state_22785[(21)]);
var inst_22740 = (state_22785[(2)]);
var inst_22741 = (inst_22728 + (1));
var tmp22806 = inst_22726;
var tmp22807 = inst_22725;
var tmp22808 = inst_22727;
var inst_22725__$1 = tmp22807;
var inst_22726__$1 = tmp22806;
var inst_22727__$1 = tmp22808;
var inst_22728__$1 = inst_22741;
var state_22785__$1 = (function (){var statearr_22812 = state_22785;
(statearr_22812[(9)] = inst_22728__$1);

(statearr_22812[(10)] = inst_22726__$1);

(statearr_22812[(22)] = inst_22740);

(statearr_22812[(20)] = inst_22725__$1);

(statearr_22812[(21)] = inst_22727__$1);

return statearr_22812;
})();
var statearr_22813_22887 = state_22785__$1;
(statearr_22813_22887[(2)] = null);

(statearr_22813_22887[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (40))){
var inst_22753 = (state_22785[(23)]);
var inst_22757 = done.call(null,null);
var inst_22758 = cljs.core.async.untap_STAR_.call(null,m,inst_22753);
var state_22785__$1 = (function (){var statearr_22814 = state_22785;
(statearr_22814[(24)] = inst_22757);

return statearr_22814;
})();
var statearr_22815_22888 = state_22785__$1;
(statearr_22815_22888[(2)] = inst_22758);

(statearr_22815_22888[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (33))){
var inst_22744 = (state_22785[(25)]);
var inst_22746 = cljs.core.chunked_seq_QMARK_.call(null,inst_22744);
var state_22785__$1 = state_22785;
if(inst_22746){
var statearr_22816_22889 = state_22785__$1;
(statearr_22816_22889[(1)] = (36));

} else {
var statearr_22817_22890 = state_22785__$1;
(statearr_22817_22890[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (13))){
var inst_22676 = (state_22785[(26)]);
var inst_22679 = cljs.core.async.close_BANG_.call(null,inst_22676);
var state_22785__$1 = state_22785;
var statearr_22818_22891 = state_22785__$1;
(statearr_22818_22891[(2)] = inst_22679);

(statearr_22818_22891[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (22))){
var inst_22697 = (state_22785[(8)]);
var inst_22700 = cljs.core.async.close_BANG_.call(null,inst_22697);
var state_22785__$1 = state_22785;
var statearr_22819_22892 = state_22785__$1;
(statearr_22819_22892[(2)] = inst_22700);

(statearr_22819_22892[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (36))){
var inst_22744 = (state_22785[(25)]);
var inst_22748 = cljs.core.chunk_first.call(null,inst_22744);
var inst_22749 = cljs.core.chunk_rest.call(null,inst_22744);
var inst_22750 = cljs.core.count.call(null,inst_22748);
var inst_22725 = inst_22749;
var inst_22726 = inst_22748;
var inst_22727 = inst_22750;
var inst_22728 = (0);
var state_22785__$1 = (function (){var statearr_22820 = state_22785;
(statearr_22820[(9)] = inst_22728);

(statearr_22820[(10)] = inst_22726);

(statearr_22820[(20)] = inst_22725);

(statearr_22820[(21)] = inst_22727);

return statearr_22820;
})();
var statearr_22821_22893 = state_22785__$1;
(statearr_22821_22893[(2)] = null);

(statearr_22821_22893[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (41))){
var inst_22744 = (state_22785[(25)]);
var inst_22760 = (state_22785[(2)]);
var inst_22761 = cljs.core.next.call(null,inst_22744);
var inst_22725 = inst_22761;
var inst_22726 = null;
var inst_22727 = (0);
var inst_22728 = (0);
var state_22785__$1 = (function (){var statearr_22822 = state_22785;
(statearr_22822[(27)] = inst_22760);

(statearr_22822[(9)] = inst_22728);

(statearr_22822[(10)] = inst_22726);

(statearr_22822[(20)] = inst_22725);

(statearr_22822[(21)] = inst_22727);

return statearr_22822;
})();
var statearr_22823_22894 = state_22785__$1;
(statearr_22823_22894[(2)] = null);

(statearr_22823_22894[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (43))){
var state_22785__$1 = state_22785;
var statearr_22824_22895 = state_22785__$1;
(statearr_22824_22895[(2)] = null);

(statearr_22824_22895[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (29))){
var inst_22769 = (state_22785[(2)]);
var state_22785__$1 = state_22785;
var statearr_22825_22896 = state_22785__$1;
(statearr_22825_22896[(2)] = inst_22769);

(statearr_22825_22896[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (44))){
var inst_22778 = (state_22785[(2)]);
var state_22785__$1 = (function (){var statearr_22826 = state_22785;
(statearr_22826[(28)] = inst_22778);

return statearr_22826;
})();
var statearr_22827_22897 = state_22785__$1;
(statearr_22827_22897[(2)] = null);

(statearr_22827_22897[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (6))){
var inst_22717 = (state_22785[(29)]);
var inst_22716 = cljs.core.deref.call(null,cs);
var inst_22717__$1 = cljs.core.keys.call(null,inst_22716);
var inst_22718 = cljs.core.count.call(null,inst_22717__$1);
var inst_22719 = cljs.core.reset_BANG_.call(null,dctr,inst_22718);
var inst_22724 = cljs.core.seq.call(null,inst_22717__$1);
var inst_22725 = inst_22724;
var inst_22726 = null;
var inst_22727 = (0);
var inst_22728 = (0);
var state_22785__$1 = (function (){var statearr_22828 = state_22785;
(statearr_22828[(9)] = inst_22728);

(statearr_22828[(29)] = inst_22717__$1);

(statearr_22828[(10)] = inst_22726);

(statearr_22828[(20)] = inst_22725);

(statearr_22828[(21)] = inst_22727);

(statearr_22828[(30)] = inst_22719);

return statearr_22828;
})();
var statearr_22829_22898 = state_22785__$1;
(statearr_22829_22898[(2)] = null);

(statearr_22829_22898[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (28))){
var inst_22744 = (state_22785[(25)]);
var inst_22725 = (state_22785[(20)]);
var inst_22744__$1 = cljs.core.seq.call(null,inst_22725);
var state_22785__$1 = (function (){var statearr_22830 = state_22785;
(statearr_22830[(25)] = inst_22744__$1);

return statearr_22830;
})();
if(inst_22744__$1){
var statearr_22831_22899 = state_22785__$1;
(statearr_22831_22899[(1)] = (33));

} else {
var statearr_22832_22900 = state_22785__$1;
(statearr_22832_22900[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (25))){
var inst_22728 = (state_22785[(9)]);
var inst_22727 = (state_22785[(21)]);
var inst_22730 = (inst_22728 < inst_22727);
var inst_22731 = inst_22730;
var state_22785__$1 = state_22785;
if(cljs.core.truth_(inst_22731)){
var statearr_22833_22901 = state_22785__$1;
(statearr_22833_22901[(1)] = (27));

} else {
var statearr_22834_22902 = state_22785__$1;
(statearr_22834_22902[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (34))){
var state_22785__$1 = state_22785;
var statearr_22835_22903 = state_22785__$1;
(statearr_22835_22903[(2)] = null);

(statearr_22835_22903[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (17))){
var state_22785__$1 = state_22785;
var statearr_22836_22904 = state_22785__$1;
(statearr_22836_22904[(2)] = null);

(statearr_22836_22904[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (3))){
var inst_22783 = (state_22785[(2)]);
var state_22785__$1 = state_22785;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_22785__$1,inst_22783);
} else {
if((state_val_22786 === (12))){
var inst_22712 = (state_22785[(2)]);
var state_22785__$1 = state_22785;
var statearr_22837_22905 = state_22785__$1;
(statearr_22837_22905[(2)] = inst_22712);

(statearr_22837_22905[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (2))){
var state_22785__$1 = state_22785;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_22785__$1,(4),ch);
} else {
if((state_val_22786 === (23))){
var state_22785__$1 = state_22785;
var statearr_22838_22906 = state_22785__$1;
(statearr_22838_22906[(2)] = null);

(statearr_22838_22906[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (35))){
var inst_22767 = (state_22785[(2)]);
var state_22785__$1 = state_22785;
var statearr_22839_22907 = state_22785__$1;
(statearr_22839_22907[(2)] = inst_22767);

(statearr_22839_22907[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (19))){
var inst_22686 = (state_22785[(7)]);
var inst_22690 = cljs.core.chunk_first.call(null,inst_22686);
var inst_22691 = cljs.core.chunk_rest.call(null,inst_22686);
var inst_22692 = cljs.core.count.call(null,inst_22690);
var inst_22666 = inst_22691;
var inst_22667 = inst_22690;
var inst_22668 = inst_22692;
var inst_22669 = (0);
var state_22785__$1 = (function (){var statearr_22840 = state_22785;
(statearr_22840[(13)] = inst_22668);

(statearr_22840[(15)] = inst_22667);

(statearr_22840[(16)] = inst_22669);

(statearr_22840[(17)] = inst_22666);

return statearr_22840;
})();
var statearr_22841_22908 = state_22785__$1;
(statearr_22841_22908[(2)] = null);

(statearr_22841_22908[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (11))){
var inst_22686 = (state_22785[(7)]);
var inst_22666 = (state_22785[(17)]);
var inst_22686__$1 = cljs.core.seq.call(null,inst_22666);
var state_22785__$1 = (function (){var statearr_22842 = state_22785;
(statearr_22842[(7)] = inst_22686__$1);

return statearr_22842;
})();
if(inst_22686__$1){
var statearr_22843_22909 = state_22785__$1;
(statearr_22843_22909[(1)] = (16));

} else {
var statearr_22844_22910 = state_22785__$1;
(statearr_22844_22910[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (9))){
var inst_22714 = (state_22785[(2)]);
var state_22785__$1 = state_22785;
var statearr_22845_22911 = state_22785__$1;
(statearr_22845_22911[(2)] = inst_22714);

(statearr_22845_22911[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (5))){
var inst_22664 = cljs.core.deref.call(null,cs);
var inst_22665 = cljs.core.seq.call(null,inst_22664);
var inst_22666 = inst_22665;
var inst_22667 = null;
var inst_22668 = (0);
var inst_22669 = (0);
var state_22785__$1 = (function (){var statearr_22846 = state_22785;
(statearr_22846[(13)] = inst_22668);

(statearr_22846[(15)] = inst_22667);

(statearr_22846[(16)] = inst_22669);

(statearr_22846[(17)] = inst_22666);

return statearr_22846;
})();
var statearr_22847_22912 = state_22785__$1;
(statearr_22847_22912[(2)] = null);

(statearr_22847_22912[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (14))){
var state_22785__$1 = state_22785;
var statearr_22848_22913 = state_22785__$1;
(statearr_22848_22913[(2)] = null);

(statearr_22848_22913[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (45))){
var inst_22775 = (state_22785[(2)]);
var state_22785__$1 = state_22785;
var statearr_22849_22914 = state_22785__$1;
(statearr_22849_22914[(2)] = inst_22775);

(statearr_22849_22914[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (26))){
var inst_22717 = (state_22785[(29)]);
var inst_22771 = (state_22785[(2)]);
var inst_22772 = cljs.core.seq.call(null,inst_22717);
var state_22785__$1 = (function (){var statearr_22850 = state_22785;
(statearr_22850[(31)] = inst_22771);

return statearr_22850;
})();
if(inst_22772){
var statearr_22851_22915 = state_22785__$1;
(statearr_22851_22915[(1)] = (42));

} else {
var statearr_22852_22916 = state_22785__$1;
(statearr_22852_22916[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (16))){
var inst_22686 = (state_22785[(7)]);
var inst_22688 = cljs.core.chunked_seq_QMARK_.call(null,inst_22686);
var state_22785__$1 = state_22785;
if(inst_22688){
var statearr_22853_22917 = state_22785__$1;
(statearr_22853_22917[(1)] = (19));

} else {
var statearr_22854_22918 = state_22785__$1;
(statearr_22854_22918[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (38))){
var inst_22764 = (state_22785[(2)]);
var state_22785__$1 = state_22785;
var statearr_22855_22919 = state_22785__$1;
(statearr_22855_22919[(2)] = inst_22764);

(statearr_22855_22919[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (30))){
var state_22785__$1 = state_22785;
var statearr_22856_22920 = state_22785__$1;
(statearr_22856_22920[(2)] = null);

(statearr_22856_22920[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (10))){
var inst_22667 = (state_22785[(15)]);
var inst_22669 = (state_22785[(16)]);
var inst_22675 = cljs.core._nth.call(null,inst_22667,inst_22669);
var inst_22676 = cljs.core.nth.call(null,inst_22675,(0),null);
var inst_22677 = cljs.core.nth.call(null,inst_22675,(1),null);
var state_22785__$1 = (function (){var statearr_22857 = state_22785;
(statearr_22857[(26)] = inst_22676);

return statearr_22857;
})();
if(cljs.core.truth_(inst_22677)){
var statearr_22858_22921 = state_22785__$1;
(statearr_22858_22921[(1)] = (13));

} else {
var statearr_22859_22922 = state_22785__$1;
(statearr_22859_22922[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (18))){
var inst_22710 = (state_22785[(2)]);
var state_22785__$1 = state_22785;
var statearr_22860_22923 = state_22785__$1;
(statearr_22860_22923[(2)] = inst_22710);

(statearr_22860_22923[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (42))){
var state_22785__$1 = state_22785;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_22785__$1,(45),dchan);
} else {
if((state_val_22786 === (37))){
var inst_22744 = (state_22785[(25)]);
var inst_22753 = (state_22785[(23)]);
var inst_22657 = (state_22785[(11)]);
var inst_22753__$1 = cljs.core.first.call(null,inst_22744);
var inst_22754 = cljs.core.async.put_BANG_.call(null,inst_22753__$1,inst_22657,done);
var state_22785__$1 = (function (){var statearr_22861 = state_22785;
(statearr_22861[(23)] = inst_22753__$1);

return statearr_22861;
})();
if(cljs.core.truth_(inst_22754)){
var statearr_22862_22924 = state_22785__$1;
(statearr_22862_22924[(1)] = (39));

} else {
var statearr_22863_22925 = state_22785__$1;
(statearr_22863_22925[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22786 === (8))){
var inst_22668 = (state_22785[(13)]);
var inst_22669 = (state_22785[(16)]);
var inst_22671 = (inst_22669 < inst_22668);
var inst_22672 = inst_22671;
var state_22785__$1 = state_22785;
if(cljs.core.truth_(inst_22672)){
var statearr_22864_22926 = state_22785__$1;
(statearr_22864_22926[(1)] = (10));

} else {
var statearr_22865_22927 = state_22785__$1;
(statearr_22865_22927[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto___22873,cs,m,dchan,dctr,done))
;
return ((function (switch__21501__auto__,c__21566__auto___22873,cs,m,dchan,dctr,done){
return (function() {
var cljs$core$async$mult_$_state_machine__21502__auto__ = null;
var cljs$core$async$mult_$_state_machine__21502__auto____0 = (function (){
var statearr_22869 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_22869[(0)] = cljs$core$async$mult_$_state_machine__21502__auto__);

(statearr_22869[(1)] = (1));

return statearr_22869;
});
var cljs$core$async$mult_$_state_machine__21502__auto____1 = (function (state_22785){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_22785);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e22870){if((e22870 instanceof Object)){
var ex__21505__auto__ = e22870;
var statearr_22871_22928 = state_22785;
(statearr_22871_22928[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_22785);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e22870;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__22929 = state_22785;
state_22785 = G__22929;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__21502__auto__ = function(state_22785){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__21502__auto____1.call(this,state_22785);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__21502__auto____0;
cljs$core$async$mult_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__21502__auto____1;
return cljs$core$async$mult_$_state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___22873,cs,m,dchan,dctr,done))
})();
var state__21568__auto__ = (function (){var statearr_22872 = f__21567__auto__.call(null);
(statearr_22872[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___22873);

return statearr_22872;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___22873,cs,m,dchan,dctr,done))
);


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var args22930 = [];
var len__17855__auto___22933 = arguments.length;
var i__17856__auto___22934 = (0);
while(true){
if((i__17856__auto___22934 < len__17855__auto___22933)){
args22930.push((arguments[i__17856__auto___22934]));

var G__22935 = (i__17856__auto___22934 + (1));
i__17856__auto___22934 = G__22935;
continue;
} else {
}
break;
}

var G__22932 = args22930.length;
switch (G__22932) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args22930.length)].join('')));

}
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.call(null,mult,ch,true);
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_.call(null,mult,ch,close_QMARK_);

return ch;
});

cljs.core.async.tap.cljs$lang$maxFixedArity = 3;
/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_.call(null,mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_.call(null,mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
var x__17452__auto__ = (((m == null))?null:m);
var m__17453__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__17452__auto__)]);
if(!((m__17453__auto__ == null))){
return m__17453__auto__.call(null,m,ch);
} else {
var m__17453__auto____$1 = (cljs.core.async.admix_STAR_["_"]);
if(!((m__17453__auto____$1 == null))){
return m__17453__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.admix*",m);
}
}
}
});

cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
var x__17452__auto__ = (((m == null))?null:m);
var m__17453__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__17452__auto__)]);
if(!((m__17453__auto__ == null))){
return m__17453__auto__.call(null,m,ch);
} else {
var m__17453__auto____$1 = (cljs.core.async.unmix_STAR_["_"]);
if(!((m__17453__auto____$1 == null))){
return m__17453__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix*",m);
}
}
}
});

cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((!((m == null))) && (!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
var x__17452__auto__ = (((m == null))?null:m);
var m__17453__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__17452__auto__)]);
if(!((m__17453__auto__ == null))){
return m__17453__auto__.call(null,m);
} else {
var m__17453__auto____$1 = (cljs.core.async.unmix_all_STAR_["_"]);
if(!((m__17453__auto____$1 == null))){
return m__17453__auto____$1.call(null,m);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix-all*",m);
}
}
}
});

cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((!((m == null))) && (!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
var x__17452__auto__ = (((m == null))?null:m);
var m__17453__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__17452__auto__)]);
if(!((m__17453__auto__ == null))){
return m__17453__auto__.call(null,m,state_map);
} else {
var m__17453__auto____$1 = (cljs.core.async.toggle_STAR_["_"]);
if(!((m__17453__auto____$1 == null))){
return m__17453__auto____$1.call(null,m,state_map);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.toggle*",m);
}
}
}
});

cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((!((m == null))) && (!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
var x__17452__auto__ = (((m == null))?null:m);
var m__17453__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__17452__auto__)]);
if(!((m__17453__auto__ == null))){
return m__17453__auto__.call(null,m,mode);
} else {
var m__17453__auto____$1 = (cljs.core.async.solo_mode_STAR_["_"]);
if(!((m__17453__auto____$1 == null))){
return m__17453__auto____$1.call(null,m,mode);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.solo-mode*",m);
}
}
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__17862__auto__ = [];
var len__17855__auto___22947 = arguments.length;
var i__17856__auto___22948 = (0);
while(true){
if((i__17856__auto___22948 < len__17855__auto___22947)){
args__17862__auto__.push((arguments[i__17856__auto___22948]));

var G__22949 = (i__17856__auto___22948 + (1));
i__17856__auto___22948 = G__22949;
continue;
} else {
}
break;
}

var argseq__17863__auto__ = ((((3) < args__17862__auto__.length))?(new cljs.core.IndexedSeq(args__17862__auto__.slice((3)),(0))):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__17863__auto__);
});

cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__22941){
var map__22942 = p__22941;
var map__22942__$1 = ((((!((map__22942 == null)))?((((map__22942.cljs$lang$protocol_mask$partition0$ & (64))) || (map__22942.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__22942):map__22942);
var opts = map__22942__$1;
var statearr_22944_22950 = state;
(statearr_22944_22950[cljs.core.async.impl.ioc_helpers.STATE_IDX] = cont_block);


var temp__4425__auto__ = cljs.core.async.do_alts.call(null,((function (map__22942,map__22942__$1,opts){
return (function (val){
var statearr_22945_22951 = state;
(statearr_22945_22951[cljs.core.async.impl.ioc_helpers.VALUE_IDX] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state);
});})(map__22942,map__22942__$1,opts))
,ports,opts);
if(cljs.core.truth_(temp__4425__auto__)){
var cb = temp__4425__auto__;
var statearr_22946_22952 = state;
(statearr_22946_22952[cljs.core.async.impl.ioc_helpers.VALUE_IDX] = cljs.core.deref.call(null,cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
});

cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3);

cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq22937){
var G__22938 = cljs.core.first.call(null,seq22937);
var seq22937__$1 = cljs.core.next.call(null,seq22937);
var G__22939 = cljs.core.first.call(null,seq22937__$1);
var seq22937__$2 = cljs.core.next.call(null,seq22937__$1);
var G__22940 = cljs.core.first.call(null,seq22937__$2);
var seq22937__$3 = cljs.core.next.call(null,seq22937__$2);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__22938,G__22939,G__22940,seq22937__$3);
});
/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.call(null,solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.call(null);
var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){
return cljs.core.async.put_BANG_.call(null,change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;
var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){
return cljs.core.reduce_kv.call(null,((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){
if(cljs.core.truth_(attr.call(null,v))){
return cljs.core.conj.call(null,ret,c);
} else {
return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;
var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){
var chs = cljs.core.deref.call(null,cs);
var mode = cljs.core.deref.call(null,solo_mode);
var solos = pick.call(null,new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick.call(null,new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.call(null,(((cljs.core._EQ_.call(null,mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && (!(cljs.core.empty_QMARK_.call(null,solos))))?cljs.core.vec.call(null,solos):cljs.core.vec.call(null,cljs.core.remove.call(null,pauses,cljs.core.keys.call(null,chs)))),change)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;
var m = (function (){
if(typeof cljs.core.async.t_cljs$core$async23116 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async23116 = (function (change,mix,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta23117){
this.change = change;
this.mix = mix;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta23117 = meta23117;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async23116.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_23118,meta23117__$1){
var self__ = this;
var _23118__$1 = this;
return (new cljs.core.async.t_cljs$core$async23116(self__.change,self__.mix,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta23117__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async23116.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_23118){
var self__ = this;
var _23118__$1 = this;
return self__.meta23117;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async23116.prototype.cljs$core$async$Mux$ = true;

cljs.core.async.t_cljs$core$async23116.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async23116.prototype.cljs$core$async$Mix$ = true;

cljs.core.async.t_cljs$core$async23116.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async23116.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async23116.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async23116.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.partial.call(null,cljs.core.merge_with,cljs.core.merge),state_map);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async23116.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.solo_modes.call(null,mode))){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str([cljs.core.str("mode must be one of: "),cljs.core.str(self__.solo_modes)].join('')),cljs.core.str("\n"),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"mode","mode",-2000032078,null))))].join('')));
}

cljs.core.reset_BANG_.call(null,self__.solo_mode,mode);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async23116.getBasis = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (){
return new cljs.core.PersistentVector(null, 11, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),cljs.core.with_meta(new cljs.core.Symbol(null,"mix","mix",2121373763,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"out","out",729986010,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Creates and returns a mix of one or more input channels which will\n  be put on the supplied out channel. Input sources can be added to\n  the mix with 'admix', and removed with 'unmix'. A mix supports\n  soloing, muting and pausing multiple inputs atomically using\n  'toggle', and can solo using either muting or pausing as determined\n  by 'solo-mode'.\n\n  Each channel can have zero or more boolean modes set via 'toggle':\n\n  :solo - when true, only this (ond other soloed) channel(s) will appear\n          in the mix output channel. :mute and :pause states of soloed\n          channels are ignored. If solo-mode is :mute, non-soloed\n          channels are muted, if :pause, non-soloed channels are\n          paused.\n\n  :mute - muted channels will have their contents consumed but not included in the mix\n  :pause - paused channels will not have their contents consumed (and thus also not included in the mix)\n"], null)),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta23117","meta23117",170497431,null)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async23116.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async23116.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async23116";

cljs.core.async.t_cljs$core$async23116.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__17394__auto__,writer__17395__auto__,opt__17396__auto__){
return cljs.core._write.call(null,writer__17395__auto__,"cljs.core.async/t_cljs$core$async23116");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.__GT_t_cljs$core$async23116 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function cljs$core$async$mix_$___GT_t_cljs$core$async23116(change__$1,mix__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta23117){
return (new cljs.core.async.t_cljs$core$async23116(change__$1,mix__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta23117));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

}

return (new cljs.core.async.t_cljs$core$async23116(change,cljs$core$async$mix,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__21566__auto___23279 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___23279,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___23279,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (state_23216){
var state_val_23217 = (state_23216[(1)]);
if((state_val_23217 === (7))){
var inst_23134 = (state_23216[(2)]);
var state_23216__$1 = state_23216;
var statearr_23218_23280 = state_23216__$1;
(statearr_23218_23280[(2)] = inst_23134);

(statearr_23218_23280[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (20))){
var inst_23146 = (state_23216[(7)]);
var state_23216__$1 = state_23216;
var statearr_23219_23281 = state_23216__$1;
(statearr_23219_23281[(2)] = inst_23146);

(statearr_23219_23281[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (27))){
var state_23216__$1 = state_23216;
var statearr_23220_23282 = state_23216__$1;
(statearr_23220_23282[(2)] = null);

(statearr_23220_23282[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (1))){
var inst_23122 = (state_23216[(8)]);
var inst_23122__$1 = calc_state.call(null);
var inst_23124 = (inst_23122__$1 == null);
var inst_23125 = cljs.core.not.call(null,inst_23124);
var state_23216__$1 = (function (){var statearr_23221 = state_23216;
(statearr_23221[(8)] = inst_23122__$1);

return statearr_23221;
})();
if(inst_23125){
var statearr_23222_23283 = state_23216__$1;
(statearr_23222_23283[(1)] = (2));

} else {
var statearr_23223_23284 = state_23216__$1;
(statearr_23223_23284[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (24))){
var inst_23176 = (state_23216[(9)]);
var inst_23169 = (state_23216[(10)]);
var inst_23190 = (state_23216[(11)]);
var inst_23190__$1 = inst_23169.call(null,inst_23176);
var state_23216__$1 = (function (){var statearr_23224 = state_23216;
(statearr_23224[(11)] = inst_23190__$1);

return statearr_23224;
})();
if(cljs.core.truth_(inst_23190__$1)){
var statearr_23225_23285 = state_23216__$1;
(statearr_23225_23285[(1)] = (29));

} else {
var statearr_23226_23286 = state_23216__$1;
(statearr_23226_23286[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (4))){
var inst_23137 = (state_23216[(2)]);
var state_23216__$1 = state_23216;
if(cljs.core.truth_(inst_23137)){
var statearr_23227_23287 = state_23216__$1;
(statearr_23227_23287[(1)] = (8));

} else {
var statearr_23228_23288 = state_23216__$1;
(statearr_23228_23288[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (15))){
var inst_23163 = (state_23216[(2)]);
var state_23216__$1 = state_23216;
if(cljs.core.truth_(inst_23163)){
var statearr_23229_23289 = state_23216__$1;
(statearr_23229_23289[(1)] = (19));

} else {
var statearr_23230_23290 = state_23216__$1;
(statearr_23230_23290[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (21))){
var inst_23168 = (state_23216[(12)]);
var inst_23168__$1 = (state_23216[(2)]);
var inst_23169 = cljs.core.get.call(null,inst_23168__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_23170 = cljs.core.get.call(null,inst_23168__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_23171 = cljs.core.get.call(null,inst_23168__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_23216__$1 = (function (){var statearr_23231 = state_23216;
(statearr_23231[(12)] = inst_23168__$1);

(statearr_23231[(10)] = inst_23169);

(statearr_23231[(13)] = inst_23170);

return statearr_23231;
})();
return cljs.core.async.ioc_alts_BANG_.call(null,state_23216__$1,(22),inst_23171);
} else {
if((state_val_23217 === (31))){
var inst_23198 = (state_23216[(2)]);
var state_23216__$1 = state_23216;
if(cljs.core.truth_(inst_23198)){
var statearr_23232_23291 = state_23216__$1;
(statearr_23232_23291[(1)] = (32));

} else {
var statearr_23233_23292 = state_23216__$1;
(statearr_23233_23292[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (32))){
var inst_23175 = (state_23216[(14)]);
var state_23216__$1 = state_23216;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_23216__$1,(35),out,inst_23175);
} else {
if((state_val_23217 === (33))){
var inst_23168 = (state_23216[(12)]);
var inst_23146 = inst_23168;
var state_23216__$1 = (function (){var statearr_23234 = state_23216;
(statearr_23234[(7)] = inst_23146);

return statearr_23234;
})();
var statearr_23235_23293 = state_23216__$1;
(statearr_23235_23293[(2)] = null);

(statearr_23235_23293[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (13))){
var inst_23146 = (state_23216[(7)]);
var inst_23153 = inst_23146.cljs$lang$protocol_mask$partition0$;
var inst_23154 = (inst_23153 & (64));
var inst_23155 = inst_23146.cljs$core$ISeq$;
var inst_23156 = (inst_23154) || (inst_23155);
var state_23216__$1 = state_23216;
if(cljs.core.truth_(inst_23156)){
var statearr_23236_23294 = state_23216__$1;
(statearr_23236_23294[(1)] = (16));

} else {
var statearr_23237_23295 = state_23216__$1;
(statearr_23237_23295[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (22))){
var inst_23175 = (state_23216[(14)]);
var inst_23176 = (state_23216[(9)]);
var inst_23174 = (state_23216[(2)]);
var inst_23175__$1 = cljs.core.nth.call(null,inst_23174,(0),null);
var inst_23176__$1 = cljs.core.nth.call(null,inst_23174,(1),null);
var inst_23177 = (inst_23175__$1 == null);
var inst_23178 = cljs.core._EQ_.call(null,inst_23176__$1,change);
var inst_23179 = (inst_23177) || (inst_23178);
var state_23216__$1 = (function (){var statearr_23238 = state_23216;
(statearr_23238[(14)] = inst_23175__$1);

(statearr_23238[(9)] = inst_23176__$1);

return statearr_23238;
})();
if(cljs.core.truth_(inst_23179)){
var statearr_23239_23296 = state_23216__$1;
(statearr_23239_23296[(1)] = (23));

} else {
var statearr_23240_23297 = state_23216__$1;
(statearr_23240_23297[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (36))){
var inst_23168 = (state_23216[(12)]);
var inst_23146 = inst_23168;
var state_23216__$1 = (function (){var statearr_23241 = state_23216;
(statearr_23241[(7)] = inst_23146);

return statearr_23241;
})();
var statearr_23242_23298 = state_23216__$1;
(statearr_23242_23298[(2)] = null);

(statearr_23242_23298[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (29))){
var inst_23190 = (state_23216[(11)]);
var state_23216__$1 = state_23216;
var statearr_23243_23299 = state_23216__$1;
(statearr_23243_23299[(2)] = inst_23190);

(statearr_23243_23299[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (6))){
var state_23216__$1 = state_23216;
var statearr_23244_23300 = state_23216__$1;
(statearr_23244_23300[(2)] = false);

(statearr_23244_23300[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (28))){
var inst_23186 = (state_23216[(2)]);
var inst_23187 = calc_state.call(null);
var inst_23146 = inst_23187;
var state_23216__$1 = (function (){var statearr_23245 = state_23216;
(statearr_23245[(15)] = inst_23186);

(statearr_23245[(7)] = inst_23146);

return statearr_23245;
})();
var statearr_23246_23301 = state_23216__$1;
(statearr_23246_23301[(2)] = null);

(statearr_23246_23301[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (25))){
var inst_23212 = (state_23216[(2)]);
var state_23216__$1 = state_23216;
var statearr_23247_23302 = state_23216__$1;
(statearr_23247_23302[(2)] = inst_23212);

(statearr_23247_23302[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (34))){
var inst_23210 = (state_23216[(2)]);
var state_23216__$1 = state_23216;
var statearr_23248_23303 = state_23216__$1;
(statearr_23248_23303[(2)] = inst_23210);

(statearr_23248_23303[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (17))){
var state_23216__$1 = state_23216;
var statearr_23249_23304 = state_23216__$1;
(statearr_23249_23304[(2)] = false);

(statearr_23249_23304[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (3))){
var state_23216__$1 = state_23216;
var statearr_23250_23305 = state_23216__$1;
(statearr_23250_23305[(2)] = false);

(statearr_23250_23305[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (12))){
var inst_23214 = (state_23216[(2)]);
var state_23216__$1 = state_23216;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_23216__$1,inst_23214);
} else {
if((state_val_23217 === (2))){
var inst_23122 = (state_23216[(8)]);
var inst_23127 = inst_23122.cljs$lang$protocol_mask$partition0$;
var inst_23128 = (inst_23127 & (64));
var inst_23129 = inst_23122.cljs$core$ISeq$;
var inst_23130 = (inst_23128) || (inst_23129);
var state_23216__$1 = state_23216;
if(cljs.core.truth_(inst_23130)){
var statearr_23251_23306 = state_23216__$1;
(statearr_23251_23306[(1)] = (5));

} else {
var statearr_23252_23307 = state_23216__$1;
(statearr_23252_23307[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (23))){
var inst_23175 = (state_23216[(14)]);
var inst_23181 = (inst_23175 == null);
var state_23216__$1 = state_23216;
if(cljs.core.truth_(inst_23181)){
var statearr_23253_23308 = state_23216__$1;
(statearr_23253_23308[(1)] = (26));

} else {
var statearr_23254_23309 = state_23216__$1;
(statearr_23254_23309[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (35))){
var inst_23201 = (state_23216[(2)]);
var state_23216__$1 = state_23216;
if(cljs.core.truth_(inst_23201)){
var statearr_23255_23310 = state_23216__$1;
(statearr_23255_23310[(1)] = (36));

} else {
var statearr_23256_23311 = state_23216__$1;
(statearr_23256_23311[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (19))){
var inst_23146 = (state_23216[(7)]);
var inst_23165 = cljs.core.apply.call(null,cljs.core.hash_map,inst_23146);
var state_23216__$1 = state_23216;
var statearr_23257_23312 = state_23216__$1;
(statearr_23257_23312[(2)] = inst_23165);

(statearr_23257_23312[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (11))){
var inst_23146 = (state_23216[(7)]);
var inst_23150 = (inst_23146 == null);
var inst_23151 = cljs.core.not.call(null,inst_23150);
var state_23216__$1 = state_23216;
if(inst_23151){
var statearr_23258_23313 = state_23216__$1;
(statearr_23258_23313[(1)] = (13));

} else {
var statearr_23259_23314 = state_23216__$1;
(statearr_23259_23314[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (9))){
var inst_23122 = (state_23216[(8)]);
var state_23216__$1 = state_23216;
var statearr_23260_23315 = state_23216__$1;
(statearr_23260_23315[(2)] = inst_23122);

(statearr_23260_23315[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (5))){
var state_23216__$1 = state_23216;
var statearr_23261_23316 = state_23216__$1;
(statearr_23261_23316[(2)] = true);

(statearr_23261_23316[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (14))){
var state_23216__$1 = state_23216;
var statearr_23262_23317 = state_23216__$1;
(statearr_23262_23317[(2)] = false);

(statearr_23262_23317[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (26))){
var inst_23176 = (state_23216[(9)]);
var inst_23183 = cljs.core.swap_BANG_.call(null,cs,cljs.core.dissoc,inst_23176);
var state_23216__$1 = state_23216;
var statearr_23263_23318 = state_23216__$1;
(statearr_23263_23318[(2)] = inst_23183);

(statearr_23263_23318[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (16))){
var state_23216__$1 = state_23216;
var statearr_23264_23319 = state_23216__$1;
(statearr_23264_23319[(2)] = true);

(statearr_23264_23319[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (38))){
var inst_23206 = (state_23216[(2)]);
var state_23216__$1 = state_23216;
var statearr_23265_23320 = state_23216__$1;
(statearr_23265_23320[(2)] = inst_23206);

(statearr_23265_23320[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (30))){
var inst_23176 = (state_23216[(9)]);
var inst_23169 = (state_23216[(10)]);
var inst_23170 = (state_23216[(13)]);
var inst_23193 = cljs.core.empty_QMARK_.call(null,inst_23169);
var inst_23194 = inst_23170.call(null,inst_23176);
var inst_23195 = cljs.core.not.call(null,inst_23194);
var inst_23196 = (inst_23193) && (inst_23195);
var state_23216__$1 = state_23216;
var statearr_23266_23321 = state_23216__$1;
(statearr_23266_23321[(2)] = inst_23196);

(statearr_23266_23321[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (10))){
var inst_23122 = (state_23216[(8)]);
var inst_23142 = (state_23216[(2)]);
var inst_23143 = cljs.core.get.call(null,inst_23142,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_23144 = cljs.core.get.call(null,inst_23142,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_23145 = cljs.core.get.call(null,inst_23142,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_23146 = inst_23122;
var state_23216__$1 = (function (){var statearr_23267 = state_23216;
(statearr_23267[(16)] = inst_23144);

(statearr_23267[(17)] = inst_23145);

(statearr_23267[(7)] = inst_23146);

(statearr_23267[(18)] = inst_23143);

return statearr_23267;
})();
var statearr_23268_23322 = state_23216__$1;
(statearr_23268_23322[(2)] = null);

(statearr_23268_23322[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (18))){
var inst_23160 = (state_23216[(2)]);
var state_23216__$1 = state_23216;
var statearr_23269_23323 = state_23216__$1;
(statearr_23269_23323[(2)] = inst_23160);

(statearr_23269_23323[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (37))){
var state_23216__$1 = state_23216;
var statearr_23270_23324 = state_23216__$1;
(statearr_23270_23324[(2)] = null);

(statearr_23270_23324[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23217 === (8))){
var inst_23122 = (state_23216[(8)]);
var inst_23139 = cljs.core.apply.call(null,cljs.core.hash_map,inst_23122);
var state_23216__$1 = state_23216;
var statearr_23271_23325 = state_23216__$1;
(statearr_23271_23325[(2)] = inst_23139);

(statearr_23271_23325[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto___23279,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
;
return ((function (switch__21501__auto__,c__21566__auto___23279,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function() {
var cljs$core$async$mix_$_state_machine__21502__auto__ = null;
var cljs$core$async$mix_$_state_machine__21502__auto____0 = (function (){
var statearr_23275 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_23275[(0)] = cljs$core$async$mix_$_state_machine__21502__auto__);

(statearr_23275[(1)] = (1));

return statearr_23275;
});
var cljs$core$async$mix_$_state_machine__21502__auto____1 = (function (state_23216){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_23216);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e23276){if((e23276 instanceof Object)){
var ex__21505__auto__ = e23276;
var statearr_23277_23326 = state_23216;
(statearr_23277_23326[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_23216);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e23276;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__23327 = state_23216;
state_23216 = G__23327;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__21502__auto__ = function(state_23216){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__21502__auto____1.call(this,state_23216);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__21502__auto____0;
cljs$core$async$mix_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__21502__auto____1;
return cljs$core$async$mix_$_state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___23279,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
})();
var state__21568__auto__ = (function (){var statearr_23278 = f__21567__auto__.call(null);
(statearr_23278[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___23279);

return statearr_23278;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___23279,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
);


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_.call(null,mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_.call(null,mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_.call(null,mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_.call(null,mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_.call(null,mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((!((p == null))) && (!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
var x__17452__auto__ = (((p == null))?null:p);
var m__17453__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__17452__auto__)]);
if(!((m__17453__auto__ == null))){
return m__17453__auto__.call(null,p,v,ch,close_QMARK_);
} else {
var m__17453__auto____$1 = (cljs.core.async.sub_STAR_["_"]);
if(!((m__17453__auto____$1 == null))){
return m__17453__auto____$1.call(null,p,v,ch,close_QMARK_);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.sub*",p);
}
}
}
});

cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
var x__17452__auto__ = (((p == null))?null:p);
var m__17453__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__17452__auto__)]);
if(!((m__17453__auto__ == null))){
return m__17453__auto__.call(null,p,v,ch);
} else {
var m__17453__auto____$1 = (cljs.core.async.unsub_STAR_["_"]);
if(!((m__17453__auto____$1 == null))){
return m__17453__auto____$1.call(null,p,v,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var args23328 = [];
var len__17855__auto___23331 = arguments.length;
var i__17856__auto___23332 = (0);
while(true){
if((i__17856__auto___23332 < len__17855__auto___23331)){
args23328.push((arguments[i__17856__auto___23332]));

var G__23333 = (i__17856__auto___23332 + (1));
i__17856__auto___23332 = G__23333;
continue;
} else {
}
break;
}

var G__23330 = args23328.length;
switch (G__23330) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args23328.length)].join('')));

}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
var x__17452__auto__ = (((p == null))?null:p);
var m__17453__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__17452__auto__)]);
if(!((m__17453__auto__ == null))){
return m__17453__auto__.call(null,p);
} else {
var m__17453__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(!((m__17453__auto____$1 == null))){
return m__17453__auto____$1.call(null,p);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
var x__17452__auto__ = (((p == null))?null:p);
var m__17453__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__17452__auto__)]);
if(!((m__17453__auto__ == null))){
return m__17453__auto__.call(null,p,v);
} else {
var m__17453__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(!((m__17453__auto____$1 == null))){
return m__17453__auto____$1.call(null,p,v);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2;

/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var args23336 = [];
var len__17855__auto___23461 = arguments.length;
var i__17856__auto___23462 = (0);
while(true){
if((i__17856__auto___23462 < len__17855__auto___23461)){
args23336.push((arguments[i__17856__auto___23462]));

var G__23463 = (i__17856__auto___23462 + (1));
i__17856__auto___23462 = G__23463;
continue;
} else {
}
break;
}

var G__23338 = args23336.length;
switch (G__23338) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args23336.length)].join('')));

}
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.call(null,ch,topic_fn,cljs.core.constantly.call(null,null));
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = ((function (mults){
return (function (topic){
var or__16796__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,mults),topic);
if(cljs.core.truth_(or__16796__auto__)){
return or__16796__auto__;
} else {
return cljs.core.get.call(null,cljs.core.swap_BANG_.call(null,mults,((function (or__16796__auto__,mults){
return (function (p1__23335_SHARP_){
if(cljs.core.truth_(p1__23335_SHARP_.call(null,topic))){
return p1__23335_SHARP_;
} else {
return cljs.core.assoc.call(null,p1__23335_SHARP_,topic,cljs.core.async.mult.call(null,cljs.core.async.chan.call(null,buf_fn.call(null,topic))));
}
});})(or__16796__auto__,mults))
),topic);
}
});})(mults))
;
var p = (function (){
if(typeof cljs.core.async.t_cljs$core$async23339 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async23339 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta23340){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta23340 = meta23340;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async23339.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_23341,meta23340__$1){
var self__ = this;
var _23341__$1 = this;
return (new cljs.core.async.t_cljs$core$async23339(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta23340__$1));
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async23339.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_23341){
var self__ = this;
var _23341__$1 = this;
return self__.meta23340;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async23339.prototype.cljs$core$async$Mux$ = true;

cljs.core.async.t_cljs$core$async23339.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async23339.prototype.cljs$core$async$Pub$ = true;

cljs.core.async.t_cljs$core$async23339.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = self__.ensure_mult.call(null,topic);
return cljs.core.async.tap.call(null,m,ch__$1,close_QMARK_);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async23339.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__4425__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,self__.mults),topic);
if(cljs.core.truth_(temp__4425__auto__)){
var m = temp__4425__auto__;
return cljs.core.async.untap.call(null,m,ch__$1);
} else {
return null;
}
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async23339.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_.call(null,self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async23339.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.call(null,self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async23339.getBasis = ((function (mults,ensure_mult){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta23340","meta23340",-1190090466,null)], null);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async23339.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async23339.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async23339";

cljs.core.async.t_cljs$core$async23339.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__17394__auto__,writer__17395__auto__,opt__17396__auto__){
return cljs.core._write.call(null,writer__17395__auto__,"cljs.core.async/t_cljs$core$async23339");
});})(mults,ensure_mult))
;

cljs.core.async.__GT_t_cljs$core$async23339 = ((function (mults,ensure_mult){
return (function cljs$core$async$__GT_t_cljs$core$async23339(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta23340){
return (new cljs.core.async.t_cljs$core$async23339(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta23340));
});})(mults,ensure_mult))
;

}

return (new cljs.core.async.t_cljs$core$async23339(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__21566__auto___23465 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___23465,mults,ensure_mult,p){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___23465,mults,ensure_mult,p){
return (function (state_23413){
var state_val_23414 = (state_23413[(1)]);
if((state_val_23414 === (7))){
var inst_23409 = (state_23413[(2)]);
var state_23413__$1 = state_23413;
var statearr_23415_23466 = state_23413__$1;
(statearr_23415_23466[(2)] = inst_23409);

(statearr_23415_23466[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (20))){
var state_23413__$1 = state_23413;
var statearr_23416_23467 = state_23413__$1;
(statearr_23416_23467[(2)] = null);

(statearr_23416_23467[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (1))){
var state_23413__$1 = state_23413;
var statearr_23417_23468 = state_23413__$1;
(statearr_23417_23468[(2)] = null);

(statearr_23417_23468[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (24))){
var inst_23392 = (state_23413[(7)]);
var inst_23401 = cljs.core.swap_BANG_.call(null,mults,cljs.core.dissoc,inst_23392);
var state_23413__$1 = state_23413;
var statearr_23418_23469 = state_23413__$1;
(statearr_23418_23469[(2)] = inst_23401);

(statearr_23418_23469[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (4))){
var inst_23344 = (state_23413[(8)]);
var inst_23344__$1 = (state_23413[(2)]);
var inst_23345 = (inst_23344__$1 == null);
var state_23413__$1 = (function (){var statearr_23419 = state_23413;
(statearr_23419[(8)] = inst_23344__$1);

return statearr_23419;
})();
if(cljs.core.truth_(inst_23345)){
var statearr_23420_23470 = state_23413__$1;
(statearr_23420_23470[(1)] = (5));

} else {
var statearr_23421_23471 = state_23413__$1;
(statearr_23421_23471[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (15))){
var inst_23386 = (state_23413[(2)]);
var state_23413__$1 = state_23413;
var statearr_23422_23472 = state_23413__$1;
(statearr_23422_23472[(2)] = inst_23386);

(statearr_23422_23472[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (21))){
var inst_23406 = (state_23413[(2)]);
var state_23413__$1 = (function (){var statearr_23423 = state_23413;
(statearr_23423[(9)] = inst_23406);

return statearr_23423;
})();
var statearr_23424_23473 = state_23413__$1;
(statearr_23424_23473[(2)] = null);

(statearr_23424_23473[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (13))){
var inst_23368 = (state_23413[(10)]);
var inst_23370 = cljs.core.chunked_seq_QMARK_.call(null,inst_23368);
var state_23413__$1 = state_23413;
if(inst_23370){
var statearr_23425_23474 = state_23413__$1;
(statearr_23425_23474[(1)] = (16));

} else {
var statearr_23426_23475 = state_23413__$1;
(statearr_23426_23475[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (22))){
var inst_23398 = (state_23413[(2)]);
var state_23413__$1 = state_23413;
if(cljs.core.truth_(inst_23398)){
var statearr_23427_23476 = state_23413__$1;
(statearr_23427_23476[(1)] = (23));

} else {
var statearr_23428_23477 = state_23413__$1;
(statearr_23428_23477[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (6))){
var inst_23392 = (state_23413[(7)]);
var inst_23394 = (state_23413[(11)]);
var inst_23344 = (state_23413[(8)]);
var inst_23392__$1 = topic_fn.call(null,inst_23344);
var inst_23393 = cljs.core.deref.call(null,mults);
var inst_23394__$1 = cljs.core.get.call(null,inst_23393,inst_23392__$1);
var state_23413__$1 = (function (){var statearr_23429 = state_23413;
(statearr_23429[(7)] = inst_23392__$1);

(statearr_23429[(11)] = inst_23394__$1);

return statearr_23429;
})();
if(cljs.core.truth_(inst_23394__$1)){
var statearr_23430_23478 = state_23413__$1;
(statearr_23430_23478[(1)] = (19));

} else {
var statearr_23431_23479 = state_23413__$1;
(statearr_23431_23479[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (25))){
var inst_23403 = (state_23413[(2)]);
var state_23413__$1 = state_23413;
var statearr_23432_23480 = state_23413__$1;
(statearr_23432_23480[(2)] = inst_23403);

(statearr_23432_23480[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (17))){
var inst_23368 = (state_23413[(10)]);
var inst_23377 = cljs.core.first.call(null,inst_23368);
var inst_23378 = cljs.core.async.muxch_STAR_.call(null,inst_23377);
var inst_23379 = cljs.core.async.close_BANG_.call(null,inst_23378);
var inst_23380 = cljs.core.next.call(null,inst_23368);
var inst_23354 = inst_23380;
var inst_23355 = null;
var inst_23356 = (0);
var inst_23357 = (0);
var state_23413__$1 = (function (){var statearr_23433 = state_23413;
(statearr_23433[(12)] = inst_23379);

(statearr_23433[(13)] = inst_23357);

(statearr_23433[(14)] = inst_23354);

(statearr_23433[(15)] = inst_23356);

(statearr_23433[(16)] = inst_23355);

return statearr_23433;
})();
var statearr_23434_23481 = state_23413__$1;
(statearr_23434_23481[(2)] = null);

(statearr_23434_23481[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (3))){
var inst_23411 = (state_23413[(2)]);
var state_23413__$1 = state_23413;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_23413__$1,inst_23411);
} else {
if((state_val_23414 === (12))){
var inst_23388 = (state_23413[(2)]);
var state_23413__$1 = state_23413;
var statearr_23435_23482 = state_23413__$1;
(statearr_23435_23482[(2)] = inst_23388);

(statearr_23435_23482[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (2))){
var state_23413__$1 = state_23413;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_23413__$1,(4),ch);
} else {
if((state_val_23414 === (23))){
var state_23413__$1 = state_23413;
var statearr_23436_23483 = state_23413__$1;
(statearr_23436_23483[(2)] = null);

(statearr_23436_23483[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (19))){
var inst_23394 = (state_23413[(11)]);
var inst_23344 = (state_23413[(8)]);
var inst_23396 = cljs.core.async.muxch_STAR_.call(null,inst_23394);
var state_23413__$1 = state_23413;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_23413__$1,(22),inst_23396,inst_23344);
} else {
if((state_val_23414 === (11))){
var inst_23354 = (state_23413[(14)]);
var inst_23368 = (state_23413[(10)]);
var inst_23368__$1 = cljs.core.seq.call(null,inst_23354);
var state_23413__$1 = (function (){var statearr_23437 = state_23413;
(statearr_23437[(10)] = inst_23368__$1);

return statearr_23437;
})();
if(inst_23368__$1){
var statearr_23438_23484 = state_23413__$1;
(statearr_23438_23484[(1)] = (13));

} else {
var statearr_23439_23485 = state_23413__$1;
(statearr_23439_23485[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (9))){
var inst_23390 = (state_23413[(2)]);
var state_23413__$1 = state_23413;
var statearr_23440_23486 = state_23413__$1;
(statearr_23440_23486[(2)] = inst_23390);

(statearr_23440_23486[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (5))){
var inst_23351 = cljs.core.deref.call(null,mults);
var inst_23352 = cljs.core.vals.call(null,inst_23351);
var inst_23353 = cljs.core.seq.call(null,inst_23352);
var inst_23354 = inst_23353;
var inst_23355 = null;
var inst_23356 = (0);
var inst_23357 = (0);
var state_23413__$1 = (function (){var statearr_23441 = state_23413;
(statearr_23441[(13)] = inst_23357);

(statearr_23441[(14)] = inst_23354);

(statearr_23441[(15)] = inst_23356);

(statearr_23441[(16)] = inst_23355);

return statearr_23441;
})();
var statearr_23442_23487 = state_23413__$1;
(statearr_23442_23487[(2)] = null);

(statearr_23442_23487[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (14))){
var state_23413__$1 = state_23413;
var statearr_23446_23488 = state_23413__$1;
(statearr_23446_23488[(2)] = null);

(statearr_23446_23488[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (16))){
var inst_23368 = (state_23413[(10)]);
var inst_23372 = cljs.core.chunk_first.call(null,inst_23368);
var inst_23373 = cljs.core.chunk_rest.call(null,inst_23368);
var inst_23374 = cljs.core.count.call(null,inst_23372);
var inst_23354 = inst_23373;
var inst_23355 = inst_23372;
var inst_23356 = inst_23374;
var inst_23357 = (0);
var state_23413__$1 = (function (){var statearr_23447 = state_23413;
(statearr_23447[(13)] = inst_23357);

(statearr_23447[(14)] = inst_23354);

(statearr_23447[(15)] = inst_23356);

(statearr_23447[(16)] = inst_23355);

return statearr_23447;
})();
var statearr_23448_23489 = state_23413__$1;
(statearr_23448_23489[(2)] = null);

(statearr_23448_23489[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (10))){
var inst_23357 = (state_23413[(13)]);
var inst_23354 = (state_23413[(14)]);
var inst_23356 = (state_23413[(15)]);
var inst_23355 = (state_23413[(16)]);
var inst_23362 = cljs.core._nth.call(null,inst_23355,inst_23357);
var inst_23363 = cljs.core.async.muxch_STAR_.call(null,inst_23362);
var inst_23364 = cljs.core.async.close_BANG_.call(null,inst_23363);
var inst_23365 = (inst_23357 + (1));
var tmp23443 = inst_23354;
var tmp23444 = inst_23356;
var tmp23445 = inst_23355;
var inst_23354__$1 = tmp23443;
var inst_23355__$1 = tmp23445;
var inst_23356__$1 = tmp23444;
var inst_23357__$1 = inst_23365;
var state_23413__$1 = (function (){var statearr_23449 = state_23413;
(statearr_23449[(17)] = inst_23364);

(statearr_23449[(13)] = inst_23357__$1);

(statearr_23449[(14)] = inst_23354__$1);

(statearr_23449[(15)] = inst_23356__$1);

(statearr_23449[(16)] = inst_23355__$1);

return statearr_23449;
})();
var statearr_23450_23490 = state_23413__$1;
(statearr_23450_23490[(2)] = null);

(statearr_23450_23490[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (18))){
var inst_23383 = (state_23413[(2)]);
var state_23413__$1 = state_23413;
var statearr_23451_23491 = state_23413__$1;
(statearr_23451_23491[(2)] = inst_23383);

(statearr_23451_23491[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23414 === (8))){
var inst_23357 = (state_23413[(13)]);
var inst_23356 = (state_23413[(15)]);
var inst_23359 = (inst_23357 < inst_23356);
var inst_23360 = inst_23359;
var state_23413__$1 = state_23413;
if(cljs.core.truth_(inst_23360)){
var statearr_23452_23492 = state_23413__$1;
(statearr_23452_23492[(1)] = (10));

} else {
var statearr_23453_23493 = state_23413__$1;
(statearr_23453_23493[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto___23465,mults,ensure_mult,p))
;
return ((function (switch__21501__auto__,c__21566__auto___23465,mults,ensure_mult,p){
return (function() {
var cljs$core$async$state_machine__21502__auto__ = null;
var cljs$core$async$state_machine__21502__auto____0 = (function (){
var statearr_23457 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_23457[(0)] = cljs$core$async$state_machine__21502__auto__);

(statearr_23457[(1)] = (1));

return statearr_23457;
});
var cljs$core$async$state_machine__21502__auto____1 = (function (state_23413){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_23413);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e23458){if((e23458 instanceof Object)){
var ex__21505__auto__ = e23458;
var statearr_23459_23494 = state_23413;
(statearr_23459_23494[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_23413);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e23458;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__23495 = state_23413;
state_23413 = G__23495;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$state_machine__21502__auto__ = function(state_23413){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21502__auto____1.call(this,state_23413);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21502__auto____0;
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21502__auto____1;
return cljs$core$async$state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___23465,mults,ensure_mult,p))
})();
var state__21568__auto__ = (function (){var statearr_23460 = f__21567__auto__.call(null);
(statearr_23460[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___23465);

return statearr_23460;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___23465,mults,ensure_mult,p))
);


return p;
});

cljs.core.async.pub.cljs$lang$maxFixedArity = 3;
/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var args23496 = [];
var len__17855__auto___23499 = arguments.length;
var i__17856__auto___23500 = (0);
while(true){
if((i__17856__auto___23500 < len__17855__auto___23499)){
args23496.push((arguments[i__17856__auto___23500]));

var G__23501 = (i__17856__auto___23500 + (1));
i__17856__auto___23500 = G__23501;
continue;
} else {
}
break;
}

var G__23498 = args23496.length;
switch (G__23498) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args23496.length)].join('')));

}
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.call(null,p,topic,ch,true);
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_.call(null,p,topic,ch,close_QMARK_);
});

cljs.core.async.sub.cljs$lang$maxFixedArity = 4;
/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_.call(null,p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var args23503 = [];
var len__17855__auto___23506 = arguments.length;
var i__17856__auto___23507 = (0);
while(true){
if((i__17856__auto___23507 < len__17855__auto___23506)){
args23503.push((arguments[i__17856__auto___23507]));

var G__23508 = (i__17856__auto___23507 + (1));
i__17856__auto___23507 = G__23508;
continue;
} else {
}
break;
}

var G__23505 = args23503.length;
switch (G__23505) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args23503.length)].join('')));

}
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_.call(null,p);
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_.call(null,p,topic);
});

cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2;
/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var args23510 = [];
var len__17855__auto___23581 = arguments.length;
var i__17856__auto___23582 = (0);
while(true){
if((i__17856__auto___23582 < len__17855__auto___23581)){
args23510.push((arguments[i__17856__auto___23582]));

var G__23583 = (i__17856__auto___23582 + (1));
i__17856__auto___23582 = G__23583;
continue;
} else {
}
break;
}

var G__23512 = args23510.length;
switch (G__23512) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args23510.length)].join('')));

}
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.call(null,f,chs,null);
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec.call(null,chs);
var out = cljs.core.async.chan.call(null,buf_or_n);
var cnt = cljs.core.count.call(null,chs__$1);
var rets = cljs.core.object_array.call(null,cnt);
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = cljs.core.mapv.call(null,((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){
return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,rets.slice((0)));
} else {
return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.call(null,cnt));
var c__21566__auto___23585 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___23585,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___23585,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (state_23551){
var state_val_23552 = (state_23551[(1)]);
if((state_val_23552 === (7))){
var state_23551__$1 = state_23551;
var statearr_23553_23586 = state_23551__$1;
(statearr_23553_23586[(2)] = null);

(statearr_23553_23586[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23552 === (1))){
var state_23551__$1 = state_23551;
var statearr_23554_23587 = state_23551__$1;
(statearr_23554_23587[(2)] = null);

(statearr_23554_23587[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23552 === (4))){
var inst_23515 = (state_23551[(7)]);
var inst_23517 = (inst_23515 < cnt);
var state_23551__$1 = state_23551;
if(cljs.core.truth_(inst_23517)){
var statearr_23555_23588 = state_23551__$1;
(statearr_23555_23588[(1)] = (6));

} else {
var statearr_23556_23589 = state_23551__$1;
(statearr_23556_23589[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23552 === (15))){
var inst_23547 = (state_23551[(2)]);
var state_23551__$1 = state_23551;
var statearr_23557_23590 = state_23551__$1;
(statearr_23557_23590[(2)] = inst_23547);

(statearr_23557_23590[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23552 === (13))){
var inst_23540 = cljs.core.async.close_BANG_.call(null,out);
var state_23551__$1 = state_23551;
var statearr_23558_23591 = state_23551__$1;
(statearr_23558_23591[(2)] = inst_23540);

(statearr_23558_23591[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23552 === (6))){
var state_23551__$1 = state_23551;
var statearr_23559_23592 = state_23551__$1;
(statearr_23559_23592[(2)] = null);

(statearr_23559_23592[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23552 === (3))){
var inst_23549 = (state_23551[(2)]);
var state_23551__$1 = state_23551;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_23551__$1,inst_23549);
} else {
if((state_val_23552 === (12))){
var inst_23537 = (state_23551[(8)]);
var inst_23537__$1 = (state_23551[(2)]);
var inst_23538 = cljs.core.some.call(null,cljs.core.nil_QMARK_,inst_23537__$1);
var state_23551__$1 = (function (){var statearr_23560 = state_23551;
(statearr_23560[(8)] = inst_23537__$1);

return statearr_23560;
})();
if(cljs.core.truth_(inst_23538)){
var statearr_23561_23593 = state_23551__$1;
(statearr_23561_23593[(1)] = (13));

} else {
var statearr_23562_23594 = state_23551__$1;
(statearr_23562_23594[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23552 === (2))){
var inst_23514 = cljs.core.reset_BANG_.call(null,dctr,cnt);
var inst_23515 = (0);
var state_23551__$1 = (function (){var statearr_23563 = state_23551;
(statearr_23563[(9)] = inst_23514);

(statearr_23563[(7)] = inst_23515);

return statearr_23563;
})();
var statearr_23564_23595 = state_23551__$1;
(statearr_23564_23595[(2)] = null);

(statearr_23564_23595[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23552 === (11))){
var inst_23515 = (state_23551[(7)]);
var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_23551,(10),Object,null,(9));
var inst_23524 = chs__$1.call(null,inst_23515);
var inst_23525 = done.call(null,inst_23515);
var inst_23526 = cljs.core.async.take_BANG_.call(null,inst_23524,inst_23525);
var state_23551__$1 = state_23551;
var statearr_23565_23596 = state_23551__$1;
(statearr_23565_23596[(2)] = inst_23526);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_23551__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23552 === (9))){
var inst_23515 = (state_23551[(7)]);
var inst_23528 = (state_23551[(2)]);
var inst_23529 = (inst_23515 + (1));
var inst_23515__$1 = inst_23529;
var state_23551__$1 = (function (){var statearr_23566 = state_23551;
(statearr_23566[(7)] = inst_23515__$1);

(statearr_23566[(10)] = inst_23528);

return statearr_23566;
})();
var statearr_23567_23597 = state_23551__$1;
(statearr_23567_23597[(2)] = null);

(statearr_23567_23597[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23552 === (5))){
var inst_23535 = (state_23551[(2)]);
var state_23551__$1 = (function (){var statearr_23568 = state_23551;
(statearr_23568[(11)] = inst_23535);

return statearr_23568;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_23551__$1,(12),dchan);
} else {
if((state_val_23552 === (14))){
var inst_23537 = (state_23551[(8)]);
var inst_23542 = cljs.core.apply.call(null,f,inst_23537);
var state_23551__$1 = state_23551;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_23551__$1,(16),out,inst_23542);
} else {
if((state_val_23552 === (16))){
var inst_23544 = (state_23551[(2)]);
var state_23551__$1 = (function (){var statearr_23569 = state_23551;
(statearr_23569[(12)] = inst_23544);

return statearr_23569;
})();
var statearr_23570_23598 = state_23551__$1;
(statearr_23570_23598[(2)] = null);

(statearr_23570_23598[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23552 === (10))){
var inst_23519 = (state_23551[(2)]);
var inst_23520 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);
var state_23551__$1 = (function (){var statearr_23571 = state_23551;
(statearr_23571[(13)] = inst_23519);

return statearr_23571;
})();
var statearr_23572_23599 = state_23551__$1;
(statearr_23572_23599[(2)] = inst_23520);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_23551__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23552 === (8))){
var inst_23533 = (state_23551[(2)]);
var state_23551__$1 = state_23551;
var statearr_23573_23600 = state_23551__$1;
(statearr_23573_23600[(2)] = inst_23533);

(statearr_23573_23600[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto___23585,chs__$1,out,cnt,rets,dchan,dctr,done))
;
return ((function (switch__21501__auto__,c__21566__auto___23585,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function() {
var cljs$core$async$state_machine__21502__auto__ = null;
var cljs$core$async$state_machine__21502__auto____0 = (function (){
var statearr_23577 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_23577[(0)] = cljs$core$async$state_machine__21502__auto__);

(statearr_23577[(1)] = (1));

return statearr_23577;
});
var cljs$core$async$state_machine__21502__auto____1 = (function (state_23551){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_23551);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e23578){if((e23578 instanceof Object)){
var ex__21505__auto__ = e23578;
var statearr_23579_23601 = state_23551;
(statearr_23579_23601[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_23551);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e23578;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__23602 = state_23551;
state_23551 = G__23602;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$state_machine__21502__auto__ = function(state_23551){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21502__auto____1.call(this,state_23551);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21502__auto____0;
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21502__auto____1;
return cljs$core$async$state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___23585,chs__$1,out,cnt,rets,dchan,dctr,done))
})();
var state__21568__auto__ = (function (){var statearr_23580 = f__21567__auto__.call(null);
(statearr_23580[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___23585);

return statearr_23580;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___23585,chs__$1,out,cnt,rets,dchan,dctr,done))
);


return out;
});

cljs.core.async.map.cljs$lang$maxFixedArity = 3;
/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var args23604 = [];
var len__17855__auto___23660 = arguments.length;
var i__17856__auto___23661 = (0);
while(true){
if((i__17856__auto___23661 < len__17855__auto___23660)){
args23604.push((arguments[i__17856__auto___23661]));

var G__23662 = (i__17856__auto___23661 + (1));
i__17856__auto___23661 = G__23662;
continue;
} else {
}
break;
}

var G__23606 = args23604.length;
switch (G__23606) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args23604.length)].join('')));

}
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.call(null,chs,null);
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__21566__auto___23664 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___23664,out){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___23664,out){
return (function (state_23636){
var state_val_23637 = (state_23636[(1)]);
if((state_val_23637 === (7))){
var inst_23615 = (state_23636[(7)]);
var inst_23616 = (state_23636[(8)]);
var inst_23615__$1 = (state_23636[(2)]);
var inst_23616__$1 = cljs.core.nth.call(null,inst_23615__$1,(0),null);
var inst_23617 = cljs.core.nth.call(null,inst_23615__$1,(1),null);
var inst_23618 = (inst_23616__$1 == null);
var state_23636__$1 = (function (){var statearr_23638 = state_23636;
(statearr_23638[(7)] = inst_23615__$1);

(statearr_23638[(8)] = inst_23616__$1);

(statearr_23638[(9)] = inst_23617);

return statearr_23638;
})();
if(cljs.core.truth_(inst_23618)){
var statearr_23639_23665 = state_23636__$1;
(statearr_23639_23665[(1)] = (8));

} else {
var statearr_23640_23666 = state_23636__$1;
(statearr_23640_23666[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23637 === (1))){
var inst_23607 = cljs.core.vec.call(null,chs);
var inst_23608 = inst_23607;
var state_23636__$1 = (function (){var statearr_23641 = state_23636;
(statearr_23641[(10)] = inst_23608);

return statearr_23641;
})();
var statearr_23642_23667 = state_23636__$1;
(statearr_23642_23667[(2)] = null);

(statearr_23642_23667[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23637 === (4))){
var inst_23608 = (state_23636[(10)]);
var state_23636__$1 = state_23636;
return cljs.core.async.ioc_alts_BANG_.call(null,state_23636__$1,(7),inst_23608);
} else {
if((state_val_23637 === (6))){
var inst_23632 = (state_23636[(2)]);
var state_23636__$1 = state_23636;
var statearr_23643_23668 = state_23636__$1;
(statearr_23643_23668[(2)] = inst_23632);

(statearr_23643_23668[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23637 === (3))){
var inst_23634 = (state_23636[(2)]);
var state_23636__$1 = state_23636;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_23636__$1,inst_23634);
} else {
if((state_val_23637 === (2))){
var inst_23608 = (state_23636[(10)]);
var inst_23610 = cljs.core.count.call(null,inst_23608);
var inst_23611 = (inst_23610 > (0));
var state_23636__$1 = state_23636;
if(cljs.core.truth_(inst_23611)){
var statearr_23645_23669 = state_23636__$1;
(statearr_23645_23669[(1)] = (4));

} else {
var statearr_23646_23670 = state_23636__$1;
(statearr_23646_23670[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23637 === (11))){
var inst_23608 = (state_23636[(10)]);
var inst_23625 = (state_23636[(2)]);
var tmp23644 = inst_23608;
var inst_23608__$1 = tmp23644;
var state_23636__$1 = (function (){var statearr_23647 = state_23636;
(statearr_23647[(11)] = inst_23625);

(statearr_23647[(10)] = inst_23608__$1);

return statearr_23647;
})();
var statearr_23648_23671 = state_23636__$1;
(statearr_23648_23671[(2)] = null);

(statearr_23648_23671[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23637 === (9))){
var inst_23616 = (state_23636[(8)]);
var state_23636__$1 = state_23636;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_23636__$1,(11),out,inst_23616);
} else {
if((state_val_23637 === (5))){
var inst_23630 = cljs.core.async.close_BANG_.call(null,out);
var state_23636__$1 = state_23636;
var statearr_23649_23672 = state_23636__$1;
(statearr_23649_23672[(2)] = inst_23630);

(statearr_23649_23672[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23637 === (10))){
var inst_23628 = (state_23636[(2)]);
var state_23636__$1 = state_23636;
var statearr_23650_23673 = state_23636__$1;
(statearr_23650_23673[(2)] = inst_23628);

(statearr_23650_23673[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23637 === (8))){
var inst_23615 = (state_23636[(7)]);
var inst_23608 = (state_23636[(10)]);
var inst_23616 = (state_23636[(8)]);
var inst_23617 = (state_23636[(9)]);
var inst_23620 = (function (){var cs = inst_23608;
var vec__23613 = inst_23615;
var v = inst_23616;
var c = inst_23617;
return ((function (cs,vec__23613,v,c,inst_23615,inst_23608,inst_23616,inst_23617,state_val_23637,c__21566__auto___23664,out){
return (function (p1__23603_SHARP_){
return cljs.core.not_EQ_.call(null,c,p1__23603_SHARP_);
});
;})(cs,vec__23613,v,c,inst_23615,inst_23608,inst_23616,inst_23617,state_val_23637,c__21566__auto___23664,out))
})();
var inst_23621 = cljs.core.filterv.call(null,inst_23620,inst_23608);
var inst_23608__$1 = inst_23621;
var state_23636__$1 = (function (){var statearr_23651 = state_23636;
(statearr_23651[(10)] = inst_23608__$1);

return statearr_23651;
})();
var statearr_23652_23674 = state_23636__$1;
(statearr_23652_23674[(2)] = null);

(statearr_23652_23674[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto___23664,out))
;
return ((function (switch__21501__auto__,c__21566__auto___23664,out){
return (function() {
var cljs$core$async$state_machine__21502__auto__ = null;
var cljs$core$async$state_machine__21502__auto____0 = (function (){
var statearr_23656 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_23656[(0)] = cljs$core$async$state_machine__21502__auto__);

(statearr_23656[(1)] = (1));

return statearr_23656;
});
var cljs$core$async$state_machine__21502__auto____1 = (function (state_23636){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_23636);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e23657){if((e23657 instanceof Object)){
var ex__21505__auto__ = e23657;
var statearr_23658_23675 = state_23636;
(statearr_23658_23675[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_23636);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e23657;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__23676 = state_23636;
state_23636 = G__23676;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$state_machine__21502__auto__ = function(state_23636){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21502__auto____1.call(this,state_23636);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21502__auto____0;
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21502__auto____1;
return cljs$core$async$state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___23664,out))
})();
var state__21568__auto__ = (function (){var statearr_23659 = f__21567__auto__.call(null);
(statearr_23659[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___23664);

return statearr_23659;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___23664,out))
);


return out;
});

cljs.core.async.merge.cljs$lang$maxFixedArity = 2;
/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce.call(null,cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var args23677 = [];
var len__17855__auto___23726 = arguments.length;
var i__17856__auto___23727 = (0);
while(true){
if((i__17856__auto___23727 < len__17855__auto___23726)){
args23677.push((arguments[i__17856__auto___23727]));

var G__23728 = (i__17856__auto___23727 + (1));
i__17856__auto___23727 = G__23728;
continue;
} else {
}
break;
}

var G__23679 = args23677.length;
switch (G__23679) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args23677.length)].join('')));

}
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.call(null,n,ch,null);
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__21566__auto___23730 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___23730,out){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___23730,out){
return (function (state_23703){
var state_val_23704 = (state_23703[(1)]);
if((state_val_23704 === (7))){
var inst_23685 = (state_23703[(7)]);
var inst_23685__$1 = (state_23703[(2)]);
var inst_23686 = (inst_23685__$1 == null);
var inst_23687 = cljs.core.not.call(null,inst_23686);
var state_23703__$1 = (function (){var statearr_23705 = state_23703;
(statearr_23705[(7)] = inst_23685__$1);

return statearr_23705;
})();
if(inst_23687){
var statearr_23706_23731 = state_23703__$1;
(statearr_23706_23731[(1)] = (8));

} else {
var statearr_23707_23732 = state_23703__$1;
(statearr_23707_23732[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23704 === (1))){
var inst_23680 = (0);
var state_23703__$1 = (function (){var statearr_23708 = state_23703;
(statearr_23708[(8)] = inst_23680);

return statearr_23708;
})();
var statearr_23709_23733 = state_23703__$1;
(statearr_23709_23733[(2)] = null);

(statearr_23709_23733[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23704 === (4))){
var state_23703__$1 = state_23703;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_23703__$1,(7),ch);
} else {
if((state_val_23704 === (6))){
var inst_23698 = (state_23703[(2)]);
var state_23703__$1 = state_23703;
var statearr_23710_23734 = state_23703__$1;
(statearr_23710_23734[(2)] = inst_23698);

(statearr_23710_23734[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23704 === (3))){
var inst_23700 = (state_23703[(2)]);
var inst_23701 = cljs.core.async.close_BANG_.call(null,out);
var state_23703__$1 = (function (){var statearr_23711 = state_23703;
(statearr_23711[(9)] = inst_23700);

return statearr_23711;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_23703__$1,inst_23701);
} else {
if((state_val_23704 === (2))){
var inst_23680 = (state_23703[(8)]);
var inst_23682 = (inst_23680 < n);
var state_23703__$1 = state_23703;
if(cljs.core.truth_(inst_23682)){
var statearr_23712_23735 = state_23703__$1;
(statearr_23712_23735[(1)] = (4));

} else {
var statearr_23713_23736 = state_23703__$1;
(statearr_23713_23736[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23704 === (11))){
var inst_23680 = (state_23703[(8)]);
var inst_23690 = (state_23703[(2)]);
var inst_23691 = (inst_23680 + (1));
var inst_23680__$1 = inst_23691;
var state_23703__$1 = (function (){var statearr_23714 = state_23703;
(statearr_23714[(10)] = inst_23690);

(statearr_23714[(8)] = inst_23680__$1);

return statearr_23714;
})();
var statearr_23715_23737 = state_23703__$1;
(statearr_23715_23737[(2)] = null);

(statearr_23715_23737[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23704 === (9))){
var state_23703__$1 = state_23703;
var statearr_23716_23738 = state_23703__$1;
(statearr_23716_23738[(2)] = null);

(statearr_23716_23738[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23704 === (5))){
var state_23703__$1 = state_23703;
var statearr_23717_23739 = state_23703__$1;
(statearr_23717_23739[(2)] = null);

(statearr_23717_23739[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23704 === (10))){
var inst_23695 = (state_23703[(2)]);
var state_23703__$1 = state_23703;
var statearr_23718_23740 = state_23703__$1;
(statearr_23718_23740[(2)] = inst_23695);

(statearr_23718_23740[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23704 === (8))){
var inst_23685 = (state_23703[(7)]);
var state_23703__$1 = state_23703;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_23703__$1,(11),out,inst_23685);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto___23730,out))
;
return ((function (switch__21501__auto__,c__21566__auto___23730,out){
return (function() {
var cljs$core$async$state_machine__21502__auto__ = null;
var cljs$core$async$state_machine__21502__auto____0 = (function (){
var statearr_23722 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_23722[(0)] = cljs$core$async$state_machine__21502__auto__);

(statearr_23722[(1)] = (1));

return statearr_23722;
});
var cljs$core$async$state_machine__21502__auto____1 = (function (state_23703){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_23703);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e23723){if((e23723 instanceof Object)){
var ex__21505__auto__ = e23723;
var statearr_23724_23741 = state_23703;
(statearr_23724_23741[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_23703);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e23723;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__23742 = state_23703;
state_23703 = G__23742;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$state_machine__21502__auto__ = function(state_23703){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21502__auto____1.call(this,state_23703);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21502__auto____0;
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21502__auto____1;
return cljs$core$async$state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___23730,out))
})();
var state__21568__auto__ = (function (){var statearr_23725 = f__21567__auto__.call(null);
(statearr_23725[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___23730);

return statearr_23725;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___23730,out))
);


return out;
});

cljs.core.async.take.cljs$lang$maxFixedArity = 3;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if(typeof cljs.core.async.t_cljs$core$async23750 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async23750 = (function (map_LT_,f,ch,meta23751){
this.map_LT_ = map_LT_;
this.f = f;
this.ch = ch;
this.meta23751 = meta23751;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async23750.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_23752,meta23751__$1){
var self__ = this;
var _23752__$1 = this;
return (new cljs.core.async.t_cljs$core$async23750(self__.map_LT_,self__.f,self__.ch,meta23751__$1));
});

cljs.core.async.t_cljs$core$async23750.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_23752){
var self__ = this;
var _23752__$1 = this;
return self__.meta23751;
});

cljs.core.async.t_cljs$core$async23750.prototype.cljs$core$async$impl$protocols$Channel$ = true;

cljs.core.async.t_cljs$core$async23750.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async23750.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async23750.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;

cljs.core.async.t_cljs$core$async23750.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,(function (){
if(typeof cljs.core.async.t_cljs$core$async23753 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async23753 = (function (map_LT_,f,ch,meta23751,_,fn1,meta23754){
this.map_LT_ = map_LT_;
this.f = f;
this.ch = ch;
this.meta23751 = meta23751;
this._ = _;
this.fn1 = fn1;
this.meta23754 = meta23754;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async23753.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (___$1){
return (function (_23755,meta23754__$1){
var self__ = this;
var _23755__$1 = this;
return (new cljs.core.async.t_cljs$core$async23753(self__.map_LT_,self__.f,self__.ch,self__.meta23751,self__._,self__.fn1,meta23754__$1));
});})(___$1))
;

cljs.core.async.t_cljs$core$async23753.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (___$1){
return (function (_23755){
var self__ = this;
var _23755__$1 = this;
return self__.meta23754;
});})(___$1))
;

cljs.core.async.t_cljs$core$async23753.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t_cljs$core$async23753.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.fn1);
});})(___$1))
;

cljs.core.async.t_cljs$core$async23753.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
});})(___$1))
;

cljs.core.async.t_cljs$core$async23753.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit.call(null,self__.fn1);
return ((function (f1,___$2,___$1){
return (function (p1__23743_SHARP_){
return f1.call(null,(((p1__23743_SHARP_ == null))?null:self__.f.call(null,p1__23743_SHARP_)));
});
;})(f1,___$2,___$1))
});})(___$1))
;

cljs.core.async.t_cljs$core$async23753.getBasis = ((function (___$1){
return (function (){
return new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"map<","map<",-1235808357,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Deprecated - this function will be removed. Use transducer instead"], null)),new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta23751","meta23751",-1904081860,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async23750","cljs.core.async/t_cljs$core$async23750",-256054261,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta23754","meta23754",42612792,null)], null);
});})(___$1))
;

cljs.core.async.t_cljs$core$async23753.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async23753.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async23753";

cljs.core.async.t_cljs$core$async23753.cljs$lang$ctorPrWriter = ((function (___$1){
return (function (this__17394__auto__,writer__17395__auto__,opt__17396__auto__){
return cljs.core._write.call(null,writer__17395__auto__,"cljs.core.async/t_cljs$core$async23753");
});})(___$1))
;

cljs.core.async.__GT_t_cljs$core$async23753 = ((function (___$1){
return (function cljs$core$async$map_LT__$___GT_t_cljs$core$async23753(map_LT___$1,f__$1,ch__$1,meta23751__$1,___$2,fn1__$1,meta23754){
return (new cljs.core.async.t_cljs$core$async23753(map_LT___$1,f__$1,ch__$1,meta23751__$1,___$2,fn1__$1,meta23754));
});})(___$1))
;

}

return (new cljs.core.async.t_cljs$core$async23753(self__.map_LT_,self__.f,self__.ch,self__.meta23751,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__16784__auto__ = ret;
if(cljs.core.truth_(and__16784__auto__)){
return !((cljs.core.deref.call(null,ret) == null));
} else {
return and__16784__auto__;
}
})())){
return cljs.core.async.impl.channels.box.call(null,self__.f.call(null,cljs.core.deref.call(null,ret)));
} else {
return ret;
}
});

cljs.core.async.t_cljs$core$async23750.prototype.cljs$core$async$impl$protocols$WritePort$ = true;

cljs.core.async.t_cljs$core$async23750.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
});

cljs.core.async.t_cljs$core$async23750.getBasis = (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"map<","map<",-1235808357,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Deprecated - this function will be removed. Use transducer instead"], null)),new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta23751","meta23751",-1904081860,null)], null);
});

cljs.core.async.t_cljs$core$async23750.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async23750.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async23750";

cljs.core.async.t_cljs$core$async23750.cljs$lang$ctorPrWriter = (function (this__17394__auto__,writer__17395__auto__,opt__17396__auto__){
return cljs.core._write.call(null,writer__17395__auto__,"cljs.core.async/t_cljs$core$async23750");
});

cljs.core.async.__GT_t_cljs$core$async23750 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async23750(map_LT___$1,f__$1,ch__$1,meta23751){
return (new cljs.core.async.t_cljs$core$async23750(map_LT___$1,f__$1,ch__$1,meta23751));
});

}

return (new cljs.core.async.t_cljs$core$async23750(cljs$core$async$map_LT_,f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if(typeof cljs.core.async.t_cljs$core$async23759 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async23759 = (function (map_GT_,f,ch,meta23760){
this.map_GT_ = map_GT_;
this.f = f;
this.ch = ch;
this.meta23760 = meta23760;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async23759.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_23761,meta23760__$1){
var self__ = this;
var _23761__$1 = this;
return (new cljs.core.async.t_cljs$core$async23759(self__.map_GT_,self__.f,self__.ch,meta23760__$1));
});

cljs.core.async.t_cljs$core$async23759.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_23761){
var self__ = this;
var _23761__$1 = this;
return self__.meta23760;
});

cljs.core.async.t_cljs$core$async23759.prototype.cljs$core$async$impl$protocols$Channel$ = true;

cljs.core.async.t_cljs$core$async23759.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async23759.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;

cljs.core.async.t_cljs$core$async23759.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async23759.prototype.cljs$core$async$impl$protocols$WritePort$ = true;

cljs.core.async.t_cljs$core$async23759.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,self__.f.call(null,val),fn1);
});

cljs.core.async.t_cljs$core$async23759.getBasis = (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"map>","map>",1676369295,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Deprecated - this function will be removed. Use transducer instead"], null)),new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta23760","meta23760",1561473832,null)], null);
});

cljs.core.async.t_cljs$core$async23759.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async23759.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async23759";

cljs.core.async.t_cljs$core$async23759.cljs$lang$ctorPrWriter = (function (this__17394__auto__,writer__17395__auto__,opt__17396__auto__){
return cljs.core._write.call(null,writer__17395__auto__,"cljs.core.async/t_cljs$core$async23759");
});

cljs.core.async.__GT_t_cljs$core$async23759 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async23759(map_GT___$1,f__$1,ch__$1,meta23760){
return (new cljs.core.async.t_cljs$core$async23759(map_GT___$1,f__$1,ch__$1,meta23760));
});

}

return (new cljs.core.async.t_cljs$core$async23759(cljs$core$async$map_GT_,f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if(typeof cljs.core.async.t_cljs$core$async23765 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async23765 = (function (filter_GT_,p,ch,meta23766){
this.filter_GT_ = filter_GT_;
this.p = p;
this.ch = ch;
this.meta23766 = meta23766;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async23765.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_23767,meta23766__$1){
var self__ = this;
var _23767__$1 = this;
return (new cljs.core.async.t_cljs$core$async23765(self__.filter_GT_,self__.p,self__.ch,meta23766__$1));
});

cljs.core.async.t_cljs$core$async23765.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_23767){
var self__ = this;
var _23767__$1 = this;
return self__.meta23766;
});

cljs.core.async.t_cljs$core$async23765.prototype.cljs$core$async$impl$protocols$Channel$ = true;

cljs.core.async.t_cljs$core$async23765.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async23765.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async23765.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;

cljs.core.async.t_cljs$core$async23765.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async23765.prototype.cljs$core$async$impl$protocols$WritePort$ = true;

cljs.core.async.t_cljs$core$async23765.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.p.call(null,val))){
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box.call(null,cljs.core.not.call(null,cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch)));
}
});

cljs.core.async.t_cljs$core$async23765.getBasis = (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"filter>","filter>",-37644455,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Deprecated - this function will be removed. Use transducer instead"], null)),new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta23766","meta23766",518305408,null)], null);
});

cljs.core.async.t_cljs$core$async23765.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async23765.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async23765";

cljs.core.async.t_cljs$core$async23765.cljs$lang$ctorPrWriter = (function (this__17394__auto__,writer__17395__auto__,opt__17396__auto__){
return cljs.core._write.call(null,writer__17395__auto__,"cljs.core.async/t_cljs$core$async23765");
});

cljs.core.async.__GT_t_cljs$core$async23765 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async23765(filter_GT___$1,p__$1,ch__$1,meta23766){
return (new cljs.core.async.t_cljs$core$async23765(filter_GT___$1,p__$1,ch__$1,meta23766));
});

}

return (new cljs.core.async.t_cljs$core$async23765(cljs$core$async$filter_GT_,p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_.call(null,cljs.core.complement.call(null,p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var args23768 = [];
var len__17855__auto___23812 = arguments.length;
var i__17856__auto___23813 = (0);
while(true){
if((i__17856__auto___23813 < len__17855__auto___23812)){
args23768.push((arguments[i__17856__auto___23813]));

var G__23814 = (i__17856__auto___23813 + (1));
i__17856__auto___23813 = G__23814;
continue;
} else {
}
break;
}

var G__23770 = args23768.length;
switch (G__23770) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args23768.length)].join('')));

}
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.call(null,p,ch,null);
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__21566__auto___23816 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___23816,out){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___23816,out){
return (function (state_23791){
var state_val_23792 = (state_23791[(1)]);
if((state_val_23792 === (7))){
var inst_23787 = (state_23791[(2)]);
var state_23791__$1 = state_23791;
var statearr_23793_23817 = state_23791__$1;
(statearr_23793_23817[(2)] = inst_23787);

(statearr_23793_23817[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23792 === (1))){
var state_23791__$1 = state_23791;
var statearr_23794_23818 = state_23791__$1;
(statearr_23794_23818[(2)] = null);

(statearr_23794_23818[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23792 === (4))){
var inst_23773 = (state_23791[(7)]);
var inst_23773__$1 = (state_23791[(2)]);
var inst_23774 = (inst_23773__$1 == null);
var state_23791__$1 = (function (){var statearr_23795 = state_23791;
(statearr_23795[(7)] = inst_23773__$1);

return statearr_23795;
})();
if(cljs.core.truth_(inst_23774)){
var statearr_23796_23819 = state_23791__$1;
(statearr_23796_23819[(1)] = (5));

} else {
var statearr_23797_23820 = state_23791__$1;
(statearr_23797_23820[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23792 === (6))){
var inst_23773 = (state_23791[(7)]);
var inst_23778 = p.call(null,inst_23773);
var state_23791__$1 = state_23791;
if(cljs.core.truth_(inst_23778)){
var statearr_23798_23821 = state_23791__$1;
(statearr_23798_23821[(1)] = (8));

} else {
var statearr_23799_23822 = state_23791__$1;
(statearr_23799_23822[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23792 === (3))){
var inst_23789 = (state_23791[(2)]);
var state_23791__$1 = state_23791;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_23791__$1,inst_23789);
} else {
if((state_val_23792 === (2))){
var state_23791__$1 = state_23791;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_23791__$1,(4),ch);
} else {
if((state_val_23792 === (11))){
var inst_23781 = (state_23791[(2)]);
var state_23791__$1 = state_23791;
var statearr_23800_23823 = state_23791__$1;
(statearr_23800_23823[(2)] = inst_23781);

(statearr_23800_23823[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23792 === (9))){
var state_23791__$1 = state_23791;
var statearr_23801_23824 = state_23791__$1;
(statearr_23801_23824[(2)] = null);

(statearr_23801_23824[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23792 === (5))){
var inst_23776 = cljs.core.async.close_BANG_.call(null,out);
var state_23791__$1 = state_23791;
var statearr_23802_23825 = state_23791__$1;
(statearr_23802_23825[(2)] = inst_23776);

(statearr_23802_23825[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23792 === (10))){
var inst_23784 = (state_23791[(2)]);
var state_23791__$1 = (function (){var statearr_23803 = state_23791;
(statearr_23803[(8)] = inst_23784);

return statearr_23803;
})();
var statearr_23804_23826 = state_23791__$1;
(statearr_23804_23826[(2)] = null);

(statearr_23804_23826[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_23792 === (8))){
var inst_23773 = (state_23791[(7)]);
var state_23791__$1 = state_23791;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_23791__$1,(11),out,inst_23773);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto___23816,out))
;
return ((function (switch__21501__auto__,c__21566__auto___23816,out){
return (function() {
var cljs$core$async$state_machine__21502__auto__ = null;
var cljs$core$async$state_machine__21502__auto____0 = (function (){
var statearr_23808 = [null,null,null,null,null,null,null,null,null];
(statearr_23808[(0)] = cljs$core$async$state_machine__21502__auto__);

(statearr_23808[(1)] = (1));

return statearr_23808;
});
var cljs$core$async$state_machine__21502__auto____1 = (function (state_23791){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_23791);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e23809){if((e23809 instanceof Object)){
var ex__21505__auto__ = e23809;
var statearr_23810_23827 = state_23791;
(statearr_23810_23827[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_23791);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e23809;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__23828 = state_23791;
state_23791 = G__23828;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$state_machine__21502__auto__ = function(state_23791){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21502__auto____1.call(this,state_23791);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21502__auto____0;
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21502__auto____1;
return cljs$core$async$state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___23816,out))
})();
var state__21568__auto__ = (function (){var statearr_23811 = f__21567__auto__.call(null);
(statearr_23811[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___23816);

return statearr_23811;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___23816,out))
);


return out;
});

cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var args23829 = [];
var len__17855__auto___23832 = arguments.length;
var i__17856__auto___23833 = (0);
while(true){
if((i__17856__auto___23833 < len__17855__auto___23832)){
args23829.push((arguments[i__17856__auto___23833]));

var G__23834 = (i__17856__auto___23833 + (1));
i__17856__auto___23833 = G__23834;
continue;
} else {
}
break;
}

var G__23831 = args23829.length;
switch (G__23831) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args23829.length)].join('')));

}
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.call(null,p,ch,null);
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.call(null,cljs.core.complement.call(null,p),ch,buf_or_n);
});

cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3;
cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__21566__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto__){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto__){
return (function (state_24001){
var state_val_24002 = (state_24001[(1)]);
if((state_val_24002 === (7))){
var inst_23997 = (state_24001[(2)]);
var state_24001__$1 = state_24001;
var statearr_24003_24044 = state_24001__$1;
(statearr_24003_24044[(2)] = inst_23997);

(statearr_24003_24044[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (20))){
var inst_23967 = (state_24001[(7)]);
var inst_23978 = (state_24001[(2)]);
var inst_23979 = cljs.core.next.call(null,inst_23967);
var inst_23953 = inst_23979;
var inst_23954 = null;
var inst_23955 = (0);
var inst_23956 = (0);
var state_24001__$1 = (function (){var statearr_24004 = state_24001;
(statearr_24004[(8)] = inst_23954);

(statearr_24004[(9)] = inst_23956);

(statearr_24004[(10)] = inst_23953);

(statearr_24004[(11)] = inst_23955);

(statearr_24004[(12)] = inst_23978);

return statearr_24004;
})();
var statearr_24005_24045 = state_24001__$1;
(statearr_24005_24045[(2)] = null);

(statearr_24005_24045[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (1))){
var state_24001__$1 = state_24001;
var statearr_24006_24046 = state_24001__$1;
(statearr_24006_24046[(2)] = null);

(statearr_24006_24046[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (4))){
var inst_23942 = (state_24001[(13)]);
var inst_23942__$1 = (state_24001[(2)]);
var inst_23943 = (inst_23942__$1 == null);
var state_24001__$1 = (function (){var statearr_24007 = state_24001;
(statearr_24007[(13)] = inst_23942__$1);

return statearr_24007;
})();
if(cljs.core.truth_(inst_23943)){
var statearr_24008_24047 = state_24001__$1;
(statearr_24008_24047[(1)] = (5));

} else {
var statearr_24009_24048 = state_24001__$1;
(statearr_24009_24048[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (15))){
var state_24001__$1 = state_24001;
var statearr_24013_24049 = state_24001__$1;
(statearr_24013_24049[(2)] = null);

(statearr_24013_24049[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (21))){
var state_24001__$1 = state_24001;
var statearr_24014_24050 = state_24001__$1;
(statearr_24014_24050[(2)] = null);

(statearr_24014_24050[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (13))){
var inst_23954 = (state_24001[(8)]);
var inst_23956 = (state_24001[(9)]);
var inst_23953 = (state_24001[(10)]);
var inst_23955 = (state_24001[(11)]);
var inst_23963 = (state_24001[(2)]);
var inst_23964 = (inst_23956 + (1));
var tmp24010 = inst_23954;
var tmp24011 = inst_23953;
var tmp24012 = inst_23955;
var inst_23953__$1 = tmp24011;
var inst_23954__$1 = tmp24010;
var inst_23955__$1 = tmp24012;
var inst_23956__$1 = inst_23964;
var state_24001__$1 = (function (){var statearr_24015 = state_24001;
(statearr_24015[(8)] = inst_23954__$1);

(statearr_24015[(9)] = inst_23956__$1);

(statearr_24015[(10)] = inst_23953__$1);

(statearr_24015[(14)] = inst_23963);

(statearr_24015[(11)] = inst_23955__$1);

return statearr_24015;
})();
var statearr_24016_24051 = state_24001__$1;
(statearr_24016_24051[(2)] = null);

(statearr_24016_24051[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (22))){
var state_24001__$1 = state_24001;
var statearr_24017_24052 = state_24001__$1;
(statearr_24017_24052[(2)] = null);

(statearr_24017_24052[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (6))){
var inst_23942 = (state_24001[(13)]);
var inst_23951 = f.call(null,inst_23942);
var inst_23952 = cljs.core.seq.call(null,inst_23951);
var inst_23953 = inst_23952;
var inst_23954 = null;
var inst_23955 = (0);
var inst_23956 = (0);
var state_24001__$1 = (function (){var statearr_24018 = state_24001;
(statearr_24018[(8)] = inst_23954);

(statearr_24018[(9)] = inst_23956);

(statearr_24018[(10)] = inst_23953);

(statearr_24018[(11)] = inst_23955);

return statearr_24018;
})();
var statearr_24019_24053 = state_24001__$1;
(statearr_24019_24053[(2)] = null);

(statearr_24019_24053[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (17))){
var inst_23967 = (state_24001[(7)]);
var inst_23971 = cljs.core.chunk_first.call(null,inst_23967);
var inst_23972 = cljs.core.chunk_rest.call(null,inst_23967);
var inst_23973 = cljs.core.count.call(null,inst_23971);
var inst_23953 = inst_23972;
var inst_23954 = inst_23971;
var inst_23955 = inst_23973;
var inst_23956 = (0);
var state_24001__$1 = (function (){var statearr_24020 = state_24001;
(statearr_24020[(8)] = inst_23954);

(statearr_24020[(9)] = inst_23956);

(statearr_24020[(10)] = inst_23953);

(statearr_24020[(11)] = inst_23955);

return statearr_24020;
})();
var statearr_24021_24054 = state_24001__$1;
(statearr_24021_24054[(2)] = null);

(statearr_24021_24054[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (3))){
var inst_23999 = (state_24001[(2)]);
var state_24001__$1 = state_24001;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_24001__$1,inst_23999);
} else {
if((state_val_24002 === (12))){
var inst_23987 = (state_24001[(2)]);
var state_24001__$1 = state_24001;
var statearr_24022_24055 = state_24001__$1;
(statearr_24022_24055[(2)] = inst_23987);

(statearr_24022_24055[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (2))){
var state_24001__$1 = state_24001;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_24001__$1,(4),in$);
} else {
if((state_val_24002 === (23))){
var inst_23995 = (state_24001[(2)]);
var state_24001__$1 = state_24001;
var statearr_24023_24056 = state_24001__$1;
(statearr_24023_24056[(2)] = inst_23995);

(statearr_24023_24056[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (19))){
var inst_23982 = (state_24001[(2)]);
var state_24001__$1 = state_24001;
var statearr_24024_24057 = state_24001__$1;
(statearr_24024_24057[(2)] = inst_23982);

(statearr_24024_24057[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (11))){
var inst_23953 = (state_24001[(10)]);
var inst_23967 = (state_24001[(7)]);
var inst_23967__$1 = cljs.core.seq.call(null,inst_23953);
var state_24001__$1 = (function (){var statearr_24025 = state_24001;
(statearr_24025[(7)] = inst_23967__$1);

return statearr_24025;
})();
if(inst_23967__$1){
var statearr_24026_24058 = state_24001__$1;
(statearr_24026_24058[(1)] = (14));

} else {
var statearr_24027_24059 = state_24001__$1;
(statearr_24027_24059[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (9))){
var inst_23989 = (state_24001[(2)]);
var inst_23990 = cljs.core.async.impl.protocols.closed_QMARK_.call(null,out);
var state_24001__$1 = (function (){var statearr_24028 = state_24001;
(statearr_24028[(15)] = inst_23989);

return statearr_24028;
})();
if(cljs.core.truth_(inst_23990)){
var statearr_24029_24060 = state_24001__$1;
(statearr_24029_24060[(1)] = (21));

} else {
var statearr_24030_24061 = state_24001__$1;
(statearr_24030_24061[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (5))){
var inst_23945 = cljs.core.async.close_BANG_.call(null,out);
var state_24001__$1 = state_24001;
var statearr_24031_24062 = state_24001__$1;
(statearr_24031_24062[(2)] = inst_23945);

(statearr_24031_24062[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (14))){
var inst_23967 = (state_24001[(7)]);
var inst_23969 = cljs.core.chunked_seq_QMARK_.call(null,inst_23967);
var state_24001__$1 = state_24001;
if(inst_23969){
var statearr_24032_24063 = state_24001__$1;
(statearr_24032_24063[(1)] = (17));

} else {
var statearr_24033_24064 = state_24001__$1;
(statearr_24033_24064[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (16))){
var inst_23985 = (state_24001[(2)]);
var state_24001__$1 = state_24001;
var statearr_24034_24065 = state_24001__$1;
(statearr_24034_24065[(2)] = inst_23985);

(statearr_24034_24065[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24002 === (10))){
var inst_23954 = (state_24001[(8)]);
var inst_23956 = (state_24001[(9)]);
var inst_23961 = cljs.core._nth.call(null,inst_23954,inst_23956);
var state_24001__$1 = state_24001;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_24001__$1,(13),out,inst_23961);
} else {
if((state_val_24002 === (18))){
var inst_23967 = (state_24001[(7)]);
var inst_23976 = cljs.core.first.call(null,inst_23967);
var state_24001__$1 = state_24001;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_24001__$1,(20),out,inst_23976);
} else {
if((state_val_24002 === (8))){
var inst_23956 = (state_24001[(9)]);
var inst_23955 = (state_24001[(11)]);
var inst_23958 = (inst_23956 < inst_23955);
var inst_23959 = inst_23958;
var state_24001__$1 = state_24001;
if(cljs.core.truth_(inst_23959)){
var statearr_24035_24066 = state_24001__$1;
(statearr_24035_24066[(1)] = (10));

} else {
var statearr_24036_24067 = state_24001__$1;
(statearr_24036_24067[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto__))
;
return ((function (switch__21501__auto__,c__21566__auto__){
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__21502__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__21502__auto____0 = (function (){
var statearr_24040 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_24040[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__21502__auto__);

(statearr_24040[(1)] = (1));

return statearr_24040;
});
var cljs$core$async$mapcat_STAR__$_state_machine__21502__auto____1 = (function (state_24001){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_24001);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e24041){if((e24041 instanceof Object)){
var ex__21505__auto__ = e24041;
var statearr_24042_24068 = state_24001;
(statearr_24042_24068[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_24001);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e24041;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24069 = state_24001;
state_24001 = G__24069;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__21502__auto__ = function(state_24001){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__21502__auto____1.call(this,state_24001);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__21502__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__21502__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto__))
})();
var state__21568__auto__ = (function (){var statearr_24043 = f__21567__auto__.call(null);
(statearr_24043[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto__);

return statearr_24043;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto__))
);

return c__21566__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var args24070 = [];
var len__17855__auto___24073 = arguments.length;
var i__17856__auto___24074 = (0);
while(true){
if((i__17856__auto___24074 < len__17855__auto___24073)){
args24070.push((arguments[i__17856__auto___24074]));

var G__24075 = (i__17856__auto___24074 + (1));
i__17856__auto___24074 = G__24075;
continue;
} else {
}
break;
}

var G__24072 = args24070.length;
switch (G__24072) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args24070.length)].join('')));

}
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.call(null,f,in$,null);
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return out;
});

cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var args24077 = [];
var len__17855__auto___24080 = arguments.length;
var i__17856__auto___24081 = (0);
while(true){
if((i__17856__auto___24081 < len__17855__auto___24080)){
args24077.push((arguments[i__17856__auto___24081]));

var G__24082 = (i__17856__auto___24081 + (1));
i__17856__auto___24081 = G__24082;
continue;
} else {
}
break;
}

var G__24079 = args24077.length;
switch (G__24079) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args24077.length)].join('')));

}
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.call(null,f,out,null);
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return in$;
});

cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var args24084 = [];
var len__17855__auto___24135 = arguments.length;
var i__17856__auto___24136 = (0);
while(true){
if((i__17856__auto___24136 < len__17855__auto___24135)){
args24084.push((arguments[i__17856__auto___24136]));

var G__24137 = (i__17856__auto___24136 + (1));
i__17856__auto___24136 = G__24137;
continue;
} else {
}
break;
}

var G__24086 = args24084.length;
switch (G__24086) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args24084.length)].join('')));

}
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.call(null,ch,null);
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__21566__auto___24139 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___24139,out){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___24139,out){
return (function (state_24110){
var state_val_24111 = (state_24110[(1)]);
if((state_val_24111 === (7))){
var inst_24105 = (state_24110[(2)]);
var state_24110__$1 = state_24110;
var statearr_24112_24140 = state_24110__$1;
(statearr_24112_24140[(2)] = inst_24105);

(statearr_24112_24140[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24111 === (1))){
var inst_24087 = null;
var state_24110__$1 = (function (){var statearr_24113 = state_24110;
(statearr_24113[(7)] = inst_24087);

return statearr_24113;
})();
var statearr_24114_24141 = state_24110__$1;
(statearr_24114_24141[(2)] = null);

(statearr_24114_24141[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24111 === (4))){
var inst_24090 = (state_24110[(8)]);
var inst_24090__$1 = (state_24110[(2)]);
var inst_24091 = (inst_24090__$1 == null);
var inst_24092 = cljs.core.not.call(null,inst_24091);
var state_24110__$1 = (function (){var statearr_24115 = state_24110;
(statearr_24115[(8)] = inst_24090__$1);

return statearr_24115;
})();
if(inst_24092){
var statearr_24116_24142 = state_24110__$1;
(statearr_24116_24142[(1)] = (5));

} else {
var statearr_24117_24143 = state_24110__$1;
(statearr_24117_24143[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24111 === (6))){
var state_24110__$1 = state_24110;
var statearr_24118_24144 = state_24110__$1;
(statearr_24118_24144[(2)] = null);

(statearr_24118_24144[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24111 === (3))){
var inst_24107 = (state_24110[(2)]);
var inst_24108 = cljs.core.async.close_BANG_.call(null,out);
var state_24110__$1 = (function (){var statearr_24119 = state_24110;
(statearr_24119[(9)] = inst_24107);

return statearr_24119;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_24110__$1,inst_24108);
} else {
if((state_val_24111 === (2))){
var state_24110__$1 = state_24110;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_24110__$1,(4),ch);
} else {
if((state_val_24111 === (11))){
var inst_24090 = (state_24110[(8)]);
var inst_24099 = (state_24110[(2)]);
var inst_24087 = inst_24090;
var state_24110__$1 = (function (){var statearr_24120 = state_24110;
(statearr_24120[(10)] = inst_24099);

(statearr_24120[(7)] = inst_24087);

return statearr_24120;
})();
var statearr_24121_24145 = state_24110__$1;
(statearr_24121_24145[(2)] = null);

(statearr_24121_24145[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24111 === (9))){
var inst_24090 = (state_24110[(8)]);
var state_24110__$1 = state_24110;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_24110__$1,(11),out,inst_24090);
} else {
if((state_val_24111 === (5))){
var inst_24090 = (state_24110[(8)]);
var inst_24087 = (state_24110[(7)]);
var inst_24094 = cljs.core._EQ_.call(null,inst_24090,inst_24087);
var state_24110__$1 = state_24110;
if(inst_24094){
var statearr_24123_24146 = state_24110__$1;
(statearr_24123_24146[(1)] = (8));

} else {
var statearr_24124_24147 = state_24110__$1;
(statearr_24124_24147[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24111 === (10))){
var inst_24102 = (state_24110[(2)]);
var state_24110__$1 = state_24110;
var statearr_24125_24148 = state_24110__$1;
(statearr_24125_24148[(2)] = inst_24102);

(statearr_24125_24148[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24111 === (8))){
var inst_24087 = (state_24110[(7)]);
var tmp24122 = inst_24087;
var inst_24087__$1 = tmp24122;
var state_24110__$1 = (function (){var statearr_24126 = state_24110;
(statearr_24126[(7)] = inst_24087__$1);

return statearr_24126;
})();
var statearr_24127_24149 = state_24110__$1;
(statearr_24127_24149[(2)] = null);

(statearr_24127_24149[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto___24139,out))
;
return ((function (switch__21501__auto__,c__21566__auto___24139,out){
return (function() {
var cljs$core$async$state_machine__21502__auto__ = null;
var cljs$core$async$state_machine__21502__auto____0 = (function (){
var statearr_24131 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_24131[(0)] = cljs$core$async$state_machine__21502__auto__);

(statearr_24131[(1)] = (1));

return statearr_24131;
});
var cljs$core$async$state_machine__21502__auto____1 = (function (state_24110){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_24110);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e24132){if((e24132 instanceof Object)){
var ex__21505__auto__ = e24132;
var statearr_24133_24150 = state_24110;
(statearr_24133_24150[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_24110);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e24132;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24151 = state_24110;
state_24110 = G__24151;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$state_machine__21502__auto__ = function(state_24110){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21502__auto____1.call(this,state_24110);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21502__auto____0;
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21502__auto____1;
return cljs$core$async$state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___24139,out))
})();
var state__21568__auto__ = (function (){var statearr_24134 = f__21567__auto__.call(null);
(statearr_24134[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___24139);

return statearr_24134;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___24139,out))
);


return out;
});

cljs.core.async.unique.cljs$lang$maxFixedArity = 2;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var args24152 = [];
var len__17855__auto___24222 = arguments.length;
var i__17856__auto___24223 = (0);
while(true){
if((i__17856__auto___24223 < len__17855__auto___24222)){
args24152.push((arguments[i__17856__auto___24223]));

var G__24224 = (i__17856__auto___24223 + (1));
i__17856__auto___24223 = G__24224;
continue;
} else {
}
break;
}

var G__24154 = args24152.length;
switch (G__24154) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args24152.length)].join('')));

}
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.call(null,n,ch,null);
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__21566__auto___24226 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___24226,out){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___24226,out){
return (function (state_24192){
var state_val_24193 = (state_24192[(1)]);
if((state_val_24193 === (7))){
var inst_24188 = (state_24192[(2)]);
var state_24192__$1 = state_24192;
var statearr_24194_24227 = state_24192__$1;
(statearr_24194_24227[(2)] = inst_24188);

(statearr_24194_24227[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24193 === (1))){
var inst_24155 = (new Array(n));
var inst_24156 = inst_24155;
var inst_24157 = (0);
var state_24192__$1 = (function (){var statearr_24195 = state_24192;
(statearr_24195[(7)] = inst_24156);

(statearr_24195[(8)] = inst_24157);

return statearr_24195;
})();
var statearr_24196_24228 = state_24192__$1;
(statearr_24196_24228[(2)] = null);

(statearr_24196_24228[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24193 === (4))){
var inst_24160 = (state_24192[(9)]);
var inst_24160__$1 = (state_24192[(2)]);
var inst_24161 = (inst_24160__$1 == null);
var inst_24162 = cljs.core.not.call(null,inst_24161);
var state_24192__$1 = (function (){var statearr_24197 = state_24192;
(statearr_24197[(9)] = inst_24160__$1);

return statearr_24197;
})();
if(inst_24162){
var statearr_24198_24229 = state_24192__$1;
(statearr_24198_24229[(1)] = (5));

} else {
var statearr_24199_24230 = state_24192__$1;
(statearr_24199_24230[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24193 === (15))){
var inst_24182 = (state_24192[(2)]);
var state_24192__$1 = state_24192;
var statearr_24200_24231 = state_24192__$1;
(statearr_24200_24231[(2)] = inst_24182);

(statearr_24200_24231[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24193 === (13))){
var state_24192__$1 = state_24192;
var statearr_24201_24232 = state_24192__$1;
(statearr_24201_24232[(2)] = null);

(statearr_24201_24232[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24193 === (6))){
var inst_24157 = (state_24192[(8)]);
var inst_24178 = (inst_24157 > (0));
var state_24192__$1 = state_24192;
if(cljs.core.truth_(inst_24178)){
var statearr_24202_24233 = state_24192__$1;
(statearr_24202_24233[(1)] = (12));

} else {
var statearr_24203_24234 = state_24192__$1;
(statearr_24203_24234[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24193 === (3))){
var inst_24190 = (state_24192[(2)]);
var state_24192__$1 = state_24192;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_24192__$1,inst_24190);
} else {
if((state_val_24193 === (12))){
var inst_24156 = (state_24192[(7)]);
var inst_24180 = cljs.core.vec.call(null,inst_24156);
var state_24192__$1 = state_24192;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_24192__$1,(15),out,inst_24180);
} else {
if((state_val_24193 === (2))){
var state_24192__$1 = state_24192;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_24192__$1,(4),ch);
} else {
if((state_val_24193 === (11))){
var inst_24172 = (state_24192[(2)]);
var inst_24173 = (new Array(n));
var inst_24156 = inst_24173;
var inst_24157 = (0);
var state_24192__$1 = (function (){var statearr_24204 = state_24192;
(statearr_24204[(7)] = inst_24156);

(statearr_24204[(8)] = inst_24157);

(statearr_24204[(10)] = inst_24172);

return statearr_24204;
})();
var statearr_24205_24235 = state_24192__$1;
(statearr_24205_24235[(2)] = null);

(statearr_24205_24235[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24193 === (9))){
var inst_24156 = (state_24192[(7)]);
var inst_24170 = cljs.core.vec.call(null,inst_24156);
var state_24192__$1 = state_24192;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_24192__$1,(11),out,inst_24170);
} else {
if((state_val_24193 === (5))){
var inst_24165 = (state_24192[(11)]);
var inst_24156 = (state_24192[(7)]);
var inst_24157 = (state_24192[(8)]);
var inst_24160 = (state_24192[(9)]);
var inst_24164 = (inst_24156[inst_24157] = inst_24160);
var inst_24165__$1 = (inst_24157 + (1));
var inst_24166 = (inst_24165__$1 < n);
var state_24192__$1 = (function (){var statearr_24206 = state_24192;
(statearr_24206[(12)] = inst_24164);

(statearr_24206[(11)] = inst_24165__$1);

return statearr_24206;
})();
if(cljs.core.truth_(inst_24166)){
var statearr_24207_24236 = state_24192__$1;
(statearr_24207_24236[(1)] = (8));

} else {
var statearr_24208_24237 = state_24192__$1;
(statearr_24208_24237[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24193 === (14))){
var inst_24185 = (state_24192[(2)]);
var inst_24186 = cljs.core.async.close_BANG_.call(null,out);
var state_24192__$1 = (function (){var statearr_24210 = state_24192;
(statearr_24210[(13)] = inst_24185);

return statearr_24210;
})();
var statearr_24211_24238 = state_24192__$1;
(statearr_24211_24238[(2)] = inst_24186);

(statearr_24211_24238[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24193 === (10))){
var inst_24176 = (state_24192[(2)]);
var state_24192__$1 = state_24192;
var statearr_24212_24239 = state_24192__$1;
(statearr_24212_24239[(2)] = inst_24176);

(statearr_24212_24239[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24193 === (8))){
var inst_24165 = (state_24192[(11)]);
var inst_24156 = (state_24192[(7)]);
var tmp24209 = inst_24156;
var inst_24156__$1 = tmp24209;
var inst_24157 = inst_24165;
var state_24192__$1 = (function (){var statearr_24213 = state_24192;
(statearr_24213[(7)] = inst_24156__$1);

(statearr_24213[(8)] = inst_24157);

return statearr_24213;
})();
var statearr_24214_24240 = state_24192__$1;
(statearr_24214_24240[(2)] = null);

(statearr_24214_24240[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto___24226,out))
;
return ((function (switch__21501__auto__,c__21566__auto___24226,out){
return (function() {
var cljs$core$async$state_machine__21502__auto__ = null;
var cljs$core$async$state_machine__21502__auto____0 = (function (){
var statearr_24218 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_24218[(0)] = cljs$core$async$state_machine__21502__auto__);

(statearr_24218[(1)] = (1));

return statearr_24218;
});
var cljs$core$async$state_machine__21502__auto____1 = (function (state_24192){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_24192);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e24219){if((e24219 instanceof Object)){
var ex__21505__auto__ = e24219;
var statearr_24220_24241 = state_24192;
(statearr_24220_24241[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_24192);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e24219;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24242 = state_24192;
state_24192 = G__24242;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$state_machine__21502__auto__ = function(state_24192){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21502__auto____1.call(this,state_24192);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21502__auto____0;
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21502__auto____1;
return cljs$core$async$state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___24226,out))
})();
var state__21568__auto__ = (function (){var statearr_24221 = f__21567__auto__.call(null);
(statearr_24221[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___24226);

return statearr_24221;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___24226,out))
);


return out;
});

cljs.core.async.partition.cljs$lang$maxFixedArity = 3;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var args24243 = [];
var len__17855__auto___24317 = arguments.length;
var i__17856__auto___24318 = (0);
while(true){
if((i__17856__auto___24318 < len__17855__auto___24317)){
args24243.push((arguments[i__17856__auto___24318]));

var G__24319 = (i__17856__auto___24318 + (1));
i__17856__auto___24318 = G__24319;
continue;
} else {
}
break;
}

var G__24245 = args24243.length;
switch (G__24245) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args24243.length)].join('')));

}
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.call(null,f,ch,null);
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__21566__auto___24321 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___24321,out){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___24321,out){
return (function (state_24287){
var state_val_24288 = (state_24287[(1)]);
if((state_val_24288 === (7))){
var inst_24283 = (state_24287[(2)]);
var state_24287__$1 = state_24287;
var statearr_24289_24322 = state_24287__$1;
(statearr_24289_24322[(2)] = inst_24283);

(statearr_24289_24322[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24288 === (1))){
var inst_24246 = [];
var inst_24247 = inst_24246;
var inst_24248 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_24287__$1 = (function (){var statearr_24290 = state_24287;
(statearr_24290[(7)] = inst_24248);

(statearr_24290[(8)] = inst_24247);

return statearr_24290;
})();
var statearr_24291_24323 = state_24287__$1;
(statearr_24291_24323[(2)] = null);

(statearr_24291_24323[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24288 === (4))){
var inst_24251 = (state_24287[(9)]);
var inst_24251__$1 = (state_24287[(2)]);
var inst_24252 = (inst_24251__$1 == null);
var inst_24253 = cljs.core.not.call(null,inst_24252);
var state_24287__$1 = (function (){var statearr_24292 = state_24287;
(statearr_24292[(9)] = inst_24251__$1);

return statearr_24292;
})();
if(inst_24253){
var statearr_24293_24324 = state_24287__$1;
(statearr_24293_24324[(1)] = (5));

} else {
var statearr_24294_24325 = state_24287__$1;
(statearr_24294_24325[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24288 === (15))){
var inst_24277 = (state_24287[(2)]);
var state_24287__$1 = state_24287;
var statearr_24295_24326 = state_24287__$1;
(statearr_24295_24326[(2)] = inst_24277);

(statearr_24295_24326[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24288 === (13))){
var state_24287__$1 = state_24287;
var statearr_24296_24327 = state_24287__$1;
(statearr_24296_24327[(2)] = null);

(statearr_24296_24327[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24288 === (6))){
var inst_24247 = (state_24287[(8)]);
var inst_24272 = inst_24247.length;
var inst_24273 = (inst_24272 > (0));
var state_24287__$1 = state_24287;
if(cljs.core.truth_(inst_24273)){
var statearr_24297_24328 = state_24287__$1;
(statearr_24297_24328[(1)] = (12));

} else {
var statearr_24298_24329 = state_24287__$1;
(statearr_24298_24329[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24288 === (3))){
var inst_24285 = (state_24287[(2)]);
var state_24287__$1 = state_24287;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_24287__$1,inst_24285);
} else {
if((state_val_24288 === (12))){
var inst_24247 = (state_24287[(8)]);
var inst_24275 = cljs.core.vec.call(null,inst_24247);
var state_24287__$1 = state_24287;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_24287__$1,(15),out,inst_24275);
} else {
if((state_val_24288 === (2))){
var state_24287__$1 = state_24287;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_24287__$1,(4),ch);
} else {
if((state_val_24288 === (11))){
var inst_24255 = (state_24287[(10)]);
var inst_24251 = (state_24287[(9)]);
var inst_24265 = (state_24287[(2)]);
var inst_24266 = [];
var inst_24267 = inst_24266.push(inst_24251);
var inst_24247 = inst_24266;
var inst_24248 = inst_24255;
var state_24287__$1 = (function (){var statearr_24299 = state_24287;
(statearr_24299[(7)] = inst_24248);

(statearr_24299[(11)] = inst_24267);

(statearr_24299[(12)] = inst_24265);

(statearr_24299[(8)] = inst_24247);

return statearr_24299;
})();
var statearr_24300_24330 = state_24287__$1;
(statearr_24300_24330[(2)] = null);

(statearr_24300_24330[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24288 === (9))){
var inst_24247 = (state_24287[(8)]);
var inst_24263 = cljs.core.vec.call(null,inst_24247);
var state_24287__$1 = state_24287;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_24287__$1,(11),out,inst_24263);
} else {
if((state_val_24288 === (5))){
var inst_24248 = (state_24287[(7)]);
var inst_24255 = (state_24287[(10)]);
var inst_24251 = (state_24287[(9)]);
var inst_24255__$1 = f.call(null,inst_24251);
var inst_24256 = cljs.core._EQ_.call(null,inst_24255__$1,inst_24248);
var inst_24257 = cljs.core.keyword_identical_QMARK_.call(null,inst_24248,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_24258 = (inst_24256) || (inst_24257);
var state_24287__$1 = (function (){var statearr_24301 = state_24287;
(statearr_24301[(10)] = inst_24255__$1);

return statearr_24301;
})();
if(cljs.core.truth_(inst_24258)){
var statearr_24302_24331 = state_24287__$1;
(statearr_24302_24331[(1)] = (8));

} else {
var statearr_24303_24332 = state_24287__$1;
(statearr_24303_24332[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24288 === (14))){
var inst_24280 = (state_24287[(2)]);
var inst_24281 = cljs.core.async.close_BANG_.call(null,out);
var state_24287__$1 = (function (){var statearr_24305 = state_24287;
(statearr_24305[(13)] = inst_24280);

return statearr_24305;
})();
var statearr_24306_24333 = state_24287__$1;
(statearr_24306_24333[(2)] = inst_24281);

(statearr_24306_24333[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24288 === (10))){
var inst_24270 = (state_24287[(2)]);
var state_24287__$1 = state_24287;
var statearr_24307_24334 = state_24287__$1;
(statearr_24307_24334[(2)] = inst_24270);

(statearr_24307_24334[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24288 === (8))){
var inst_24255 = (state_24287[(10)]);
var inst_24247 = (state_24287[(8)]);
var inst_24251 = (state_24287[(9)]);
var inst_24260 = inst_24247.push(inst_24251);
var tmp24304 = inst_24247;
var inst_24247__$1 = tmp24304;
var inst_24248 = inst_24255;
var state_24287__$1 = (function (){var statearr_24308 = state_24287;
(statearr_24308[(7)] = inst_24248);

(statearr_24308[(14)] = inst_24260);

(statearr_24308[(8)] = inst_24247__$1);

return statearr_24308;
})();
var statearr_24309_24335 = state_24287__$1;
(statearr_24309_24335[(2)] = null);

(statearr_24309_24335[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto___24321,out))
;
return ((function (switch__21501__auto__,c__21566__auto___24321,out){
return (function() {
var cljs$core$async$state_machine__21502__auto__ = null;
var cljs$core$async$state_machine__21502__auto____0 = (function (){
var statearr_24313 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_24313[(0)] = cljs$core$async$state_machine__21502__auto__);

(statearr_24313[(1)] = (1));

return statearr_24313;
});
var cljs$core$async$state_machine__21502__auto____1 = (function (state_24287){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_24287);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e24314){if((e24314 instanceof Object)){
var ex__21505__auto__ = e24314;
var statearr_24315_24336 = state_24287;
(statearr_24315_24336[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_24287);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e24314;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24337 = state_24287;
state_24287 = G__24337;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
cljs$core$async$state_machine__21502__auto__ = function(state_24287){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__21502__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__21502__auto____1.call(this,state_24287);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__21502__auto____0;
cljs$core$async$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__21502__auto____1;
return cljs$core$async$state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___24321,out))
})();
var state__21568__auto__ = (function (){var statearr_24316 = f__21567__auto__.call(null);
(statearr_24316[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___24321);

return statearr_24316;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___24321,out))
);


return out;
});

cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3;

//# sourceMappingURL=async.js.map?rel=1448506746410