// Compiled by ClojureScript 1.7.170 {}
goog.provide('cljs.repl');
goog.require('cljs.core');
cljs.repl.print_doc = (function cljs$repl$print_doc(m){
cljs.core.println.call(null,"-------------------------");

cljs.core.println.call(null,[cljs.core.str((function (){var temp__4425__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__4425__auto__)){
var ns = temp__4425__auto__;
return [cljs.core.str(ns),cljs.core.str("/")].join('');
} else {
return null;
}
})()),cljs.core.str(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Protocol");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__25533_25547 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__25534_25548 = null;
var count__25535_25549 = (0);
var i__25536_25550 = (0);
while(true){
if((i__25536_25550 < count__25535_25549)){
var f_25551 = cljs.core._nth.call(null,chunk__25534_25548,i__25536_25550);
cljs.core.println.call(null,"  ",f_25551);

var G__25552 = seq__25533_25547;
var G__25553 = chunk__25534_25548;
var G__25554 = count__25535_25549;
var G__25555 = (i__25536_25550 + (1));
seq__25533_25547 = G__25552;
chunk__25534_25548 = G__25553;
count__25535_25549 = G__25554;
i__25536_25550 = G__25555;
continue;
} else {
var temp__4425__auto___25556 = cljs.core.seq.call(null,seq__25533_25547);
if(temp__4425__auto___25556){
var seq__25533_25557__$1 = temp__4425__auto___25556;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__25533_25557__$1)){
var c__17600__auto___25558 = cljs.core.chunk_first.call(null,seq__25533_25557__$1);
var G__25559 = cljs.core.chunk_rest.call(null,seq__25533_25557__$1);
var G__25560 = c__17600__auto___25558;
var G__25561 = cljs.core.count.call(null,c__17600__auto___25558);
var G__25562 = (0);
seq__25533_25547 = G__25559;
chunk__25534_25548 = G__25560;
count__25535_25549 = G__25561;
i__25536_25550 = G__25562;
continue;
} else {
var f_25563 = cljs.core.first.call(null,seq__25533_25557__$1);
cljs.core.println.call(null,"  ",f_25563);

var G__25564 = cljs.core.next.call(null,seq__25533_25557__$1);
var G__25565 = null;
var G__25566 = (0);
var G__25567 = (0);
seq__25533_25547 = G__25564;
chunk__25534_25548 = G__25565;
count__25535_25549 = G__25566;
i__25536_25550 = G__25567;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_25568 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__16796__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__16796__auto__)){
return or__16796__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.call(null,arglists_25568);
} else {
cljs.core.prn.call(null,((cljs.core._EQ_.call(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first.call(null,arglists_25568)))?cljs.core.second.call(null,arglists_25568):arglists_25568));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Special Form");

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.contains_QMARK_.call(null,m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.call(null,[cljs.core.str("\n  Please see http://clojure.org/"),cljs.core.str(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join(''));
} else {
return null;
}
} else {
return cljs.core.println.call(null,[cljs.core.str("\n  Please see http://clojure.org/special_forms#"),cljs.core.str(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Macro");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"REPL Special Function");
} else {
}

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__25537 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__25538 = null;
var count__25539 = (0);
var i__25540 = (0);
while(true){
if((i__25540 < count__25539)){
var vec__25541 = cljs.core._nth.call(null,chunk__25538,i__25540);
var name = cljs.core.nth.call(null,vec__25541,(0),null);
var map__25542 = cljs.core.nth.call(null,vec__25541,(1),null);
var map__25542__$1 = ((((!((map__25542 == null)))?((((map__25542.cljs$lang$protocol_mask$partition0$ & (64))) || (map__25542.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__25542):map__25542);
var doc = cljs.core.get.call(null,map__25542__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists = cljs.core.get.call(null,map__25542__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name);

cljs.core.println.call(null," ",arglists);

if(cljs.core.truth_(doc)){
cljs.core.println.call(null," ",doc);
} else {
}

var G__25569 = seq__25537;
var G__25570 = chunk__25538;
var G__25571 = count__25539;
var G__25572 = (i__25540 + (1));
seq__25537 = G__25569;
chunk__25538 = G__25570;
count__25539 = G__25571;
i__25540 = G__25572;
continue;
} else {
var temp__4425__auto__ = cljs.core.seq.call(null,seq__25537);
if(temp__4425__auto__){
var seq__25537__$1 = temp__4425__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__25537__$1)){
var c__17600__auto__ = cljs.core.chunk_first.call(null,seq__25537__$1);
var G__25573 = cljs.core.chunk_rest.call(null,seq__25537__$1);
var G__25574 = c__17600__auto__;
var G__25575 = cljs.core.count.call(null,c__17600__auto__);
var G__25576 = (0);
seq__25537 = G__25573;
chunk__25538 = G__25574;
count__25539 = G__25575;
i__25540 = G__25576;
continue;
} else {
var vec__25544 = cljs.core.first.call(null,seq__25537__$1);
var name = cljs.core.nth.call(null,vec__25544,(0),null);
var map__25545 = cljs.core.nth.call(null,vec__25544,(1),null);
var map__25545__$1 = ((((!((map__25545 == null)))?((((map__25545.cljs$lang$protocol_mask$partition0$ & (64))) || (map__25545.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__25545):map__25545);
var doc = cljs.core.get.call(null,map__25545__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists = cljs.core.get.call(null,map__25545__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name);

cljs.core.println.call(null," ",arglists);

if(cljs.core.truth_(doc)){
cljs.core.println.call(null," ",doc);
} else {
}

var G__25577 = cljs.core.next.call(null,seq__25537__$1);
var G__25578 = null;
var G__25579 = (0);
var G__25580 = (0);
seq__25537 = G__25577;
chunk__25538 = G__25578;
count__25539 = G__25579;
i__25540 = G__25580;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
}
});

//# sourceMappingURL=repl.js.map?rel=1448506747376