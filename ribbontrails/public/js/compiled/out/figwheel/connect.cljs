(ns figwheel.connect (:require [ribbontrails.core] [figwheel.client] [figwheel.client.utils]))
(figwheel.client/start {:on-jsload (fn [& x] (if js/ribbontrails.core.on-js-reload (apply js/ribbontrails.core.on-js-reload x) (figwheel.client.utils/log :debug "Figwheel: :on-jsload hook 'ribbontrails.core/on-js-reload' is missing"))), :build-id "dev", :websocket-url "ws://localhost:3449/figwheel-ws"})

