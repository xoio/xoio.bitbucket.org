// Compiled by ClojureScript 1.7.170 {}
goog.provide('figwheel.client.file_reloading');
goog.require('cljs.core');
goog.require('goog.string');
goog.require('goog.Uri');
goog.require('goog.net.jsloader');
goog.require('cljs.core.async');
goog.require('goog.object');
goog.require('clojure.set');
goog.require('clojure.string');
goog.require('figwheel.client.utils');
figwheel.client.file_reloading.queued_file_reload;
if(typeof figwheel.client.file_reloading.figwheel_meta_pragmas !== 'undefined'){
} else {
figwheel.client.file_reloading.figwheel_meta_pragmas = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
figwheel.client.file_reloading.on_jsload_custom_event = (function figwheel$client$file_reloading$on_jsload_custom_event(url){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.js-reload",url);
});
figwheel.client.file_reloading.before_jsload_custom_event = (function figwheel$client$file_reloading$before_jsload_custom_event(files){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.before-js-reload",files);
});
figwheel.client.file_reloading.namespace_file_map_QMARK_ = (function figwheel$client$file_reloading$namespace_file_map_QMARK_(m){
var or__16796__auto__ = (cljs.core.map_QMARK_.call(null,m)) && (typeof new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(m) === 'string') && (((new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) == null)) || (typeof new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) === 'string')) && (cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(m),new cljs.core.Keyword(null,"namespace","namespace",-377510372)));
if(or__16796__auto__){
return or__16796__auto__;
} else {
cljs.core.println.call(null,"Error not namespace-file-map",cljs.core.pr_str.call(null,m));

return false;
}
});
figwheel.client.file_reloading.add_cache_buster = (function figwheel$client$file_reloading$add_cache_buster(url){

return goog.Uri.parse(url).makeUnique();
});
figwheel.client.file_reloading.name__GT_path = (function figwheel$client$file_reloading$name__GT_path(ns){

return (goog.dependencies_.nameToPath[ns]);
});
figwheel.client.file_reloading.provided_QMARK_ = (function figwheel$client$file_reloading$provided_QMARK_(ns){
return (goog.dependencies_.written[figwheel.client.file_reloading.name__GT_path.call(null,ns)]);
});
figwheel.client.file_reloading.fix_node_request_url = (function figwheel$client$file_reloading$fix_node_request_url(url){

if(cljs.core.truth_(goog.string.startsWith(url,"../"))){
return clojure.string.replace.call(null,url,"../","");
} else {
return [cljs.core.str("goog/"),cljs.core.str(url)].join('');
}
});
figwheel.client.file_reloading.immutable_ns_QMARK_ = (function figwheel$client$file_reloading$immutable_ns_QMARK_(name){
var or__16796__auto__ = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 9, ["svgpan.SvgPan",null,"far.out",null,"testDep.bar",null,"someprotopackage.TestPackageTypes",null,"goog",null,"an.existing.path",null,"cljs.core",null,"ns",null,"dup.base",null], null), null).call(null,name);
if(cljs.core.truth_(or__16796__auto__)){
return or__16796__auto__;
} else {
return cljs.core.some.call(null,cljs.core.partial.call(null,goog.string.startsWith,name),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, ["goog.","cljs.","clojure.","fake.","proto2."], null));
}
});
figwheel.client.file_reloading.get_requires = (function figwheel$client$file_reloading$get_requires(ns){
return cljs.core.set.call(null,cljs.core.filter.call(null,(function (p1__24509_SHARP_){
return cljs.core.not.call(null,figwheel.client.file_reloading.immutable_ns_QMARK_.call(null,p1__24509_SHARP_));
}),goog.object.getKeys((goog.dependencies_.requires[figwheel.client.file_reloading.name__GT_path.call(null,ns)]))));
});
if(typeof figwheel.client.file_reloading.dependency_data !== 'undefined'){
} else {
figwheel.client.file_reloading.dependency_data = cljs.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"dependents","dependents",136812837),cljs.core.PersistentArrayMap.EMPTY], null));
}
figwheel.client.file_reloading.path_to_name_BANG_ = (function figwheel$client$file_reloading$path_to_name_BANG_(path,name){
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependency_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),path], null),cljs.core.fnil.call(null,clojure.set.union,cljs.core.PersistentHashSet.EMPTY),cljs.core.PersistentHashSet.fromArray([name], true));
});
/**
 * Setup a path to name dependencies map.
 * That goes from path -> #{ ns-names }
 */
figwheel.client.file_reloading.setup_path__GT_name_BANG_ = (function figwheel$client$file_reloading$setup_path__GT_name_BANG_(){
var nameToPath = goog.object.filter(goog.dependencies_.nameToPath,(function (v,k,o){
return goog.string.startsWith(v,"../");
}));
return goog.object.forEach(nameToPath,((function (nameToPath){
return (function (v,k,o){
return figwheel.client.file_reloading.path_to_name_BANG_.call(null,v,k);
});})(nameToPath))
);
});
/**
 * returns a set of namespaces defined by a path
 */
figwheel.client.file_reloading.path__GT_name = (function figwheel$client$file_reloading$path__GT_name(path){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.dependency_data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),path], null));
});
figwheel.client.file_reloading.name_to_parent_BANG_ = (function figwheel$client$file_reloading$name_to_parent_BANG_(ns,parent_ns){
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependency_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dependents","dependents",136812837),ns], null),cljs.core.fnil.call(null,clojure.set.union,cljs.core.PersistentHashSet.EMPTY),cljs.core.PersistentHashSet.fromArray([parent_ns], true));
});
/**
 * This reverses the goog.dependencies_.requires for looking up ns-dependents.
 */
figwheel.client.file_reloading.setup_ns__GT_dependents_BANG_ = (function figwheel$client$file_reloading$setup_ns__GT_dependents_BANG_(){
var requires = goog.object.filter(goog.dependencies_.requires,(function (v,k,o){
return goog.string.startsWith(k,"../");
}));
return goog.object.forEach(requires,((function (requires){
return (function (v,k,_){
return goog.object.forEach(v,((function (requires){
return (function (v_SINGLEQUOTE_,k_SINGLEQUOTE_,___$1){
var seq__24514 = cljs.core.seq.call(null,figwheel.client.file_reloading.path__GT_name.call(null,k));
var chunk__24515 = null;
var count__24516 = (0);
var i__24517 = (0);
while(true){
if((i__24517 < count__24516)){
var n = cljs.core._nth.call(null,chunk__24515,i__24517);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,k_SINGLEQUOTE_,n);

var G__24518 = seq__24514;
var G__24519 = chunk__24515;
var G__24520 = count__24516;
var G__24521 = (i__24517 + (1));
seq__24514 = G__24518;
chunk__24515 = G__24519;
count__24516 = G__24520;
i__24517 = G__24521;
continue;
} else {
var temp__4425__auto__ = cljs.core.seq.call(null,seq__24514);
if(temp__4425__auto__){
var seq__24514__$1 = temp__4425__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__24514__$1)){
var c__17600__auto__ = cljs.core.chunk_first.call(null,seq__24514__$1);
var G__24522 = cljs.core.chunk_rest.call(null,seq__24514__$1);
var G__24523 = c__17600__auto__;
var G__24524 = cljs.core.count.call(null,c__17600__auto__);
var G__24525 = (0);
seq__24514 = G__24522;
chunk__24515 = G__24523;
count__24516 = G__24524;
i__24517 = G__24525;
continue;
} else {
var n = cljs.core.first.call(null,seq__24514__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,k_SINGLEQUOTE_,n);

var G__24526 = cljs.core.next.call(null,seq__24514__$1);
var G__24527 = null;
var G__24528 = (0);
var G__24529 = (0);
seq__24514 = G__24526;
chunk__24515 = G__24527;
count__24516 = G__24528;
i__24517 = G__24529;
continue;
}
} else {
return null;
}
}
break;
}
});})(requires))
);
});})(requires))
);
});
figwheel.client.file_reloading.ns__GT_dependents = (function figwheel$client$file_reloading$ns__GT_dependents(ns){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.dependency_data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dependents","dependents",136812837),ns], null));
});
figwheel.client.file_reloading.build_topo_sort = (function figwheel$client$file_reloading$build_topo_sort(get_deps){
var get_deps__$1 = cljs.core.memoize.call(null,get_deps);
var topo_sort_helper_STAR_ = ((function (get_deps__$1){
return (function figwheel$client$file_reloading$build_topo_sort_$_topo_sort_helper_STAR_(x,depth,state){
var deps = get_deps__$1.call(null,x);
if(cljs.core.empty_QMARK_.call(null,deps)){
return null;
} else {
return topo_sort_STAR_.call(null,deps,depth,state);
}
});})(get_deps__$1))
;
var topo_sort_STAR_ = ((function (get_deps__$1){
return (function() {
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_ = null;
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1 = (function (deps){
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.call(null,deps,(0),cljs.core.atom.call(null,cljs.core.sorted_map.call(null)));
});
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3 = (function (deps,depth,state){
cljs.core.swap_BANG_.call(null,state,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [depth], null),cljs.core.fnil.call(null,cljs.core.into,cljs.core.PersistentHashSet.EMPTY),deps);

var seq__24568_24575 = cljs.core.seq.call(null,deps);
var chunk__24569_24576 = null;
var count__24570_24577 = (0);
var i__24571_24578 = (0);
while(true){
if((i__24571_24578 < count__24570_24577)){
var dep_24579 = cljs.core._nth.call(null,chunk__24569_24576,i__24571_24578);
topo_sort_helper_STAR_.call(null,dep_24579,(depth + (1)),state);

var G__24580 = seq__24568_24575;
var G__24581 = chunk__24569_24576;
var G__24582 = count__24570_24577;
var G__24583 = (i__24571_24578 + (1));
seq__24568_24575 = G__24580;
chunk__24569_24576 = G__24581;
count__24570_24577 = G__24582;
i__24571_24578 = G__24583;
continue;
} else {
var temp__4425__auto___24584 = cljs.core.seq.call(null,seq__24568_24575);
if(temp__4425__auto___24584){
var seq__24568_24585__$1 = temp__4425__auto___24584;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__24568_24585__$1)){
var c__17600__auto___24586 = cljs.core.chunk_first.call(null,seq__24568_24585__$1);
var G__24587 = cljs.core.chunk_rest.call(null,seq__24568_24585__$1);
var G__24588 = c__17600__auto___24586;
var G__24589 = cljs.core.count.call(null,c__17600__auto___24586);
var G__24590 = (0);
seq__24568_24575 = G__24587;
chunk__24569_24576 = G__24588;
count__24570_24577 = G__24589;
i__24571_24578 = G__24590;
continue;
} else {
var dep_24591 = cljs.core.first.call(null,seq__24568_24585__$1);
topo_sort_helper_STAR_.call(null,dep_24591,(depth + (1)),state);

var G__24592 = cljs.core.next.call(null,seq__24568_24585__$1);
var G__24593 = null;
var G__24594 = (0);
var G__24595 = (0);
seq__24568_24575 = G__24592;
chunk__24569_24576 = G__24593;
count__24570_24577 = G__24594;
i__24571_24578 = G__24595;
continue;
}
} else {
}
}
break;
}

if(cljs.core._EQ_.call(null,depth,(0))){
return elim_dups_STAR_.call(null,cljs.core.reverse.call(null,cljs.core.vals.call(null,cljs.core.deref.call(null,state))));
} else {
return null;
}
});
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_ = function(deps,depth,state){
switch(arguments.length){
case 1:
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1.call(this,deps);
case 3:
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3.call(this,deps,depth,state);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1;
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.cljs$core$IFn$_invoke$arity$3 = figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3;
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_;
})()
;})(get_deps__$1))
;
var elim_dups_STAR_ = ((function (get_deps__$1){
return (function figwheel$client$file_reloading$build_topo_sort_$_elim_dups_STAR_(p__24572){
var vec__24574 = p__24572;
var x = cljs.core.nth.call(null,vec__24574,(0),null);
var xs = cljs.core.nthnext.call(null,vec__24574,(1));
if((x == null)){
return cljs.core.List.EMPTY;
} else {
return cljs.core.cons.call(null,x,figwheel$client$file_reloading$build_topo_sort_$_elim_dups_STAR_.call(null,cljs.core.map.call(null,((function (vec__24574,x,xs,get_deps__$1){
return (function (p1__24530_SHARP_){
return clojure.set.difference.call(null,p1__24530_SHARP_,x);
});})(vec__24574,x,xs,get_deps__$1))
,xs)));
}
});})(get_deps__$1))
;
return topo_sort_STAR_;
});
figwheel.client.file_reloading.get_all_dependencies = (function figwheel$client$file_reloading$get_all_dependencies(ns){
var topo_sort_SINGLEQUOTE_ = figwheel.client.file_reloading.build_topo_sort.call(null,figwheel.client.file_reloading.get_requires);
return cljs.core.apply.call(null,cljs.core.concat,topo_sort_SINGLEQUOTE_.call(null,cljs.core.set.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [ns], null))));
});
figwheel.client.file_reloading.get_all_dependents = (function figwheel$client$file_reloading$get_all_dependents(nss){
var topo_sort_SINGLEQUOTE_ = figwheel.client.file_reloading.build_topo_sort.call(null,figwheel.client.file_reloading.ns__GT_dependents);
return cljs.core.reverse.call(null,cljs.core.apply.call(null,cljs.core.concat,topo_sort_SINGLEQUOTE_.call(null,cljs.core.set.call(null,nss))));
});
figwheel.client.file_reloading.unprovide_BANG_ = (function figwheel$client$file_reloading$unprovide_BANG_(ns){
var path = figwheel.client.file_reloading.name__GT_path.call(null,ns);
goog.object.remove(goog.dependencies_.visited,path);

goog.object.remove(goog.dependencies_.written,path);

return goog.object.remove(goog.dependencies_.written,[cljs.core.str(goog.basePath),cljs.core.str(path)].join(''));
});
figwheel.client.file_reloading.resolve_ns = (function figwheel$client$file_reloading$resolve_ns(ns){
return [cljs.core.str(goog.basePath),cljs.core.str(figwheel.client.file_reloading.name__GT_path.call(null,ns))].join('');
});
figwheel.client.file_reloading.addDependency = (function figwheel$client$file_reloading$addDependency(path,provides,requires){
var seq__24608 = cljs.core.seq.call(null,provides);
var chunk__24609 = null;
var count__24610 = (0);
var i__24611 = (0);
while(true){
if((i__24611 < count__24610)){
var prov = cljs.core._nth.call(null,chunk__24609,i__24611);
figwheel.client.file_reloading.path_to_name_BANG_.call(null,path,prov);

var seq__24612_24620 = cljs.core.seq.call(null,requires);
var chunk__24613_24621 = null;
var count__24614_24622 = (0);
var i__24615_24623 = (0);
while(true){
if((i__24615_24623 < count__24614_24622)){
var req_24624 = cljs.core._nth.call(null,chunk__24613_24621,i__24615_24623);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_24624,prov);

var G__24625 = seq__24612_24620;
var G__24626 = chunk__24613_24621;
var G__24627 = count__24614_24622;
var G__24628 = (i__24615_24623 + (1));
seq__24612_24620 = G__24625;
chunk__24613_24621 = G__24626;
count__24614_24622 = G__24627;
i__24615_24623 = G__24628;
continue;
} else {
var temp__4425__auto___24629 = cljs.core.seq.call(null,seq__24612_24620);
if(temp__4425__auto___24629){
var seq__24612_24630__$1 = temp__4425__auto___24629;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__24612_24630__$1)){
var c__17600__auto___24631 = cljs.core.chunk_first.call(null,seq__24612_24630__$1);
var G__24632 = cljs.core.chunk_rest.call(null,seq__24612_24630__$1);
var G__24633 = c__17600__auto___24631;
var G__24634 = cljs.core.count.call(null,c__17600__auto___24631);
var G__24635 = (0);
seq__24612_24620 = G__24632;
chunk__24613_24621 = G__24633;
count__24614_24622 = G__24634;
i__24615_24623 = G__24635;
continue;
} else {
var req_24636 = cljs.core.first.call(null,seq__24612_24630__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_24636,prov);

var G__24637 = cljs.core.next.call(null,seq__24612_24630__$1);
var G__24638 = null;
var G__24639 = (0);
var G__24640 = (0);
seq__24612_24620 = G__24637;
chunk__24613_24621 = G__24638;
count__24614_24622 = G__24639;
i__24615_24623 = G__24640;
continue;
}
} else {
}
}
break;
}

var G__24641 = seq__24608;
var G__24642 = chunk__24609;
var G__24643 = count__24610;
var G__24644 = (i__24611 + (1));
seq__24608 = G__24641;
chunk__24609 = G__24642;
count__24610 = G__24643;
i__24611 = G__24644;
continue;
} else {
var temp__4425__auto__ = cljs.core.seq.call(null,seq__24608);
if(temp__4425__auto__){
var seq__24608__$1 = temp__4425__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__24608__$1)){
var c__17600__auto__ = cljs.core.chunk_first.call(null,seq__24608__$1);
var G__24645 = cljs.core.chunk_rest.call(null,seq__24608__$1);
var G__24646 = c__17600__auto__;
var G__24647 = cljs.core.count.call(null,c__17600__auto__);
var G__24648 = (0);
seq__24608 = G__24645;
chunk__24609 = G__24646;
count__24610 = G__24647;
i__24611 = G__24648;
continue;
} else {
var prov = cljs.core.first.call(null,seq__24608__$1);
figwheel.client.file_reloading.path_to_name_BANG_.call(null,path,prov);

var seq__24616_24649 = cljs.core.seq.call(null,requires);
var chunk__24617_24650 = null;
var count__24618_24651 = (0);
var i__24619_24652 = (0);
while(true){
if((i__24619_24652 < count__24618_24651)){
var req_24653 = cljs.core._nth.call(null,chunk__24617_24650,i__24619_24652);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_24653,prov);

var G__24654 = seq__24616_24649;
var G__24655 = chunk__24617_24650;
var G__24656 = count__24618_24651;
var G__24657 = (i__24619_24652 + (1));
seq__24616_24649 = G__24654;
chunk__24617_24650 = G__24655;
count__24618_24651 = G__24656;
i__24619_24652 = G__24657;
continue;
} else {
var temp__4425__auto___24658__$1 = cljs.core.seq.call(null,seq__24616_24649);
if(temp__4425__auto___24658__$1){
var seq__24616_24659__$1 = temp__4425__auto___24658__$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__24616_24659__$1)){
var c__17600__auto___24660 = cljs.core.chunk_first.call(null,seq__24616_24659__$1);
var G__24661 = cljs.core.chunk_rest.call(null,seq__24616_24659__$1);
var G__24662 = c__17600__auto___24660;
var G__24663 = cljs.core.count.call(null,c__17600__auto___24660);
var G__24664 = (0);
seq__24616_24649 = G__24661;
chunk__24617_24650 = G__24662;
count__24618_24651 = G__24663;
i__24619_24652 = G__24664;
continue;
} else {
var req_24665 = cljs.core.first.call(null,seq__24616_24659__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_24665,prov);

var G__24666 = cljs.core.next.call(null,seq__24616_24659__$1);
var G__24667 = null;
var G__24668 = (0);
var G__24669 = (0);
seq__24616_24649 = G__24666;
chunk__24617_24650 = G__24667;
count__24618_24651 = G__24668;
i__24619_24652 = G__24669;
continue;
}
} else {
}
}
break;
}

var G__24670 = cljs.core.next.call(null,seq__24608__$1);
var G__24671 = null;
var G__24672 = (0);
var G__24673 = (0);
seq__24608 = G__24670;
chunk__24609 = G__24671;
count__24610 = G__24672;
i__24611 = G__24673;
continue;
}
} else {
return null;
}
}
break;
}
});
figwheel.client.file_reloading.figwheel_require = (function figwheel$client$file_reloading$figwheel_require(src,reload){
goog.require = figwheel$client$file_reloading$figwheel_require;

if(cljs.core._EQ_.call(null,reload,"reload-all")){
var seq__24678_24682 = cljs.core.seq.call(null,figwheel.client.file_reloading.get_all_dependencies.call(null,src));
var chunk__24679_24683 = null;
var count__24680_24684 = (0);
var i__24681_24685 = (0);
while(true){
if((i__24681_24685 < count__24680_24684)){
var ns_24686 = cljs.core._nth.call(null,chunk__24679_24683,i__24681_24685);
figwheel.client.file_reloading.unprovide_BANG_.call(null,ns_24686);

var G__24687 = seq__24678_24682;
var G__24688 = chunk__24679_24683;
var G__24689 = count__24680_24684;
var G__24690 = (i__24681_24685 + (1));
seq__24678_24682 = G__24687;
chunk__24679_24683 = G__24688;
count__24680_24684 = G__24689;
i__24681_24685 = G__24690;
continue;
} else {
var temp__4425__auto___24691 = cljs.core.seq.call(null,seq__24678_24682);
if(temp__4425__auto___24691){
var seq__24678_24692__$1 = temp__4425__auto___24691;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__24678_24692__$1)){
var c__17600__auto___24693 = cljs.core.chunk_first.call(null,seq__24678_24692__$1);
var G__24694 = cljs.core.chunk_rest.call(null,seq__24678_24692__$1);
var G__24695 = c__17600__auto___24693;
var G__24696 = cljs.core.count.call(null,c__17600__auto___24693);
var G__24697 = (0);
seq__24678_24682 = G__24694;
chunk__24679_24683 = G__24695;
count__24680_24684 = G__24696;
i__24681_24685 = G__24697;
continue;
} else {
var ns_24698 = cljs.core.first.call(null,seq__24678_24692__$1);
figwheel.client.file_reloading.unprovide_BANG_.call(null,ns_24698);

var G__24699 = cljs.core.next.call(null,seq__24678_24692__$1);
var G__24700 = null;
var G__24701 = (0);
var G__24702 = (0);
seq__24678_24682 = G__24699;
chunk__24679_24683 = G__24700;
count__24680_24684 = G__24701;
i__24681_24685 = G__24702;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(reload)){
figwheel.client.file_reloading.unprovide_BANG_.call(null,src);
} else {
}

return goog.require_figwheel_backup_(src);
});
/**
 * Reusable browser REPL bootstrapping. Patches the essential functions
 *   in goog.base to support re-loading of namespaces after page load.
 */
figwheel.client.file_reloading.bootstrap_goog_base = (function figwheel$client$file_reloading$bootstrap_goog_base(){
if(cljs.core.truth_(COMPILED)){
return null;
} else {
goog.require_figwheel_backup_ = (function (){var or__16796__auto__ = goog.require__;
if(cljs.core.truth_(or__16796__auto__)){
return or__16796__auto__;
} else {
return goog.require;
}
})();

goog.isProvided_ = (function (name){
return false;
});

figwheel.client.file_reloading.setup_path__GT_name_BANG_.call(null);

figwheel.client.file_reloading.setup_ns__GT_dependents_BANG_.call(null);

goog.addDependency_figwheel_backup_ = goog.addDependency;

goog.addDependency = (function() { 
var G__24703__delegate = function (args){
cljs.core.apply.call(null,figwheel.client.file_reloading.addDependency,args);

return cljs.core.apply.call(null,goog.addDependency_figwheel_backup_,args);
};
var G__24703 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__24704__i = 0, G__24704__a = new Array(arguments.length -  0);
while (G__24704__i < G__24704__a.length) {G__24704__a[G__24704__i] = arguments[G__24704__i + 0]; ++G__24704__i;}
  args = new cljs.core.IndexedSeq(G__24704__a,0);
} 
return G__24703__delegate.call(this,args);};
G__24703.cljs$lang$maxFixedArity = 0;
G__24703.cljs$lang$applyTo = (function (arglist__24705){
var args = cljs.core.seq(arglist__24705);
return G__24703__delegate(args);
});
G__24703.cljs$core$IFn$_invoke$arity$variadic = G__24703__delegate;
return G__24703;
})()
;

goog.constructNamespace_("cljs.user");

goog.global.CLOSURE_IMPORT_SCRIPT = figwheel.client.file_reloading.queued_file_reload;

return goog.require = figwheel.client.file_reloading.figwheel_require;
}
});
figwheel.client.file_reloading.patch_goog_base = (function figwheel$client$file_reloading$patch_goog_base(){
if(typeof figwheel.client.file_reloading.bootstrapped_cljs !== 'undefined'){
return null;
} else {
figwheel.client.file_reloading.bootstrapped_cljs = (function (){
figwheel.client.file_reloading.bootstrap_goog_base.call(null);

return true;
})()
;
}
});
figwheel.client.file_reloading.reload_file_STAR_ = (function (){var pred__24707 = cljs.core._EQ_;
var expr__24708 = figwheel.client.utils.host_env_QMARK_.call(null);
if(cljs.core.truth_(pred__24707.call(null,new cljs.core.Keyword(null,"node","node",581201198),expr__24708))){
var path_parts = ((function (pred__24707,expr__24708){
return (function (p1__24706_SHARP_){
return clojure.string.split.call(null,p1__24706_SHARP_,/[\/\\]/);
});})(pred__24707,expr__24708))
;
var sep = (cljs.core.truth_(cljs.core.re_matches.call(null,/win.*/,process.platform))?"\\":"/");
var root = clojure.string.join.call(null,sep,cljs.core.pop.call(null,cljs.core.pop.call(null,path_parts.call(null,__dirname))));
return ((function (path_parts,sep,root,pred__24707,expr__24708){
return (function (request_url,callback){

var cache_path = clojure.string.join.call(null,sep,cljs.core.cons.call(null,root,path_parts.call(null,figwheel.client.file_reloading.fix_node_request_url.call(null,request_url))));
(require.cache[cache_path] = null);

return callback.call(null,(function (){try{return require(cache_path);
}catch (e24710){if((e24710 instanceof Error)){
var e = e24710;
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Figwheel: Error loading file "),cljs.core.str(cache_path)].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),e.stack);

return false;
} else {
throw e24710;

}
}})());
});
;})(path_parts,sep,root,pred__24707,expr__24708))
} else {
if(cljs.core.truth_(pred__24707.call(null,new cljs.core.Keyword(null,"html","html",-998796897),expr__24708))){
return ((function (pred__24707,expr__24708){
return (function (request_url,callback){

var deferred = goog.net.jsloader.load(figwheel.client.file_reloading.add_cache_buster.call(null,request_url),{"cleanupWhenDone": true});
deferred.addCallback(((function (deferred,pred__24707,expr__24708){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [true], null));
});})(deferred,pred__24707,expr__24708))
);

return deferred.addErrback(((function (deferred,pred__24707,expr__24708){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [false], null));
});})(deferred,pred__24707,expr__24708))
);
});
;})(pred__24707,expr__24708))
} else {
return ((function (pred__24707,expr__24708){
return (function (a,b){
throw "Reload not defined for this platform";
});
;})(pred__24707,expr__24708))
}
}
})();
figwheel.client.file_reloading.reload_file = (function figwheel$client$file_reloading$reload_file(p__24711,callback){
var map__24714 = p__24711;
var map__24714__$1 = ((((!((map__24714 == null)))?((((map__24714.cljs$lang$protocol_mask$partition0$ & (64))) || (map__24714.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__24714):map__24714);
var file_msg = map__24714__$1;
var request_url = cljs.core.get.call(null,map__24714__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));

figwheel.client.utils.debug_prn.call(null,[cljs.core.str("FigWheel: Attempting to load "),cljs.core.str(request_url)].join(''));

return figwheel.client.file_reloading.reload_file_STAR_.call(null,request_url,((function (map__24714,map__24714__$1,file_msg,request_url){
return (function (success_QMARK_){
if(cljs.core.truth_(success_QMARK_)){
figwheel.client.utils.debug_prn.call(null,[cljs.core.str("FigWheel: Successfully loaded "),cljs.core.str(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.assoc.call(null,file_msg,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),true)], null));
} else {
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Figwheel: Error loading file "),cljs.core.str(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});})(map__24714,map__24714__$1,file_msg,request_url))
);
});
if(typeof figwheel.client.file_reloading.reload_chan !== 'undefined'){
} else {
figwheel.client.file_reloading.reload_chan = cljs.core.async.chan.call(null);
}
if(typeof figwheel.client.file_reloading.on_load_callbacks !== 'undefined'){
} else {
figwheel.client.file_reloading.on_load_callbacks = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
if(typeof figwheel.client.file_reloading.dependencies_loaded !== 'undefined'){
} else {
figwheel.client.file_reloading.dependencies_loaded = cljs.core.atom.call(null,cljs.core.PersistentVector.EMPTY);
}
figwheel.client.file_reloading.blocking_load = (function figwheel$client$file_reloading$blocking_load(url){
var out = cljs.core.async.chan.call(null);
figwheel.client.file_reloading.reload_file.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"request-url","request-url",2100346596),url], null),((function (out){
return (function (file_msg){
cljs.core.async.put_BANG_.call(null,out,file_msg);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);

return out;
});
if(typeof figwheel.client.file_reloading.reloader_loop !== 'undefined'){
} else {
figwheel.client.file_reloading.reloader_loop = (function (){var c__21566__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto__){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto__){
return (function (state_24738){
var state_val_24739 = (state_24738[(1)]);
if((state_val_24739 === (7))){
var inst_24734 = (state_24738[(2)]);
var state_24738__$1 = state_24738;
var statearr_24740_24760 = state_24738__$1;
(statearr_24740_24760[(2)] = inst_24734);

(statearr_24740_24760[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24739 === (1))){
var state_24738__$1 = state_24738;
var statearr_24741_24761 = state_24738__$1;
(statearr_24741_24761[(2)] = null);

(statearr_24741_24761[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24739 === (4))){
var inst_24718 = (state_24738[(7)]);
var inst_24718__$1 = (state_24738[(2)]);
var state_24738__$1 = (function (){var statearr_24742 = state_24738;
(statearr_24742[(7)] = inst_24718__$1);

return statearr_24742;
})();
if(cljs.core.truth_(inst_24718__$1)){
var statearr_24743_24762 = state_24738__$1;
(statearr_24743_24762[(1)] = (5));

} else {
var statearr_24744_24763 = state_24738__$1;
(statearr_24744_24763[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24739 === (6))){
var state_24738__$1 = state_24738;
var statearr_24745_24764 = state_24738__$1;
(statearr_24745_24764[(2)] = null);

(statearr_24745_24764[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24739 === (3))){
var inst_24736 = (state_24738[(2)]);
var state_24738__$1 = state_24738;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_24738__$1,inst_24736);
} else {
if((state_val_24739 === (2))){
var state_24738__$1 = state_24738;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_24738__$1,(4),figwheel.client.file_reloading.reload_chan);
} else {
if((state_val_24739 === (11))){
var inst_24730 = (state_24738[(2)]);
var state_24738__$1 = (function (){var statearr_24746 = state_24738;
(statearr_24746[(8)] = inst_24730);

return statearr_24746;
})();
var statearr_24747_24765 = state_24738__$1;
(statearr_24747_24765[(2)] = null);

(statearr_24747_24765[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24739 === (9))){
var inst_24724 = (state_24738[(9)]);
var inst_24722 = (state_24738[(10)]);
var inst_24726 = inst_24724.call(null,inst_24722);
var state_24738__$1 = state_24738;
var statearr_24748_24766 = state_24738__$1;
(statearr_24748_24766[(2)] = inst_24726);

(statearr_24748_24766[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24739 === (5))){
var inst_24718 = (state_24738[(7)]);
var inst_24720 = figwheel.client.file_reloading.blocking_load.call(null,inst_24718);
var state_24738__$1 = state_24738;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_24738__$1,(8),inst_24720);
} else {
if((state_val_24739 === (10))){
var inst_24722 = (state_24738[(10)]);
var inst_24728 = cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependencies_loaded,cljs.core.conj,inst_24722);
var state_24738__$1 = state_24738;
var statearr_24749_24767 = state_24738__$1;
(statearr_24749_24767[(2)] = inst_24728);

(statearr_24749_24767[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24739 === (8))){
var inst_24724 = (state_24738[(9)]);
var inst_24718 = (state_24738[(7)]);
var inst_24722 = (state_24738[(2)]);
var inst_24723 = cljs.core.deref.call(null,figwheel.client.file_reloading.on_load_callbacks);
var inst_24724__$1 = cljs.core.get.call(null,inst_24723,inst_24718);
var state_24738__$1 = (function (){var statearr_24750 = state_24738;
(statearr_24750[(9)] = inst_24724__$1);

(statearr_24750[(10)] = inst_24722);

return statearr_24750;
})();
if(cljs.core.truth_(inst_24724__$1)){
var statearr_24751_24768 = state_24738__$1;
(statearr_24751_24768[(1)] = (9));

} else {
var statearr_24752_24769 = state_24738__$1;
(statearr_24752_24769[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto__))
;
return ((function (switch__21501__auto__,c__21566__auto__){
return (function() {
var figwheel$client$file_reloading$state_machine__21502__auto__ = null;
var figwheel$client$file_reloading$state_machine__21502__auto____0 = (function (){
var statearr_24756 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_24756[(0)] = figwheel$client$file_reloading$state_machine__21502__auto__);

(statearr_24756[(1)] = (1));

return statearr_24756;
});
var figwheel$client$file_reloading$state_machine__21502__auto____1 = (function (state_24738){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_24738);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e24757){if((e24757 instanceof Object)){
var ex__21505__auto__ = e24757;
var statearr_24758_24770 = state_24738;
(statearr_24758_24770[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_24738);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e24757;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24771 = state_24738;
state_24738 = G__24771;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
figwheel$client$file_reloading$state_machine__21502__auto__ = function(state_24738){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$state_machine__21502__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$state_machine__21502__auto____1.call(this,state_24738);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$state_machine__21502__auto____0;
figwheel$client$file_reloading$state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$state_machine__21502__auto____1;
return figwheel$client$file_reloading$state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto__))
})();
var state__21568__auto__ = (function (){var statearr_24759 = f__21567__auto__.call(null);
(statearr_24759[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto__);

return statearr_24759;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto__))
);

return c__21566__auto__;
})();
}
figwheel.client.file_reloading.queued_file_reload = (function figwheel$client$file_reloading$queued_file_reload(url){
return cljs.core.async.put_BANG_.call(null,figwheel.client.file_reloading.reload_chan,url);
});
figwheel.client.file_reloading.require_with_callback = (function figwheel$client$file_reloading$require_with_callback(p__24772,callback){
var map__24775 = p__24772;
var map__24775__$1 = ((((!((map__24775 == null)))?((((map__24775.cljs$lang$protocol_mask$partition0$ & (64))) || (map__24775.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__24775):map__24775);
var file_msg = map__24775__$1;
var namespace = cljs.core.get.call(null,map__24775__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var request_url = figwheel.client.file_reloading.resolve_ns.call(null,namespace);
cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.on_load_callbacks,cljs.core.assoc,request_url,((function (request_url,map__24775,map__24775__$1,file_msg,namespace){
return (function (file_msg_SINGLEQUOTE_){
cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.on_load_callbacks,cljs.core.dissoc,request_url);

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.merge.call(null,file_msg,cljs.core.select_keys.call(null,file_msg_SINGLEQUOTE_,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375)], null)))], null));
});})(request_url,map__24775,map__24775__$1,file_msg,namespace))
);

return figwheel.client.file_reloading.figwheel_require.call(null,cljs.core.name.call(null,namespace),true);
});
figwheel.client.file_reloading.reload_file_QMARK_ = (function figwheel$client$file_reloading$reload_file_QMARK_(p__24777){
var map__24780 = p__24777;
var map__24780__$1 = ((((!((map__24780 == null)))?((((map__24780.cljs$lang$protocol_mask$partition0$ & (64))) || (map__24780.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__24780):map__24780);
var file_msg = map__24780__$1;
var namespace = cljs.core.get.call(null,map__24780__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

var meta_pragmas = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
var and__16784__auto__ = cljs.core.not.call(null,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179).cljs$core$IFn$_invoke$arity$1(meta_pragmas));
if(and__16784__auto__){
var or__16796__auto__ = new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
if(cljs.core.truth_(or__16796__auto__)){
return or__16796__auto__;
} else {
var or__16796__auto____$1 = new cljs.core.Keyword(null,"figwheel-load","figwheel-load",1316089175).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
if(cljs.core.truth_(or__16796__auto____$1)){
return or__16796__auto____$1;
} else {
return figwheel.client.file_reloading.provided_QMARK_.call(null,cljs.core.name.call(null,namespace));
}
}
} else {
return and__16784__auto__;
}
});
figwheel.client.file_reloading.js_reload = (function figwheel$client$file_reloading$js_reload(p__24782,callback){
var map__24785 = p__24782;
var map__24785__$1 = ((((!((map__24785 == null)))?((((map__24785.cljs$lang$protocol_mask$partition0$ & (64))) || (map__24785.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__24785):map__24785);
var file_msg = map__24785__$1;
var request_url = cljs.core.get.call(null,map__24785__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
var namespace = cljs.core.get.call(null,map__24785__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

if(cljs.core.truth_(figwheel.client.file_reloading.reload_file_QMARK_.call(null,file_msg))){
return figwheel.client.file_reloading.require_with_callback.call(null,file_msg,callback);
} else {
figwheel.client.utils.debug_prn.call(null,[cljs.core.str("Figwheel: Not trying to load file "),cljs.core.str(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});
figwheel.client.file_reloading.reload_js_file = (function figwheel$client$file_reloading$reload_js_file(file_msg){
var out = cljs.core.async.chan.call(null);
figwheel.client.file_reloading.js_reload.call(null,file_msg,((function (out){
return (function (url){
cljs.core.async.put_BANG_.call(null,out,url);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);

return out;
});
/**
 * Returns a chanel with one collection of loaded filenames on it.
 */
figwheel.client.file_reloading.load_all_js_files = (function figwheel$client$file_reloading$load_all_js_files(files){
var out = cljs.core.async.chan.call(null);
var c__21566__auto___24873 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___24873,out){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___24873,out){
return (function (state_24855){
var state_val_24856 = (state_24855[(1)]);
if((state_val_24856 === (1))){
var inst_24833 = cljs.core.nth.call(null,files,(0),null);
var inst_24834 = cljs.core.nthnext.call(null,files,(1));
var inst_24835 = files;
var state_24855__$1 = (function (){var statearr_24857 = state_24855;
(statearr_24857[(7)] = inst_24835);

(statearr_24857[(8)] = inst_24833);

(statearr_24857[(9)] = inst_24834);

return statearr_24857;
})();
var statearr_24858_24874 = state_24855__$1;
(statearr_24858_24874[(2)] = null);

(statearr_24858_24874[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24856 === (2))){
var inst_24835 = (state_24855[(7)]);
var inst_24838 = (state_24855[(10)]);
var inst_24838__$1 = cljs.core.nth.call(null,inst_24835,(0),null);
var inst_24839 = cljs.core.nthnext.call(null,inst_24835,(1));
var inst_24840 = (inst_24838__$1 == null);
var inst_24841 = cljs.core.not.call(null,inst_24840);
var state_24855__$1 = (function (){var statearr_24859 = state_24855;
(statearr_24859[(11)] = inst_24839);

(statearr_24859[(10)] = inst_24838__$1);

return statearr_24859;
})();
if(inst_24841){
var statearr_24860_24875 = state_24855__$1;
(statearr_24860_24875[(1)] = (4));

} else {
var statearr_24861_24876 = state_24855__$1;
(statearr_24861_24876[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24856 === (3))){
var inst_24853 = (state_24855[(2)]);
var state_24855__$1 = state_24855;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_24855__$1,inst_24853);
} else {
if((state_val_24856 === (4))){
var inst_24838 = (state_24855[(10)]);
var inst_24843 = figwheel.client.file_reloading.reload_js_file.call(null,inst_24838);
var state_24855__$1 = state_24855;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_24855__$1,(7),inst_24843);
} else {
if((state_val_24856 === (5))){
var inst_24849 = cljs.core.async.close_BANG_.call(null,out);
var state_24855__$1 = state_24855;
var statearr_24862_24877 = state_24855__$1;
(statearr_24862_24877[(2)] = inst_24849);

(statearr_24862_24877[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24856 === (6))){
var inst_24851 = (state_24855[(2)]);
var state_24855__$1 = state_24855;
var statearr_24863_24878 = state_24855__$1;
(statearr_24863_24878[(2)] = inst_24851);

(statearr_24863_24878[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_24856 === (7))){
var inst_24839 = (state_24855[(11)]);
var inst_24845 = (state_24855[(2)]);
var inst_24846 = cljs.core.async.put_BANG_.call(null,out,inst_24845);
var inst_24835 = inst_24839;
var state_24855__$1 = (function (){var statearr_24864 = state_24855;
(statearr_24864[(7)] = inst_24835);

(statearr_24864[(12)] = inst_24846);

return statearr_24864;
})();
var statearr_24865_24879 = state_24855__$1;
(statearr_24865_24879[(2)] = null);

(statearr_24865_24879[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(c__21566__auto___24873,out))
;
return ((function (switch__21501__auto__,c__21566__auto___24873,out){
return (function() {
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__21502__auto__ = null;
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__21502__auto____0 = (function (){
var statearr_24869 = [null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_24869[(0)] = figwheel$client$file_reloading$load_all_js_files_$_state_machine__21502__auto__);

(statearr_24869[(1)] = (1));

return statearr_24869;
});
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__21502__auto____1 = (function (state_24855){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_24855);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e24870){if((e24870 instanceof Object)){
var ex__21505__auto__ = e24870;
var statearr_24871_24880 = state_24855;
(statearr_24871_24880[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_24855);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e24870;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__24881 = state_24855;
state_24855 = G__24881;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
figwheel$client$file_reloading$load_all_js_files_$_state_machine__21502__auto__ = function(state_24855){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__21502__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__21502__auto____1.call(this,state_24855);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$load_all_js_files_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__21502__auto____0;
figwheel$client$file_reloading$load_all_js_files_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__21502__auto____1;
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___24873,out))
})();
var state__21568__auto__ = (function (){var statearr_24872 = f__21567__auto__.call(null);
(statearr_24872[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___24873);

return statearr_24872;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___24873,out))
);


return cljs.core.async.into.call(null,cljs.core.PersistentVector.EMPTY,out);
});
figwheel.client.file_reloading.eval_body = (function figwheel$client$file_reloading$eval_body(p__24882,opts){
var map__24886 = p__24882;
var map__24886__$1 = ((((!((map__24886 == null)))?((((map__24886.cljs$lang$protocol_mask$partition0$ & (64))) || (map__24886.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__24886):map__24886);
var eval_body__$1 = cljs.core.get.call(null,map__24886__$1,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883));
var file = cljs.core.get.call(null,map__24886__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
if(cljs.core.truth_((function (){var and__16784__auto__ = eval_body__$1;
if(cljs.core.truth_(and__16784__auto__)){
return typeof eval_body__$1 === 'string';
} else {
return and__16784__auto__;
}
})())){
var code = eval_body__$1;
try{figwheel.client.utils.debug_prn.call(null,[cljs.core.str("Evaling file "),cljs.core.str(file)].join(''));

return figwheel.client.utils.eval_helper.call(null,code,opts);
}catch (e24888){var e = e24888;
return figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Unable to evaluate "),cljs.core.str(file)].join(''));
}} else {
return null;
}
});
figwheel.client.file_reloading.expand_files = (function figwheel$client$file_reloading$expand_files(files){
var deps = figwheel.client.file_reloading.get_all_dependents.call(null,cljs.core.map.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372),files));
return cljs.core.filter.call(null,cljs.core.comp.call(null,cljs.core.not,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, ["figwheel.connect",null], null), null),new cljs.core.Keyword(null,"namespace","namespace",-377510372)),cljs.core.map.call(null,((function (deps){
return (function (n){
var temp__4423__auto__ = cljs.core.first.call(null,cljs.core.filter.call(null,((function (deps){
return (function (p1__24889_SHARP_){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__24889_SHARP_),n);
});})(deps))
,files));
if(cljs.core.truth_(temp__4423__auto__)){
var file_msg = temp__4423__auto__;
return file_msg;
} else {
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"namespace","namespace",-377510372),new cljs.core.Keyword(null,"namespace","namespace",-377510372),n], null);
}
});})(deps))
,deps));
});
figwheel.client.file_reloading.sort_files = (function figwheel$client$file_reloading$sort_files(files){
if((cljs.core.count.call(null,files) <= (1))){
return files;
} else {
var keep_files = cljs.core.set.call(null,cljs.core.keep.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372),files));
return cljs.core.filter.call(null,cljs.core.comp.call(null,keep_files,new cljs.core.Keyword(null,"namespace","namespace",-377510372)),figwheel.client.file_reloading.expand_files.call(null,files));
}
});
figwheel.client.file_reloading.get_figwheel_always = (function figwheel$client$file_reloading$get_figwheel_always(){
return cljs.core.map.call(null,(function (p__24894){
var vec__24895 = p__24894;
var k = cljs.core.nth.call(null,vec__24895,(0),null);
var v = cljs.core.nth.call(null,vec__24895,(1),null);
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"namespace","namespace",-377510372),k,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"namespace","namespace",-377510372)], null);
}),cljs.core.filter.call(null,(function (p__24896){
var vec__24897 = p__24896;
var k = cljs.core.nth.call(null,vec__24897,(0),null);
var v = cljs.core.nth.call(null,vec__24897,(1),null);
return new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(v);
}),cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas)));
});
figwheel.client.file_reloading.reload_js_files = (function figwheel$client$file_reloading$reload_js_files(p__24901,p__24902){
var map__25149 = p__24901;
var map__25149__$1 = ((((!((map__25149 == null)))?((((map__25149.cljs$lang$protocol_mask$partition0$ & (64))) || (map__25149.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__25149):map__25149);
var opts = map__25149__$1;
var before_jsload = cljs.core.get.call(null,map__25149__$1,new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128));
var on_jsload = cljs.core.get.call(null,map__25149__$1,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602));
var reload_dependents = cljs.core.get.call(null,map__25149__$1,new cljs.core.Keyword(null,"reload-dependents","reload-dependents",-956865430));
var map__25150 = p__24902;
var map__25150__$1 = ((((!((map__25150 == null)))?((((map__25150.cljs$lang$protocol_mask$partition0$ & (64))) || (map__25150.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__25150):map__25150);
var msg = map__25150__$1;
var files = cljs.core.get.call(null,map__25150__$1,new cljs.core.Keyword(null,"files","files",-472457450));
var figwheel_meta = cljs.core.get.call(null,map__25150__$1,new cljs.core.Keyword(null,"figwheel-meta","figwheel-meta",-225970237));
var recompile_dependents = cljs.core.get.call(null,map__25150__$1,new cljs.core.Keyword(null,"recompile-dependents","recompile-dependents",523804171));
if(cljs.core.empty_QMARK_.call(null,figwheel_meta)){
} else {
cljs.core.reset_BANG_.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas,figwheel_meta);
}

var c__21566__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (state_25303){
var state_val_25304 = (state_25303[(1)]);
if((state_val_25304 === (7))){
var inst_25165 = (state_25303[(7)]);
var inst_25166 = (state_25303[(8)]);
var inst_25164 = (state_25303[(9)]);
var inst_25167 = (state_25303[(10)]);
var inst_25172 = cljs.core._nth.call(null,inst_25165,inst_25167);
var inst_25173 = figwheel.client.file_reloading.eval_body.call(null,inst_25172,opts);
var inst_25174 = (inst_25167 + (1));
var tmp25305 = inst_25165;
var tmp25306 = inst_25166;
var tmp25307 = inst_25164;
var inst_25164__$1 = tmp25307;
var inst_25165__$1 = tmp25305;
var inst_25166__$1 = tmp25306;
var inst_25167__$1 = inst_25174;
var state_25303__$1 = (function (){var statearr_25308 = state_25303;
(statearr_25308[(7)] = inst_25165__$1);

(statearr_25308[(11)] = inst_25173);

(statearr_25308[(8)] = inst_25166__$1);

(statearr_25308[(9)] = inst_25164__$1);

(statearr_25308[(10)] = inst_25167__$1);

return statearr_25308;
})();
var statearr_25309_25395 = state_25303__$1;
(statearr_25309_25395[(2)] = null);

(statearr_25309_25395[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (20))){
var inst_25207 = (state_25303[(12)]);
var inst_25215 = figwheel.client.file_reloading.sort_files.call(null,inst_25207);
var state_25303__$1 = state_25303;
var statearr_25310_25396 = state_25303__$1;
(statearr_25310_25396[(2)] = inst_25215);

(statearr_25310_25396[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (27))){
var state_25303__$1 = state_25303;
var statearr_25311_25397 = state_25303__$1;
(statearr_25311_25397[(2)] = null);

(statearr_25311_25397[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (1))){
var inst_25156 = (state_25303[(13)]);
var inst_25153 = before_jsload.call(null,files);
var inst_25154 = figwheel.client.file_reloading.before_jsload_custom_event.call(null,files);
var inst_25155 = (function (){return ((function (inst_25156,inst_25153,inst_25154,state_val_25304,c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__24898_SHARP_){
return new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__24898_SHARP_);
});
;})(inst_25156,inst_25153,inst_25154,state_val_25304,c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_25156__$1 = cljs.core.filter.call(null,inst_25155,files);
var inst_25157 = cljs.core.not_empty.call(null,inst_25156__$1);
var state_25303__$1 = (function (){var statearr_25312 = state_25303;
(statearr_25312[(13)] = inst_25156__$1);

(statearr_25312[(14)] = inst_25153);

(statearr_25312[(15)] = inst_25154);

return statearr_25312;
})();
if(cljs.core.truth_(inst_25157)){
var statearr_25313_25398 = state_25303__$1;
(statearr_25313_25398[(1)] = (2));

} else {
var statearr_25314_25399 = state_25303__$1;
(statearr_25314_25399[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (24))){
var state_25303__$1 = state_25303;
var statearr_25315_25400 = state_25303__$1;
(statearr_25315_25400[(2)] = null);

(statearr_25315_25400[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (39))){
var inst_25257 = (state_25303[(16)]);
var state_25303__$1 = state_25303;
var statearr_25316_25401 = state_25303__$1;
(statearr_25316_25401[(2)] = inst_25257);

(statearr_25316_25401[(1)] = (40));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (46))){
var inst_25298 = (state_25303[(2)]);
var state_25303__$1 = state_25303;
var statearr_25317_25402 = state_25303__$1;
(statearr_25317_25402[(2)] = inst_25298);

(statearr_25317_25402[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (4))){
var inst_25201 = (state_25303[(2)]);
var inst_25202 = cljs.core.List.EMPTY;
var inst_25203 = cljs.core.reset_BANG_.call(null,figwheel.client.file_reloading.dependencies_loaded,inst_25202);
var inst_25204 = (function (){return ((function (inst_25201,inst_25202,inst_25203,state_val_25304,c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__24899_SHARP_){
var and__16784__auto__ = new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__24899_SHARP_);
if(cljs.core.truth_(and__16784__auto__)){
return cljs.core.not.call(null,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__24899_SHARP_));
} else {
return and__16784__auto__;
}
});
;})(inst_25201,inst_25202,inst_25203,state_val_25304,c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_25205 = cljs.core.filter.call(null,inst_25204,files);
var inst_25206 = figwheel.client.file_reloading.get_figwheel_always.call(null);
var inst_25207 = cljs.core.concat.call(null,inst_25205,inst_25206);
var state_25303__$1 = (function (){var statearr_25318 = state_25303;
(statearr_25318[(12)] = inst_25207);

(statearr_25318[(17)] = inst_25203);

(statearr_25318[(18)] = inst_25201);

return statearr_25318;
})();
if(cljs.core.truth_(reload_dependents)){
var statearr_25319_25403 = state_25303__$1;
(statearr_25319_25403[(1)] = (16));

} else {
var statearr_25320_25404 = state_25303__$1;
(statearr_25320_25404[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (15))){
var inst_25191 = (state_25303[(2)]);
var state_25303__$1 = state_25303;
var statearr_25321_25405 = state_25303__$1;
(statearr_25321_25405[(2)] = inst_25191);

(statearr_25321_25405[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (21))){
var inst_25217 = (state_25303[(19)]);
var inst_25217__$1 = (state_25303[(2)]);
var inst_25218 = figwheel.client.file_reloading.load_all_js_files.call(null,inst_25217__$1);
var state_25303__$1 = (function (){var statearr_25322 = state_25303;
(statearr_25322[(19)] = inst_25217__$1);

return statearr_25322;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_25303__$1,(22),inst_25218);
} else {
if((state_val_25304 === (31))){
var inst_25301 = (state_25303[(2)]);
var state_25303__$1 = state_25303;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_25303__$1,inst_25301);
} else {
if((state_val_25304 === (32))){
var inst_25257 = (state_25303[(16)]);
var inst_25262 = inst_25257.cljs$lang$protocol_mask$partition0$;
var inst_25263 = (inst_25262 & (64));
var inst_25264 = inst_25257.cljs$core$ISeq$;
var inst_25265 = (inst_25263) || (inst_25264);
var state_25303__$1 = state_25303;
if(cljs.core.truth_(inst_25265)){
var statearr_25323_25406 = state_25303__$1;
(statearr_25323_25406[(1)] = (35));

} else {
var statearr_25324_25407 = state_25303__$1;
(statearr_25324_25407[(1)] = (36));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (40))){
var inst_25278 = (state_25303[(20)]);
var inst_25277 = (state_25303[(2)]);
var inst_25278__$1 = cljs.core.get.call(null,inst_25277,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179));
var inst_25279 = cljs.core.get.call(null,inst_25277,new cljs.core.Keyword(null,"not-required","not-required",-950359114));
var inst_25280 = cljs.core.not_empty.call(null,inst_25278__$1);
var state_25303__$1 = (function (){var statearr_25325 = state_25303;
(statearr_25325[(21)] = inst_25279);

(statearr_25325[(20)] = inst_25278__$1);

return statearr_25325;
})();
if(cljs.core.truth_(inst_25280)){
var statearr_25326_25408 = state_25303__$1;
(statearr_25326_25408[(1)] = (41));

} else {
var statearr_25327_25409 = state_25303__$1;
(statearr_25327_25409[(1)] = (42));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (33))){
var state_25303__$1 = state_25303;
var statearr_25328_25410 = state_25303__$1;
(statearr_25328_25410[(2)] = false);

(statearr_25328_25410[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (13))){
var inst_25177 = (state_25303[(22)]);
var inst_25181 = cljs.core.chunk_first.call(null,inst_25177);
var inst_25182 = cljs.core.chunk_rest.call(null,inst_25177);
var inst_25183 = cljs.core.count.call(null,inst_25181);
var inst_25164 = inst_25182;
var inst_25165 = inst_25181;
var inst_25166 = inst_25183;
var inst_25167 = (0);
var state_25303__$1 = (function (){var statearr_25329 = state_25303;
(statearr_25329[(7)] = inst_25165);

(statearr_25329[(8)] = inst_25166);

(statearr_25329[(9)] = inst_25164);

(statearr_25329[(10)] = inst_25167);

return statearr_25329;
})();
var statearr_25330_25411 = state_25303__$1;
(statearr_25330_25411[(2)] = null);

(statearr_25330_25411[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (22))){
var inst_25225 = (state_25303[(23)]);
var inst_25221 = (state_25303[(24)]);
var inst_25217 = (state_25303[(19)]);
var inst_25220 = (state_25303[(25)]);
var inst_25220__$1 = (state_25303[(2)]);
var inst_25221__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_25220__$1);
var inst_25222 = (function (){var all_files = inst_25217;
var res_SINGLEQUOTE_ = inst_25220__$1;
var res = inst_25221__$1;
return ((function (all_files,res_SINGLEQUOTE_,res,inst_25225,inst_25221,inst_25217,inst_25220,inst_25220__$1,inst_25221__$1,state_val_25304,c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__24900_SHARP_){
return cljs.core.not.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375).cljs$core$IFn$_invoke$arity$1(p1__24900_SHARP_));
});
;})(all_files,res_SINGLEQUOTE_,res,inst_25225,inst_25221,inst_25217,inst_25220,inst_25220__$1,inst_25221__$1,state_val_25304,c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_25223 = cljs.core.filter.call(null,inst_25222,inst_25220__$1);
var inst_25224 = cljs.core.deref.call(null,figwheel.client.file_reloading.dependencies_loaded);
var inst_25225__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_25224);
var inst_25226 = cljs.core.not_empty.call(null,inst_25225__$1);
var state_25303__$1 = (function (){var statearr_25331 = state_25303;
(statearr_25331[(23)] = inst_25225__$1);

(statearr_25331[(24)] = inst_25221__$1);

(statearr_25331[(25)] = inst_25220__$1);

(statearr_25331[(26)] = inst_25223);

return statearr_25331;
})();
if(cljs.core.truth_(inst_25226)){
var statearr_25332_25412 = state_25303__$1;
(statearr_25332_25412[(1)] = (23));

} else {
var statearr_25333_25413 = state_25303__$1;
(statearr_25333_25413[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (36))){
var state_25303__$1 = state_25303;
var statearr_25334_25414 = state_25303__$1;
(statearr_25334_25414[(2)] = false);

(statearr_25334_25414[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (41))){
var inst_25278 = (state_25303[(20)]);
var inst_25282 = cljs.core.comp.call(null,figwheel.client.file_reloading.name__GT_path,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var inst_25283 = cljs.core.map.call(null,inst_25282,inst_25278);
var inst_25284 = cljs.core.pr_str.call(null,inst_25283);
var inst_25285 = [cljs.core.str("figwheel-no-load meta-data: "),cljs.core.str(inst_25284)].join('');
var inst_25286 = figwheel.client.utils.log.call(null,inst_25285);
var state_25303__$1 = state_25303;
var statearr_25335_25415 = state_25303__$1;
(statearr_25335_25415[(2)] = inst_25286);

(statearr_25335_25415[(1)] = (43));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (43))){
var inst_25279 = (state_25303[(21)]);
var inst_25289 = (state_25303[(2)]);
var inst_25290 = cljs.core.not_empty.call(null,inst_25279);
var state_25303__$1 = (function (){var statearr_25336 = state_25303;
(statearr_25336[(27)] = inst_25289);

return statearr_25336;
})();
if(cljs.core.truth_(inst_25290)){
var statearr_25337_25416 = state_25303__$1;
(statearr_25337_25416[(1)] = (44));

} else {
var statearr_25338_25417 = state_25303__$1;
(statearr_25338_25417[(1)] = (45));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (29))){
var inst_25257 = (state_25303[(16)]);
var inst_25225 = (state_25303[(23)]);
var inst_25221 = (state_25303[(24)]);
var inst_25217 = (state_25303[(19)]);
var inst_25220 = (state_25303[(25)]);
var inst_25223 = (state_25303[(26)]);
var inst_25253 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: NOT loading these files ");
var inst_25256 = (function (){var all_files = inst_25217;
var res_SINGLEQUOTE_ = inst_25220;
var res = inst_25221;
var files_not_loaded = inst_25223;
var dependencies_that_loaded = inst_25225;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_25257,inst_25225,inst_25221,inst_25217,inst_25220,inst_25223,inst_25253,state_val_25304,c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__25255){
var map__25339 = p__25255;
var map__25339__$1 = ((((!((map__25339 == null)))?((((map__25339.cljs$lang$protocol_mask$partition0$ & (64))) || (map__25339.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__25339):map__25339);
var namespace = cljs.core.get.call(null,map__25339__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var meta_data = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
if((meta_data == null)){
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);
} else {
if(cljs.core.truth_(meta_data.call(null,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179)))){
return new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179);
} else {
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);

}
}
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_25257,inst_25225,inst_25221,inst_25217,inst_25220,inst_25223,inst_25253,state_val_25304,c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_25257__$1 = cljs.core.group_by.call(null,inst_25256,inst_25223);
var inst_25259 = (inst_25257__$1 == null);
var inst_25260 = cljs.core.not.call(null,inst_25259);
var state_25303__$1 = (function (){var statearr_25341 = state_25303;
(statearr_25341[(16)] = inst_25257__$1);

(statearr_25341[(28)] = inst_25253);

return statearr_25341;
})();
if(inst_25260){
var statearr_25342_25418 = state_25303__$1;
(statearr_25342_25418[(1)] = (32));

} else {
var statearr_25343_25419 = state_25303__$1;
(statearr_25343_25419[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (44))){
var inst_25279 = (state_25303[(21)]);
var inst_25292 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_25279);
var inst_25293 = cljs.core.pr_str.call(null,inst_25292);
var inst_25294 = [cljs.core.str("not required: "),cljs.core.str(inst_25293)].join('');
var inst_25295 = figwheel.client.utils.log.call(null,inst_25294);
var state_25303__$1 = state_25303;
var statearr_25344_25420 = state_25303__$1;
(statearr_25344_25420[(2)] = inst_25295);

(statearr_25344_25420[(1)] = (46));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (6))){
var inst_25198 = (state_25303[(2)]);
var state_25303__$1 = state_25303;
var statearr_25345_25421 = state_25303__$1;
(statearr_25345_25421[(2)] = inst_25198);

(statearr_25345_25421[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (28))){
var inst_25223 = (state_25303[(26)]);
var inst_25250 = (state_25303[(2)]);
var inst_25251 = cljs.core.not_empty.call(null,inst_25223);
var state_25303__$1 = (function (){var statearr_25346 = state_25303;
(statearr_25346[(29)] = inst_25250);

return statearr_25346;
})();
if(cljs.core.truth_(inst_25251)){
var statearr_25347_25422 = state_25303__$1;
(statearr_25347_25422[(1)] = (29));

} else {
var statearr_25348_25423 = state_25303__$1;
(statearr_25348_25423[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (25))){
var inst_25221 = (state_25303[(24)]);
var inst_25237 = (state_25303[(2)]);
var inst_25238 = cljs.core.not_empty.call(null,inst_25221);
var state_25303__$1 = (function (){var statearr_25349 = state_25303;
(statearr_25349[(30)] = inst_25237);

return statearr_25349;
})();
if(cljs.core.truth_(inst_25238)){
var statearr_25350_25424 = state_25303__$1;
(statearr_25350_25424[(1)] = (26));

} else {
var statearr_25351_25425 = state_25303__$1;
(statearr_25351_25425[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (34))){
var inst_25272 = (state_25303[(2)]);
var state_25303__$1 = state_25303;
if(cljs.core.truth_(inst_25272)){
var statearr_25352_25426 = state_25303__$1;
(statearr_25352_25426[(1)] = (38));

} else {
var statearr_25353_25427 = state_25303__$1;
(statearr_25353_25427[(1)] = (39));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (17))){
var state_25303__$1 = state_25303;
var statearr_25354_25428 = state_25303__$1;
(statearr_25354_25428[(2)] = recompile_dependents);

(statearr_25354_25428[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (3))){
var state_25303__$1 = state_25303;
var statearr_25355_25429 = state_25303__$1;
(statearr_25355_25429[(2)] = null);

(statearr_25355_25429[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (12))){
var inst_25194 = (state_25303[(2)]);
var state_25303__$1 = state_25303;
var statearr_25356_25430 = state_25303__$1;
(statearr_25356_25430[(2)] = inst_25194);

(statearr_25356_25430[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (2))){
var inst_25156 = (state_25303[(13)]);
var inst_25163 = cljs.core.seq.call(null,inst_25156);
var inst_25164 = inst_25163;
var inst_25165 = null;
var inst_25166 = (0);
var inst_25167 = (0);
var state_25303__$1 = (function (){var statearr_25357 = state_25303;
(statearr_25357[(7)] = inst_25165);

(statearr_25357[(8)] = inst_25166);

(statearr_25357[(9)] = inst_25164);

(statearr_25357[(10)] = inst_25167);

return statearr_25357;
})();
var statearr_25358_25431 = state_25303__$1;
(statearr_25358_25431[(2)] = null);

(statearr_25358_25431[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (23))){
var inst_25225 = (state_25303[(23)]);
var inst_25221 = (state_25303[(24)]);
var inst_25217 = (state_25303[(19)]);
var inst_25220 = (state_25303[(25)]);
var inst_25223 = (state_25303[(26)]);
var inst_25228 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these dependencies");
var inst_25230 = (function (){var all_files = inst_25217;
var res_SINGLEQUOTE_ = inst_25220;
var res = inst_25221;
var files_not_loaded = inst_25223;
var dependencies_that_loaded = inst_25225;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_25225,inst_25221,inst_25217,inst_25220,inst_25223,inst_25228,state_val_25304,c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__25229){
var map__25359 = p__25229;
var map__25359__$1 = ((((!((map__25359 == null)))?((((map__25359.cljs$lang$protocol_mask$partition0$ & (64))) || (map__25359.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__25359):map__25359);
var request_url = cljs.core.get.call(null,map__25359__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
return clojure.string.replace.call(null,request_url,goog.basePath,"");
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_25225,inst_25221,inst_25217,inst_25220,inst_25223,inst_25228,state_val_25304,c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_25231 = cljs.core.reverse.call(null,inst_25225);
var inst_25232 = cljs.core.map.call(null,inst_25230,inst_25231);
var inst_25233 = cljs.core.pr_str.call(null,inst_25232);
var inst_25234 = figwheel.client.utils.log.call(null,inst_25233);
var state_25303__$1 = (function (){var statearr_25361 = state_25303;
(statearr_25361[(31)] = inst_25228);

return statearr_25361;
})();
var statearr_25362_25432 = state_25303__$1;
(statearr_25362_25432[(2)] = inst_25234);

(statearr_25362_25432[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (35))){
var state_25303__$1 = state_25303;
var statearr_25363_25433 = state_25303__$1;
(statearr_25363_25433[(2)] = true);

(statearr_25363_25433[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (19))){
var inst_25207 = (state_25303[(12)]);
var inst_25213 = figwheel.client.file_reloading.expand_files.call(null,inst_25207);
var state_25303__$1 = state_25303;
var statearr_25364_25434 = state_25303__$1;
(statearr_25364_25434[(2)] = inst_25213);

(statearr_25364_25434[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (11))){
var state_25303__$1 = state_25303;
var statearr_25365_25435 = state_25303__$1;
(statearr_25365_25435[(2)] = null);

(statearr_25365_25435[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (9))){
var inst_25196 = (state_25303[(2)]);
var state_25303__$1 = state_25303;
var statearr_25366_25436 = state_25303__$1;
(statearr_25366_25436[(2)] = inst_25196);

(statearr_25366_25436[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (5))){
var inst_25166 = (state_25303[(8)]);
var inst_25167 = (state_25303[(10)]);
var inst_25169 = (inst_25167 < inst_25166);
var inst_25170 = inst_25169;
var state_25303__$1 = state_25303;
if(cljs.core.truth_(inst_25170)){
var statearr_25367_25437 = state_25303__$1;
(statearr_25367_25437[(1)] = (7));

} else {
var statearr_25368_25438 = state_25303__$1;
(statearr_25368_25438[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (14))){
var inst_25177 = (state_25303[(22)]);
var inst_25186 = cljs.core.first.call(null,inst_25177);
var inst_25187 = figwheel.client.file_reloading.eval_body.call(null,inst_25186,opts);
var inst_25188 = cljs.core.next.call(null,inst_25177);
var inst_25164 = inst_25188;
var inst_25165 = null;
var inst_25166 = (0);
var inst_25167 = (0);
var state_25303__$1 = (function (){var statearr_25369 = state_25303;
(statearr_25369[(7)] = inst_25165);

(statearr_25369[(8)] = inst_25166);

(statearr_25369[(9)] = inst_25164);

(statearr_25369[(10)] = inst_25167);

(statearr_25369[(32)] = inst_25187);

return statearr_25369;
})();
var statearr_25370_25439 = state_25303__$1;
(statearr_25370_25439[(2)] = null);

(statearr_25370_25439[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (45))){
var state_25303__$1 = state_25303;
var statearr_25371_25440 = state_25303__$1;
(statearr_25371_25440[(2)] = null);

(statearr_25371_25440[(1)] = (46));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (26))){
var inst_25225 = (state_25303[(23)]);
var inst_25221 = (state_25303[(24)]);
var inst_25217 = (state_25303[(19)]);
var inst_25220 = (state_25303[(25)]);
var inst_25223 = (state_25303[(26)]);
var inst_25240 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these files");
var inst_25242 = (function (){var all_files = inst_25217;
var res_SINGLEQUOTE_ = inst_25220;
var res = inst_25221;
var files_not_loaded = inst_25223;
var dependencies_that_loaded = inst_25225;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_25225,inst_25221,inst_25217,inst_25220,inst_25223,inst_25240,state_val_25304,c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__25241){
var map__25372 = p__25241;
var map__25372__$1 = ((((!((map__25372 == null)))?((((map__25372.cljs$lang$protocol_mask$partition0$ & (64))) || (map__25372.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__25372):map__25372);
var namespace = cljs.core.get.call(null,map__25372__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var file = cljs.core.get.call(null,map__25372__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
if(cljs.core.truth_(namespace)){
return figwheel.client.file_reloading.name__GT_path.call(null,cljs.core.name.call(null,namespace));
} else {
return file;
}
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_25225,inst_25221,inst_25217,inst_25220,inst_25223,inst_25240,state_val_25304,c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_25243 = cljs.core.map.call(null,inst_25242,inst_25221);
var inst_25244 = cljs.core.pr_str.call(null,inst_25243);
var inst_25245 = figwheel.client.utils.log.call(null,inst_25244);
var inst_25246 = (function (){var all_files = inst_25217;
var res_SINGLEQUOTE_ = inst_25220;
var res = inst_25221;
var files_not_loaded = inst_25223;
var dependencies_that_loaded = inst_25225;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_25225,inst_25221,inst_25217,inst_25220,inst_25223,inst_25240,inst_25242,inst_25243,inst_25244,inst_25245,state_val_25304,c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (){
figwheel.client.file_reloading.on_jsload_custom_event.call(null,res);

return cljs.core.apply.call(null,on_jsload,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [res], null));
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_25225,inst_25221,inst_25217,inst_25220,inst_25223,inst_25240,inst_25242,inst_25243,inst_25244,inst_25245,state_val_25304,c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_25247 = setTimeout(inst_25246,(10));
var state_25303__$1 = (function (){var statearr_25374 = state_25303;
(statearr_25374[(33)] = inst_25245);

(statearr_25374[(34)] = inst_25240);

return statearr_25374;
})();
var statearr_25375_25441 = state_25303__$1;
(statearr_25375_25441[(2)] = inst_25247);

(statearr_25375_25441[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (16))){
var state_25303__$1 = state_25303;
var statearr_25376_25442 = state_25303__$1;
(statearr_25376_25442[(2)] = reload_dependents);

(statearr_25376_25442[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (38))){
var inst_25257 = (state_25303[(16)]);
var inst_25274 = cljs.core.apply.call(null,cljs.core.hash_map,inst_25257);
var state_25303__$1 = state_25303;
var statearr_25377_25443 = state_25303__$1;
(statearr_25377_25443[(2)] = inst_25274);

(statearr_25377_25443[(1)] = (40));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (30))){
var state_25303__$1 = state_25303;
var statearr_25378_25444 = state_25303__$1;
(statearr_25378_25444[(2)] = null);

(statearr_25378_25444[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (10))){
var inst_25177 = (state_25303[(22)]);
var inst_25179 = cljs.core.chunked_seq_QMARK_.call(null,inst_25177);
var state_25303__$1 = state_25303;
if(inst_25179){
var statearr_25379_25445 = state_25303__$1;
(statearr_25379_25445[(1)] = (13));

} else {
var statearr_25380_25446 = state_25303__$1;
(statearr_25380_25446[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (18))){
var inst_25211 = (state_25303[(2)]);
var state_25303__$1 = state_25303;
if(cljs.core.truth_(inst_25211)){
var statearr_25381_25447 = state_25303__$1;
(statearr_25381_25447[(1)] = (19));

} else {
var statearr_25382_25448 = state_25303__$1;
(statearr_25382_25448[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (42))){
var state_25303__$1 = state_25303;
var statearr_25383_25449 = state_25303__$1;
(statearr_25383_25449[(2)] = null);

(statearr_25383_25449[(1)] = (43));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (37))){
var inst_25269 = (state_25303[(2)]);
var state_25303__$1 = state_25303;
var statearr_25384_25450 = state_25303__$1;
(statearr_25384_25450[(2)] = inst_25269);

(statearr_25384_25450[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_25304 === (8))){
var inst_25164 = (state_25303[(9)]);
var inst_25177 = (state_25303[(22)]);
var inst_25177__$1 = cljs.core.seq.call(null,inst_25164);
var state_25303__$1 = (function (){var statearr_25385 = state_25303;
(statearr_25385[(22)] = inst_25177__$1);

return statearr_25385;
})();
if(inst_25177__$1){
var statearr_25386_25451 = state_25303__$1;
(statearr_25386_25451[(1)] = (10));

} else {
var statearr_25387_25452 = state_25303__$1;
(statearr_25387_25452[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents))
;
return ((function (switch__21501__auto__,c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents){
return (function() {
var figwheel$client$file_reloading$reload_js_files_$_state_machine__21502__auto__ = null;
var figwheel$client$file_reloading$reload_js_files_$_state_machine__21502__auto____0 = (function (){
var statearr_25391 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_25391[(0)] = figwheel$client$file_reloading$reload_js_files_$_state_machine__21502__auto__);

(statearr_25391[(1)] = (1));

return statearr_25391;
});
var figwheel$client$file_reloading$reload_js_files_$_state_machine__21502__auto____1 = (function (state_25303){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_25303);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e25392){if((e25392 instanceof Object)){
var ex__21505__auto__ = e25392;
var statearr_25393_25453 = state_25303;
(statearr_25393_25453[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_25303);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e25392;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__25454 = state_25303;
state_25303 = G__25454;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
figwheel$client$file_reloading$reload_js_files_$_state_machine__21502__auto__ = function(state_25303){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__21502__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__21502__auto____1.call(this,state_25303);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$reload_js_files_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$reload_js_files_$_state_machine__21502__auto____0;
figwheel$client$file_reloading$reload_js_files_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$reload_js_files_$_state_machine__21502__auto____1;
return figwheel$client$file_reloading$reload_js_files_$_state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var state__21568__auto__ = (function (){var statearr_25394 = f__21567__auto__.call(null);
(statearr_25394[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto__);

return statearr_25394;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto__,map__25149,map__25149__$1,opts,before_jsload,on_jsload,reload_dependents,map__25150,map__25150__$1,msg,files,figwheel_meta,recompile_dependents))
);

return c__21566__auto__;
});
figwheel.client.file_reloading.current_links = (function figwheel$client$file_reloading$current_links(){
return Array.prototype.slice.call(document.getElementsByTagName("link"));
});
figwheel.client.file_reloading.truncate_url = (function figwheel$client$file_reloading$truncate_url(url){
return clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,cljs.core.first.call(null,clojure.string.split.call(null,url,/\?/)),[cljs.core.str(location.protocol),cljs.core.str("//")].join(''),""),".*://",""),/^\/\//,""),/[^\\/]*/,"");
});
figwheel.client.file_reloading.matches_file_QMARK_ = (function figwheel$client$file_reloading$matches_file_QMARK_(p__25457,link){
var map__25460 = p__25457;
var map__25460__$1 = ((((!((map__25460 == null)))?((((map__25460.cljs$lang$protocol_mask$partition0$ & (64))) || (map__25460.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__25460):map__25460);
var file = cljs.core.get.call(null,map__25460__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var temp__4425__auto__ = link.href;
if(cljs.core.truth_(temp__4425__auto__)){
var link_href = temp__4425__auto__;
var match = clojure.string.join.call(null,"/",cljs.core.take_while.call(null,cljs.core.identity,cljs.core.map.call(null,((function (link_href,temp__4425__auto__,map__25460,map__25460__$1,file){
return (function (p1__25455_SHARP_,p2__25456_SHARP_){
if(cljs.core._EQ_.call(null,p1__25455_SHARP_,p2__25456_SHARP_)){
return p1__25455_SHARP_;
} else {
return false;
}
});})(link_href,temp__4425__auto__,map__25460,map__25460__$1,file))
,cljs.core.reverse.call(null,clojure.string.split.call(null,file,"/")),cljs.core.reverse.call(null,clojure.string.split.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href),"/")))));
var match_length = cljs.core.count.call(null,match);
var file_name_length = cljs.core.count.call(null,cljs.core.last.call(null,clojure.string.split.call(null,file,"/")));
if((match_length >= file_name_length)){
return new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"link","link",-1769163468),link,new cljs.core.Keyword(null,"link-href","link-href",-250644450),link_href,new cljs.core.Keyword(null,"match-length","match-length",1101537310),match_length,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083),cljs.core.count.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href))], null);
} else {
return null;
}
} else {
return null;
}
});
figwheel.client.file_reloading.get_correct_link = (function figwheel$client$file_reloading$get_correct_link(f_data){
var temp__4425__auto__ = cljs.core.first.call(null,cljs.core.sort_by.call(null,(function (p__25466){
var map__25467 = p__25466;
var map__25467__$1 = ((((!((map__25467 == null)))?((((map__25467.cljs$lang$protocol_mask$partition0$ & (64))) || (map__25467.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__25467):map__25467);
var match_length = cljs.core.get.call(null,map__25467__$1,new cljs.core.Keyword(null,"match-length","match-length",1101537310));
var current_url_length = cljs.core.get.call(null,map__25467__$1,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083));
return (current_url_length - match_length);
}),cljs.core.keep.call(null,(function (p1__25462_SHARP_){
return figwheel.client.file_reloading.matches_file_QMARK_.call(null,f_data,p1__25462_SHARP_);
}),figwheel.client.file_reloading.current_links.call(null))));
if(cljs.core.truth_(temp__4425__auto__)){
var res = temp__4425__auto__;
return new cljs.core.Keyword(null,"link","link",-1769163468).cljs$core$IFn$_invoke$arity$1(res);
} else {
return null;
}
});
figwheel.client.file_reloading.clone_link = (function figwheel$client$file_reloading$clone_link(link,url){
var clone = document.createElement("link");
clone.rel = "stylesheet";

clone.media = link.media;

clone.disabled = link.disabled;

clone.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return clone;
});
figwheel.client.file_reloading.create_link = (function figwheel$client$file_reloading$create_link(url){
var link = document.createElement("link");
link.rel = "stylesheet";

link.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return link;
});
figwheel.client.file_reloading.add_link_to_doc = (function figwheel$client$file_reloading$add_link_to_doc(var_args){
var args25469 = [];
var len__17855__auto___25472 = arguments.length;
var i__17856__auto___25473 = (0);
while(true){
if((i__17856__auto___25473 < len__17855__auto___25472)){
args25469.push((arguments[i__17856__auto___25473]));

var G__25474 = (i__17856__auto___25473 + (1));
i__17856__auto___25473 = G__25474;
continue;
} else {
}
break;
}

var G__25471 = args25469.length;
switch (G__25471) {
case 1:
return figwheel.client.file_reloading.add_link_to_doc.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return figwheel.client.file_reloading.add_link_to_doc.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args25469.length)].join('')));

}
});

figwheel.client.file_reloading.add_link_to_doc.cljs$core$IFn$_invoke$arity$1 = (function (new_link){
return (document.getElementsByTagName("head")[(0)]).appendChild(new_link);
});

figwheel.client.file_reloading.add_link_to_doc.cljs$core$IFn$_invoke$arity$2 = (function (orig_link,klone){
var parent = orig_link.parentNode;
if(cljs.core._EQ_.call(null,orig_link,parent.lastChild)){
parent.appendChild(klone);
} else {
parent.insertBefore(klone,orig_link.nextSibling);
}

return setTimeout(((function (parent){
return (function (){
return parent.removeChild(orig_link);
});})(parent))
,(300));
});

figwheel.client.file_reloading.add_link_to_doc.cljs$lang$maxFixedArity = 2;
figwheel.client.file_reloading.distictify = (function figwheel$client$file_reloading$distictify(key,seqq){
return cljs.core.vals.call(null,cljs.core.reduce.call(null,(function (p1__25476_SHARP_,p2__25477_SHARP_){
return cljs.core.assoc.call(null,p1__25476_SHARP_,cljs.core.get.call(null,p2__25477_SHARP_,key),p2__25477_SHARP_);
}),cljs.core.PersistentArrayMap.EMPTY,seqq));
});
figwheel.client.file_reloading.reload_css_file = (function figwheel$client$file_reloading$reload_css_file(p__25478){
var map__25481 = p__25478;
var map__25481__$1 = ((((!((map__25481 == null)))?((((map__25481.cljs$lang$protocol_mask$partition0$ & (64))) || (map__25481.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__25481):map__25481);
var f_data = map__25481__$1;
var file = cljs.core.get.call(null,map__25481__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var temp__4425__auto__ = figwheel.client.file_reloading.get_correct_link.call(null,f_data);
if(cljs.core.truth_(temp__4425__auto__)){
var link = temp__4425__auto__;
return figwheel.client.file_reloading.add_link_to_doc.call(null,link,figwheel.client.file_reloading.clone_link.call(null,link,link.href));
} else {
return null;
}
});
figwheel.client.file_reloading.reload_css_files = (function figwheel$client$file_reloading$reload_css_files(p__25483,files_msg){
var map__25490 = p__25483;
var map__25490__$1 = ((((!((map__25490 == null)))?((((map__25490.cljs$lang$protocol_mask$partition0$ & (64))) || (map__25490.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__25490):map__25490);
var opts = map__25490__$1;
var on_cssload = cljs.core.get.call(null,map__25490__$1,new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318));
if(cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))){
var seq__25492_25496 = cljs.core.seq.call(null,figwheel.client.file_reloading.distictify.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(files_msg)));
var chunk__25493_25497 = null;
var count__25494_25498 = (0);
var i__25495_25499 = (0);
while(true){
if((i__25495_25499 < count__25494_25498)){
var f_25500 = cljs.core._nth.call(null,chunk__25493_25497,i__25495_25499);
figwheel.client.file_reloading.reload_css_file.call(null,f_25500);

var G__25501 = seq__25492_25496;
var G__25502 = chunk__25493_25497;
var G__25503 = count__25494_25498;
var G__25504 = (i__25495_25499 + (1));
seq__25492_25496 = G__25501;
chunk__25493_25497 = G__25502;
count__25494_25498 = G__25503;
i__25495_25499 = G__25504;
continue;
} else {
var temp__4425__auto___25505 = cljs.core.seq.call(null,seq__25492_25496);
if(temp__4425__auto___25505){
var seq__25492_25506__$1 = temp__4425__auto___25505;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__25492_25506__$1)){
var c__17600__auto___25507 = cljs.core.chunk_first.call(null,seq__25492_25506__$1);
var G__25508 = cljs.core.chunk_rest.call(null,seq__25492_25506__$1);
var G__25509 = c__17600__auto___25507;
var G__25510 = cljs.core.count.call(null,c__17600__auto___25507);
var G__25511 = (0);
seq__25492_25496 = G__25508;
chunk__25493_25497 = G__25509;
count__25494_25498 = G__25510;
i__25495_25499 = G__25511;
continue;
} else {
var f_25512 = cljs.core.first.call(null,seq__25492_25506__$1);
figwheel.client.file_reloading.reload_css_file.call(null,f_25512);

var G__25513 = cljs.core.next.call(null,seq__25492_25506__$1);
var G__25514 = null;
var G__25515 = (0);
var G__25516 = (0);
seq__25492_25496 = G__25513;
chunk__25493_25497 = G__25514;
count__25494_25498 = G__25515;
i__25495_25499 = G__25516;
continue;
}
} else {
}
}
break;
}

return setTimeout(((function (map__25490,map__25490__$1,opts,on_cssload){
return (function (){
return on_cssload.call(null,new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(files_msg));
});})(map__25490,map__25490__$1,opts,on_cssload))
,(100));
} else {
return null;
}
});

//# sourceMappingURL=file_reloading.js.map?rel=1448506747290