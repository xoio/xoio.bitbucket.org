// Compiled by ClojureScript 1.7.170 {}
goog.provide('figwheel.client');
goog.require('cljs.core');
goog.require('goog.userAgent.product');
goog.require('goog.Uri');
goog.require('cljs.core.async');
goog.require('figwheel.client.socket');
goog.require('figwheel.client.file_reloading');
goog.require('clojure.string');
goog.require('figwheel.client.utils');
goog.require('cljs.repl');
goog.require('figwheel.client.heads_up');
figwheel.client.figwheel_repl_print = (function figwheel$client$figwheel_repl_print(args){
figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"callback",new cljs.core.Keyword(null,"callback-name","callback-name",336964714),"figwheel-repl-print",new cljs.core.Keyword(null,"content","content",15833224),args], null));

return args;
});
figwheel.client.autoload_QMARK_ = (cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))?(function (){
var pred__25952 = cljs.core._EQ_;
var expr__25953 = (function (){var or__16796__auto__ = localStorage.getItem("figwheel_autoload");
if(cljs.core.truth_(or__16796__auto__)){
return or__16796__auto__;
} else {
return "true";
}
})();
if(cljs.core.truth_(pred__25952.call(null,"true",expr__25953))){
return true;
} else {
if(cljs.core.truth_(pred__25952.call(null,"false",expr__25953))){
return false;
} else {
throw (new Error([cljs.core.str("No matching clause: "),cljs.core.str(expr__25953)].join('')));
}
}
}):(function (){
return true;
}));
figwheel.client.toggle_autoload = (function figwheel$client$toggle_autoload(){
if(cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))){
localStorage.setItem("figwheel_autoload",cljs.core.not.call(null,figwheel.client.autoload_QMARK_.call(null)));

return figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),[cljs.core.str("Figwheel autoloading "),cljs.core.str((cljs.core.truth_(figwheel.client.autoload_QMARK_.call(null))?"ON":"OFF"))].join(''));
} else {
return null;
}
});
goog.exportSymbol('figwheel.client.toggle_autoload', figwheel.client.toggle_autoload);
figwheel.client.console_print = (function figwheel$client$console_print(args){
console.log.apply(console,cljs.core.into_array.call(null,args));

return args;
});
figwheel.client.enable_repl_print_BANG_ = (function figwheel$client$enable_repl_print_BANG_(){
cljs.core._STAR_print_newline_STAR_ = false;

return cljs.core._STAR_print_fn_STAR_ = (function() { 
var G__25955__delegate = function (args){
return figwheel.client.figwheel_repl_print.call(null,figwheel.client.console_print.call(null,args));
};
var G__25955 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__25956__i = 0, G__25956__a = new Array(arguments.length -  0);
while (G__25956__i < G__25956__a.length) {G__25956__a[G__25956__i] = arguments[G__25956__i + 0]; ++G__25956__i;}
  args = new cljs.core.IndexedSeq(G__25956__a,0);
} 
return G__25955__delegate.call(this,args);};
G__25955.cljs$lang$maxFixedArity = 0;
G__25955.cljs$lang$applyTo = (function (arglist__25957){
var args = cljs.core.seq(arglist__25957);
return G__25955__delegate(args);
});
G__25955.cljs$core$IFn$_invoke$arity$variadic = G__25955__delegate;
return G__25955;
})()
;
});
figwheel.client.get_essential_messages = (function figwheel$client$get_essential_messages(ed){
if(cljs.core.truth_(ed)){
return cljs.core.cons.call(null,cljs.core.select_keys.call(null,ed,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"message","message",-406056002),new cljs.core.Keyword(null,"class","class",-2030961996)], null)),figwheel$client$get_essential_messages.call(null,new cljs.core.Keyword(null,"cause","cause",231901252).cljs$core$IFn$_invoke$arity$1(ed)));
} else {
return null;
}
});
figwheel.client.error_msg_format = (function figwheel$client$error_msg_format(p__25958){
var map__25961 = p__25958;
var map__25961__$1 = ((((!((map__25961 == null)))?((((map__25961.cljs$lang$protocol_mask$partition0$ & (64))) || (map__25961.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__25961):map__25961);
var message = cljs.core.get.call(null,map__25961__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var class$ = cljs.core.get.call(null,map__25961__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
return [cljs.core.str(class$),cljs.core.str(" : "),cljs.core.str(message)].join('');
});
figwheel.client.format_messages = cljs.core.comp.call(null,cljs.core.partial.call(null,cljs.core.map,figwheel.client.error_msg_format),figwheel.client.get_essential_messages);
figwheel.client.focus_msgs = (function figwheel$client$focus_msgs(name_set,msg_hist){
return cljs.core.cons.call(null,cljs.core.first.call(null,msg_hist),cljs.core.filter.call(null,cljs.core.comp.call(null,name_set,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863)),cljs.core.rest.call(null,msg_hist)));
});
figwheel.client.reload_file_QMARK__STAR_ = (function figwheel$client$reload_file_QMARK__STAR_(msg_name,opts){
var or__16796__auto__ = new cljs.core.Keyword(null,"load-warninged-code","load-warninged-code",-2030345223).cljs$core$IFn$_invoke$arity$1(opts);
if(cljs.core.truth_(or__16796__auto__)){
return or__16796__auto__;
} else {
return cljs.core.not_EQ_.call(null,msg_name,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356));
}
});
figwheel.client.reload_file_state_QMARK_ = (function figwheel$client$reload_file_state_QMARK_(msg_names,opts){
var and__16784__auto__ = cljs.core._EQ_.call(null,cljs.core.first.call(null,msg_names),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563));
if(and__16784__auto__){
return figwheel.client.reload_file_QMARK__STAR_.call(null,cljs.core.second.call(null,msg_names),opts);
} else {
return and__16784__auto__;
}
});
figwheel.client.block_reload_file_state_QMARK_ = (function figwheel$client$block_reload_file_state_QMARK_(msg_names,opts){
return (cljs.core._EQ_.call(null,cljs.core.first.call(null,msg_names),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563))) && (cljs.core.not.call(null,figwheel.client.reload_file_QMARK__STAR_.call(null,cljs.core.second.call(null,msg_names),opts)));
});
figwheel.client.warning_append_state_QMARK_ = (function figwheel$client$warning_append_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356)], null),cljs.core.take.call(null,(2),msg_names));
});
figwheel.client.warning_state_QMARK_ = (function figwheel$client$warning_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),cljs.core.first.call(null,msg_names));
});
figwheel.client.rewarning_state_QMARK_ = (function figwheel$client$rewarning_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356)], null),cljs.core.take.call(null,(3),msg_names));
});
figwheel.client.compile_fail_state_QMARK_ = (function figwheel$client$compile_fail_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),cljs.core.first.call(null,msg_names));
});
figwheel.client.compile_refail_state_QMARK_ = (function figwheel$client$compile_refail_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289)], null),cljs.core.take.call(null,(2),msg_names));
});
figwheel.client.css_loaded_state_QMARK_ = (function figwheel$client$css_loaded_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"css-files-changed","css-files-changed",720773874),cljs.core.first.call(null,msg_names));
});
figwheel.client.file_reloader_plugin = (function figwheel$client$file_reloader_plugin(opts){
var ch = cljs.core.async.chan.call(null);
var c__21566__auto___26123 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___26123,ch){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___26123,ch){
return (function (state_26092){
var state_val_26093 = (state_26092[(1)]);
if((state_val_26093 === (7))){
var inst_26088 = (state_26092[(2)]);
var state_26092__$1 = state_26092;
var statearr_26094_26124 = state_26092__$1;
(statearr_26094_26124[(2)] = inst_26088);

(statearr_26094_26124[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26093 === (1))){
var state_26092__$1 = state_26092;
var statearr_26095_26125 = state_26092__$1;
(statearr_26095_26125[(2)] = null);

(statearr_26095_26125[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26093 === (4))){
var inst_26045 = (state_26092[(7)]);
var inst_26045__$1 = (state_26092[(2)]);
var state_26092__$1 = (function (){var statearr_26096 = state_26092;
(statearr_26096[(7)] = inst_26045__$1);

return statearr_26096;
})();
if(cljs.core.truth_(inst_26045__$1)){
var statearr_26097_26126 = state_26092__$1;
(statearr_26097_26126[(1)] = (5));

} else {
var statearr_26098_26127 = state_26092__$1;
(statearr_26098_26127[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26093 === (15))){
var inst_26052 = (state_26092[(8)]);
var inst_26067 = new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(inst_26052);
var inst_26068 = cljs.core.first.call(null,inst_26067);
var inst_26069 = new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(inst_26068);
var inst_26070 = [cljs.core.str("Figwheel: Not loading code with warnings - "),cljs.core.str(inst_26069)].join('');
var inst_26071 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),inst_26070);
var state_26092__$1 = state_26092;
var statearr_26099_26128 = state_26092__$1;
(statearr_26099_26128[(2)] = inst_26071);

(statearr_26099_26128[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26093 === (13))){
var inst_26076 = (state_26092[(2)]);
var state_26092__$1 = state_26092;
var statearr_26100_26129 = state_26092__$1;
(statearr_26100_26129[(2)] = inst_26076);

(statearr_26100_26129[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26093 === (6))){
var state_26092__$1 = state_26092;
var statearr_26101_26130 = state_26092__$1;
(statearr_26101_26130[(2)] = null);

(statearr_26101_26130[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26093 === (17))){
var inst_26074 = (state_26092[(2)]);
var state_26092__$1 = state_26092;
var statearr_26102_26131 = state_26092__$1;
(statearr_26102_26131[(2)] = inst_26074);

(statearr_26102_26131[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26093 === (3))){
var inst_26090 = (state_26092[(2)]);
var state_26092__$1 = state_26092;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_26092__$1,inst_26090);
} else {
if((state_val_26093 === (12))){
var inst_26051 = (state_26092[(9)]);
var inst_26065 = figwheel.client.block_reload_file_state_QMARK_.call(null,inst_26051,opts);
var state_26092__$1 = state_26092;
if(cljs.core.truth_(inst_26065)){
var statearr_26103_26132 = state_26092__$1;
(statearr_26103_26132[(1)] = (15));

} else {
var statearr_26104_26133 = state_26092__$1;
(statearr_26104_26133[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26093 === (2))){
var state_26092__$1 = state_26092;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26092__$1,(4),ch);
} else {
if((state_val_26093 === (11))){
var inst_26052 = (state_26092[(8)]);
var inst_26057 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_26058 = figwheel.client.file_reloading.reload_js_files.call(null,opts,inst_26052);
var inst_26059 = cljs.core.async.timeout.call(null,(1000));
var inst_26060 = [inst_26058,inst_26059];
var inst_26061 = (new cljs.core.PersistentVector(null,2,(5),inst_26057,inst_26060,null));
var state_26092__$1 = state_26092;
return cljs.core.async.ioc_alts_BANG_.call(null,state_26092__$1,(14),inst_26061);
} else {
if((state_val_26093 === (9))){
var inst_26052 = (state_26092[(8)]);
var inst_26078 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),"Figwheel: code autoloading is OFF");
var inst_26079 = new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(inst_26052);
var inst_26080 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_26079);
var inst_26081 = [cljs.core.str("Not loading: "),cljs.core.str(inst_26080)].join('');
var inst_26082 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),inst_26081);
var state_26092__$1 = (function (){var statearr_26105 = state_26092;
(statearr_26105[(10)] = inst_26078);

return statearr_26105;
})();
var statearr_26106_26134 = state_26092__$1;
(statearr_26106_26134[(2)] = inst_26082);

(statearr_26106_26134[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26093 === (5))){
var inst_26045 = (state_26092[(7)]);
var inst_26047 = [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),null,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),null];
var inst_26048 = (new cljs.core.PersistentArrayMap(null,2,inst_26047,null));
var inst_26049 = (new cljs.core.PersistentHashSet(null,inst_26048,null));
var inst_26050 = figwheel.client.focus_msgs.call(null,inst_26049,inst_26045);
var inst_26051 = cljs.core.map.call(null,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863),inst_26050);
var inst_26052 = cljs.core.first.call(null,inst_26050);
var inst_26053 = figwheel.client.autoload_QMARK_.call(null);
var state_26092__$1 = (function (){var statearr_26107 = state_26092;
(statearr_26107[(9)] = inst_26051);

(statearr_26107[(8)] = inst_26052);

return statearr_26107;
})();
if(cljs.core.truth_(inst_26053)){
var statearr_26108_26135 = state_26092__$1;
(statearr_26108_26135[(1)] = (8));

} else {
var statearr_26109_26136 = state_26092__$1;
(statearr_26109_26136[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26093 === (14))){
var inst_26063 = (state_26092[(2)]);
var state_26092__$1 = state_26092;
var statearr_26110_26137 = state_26092__$1;
(statearr_26110_26137[(2)] = inst_26063);

(statearr_26110_26137[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26093 === (16))){
var state_26092__$1 = state_26092;
var statearr_26111_26138 = state_26092__$1;
(statearr_26111_26138[(2)] = null);

(statearr_26111_26138[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26093 === (10))){
var inst_26084 = (state_26092[(2)]);
var state_26092__$1 = (function (){var statearr_26112 = state_26092;
(statearr_26112[(11)] = inst_26084);

return statearr_26112;
})();
var statearr_26113_26139 = state_26092__$1;
(statearr_26113_26139[(2)] = null);

(statearr_26113_26139[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26093 === (8))){
var inst_26051 = (state_26092[(9)]);
var inst_26055 = figwheel.client.reload_file_state_QMARK_.call(null,inst_26051,opts);
var state_26092__$1 = state_26092;
if(cljs.core.truth_(inst_26055)){
var statearr_26114_26140 = state_26092__$1;
(statearr_26114_26140[(1)] = (11));

} else {
var statearr_26115_26141 = state_26092__$1;
(statearr_26115_26141[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto___26123,ch))
;
return ((function (switch__21501__auto__,c__21566__auto___26123,ch){
return (function() {
var figwheel$client$file_reloader_plugin_$_state_machine__21502__auto__ = null;
var figwheel$client$file_reloader_plugin_$_state_machine__21502__auto____0 = (function (){
var statearr_26119 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_26119[(0)] = figwheel$client$file_reloader_plugin_$_state_machine__21502__auto__);

(statearr_26119[(1)] = (1));

return statearr_26119;
});
var figwheel$client$file_reloader_plugin_$_state_machine__21502__auto____1 = (function (state_26092){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_26092);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e26120){if((e26120 instanceof Object)){
var ex__21505__auto__ = e26120;
var statearr_26121_26142 = state_26092;
(statearr_26121_26142[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_26092);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e26120;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__26143 = state_26092;
state_26092 = G__26143;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
figwheel$client$file_reloader_plugin_$_state_machine__21502__auto__ = function(state_26092){
switch(arguments.length){
case 0:
return figwheel$client$file_reloader_plugin_$_state_machine__21502__auto____0.call(this);
case 1:
return figwheel$client$file_reloader_plugin_$_state_machine__21502__auto____1.call(this,state_26092);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloader_plugin_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloader_plugin_$_state_machine__21502__auto____0;
figwheel$client$file_reloader_plugin_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloader_plugin_$_state_machine__21502__auto____1;
return figwheel$client$file_reloader_plugin_$_state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___26123,ch))
})();
var state__21568__auto__ = (function (){var statearr_26122 = f__21567__auto__.call(null);
(statearr_26122[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___26123);

return statearr_26122;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___26123,ch))
);


return ((function (ch){
return (function (msg_hist){
cljs.core.async.put_BANG_.call(null,ch,msg_hist);

return msg_hist;
});
;})(ch))
});
figwheel.client.truncate_stack_trace = (function figwheel$client$truncate_stack_trace(stack_str){
return cljs.core.take_while.call(null,(function (p1__26144_SHARP_){
return cljs.core.not.call(null,cljs.core.re_matches.call(null,/.*eval_javascript_STAR__STAR_.*/,p1__26144_SHARP_));
}),clojure.string.split_lines.call(null,stack_str));
});
figwheel.client.get_ua_product = (function figwheel$client$get_ua_product(){
if(cljs.core.truth_(figwheel.client.utils.node_env_QMARK_.call(null))){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.SAFARI)){
return new cljs.core.Keyword(null,"safari","safari",497115653);
} else {
if(cljs.core.truth_(goog.userAgent.product.CHROME)){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.FIREFOX)){
return new cljs.core.Keyword(null,"firefox","firefox",1283768880);
} else {
if(cljs.core.truth_(goog.userAgent.product.IE)){
return new cljs.core.Keyword(null,"ie","ie",2038473780);
} else {
return null;
}
}
}
}
}
});
var base_path_26151 = figwheel.client.utils.base_url_path.call(null);
figwheel.client.eval_javascript_STAR__STAR_ = ((function (base_path_26151){
return (function figwheel$client$eval_javascript_STAR__STAR_(code,opts,result_handler){
try{var _STAR_print_fn_STAR_26149 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR_26150 = cljs.core._STAR_print_newline_STAR_;
cljs.core._STAR_print_fn_STAR_ = ((function (_STAR_print_fn_STAR_26149,_STAR_print_newline_STAR_26150,base_path_26151){
return (function() { 
var G__26152__delegate = function (args){
return figwheel.client.figwheel_repl_print.call(null,figwheel.client.console_print.call(null,args));
};
var G__26152 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__26153__i = 0, G__26153__a = new Array(arguments.length -  0);
while (G__26153__i < G__26153__a.length) {G__26153__a[G__26153__i] = arguments[G__26153__i + 0]; ++G__26153__i;}
  args = new cljs.core.IndexedSeq(G__26153__a,0);
} 
return G__26152__delegate.call(this,args);};
G__26152.cljs$lang$maxFixedArity = 0;
G__26152.cljs$lang$applyTo = (function (arglist__26154){
var args = cljs.core.seq(arglist__26154);
return G__26152__delegate(args);
});
G__26152.cljs$core$IFn$_invoke$arity$variadic = G__26152__delegate;
return G__26152;
})()
;})(_STAR_print_fn_STAR_26149,_STAR_print_newline_STAR_26150,base_path_26151))
;

cljs.core._STAR_print_newline_STAR_ = false;

try{return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"success","success",1890645906),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"value","value",305978217),[cljs.core.str(figwheel.client.utils.eval_helper.call(null,code,opts))].join('')], null));
}finally {cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR_26150;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR_26149;
}}catch (e26148){if((e26148 instanceof Error)){
var e = e26148;
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"exception","exception",-335277064),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.pr_str.call(null,e),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"stacktrace","stacktrace",-95588394),clojure.string.join.call(null,"\n",figwheel.client.truncate_stack_trace.call(null,e.stack)),new cljs.core.Keyword(null,"base-path","base-path",495760020),base_path_26151], null));
} else {
var e = e26148;
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"exception","exception",-335277064),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.pr_str.call(null,e),new cljs.core.Keyword(null,"stacktrace","stacktrace",-95588394),"No stacktrace available."], null));

}
}});})(base_path_26151))
;
/**
 * The REPL can disconnect and reconnect lets ensure cljs.user exists at least.
 */
figwheel.client.ensure_cljs_user = (function figwheel$client$ensure_cljs_user(){
if(cljs.core.truth_(cljs.user)){
return null;
} else {
return cljs.user = {};
}
});
figwheel.client.repl_plugin = (function figwheel$client$repl_plugin(p__26155){
var map__26162 = p__26155;
var map__26162__$1 = ((((!((map__26162 == null)))?((((map__26162.cljs$lang$protocol_mask$partition0$ & (64))) || (map__26162.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__26162):map__26162);
var opts = map__26162__$1;
var build_id = cljs.core.get.call(null,map__26162__$1,new cljs.core.Keyword(null,"build-id","build-id",1642831089));
return ((function (map__26162,map__26162__$1,opts,build_id){
return (function (p__26164){
var vec__26165 = p__26164;
var map__26166 = cljs.core.nth.call(null,vec__26165,(0),null);
var map__26166__$1 = ((((!((map__26166 == null)))?((((map__26166.cljs$lang$protocol_mask$partition0$ & (64))) || (map__26166.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__26166):map__26166);
var msg = map__26166__$1;
var msg_name = cljs.core.get.call(null,map__26166__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = cljs.core.nthnext.call(null,vec__26165,(1));
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"repl-eval","repl-eval",-1784727398),msg_name)){
figwheel.client.ensure_cljs_user.call(null);

return figwheel.client.eval_javascript_STAR__STAR_.call(null,new cljs.core.Keyword(null,"code","code",1586293142).cljs$core$IFn$_invoke$arity$1(msg),opts,((function (vec__26165,map__26166,map__26166__$1,msg,msg_name,_,map__26162,map__26162__$1,opts,build_id){
return (function (res){
return figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"callback",new cljs.core.Keyword(null,"callback-name","callback-name",336964714),new cljs.core.Keyword(null,"callback-name","callback-name",336964714).cljs$core$IFn$_invoke$arity$1(msg),new cljs.core.Keyword(null,"content","content",15833224),res], null));
});})(vec__26165,map__26166,map__26166__$1,msg,msg_name,_,map__26162,map__26162__$1,opts,build_id))
);
} else {
return null;
}
});
;})(map__26162,map__26162__$1,opts,build_id))
});
figwheel.client.css_reloader_plugin = (function figwheel$client$css_reloader_plugin(opts){
return (function (p__26172){
var vec__26173 = p__26172;
var map__26174 = cljs.core.nth.call(null,vec__26173,(0),null);
var map__26174__$1 = ((((!((map__26174 == null)))?((((map__26174.cljs$lang$protocol_mask$partition0$ & (64))) || (map__26174.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__26174):map__26174);
var msg = map__26174__$1;
var msg_name = cljs.core.get.call(null,map__26174__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = cljs.core.nthnext.call(null,vec__26173,(1));
if(cljs.core._EQ_.call(null,msg_name,new cljs.core.Keyword(null,"css-files-changed","css-files-changed",720773874))){
return figwheel.client.file_reloading.reload_css_files.call(null,opts,msg);
} else {
return null;
}
});
});
figwheel.client.compile_fail_warning_plugin = (function figwheel$client$compile_fail_warning_plugin(p__26176){
var map__26186 = p__26176;
var map__26186__$1 = ((((!((map__26186 == null)))?((((map__26186.cljs$lang$protocol_mask$partition0$ & (64))) || (map__26186.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__26186):map__26186);
var on_compile_warning = cljs.core.get.call(null,map__26186__$1,new cljs.core.Keyword(null,"on-compile-warning","on-compile-warning",-1195585947));
var on_compile_fail = cljs.core.get.call(null,map__26186__$1,new cljs.core.Keyword(null,"on-compile-fail","on-compile-fail",728013036));
return ((function (map__26186,map__26186__$1,on_compile_warning,on_compile_fail){
return (function (p__26188){
var vec__26189 = p__26188;
var map__26190 = cljs.core.nth.call(null,vec__26189,(0),null);
var map__26190__$1 = ((((!((map__26190 == null)))?((((map__26190.cljs$lang$protocol_mask$partition0$ & (64))) || (map__26190.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__26190):map__26190);
var msg = map__26190__$1;
var msg_name = cljs.core.get.call(null,map__26190__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = cljs.core.nthnext.call(null,vec__26189,(1));
var pred__26192 = cljs.core._EQ_;
var expr__26193 = msg_name;
if(cljs.core.truth_(pred__26192.call(null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),expr__26193))){
return on_compile_warning.call(null,msg);
} else {
if(cljs.core.truth_(pred__26192.call(null,new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),expr__26193))){
return on_compile_fail.call(null,msg);
} else {
return null;
}
}
});
;})(map__26186,map__26186__$1,on_compile_warning,on_compile_fail))
});
figwheel.client.heads_up_plugin_msg_handler = (function figwheel$client$heads_up_plugin_msg_handler(opts,msg_hist_SINGLEQUOTE_){
var msg_hist = figwheel.client.focus_msgs.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),null,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),null], null), null),msg_hist_SINGLEQUOTE_);
var msg_names = cljs.core.map.call(null,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863),msg_hist);
var msg = cljs.core.first.call(null,msg_hist);
var c__21566__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto__,msg_hist,msg_names,msg){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto__,msg_hist,msg_names,msg){
return (function (state_26409){
var state_val_26410 = (state_26409[(1)]);
if((state_val_26410 === (7))){
var inst_26333 = (state_26409[(2)]);
var state_26409__$1 = state_26409;
if(cljs.core.truth_(inst_26333)){
var statearr_26411_26457 = state_26409__$1;
(statearr_26411_26457[(1)] = (8));

} else {
var statearr_26412_26458 = state_26409__$1;
(statearr_26412_26458[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (20))){
var inst_26403 = (state_26409[(2)]);
var state_26409__$1 = state_26409;
var statearr_26413_26459 = state_26409__$1;
(statearr_26413_26459[(2)] = inst_26403);

(statearr_26413_26459[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (27))){
var inst_26399 = (state_26409[(2)]);
var state_26409__$1 = state_26409;
var statearr_26414_26460 = state_26409__$1;
(statearr_26414_26460[(2)] = inst_26399);

(statearr_26414_26460[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (1))){
var inst_26326 = figwheel.client.reload_file_state_QMARK_.call(null,msg_names,opts);
var state_26409__$1 = state_26409;
if(cljs.core.truth_(inst_26326)){
var statearr_26415_26461 = state_26409__$1;
(statearr_26415_26461[(1)] = (2));

} else {
var statearr_26416_26462 = state_26409__$1;
(statearr_26416_26462[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (24))){
var inst_26401 = (state_26409[(2)]);
var state_26409__$1 = state_26409;
var statearr_26417_26463 = state_26409__$1;
(statearr_26417_26463[(2)] = inst_26401);

(statearr_26417_26463[(1)] = (20));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (4))){
var inst_26407 = (state_26409[(2)]);
var state_26409__$1 = state_26409;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_26409__$1,inst_26407);
} else {
if((state_val_26410 === (15))){
var inst_26405 = (state_26409[(2)]);
var state_26409__$1 = state_26409;
var statearr_26418_26464 = state_26409__$1;
(statearr_26418_26464[(2)] = inst_26405);

(statearr_26418_26464[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (21))){
var inst_26364 = (state_26409[(2)]);
var state_26409__$1 = state_26409;
var statearr_26419_26465 = state_26409__$1;
(statearr_26419_26465[(2)] = inst_26364);

(statearr_26419_26465[(1)] = (20));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (31))){
var inst_26388 = figwheel.client.css_loaded_state_QMARK_.call(null,msg_names);
var state_26409__$1 = state_26409;
if(cljs.core.truth_(inst_26388)){
var statearr_26420_26466 = state_26409__$1;
(statearr_26420_26466[(1)] = (34));

} else {
var statearr_26421_26467 = state_26409__$1;
(statearr_26421_26467[(1)] = (35));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (32))){
var inst_26397 = (state_26409[(2)]);
var state_26409__$1 = state_26409;
var statearr_26422_26468 = state_26409__$1;
(statearr_26422_26468[(2)] = inst_26397);

(statearr_26422_26468[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (33))){
var inst_26386 = (state_26409[(2)]);
var state_26409__$1 = state_26409;
var statearr_26423_26469 = state_26409__$1;
(statearr_26423_26469[(2)] = inst_26386);

(statearr_26423_26469[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (13))){
var inst_26347 = figwheel.client.heads_up.clear.call(null);
var state_26409__$1 = state_26409;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26409__$1,(16),inst_26347);
} else {
if((state_val_26410 === (22))){
var inst_26368 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_26369 = figwheel.client.heads_up.append_message.call(null,inst_26368);
var state_26409__$1 = state_26409;
var statearr_26424_26470 = state_26409__$1;
(statearr_26424_26470[(2)] = inst_26369);

(statearr_26424_26470[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (36))){
var inst_26395 = (state_26409[(2)]);
var state_26409__$1 = state_26409;
var statearr_26425_26471 = state_26409__$1;
(statearr_26425_26471[(2)] = inst_26395);

(statearr_26425_26471[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (29))){
var inst_26379 = (state_26409[(2)]);
var state_26409__$1 = state_26409;
var statearr_26426_26472 = state_26409__$1;
(statearr_26426_26472[(2)] = inst_26379);

(statearr_26426_26472[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (6))){
var inst_26328 = (state_26409[(7)]);
var state_26409__$1 = state_26409;
var statearr_26427_26473 = state_26409__$1;
(statearr_26427_26473[(2)] = inst_26328);

(statearr_26427_26473[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (28))){
var inst_26375 = (state_26409[(2)]);
var inst_26376 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_26377 = figwheel.client.heads_up.display_warning.call(null,inst_26376);
var state_26409__$1 = (function (){var statearr_26428 = state_26409;
(statearr_26428[(8)] = inst_26375);

return statearr_26428;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26409__$1,(29),inst_26377);
} else {
if((state_val_26410 === (25))){
var inst_26373 = figwheel.client.heads_up.clear.call(null);
var state_26409__$1 = state_26409;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26409__$1,(28),inst_26373);
} else {
if((state_val_26410 === (34))){
var inst_26390 = figwheel.client.heads_up.flash_loaded.call(null);
var state_26409__$1 = state_26409;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26409__$1,(37),inst_26390);
} else {
if((state_val_26410 === (17))){
var inst_26355 = (state_26409[(2)]);
var state_26409__$1 = state_26409;
var statearr_26429_26474 = state_26409__$1;
(statearr_26429_26474[(2)] = inst_26355);

(statearr_26429_26474[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (3))){
var inst_26345 = figwheel.client.compile_refail_state_QMARK_.call(null,msg_names);
var state_26409__$1 = state_26409;
if(cljs.core.truth_(inst_26345)){
var statearr_26430_26475 = state_26409__$1;
(statearr_26430_26475[(1)] = (13));

} else {
var statearr_26431_26476 = state_26409__$1;
(statearr_26431_26476[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (12))){
var inst_26341 = (state_26409[(2)]);
var state_26409__$1 = state_26409;
var statearr_26432_26477 = state_26409__$1;
(statearr_26432_26477[(2)] = inst_26341);

(statearr_26432_26477[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (2))){
var inst_26328 = (state_26409[(7)]);
var inst_26328__$1 = figwheel.client.autoload_QMARK_.call(null);
var state_26409__$1 = (function (){var statearr_26433 = state_26409;
(statearr_26433[(7)] = inst_26328__$1);

return statearr_26433;
})();
if(cljs.core.truth_(inst_26328__$1)){
var statearr_26434_26478 = state_26409__$1;
(statearr_26434_26478[(1)] = (5));

} else {
var statearr_26435_26479 = state_26409__$1;
(statearr_26435_26479[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (23))){
var inst_26371 = figwheel.client.rewarning_state_QMARK_.call(null,msg_names);
var state_26409__$1 = state_26409;
if(cljs.core.truth_(inst_26371)){
var statearr_26436_26480 = state_26409__$1;
(statearr_26436_26480[(1)] = (25));

} else {
var statearr_26437_26481 = state_26409__$1;
(statearr_26437_26481[(1)] = (26));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (35))){
var state_26409__$1 = state_26409;
var statearr_26438_26482 = state_26409__$1;
(statearr_26438_26482[(2)] = null);

(statearr_26438_26482[(1)] = (36));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (19))){
var inst_26366 = figwheel.client.warning_append_state_QMARK_.call(null,msg_names);
var state_26409__$1 = state_26409;
if(cljs.core.truth_(inst_26366)){
var statearr_26439_26483 = state_26409__$1;
(statearr_26439_26483[(1)] = (22));

} else {
var statearr_26440_26484 = state_26409__$1;
(statearr_26440_26484[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (11))){
var inst_26337 = (state_26409[(2)]);
var state_26409__$1 = state_26409;
var statearr_26441_26485 = state_26409__$1;
(statearr_26441_26485[(2)] = inst_26337);

(statearr_26441_26485[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (9))){
var inst_26339 = figwheel.client.heads_up.clear.call(null);
var state_26409__$1 = state_26409;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26409__$1,(12),inst_26339);
} else {
if((state_val_26410 === (5))){
var inst_26330 = new cljs.core.Keyword(null,"autoload","autoload",-354122500).cljs$core$IFn$_invoke$arity$1(opts);
var state_26409__$1 = state_26409;
var statearr_26442_26486 = state_26409__$1;
(statearr_26442_26486[(2)] = inst_26330);

(statearr_26442_26486[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (14))){
var inst_26357 = figwheel.client.compile_fail_state_QMARK_.call(null,msg_names);
var state_26409__$1 = state_26409;
if(cljs.core.truth_(inst_26357)){
var statearr_26443_26487 = state_26409__$1;
(statearr_26443_26487[(1)] = (18));

} else {
var statearr_26444_26488 = state_26409__$1;
(statearr_26444_26488[(1)] = (19));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (26))){
var inst_26381 = figwheel.client.warning_state_QMARK_.call(null,msg_names);
var state_26409__$1 = state_26409;
if(cljs.core.truth_(inst_26381)){
var statearr_26445_26489 = state_26409__$1;
(statearr_26445_26489[(1)] = (30));

} else {
var statearr_26446_26490 = state_26409__$1;
(statearr_26446_26490[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (16))){
var inst_26349 = (state_26409[(2)]);
var inst_26350 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_26351 = figwheel.client.format_messages.call(null,inst_26350);
var inst_26352 = new cljs.core.Keyword(null,"cause","cause",231901252).cljs$core$IFn$_invoke$arity$1(msg);
var inst_26353 = figwheel.client.heads_up.display_error.call(null,inst_26351,inst_26352);
var state_26409__$1 = (function (){var statearr_26447 = state_26409;
(statearr_26447[(9)] = inst_26349);

return statearr_26447;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26409__$1,(17),inst_26353);
} else {
if((state_val_26410 === (30))){
var inst_26383 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_26384 = figwheel.client.heads_up.display_warning.call(null,inst_26383);
var state_26409__$1 = state_26409;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26409__$1,(33),inst_26384);
} else {
if((state_val_26410 === (10))){
var inst_26343 = (state_26409[(2)]);
var state_26409__$1 = state_26409;
var statearr_26448_26491 = state_26409__$1;
(statearr_26448_26491[(2)] = inst_26343);

(statearr_26448_26491[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (18))){
var inst_26359 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_26360 = figwheel.client.format_messages.call(null,inst_26359);
var inst_26361 = new cljs.core.Keyword(null,"cause","cause",231901252).cljs$core$IFn$_invoke$arity$1(msg);
var inst_26362 = figwheel.client.heads_up.display_error.call(null,inst_26360,inst_26361);
var state_26409__$1 = state_26409;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26409__$1,(21),inst_26362);
} else {
if((state_val_26410 === (37))){
var inst_26392 = (state_26409[(2)]);
var state_26409__$1 = state_26409;
var statearr_26449_26492 = state_26409__$1;
(statearr_26449_26492[(2)] = inst_26392);

(statearr_26449_26492[(1)] = (36));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26410 === (8))){
var inst_26335 = figwheel.client.heads_up.flash_loaded.call(null);
var state_26409__$1 = state_26409;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26409__$1,(11),inst_26335);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__21566__auto__,msg_hist,msg_names,msg))
;
return ((function (switch__21501__auto__,c__21566__auto__,msg_hist,msg_names,msg){
return (function() {
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__21502__auto__ = null;
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__21502__auto____0 = (function (){
var statearr_26453 = [null,null,null,null,null,null,null,null,null,null];
(statearr_26453[(0)] = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__21502__auto__);

(statearr_26453[(1)] = (1));

return statearr_26453;
});
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__21502__auto____1 = (function (state_26409){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_26409);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e26454){if((e26454 instanceof Object)){
var ex__21505__auto__ = e26454;
var statearr_26455_26493 = state_26409;
(statearr_26455_26493[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_26409);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e26454;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__26494 = state_26409;
state_26409 = G__26494;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__21502__auto__ = function(state_26409){
switch(arguments.length){
case 0:
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__21502__auto____0.call(this);
case 1:
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__21502__auto____1.call(this,state_26409);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__21502__auto____0;
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__21502__auto____1;
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto__,msg_hist,msg_names,msg))
})();
var state__21568__auto__ = (function (){var statearr_26456 = f__21567__auto__.call(null);
(statearr_26456[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto__);

return statearr_26456;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto__,msg_hist,msg_names,msg))
);

return c__21566__auto__;
});
figwheel.client.heads_up_plugin = (function figwheel$client$heads_up_plugin(opts){
var ch = cljs.core.async.chan.call(null);
figwheel.client.heads_up_config_options_STAR__STAR_ = opts;

var c__21566__auto___26557 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto___26557,ch){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto___26557,ch){
return (function (state_26540){
var state_val_26541 = (state_26540[(1)]);
if((state_val_26541 === (1))){
var state_26540__$1 = state_26540;
var statearr_26542_26558 = state_26540__$1;
(statearr_26542_26558[(2)] = null);

(statearr_26542_26558[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26541 === (2))){
var state_26540__$1 = state_26540;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26540__$1,(4),ch);
} else {
if((state_val_26541 === (3))){
var inst_26538 = (state_26540[(2)]);
var state_26540__$1 = state_26540;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_26540__$1,inst_26538);
} else {
if((state_val_26541 === (4))){
var inst_26528 = (state_26540[(7)]);
var inst_26528__$1 = (state_26540[(2)]);
var state_26540__$1 = (function (){var statearr_26543 = state_26540;
(statearr_26543[(7)] = inst_26528__$1);

return statearr_26543;
})();
if(cljs.core.truth_(inst_26528__$1)){
var statearr_26544_26559 = state_26540__$1;
(statearr_26544_26559[(1)] = (5));

} else {
var statearr_26545_26560 = state_26540__$1;
(statearr_26545_26560[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26541 === (5))){
var inst_26528 = (state_26540[(7)]);
var inst_26530 = figwheel.client.heads_up_plugin_msg_handler.call(null,opts,inst_26528);
var state_26540__$1 = state_26540;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26540__$1,(8),inst_26530);
} else {
if((state_val_26541 === (6))){
var state_26540__$1 = state_26540;
var statearr_26546_26561 = state_26540__$1;
(statearr_26546_26561[(2)] = null);

(statearr_26546_26561[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26541 === (7))){
var inst_26536 = (state_26540[(2)]);
var state_26540__$1 = state_26540;
var statearr_26547_26562 = state_26540__$1;
(statearr_26547_26562[(2)] = inst_26536);

(statearr_26547_26562[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_26541 === (8))){
var inst_26532 = (state_26540[(2)]);
var state_26540__$1 = (function (){var statearr_26548 = state_26540;
(statearr_26548[(8)] = inst_26532);

return statearr_26548;
})();
var statearr_26549_26563 = state_26540__$1;
(statearr_26549_26563[(2)] = null);

(statearr_26549_26563[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
});})(c__21566__auto___26557,ch))
;
return ((function (switch__21501__auto__,c__21566__auto___26557,ch){
return (function() {
var figwheel$client$heads_up_plugin_$_state_machine__21502__auto__ = null;
var figwheel$client$heads_up_plugin_$_state_machine__21502__auto____0 = (function (){
var statearr_26553 = [null,null,null,null,null,null,null,null,null];
(statearr_26553[(0)] = figwheel$client$heads_up_plugin_$_state_machine__21502__auto__);

(statearr_26553[(1)] = (1));

return statearr_26553;
});
var figwheel$client$heads_up_plugin_$_state_machine__21502__auto____1 = (function (state_26540){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_26540);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e26554){if((e26554 instanceof Object)){
var ex__21505__auto__ = e26554;
var statearr_26555_26564 = state_26540;
(statearr_26555_26564[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_26540);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e26554;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__26565 = state_26540;
state_26540 = G__26565;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
figwheel$client$heads_up_plugin_$_state_machine__21502__auto__ = function(state_26540){
switch(arguments.length){
case 0:
return figwheel$client$heads_up_plugin_$_state_machine__21502__auto____0.call(this);
case 1:
return figwheel$client$heads_up_plugin_$_state_machine__21502__auto____1.call(this,state_26540);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up_plugin_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up_plugin_$_state_machine__21502__auto____0;
figwheel$client$heads_up_plugin_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up_plugin_$_state_machine__21502__auto____1;
return figwheel$client$heads_up_plugin_$_state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto___26557,ch))
})();
var state__21568__auto__ = (function (){var statearr_26556 = f__21567__auto__.call(null);
(statearr_26556[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto___26557);

return statearr_26556;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto___26557,ch))
);


figwheel.client.heads_up.ensure_container.call(null);

return ((function (ch){
return (function (msg_hist){
cljs.core.async.put_BANG_.call(null,ch,msg_hist);

return msg_hist;
});
;})(ch))
});
figwheel.client.enforce_project_plugin = (function figwheel$client$enforce_project_plugin(opts){
return (function (msg_hist){
if(((1) < cljs.core.count.call(null,cljs.core.set.call(null,cljs.core.keep.call(null,new cljs.core.Keyword(null,"project-id","project-id",206449307),cljs.core.take.call(null,(5),msg_hist)))))){
figwheel.client.socket.close_BANG_.call(null);

console.error("Figwheel: message received from different project. Shutting socket down.");

if(cljs.core.truth_(new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(opts))){
var c__21566__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__21566__auto__){
return (function (){
var f__21567__auto__ = (function (){var switch__21501__auto__ = ((function (c__21566__auto__){
return (function (state_26586){
var state_val_26587 = (state_26586[(1)]);
if((state_val_26587 === (1))){
var inst_26581 = cljs.core.async.timeout.call(null,(3000));
var state_26586__$1 = state_26586;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_26586__$1,(2),inst_26581);
} else {
if((state_val_26587 === (2))){
var inst_26583 = (state_26586[(2)]);
var inst_26584 = figwheel.client.heads_up.display_system_warning.call(null,"Connection from different project","Shutting connection down!!!!!");
var state_26586__$1 = (function (){var statearr_26588 = state_26586;
(statearr_26588[(7)] = inst_26583);

return statearr_26588;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_26586__$1,inst_26584);
} else {
return null;
}
}
});})(c__21566__auto__))
;
return ((function (switch__21501__auto__,c__21566__auto__){
return (function() {
var figwheel$client$enforce_project_plugin_$_state_machine__21502__auto__ = null;
var figwheel$client$enforce_project_plugin_$_state_machine__21502__auto____0 = (function (){
var statearr_26592 = [null,null,null,null,null,null,null,null];
(statearr_26592[(0)] = figwheel$client$enforce_project_plugin_$_state_machine__21502__auto__);

(statearr_26592[(1)] = (1));

return statearr_26592;
});
var figwheel$client$enforce_project_plugin_$_state_machine__21502__auto____1 = (function (state_26586){
while(true){
var ret_value__21503__auto__ = (function (){try{while(true){
var result__21504__auto__ = switch__21501__auto__.call(null,state_26586);
if(cljs.core.keyword_identical_QMARK_.call(null,result__21504__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__21504__auto__;
}
break;
}
}catch (e26593){if((e26593 instanceof Object)){
var ex__21505__auto__ = e26593;
var statearr_26594_26596 = state_26586;
(statearr_26594_26596[(5)] = ex__21505__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_26586);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e26593;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__21503__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__26597 = state_26586;
state_26586 = G__26597;
continue;
} else {
return ret_value__21503__auto__;
}
break;
}
});
figwheel$client$enforce_project_plugin_$_state_machine__21502__auto__ = function(state_26586){
switch(arguments.length){
case 0:
return figwheel$client$enforce_project_plugin_$_state_machine__21502__auto____0.call(this);
case 1:
return figwheel$client$enforce_project_plugin_$_state_machine__21502__auto____1.call(this,state_26586);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$enforce_project_plugin_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$enforce_project_plugin_$_state_machine__21502__auto____0;
figwheel$client$enforce_project_plugin_$_state_machine__21502__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$enforce_project_plugin_$_state_machine__21502__auto____1;
return figwheel$client$enforce_project_plugin_$_state_machine__21502__auto__;
})()
;})(switch__21501__auto__,c__21566__auto__))
})();
var state__21568__auto__ = (function (){var statearr_26595 = f__21567__auto__.call(null);
(statearr_26595[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__21566__auto__);

return statearr_26595;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__21568__auto__);
});})(c__21566__auto__))
);

return c__21566__auto__;
} else {
return null;
}
} else {
return null;
}
});
});
figwheel.client.default_on_jsload = cljs.core.identity;
figwheel.client.default_on_compile_fail = (function figwheel$client$default_on_compile_fail(p__26598){
var map__26605 = p__26598;
var map__26605__$1 = ((((!((map__26605 == null)))?((((map__26605.cljs$lang$protocol_mask$partition0$ & (64))) || (map__26605.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__26605):map__26605);
var ed = map__26605__$1;
var formatted_exception = cljs.core.get.call(null,map__26605__$1,new cljs.core.Keyword(null,"formatted-exception","formatted-exception",-116489026));
var exception_data = cljs.core.get.call(null,map__26605__$1,new cljs.core.Keyword(null,"exception-data","exception-data",-512474886));
var cause = cljs.core.get.call(null,map__26605__$1,new cljs.core.Keyword(null,"cause","cause",231901252));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: Compile Exception");

var seq__26607_26611 = cljs.core.seq.call(null,figwheel.client.format_messages.call(null,exception_data));
var chunk__26608_26612 = null;
var count__26609_26613 = (0);
var i__26610_26614 = (0);
while(true){
if((i__26610_26614 < count__26609_26613)){
var msg_26615 = cljs.core._nth.call(null,chunk__26608_26612,i__26610_26614);
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),msg_26615);

var G__26616 = seq__26607_26611;
var G__26617 = chunk__26608_26612;
var G__26618 = count__26609_26613;
var G__26619 = (i__26610_26614 + (1));
seq__26607_26611 = G__26616;
chunk__26608_26612 = G__26617;
count__26609_26613 = G__26618;
i__26610_26614 = G__26619;
continue;
} else {
var temp__4425__auto___26620 = cljs.core.seq.call(null,seq__26607_26611);
if(temp__4425__auto___26620){
var seq__26607_26621__$1 = temp__4425__auto___26620;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26607_26621__$1)){
var c__17600__auto___26622 = cljs.core.chunk_first.call(null,seq__26607_26621__$1);
var G__26623 = cljs.core.chunk_rest.call(null,seq__26607_26621__$1);
var G__26624 = c__17600__auto___26622;
var G__26625 = cljs.core.count.call(null,c__17600__auto___26622);
var G__26626 = (0);
seq__26607_26611 = G__26623;
chunk__26608_26612 = G__26624;
count__26609_26613 = G__26625;
i__26610_26614 = G__26626;
continue;
} else {
var msg_26627 = cljs.core.first.call(null,seq__26607_26621__$1);
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),msg_26627);

var G__26628 = cljs.core.next.call(null,seq__26607_26621__$1);
var G__26629 = null;
var G__26630 = (0);
var G__26631 = (0);
seq__26607_26611 = G__26628;
chunk__26608_26612 = G__26629;
count__26609_26613 = G__26630;
i__26610_26614 = G__26631;
continue;
}
} else {
}
}
break;
}

if(cljs.core.truth_(cause)){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),[cljs.core.str("Error on file "),cljs.core.str(new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(cause)),cljs.core.str(", line "),cljs.core.str(new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(cause)),cljs.core.str(", column "),cljs.core.str(new cljs.core.Keyword(null,"column","column",2078222095).cljs$core$IFn$_invoke$arity$1(cause))].join(''));
} else {
}

return ed;
});
figwheel.client.default_on_compile_warning = (function figwheel$client$default_on_compile_warning(p__26632){
var map__26635 = p__26632;
var map__26635__$1 = ((((!((map__26635 == null)))?((((map__26635.cljs$lang$protocol_mask$partition0$ & (64))) || (map__26635.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__26635):map__26635);
var w = map__26635__$1;
var message = cljs.core.get.call(null,map__26635__$1,new cljs.core.Keyword(null,"message","message",-406056002));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),[cljs.core.str("Figwheel: Compile Warning - "),cljs.core.str(message)].join(''));

return w;
});
figwheel.client.default_before_load = (function figwheel$client$default_before_load(files){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: notified of file changes");

return files;
});
figwheel.client.default_on_cssload = (function figwheel$client$default_on_cssload(files){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded CSS files");

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),cljs.core.pr_str.call(null,cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),files)));

return files;
});
if(typeof figwheel.client.config_defaults !== 'undefined'){
} else {
figwheel.client.config_defaults = cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"on-compile-warning","on-compile-warning",-1195585947),new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),new cljs.core.Keyword(null,"reload-dependents","reload-dependents",-956865430),new cljs.core.Keyword(null,"on-compile-fail","on-compile-fail",728013036),new cljs.core.Keyword(null,"debug","debug",-1608172596),new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202),new cljs.core.Keyword(null,"websocket-url","websocket-url",-490444938),new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128),new cljs.core.Keyword(null,"load-warninged-code","load-warninged-code",-2030345223),new cljs.core.Keyword(null,"eval-fn","eval-fn",-1111644294),new cljs.core.Keyword(null,"retry-count","retry-count",1936122875),new cljs.core.Keyword(null,"autoload","autoload",-354122500),new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318)],[figwheel.client.default_on_compile_warning,figwheel.client.default_on_jsload,true,figwheel.client.default_on_compile_fail,false,true,[cljs.core.str("ws://"),cljs.core.str((cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))?location.host:"localhost:3449")),cljs.core.str("/figwheel-ws")].join(''),figwheel.client.default_before_load,false,false,(100),true,figwheel.client.default_on_cssload]);
}
figwheel.client.handle_deprecated_jsload_callback = (function figwheel$client$handle_deprecated_jsload_callback(config){
if(cljs.core.truth_(new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369).cljs$core$IFn$_invoke$arity$1(config))){
return cljs.core.dissoc.call(null,cljs.core.assoc.call(null,config,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369).cljs$core$IFn$_invoke$arity$1(config)),new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369));
} else {
return config;
}
});
figwheel.client.base_plugins = (function figwheel$client$base_plugins(system_options){
var base = new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"enforce-project-plugin","enforce-project-plugin",959402899),figwheel.client.enforce_project_plugin,new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733),figwheel.client.file_reloader_plugin,new cljs.core.Keyword(null,"comp-fail-warning-plugin","comp-fail-warning-plugin",634311),figwheel.client.compile_fail_warning_plugin,new cljs.core.Keyword(null,"css-reloader-plugin","css-reloader-plugin",2002032904),figwheel.client.css_reloader_plugin,new cljs.core.Keyword(null,"repl-plugin","repl-plugin",-1138952371),figwheel.client.repl_plugin], null);
var base__$1 = ((cljs.core.not.call(null,figwheel.client.utils.html_env_QMARK_.call(null)))?cljs.core.select_keys.call(null,base,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733),new cljs.core.Keyword(null,"comp-fail-warning-plugin","comp-fail-warning-plugin",634311),new cljs.core.Keyword(null,"repl-plugin","repl-plugin",-1138952371)], null)):base);
var base__$2 = ((new cljs.core.Keyword(null,"autoload","autoload",-354122500).cljs$core$IFn$_invoke$arity$1(system_options) === false)?cljs.core.dissoc.call(null,base__$1,new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733)):base__$1);
if(cljs.core.truth_((function (){var and__16784__auto__ = new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(system_options);
if(cljs.core.truth_(and__16784__auto__)){
return figwheel.client.utils.html_env_QMARK_.call(null);
} else {
return and__16784__auto__;
}
})())){
return cljs.core.assoc.call(null,base__$2,new cljs.core.Keyword(null,"heads-up-display-plugin","heads-up-display-plugin",1745207501),figwheel.client.heads_up_plugin);
} else {
return base__$2;
}
});
figwheel.client.add_message_watch = (function figwheel$client$add_message_watch(key,callback){
return cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,key,(function (_,___$1,___$2,msg_hist){
return callback.call(null,cljs.core.first.call(null,msg_hist));
}));
});
figwheel.client.add_plugins = (function figwheel$client$add_plugins(plugins,system_options){
var seq__26643 = cljs.core.seq.call(null,plugins);
var chunk__26644 = null;
var count__26645 = (0);
var i__26646 = (0);
while(true){
if((i__26646 < count__26645)){
var vec__26647 = cljs.core._nth.call(null,chunk__26644,i__26646);
var k = cljs.core.nth.call(null,vec__26647,(0),null);
var plugin = cljs.core.nth.call(null,vec__26647,(1),null);
if(cljs.core.truth_(plugin)){
var pl_26649 = plugin.call(null,system_options);
cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,k,((function (seq__26643,chunk__26644,count__26645,i__26646,pl_26649,vec__26647,k,plugin){
return (function (_,___$1,___$2,msg_hist){
return pl_26649.call(null,msg_hist);
});})(seq__26643,chunk__26644,count__26645,i__26646,pl_26649,vec__26647,k,plugin))
);
} else {
}

var G__26650 = seq__26643;
var G__26651 = chunk__26644;
var G__26652 = count__26645;
var G__26653 = (i__26646 + (1));
seq__26643 = G__26650;
chunk__26644 = G__26651;
count__26645 = G__26652;
i__26646 = G__26653;
continue;
} else {
var temp__4425__auto__ = cljs.core.seq.call(null,seq__26643);
if(temp__4425__auto__){
var seq__26643__$1 = temp__4425__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26643__$1)){
var c__17600__auto__ = cljs.core.chunk_first.call(null,seq__26643__$1);
var G__26654 = cljs.core.chunk_rest.call(null,seq__26643__$1);
var G__26655 = c__17600__auto__;
var G__26656 = cljs.core.count.call(null,c__17600__auto__);
var G__26657 = (0);
seq__26643 = G__26654;
chunk__26644 = G__26655;
count__26645 = G__26656;
i__26646 = G__26657;
continue;
} else {
var vec__26648 = cljs.core.first.call(null,seq__26643__$1);
var k = cljs.core.nth.call(null,vec__26648,(0),null);
var plugin = cljs.core.nth.call(null,vec__26648,(1),null);
if(cljs.core.truth_(plugin)){
var pl_26658 = plugin.call(null,system_options);
cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,k,((function (seq__26643,chunk__26644,count__26645,i__26646,pl_26658,vec__26648,k,plugin,seq__26643__$1,temp__4425__auto__){
return (function (_,___$1,___$2,msg_hist){
return pl_26658.call(null,msg_hist);
});})(seq__26643,chunk__26644,count__26645,i__26646,pl_26658,vec__26648,k,plugin,seq__26643__$1,temp__4425__auto__))
);
} else {
}

var G__26659 = cljs.core.next.call(null,seq__26643__$1);
var G__26660 = null;
var G__26661 = (0);
var G__26662 = (0);
seq__26643 = G__26659;
chunk__26644 = G__26660;
count__26645 = G__26661;
i__26646 = G__26662;
continue;
}
} else {
return null;
}
}
break;
}
});
figwheel.client.start = (function figwheel$client$start(var_args){
var args26663 = [];
var len__17855__auto___26666 = arguments.length;
var i__17856__auto___26667 = (0);
while(true){
if((i__17856__auto___26667 < len__17855__auto___26666)){
args26663.push((arguments[i__17856__auto___26667]));

var G__26668 = (i__17856__auto___26667 + (1));
i__17856__auto___26667 = G__26668;
continue;
} else {
}
break;
}

var G__26665 = args26663.length;
switch (G__26665) {
case 1:
return figwheel.client.start.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 0:
return figwheel.client.start.cljs$core$IFn$_invoke$arity$0();

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args26663.length)].join('')));

}
});

figwheel.client.start.cljs$core$IFn$_invoke$arity$1 = (function (opts){
if((goog.dependencies_ == null)){
return null;
} else {
if(typeof figwheel.client.__figwheel_start_once__ !== 'undefined'){
return null;
} else {
figwheel.client.__figwheel_start_once__ = setTimeout((function (){
var plugins_SINGLEQUOTE_ = new cljs.core.Keyword(null,"plugins","plugins",1900073717).cljs$core$IFn$_invoke$arity$1(opts);
var merge_plugins = new cljs.core.Keyword(null,"merge-plugins","merge-plugins",-1193912370).cljs$core$IFn$_invoke$arity$1(opts);
var system_options = figwheel.client.handle_deprecated_jsload_callback.call(null,cljs.core.merge.call(null,figwheel.client.config_defaults,cljs.core.dissoc.call(null,opts,new cljs.core.Keyword(null,"plugins","plugins",1900073717),new cljs.core.Keyword(null,"merge-plugins","merge-plugins",-1193912370))));
var plugins = (cljs.core.truth_(plugins_SINGLEQUOTE_)?plugins_SINGLEQUOTE_:cljs.core.merge.call(null,figwheel.client.base_plugins.call(null,system_options),merge_plugins));
figwheel.client.utils._STAR_print_debug_STAR_ = new cljs.core.Keyword(null,"debug","debug",-1608172596).cljs$core$IFn$_invoke$arity$1(opts);

figwheel.client.add_plugins.call(null,plugins,system_options);

figwheel.client.file_reloading.patch_goog_base.call(null);

return figwheel.client.socket.open.call(null,system_options);
}));
}
}
});

figwheel.client.start.cljs$core$IFn$_invoke$arity$0 = (function (){
return figwheel.client.start.call(null,cljs.core.PersistentArrayMap.EMPTY);
});

figwheel.client.start.cljs$lang$maxFixedArity = 1;
figwheel.client.watch_and_reload_with_opts = figwheel.client.start;
figwheel.client.watch_and_reload = (function figwheel$client$watch_and_reload(var_args){
var args__17862__auto__ = [];
var len__17855__auto___26674 = arguments.length;
var i__17856__auto___26675 = (0);
while(true){
if((i__17856__auto___26675 < len__17855__auto___26674)){
args__17862__auto__.push((arguments[i__17856__auto___26675]));

var G__26676 = (i__17856__auto___26675 + (1));
i__17856__auto___26675 = G__26676;
continue;
} else {
}
break;
}

var argseq__17863__auto__ = ((((0) < args__17862__auto__.length))?(new cljs.core.IndexedSeq(args__17862__auto__.slice((0)),(0))):null);
return figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic(argseq__17863__auto__);
});

figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic = (function (p__26671){
var map__26672 = p__26671;
var map__26672__$1 = ((((!((map__26672 == null)))?((((map__26672.cljs$lang$protocol_mask$partition0$ & (64))) || (map__26672.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__26672):map__26672);
var opts = map__26672__$1;
return figwheel.client.start.call(null,opts);
});

figwheel.client.watch_and_reload.cljs$lang$maxFixedArity = (0);

figwheel.client.watch_and_reload.cljs$lang$applyTo = (function (seq26670){
return figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq26670));
});

//# sourceMappingURL=client.js.map?rel=1448506748233