// Compiled by ClojureScript 1.7.170 {}
goog.provide('typography.cljs');
goog.require('cljs.core');
goog.require('ribbontrails.attractor');
typography.cljs.CreateAttractors = (function typography$cljs$CreateAttractors(text){
var geo = (new THREE.TextGeometry(text,cljs.core.clj__GT_js.call(null,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"size","size",1098693007),(70),new cljs.core.Keyword(null,"height","height",1025178622),(20),new cljs.core.Keyword(null,"curveSegments","curveSegments",-1293134970),(4),new cljs.core.Keyword(null,"bevelThickness","bevelThickness",1360311084),(2),new cljs.core.Keyword(null,"font","font",-1506159249),"droid sans"], null))));
return null;
});

//# sourceMappingURL=cljs.js.map?rel=1448639490541