(ns typography.cljs
  (:require
    [ribbontrails.attractor :as attractor]))


(defn CreateAttractors [text]
  (let [geo (js/THREE.TextGeometry. text
    (clj->js {:size 70
              :height 20
              :curveSegments 4
              :bevelThickness 2
              :font "droid sans"}))]
              ))
