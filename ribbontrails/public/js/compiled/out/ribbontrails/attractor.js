// Compiled by ClojureScript 1.7.170 {}
goog.provide('ribbontrails.attractor');
goog.require('cljs.core');
goog.require('ribbontrails.math');
ribbontrails.attractor.limit = (function ribbontrails$attractor$limit(vec,max){
var magSq = vec.lengthSq();
if((magSq > (max * max))){
var r = (0);
vec.normalize();

vec.multiplyScalar();

return vec;
} else {
return null;
}
});

/**
* @constructor
 * @implements {ribbontrails.attractor.Object}
*/
ribbontrails.attractor.Attractor = (function (position,mass){
this.position = position;
this.mass = mass;
})
ribbontrails.attractor.Attractor.prototype.init = (function (){
var self__ = this;
var this$ = this;
(this$["G"] = 1.0);

(this$["constant"] = 1.0);

return this$;
});

ribbontrails.attractor.Attractor.prototype.attract = (function (obj){
var self__ = this;
var this$ = this;

var attractForce = this$.position.sub(obj.position);
var forceLength = attractForce.length();
var d = ribbontrails.math.constrain.call(null,forceLength,5.0,25.0);
attractForce.normalize();

var strength = (((this$.G * this$.mass) * obj.mass) / (d * d));
return attractForce;
});

ribbontrails.attractor.Attractor.prototype.attractPosition = (function (position__$1){
var self__ = this;
var this$ = this;
var attractForce = this$.position.sub(position__$1);
var forceLength = attractForce.length();
var d = ribbontrails.math.constrain.call(null,forceLength,5.0,25.0);
attractForce.normalize();

var strength = (((this$.G * this$.mass) * 1.0) / (d * d));
return attractForce.multiplyScalar(strength);
});

ribbontrails.attractor.Attractor.prototype.seek = (function (position__$1,velocity){
var self__ = this;
var this$ = this;
var desired = this$.position.sub(position__$1);
desired.normalize();

desired.multiplyScalar(2.0);

var steer = desired.sub(velocity);
ribbontrails.attractor.limit.call(null,steer,0.03);

return steer;
});

ribbontrails.attractor.Attractor.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"position","position",-371200385,null),new cljs.core.Symbol(null,"mass","mass",-498418519,null)], null);
});

ribbontrails.attractor.Attractor.cljs$lang$type = true;

ribbontrails.attractor.Attractor.cljs$lang$ctorStr = "ribbontrails.attractor/Attractor";

ribbontrails.attractor.Attractor.cljs$lang$ctorPrWriter = (function (this__17394__auto__,writer__17395__auto__,opt__17396__auto__){
return cljs.core._write.call(null,writer__17395__auto__,"ribbontrails.attractor/Attractor");
});

ribbontrails.attractor.__GT_Attractor = (function ribbontrails$attractor$__GT_Attractor(position,mass){
return (new ribbontrails.attractor.Attractor(position,mass));
});

ribbontrails.attractor.makeAttractor = (function ribbontrails$attractor$makeAttractor(var_args){
var args26845 = [];
var len__17854__auto___26848 = arguments.length;
var i__17855__auto___26849 = (0);
while(true){
if((i__17855__auto___26849 < len__17854__auto___26848)){
args26845.push((arguments[i__17855__auto___26849]));

var G__26850 = (i__17855__auto___26849 + (1));
i__17855__auto___26849 = G__26850;
continue;
} else {
}
break;
}

var G__26847 = args26845.length;
switch (G__26847) {
case 0:
return ribbontrails.attractor.makeAttractor.cljs$core$IFn$_invoke$arity$0();

break;
case 2:
return ribbontrails.attractor.makeAttractor.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args26845.length)].join('')));

}
});

ribbontrails.attractor.makeAttractor.cljs$core$IFn$_invoke$arity$0 = (function (){

var attractor = (new ribbontrails.attractor.Attractor((new THREE.Vector3()),1.0));
return attractor.init();
});

ribbontrails.attractor.makeAttractor.cljs$core$IFn$_invoke$arity$2 = (function (position,mass){

var attractor = (new ribbontrails.attractor.Attractor(position,mass));
return attractor.init();
});

ribbontrails.attractor.makeAttractor.cljs$lang$maxFixedArity = 2;

//# sourceMappingURL=attractor.js.map?rel=1448678384643