(ns ribbontrails.attractor
  (:require
   [ribbontrails.math :as math]))

(defn limit [vec max]
  (let [magSq (.lengthSq vec)]
    (if (> magSq (* max max))
      (let [r 0]
        (.normalize vec)
        (.multiplyScalar vec)
        vec))
      ))

(deftype Attractor [position mass]
  Object
  (init [this]
        (aset this "G" 1.0) ;; Gravitational constant
        (aset this "constant" 1.0) ;; same a G, but using word to better describe
        this)

  (attract [this obj]
    "Calculates the attractive force between the attractor and the attracte"
    (let [attractForce (.sub (.-position this) (.-position obj))]
      (let [forceLength (.length attractForce)]
        (let [d (math/constrain forceLength 5.0 25.0)]
          (.normalize attractForce)
            (let [strength (/ (* (.-G this) (.-mass this) (.-mass obj))
                                       (* d d))]
            attractForce)
    ))))
  (attractPosition [this position]
    (let [attractForce (.sub (.-position this) position)]
      (let [forceLength (.length attractForce)]
        (let [d (math/constrain forceLength 5.0 25.0)]
          (.normalize attractForce)
          (let [strength (/ (* (.-G this) (.-mass this) 1.0)
                                     (* d d))]
              (.multiplyScalar attractForce strength))))))

   (seek [this position velocity]
     (let [desired (.sub (.-position this) position)]
      (.normalize desired)
      (.multiplyScalar desired 2.0)
      (let [steer (.sub desired velocity)]
        (limit steer 0.03)
        steer)
      ))

  )


  (defn makeAttractor
    ([]
     "Creates a attractor with a pre-defined position and mass"
     (let [attractor (Attractor. (js/THREE.Vector3.) 1.0)]
       (.init attractor)))

   ([position mass]
      "Creates a attractor with a pre-defined position and mass"
     (let [attractor (Attractor. position mass)]
       (.init attractor)))
    )
