// Compiled by ClojureScript 1.7.170 {}
goog.provide('ribbontrails.typography');
goog.require('cljs.core');
goog.require('thi.ng.typedarrays.core');
goog.require('ribbontrails.attractor');
ribbontrails.typography.CreateAttractors = (function ribbontrails$typography$CreateAttractors(text){
var geo = (new THREE.TextGeometry(text,cljs.core.clj__GT_js.call(null,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"size","size",1098693007),(70),new cljs.core.Keyword(null,"height","height",1025178622),(20),new cljs.core.Keyword(null,"curveSegments","curveSegments",-1293134970),(4),new cljs.core.Keyword(null,"bevelThickness","bevelThickness",1360311084),(2),new cljs.core.Keyword(null,"font","font",-1506159249),"droid sans"], null))));
var buffGeo = (new THREE.BufferGeometry());
var vertLen = geo.vertices.length;
var verts = geo.vertices;
var textVerts = [];
var colors = thi.ng.typedarrays.core.float32.call(null,((3) * vertLen));
var n__17699__auto___26854 = geo.vertices.length;
var i_26855 = (0);
while(true){
if((i_26855 < n__17699__auto___26854)){
var vert_26856 = (geo.vertices[i_26855]);
textVerts.push(vert_26856.x,vert_26856.y,vert_26856.z);

var G__26857 = (i_26855 + (1));
i_26855 = G__26857;
continue;
} else {
}
break;
}

var vertices_26858 = thi.ng.typedarrays.core.float32.call(null,textVerts);
var positionAttrib_26859 = (new THREE.BufferAttribute(vertices_26858,(3)));
buffGeo.addAttribute("position",positionAttrib_26859);

var mat = (new THREE.MeshBasicMaterial(cljs.core.clj__GT_js.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"color","color",1011675173),(16711680)], null))));
var pt = (new THREE.Points(buffGeo,mat));
var attractors = [];
var n__17699__auto___26860 = vertLen;
var i_26861 = (0);
while(true){
if((i_26861 < n__17699__auto___26860)){
var attractor_26862 = ribbontrails.attractor.makeAttractor.call(null,(verts[i_26861]),1.0);
attractors.push(attractor_26862);

var G__26863 = (i_26861 + (1));
i_26861 = G__26863;
continue;
} else {
}
break;
}

return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"mesh","mesh",456320595),pt,new cljs.core.Keyword(null,"attractors","attractors",464325791),attractors], null);
});

//# sourceMappingURL=typography.js.map?rel=1448678384661