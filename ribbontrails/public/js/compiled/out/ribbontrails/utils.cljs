(ns ribbontrails.utils)


(defn getAspectRatio []
  (/ js/window.innerWidth js/window.innerHeight))
