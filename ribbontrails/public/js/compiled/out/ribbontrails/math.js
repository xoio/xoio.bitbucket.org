// Compiled by ClojureScript 1.7.170 {}
goog.provide('ribbontrails.math');
goog.require('cljs.core');
goog.require('thi.ng.typedarrays.core');
goog.require('thi.ng.math.core');
goog.require('thi.ng.geom.core');
goog.require('thi.ng.geom.core.vector');
goog.require('thi.ng.math.simplexnoise');
ribbontrails.math.getVecX = (function ribbontrails$math$getVecX(v){

return (v.buf[(0)]);
});
ribbontrails.math.getVecY = (function ribbontrails$math$getVecY(v){

return (v.buf[(1)]);
});
ribbontrails.math.getVecZ = (function ribbontrails$math$getVecZ(v){

return (v.buf[(2)]);
});
ribbontrails.math.randFloat = (function ribbontrails$math$randFloat(max){
return thi.ng.math.simplexnoise.noise1.call(null,max);
});
ribbontrails.math.divideScalar = (function ribbontrails$math$divideScalar(v,scalar){

(v.buf[(0)] = (scalar / ribbontrails.math.getVecX.call(null,v)));

(v.buf[(1)] = (scalar / ribbontrails.math.getVecX.call(null,v)));

(v.buf[(2)] = (scalar / ribbontrails.math.getVecX.call(null,v)));

return v;
});
ribbontrails.math.multiplyScalar = (function ribbontrails$math$multiplyScalar(v,scalar){

(v.buf[(0)] = (scalar * ribbontrails.math.getVecX.call(null,v)));

(v.buf[(1)] = (scalar * ribbontrails.math.getVecX.call(null,v)));

(v.buf[(2)] = (scalar * ribbontrails.math.getVecX.call(null,v)));

return v;
});
ribbontrails.math.addScalar = (function ribbontrails$math$addScalar(v,scalar){

(v.buf[(0)] = (scalar + ribbontrails.math.getVecX.call(null,v)));

(v.buf[(1)] = (scalar + ribbontrails.math.getVecX.call(null,v)));

(v.buf[(2)] = (scalar + ribbontrails.math.getVecX.call(null,v)));

return v;
});
ribbontrails.math.subScalar = (function ribbontrails$math$subScalar(v,scalar){

(v.buf[(0)] = (scalar - ribbontrails.math.getVecX.call(null,v)));

(v.buf[(1)] = (scalar - ribbontrails.math.getVecX.call(null,v)));

(v.buf[(2)] = (scalar - ribbontrails.math.getVecX.call(null,v)));

return v;
});
ribbontrails.math.lineLength = (function ribbontrails$math$lineLength(x,y,x0,y0){
var x__$1 = ((x - x0) * x);
var y__$1 = ((y - y0) * y);
return Math.sqrt((x__$1 + y__$1));
});
ribbontrails.math.randomRange = (function ribbontrails$math$randomRange(min,max){
var v1 = (Math.random() * (max - min));
return (min + v1);
});
ribbontrails.math.randomInt = (function ribbontrails$math$randomInt(min,max){
var s1 = (min + Math.random());
var s2 = ((max - min) + (1));
return Math.floor((s1 * s2));
});
ribbontrails.math.mapValue = (function ribbontrails$math$mapValue(value,min1,max1,min2,max2){
return cljs.core.List.EMPTY;
});
ribbontrails.math.normalize = (function ribbontrails$math$normalize(value,min,max){
var toDivide = (value - min);
var divisor = (max - min);
return (toDivide / divisor);
});
ribbontrails.math.lerp = (function ribbontrails$math$lerp(value,min,max){
var diff = (max - min);
return (min + (diff * value));
});
ribbontrails.math.randVector3 = (function ribbontrails$math$randVector3(range){
if(cljs.core.not_EQ_.call(null,null,window.THREE)){
return (new THREE.Vector3(ribbontrails.math.randomRange.call(null,((-1) * range),range),ribbontrails.math.randomRange.call(null,((-1) * range),range),ribbontrails.math.randomRange.call(null,((-1) * range),range)));
} else {
return thi.ng.geom.core.vector.vec3.call(null,ribbontrails.math.randomRange.call(null,((-1) * range),range),ribbontrails.math.randomRange.call(null,((-1) * range),range),ribbontrails.math.randomRange.call(null,((-1) * range),range));
}
});
ribbontrails.math.vector3 = (function ribbontrails$math$vector3(){
if(cljs.core.not_EQ_.call(null,null,window.THREE)){
return (new THREE.Vector3());
} else {
return thi.ng.geom.core.vector.vec3.call(null,(0),(0),(0));
}
});
ribbontrails.math.constrain = (function ribbontrails$math$constrain(amt,low,hight){

if((amt < low)){
return low;
} else {
if((amt > hight)){
return hight;
} else {
return amt;
}
}
});
ribbontrails.math.flattenArray = (function ribbontrails$math$flattenArray(vecarray){

var finalArray = [];
var n__17699__auto___21307 = vecarray.length;
var i_21308 = (0);
while(true){
if((i_21308 < n__17699__auto___21307)){
var vec_21309 = cljs.core.nth.call(null,vecarray,i_21308);
if(cljs.core._EQ_.call(null,cljs.core.type.call(null,vec_21309),THREE.Vector3)){
var reserve_21310 = (0);
finalArray.push(vec_21309.x);

finalArray.push(vec_21309.y);

finalArray.push(vec_21309.z);
} else {
finalArray.push(vec_21309);
}

var G__21311 = (i_21308 + (1));
i_21308 = G__21311;
continue;
} else {
}
break;
}

return finalArray;
});
ribbontrails.math.float32 = (function ribbontrails$math$float32(amountOrArray){

if(cljs.core._EQ_.call(null,cljs.core.type.call(null,amountOrArray),Array)){
var flatArray = ribbontrails.math.flattenArray.call(null,amountOrArray);
return thi.ng.typedarrays.core.float32.call(null,flatArray);
} else {
return thi.ng.typedarrays.core.float32.call(null,((3) * amountOrArray));
}
});
ribbontrails.math.toThingVector = (function ribbontrails$math$toThingVector(threeVector){

return thi.ng.geom.core.vector.vec3.call(null,threeVector.x,threeVector.y,threeVector.z);
});
ribbontrails.math.assignRandToArray = (function ribbontrails$math$assignRandToArray(arr,value){

var i_21312 = (0);
while(true){
if((i_21312 < arr.length)){
var x_21313 = i_21312;
var y_21314 = ((1) + i_21312);
var z_21315 = ((2) + i_21312);
(arr[x_21313] = (Math.random * value));

(arr[y_21314] = (Math.random * value));

(arr[z_21315] = (Math.random * value));

var G__21316 = ((3) + i_21312);
i_21312 = G__21316;
continue;
} else {
cljs.core.List.EMPTY;
}
break;
}

return arr;
});
ribbontrails.math.assignToArray = (function ribbontrails$math$assignToArray(arr,cb){

var i_21317 = (0);
while(true){
if((i_21317 < arr.length)){
var x_21318 = i_21317;
var y_21319 = ((1) + i_21317);
var z_21320 = ((2) + i_21317);
cb.call(null,arr,x_21318,y_21319,z_21320);

var G__21321 = ((3) + i_21317);
i_21317 = G__21321;
continue;
} else {
cljs.core.List.EMPTY;
}
break;
}

return arr;
});

//# sourceMappingURL=math.js.map?rel=1448665659188