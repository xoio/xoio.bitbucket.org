// Compiled by ClojureScript 1.7.170 {}
goog.provide('ribbontrails.core');
goog.require('cljs.core');
goog.require('ribbontrails.global');
goog.require('thi.ng.geom.webgl.animator');
goog.require('ribbontrails.scene');
cljs.core.enable_console_print_BANG_.call(null);
ribbontrails.core.composer = ribbontrails.scene.composer;
ribbontrails.core.sceneObj = ribbontrails.scene.scene;
ribbontrails.core.camera = ribbontrails.scene.camera;
ribbontrails.core.ribbon = (ribbontrails.global.ribbons[(0)]);
if(typeof ribbontrails.core.rotateVal !== 'undefined'){
} else {
ribbontrails.core.rotateVal = 0.001;
}
thi.ng.geom.webgl.animator.animate.call(null,(function (p__30566){
var vec__30567 = p__30566;
var t = cljs.core.nth.call(null,vec__30567,(0),null);
var frame = cljs.core.nth.call(null,vec__30567,(1),null);
ribbontrails.global.noiseTime = (ribbontrails.global.noiseTime + ribbontrails.global.noiseSpeed);

ribbontrails.global.stats.update();

var n__17699__auto___30568 = ribbontrails.global.RIBBON_COUNT;
var i_30569 = (0);
while(true){
if((i_30569 < n__17699__auto___30568)){
var ribbon_30570 = (ribbontrails.global.ribbons[i_30569]);
var material_30571 = ribbon_30570.material;
ribbon_30570.update();

material_30571.uniforms.time.value = t;

var G__30572 = (i_30569 + (1));
i_30569 = G__30572;
continue;
} else {
}
break;
}

ribbontrails.core.composer.reset();

ribbontrails.core.composer.render(ribbontrails.core.sceneObj,ribbontrails.core.camera);

ribbontrails.core.composer.pass(ribbontrails.scene.ssao);

ribbontrails.core.composer.toScreen();

ribbontrails.core.rotateVal = (ribbontrails.core.rotateVal + 0.0134059303);

((ribbontrails.scene.worldHolder["rotation"])["x"] = (0.5 * ribbontrails.core.rotateVal));

((ribbontrails.scene.worldHolder["rotation"])["y"] = (0.5 * ribbontrails.core.rotateVal));

return true;
}));
ribbontrails.core.on_js_reload = (function ribbontrails$core$on_js_reload(){
return null;
});

//# sourceMappingURL=core.js.map?rel=1448681516720