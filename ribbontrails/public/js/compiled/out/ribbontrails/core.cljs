(ns ribbontrails.core
  (:require
    [ribbontrails.global :as global]
    [thi.ng.geom.webgl.animator :as anim]
    [ribbontrails.scene :as scene]))

(enable-console-print!)

  (def composer scene/composer)
  (def sceneObj scene/scene)
  (def camera scene/camera)


  (def ribbon (aget global/ribbons 0))

   (defonce rotateVal 0.001)
   (anim/animate
      (fn [[t frame]]
          ;; increment noiseTime
          (set! global/noiseTime (+ global/noiseTime global/noiseSpeed) )
          (.update global/stats)

          (dotimes [i global/RIBBON_COUNT]
            (let [ribbon (aget global/ribbons i)
                  material (.-material ribbon)]
            (.update ribbon)
            (set! (.-uniforms.time.value material) t)
            ))

          (.reset composer)
          (.render composer sceneObj camera)
          (.pass composer scene/ssao)
          ;;(.pass composer scene/blur)
          (.toScreen composer)
          ;;(.render scene/renderer scene/scene scene/camera)
          (set! rotateVal (+ rotateVal 0.0134059303))

          (aset (aget scene/worldHolder "rotation") "x" (* 0.5 rotateVal))
          (aset (aget scene/worldHolder "rotation") "y" (* 0.5 rotateVal))
          true))

(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
