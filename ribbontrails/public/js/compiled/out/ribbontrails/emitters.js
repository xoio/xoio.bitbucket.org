// Compiled by ClojureScript 1.7.170 {}
goog.provide('ribbontrails.emitters');
goog.require('cljs.core');
goog.require('ribbontrails.math');
goog.require('ribbontrails.global');
ribbontrails.emitters.makeEmitters = (function ribbontrails$emitters$makeEmitters(){
var n__17699__auto__ = ribbontrails.global.EMITTER_COUNT;
var i = (0);
while(true){
if((i < n__17699__auto__)){
ribbontrails.global.emitters.push(ribbontrails.math.randVector3.call(null,(ribbontrails.global.BOUNDS / (2))));

var G__21393 = (i + (1));
i = G__21393;
continue;
} else {
return null;
}
break;
}
});

//# sourceMappingURL=emitters.js.map?rel=1448665659281