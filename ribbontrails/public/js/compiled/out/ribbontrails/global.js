// Compiled by ClojureScript 1.7.170 {}
goog.provide('ribbontrails.global');
goog.require('cljs.core');
if(typeof ribbontrails.global.up !== 'undefined'){
} else {
ribbontrails.global.up = (new THREE.Vector3((0),(1),(0)));
}
if(typeof ribbontrails.global.mainVec !== 'undefined'){
} else {
ribbontrails.global.mainVec = (new THREE.Vector3());
}
if(typeof ribbontrails.global.tangent !== 'undefined'){
} else {
ribbontrails.global.tangent = (new THREE.Vector3());
}
if(typeof ribbontrails.global.normal !== 'undefined'){
} else {
ribbontrails.global.normal = (new THREE.Vector3());
}
if(typeof ribbontrails.global.startRange !== 'undefined'){
} else {
ribbontrails.global.startRange = (100);
}
if(typeof ribbontrails.global.col !== 'undefined'){
} else {
ribbontrails.global.col = (new THREE.Vector3());
}
if(typeof ribbontrails.global.RIBBON_COUNT !== 'undefined'){
} else {
ribbontrails.global.RIBBON_COUNT = (600);
}
if(typeof ribbontrails.global.ribbons !== 'undefined'){
} else {
ribbontrails.global.ribbons = [];
}
if(typeof ribbontrails.global.EMITTER_COUNT !== 'undefined'){
} else {
ribbontrails.global.EMITTER_COUNT = (3);
}
if(typeof ribbontrails.global.emitters !== 'undefined'){
} else {
ribbontrails.global.emitters = [];
}
if(typeof ribbontrails.global.noiseTime !== 'undefined'){
} else {
ribbontrails.global.noiseTime = (Math.random() * (1000));
}
if(typeof ribbontrails.global.ribbonSpeed !== 'undefined'){
} else {
ribbontrails.global.ribbonSpeed = (2);
}
if(typeof ribbontrails.global.BOUNDS !== 'undefined'){
} else {
ribbontrails.global.BOUNDS = (1000);
}
if(typeof ribbontrails.global.worldHolder !== 'undefined'){
} else {
ribbontrails.global.worldHolder = (new THREE.Object3D());
}
if(typeof ribbontrails.global.mousePos !== 'undefined'){
} else {
ribbontrails.global.mousePos = new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"x","x",2099068185),(0),new cljs.core.Keyword(null,"y","y",-1757859776),(0)], null);
}
if(typeof ribbontrails.global.noiseSpeed !== 'undefined'){
} else {
ribbontrails.global.noiseSpeed = 0.001;
}
if(typeof ribbontrails.global.resetFlat !== 'undefined'){
} else {
ribbontrails.global.resetFlat = [];
}
if(typeof ribbontrails.global.updateFlat !== 'undefined'){
} else {
ribbontrails.global.updateFlat = [];
}
if(typeof ribbontrails.global.noiseScale !== 'undefined'){
} else {
ribbontrails.global.noiseScale = (1200);
}
if(typeof ribbontrails.global.noiseSeperation !== 'undefined'){
} else {
ribbontrails.global.noiseSeperation = 0.1;
}
if(typeof ribbontrails.global.stats !== 'undefined'){
} else {
ribbontrails.global.stats = (new Stats());
}
ribbontrails.global.stats.domElement.style.position = "absolute";
ribbontrails.global.stats.domElement.style.top = (0);
ribbontrails.global.stats.domElement.style.zIndex = (999);
document.body.appendChild(ribbontrails.global.stats.domElement);

//# sourceMappingURL=global.js.map?rel=1448577964127