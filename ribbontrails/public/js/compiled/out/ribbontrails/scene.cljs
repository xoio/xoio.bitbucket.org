(ns ribbontrails.scene
  (:require
   [ribbontrails.typography :as typography]
   [ribbontrails.global :as global]
   [ribbontrails.ribbon :as ribbon]
   [ribbontrails.emitters :as emitters]
   [ribbontrails.math :as math]
   [ribbontrails.utils :as utils]))

(defonce renderer (js/THREE.WebGLRenderer.))
(defonce scene (js/THREE.Scene.))
(defonce camera (js/THREE.PerspectiveCamera. 75.0 (utils/getAspectRatio) 1.0 100000))
(aset (.-position camera) "z" 1000)

(defonce worldHolder (js/THREE.Object3D.))
(defonce composer (js/WAGNER.Composer. renderer))
(defonce blur (js/WAGNER.CircularBlurPass.))
(defonce ssao (js/WAGNER.FXAAPass.))
(.setSize composer js/window.innerWidth js/window.innerHeight)

;; setup renderer
(.setSize renderer js/window.innerWidth js/window.innerHeight)

(.appendChild js/document.body (.-domElement renderer))

;; add mousemove logic
(.addEventListener js/window "mousemove" (fn [e]
  (.preventDefault e)
  (let [xCalc (* (/ (.-clientX e) js/window.innerWidth) (- 2 1))
        yCalc (* -1 (* (/ (.-clientY e) js/window.innerHeight) (+ 2 1)))]
        (aset global/mousePos :x xCalc)
        aset global/mousePos :y yCalc)
))

;; add resize logic
(.addEventListener js/window "resize"
  (fn [e]
    (aset camera "aspect" (utils/getAspectRatio))
     (.updateProjectionMatrix camera)
       (.setSize renderer js/window.innerWidth js/window.innerHeight)))


;; build emiters
(emitters/makeEmitters)



;;add worldholder to scene
(.add scene worldHolder)

;; build out the attractors
(let [t (typography/CreateAttractors "soob")]
  (let [pos (.-position (:mesh t))]
    (aset pos "x" -100)
    (aset pos "y" -50)
    (aset pos "z" 500))

  ;; add visual rep to mesh
;;  (.add scene (:mesh t))

  (let [attractors (:attractors t)]
    (dotimes [i (.-length attractors)]
      (let [rib (ribbon/MakeRibbon worldHolder (aget attractors i))]
        (.push global/ribbons rib)))))

(js/console.log (aget global/ribbons 0))
  ;; build ribbons
;;  (dotimes [i global/RIBBON_COUNT]
;;    (let [rib (ribbon/MakeRibbon worldHolder)]
;;      (.push global/ribbons rib)))

(js/console.log scene)
