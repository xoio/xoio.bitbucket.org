// Compiled by ClojureScript 1.7.170 {}
goog.provide('ribbontrails.ribbon');
goog.require('cljs.core');
goog.require('ribbontrails.global');
goog.require('thi.ng.typedarrays.core');
goog.require('thi.ng.math.simplexnoise');
goog.require('ribbontrails.math');
if(typeof ribbontrails.ribbon.SimplexNoise !== 'undefined'){
} else {
ribbontrails.ribbon.SimplexNoise = (new window.SimplexNoise());
}
if(typeof ribbontrails.ribbon.color !== 'undefined'){
} else {
ribbontrails.ribbon.color = (new THREE.Color());
}
if(typeof ribbontrails.ribbon.hue !== 'undefined'){
} else {
ribbontrails.ribbon.hue = (0);
}
ribbontrails.ribbon.lightness = ribbontrails.math.randomRange.call(null,0.2,0.6);
ribbontrails.ribbon.sat = ribbontrails.math.randomRange.call(null,0.6,(1));
ribbontrails.ribbon.flattenIndices = (function ribbontrails$ribbon$flattenIndices(vecarray){

var finalArray = [];
var n__17699__auto___30462 = vecarray.length;
var i_30463 = (0);
while(true){
if((i_30463 < n__17699__auto___30462)){
finalArray.push((vecarray[i_30463]).a);

finalArray.push((vecarray[i_30463]).b);

finalArray.push((0));

finalArray.push((vecarray[i_30463]).a);

var G__30464 = (i_30463 + (1));
i_30463 = G__30464;
continue;
} else {
}
break;
}

return finalArray;
});
ribbontrails.ribbon.updatePositions = (function ribbontrails$ribbon$updatePositions(geo,verts){

var positionAttrib = geo.getAttribute("position");
var vertices = positionAttrib.array;
var n__17699__auto__ = vertices.length;
var i = (0);
while(true){
if((i < n__17699__auto__)){
(vertices[i] = (verts[i]));

var G__30465 = (i + (1));
i = G__30465;
continue;
} else {
return null;
}
break;
}
});

/**
* @constructor
 * @implements {ribbontrails.ribbon.Object}
*/
ribbontrails.ribbon.Ribbon = (function (attractor){
this.attractor = attractor;
})
ribbontrails.ribbon.Ribbon.prototype.init = (function (){
var self__ = this;
var this$ = this;
(this$["LEN"] = (40));

(this$["velocity"] = (new THREE.Vector3()));

(this$["ribbonWidth"] = ribbontrails.math.randomRange.call(null,(2),(12)));

(this$["head"] = (new THREE.Vector3()));

(this$["tail"] = (new THREE.Vector3()));

(this$["speed"] = ribbontrails.math.randomRange.call(null,(5),(20)));

(this$["clumpiness"] = 0.8);

(this$["ribbonSpeed"] = (1));

(this$["attractorActive"] = true);

(this$["attractForce"] = (new THREE.Vector3()));

return this$;
});

ribbontrails.ribbon.Ribbon.prototype.reset = (function (){
var self__ = this;
var this$ = this;
var verts = this$.vertices;
var noiseSeperation = ribbontrails.global.noiseSeperation;
(this$["id"] = (Math.random() * noiseSeperation));

var clumpiness_30466 = this$.clumpiness;
var head_30467 = this$.head;
var emiterId_30468 = ribbontrails.math.randomInt.call(null,(0),(ribbontrails.global.EMITTER_COUNT - (1)));
var randBoundsVector_30469 = ribbontrails.math.randVector3.call(null,ribbontrails.global.BOUNDS);
var randVec3_30470 = ribbontrails.math.randVector3.call(null,ribbontrails.global.startRange);
if((Math.random() < clumpiness_30466)){
head_30467.addVectors((ribbontrails.global.emitters[emiterId_30468]),randVec3_30470);
} else {
head_30467.copy(randBoundsVector_30469);
}

this$.tail.copy(this$.head);

var n__17699__auto___30471 = this$.LEN;
var i_30472 = (0);
while(true){
if((i_30472 < n__17699__auto___30471)){
var v1_30473 = (verts[(i_30472 * (2))]);
var head_30474 = this$.head;
var v2_30475 = (verts[((i_30472 * (2)) + (1))]);
v1_30473.copy(head_30474);

v2_30475.copy(head_30474);

var G__30476 = (i_30472 + (1));
i_30472 = G__30476;
continue;
} else {
}
break;
}

ribbontrails.global.resetFlat = [];

var n__17699__auto___30477 = verts.length;
var i_30478 = (0);
while(true){
if((i_30478 < n__17699__auto___30477)){
ribbontrails.global.resetFlat.push((verts[i_30478]).x,(verts[i_30478]).y,(verts[i_30478]).z);

var G__30479 = (i_30478 + (1));
i_30478 = G__30479;
continue;
} else {
}
break;
}

ribbontrails.ribbon.updatePositions.call(null,this$.geometry,ribbontrails.global.resetFlat);

var positionAttrib = this$.geometry.getAttribute("position");
return (positionAttrib["needsUpdate"] = true);
});

ribbontrails.ribbon.Ribbon.prototype.update = (function (){
var self__ = this;
var this$ = this;
var verts = this$.vertices;
var tail = this$.tail;
var head = this$.head;
var mainVec = ribbontrails.global.mainVec;
var velocity = this$.velocity;
var simplexNoise = ribbontrails.ribbon.SimplexNoise;
var id = this$.id;
var tangent = ribbontrails.global.tangent;
var up = ribbontrails.global.up;
var noiseTime = ribbontrails.global.noiseTime;
var ribbonWidth = this$.ribbonWidth;
var noiseScale = ribbontrails.global.noiseScale;
var normal = ribbontrails.global.normal;
var BOUNDS = ribbontrails.global.BOUNDS;
var attractor__$1 = this$.attractor;
var geometry = this$.geometry;
tail.copy(head);

mainVec.copy(head);

mainVec.divideScalar(noiseScale);

var timeX_30480 = (((0) + noiseTime) + this$.id);
var timeY_30481 = (((50) + noiseTime) + this$.id);
var timeZ_30482 = (((100) + noiseTime) + this$.id);
var speed_30483 = (this$.speed * this$.ribbonSpeed);
var _vec_30484 = ribbontrails.global.mainVec;
var velocity_30485__$1 = this$.velocity;
(velocity_30485__$1["x"] = (ribbontrails.ribbon.SimplexNoise.noise4d((_vec_30484["x"]),(_vec_30484["y"]),(_vec_30484["z"]),timeX_30480) * speed_30483));

(velocity_30485__$1["y"] = (ribbontrails.ribbon.SimplexNoise.noise4d((_vec_30484["x"]),(_vec_30484["y"]),(_vec_30484["z"]),timeY_30481) * speed_30483));

(velocity_30485__$1["z"] = (ribbontrails.ribbon.SimplexNoise.noise4d((_vec_30484["x"]),(_vec_30484["y"]),(_vec_30484["z"]),timeZ_30482) * speed_30483));

head.add(velocity);

if((head.x > BOUNDS)){
this$.reset();
} else {
}

if((head.x < ((-1) * BOUNDS))){
this$.reset();
} else {
}

if((head.y > BOUNDS)){
this$.reset();
} else {
}

if((head.y < ((-1) * BOUNDS))){
this$.reset();
} else {
}

if((head.z > BOUNDS)){
this$.reset();
} else {
}

if((head.z < ((-1) * BOUNDS))){
this$.reset();
} else {
}

tangent.subVectors(head,tail);

tangent.normalize();

mainVec.crossVectors(tangent,up);

mainVec.normalize();

normal.crossVectors(tangent,mainVec);

normal.multiplyScalar(ribbonWidth);

var aForce_30486 = attractor__$1.seek(mainVec,velocity);
head.add(aForce_30486);

tail.add(aForce_30486);

normal.add(aForce_30486);

var i_30487 = (this$.LEN - (1));
while(true){
if((i_30487 > (0))){
var v_30488 = (verts[(i_30487 * (2))]);
var c1_30489 = (verts[((i_30487 - (1)) * (2))]);
v_30488.copy(c1_30489);

var v2_30490 = (verts[((i_30487 * (2)) + (1))]);
var c2_30491 = (verts[(((i_30487 - (1)) * (2)) + (1))]);
v2_30490.copy(c2_30491);

var G__30492 = (i_30487 - (1));
i_30487 = G__30492;
continue;
} else {
}
break;
}

(verts[(0)]).copy(head);

(verts[(0)]).add(normal);

(verts[(1)]).copy(head);

(verts[(1)]).sub(normal);

ribbontrails.global.updateFlat = [];

var n__17699__auto___30493 = verts.length;
var i_30494 = (0);
while(true){
if((i_30494 < n__17699__auto___30493)){
var vert_30495 = (verts[i_30494]);
ribbontrails.global.updateFlat.push(vert_30495.x);

ribbontrails.global.updateFlat.push(vert_30495.y);

ribbontrails.global.updateFlat.push(vert_30495.z);

var G__30496 = (i_30494 + (1));
i_30494 = G__30496;
continue;
} else {
}
break;
}

ribbontrails.ribbon.updatePositions.call(null,this$.geometry,ribbontrails.global.updateFlat);

var positionAttrib = this$.geometry.getAttribute("position");
var vertices = positionAttrib.array;
return (positionAttrib["needsUpdate"] = true);
});

ribbontrails.ribbon.Ribbon.prototype.buildMesh = (function (){
var self__ = this;
var this$ = this;
var geo_30497 = (new THREE.BufferGeometry());
var vertices_30498 = thi.ng.typedarrays.core.float32.call(null,((6) * this$.LEN));
var colors_30499 = thi.ng.typedarrays.core.float32.call(null,((3) * this$.LEN));
var indices_30500 = thi.ng.typedarrays.core.uint16.call(null,((3) * this$.LEN));
var verts_30501 = [];
var indexes_30502 = [];
var n__17699__auto___30503 = this$.LEN;
var i_30504 = (0);
while(true){
if((i_30504 < n__17699__auto___30503)){
verts_30501.push((new THREE.Vector3((0),(0),(0))));

verts_30501.push((new THREE.Vector3((0),(0),(0))));

var G__30505 = (i_30504 + (1));
i_30504 = G__30505;
continue;
} else {
}
break;
}

var verticesFlat_30506 = ribbontrails.math.flattenArray.call(null,verts_30501);
var n__17699__auto___30507 = verticesFlat_30506.length;
var i_30508 = (0);
while(true){
if((i_30508 < n__17699__auto___30507)){
(vertices_30498[i_30508] = (verticesFlat_30506[i_30508]));

var G__30509 = (i_30508 + (1));
i_30508 = G__30509;
continue;
} else {
}
break;
}

var n__17699__auto___30510 = this$.LEN;
var i_30511 = (0);
while(true){
if((i_30511 < n__17699__auto___30510)){
indexes_30502.push((new THREE.Face3(((2) * i_30511),((1) + (i_30511 * (2))),((2) + (i_30511 * (2))))));

indexes_30502.push((new THREE.Face3(((1) + (i_30511 * (2))),((3) + (i_30511 * (2))),((2) + (i_30511 * (2))))));

var G__30512 = (i_30511 + (1));
i_30511 = G__30512;
continue;
} else {
}
break;
}

var indicesFlat_30513 = ribbontrails.ribbon.flattenIndices.call(null,indexes_30502);
var n__17699__auto___30514 = indicesFlat_30513.length;
var i_30515 = (0);
while(true){
if((i_30515 < n__17699__auto___30514)){
(indices_30500[i_30515] = (indicesFlat_30513[i_30515]));

var G__30516 = (i_30515 + (1));
i_30515 = G__30516;
continue;
} else {
}
break;
}

ribbontrails.ribbon.hue = ((this$.head.x / (this$.BOUNDS / (2))) / ((2) + 0.5));

if((Math.random() < 0.1)){
ribbontrails.ribbon.hue = Math.random();
} else {
}

ribbontrails.ribbon.hue = ((this$.head.x / (this$.BOUNDS / (2))) / ((2) + 0.5));

if((Math.random() < 0.1)){
ribbontrails.ribbon.hue = Math.random();
} else {
}

var i_30517 = (0);
while(true){
if((i_30517 < colors_30499.length)){
var p1_30518 = (((1) - i_30517) / this$.LEN);
var p2_30519 = (ribbontrails.ribbon.lightness / (4));
var p3_30520 = ((3) / (4));
var p4_30521 = (ribbontrails.ribbon.lightness * p3_30520);
var lightVal_30522 = ((p1_30518 * p2_30519) + (p3_30520 * p4_30521));
cljs.core.List.EMPTY;

ribbontrails.ribbon.color.setHSL(ribbontrails.ribbon.hue,ribbontrails.ribbon.sat,lightVal_30522);

(colors_30499[i_30517] = ribbontrails.ribbon.color.r);

(colors_30499[(i_30517 + (1))] = ribbontrails.ribbon.color.g);

(colors_30499[(i_30517 + (2))] = ribbontrails.ribbon.color.b);

var G__30523 = (i_30517 + (3));
i_30517 = G__30523;
continue;
} else {
}
break;
}

var colorAttrib_30524 = (new THREE.BufferAttribute(colors_30499,(3)));
geo_30497.addAttribute("color",colorAttrib_30524);

(this$["colorArray"] = colors_30499);

var positionAttrib_30525 = (new THREE.BufferAttribute(vertices_30498,(3)));
geo_30497.addAttribute("position",positionAttrib_30525);

var indexAttrib_30526 = (new THREE.BufferAttribute(indices_30500,(1)));
geo_30497.setIndex(indexAttrib_30526);

(this$["vertices"] = verts_30501);

(this$["vertlen"] = verts_30501.length);

(this$["geometry"] = geo_30497);

var material_30527 = (new THREE.ShaderMaterial(cljs.core.clj__GT_js.call(null,new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"blending","blending",31165821),THREE.AdditiveBlending,new cljs.core.Keyword(null,"transparent","transparent",-2073609949),false,new cljs.core.Keyword(null,"depthWrite","depthWrite",1125643927),false,new cljs.core.Keyword(null,"uniforms","uniforms",-782808153),cljs.core.clj__GT_js.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"time","time",1385887882),cljs.core.clj__GT_js.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),"f",new cljs.core.Keyword(null,"value","value",305978217),0.0], null))], null)),new cljs.core.Keyword(null,"vertexShader","vertexShader",-668531773),document.getElementById("vertexShader").textContent,new cljs.core.Keyword(null,"fragmentShader","fragmentShader",1579642943),document.getElementById("fragmentShader").textContent], null))));
(this$["mesh"] = (new THREE.Mesh(geo_30497,material_30527)));

(this$["material"] = material_30527);

return this$;
});

ribbontrails.ribbon.Ribbon.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"attractor","attractor",-453712809,null)], null);
});

ribbontrails.ribbon.Ribbon.cljs$lang$type = true;

ribbontrails.ribbon.Ribbon.cljs$lang$ctorStr = "ribbontrails.ribbon/Ribbon";

ribbontrails.ribbon.Ribbon.cljs$lang$ctorPrWriter = (function (this__17394__auto__,writer__17395__auto__,opt__17396__auto__){
return cljs.core._write.call(null,writer__17395__auto__,"ribbontrails.ribbon/Ribbon");
});

ribbontrails.ribbon.__GT_Ribbon = (function ribbontrails$ribbon$__GT_Ribbon(attractor){
return (new ribbontrails.ribbon.Ribbon(attractor));
});

ribbontrails.ribbon.MakeRibbon = (function ribbontrails$ribbon$MakeRibbon(scene,attractor){
var rib = (new ribbontrails.ribbon.Ribbon(attractor));
rib.init();

rib.buildMesh();

rib.reset();

scene.add(rib.mesh);

return rib;
});

//# sourceMappingURL=ribbon.js.map?rel=1448681207117