(ns ribbontrails.emitters
  (:require
   [ribbontrails.math :as math]
   [ribbontrails.global :as global]))


(defn makeEmitters []
  (dotimes [i global/EMITTER_COUNT]
    (.push global/emitters (math/randVector3 (/ global/BOUNDS 2))
    )))
