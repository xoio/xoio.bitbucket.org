// Compiled by ClojureScript 1.7.170 {}
goog.provide('ribbontrails.scene');
goog.require('cljs.core');
goog.require('ribbontrails.global');
goog.require('ribbontrails.math');
goog.require('ribbontrails.ribbon');
goog.require('ribbontrails.emitters');
goog.require('ribbontrails.utils');
goog.require('ribbontrails.typography');
if(typeof ribbontrails.scene.renderer !== 'undefined'){
} else {
ribbontrails.scene.renderer = (new THREE.WebGLRenderer());
}
if(typeof ribbontrails.scene.scene !== 'undefined'){
} else {
ribbontrails.scene.scene = (new THREE.Scene());
}
if(typeof ribbontrails.scene.camera !== 'undefined'){
} else {
ribbontrails.scene.camera = (new THREE.PerspectiveCamera(75.0,ribbontrails.utils.getAspectRatio.call(null),1.0,(100000)));
}
(ribbontrails.scene.camera.position["z"] = (1000));
if(typeof ribbontrails.scene.worldHolder !== 'undefined'){
} else {
ribbontrails.scene.worldHolder = (new THREE.Object3D());
}
if(typeof ribbontrails.scene.composer !== 'undefined'){
} else {
ribbontrails.scene.composer = (new WAGNER.Composer(ribbontrails.scene.renderer));
}
if(typeof ribbontrails.scene.blur !== 'undefined'){
} else {
ribbontrails.scene.blur = (new WAGNER.CircularBlurPass());
}
if(typeof ribbontrails.scene.ssao !== 'undefined'){
} else {
ribbontrails.scene.ssao = (new WAGNER.FXAAPass());
}
ribbontrails.scene.composer.setSize(window.innerWidth,window.innerHeight);
ribbontrails.scene.renderer.setSize(window.innerWidth,window.innerHeight);
document.body.appendChild(ribbontrails.scene.renderer.domElement);
window.addEventListener("mousemove",(function (e){
e.preventDefault();

var xCalc = ((e.clientX / window.innerWidth) * ((2) - (1)));
var yCalc = ((-1) * ((e.clientY / window.innerHeight) * ((2) + (1))));
(ribbontrails.global.mousePos[new cljs.core.Keyword(null,"x","x",2099068185)] = xCalc);




return yCalc;
}));
window.addEventListener("resize",(function (e){
(ribbontrails.scene.camera["aspect"] = ribbontrails.utils.getAspectRatio.call(null));

ribbontrails.scene.camera.updateProjectionMatrix();

return ribbontrails.scene.renderer.setSize(window.innerWidth,window.innerHeight);
}));
ribbontrails.emitters.makeEmitters.call(null);
ribbontrails.scene.scene.add(ribbontrails.scene.worldHolder);
var t_30530 = ribbontrails.typography.CreateAttractors.call(null,"soob");
var pos_30531 = new cljs.core.Keyword(null,"mesh","mesh",456320595).cljs$core$IFn$_invoke$arity$1(t_30530).position;
(pos_30531["x"] = (-100));

(pos_30531["y"] = (-50));

(pos_30531["z"] = (500));

var attractors_30532 = new cljs.core.Keyword(null,"attractors","attractors",464325791).cljs$core$IFn$_invoke$arity$1(t_30530);
var n__17699__auto___30533 = attractors_30532.length;
var i_30534 = (0);
while(true){
if((i_30534 < n__17699__auto___30533)){
var rib_30535 = ribbontrails.ribbon.MakeRibbon.call(null,ribbontrails.scene.worldHolder,(attractors_30532[i_30534]));
ribbontrails.global.ribbons.push(rib_30535);

var G__30536 = (i_30534 + (1));
i_30534 = G__30536;
continue;
} else {
}
break;
}
console.log((ribbontrails.global.ribbons[(0)]));
console.log(ribbontrails.scene.scene);

//# sourceMappingURL=scene.js.map?rel=1448681207148