
import RenderBuffer from "./RenderBuffer"
const glslify = require('glslify');

class Sphere extends RenderBuffer{
    constructor(renderer){
        super(20,glslify("./shaders/simulation.glsl"),renderer);
        var size = 20;

        this.phiBuffer = new RenderBuffer(size,glslify('./shaders/phiCalc.glsl'),renderer);
        this.thetaBuffer = new RenderBuffer(size,glslify('./shaders/thetaCalc.glsl'),renderer);


        this._setOriginData();
        this._calculateThetaAndPhiSpeeds();
        this.size = size;
        this._buildRenderScene();
    }

    /**
     * Calculates a random value for deriving a increase in theta and phi
     * and encoders
     * @private
     */
    _calculateThetaAndPhiSpeeds(){
        var s2 = this.size * this.size;
        var size = this.size;
        var phiSpeeds = new Float32Array( s2 * 4 );
        var thetaSpeeds = new Float32Array( s2 * 4 );

        for( var i =0; i < phiSpeeds.length; i++ ){
            phiSpeeds[ i ] = (Math.random() - .5 ) * size;
            thetaSpeeds[ i ] = (Math.random() - .5 ) * size;
        }

        this.thetaSpeeds = new THREE.DataTexture(
            thetaSpeeds,
            this.size,
            this.size,
            THREE.RGBAFormat,
            THREE.FloatType
        );

        this.phiSpeeds = new THREE.DataTexture(
            phiSpeeds,
            this.size,
            this.size,
            THREE.RGBAFormat,
            THREE.FloatType
        );
        this.thetaSpeeds.minFilter =  THREE.NearestFilter;
        this.thetaSpeeds.magFilter = THREE.NearestFilter;
        this.thetaSpeeds.needsUpdate = true;

        this.phiSpeeds.minFilter =  THREE.NearestFilter;
        this.phiSpeeds.magFilter = THREE.NearestFilter;
        this.phiSpeeds.needsUpdate = true;

        this.thetaBuffer.setUniform('thetaSpeed',this.thetaSpeeds);
        this.phiBuffer.setUniform('phiSpeed',this.phiSpeeds);
    }

    _setOriginData(){
        var size = this.size;
        this.resetRand(8); // populate sphere data
        this.phiBuffer.resetRand(8) // populate phi data
        this.thetaBuffer.resetRand(8) // populate theta data

        this.setUniform('theta',null,'t');
        this.setUniform('phi',null,'t');
    }

    _randomRange(min, max) {
        return min + Math.random() * (max - min);
    }

    _random(min,max){
        return min + Math.random() * (max - min);
    }
    /**
     * Build the geometry and material we're gonna render
     * @private
     */
    _buildRenderScene(){
        var size = this.size;

        var geo = new THREE.BufferGeometry();
        var positions = new Float32Array(  size * size * 3 );

        for ( var i = 0, j = 0, l = positions.length / 3; i < l; i ++, j += 3 ) {
            positions[ j     ] = ( i % size ) / size;
            positions[ j + 1 ] = Math.floor( i / size ) / size;
        }

        var posA = new THREE.BufferAttribute( positions , 3 );
        geo.addAttribute( 'position', posA );

        var mat = new THREE.RawShaderMaterial({
            vertexShader:glslify('./shaders/render-v.glsl'),
            fragmentShader:glslify('./shaders/render-f.glsl'),
            uniforms:{
                time:{
                    type:'f',
                    value:0.0
                },
                map:{
                    type:'t',
                    value:null
                }
            }
        });
        this.renderMesh = new THREE.Points(geo,mat);
        this.renderMat = mat;
    }

    updateScene(){
        this.renderMat.uniforms.time.value = performance.now() * 0.0001;
        this.renderMat.uniforms.map.value = this.getOutput();
        this.update();
        this.phiBuffer.update();
        this.thetaBuffer.update();
        this.updateUniform('theta',this.thetaBuffer.getOutput());
        this.updateUniform('phi',this.phiBuffer.getOutput());
    }

    addTo(scene){
        scene.add(this.renderMesh);
        return this;
    }
}

export default Sphere;