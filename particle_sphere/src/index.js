import Sphere from "./Sphere"

var raf = require('raf');
WAGNER.vertexShadersPath = "/particle_sphere/public/post/vertex-shaders";
WAGNER.fragmentShadersPath = "/particle_sphere/public/post/fragment-shaders";

var renderer = new THREE.WebGLRenderer()
//renderer.setClearColor(0x111112);
var composer = new WAGNER.Composer(renderer,{
    useRGBA:true
});
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75.0,window.innerWidth / window.innerHeight, 1.0,1000);
camera.position.z = 100;

renderer.setSize(window.innerWidth,window.innerHeight);
composer.setSize(window.innerWidth,window.innerHeight);
document.body.appendChild(renderer.domElement);


var stats = new Stats();
stats.domElement.style.position = 'absolute';
stats.domElement.style.top = '0px';
document.body.appendChild( stats.domElement );


var sphere = new Sphere(renderer);
sphere.addTo(scene);

var blurPass = new WAGNER.FullBoxBlurPass();
blurPass.params.amount = 2;

var bloomPass = new WAGNER.MultiPassBloomPass();
bloomPass.params.strength = 10.0;

var zoomBlurPass = new WAGNER.ZoomBlurPass();
zoomBlurPass.params.strength = 0.034930;


animate();
function animate(){
    requestAnimationFrame(animate);

    sphere.updateScene();
    //renderer.render(scene,camera);
    stats.update();

    composer.reset();
    composer.render( scene, camera );

    composer.pass( blurPass );
    composer.pass( bloomPass );

    composer.pass(zoomBlurPass);
    composer.toScreen();

}
