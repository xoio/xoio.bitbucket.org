/**
 * Mostly static class for dealing with various common operations.
 * Purposely kept as loosely coupled as possible.
 *
 * Will default to a Three.js implementation if Three.js is available. When it makes sense for and a clean api can
 * be put together, some functions allow you to specify a specific format.
 *
 * requires glslify
 */
var glslify = require('glslify');

class GLUtils {
    constructor(){}

    /**
     * Generates a random texture with random data
     * @param width width for the texture
     * @param height height for the texture
     * @param data optionally pass in your own data - otherwise a array with random values will be used.
     * @returns {THREE.DataTexture}
     */
    static generateTexture(width, height, data) {

        data = data !== undefined ? data : (function( ){
            var radius = 200;
            var num = width * height;
            var data = new Float32Array( num * 3);
            for (var i = 0, i3 = 0; i < num; i ++, i3 += 3) {
                data[i3] = 0;
                data[i3 + 1] = 0;
                data[i3 + 2] = 0;
            }
            return data;
        })();

        if(THREE){
            var tex = new THREE.DataTexture(data, width,height, THREE.RGBAFormat, THREE.FloatType );
            tex.minFilter = THREE.NearestFilter;
            tex.magFilter = THREE.NearestFilter;
            tex.generateMipmaps = false;
            tex.needsUpdate = true;
        }

        return tex;
    }

    static generateDefaultShader(buildType){
        return GLUtils.createPassthru(buildType);
    }

    /**
     * Generates a shader object or a full program if Three.js is available
     * @param vertex the path to the vertex shader
     * @param fragment the path to the fragment shader
     * @param uniforms any uniforms
     * @param attributes any attributes
     * @returns {THREE.ShaderMaterial}
     */
    static generateShader(vertex,fragment,uniforms,attributes){

        if(THREE){
            uniforms = uniforms !== undefined ? uniforms : {
                time:{
                    type:'f',
                    value:0.0
                },

                originTexture:{
                    type:'t',
                    value:null
                },

                destinationTexture:{
                    type:'t',
                    value:null
                }
            };

            //if we just need a fragment shader, you can use the keyword 'passthru' to have the function
            //auto generate a passthru shader
            if(vertex === 'passthru'){
                vertex = GLUtils.createPassthruVertex();
            }


            return new THREE.ShaderMaterial({
                vertexShader:vertex,
                fragmentShader:fragment,
                uniforms:uniforms
            });

        }
    }

    static generateRenderTarget(width,height,options){
        if(THREE){

            var defaults = {
                minFilter: THREE.NearestFilter,
                magFilter: THREE.NearestFilter,
                format: THREE.RGBAFormat,
                type: THREE.FloatType,
                stencilBuffer: false
            };

            return new THREE.WebGLRenderTarget(width,height,defaults);
        }
    }

    /**
     * Generates a pass-thru vertex shader
     * @returns {string}
     * @private
     */
    static createPassthruVertex(){
        //more threejs specific
        if(THREE){
            return [
                'varying vec2 vUv;',
                'void main() {',
                '  vUv = uv;',
                '  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );',
                '}'
            ].join('\n');
        }
    }

    /**
     *	Creates a passthru shader
     */
    static createPassthru(buildType){
        this.passThruVS =[
            'varying vec2 vUv;',
            'void main() {',
            '  vUv = uv;',
            '  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );',
            '}'
        ].join('\n');

        this.passThruFS = [
            'uniform sampler2D texture;',
            'varying vec2 vUv;',
            'void main() {',
            '  vec2 read = vUv;',
            '  vec4 c = texture2D( texture , vUv );',
            '  gl_FragColor = c ;',
            //   'gl_FragColor = vec4(1.);',
            '}'
        ].join('\n');

        if(THREE || buildType === 'three'){
            var uniforms = {
                texture:{  type:'t'  , value:null }
            };

            var texturePassShader = new THREE.ShaderMaterial({
                uniforms:uniforms,
                vertexShader:this.passThruVS,
                fragmentShader:this.passThruFS
            });
        }

        return texturePassShader;

    }
}

export default GLUtils;/**
 * Created by sortofsleepy on 12/9/15.
 */
