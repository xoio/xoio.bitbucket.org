/**
 * A class for doing GPU pingponging.
 * Largely based on Isaac Cohen's PhysicsRenderer(https://github.com/cabbibo), cleaned up a bit, and
 * adding some flags for the future so this can (hopefully) be used interchangeably with
 * regular WebGL code in addition to Three.js
 *
 * Requires nitro-glutils
 */

import GLUtils from "./GLUtils"
class RenderBuffer {
    constructor(size , shader , renderer) {

        this.renderer = renderer;

        this.size = size !== undefined ? size : 128;
        this.s2   = size * size;

        this.renderer = renderer;

        this.clock = new THREE.Clock();

        this.resolution = new THREE.Vector2( this.size , this.size );

        this.counter = 0;

        this.texturePassProgram = GLUtils.createPassthru();

        // WHERE THE MAGIC HAPPENS
        this.simulation = GLUtils.generateShader('passthru',shader,{
            originTexture:{      type:"t"  , value:null },
            destinationTexture:{       type:"t"  , value:null },
            resolution: { type:"v2" , value: this.resolution }
        });

        // Sets up our render targets
        this.buildRenderTargets();

        //build scene for target rendering
        this.buildRenderScene();

    }

    /**
     * Updates the simulation
     */
    update(){
        var flipFlop = this.counter % 3;

        if( flipFlop == 0 ){
            this.simulation.uniforms.destinationTexture.value = this.rt_2;
            this.pass( this.simulation, this.rt_3 );

        }else if( flipFlop == 1 ){
            this.simulation.uniforms.destinationTexture.value = this.rt_3;
            this.pass( this.simulation , this.rt_1 );
        }else if( flipFlop == 2 ){
            this.simulation.uniforms.destinationTexture.value = this.rt_1;
            this.pass( this.simulation , this.rt_2 );
        }

        this.counter ++;

    }

    /**
     * Runs a pass against the current target. Sets the output target for use
     * in rendering
     * @param shader the shader to use
     * @param target the target to run the pass on
     *
     * TODO remember to take out the shader param as we don't really need it.
     */
    pass(shader,target){

        this.mesh.material = shader;
        this.renderer.render( this.scene, this.camera, target, false );
        this.output = target;
    }

    /**
     * Gets the current output of the simulation.
     * @returns {*}
     */
    getOutput(){
        if(this.output !== undefined){
            return this.output;
        }
    }


    /**
     * Allows you to inject additional uniforms into the simulation shader
     * @param uniform the name of the uniform
     * @param value the value to initially set for the uniform
     * @param uniformType optional - if it's not defined, will attempt to derrive type based on the value
     * being passed in.
     */
    setUniform(uniform,value,uniformType){
        if(THREE){
            var typeOfUniform = null;
            if(uniformType !== undefined){
                typeOfUniform = uniformType;
            }else{
                typeOfUniform = determineType(value);
            }

            //by default lets just assume it's a floating point uniform
            //TODO add more checks?
            if(typeOfUniform === null){
                typeOfUniform = 'f'
            }

            this.simulation.uniforms[uniform] = {
                type:typeOfUniform,
                value:value
            };

            /**
             * Attempts to derrive the type of uniform based on the value being passed in
             * @param value
             * @returns {*}
             */
            function determineType(value){
                var instance = null;

                if(value instanceof THREE.Vector2){
                    instance = 'v2';
                } else if(value instanceof THREE.Vector3){
                    instance = 'v3';
                }else if((value instanceof THREE.Texture)){
                    instance = 't';
                }else if(value instanceof THREE.WebGLRenderTarget){
                    instance = 't';
                }else{
                    instance = 'f';
                }
                return instance;
            }
        }
    }

    updateUniform(name,value){
        if(this.simulation.uniforms[name] !== undefined){
            this.simulation.uniforms[name].value = value;
        }
    }


    /**
     * Kicks things off with a bit of random data.
     * @param size
     * @param alpha
     */
    resetRand(size,alpha){

        if(THREE){
            var size = size || 100;
            var data = new Float32Array( this.s2 * 4 );

            for( var i =0; i < data.length; i++ ){

                //console.log('ss');
                data[ i ] = (Math.random() - .5 ) * size;

                if( alpha && i % 4 ===3 ){
                    data[i] = 0;
                }

            }

            var texture = new THREE.DataTexture(
                data,
                this.size,
                this.size,
                THREE.RGBAFormat,
                THREE.FloatType
            );

            texture.minFilter =  THREE.NearestFilter,
                texture.magFilter = THREE.NearestFilter,
                texture.needsUpdate = true;

            this.setData(texture);
        }
    }

    ////////////////// INTERNAL /////////////////////////

    /**
     * Sets the data to be used for the simulation and runs a pass across all the buffers.
     * @param texture
     */
    setData(texture){
        this.texture = texture;
        this.texturePassProgram.uniforms.texture.value = texture;

        this.pass( this.texturePassProgram , this.rt_1 );
        this.pass( this.texturePassProgram , this.rt_2 );
        this.pass( this.texturePassProgram , this.rt_3 );
    }

    buildRenderScene(){
        if(THREE){
            var camera = new THREE.OrthographicCamera( - 0.5, 0.5, 0.5, - 0.5, 0, 1 );
            var scene = new THREE.Scene();
            var mesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( 1, 1 ) );

            var size = this.size;
            scene.add(mesh);

            this.mesh = mesh;
            this.camera = camera;
            this.scene = scene;
        }
    }

    buildRenderTargets(){
        if(THREE){
            // Sets up our render targets
            this.rt_1 = GLUtils.generateRenderTarget(this.size,this.size);
            this.rt_2 = this.rt_1.clone();
            this.rt_3 = this.rt_1.clone();
        }else{
            this.rt_1 = this.renderer.createFramebuffer();
            this.rt_2 = this.renderer.createFramebuffer();
            this.rt_3 = this.renderer.createFramebuffer();
        }
    }
}

export default RenderBuffer;