precision mediump float;
precision mediump int;
uniform mat4 modelViewMatrix; // optional
uniform mat4 projectionMatrix; // optional
attribute vec3 position;
attribute vec4 color;

uniform float time;

varying vec3 vPosition;
varying vec4 vColor;
varying float timeOut;
uniform sampler2D map;
uniform sampler2D poop;
void main()	{
    vec4 pos = texture2D( map , position.xy );
    vPosition = position;
	vColor = color;


     vec4 poss = vec4( pos.xyz, 1.0 );

    gl_PointSize = 20.0;

    gl_Position = projectionMatrix * modelViewMatrix * poss;

}