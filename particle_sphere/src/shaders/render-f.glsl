precision mediump float;
precision mediump int;
uniform sampler2D mmap;
uniform float time;
varying vec3 vPosition;
varying vec4 vColor;

void main()	{
    vec4 data = texture2D(mmap,vPosition.xy);
    vec4 color = vec4( vColor );
    color= vec4(vPosition.xy,0.5+0.5*sin(time),1.0);


    gl_FragColor = vec4(color.r,0.2,color.g,1.0);

}
