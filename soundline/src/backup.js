/**
 * Quick thing based on one of @mattdesl's codevember sketchs (#21 specifically)
 * http://mattdesl.svbtle.com/codevember
 *
 * @type {*|exports|module.exports}
 */
const createLine = require('./gl-line-3d');
const raf = require('raf');
const createOrbit = require('orbit-controls');
const createCamera = require('perspective-camera');
const vignette = require('gl-vignette-background');
const hexRgbByte = require('hex-rgb');
const hexRgb = (str) => hexRgbByte(str).map(x => x / 255);
const assign = require('object-assign');
const setIdentity = require('gl-mat4/identity');
const rotate = require('gl-mat4/rotate')
const soundCloud = require('soundcloud-badge');
const createShader = require('gl-shader');
const glAudioAnalyser = require('gl-audio-analyser');
const glslify = require('glslify');
const lerp = require('lerp');
const newArray = require('array-range');
const once = require('once');
const SimplexNoise = require('./SimplexNoise')
const createTexture = require('gl-texture2d');

////////// SETTINGS  //////////
const defaults = {
    opacity: 0.5,
    useHue: false,
    additive: false
}

const presets = [
    { gradient: [ '#fff', '#4f4f4f' ],
        color: '#000', useHue: true }
];

let settings = presets[Math.floor(Math.random() * presets.length)]
settings = assign({}, defaults, settings)
let time = 0;
///////// GLOBAL ///////////
var noise = new SimplexNoise();

var analyser = "";
const identity = setIdentity([])
let aspect = window.innerWidth / window.innerHeight;
const radius = 0.1
const thickness = 0.01
const steps = 200
const segments = 100;

var lines = [];
var paths = [];

var lines2 = [];
var paths2 = [];
var img = new Image();
const colorVec = hexRgb(settings.color);

////////// BUILD CONTEXT //////////
var gl = require('webgl-context')({
    width:window.innerWidth,
    height:window.innerHeight
});

document.body.appendChild(gl.canvas);
///////// BUILD CAMERA //////////

var reflect = createTexture(gl,img);

const camera = createCamera({
    fov: 50 * Math.PI / 180,
    position: [0, 0, 0.0003],
    near: 0.0001,
    far: 10000
})

const controls = createOrbit({
    element: gl.canvas,
    distanceBounds: [0.5, 100],
    distance: 0.5
})

///////// BUILD SHADER ////////////
const shader = createShader(gl,
    glslify(__dirname + '/shaders/line.vert'),
    glslify(__dirname + '/shaders/line.frag')
)

///////// BUILD BACKGROUND ////////
const background = vignette(gl)
background.style({
    aspect: 1,
    color1: hexRgb(settings.gradient[0]),
    color2: hexRgb(settings.gradient[1]),
    smoothing: [ -0.5, 1.0 ],
    noiseAlpha: 0.1,
    offset: [ -0.05, -0.15 ]
});
////////////// SETUP AUDIO //////////////////////
const sc = soundCloud({
    client_id:"f07c29e017bd7cfd17774ab7fa60f5b1",
    song:"https://soundcloud.com/davidbowieofficial/ziggy-stardust-1",
    dark:true,
    getFonts:true,
},function(err,src,data,div){
    const audio = new window.Audio()
    audio.loop = true
    audio.crossOrigin = 'Anonymous'

    img.src = "/img/hdrLight.jpg";

    img.onload = function(){
        audio.addEventListener('canplay', once(() => {
            start(audio);
        }));
        audio.src = src;
    }


});
////////////////////////////////////

function start(audio) {

    analyser = glAudioAnalyser(gl, audio);
    audio.play();

    //build first loop
    paths = newArray(100).map(createcirc)
    lines = paths.map(path => {
        return createLine(gl, shader, path)
    });

    //build second loop
    paths2 = newArray(100).map(createcirc);
    lines2 = paths.map(path => {
        return createLine(gl, shader, path)
    });

    lines2.forEach((line, i, list) => {
        var mvMatrix = setIdentity([])
        rotate(mvMatrix,mvMatrix,90 * 180,[1,1,1]);
        line.model = mvMatrix;
    })

}


///////// LOOP ////////
raf(function tick(dt){
    time += Math.min(30, dt) / 1000
    const width = gl.drawingBufferWidth
    const height = gl.drawingBufferHeight

    // set up our camera
    camera.viewport[2] = width
    camera.viewport[3] = height
    controls.update(camera.position, camera.direction, camera.up)
    camera.update()

    gl.viewport(0, 0, width, height)
    gl.clearColor(0, 0, 0, 1)
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

    const size = Math.min(width, height) * 1.5
    gl.disable(gl.DEPTH_TEST)
    background.style({
        scale: [ 1 / width * size, 1 / height * size ]
    })
    background.draw()

    shader.bind()
    shader.uniforms.time = time;
    shader.uniforms.radius = radius
    shader.uniforms.audioTexture = 0
    shader.uniforms.opacity = settings.opacity
    shader.uniforms.useHue = settings.useHue

    if(analyser){
        analyser.bindFrequencies(0)
    }
    lines.forEach((line, i, list) => {
        var mat = identity;
        rotate(mat,mat,(time) * 0.0000005,[1,0,0]);


        line.color = colorVec
        line.thickness = thickness
        line.model = identity;
        line.view = camera.view
        line.projection = camera.projection
        line.aspect = width / height
        line.miter = 0
        shader.uniforms.audioTexture = 0
        shader.uniforms.index = i / (list.length - 1)
        line.draw()
    })


    lines2.forEach((line, i, list) => {

        var mat = line.model
        rotate(mat,mat,(time) * 0.00005,[1,1,0]);

        line.color = colorVec
        line.thickness = thickness
        //line.model = identity;
        line.view = camera.view
        line.projection = camera.projection
        line.aspect = width / height
        line.miter = 0
        shader.uniforms.index = i / (list.length - 1)
        shader.uniforms.audioTexture = 0

        line.draw()
    })



    raf(tick);
});


function createcirc () {
    var deg2rad = 3.14149 / 180;
    var radius = Math.random() * 2.0;

    var comps = [];

    for(var i = 0; i < 360; ++i){
        var deg = i * deg2rad;
        var x = Math.cos(deg) * radius;
        var y = Math.sin(deg) * radius;
        var z = 0.0;

        //normalize values
        var len = x * x + y * y + z * z;
        if(len > 0){
            len = 1 / Math.sqrt(len);
            x *= len;
            y *= len;
            z *= len;
        }

        comps.push([x,y,z]);
    }

    return comps;
}
