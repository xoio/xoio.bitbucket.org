const rsvp = require('rsvp');


export function http(url){
    if(url === null){
        console.error("http() requires a url")
        return false;
    }
    var req = new XMLHttpRequest();
    req.open("GET",url,true);
    req.send();
    return new rsvp.Promise(function(resolve,reject){

        req.onreadystatechange = function(){
            if(req.readyState === 4){
                if(req.status === 200){
                    resolve(req.responseText);
                }else{
                    reject(req.responseText);
                }
            }else{
                setTimeout(function(){
                    reject({
                        msg:"unable to make request",
                        status:req.status,
                        state:req.readyState
                    })
                },1000);
            }

        }
    });
}