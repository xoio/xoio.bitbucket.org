import {http} from "./http"
var inArr = [11,18,21,28,31,38,40,55,60,62];
var val = 67;

var test = isSumPossible(inArr,val)
console.log(test);

function isSumPossible(a, n) {
    var len = a.length;
    var possible = 0;

    for(var i = 0; i < len; ++i){
        var promise = compare(a[i],i);

        if(promise !== 0){
            possible = 1;
        }
    }

    function compare(val,index){
        var found = 0;
        for(var i = 0; i < len; ++i){
            if(i !== index){
                if(val + a[i] === n){
                    found = 1;
                }
            }

        }

        return found;
    }

    return possible;
}
