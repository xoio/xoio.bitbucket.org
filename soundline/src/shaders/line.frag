precision highp float;
uniform sampler2D reflectTexture;
uniform sampler2D brushTexture;
uniform vec3 color;
uniform float opacity;
uniform bool useHue;
uniform float iGlobalTime;
varying float vAngle;
varying vec2 uvCoords;

#define PI 3.14
#pragma glslify: hsl2rgb = require('glsl-hsl2rgb')
#pragma glslify: noise3d = require('glsl-noise/simplex/3d')

void main() {
  vec3 tCol = color;
  float val = noise3d(tCol) + iGlobalTime;

  vec4 reflect = texture2D(reflectTexture,uvCoords);

  if (useHue) {
    float sat = 1.7;
    float light = 0.6;

    val += sat * light;

    float rainbow = sin(vAngle / 1.0) * 0.5 + 0.5;
    float hue = 0.0;
    hue += mix(0.5, 0.9, rainbow);
    tCol = hsl2rgb(vec3(hue, sat, light));
  }

  tCol += val;

  // vec2 vUv = vec2(uvCoords.x, 1.0 - abs(uvCoords.y));
  // vec4 brush = texture2D(brushTexture, vUv);

  vec4 color = vec4(tCol, opacity);

  gl_FragColor = color;
  gl_FragColor.rgb *= gl_FragColor.a;
}