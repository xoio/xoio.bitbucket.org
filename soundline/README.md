# soundline #

Audio explorations using webgl.
Based off of one of [Matt DesLauriers](https://twitter.com/mattdesl)'s Codevember examples
[http://mattdesl.github.io/codevember/21.html]()

### How do I get set up? ###

* `npm install`
* run `npm test` to compile your source file(defaults to `index.js`) which will appear in the `public/js` folder