(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _RenderBuffer = require('./src/RenderBuffer');

var _RenderBuffer2 = _interopRequireDefault(_RenderBuffer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }



var width = 512;
var height = 512;

var renderer = new THREE.WebGLRenderer();
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75.0, window.innerWidth / window.innerHeight, 1.0, 1000.0);
camera.position.z = 100;

renderer.setSize(window.innerWidth, window.innerHeight);

var geometry = new THREE.IcosahedronGeometry(100, 5);
var facesLength = geometry.faces.length;
var point = new THREE.Vector3();
var data = new Float32Array(width * height * 3);
for (var i = 0, l = data.length; i < l; i += 3) {

    var face = geometry.faces[Math.floor(Math.random() * facesLength)];

    var vertex1 = geometry.vertices[face.a];
    var vertex2 = geometry.vertices[Math.random() > 0.5 ? face.b : face.c];

    point.subVectors(vertex2, vertex1);
    point.multiplyScalar(Math.random());
    point.add(vertex1);

    data[i] = point.x;
    data[i + 1] = point.y;
    data[i + 2] = point.z;
}

var texture = new THREE.DataTexture(data, width, height, THREE.RGBFormat, THREE.FloatType, THREE.DEFAULT_MAPPING, THREE.RepeatWrapping, THREE.RepeatWrapping);
texture.needsUpdate = true;

var simulation = "#define GLSLIFY 1\n// simulation\n\nvarying vec2 vUv;\nuniform sampler2D texture;\nuniform float timer;\nuniform float frequency;\nuniform float amplitude;\nuniform float maxDistance;\n\n//\n// Description : Array and textureless GLSL 2D simplex noise function.\n//      Author : Ian McEwan, Ashima Arts.\n//  Maintainer : ijm\n//     Lastmod : 20110822 (ijm)\n//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.\n//               Distributed under the MIT License. See LICENSE file.\n//               https://github.com/ashima/webgl-noise\n//\n\nvec3 mod289(vec3 x) {\n    return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec2 mod289(vec2 x) {\n    return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec3 permute(vec3 x) {\n    return mod289(((x*34.0)+1.0)*x);\n}\n\nfloat noise(vec2 v)\n{\n    const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0\n                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)\n                     -0.577350269189626,  // -1.0 + 2.0 * C.x\n                      0.024390243902439); // 1.0 / 41.0\n    // First corner\n    vec2 i  = floor(v + dot(v, C.yy) );\n    vec2 x0 = v -   i + dot(i, C.xx);\n\n    // Other corners\n    vec2 i1;\n    //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0\n    //i1.y = 1.0 - i1.x;\n    i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);\n    // x0 = x0 - 0.0 + 0.0 * C.xx ;\n    // x1 = x0 - i1 + 1.0 * C.xx ;\n    // x2 = x0 - 1.0 + 2.0 * C.xx ;\n    vec4 x12 = x0.xyxy + C.xxzz;\n    x12.xy -= i1;\n\n    // Permutations\n    i = mod289(i); // Avoid truncation effects in permutation\n    vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))\n        + i.x + vec3(0.0, i1.x, 1.0 ));\n\n    vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);\n    m = m*m ;\n    m = m*m ;\n\n    // Gradients: 41 points uniformly over a line, mapped onto a diamond.\n    // The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)\n\n    vec3 x = 2.0 * fract(p * C.www) - 1.0;\n    vec3 h = abs(x) - 0.5;\n    vec3 ox = floor(x + 0.5);\n    vec3 a0 = x - ox;\n\n    // Normalise gradients implicitly by scaling m\n    // Approximation of: m *= inversesqrt( a0*a0 + h*h );\n    m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );\n\n    // Compute final noise value at P\n    vec3 g;\n    g.x  = a0.x  * x0.x  + h.x  * x0.y;\n    g.yz = a0.yz * x12.xz + h.yz * x12.yw;\n    return 130.0 * dot(m, g);\n}\n\nvec3 curl(float\tx,\tfloat\ty,\tfloat\tz)\n{\n\n    float\teps\t= 1., eps2 = 2. * eps;\n    float\tn1,\tn2,\ta,\tb;\n\n    x += timer * .05;\n    y += timer * .05;\n    z += timer * .05;\n\n    vec3\tcurl = vec3(0.);\n\n    n1\t=\tnoise(vec2( x,\ty\t+\teps ));\n    n2\t=\tnoise(vec2( x,\ty\t-\teps ));\n    a\t=\t(n1\t-\tn2)/eps2;\n\n    n1\t=\tnoise(vec2( x,\tz\t+\teps));\n    n2\t=\tnoise(vec2( x,\tz\t-\teps));\n    b\t=\t(n1\t-\tn2)/eps2;\n\n    curl.x\t=\ta\t-\tb;\n\n    n1\t=\tnoise(vec2( y,\tz\t+\teps));\n    n2\t=\tnoise(vec2( y,\tz\t-\teps));\n    a\t=\t(n1\t-\tn2)/eps2;\n\n    n1\t=\tnoise(vec2( x\t+\teps,\tz));\n    n2\t=\tnoise(vec2( x\t+\teps,\tz));\n    b\t=\t(n1\t-\tn2)/eps2;\n\n    curl.y\t=\ta\t-\tb;\n\n    n1\t=\tnoise(vec2( x\t+\teps,\ty));\n    n2\t=\tnoise(vec2( x\t-\teps,\ty));\n    a\t=\t(n1\t-\tn2)/eps2;\n\n    n1\t=\tnoise(vec2(  y\t+\teps,\tz));\n    n2\t=\tnoise(vec2(  y\t-\teps,\tz));\n    b\t=\t(n1\t-\tn2)/eps2;\n\n    curl.z\t=\ta\t-\tb;\n\n    return\tcurl;\n}\n\nvoid main() {\n\n    vec3 pos = texture2D( texture, vUv ).xyz;\n\n    vec3 tar = pos + curl( pos.x * frequency, pos.y * frequency, pos.z * frequency ) * amplitude;\n\n    float d = length( pos-tar ) / maxDistance;\n    pos = mix( pos, tar, pow( d, 5. ) );\n\n    gl_FragColor = vec4( pos, 1. );\n\n}\n";

var renderShader = new THREE.ShaderMaterial({
    uniforms: {
        positions: { type: "t", value: null },
        pointSize: { type: "f", value: 3 },
        spark: {
            type: 't',
            value: THREE.ImageUtils.loadTexture('/public/img/spark1.png')
        },
        size: {
            type: 'f',
            value: 1024
        }
    },
    vertexShader: "#define GLSLIFY 1\n\n//float texture containing the positions of each particle\nuniform sampler2D positions;\nuniform vec2 nearFar;\nuniform float pointSize;\nvarying vec3 vPosition;\nvarying vec2 vUv;\nuniform float size;\nvoid main() {\n\n    vec2 uv = position.xy + vec2(0.5 / size, 0.5 / size);\n\n    //the mesh is a nomrliazed square so the uvs = the xy positions of the vertices\n    vec3 pos = texture2D(positions, position.xy ).xyz;\n\n    //pos now contains the position of a point in space taht can be transformed\n    gl_Position = projectionMatrix * modelViewMatrix * vec4( pos, 1.0 );\n\n}",
    fragmentShader: "#define GLSLIFY 1\nuniform vec2 nearFar;\nuniform vec3 small;\nuniform vec3 big;\n\nuniform sampler2D spark;\nvarying vec2 vUv;\nvarying vec3 vPosition;\nvoid main()\n{\n\n      vec4 dat = texture2D(spark,vUv);\n      float depth = smoothstep( 10.24, 1.0, vPosition.y / vPosition.x);\n      //gl_FragColor = vec4( (vec3(0.0, 0.03, 0.05) + (vPosition * 0.25)), depth ) + dat;\n\n    gl_FragColor = vec4( 1.0,1.0,0.0, .2 );\n}",
    transparent: true,
    blending: THREE.AdditiveBlending
});

var buffer = new _RenderBuffer2.default(width, simulation, renderer, {
    texture: { type: "t", value: texture },
    timer: { type: "f", value: 0 },
    frequency: { type: "f", value: 0.01 },
    amplitude: { type: "f", value: 96 },
    maxDistance: { type: "f", value: 48 }
});

//6 the particles:
//create a vertex buffer of size width * height with normalized coordinates
var l = width * height;
var vertices = new Float32Array(l * 3);
for (var i = 0; i < l; i++) {

    var i3 = i * 3;
    vertices[i3] = i % width / width;
    vertices[i3 + 1] = i / width / height;
}

//create the particles geometry
var geometry = new THREE.BufferGeometry();
geometry.addAttribute('position', new THREE.BufferAttribute(vertices, 3));

//the rendermaterial is used to render the particles
var particles = new THREE.Points(geometry, renderShader);

scene.add(particles);

document.body.appendChild(renderer.domElement);

animate();
function animate() {
    requestAnimationFrame(animate);
    buffer.run();

    renderShader.uniforms.positions.value = buffer.output;
    renderer.render(scene, camera);
}

},{"./src/RenderBuffer":4}],2:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Simple mostly static class to check for basic capabilities
 */
var defaultErrorString = "Oh NOEs! your browser doesn't appear to support WebGL";

var gldefaultOpts = {
    errorString: "Oh NOEs! your browser doesn't appear to support WebGL",
    saveContext: true
};

var Capabilities = function () {
    function Capabilities() {
        _classCallCheck(this, Capabilities);
    }

    _createClass(Capabilities, null, [{
        key: "isMobile",

        /**
         * Does a basic check for mobile based on the UserAgent string
         */
        value: function isMobile() {
            var ua = navigator.userAgent;
            var search = new RegExp("Android|webOS|iPhone|iPad|iPod|BlackBerry");

            if (ua.search(search) !== -1) {
                return true;
            } else {
                return false;
            }
        }

        /**
         * Checks for the existance of Three.js
         * @returns {boolean}
         */

    }, {
        key: "haveThreejs",
        value: function haveThreejs() {
            if (window.THREE !== undefined) {
                return true;
            } else {
                return false;
            }
        }

        /**
         * Simple function to check for WebGL. Will also simultaneously return canvas and context or a
         * THREE.WebGLRenderer when using Three.js. Will check for WebGL2 first before going to WebGL1
         * @param opts object of options. See {@link gldefaultOpts} for whats available
         * @returns {*}
         */

    }, {
        key: "canWebGL",
        value: function canWebGL() {
            var opts = arguments.length <= 0 || arguments[0] === undefined ? gldefaultOpts : arguments[0];

            var canvas = document.createElement("canvas");
            var ctx = null;
            var types = ["webgl2", "experimental-webgl2", "webgl", "experimental-webgl"];

            for (var i = 0; i < 4; ++i) {
                var _ctx = canvas.getContext(types[i]);
                if (_ctx !== null) {
                    ctx = _ctx;
                }
            }

            //if context is null, console error message
            if (ctx === null) {
                console.error(opts.errormessage);
                return false;
            } else {
                //otherwise, package result
                var result = {};

                try {

                    if (opts.saveContext) {
                        result = new THREE.WebGLRenderer();
                        window.globalContext = result.getContext();
                    } else {
                        result = true;
                    }
                } catch (e) {
                    if (opts.saveContext) {
                        result = {
                            canvas: canvas,
                            gl: ctx
                        };
                        window.globalContext = result.ctx;
                    } else {
                        result = true;
                    }
                }

                return result;
            }
        }
    }]);

    return Capabilities;
}();

exports.default = Capabilities;

},{}],3:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _Capabilities = require("./Capabilities");

var _Capabilities2 = _interopRequireDefault(_Capabilities);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var GLUtils = function () {
    function GLUtils() {
        _classCallCheck(this, GLUtils);
    }

    /**
     * Checks for the existance of a WebGL extension.
     * @param extensionName the name of the extension to check for
     * @param context a webgl rendering context. You only need to pass one in
     * if you did not run {@link Capabilities#canWebGL}, otherwise will use the assigned global context
     * @returns {boolean}
     */

    _createClass(GLUtils, null, [{
        key: "checkExtension",
        value: function checkExtension(extensionName) {
            var context = arguments.length <= 1 || arguments[1] === undefined ? null : arguments[1];

            var gl = window.globalContext;
            var check = "";
            if (gl !== undefined) {
                check = gl.getExtension(extensionName);
            } else if (context !== null) {
                check = context.getExtension(extensionName);
            } else {
                console.error("Unable to check for extension due to a lack of a WebGLRenderingContext");
            }

            if (check !== null || check !== "") {
                return true;
            } else {
                return false;
            }
        }
        /**
         *	Creates a passthru shader
         */

    }, {
        key: "createPassthru",
        value: function createPassthru(buildType) {
            this.passThruVS = ['varying vec2 vUv;', 'void main() {', '  vUv = uv;', '  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );', '}'].join('\n');

            this.passThruFS = ['uniform sampler2D texture;', 'varying vec2 vUv;', 'void main() {', '  vec2 read = vUv;', '  vec4 c = texture2D( texture , vUv );', '  gl_FragColor = c ;',
            //   'gl_FragColor = vec4(1.);',
            '}'].join('\n');

            if (_Capabilities2.default.haveThreejs()) {
                var uniforms = {
                    texture: { type: 't', value: null }
                };

                var texturePassShader = new THREE.ShaderMaterial({
                    uniforms: uniforms,
                    vertexShader: this.passThruVS,
                    fragmentShader: this.passThruFS
                });
                return texturePassShader;
            }
        }
    }, {
        key: "generateRenderTarget",
        value: function generateRenderTarget(width, height, options) {
            if (_Capabilities2.default.haveThreejs()) {

                var defaults = {
                    minFilter: THREE.NearestFilter,
                    magFilter: THREE.NearestFilter,
                    format: THREE.RGBAFormat,
                    type: THREE.FloatType,
                    stencilBuffer: false
                };

                return new THREE.WebGLRenderTarget(width, height, defaults);
            }
        }

        //alias for {@link GLUtils#generateShader}

    }, {
        key: "createShader",
        value: function createShader(vSource, fSource, uniforms, context) {
            return GLUtils.generateShader(vSource, fSource, uniforms, context);
        }

        /**
         * Generates a Shader program
         * @param vSource the source for the vertex shader
         * @param fSource the source for the fragment shader
         * @param uniforms any additional uniforms you want embeded in the shader
         * @param context a WebGLRendering context when not using threejs
         * @returns {THREE.ShaderMaterial}
         */

    }, {
        key: "generateShader",
        value: function generateShader(vSource, fSource, uniforms, context) {
            if (_Capabilities2.default.haveThreejs()) {
                var vertex = "";
                var _uniforms = {
                    time: {
                        type: 'f',
                        value: 0.0
                    },
                    mousePos: {
                        type: 'v2',
                        value: new THREE.Vector2()
                    }
                };

                if (uniforms !== undefined) {
                    Object.assign(_uniforms, uniforms);
                }

                if (vSource === "passthru") {
                    vertex = GLUtils.createPassthruVertex();
                }

                return new THREE.ShaderMaterial({
                    vertexShader: vertex,
                    fragmentShader: fSource,
                    uniforms: uniforms
                });
            } else {
                var gl = context;
                var vertexShader = gl.createShader(gl.VERTEX_SHADER);
                var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);

                gl.shaderSource(vertexShader, vSource);

                gl.shaderSource(fragmentShader, fSource);

                gl.compileShader(vertexShader);
                if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
                    console.error("Shader failed to compile", gl.getShaderInfoLog(vertexShader));
                    return null;
                }

                gl.compileShader(fragmentShader);
                if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
                    console.error("Shader failed to compile", gl.getShaderInfoLog(fragmentShader));
                    return null;
                }

                var program = gl.createProgram();

                gl.attachShader(program, vertexShader);
                gl.attachShader(program, fragmentShader);

                gl.deleteShader(vertexShader);
                gl.deleteShader(fragmentShader);

                gl.linkProgram(program);

                if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
                    console.error("Shader program failed to link", gl.getProgramInfoLog(program));
                    gl.deleteProgram(program);
                    return null;
                }

                return program;
            }
        }

        /**
         * Generates a Fbo (or a THREE.WebGLRenderTarget when using Three.js)
         * @param width the width for the fbo
         * @param height the height for the fbo
         * @param options any options to pass into the fbo
         * @returns {THREE.WebGLRenderTarget}
         */

    }, {
        key: "generateFbo",
        value: function generateFbo() {
            var width = arguments.length <= 0 || arguments[0] === undefined ? 1024 : arguments[0];
            var height = arguments.length <= 1 || arguments[1] === undefined ? 1024 : arguments[1];
            var options = arguments[2];

            if (_Capabilities2.default.haveThreejs()) {
                var defaults = {
                    minFilter: THREE.NearestFilter,
                    magFilter: THREE.NearestFilter,
                    format: THREE.RGBAFormat,
                    type: THREE.FloatType,
                    stencilBuffer: false
                };

                if (options !== undefined) {
                    Object.assign(defaults, options);
                }

                return new THREE.WebGLRenderTarget(width, height, defaults);
            }
        }

        /**
         * Generates a pass-thru vertex shader
         * @returns {string}
         * @private
         */

    }, {
        key: "createPassthruVertex",
        value: function createPassthruVertex() {
            //more threejs specific
            if (_Capabilities2.default.haveThreejs()) {
                return ['varying vec2 vUv;', 'void main() {', '  vUv = uv;', '  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );', '}'].join('\n');
            }
        }
    }]);

    return GLUtils;
}();

exports.default = GLUtils;

},{"./Capabilities":2}],4:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * A class for doing GPU pingponging.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * Largely based on Isaac Cohen's PhysicsRenderer(https://github.com/cabbibo), cleaned up a bit, and
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * adding some flags for the future so this can (hopefully) be used interchangeably with
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * regular WebGL code in addition to Three.js
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * Requires nitro-glutils
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _GLUtils = require("./GLUtils.js");

var _GLUtils2 = _interopRequireDefault(_GLUtils);

var _Capabilities = require("./Capabilities");

var _Capabilities2 = _interopRequireDefault(_Capabilities);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var RenderBuffer = function () {
    function RenderBuffer(size, shader, renderer, additionalUniforms) {
        _classCallCheck(this, RenderBuffer);

        this.renderer = renderer;

        this.size = size !== undefined ? size : 128;
        this.s2 = size * size;

        this.renderer = renderer;

        this.clock = new THREE.Clock();

        this.resolution = new THREE.Vector2(this.size, this.size);

        this.counter = 0;

        this.texturePassProgram = _GLUtils2.default.createPassthru();

        //! a flag to help aid in determining how many render targets to create.
        this.dataSet = false;

        //! a flag to indicate whether or not render targets have been built.
        this.fbosBuilt = false;

        // WHERE THE MAGIC HAPPENS
        var uniforms = this._buildUniforms(additionalUniforms);

        this.simulation = _GLUtils2.default.generateShader('passthru', shader, uniforms);
        this.simulationMaterial = this.simulation;

        //build scene for target rendering
        this._buildRenderScene();
    }

    _createClass(RenderBuffer, [{
        key: "run",
        value: function run() {
            //update default uniforms
            this.simulationMaterial.uniforms.timer.value += 0.01;

            /**
             * If we've set some base data,
             * we run it through all of the render targets
             * keeping track of the previous result.
             *
             * If we haven't, we only utilize one target.
             */
            if (this.dataSet) {
                this.update();
            } else {

                //quickly build out a render target
                if (this.counter < 1) {
                    this._buildRenderTargets();
                    this.counter++;
                }

                //1 update the simulation and render the result in a target texture
                this.renderer.render(this.scene, this.camera, this.rtt, true);

                //set the output so we can grab the result in the rendering shader.
                this.output = this.rtt;
            }
        }

        /**
         * Updates the simulation
         */

    }, {
        key: "update",
        value: function update() {
            var flipFlop = this.counter % 3;

            if (flipFlop == 0) {
                this.simulation.uniforms.destinationTexture.value = this.rt_2;
                this.pass(this.simulation, this.rt_3);
            } else if (flipFlop == 1) {
                this.simulation.uniforms.destinationTexture.value = this.rt_3;
                this.pass(this.simulation, this.rt_1);
            } else if (flipFlop == 2) {
                this.simulation.uniforms.destinationTexture.value = this.rt_1;
                this.pass(this.simulation, this.rt_2);
            }

            this.counter++;
        }

        /**
         * Runs a pass against the current target. Sets the output target for use
         * in rendering
         * @param shader the shader to use
         * @param target the target to run the pass on
         *
         * TODO remember to take out the shader param as we don't really need it.
         */

    }, {
        key: "pass",
        value: function pass(shader, target) {

            this.mesh.material = shader;

            this.renderer.render(this.scene, this.camera, target, false);
            this.output = target;
        }

        /**
         * Gets the current output of the simulation.
         * @returns {*}
         */

    }, {
        key: "getOutput",
        value: function getOutput() {
            if (this.output !== undefined) {
                return this.output;
            }
        }

        /**
         * Set a additional uniform variable in the simulation
         * @param uniform name of hte uniform
         * @param value the value for the nuniform
         */

    }, {
        key: "setUniform",
        value: function setUniform(uniform, value) {
            if (THREE) {
                var determineType = function determineType(value) {
                    var instance = null;

                    if (value instanceof THREE.Vector2) {
                        instance = 'v2';
                    } else if (value instanceof THREE.Vector3) {
                        instance = 'v3';
                    } else if (value instanceof THREE.Texture) {
                        instance = 't';
                    } else if (value instanceof THREE.WebGLRenderTarget) {
                        instance = 't';
                    } else {
                        instance = 'f';
                    }

                    return instance;
                };

                this.simulation.uniforms[uniform] = {
                    type: determineType(value),
                    value: value
                };
            }
        }
    }, {
        key: "updateUniform",
        value: function updateUniform(uniform, value) {
            try {
                this.simulation.uniforms[uniform].value = value;
            } catch (e) {}
        }

        /**
         * Kicks things off with a bit of random data.
         * @param size
         * @param alpha
         */

    }, {
        key: "resetRand",
        value: function resetRand(size, alpha) {

            if (THREE) {
                var size = size || 100;
                var data = new Float32Array(this.s2 * 4);

                for (var i = 0; i < data.length; i++) {

                    //console.log('ss');
                    data[i] = (Math.random() - .5) * size;

                    if (alpha && i % 4 === 3) {
                        data[i] = 0;
                    }
                }

                var texture = new THREE.DataTexture(data, this.size, this.size, THREE.RGBAFormat, THREE.FloatType);

                texture.minFilter = THREE.NearestFilter, texture.magFilter = THREE.NearestFilter, texture.needsUpdate = true;

                this.setData(texture);
            }
        }

        ////////////////// INTERNAL /////////////////////////

    }, {
        key: "_buildUniforms",
        value: function _buildUniforms(uniforms) {
            var _uniforms = {
                originTexture: {
                    type: "t",
                    value: null
                },
                destinationTexture: {
                    type: "t",
                    value: null
                },
                resolution: {
                    type: "v2",
                    value: this.resolution
                },
                timer: {
                    type: 'f',
                    value: 0.0
                }
            };

            if (uniforms !== undefined) {
                Object.assign(_uniforms, uniforms);
            }
            return _uniforms;
        }

        /**
         * Sets the data to be used for the simulation and runs a pass across all the buffers.
         * @param texture
         */

    }, {
        key: "setData",
        value: function setData(texture) {
            this.texture = texture;
            this.texturePassProgram.uniforms.texture.value = texture;

            this.dataSet = true;

            this._buildRenderTargets();

            this.pass(this.texturePassProgram, this.rt_1);
            this.pass(this.texturePassProgram, this.rt_2);
            this.pass(this.texturePassProgram, this.rt_3);
        }
    }, {
        key: "_buildRenderScene",
        value: function _buildRenderScene() {
            if (THREE) {
                var camera = new THREE.OrthographicCamera(-0.5, 0.5, 0.5, -0.5, 0, 1);
                var scene = new THREE.Scene();
                var mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(1, 1), this.simulationMaterial);

                var size = this.size;
                scene.add(mesh);

                this.mesh = mesh;
                this.camera = camera;
                this.scene = scene;
            }
        }
    }, {
        key: "_buildRenderTargets",
        value: function _buildRenderTargets() {
            if (_Capabilities2.default.haveThreejs()) {
                if (this.dataSet) {
                    // Sets up our render targets
                    this.rt_1 = _GLUtils2.default.generateRenderTarget(this.size, this.size);
                    this.rt_2 = this.rt_1.clone();
                    this.rt_3 = this.rt_1.clone();
                } else {

                    this.rtt = _GLUtils2.default.generateRenderTarget(this.size, this.size);
                }
            } else {
                this.rt_1 = this.renderer.createFramebuffer();
                this.rt_2 = this.renderer.createFramebuffer();
                this.rt_3 = this.renderer.createFramebuffer();
            }

            this.fbosBuilt = true;
        }
    }]);

    return RenderBuffer;
}();

exports.default = RenderBuffer;

},{"./Capabilities":2,"./GLUtils.js":3}]},{},[1]);
