# Particle..somthing? #

Just a simple experiment with particles done while reading [Nicolas Barradeau's blog post](http://barradeau.com/blog/?p=621) about FBOs(or RenderTargets if you use Threejs). Parts of it are the same code with some small twists here and there. 

### How do I get set up? ###

* `npm install`
* run `npm test` to compile your source file(defaults to `index.js`) which will appear in the `public/js` folder