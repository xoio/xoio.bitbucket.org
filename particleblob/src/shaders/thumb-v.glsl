varying vec2 vUv;

float angleVel = 0.2;
float amplitude = 100.0;

uniform float frameCount;
uniform float noiseTime;
uniform float speed;
uniform float time;
uniform vec2 mousePosition;

#pragma glslify: noise2 = require(glsl-noise/simplex/2d)
#pragma glslify: noise = require(glsl-noise/simplex/4d)


void main() {
    vUv = uv;

    vec4 pos = vec4(position,1.0);

    float offset = time * 0.004;
    pos.z += sin( pos.x * 1.1467 + offset ) * (noiseTime * 0.04) + cos( pos.z * 0.7325 + offset ) * noise(pos);


	gl_Position = projectionMatrix * modelViewMatrix *  pos;

}