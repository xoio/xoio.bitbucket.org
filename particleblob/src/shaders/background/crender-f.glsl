uniform sampler2D spark;
varying vec3 vPosition;
varying vec2 vUv;
void main(){
    vec4 particle = texture2D(spark,vUv);
  float depth = smoothstep( 10.24, 1.0, gl_FragCoord.z / gl_FragCoord.w );
  vec4 calcColor = vec4( (vec3(0.0, 0.03, 0.05) + (vPosition * 0.25)), depth );
  gl_FragColor = calcColor * particle;

}

