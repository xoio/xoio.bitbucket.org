

//float texture containing the positions of each particle
uniform sampler2D positions;
uniform vec2 nearFar;
uniform float pointSize;
varying vec3 vPosition;
varying vec2 vUv;
uniform float size;
void main() {

    vec2 uv = position.xy + vec2(0.5 / size, 0.5 / size);


    //the mesh is a nomrliazed square so the uvs = the xy positions of the vertices
    vec3 pos = texture2D(positions, position.xy ).xyz;





    //pos now contains the position of a point in space taht can be transformed
    gl_Position = projectionMatrix * modelViewMatrix * vec4( pos, 1.0 );


}