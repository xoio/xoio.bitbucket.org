
uniform sampler2D t_pos;

varying vec2 vUv;
varying vec3 vPosition;
void main(){
    vUv = uv;
    vPosition = position;
  vec4 pos = texture2D( t_pos , position.xy );

  vec3 dif = cameraPosition - pos.xyz;

  gl_PointSize = min( 5. ,  50. / length( dif ));
  gl_Position = projectionMatrix * modelViewMatrix * vec4( pos.xyz , 1. );


}
