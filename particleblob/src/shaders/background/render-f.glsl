uniform vec2 nearFar;
uniform vec3 small;
uniform vec3 big;

uniform sampler2D spark;
varying vec2 vUv;
varying vec3 vPosition;
void main()
{

      vec4 dat = texture2D(spark,vUv);
      float depth = smoothstep( 10.24, 1.0, vPosition.y / vPosition.x);
      //gl_FragColor = vec4( (vec3(0.0, 0.03, 0.05) + (vPosition * 0.25)), depth ) + dat;

    gl_FragColor = vec4( 1.0,1.0,0.0, .2 );
}