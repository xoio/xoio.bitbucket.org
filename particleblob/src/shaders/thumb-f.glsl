varying vec2 vUv;
uniform sampler2D thumbTex;
uniform float time;
void main(){
    vec4 dat = texture2D(thumbTex,vUv);

    gl_FragColor = dat;
    //gl_FragColor = vec4(sin(time),1.0,0.0,1.0);
}