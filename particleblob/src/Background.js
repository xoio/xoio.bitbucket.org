import RenderBuffer from "./RenderBuffer"
const glslify = require('glslify');

class Background {
    constructor(width,height){

        let renderer = new THREE.WebGLRenderer();
        let scene = new THREE.Scene();
        let camera = new THREE.PerspectiveCamera( 75, width / height , 1, 1000 );
        camera.position.z = 100;


        renderer.setClearColor( 0x303030 )
        renderer.setSize(width,height);

        var width  = 128;
        var height = 512;

        let simulation = glslify('./shaders/background/curl.glsl');


        this.buffer = new RenderBuffer(width,simulation,renderer,{
            noiseSize: { type:"f" , value: .1 }
        })

        this.buffer.resetRand(5)

        this.renderShader = new THREE.ShaderMaterial( {
            uniforms: {
                t_pos:{ type:"t" , value: null }
            },
            vertexShader: glslify('./shaders/background/crender-v.glsl'),
            fragmentShader: glslify('./shaders/background/crender-f.glsl'),
            transparent: true
        } );


        var geo = this.createLookupGeometry( width );


        var particles = new THREE.Points( geo , this.renderShader );
        particles.frustumCulled = false;
        scene.add( particles );


        this.renderer = renderer;
        this.scene = scene;
        this.camera = camera;

    }

    createLookupGeometry( size ) {

        var geo = new THREE.BufferGeometry();
        var positions = new Float32Array(size * size * 3);

        for (var i = 0, j = 0, l = positions.length / 3; i < l; i++, j += 3) {

            positions[j] = ( i % size ) / size;
            positions[j + 1] = Math.floor(i / size) / size;

        }

        var posA = new THREE.BufferAttribute(positions, 3);
        geo.addAttribute('position', posA);

        return geo;

    }
    render(){
        this.buffer.update();

        this.renderShader.uniforms.t_pos.value = this.buffer.output;
        this.renderer.render(this.scene,this.camera);
    }
    _buildParticles(render){
        var width = 512;
        var height = 512;

        //6 the particles:
        //create a vertex buffer of size width * height with normalized coordinates
        var l = (width * height );
        var vertices = new Float32Array( l * 3 );
        for ( var i = 0; i < l; i++ ) {

            var i3 = i * 3;
            vertices[ i3 ] = ( i % width ) / width ;
            vertices[ i3 + 1 ] = ( i / width ) / height;
        }

        //create the particles geometry
        var geometry = new THREE.BufferGeometry();
        geometry.addAttribute( 'position',  new THREE.BufferAttribute( vertices, 3 ) );

        //the rendermaterial is used to render the particles
        this.particles = new THREE.Points( geometry, render );

        this.scene.add(this.particles);
    }

    appendTo(container){
        container.appendChild(this.renderer.domElement);
    }

    //returns a Float32Array buffer of spherical 3D points
    getSphere( count, size ) {

        var len = count * 3;
        var data = new Float32Array(len);
        var p = new THREE.Vector3();
        for (var i = 0; i < len; i += 3) {
            this.getPoint(p, size);
            data[i] = p.x;
            data[i + 1] = p.y;
            data[i + 2] = p.z;
        }
        return data;
    }

    getPoint(v,size) {
        //the 'discard' method, not the most efficient
        v.x = Math.random() * 2 - 1;
        v.y = Math.random() * 2 - 1;
        v.z = Math.random() * 2 - 1;
        if (v.length() > 1)return this.getPoint(v, size);
        return v.normalize().multiplyScalar(size);

        //exact but slow-ish
        /*
         var phi = Math.random() * 2 * Math.PI;
         var costheta = Math.random() * 2 -1;
         var u = Math.random();

         var theta = Math.acos( costheta );
         var r = size * Math.cbrt( u );

         v.x = r * Math.sin( theta) * Math.cos( phi );
         v.y = r * Math.sin( theta) * Math.sin( phi );
         v.z = r * Math.cos( theta );
         return v;
         //*/
    }

    _buildData(){

        var data = this.getSphere(512 * 512,128);
        var width = 512;
        var height = 512;

        this.data = data;

    }




}

export default Background