const glslify = require('glslify');
import Capabilities from "./Capabilities"
import MathUtils from "./MathUtils"
class Thumbnail {
    /**
     * Constructor for displaying thumbnail
     * @param img the <img> element to use for the texture
     * @param width a optional width. If one isn't provided, will default to image width
     * @param height a optional height. If one isn't provided will default to image height
     */
    constructor(img,opts={}){
        img = img !== undefined ? img : (function(){
            console.error("Thumbnail needs a image");
            return;
        })();

        //assign some default props
        Object.assign(opts,{background:0xffffff});

        this.width = opts.width || img.width;
        this.height = opts.height || img.height;


        //check for webgl capabilities
        this.instance = Capabilities.canWebGL();
        this.instance.setClearColor(opts.background);

        this.framecount = 0.0;

        this.img = img;

        this.mousePos = {x:0,y:0};
        this.mouseOver = false;

        this._setupThumb(img);

        this._setupMouseOverOut();
    }

    _setupMouseOverOut(){
        let self = this;
        this.instance.domElement.addEventListener("mouseover",function(e){
            self.mouseOver = true;
        });
        this.instance.domElement.addEventListener("mouseout",function(e){
            self.mouseOver = false;
        });
    }

    addEventListener(type,callback){
        let self = this;

        if(Capabilities.haveThreejs()){
            if((type !== "mouseover") || (type !== "mouseout")){
                this.instance.domElement.addEventListener(type,function(e){
                    callback(e,self);
                });
            }else{
                console.warn("mouseover and mouseout are already set");
            }
        }
    }

    setMousePosition(pos){
       if((pos.hasOwnProperty("x") && pos.hasOwnProperty("y"))){
           this.mousePos.x = pos.x;
           this.mousePos.y = pos.y;
       }
    }


    appendTo(el){
        el.appendChild(this.instance.domElement);
    }

    /**
     * Sets up the thumbnail
     * @private
     */
    _setupThumb(img){
        let instance = this.instance;
        if(Capabilities.haveThreejs()){
            instance.setSize(this.width,this.height);
            this.scene = new THREE.Scene();
            this.camera = new THREE.PerspectiveCamera(75.0,this.width / this.height, 1.0,10000.0);

            var geometry = new THREE.PlaneGeometry(this.width,this.height);
            var tessellateModifier = new THREE.TessellateModifier(8);

            for(var i = 0; i < 6; ++i){
               tessellateModifier.modify(geometry);
            }
            this._calculateZPosition();
            
            var tex = new THREE.Texture(img);
            tex.needsUpdate = true;

            var mat = new THREE.ShaderMaterial({
                vertexShader:glslify('./shaders/thumb-v.glsl'),
                fragmentShader:glslify("./shaders/thumb-f.glsl"),
                uniforms:{
                    thumbTex:{
                        type:'t',
                        value:tex
                    },
                    frameCount:{
                        type:'f',
                        value:0.0
                    },
                    noiseTime:{
                        type:'f',
                        value:Math.random() * 1000
                    },

                    speed:{
                        type:'f',
                        value:MathUtils.randomRange(5,20)
                    },
                    time:{
                        type:'f',
                        value:0.0
                    },
                    mousePosition :{
                        type:'v2',
                        value:new THREE.Vector2(0,0)
                    }
                }
            });

            mat.needsUpdate = true;
            geometry.verticesNeedUpdate = true;

            var mesh = new THREE.Mesh(geometry,mat);
            //mesh.scale.x = this.width;
            //mesh.scale.y = this.height;


            this.mesh = mesh;
            this.material = mat;

            this.scene.add(mesh);
            document.body.appendChild(instance.domElement);
        }

    }


    //calculate target Z as per this thread
    //http://stackoverflow.com/questions/14533135/how-to-calculate-the-z-distance-of-a-camera-to-view-an-image-at-100-of-its-orig
    _calculateZPosition(){
        var vFOV = this.camera.fov * (Math.PI / 180);
        var targetZ = this.height / (2 * Math.tan(vFOV / 2));
        this.camera.position.z = targetZ;
    }

    /**
     * To be called when the window is resized
     */
    resize(){
        if(Capabilities.haveThreejs()){
            this.camera.aspect = this.width / this.height;
            this._calculateZPosition();
            this.camera.updateProjectionMatrix();

            this.material.needsUpdate = true;
            this.instance.setSize( this.width, this.height );
        }
    }

    render(){
        var time = Date.now() * 0.01;
        this.framecount += 1.0;
        this.material.uniforms.frameCount.value = this.framecount;
        this.material.uniforms.noiseTime.value += 0.001
        this.material.uniforms.time.value = performance.now();

        this.material.uniforms.mousePosition.value.x = this.mousePos.x;
        this.material.uniforms.mousePosition.value.y = this.mousePos.y;
        this.instance.render(this.scene,this.camera);
    }

}

export default Thumbnail
