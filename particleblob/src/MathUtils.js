class MathUtils {
    constructor(){}
    static map(value, min1, max1, min2, max2) {
        return this.lerp(this.norm(value, min1, max1), min2, max2);
    }

    static lerp(value, min, max) {
        return min + (max - min) * value;
    }

    static norm(value , min, max) {
        return (value - min) / (max - min);
    }

    static randomRange(min, max) {
        return min + Math.random() * (max - min);
    }
}

export default MathUtils;