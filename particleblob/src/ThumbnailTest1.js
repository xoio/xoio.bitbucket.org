import Thumbnail from "./src/Thumbnail"
var container = document.createElement("div");
container.style.position = "relative";
document.body.appendChild(container);

var img = document.getElementsByTagName("img")[0];

var title = document.createElement("h1");
title.innerHTML = "test";
title.style.cssText = "position:absolute;left:50%;top:50%;transform:translateX(-50%) translateY(-50%)";
container.appendChild(title)

var thumb = new Thumbnail(img,{
    width:320,
    height:240
});
thumb.appendTo(container);



thumb.addEventListener("mousemove",function(event){
    var mouse = {};

    var mx = ( event.clientX);
    var my = ( event.clientY );

    var cx = thumb.width / 2;
    var cy = thumb.height / 2;

    mouse.x = mx - cx;
    mouse.y = my - cy;
    thumb.setMousePosition(mouse);
});



var stats = new Stats();
stats.domElement.style.position = 'absolute';
stats.domElement.style.top = '0px';
container.appendChild( stats.domElement );
animate();
function animate(){
    requestAnimationFrame(animate);
    stats.update();
    thumb.render();
}