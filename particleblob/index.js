const glslify = require('glslify');
import RenderBuffer from "./src/RenderBuffer"

var width  = 512;
var height = 512;

var renderer = new THREE.WebGLRenderer();
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75.0,window.innerWidth/window.innerHeight,1.0,1000.0);
camera.position.z = 100;


renderer.setSize(window.innerWidth,window.innerHeight);


var geometry = new THREE.IcosahedronGeometry( 100, 5 );
var facesLength = geometry.faces.length;
var point = new THREE.Vector3();
var data = new Float32Array( width * height * 3 );
for ( var i = 0, l = data.length; i < l; i += 3 ) {

    var face = geometry.faces[ Math.floor( Math.random() * facesLength ) ];

    var vertex1 = geometry.vertices[ face.a ];
    var vertex2 = geometry.vertices[ Math.random() > 0.5 ? face.b : face.c ];

    point.subVectors( vertex2, vertex1 );
    point.multiplyScalar( Math.random() );
    point.add( vertex1 );

    data[ i ] = point.x;
    data[ i + 1 ] = point.y;
    data[ i + 2 ] = point.z;

}


var texture = new THREE.DataTexture( data, width, height, THREE.RGBFormat, THREE.FloatType, THREE.DEFAULT_MAPPING, THREE.RepeatWrapping, THREE.RepeatWrapping );
texture.needsUpdate = true;

let simulation = glslify('./src/shaders/background/simulation.glsl');

var renderShader = new THREE.ShaderMaterial( {
    uniforms: {
        positions: { type: "t", value: null },
        pointSize: { type: "f", value: 3 },
        spark:{
            type:'t',
            value:THREE.ImageUtils.loadTexture('/public/img/spark1.png')
        },
        size:{
            type:'f',
            value:1024
        }
    },
    vertexShader: glslify('./src/shaders/background/render-v.glsl'),
    fragmentShader: glslify('./src/shaders/background/render-f.glsl'),
    transparent: true,
    blending:THREE.AdditiveBlending
} );


var buffer = new RenderBuffer( width, simulation, renderer,{
    texture: { type: "t", value: texture },
    timer: { type: "f", value: 0},
    frequency: { type: "f", value: 0.01 },
    amplitude: { type: "f", value: 96 },
    maxDistance: { type: "f", value: 48 }
});

//6 the particles:
//create a vertex buffer of size width * height with normalized coordinates
var l = (width * height );
var vertices = new Float32Array( l * 3 );
for ( var i = 0; i < l; i++ ) {

    var i3 = i * 3;
    vertices[ i3 ] = ( i % width ) / width ;
    vertices[ i3 + 1 ] = ( i / width ) / height;
}

//create the particles geometry
var geometry = new THREE.BufferGeometry();
geometry.addAttribute( 'position',  new THREE.BufferAttribute( vertices, 3 ) );

//the rendermaterial is used to render the particles
var particles = new THREE.Points( geometry, renderShader );

scene.add(particles);

document.body.appendChild(renderer.domElement)


animate();
function animate(){
    requestAnimationFrame(animate);
    buffer.run();

    renderShader.uniforms.positions.value = buffer.output;
    renderer.render(scene,camera);
}
