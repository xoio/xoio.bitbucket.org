# STUFF YOU CAN READ! #

This contains the source for a bunch of small webgl practice exercises or, maybe a better word might be "sketches" that I've been doing off and on for a little while. These were used as opportunities to try better understand WebGL / OpenGL fundamentals as well as to further examine the ideas of things I've seen on the net. 

### How do I get set up? ###

Currently undergoing re-structuring again. The current plan is to just keep the source here and move the viewable versions elsewhere. 

But for now... it's a bit weird. I've only recently just started trying to figure out a good format that I like for structuring these things and I only really just started including source files as well, so, eh... it's a bit of a mess at the moment. 

Essentially, it boils down to this

* any public facing things go in folders called `public`
* source files stay outside of the public folder.

With the exception of `beats`, `birds_experiment`, and `waves` the rest generally follow this same format. With those two, I had to shift things a bit so those three folders might look a little weird. 

Anyways, in terms of getting setup and trying it out yourself, except for `ribbontrails` and the previously mentioned exceptions, these are all node.js based projects and should all include the source, so you should be able to just run:

* `npm install`
* then run `gulp` which will automatically bring up a browser with the project. 



## Notes ##
* These should all be considered desktop experiences. Yes WebGL can nowadays be run on just about every phone but often times you might need to code things a bit differently to take into account of processor/memory permutations which , well, was not something I was thinking of when making these.
* there is some, uh.. unlicensed music.. hehehe  ∩(︶▽︶)∩. If you're the copyright owner, happen to come across this and would like me to take the music out, please just send me a note @ joe@xoio.co